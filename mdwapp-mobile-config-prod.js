// This section sets up some basic app metadata,
// the entire section is optional.
App.info({
    version: "0.5.0",
    id: 'com.vatfree.vattie',
    name: 'Vattie',
    description: 'Get VAT back like a boss',
    author: 'vatfree.com',
    email: 'support@vatfree.com',
    website: 'https://www.vatfree.com'
});

// Set up resources such as icons and launch screens.
App.icons({
    "app_store": "resources/icons/app_store.png", // 1024x1024
    "iphone_2x": "resources/icons/iphone_2x.png", // 120x120
    "iphone_3x": "resources/icons/iphone_3x.png", // 180x180
    "ipad": "resources/icons/ipad.png", // 76x76
    "ipad_2x": "resources/icons/ipad_2x.png", // 152x152
    "ipad_pro": "resources/icons/ipad_pro.png", // 167x167
    "ios_settings": "resources/icons/ios_settings.png", // 29x29
    "ios_settings_2x": "resources/icons/ios_settings_2x.png", // 58x58
    "ios_settings_3x": "resources/icons/ios_settings_3x.png", // 87x87
    "ios_spotlight": "resources/icons/ios_spotlight.png", // 40x40
    "ios_spotlight_2x": "resources/icons/ios_spotlight_2x.png", // 80x80
    "ios_notification": "resources/icons/ios_notification.png", // 20x20
    "ios_notification_2x": "resources/icons/ios_notification_2x.png", // 40x40
    "ios_notification_3x":"resources/icons/ios_notification_3x.png", // 60x60
    "iphone_legacy": "resources/icons/iphone_legacy.png", // 57x57
    "iphone_legacy_2x": "resources/icons/iphone_legacy_2x.png", // 114x114
    "ipad_spotlight_legacy": "resources/icons/ipad_spotlight_legacy.png", // 50x50
    "ipad_spotlight_legacy_2x": "resources/icons/ipad_spotlight_legacy_2x.png", // 100x100
    "ipad_app_legacy": "resources/icons/ipad_app_legacy.png", // 72x72
    "ipad_app_legacy_2x": "resources/icons/ipad_app_legacy_2x.png", // 144x144
    "android_mdpi": "resources/icons/android_mdpi.png", // 48x48
    "android_hdpi": "resources/icons/android_hdpi.png", // 72x72
    "android_xhdpi": "resources/icons/android_xhdpi.png", // 96x96
    "android_xxhdpi": "resources/icons/android_xxhdpi.png", // 144x144
    "android_xxxhdpi": "resources/icons/android_xxxhdpi.png", // 192x192
    "android_store": "resources/icons/android_store.png" // 512x512
});

App.launchScreens({
    "android_mdpi_portrait": "resources/splashes/android_mdpi_portrait.png", // 320x480
    "android_mdpi_landscape": "resources/splashes/android_mdpi_landscape.png", // 480x320
    "android_hdpi_portrait": "resources/splashes/android_hdpi_portrait.png", // 480x800
    "android_hdpi_landscape": "resources/splashes/android_hdpi_landscape.png", // 800x480
    "android_xhdpi_portrait": "resources/splashes/android_xhdpi_portrait.png", // 720x1280
    "android_xhdpi_landscape": "resources/splashes/android_xhdpi_landscape.png", // 1280x720
    "android_xxhdpi_portrait": "resources/splashes/android_xxhdpi_portrait.png", // 1080x1440
    "android_xxhdpi_landscape": "resources/splashes/android_xxhdpi_landscape.png", // 1440x1080
    "android_featured": "resources/splashes/android_featured.png" // 1024x500
});

App.appendToConfig(`
  <splash src="../../../resources/splashes/Default@3x~universal~anyany.png" />
`);

// Set PhoneGap/Cordova preferences
App.setPreference('Orientation', 'portrait');
App.setPreference('StatusBarOverlaysWebView', 'true');
App.setPreference('StatusBarStyle', 'lightcontent');
App.setPreference('StatusBarBackgroundColor', '#e2017b');
App.setPreference('BackgroundColor', '#e2017b');
App.setPreference('BackupWebStorage', 'local');
App.setPreference('fullscreen', 'false');
App.setPreference('WebAppStartupTimeout', 60000);

// Pass preferences for a particular PhoneGap/Cordova plugin
//App.configurePlugin('com.phonegap.plugins.facebookconnect', {
//    APP_ID: '1234567890',
//    API_KEY: 'supersecretapikey'
//});
// Add custom tags for a particular PhoneGap/Cordova plugin
// to the end of generated config.xml.

App.configurePlugin('cordova-plugin-googleplus', {
    //'CLIENT_ID': '206501429660-ultgd8b8jjav8g0j55lcq5hsmf6svf6v.apps.googleusercontent.com'
    'REVERSED_CLIENT_ID': 'com.googleusercontent.apps.206501429660-ultgd8b8jjav8g0j55lcq5hsmf6svf6v'
});

App.configurePlugin('cordova-plugin-googlemaps', {
    'API_KEY_FOR_ANDROID': 'AIzaSyDDMRM6ivOHbu9aFGckYCMyO7kYRGw6wfk',
    'API_KEY_FOR_IOS': 'AIzaSyDDMRM6ivOHbu9aFGckYCMyO7kYRGw6wfk'
});

// Android 4.0.3
App.setPreference('android-targetSdkVersion', '27');
App.setPreference('android-minSdkVersion', '27');

App.accessRule("*");
App.accessRule("blob:*");

// Universal Links is shown as an example here.
App.appendToConfig(`
    <allow-navigation href="https://www.vatfree.com/*" />
    <platform name="ios">
        <plugin name="cordova-plugin-media-capture" source="npm" spec="*">
            <variable name="CAMERA_USAGE_DESCRIPTION" value="App would like to access the camera." />
            <variable name="MICROPHONE_USAGE_DESCRIPTION" value="App would like to access the microphone." />
            <variable name="PHOTOLIBRARY_USAGE_DESCRIPTION" value="App would like to access the library." />
        </plugin>
        <config-file parent="NSPhotoLibraryUsageDescription" platform="ios" target="*-Info.plist">
            <string>This app needs access to your Photo Library to allow saved photo uploads.</string>
        </config-file>
        <config-file parent="NSCameraUsageDescription" platform="ios" target="*-Info.plist">
            <string>This app needs access to the camera to allow photo uploads.</string>
        </config-file>
        <config-file parent="NSLocationAlwaysUsageDescription" platform="ios" target="*-Info.plist">
            <string>This app needs access to your location.</string>
        </config-file>
    </platform>
`);

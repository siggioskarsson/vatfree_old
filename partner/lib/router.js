FlowRouter.notFound = {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "login"});
    }
};

FlowRouter.route('/', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "login"});
    }
});

FlowRouter.route('/invoice/:invoiceId/:secret', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "view_invoice"});
    }
});

FlowRouter.route('/invoices/:invoiceId/:secret', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "view_invoices"});
    }
});

FlowRouter.route('/dashboard/:invoiceId/:secret', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "view_dashboard", title: "Real-time Tax-free Shopping Dashboard"});
    }
});

FlowRouter.route('/register', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "view_register"});
    }
});

FlowRouter.route('/register/:shopId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "view_register"});
    }
});

FlowRouter.route('/register/contract', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "view_register_contract"});
    }
});

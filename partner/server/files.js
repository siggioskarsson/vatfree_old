Files.allow({
    download:function(userId, doc) {
        return doc.target === 'receipts' || doc.target === 'invoices';
    }
});

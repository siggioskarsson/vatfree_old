//import invoiceJobs from "meteor/vatfree:invoices/invoiceJobs"

Meteor.methods({
    'partner-create-invoice-pdf-images'(invoiceId) {
        // TEMP until we have our partners login and check it properly in the normal function
        check(invoiceId, String);

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice) return false;

        if (Files.find({
                itemId: invoiceId,
                target: "invoices",
                subType: "images"
            }).count() > 0) {

            return false;
        }

        let job = new Job(invoiceJobs, 'invoicePdfImages',
            {
                invoiceId: invoice._id,
                invoiceNr: invoice.invoiceNr
            }
        );
        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 5,
                wait: 5 * 60 * 1000
            })  // 5 minutes between attempts
            .save();               // Commit it to the server
    },
    getPartnerDirectUrl: function(fileId, invoiceId, invoiceSecret) {
        this.unblock();
        check(invoiceId, String);
        check(invoiceSecret, String);

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice || invoice.secret !== invoiceSecret) {
            throw new Meteor.Error(404, 'Not allowed');
        }

        let fileObj = Files.findOne({
            _id: fileId
        });

        if (fileObj) {
            if (fileObj.copies && fileObj.copies['vatfree']) {
                return fileObj.getDirectUrl('vatfree');
            } else {
                throw new Meteor.Error(500, 'File is not uploaded yet');
            }
        } else {
            return false;
        }
    }
});

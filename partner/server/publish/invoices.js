Meteor.publish('my-invoices', function(id, secret) {
    let invoice = Invoices.findOne({
        _id: id,
        secret: secret
    });
    if (!invoice) {
        throw new Meteor.Error(404, "Invoice not found");
    }

    let selector = {
        deleted: {
            $ne: true
        }
    };
    if (invoice.shopId) {
        selector.shopId = invoice.shopId;
    } else {
        selector.companyId = invoice.companyId;
    }

    return Invoices.find(selector);
});

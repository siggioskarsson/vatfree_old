Meteor.publish('partner-countries', function(id, secret) {
    let invoice = Invoices.findOne({
        _id: id,
        secret: secret
    });
    if (!invoice) {
        throw new Meteor.Error(404, "Invoice not found");
    }

    return Countries.find();
});

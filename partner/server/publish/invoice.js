Meteor.publishComposite('my-invoice', function(id, secret) {
    return {
        find: function () {
            return Invoices.find({
                _id: id,
                secret: secret
            });
        },
        children: [
            {
                find: function (invoice) {
                    if (invoice.deleted) {
                        return false;
                    } else {
                        return Receipts.find({
                            _id: {
                                $in: invoice.receiptIds || []
                            }
                        });
                    }
                },
                children: [
                    {
                        find: function (receipt) {
                            return Shops.find({
                                _id: receipt.shopId
                            });
                        }
                    },
                    {
                        find: function (receipt) {
                            return Countries.find({
                                _id: receipt.countryId
                            });
                        }
                    },
                    {
                        find: function (receipt) {
                            return Files.find({
                                itemId: receipt._id,
                                target: 'receipts'
                            });
                        }
                    }
                ]
            },
            {
                find: function (invoice) {
                    return Shops.find({
                        _id: invoice.shopId
                    });
                },
                children: [
                    {
                        find: function (shop) {
                            return Files.find({
                                itemId: shop._id,
                                target: 'shops',
                                category: 'logo'
                            });
                        }
                    }
                ]
            },
            {
                find: function (invoice) {
                    return Companies.find({
                        _id: invoice.companyId
                    });
                },
                children: [
                    {
                        find: function (company) {
                            return Files.find({
                                itemId: company._id,
                                target: 'companies',
                                category: 'logo'
                            });
                        }
                    }
                ]
            },
            {
                find: function (invoice) {
                    return Files.find({
                        itemId: invoice._id,
                        target: 'invoices'
                    });
                }
            }
        ]
    };
});

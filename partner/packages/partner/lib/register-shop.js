export const validateNewShopData = function(shopData) {
    let requiredFields = [
        'name',
        'addressFirst',
        'postCode',
        'city',
        'vatNumber',
        'chamberOfCommerce',
        'contactName',
        'contactFunction'
    ];
    let errors = [];
    _.each(requiredFields, (fieldName) => {
        if (!_.has(shopData, fieldName)) {
            errors.push("Missing field " + fieldName);
        }
    });

    return errors;
};

import { validateNewShopData } from 'meteor/partner/lib/register-shop';
import moment from 'moment';

let getMapReduceQuery = function (invoice) {
    let mapReduceQuery = {
        status: {
            $nin: ['rejected', 'deleted']
        }
    };

    let entity;
    if (invoice.companyId) {
        entity = Companies.findOne({_id: invoice.companyId});
        mapReduceQuery.companyId = invoice.companyId;
    } else if (invoice.shopId) {
        entity = Shops.findOne({_id: invoice.shopId});
        mapReduceQuery.shopId = invoice.shopId;
    }

    if (entity && entity.partnerDashboardFrom) {
        mapReduceQuery.createdAt = {
            $gte: moment(entity.partnerDashboardFrom).startOf('day').toDate(),
        };
    }

    return mapReduceQuery;
};

let aggregateReceiptData = function (mapReduceQuery, groupById) {
    const isPaid = {
        $in: [
            '$status',
            ['paidByShop', 'requestPayout', 'paid', 'userPendingForPayment', 'depreciated']
        ]
    };
    const isPipeline = {
        $in: [
            '$status',
            [
                'waitingForCustomsStamp',
                'userPending',
                'waitingForOriginal',
                'visualValidationPending',
                'waitingForDoubleCheck',
                'shopPending',
                'readyForInvoicing',
            ]
        ]
    };

    const pipeline = [
        {
            $match: mapReduceQuery
        }, {
            $group: {
                _id: groupById,
                amount: {$sum: '$euroAmounts.amount'},
                vat: {$sum: '$euroAmounts.totalVat'},
                vatP: {
                    $sum: {
                        $cond: [
                            isPaid,
                            '$euroAmounts.totalVat',
                            0
                        ]
                    }
                },
                vatPipeline: {
                    $sum: {
                        $cond: [
                            isPipeline,
                            '$euroAmounts.totalVat',
                            0
                        ]
                    }
                },
                vatPipelineN: {
                    $sum: {
                        $cond: [
                            isPipeline,
                            1,
                            0
                        ]
                    }
                },
                fee: {$sum: {$add: ['$euroAmounts.serviceFee', '$euroAmounts.deferredServiceFee']}},
                rFee: {
                    $sum: {
                        $cond: [
                            isPaid,
                            '$euroAmounts.serviceFee',
                            0
                        ]
                    }
                },
                refund: {$sum: '$euroAmounts.refund'},
                refundP: {
                    $sum: {
                        $cond: [
                            isPaid,
                            '$euroAmounts.refund',
                            0
                        ]
                    }
                },
                nr: {$sum: 1},
                natoNr: {
                    $sum: {
                        $cond: [
                            {
                                $eq: [ "$receiptTypeId", 'nY7SoFKGFbHXBeHT6' ]
                            },
                            1,
                            0
                        ]
                    }
                },
                cargoNr: {
                    $sum: {
                        $cond: [
                            {
                                $eq: [ "$receiptTypeId", 'KuzFRkKwRbvR4275n' ]
                            },
                            1,
                            0
                        ]
                    }
                },
                nrP: {
                    $sum: {
                        $cond: [
                            isPaid,
                            1,
                            0
                        ]
                    }
                }
            }
        }
    ];
    let result = Receipts.rawCollection().aggregate(pipeline);
    return result.toArray();
};

let checkInvoiceValid = function (invoiceId, secret) {
    check(invoiceId, String);
    check(secret, String);
    const invoice = Invoices.findOne({
        _id: invoiceId,
        secret: secret
    });
    if (!invoice) {
        throw new Meteor.Error(404, 'Invoice not found');
    }
    return invoice;
};

Meteor.methods({
    'partner-dashboard-get-top10-countries' (invoiceId, secret) {
        const invoice = checkInvoiceValid(invoiceId, secret);

        let groupById = '$travellerCountryId';
        let mapReduceQuery = getMapReduceQuery(invoice);
        console.log(mapReduceQuery);
        return aggregateReceiptData(mapReduceQuery, groupById);
    },
    'partner-dashboard-get-per-month' (invoiceId, secret) {
        const invoice = checkInvoiceValid(invoiceId, secret);

        let groupById = {
            $concat: [
                {$toUpper: '$createdYear'},
                'M',
                {$toUpper: '$createdMonth'}
            ]
        };
        let mapReduceQuery = getMapReduceQuery(invoice);
        return aggregateReceiptData(mapReduceQuery, groupById);
    },
    'register-new-shop'(shopData) {
        // check whether all data is present
        let errors = validateNewShopData(shopData);
        if (errors.length > 0) {
            throw new Meteor.Error(500, "Invalid data sent");
        }

        // clean data
        const shopId = shopData._id || Random.id();
        shopData.shopId = shopId;
        const insertShopData = {
            _id: shopId,
            name: shopData.name,
            geo: JSON.parse(shopData.geo),
            addressFirst: shopData.addressFirst,
            postCode: shopData.postCode,
            city: shopData.city,
            countryId: shopData.countryId,
            currencyId: shopData.currencyId,
            language: shopData.language,
            subscription: shopData.subscription || "none",
            email: shopData.email,
            tel: shopData.tel,
            website: shopData.website,
            vatNumber: shopData.vatNumber,
            chamberOfCommerce: shopData.chamberOfCommerce,
            partnershipStatus: 'partner',
            billingVat: shopData.vatNumber,
            billingName: shopData.debtCollectionName,
            billingEmail: shopData.billingEmail,
            billingEntity: true,
        };

        if (shopData.debtCollectionAccount) {
            insertShopData.debtCollectionAccount = shopData.debtCollectionAccount;
            insertShopData.debtCollectionMandate = shopId + '-001';
            insertShopData.debtCollectionDate = new Date();
        }

        if (shopData._id) {
            // existing shop
            Shops.update({
                _id: shopData._id
            },{
                $set: insertShopData
            });
        } else {
            // insert shop
            Shops.insert(insertShopData);
        }

        // create contact
        const profile = {
            name: shopData.contactName,
            jobTitle: shopData.contactFunction,
            email: shopData.contactEmail,
            companyName: shopData.companyName,
            language: shopData.nl,
            shopId: shopId
        };
        const privateData = {};
        const contactId =  Contacts.insert({
            profile: profile,
            private: privateData,
            roles: {
                __global_roles__: ['contact']
            }
        });

        // link contact to shop and set as billing contact
        Shops.update({
            _id: shopId
        }, {
            $addToSet: {
                billingContacts: contactId
            }
        });

        let job = new Job(shopJobs, 'shopRegistration',
            {
                shopId: shopId,
                contactId: contactId,
                signature: shopData.signature
            }
        );
        job.priority('normal')
            .delay(30*1000)
            .retry({
                retries: 1,
                wait: 5 * 60 * 1000
            })
            .save();

        ActivityLogs.insert({
            type: 'task',
            userId: _.deepPickValue(Meteor.settings, 'activityLogs.users.shopRegistration') || null,
            subject: 'New partner shop registered',
            status: 'new',
            shopId: shopId
        });

        return shopId;
    }
});

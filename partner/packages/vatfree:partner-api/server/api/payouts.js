Api.addRoute('payouts', {
    authRequired: true
}, {
    get: {
        action: function () {
            if (!Roles.userIsInRole(this.userId, 'partner', Roles.GLOBAL_GROUP)) {
                return statusCodes.noAccess;
            }

            const currencies = {};
            Currencies.find({}, {
                fields: {
                    _id: 1,
                    code: 1
                }
            }).forEach((currency) => {
                currencies[currency._id] = currency.code;
            });

            const payouts = [];
            Payouts.find({
                partnerId: this.userId
            }, {
                fields: {
                    payoutNr: 1,
                    status: 1,
                    amount: 1,
                    currencyId: 1,
                    createdAt: 1,
                    updatedAt: 1,
                    paidAt: 1
                }
            }).forEach((payout) => {
                payout.currency = currencies[payout.currencyId];
                delete payout.currencyId;
                payouts.push(payout);
            });

            return Vatfree.api.returnData(payouts);
        }
    }
});

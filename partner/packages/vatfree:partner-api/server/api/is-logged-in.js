Api.addRoute('is-logged-in', {
    authRequired: false
}, {
    error: function() {
        return Vatfree.api.returnData({
            loggedIn: false
        });
    },
    get: {
        action: function () {

            console.log(this.bodyParams);

            return Vatfree.api.returnData({
                loggedIn: !!Roles.userIsInRole(this.userId, 'partner', Roles.GLOBAL_GROUP)
            });
        }
    },
    post: {
        action: function () {

            console.log(this.bodyParams);

            return Vatfree.api.returnData({
                loggedIn: !!Roles.userIsInRole(this.userId, 'partner', Roles.GLOBAL_GROUP)
            });
        }
    }
});

Api.addRoute('payout/:payoutId', {
    authRequired: true
}, {
    get: {
        action: function () {
            if (!Roles.userIsInRole(this.userId, 'partner', Roles.GLOBAL_GROUP)) {
                return statusCodes.noAccess;
            }

            check(this.urlParams.payoutId, String);
            const payoutId = this.urlParams.payoutId;
            const payout = Payouts.findOne({
                _id: payoutId,
                partnerId: this.userId
            }, {
                fields: {
                    _id: 1,
                    payoutNr: 1,
                    status: 1,
                    amount: 1,
                    costs: 1,
                    currencyId: 1,
                    receiptIds: 1,
                    paymentMethod: 1,
                    paymentNr: 1,
                    paymentAccountName: 1,
                    createdAt: 1,
                    updatedAt: 1,
                    paidAt: 1,
                    payoutBatchId: 1,
                    statusDates: 1
                }
            });
            if (!payout) {
                return statusCodes.noData;
            }

            const currency = Currencies.findOne({
                _id: payout.currencyId
            }, {
                fields: {
                    code: 1
                }
            });
            if (!currency) {
                return statusCodes.processingError;
            }

            payout.currency = currency.code;
            delete payout.currencyId;

            const receiptIds = [];
            _.each(payout.receiptIds, (receiptId) => {
                if (receiptId.indexOf('partner_') === -1) {
                    receiptIds.push(receiptId)
                }
            });
            if (receiptIds.length > 0) {
                payout.receiptIds = receiptIds;
            } else {
                delete payout.receiptIds;
            }

            return Vatfree.api.returnData(payout);
        }
    }
});

Api.addRoute('payout', {
    authRequired: true
}, {
    post: {
        action: function () {
            if (!Roles.userIsInRole(this.userId, 'partner', Roles.GLOBAL_GROUP)) {
                return statusCodes.noAccess;
            }

            check(this.bodyParams.amount, Number);
            check(this.bodyParams.currency, String);
            check(this.bodyParams.paymentMethod, String);
            check(this.bodyParams.paymentNr, String);
            check(this.bodyParams.paymentAccountName, String);

            const currency = Currencies.findOne({code: this.bodyParams.currency});
            if (!currency) {
                return Vatfree.api.returnError("Could not find currency");
            }

            if (!_.contains(Vatfree.payments.methods, this.bodyParams.paymentMethod)) {
                return Vatfree.api.returnError("Invalid payout method");
            }

            const paymentNr = this.bodyParams.paymentNr.replace(/\s/g, '');
            if (!Vatfree.payouts.validatePayout(this.bodyParams.paymentMethod, paymentNr)) {
                return Vatfree.api.returnError("Invalid payout " + this.bodyParams.paymentNr);
            }

            const payout = _.clone(this.bodyParams);
            payout.paymentNr = paymentNr; // without white space
            payout.currencyId = currency._id;
            payout.partnerId = this.userId;
            payout.travellerId = this.userId;
            payout.status = 'new';

            if (!_.has(payout, 'receiptIds')) {
                payout.receiptIds = [
                    'partner_' + Random.id()
                ];
            }

            let newPayout;
            try {
                const payoutId = Payouts.insert(payout);

                // get the Payout, with the newly created payoutNr
                newPayout = Payouts.findOne({_id: payoutId});
                if (!newPayout) {
                    return statusCodes.processingError;
                }
            } catch(e) {
                return Vatfree.api.returnError(e.message);
            }

            return Vatfree.api.returnData({
                _id: newPayout._id,
                payoutNr: newPayout.payoutNr
            });
        }
    }
});

Api.addRoute('validPayout', {
    authRequired: true
}, {
    post: {
        action: function () {
            if (!Roles.userIsInRole(this.userId, 'partner', Roles.GLOBAL_GROUP)) {
                return statusCodes.noAccess;
            }

            check(this.bodyParams.paymentMethod, String);
            check(this.bodyParams.paymentNr, String);

            const isValid = Vatfree.payouts.validatePayout(this.bodyParams.paymentMethod, this.bodyParams.paymentNr);
            return Vatfree.api.returnData({
                valid: isValid ? 'YES' : 'NO'
            });
        }
    }
});

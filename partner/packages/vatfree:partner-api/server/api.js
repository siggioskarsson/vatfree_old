Vatfree.api = {};

Vatfree.api.returnData = function(data) {
    let returnValue = _.clone(statusCodes.ok);
    returnValue.body.data = data;

    return returnValue;
};

Vatfree.api.returnError = function(errorMessage) {
    let returnValue = _.clone(statusCodes.processingError);
    returnValue.body.message = errorMessage;

    return returnValue;
};

// curl -X POST http://local.vatfree.com:6021/api/v1/login -d "email=siggi.oskarsson@gmail.com&password=test123"
// curl -X POST -H "X-Auth-Token: P_FEqbTaillNuld28LmwzZFNIUA571szCYzdephVPxj" -H "X-User-Id: d6csdcGkKEQzMAMQP" http://localhost:6001/api/v1/logout
/*
{
  "status": "success",
  "data": {
    "authToken": "0TrL2FL_YKWnuvVF69moOd4KDo9NVT6m-V04HQCg0eZ",
    "userId": "d6csdcGkKEQzMAMQP"
  }
}

curl -H "X-Auth-Token: 0TrL2FL_YKWnuvVF69moOd4KDo9NVT6m-V04HQCg0eZ" -H "X-User-Id: d6csdcGkKEQzMAMQP" http://localhost:6001/api/v1/receipts
curl -H "X-Auth-Token: 0TrL2FL_YKWnuvVF69moOd4KDo9NVT6m-V04HQCg0eZ" -H "X-User-Id: d6csdcGkKEQzMAMQP" http://localhost:6001/api/v1/is-logged-in
 */

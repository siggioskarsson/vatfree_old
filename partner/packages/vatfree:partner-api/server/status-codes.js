/* global statusCodes: true */

statusCodes = {};

statusCodes.ok = {
    statusCode: 200,
    body: {
        status: "success"
    }
};

statusCodes.processingError = {
    statusCode: 500,
    body: {
        status: "error",
        message: "Something went wrong in the processing"
    }
};

statusCodes.incorrectInput = {
    statusCode: 500,
    body: {
        status: "error",
        message: "Incorrect input"
    }
};

statusCodes.noAccess = {
    statusCode: 403,
    body: {
        status: "error",
        message: "No access"
    }
};

statusCodes.noData = {
    statusCode: 404,
    body: {
        status: "error",
        message: "Could not find data"
    }
};

statusCodes.notImplemented = {
    statusCode: 500,
    body: {
        status: "error",
        message: "This function has not yet been implemented"
    }
};

#!/usr/bin/env bash
meteor npm install --no-optional
MONGO_URL=mongodb://localhost:6002/meteor ROOT_URL=http://local.vatfree.com:6021/ meteor --port 6021 --settings settings.json

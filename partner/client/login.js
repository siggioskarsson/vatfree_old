Template.login.onCreated(function() {
    this.registering = new ReactiveVar();
    this.isLoggingIn = new ReactiveVar();
});

Template.login.helpers({
    isRegistering() {
        return Template.instance().registering.get();
    },
    isLoggingIn() {
        return Template.instance().isLoggingIn.get();
    }
});

Template.login.events({
    'click .register-account'(e, template) {
        e.preventDefault();
        template.registering.set(true);
    },
    'click .cancel-register-account'(e, template) {
        e.preventDefault();
        template.registering.set();
    },
    'click .google-login'(e) {
        e.preventDefault();
        Meteor.loginWithGoogle((err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            }
        });
    },
    'click .facebook-login'(e) {
        e.preventDefault();
        Meteor.loginWithFacebook((err, result) => {
            if (err) {
                console.log(err);
                toastr.error(err.reason || err.message);
            }
        });
    },
    'submit form[name="register-form"]'(e, template) {
        e.preventDefault();
        let email = $('input[name="register_email"]').val();
        Meteor.call('registerWithMagicLink', email, (err, result) => {
            if (err) {
                alert(err.reason);
            } else {
                alert('An email has been sent to you to confirm your email address. Please check your email');
                template.registering.set();
            }
        });

        /*
        let password = $('input[name="password"]');
        Meteor.loginWithPassword(email, password, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            }
        });
        */
    },
    'submit form[name="password-login"]'(e, template) {
        e.preventDefault();
        let email = $('input[name="email"]').val();
        template.isLoggingIn.set(true);
        Meteor.call('loginWithMagicLink', email, (err, result) => {
            if (err) {
                alert(err.reason);
            } else {
                alert('An email has been sent to you. Please click on the link in the email to login');
                $('input[name="email"]').val('');
            }
            template.isLoggingIn.set();
        });

        /*
        let password = $('input[name="password"]');
        Meteor.loginWithPassword(email, password, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            }
        });
        */
    }
});

import Chart from 'chart.js';

const getColors = function(index) {
    let colors = [
        {
            backgroundColor: "rgba(26,179,148,0.5)",
            borderColor: "rgba(26,179,148,0.7)",
            pointBackgroundColor: "rgba(26,179,148,1)",
            pointBorderColor: "#fff",
        },
        {
            backgroundColor: "rgba(220,220,220,0.5)",
            borderColor: "rgba(220,220,220,1)",
            pointBackgroundColor: "rgba(220,220,220,1)",
            pointBorderColor: "#fff",
        }
    ];

    return colors[index];
};

let getLineOptions = function (useY2Axis) {
    const lineOptions = {
        responsive: true,
        scales: {
            yAxes: [{
                id: 'y1Axis',
                type: 'linear',
                position: 'left',
                ticks: {
                    min: 0
                }
            }]
        }
    };
    if (useY2Axis) {
        lineOptions.scales.yAxes.push({
            id: 'y2Axis',
            type: 'linear',
            position: 'right',
            ticks: {
                min: 0
            },
            gridLines: {
                drawOnChartArea: false
            }
        });
    }
    return lineOptions;
};

Template.dashboard_chart.onRendered(function() {
    this.autorun(() => {
        let templateData = Template.currentData();
        let data = templateData.data;
        Tracker.afterFlush(() => {
            let useY2Axis = false;
            let dataSets = [];
            _.each(data.series, (series, index) => {
                let colors = getColors(index);
                let dataset = {
                    label: series.label,
                    yAxisID: series.axis,
                    data: series.data,
                    ...colors
                };
                if (dataset.yAxisID === 'y2Axis') {
                    dataset.type = 'line';
                    useY2Axis = true;
                }

                dataSets.push(dataset);
            });

            const lineData = {
                labels: data.labels,
                datasets: dataSets
            };
            const lineOptions = getLineOptions(useY2Axis);

            if (this.$("#lineChart").length > 0) {
                const ctx = this.$("#lineChart")[0].getContext("2d");
                new Chart(ctx, {type: 'bar', data: lineData, options: lineOptions});
            }
        });
    });
});

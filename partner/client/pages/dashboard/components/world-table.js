Template.dashboard_world_table.onCreated(function() {
    this.countryData = new ReactiveVar();
    this.loading = new ReactiveVar();
});

Template.dashboard_world_table.onRendered(function() {
    let template = this;
    this.loading.set(true);
    Meteor.call('partner-dashboard-get-top10-countries', FlowRouter.getParam('invoiceId'), FlowRouter.getParam('secret'), (err, data) => {
        if (err) {
            toastr.error(err.message);
        } else {
            template.countryData.set(data);
        }
        this.loading.set(false);
    });

    this.autorun(() => {
        let countryData = this.countryData.get();
        if (countryData) {
            const mapData = {};
            _.each(countryData, (data) => {
                let country = Countries.findOne({_id: data._id});
                if (country) {
                    mapData[country.code] = data.amount;
                }
            });

            Tracker.afterFlush(() => {
                const mapOptions = {
                    map: 'world_mill_en',
                    backgroundColor: "transparent",
                    zoomOnScroll: false,
                    regionStyle: {
                        initial: {
                            fill: '#e4e4e4',
                            "fill-opacity": 0.9,
                            stroke: 'none',
                            "stroke-width": 0,
                            "stroke-opacity": 0
                        }
                    },

                    series: {
                        regions: [{
                            values: mapData,
                            scale: ["#1ab394", "#22d6b1"],
                            normalizeFunction: 'polynomial'
                        }]
                    },
                    onRegionTipShow: function(e, el, code){
                        el.html(el.html() + ' - ' + Vatfree.numbers.formatCurrency(mapData[code], 0) + '');
                    }
                };

                template.$('#world-map').vectorMap(mapOptions);
            });
        }
    });
});

Template.dashboard_world_table.helpers({
    isLoading() {
        return Template.instance().loading.get();
    },
    getCountryData() {
        let tableData = [];
        let countryData = Template.instance().countryData.get();
        if (countryData) {
            countryData.sort(function(a, b) {
                return a.amount > b.amount ? -1 : 1;
            });

            let n = 1;
            _.each(countryData, (data) => {
                let country = Countries.findOne({_id: data._id});
                if (country && n <= 10) {
                    tableData.push({
                        n: n,
                        name: country.name,
                        code: country.code,
                        codeLC: country.code.toLowerCase(),
                        avg: Math.round(data.amount / data.nr),
                        ...data
                    });
                    n++;
                }
            });
        }

        return tableData;
    },
    numberOfCountries() {
        let countryData = Template.instance().countryData.get();
        return _.size(countryData);
    }
});

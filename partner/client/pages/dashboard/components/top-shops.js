Template.view_dashboard_top_shops_list.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('my-top-shops', FlowRouter.getParam('invoiceId'), FlowRouter.getParam('secret'));
    });
});

Template.view_dashboard_top_shops_list.onCreated(function() {
    this.sort = new ReactiveVar({
        createdAt: -1
    });
});

Template.view_dashboard_top_shops_list.helpers({
    getShops() {
        return Shops.find({},{
            sort: {
                'stats.numberOfRequests': -1
            },
            limit: 5
        });
    },
    getCurrencyId() {
        const invoice = Invoices.findOne({_id: FlowRouter.getParam('invoiceId')}) || {};
        return invoice.currencyId;
    },
    getCountry() {
        return Countries.findOne({_id: this.countryId});
    },
    getNr(index) {
        return index + 1;
    }
});

Template.view_dashboard_top_shops_list.events({
});

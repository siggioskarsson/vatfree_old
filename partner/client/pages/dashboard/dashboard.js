import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);

Template.view_dashboard.onCreated(function() {
    this.monthData = new ReactiveVar();
});

Template.view_dashboard.onRendered(function() {
    let invoiceId = FlowRouter.getParam('invoiceId');
    let secret = FlowRouter.getParam('secret');

    this.subscribe('partner-countries', invoiceId, secret);

    Meteor.call('partner-dashboard-get-per-month', invoiceId, secret, (err, data) => {
        if (err) {
            toastr.error(err.message);
        } else {
            this.monthData.set(data);
        }
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('my-invoices', FlowRouter.getParam('invoiceId'), FlowRouter.getParam('secret'));
    });
});

let getTotal = function (attribute) {
    let monthData = Template.instance().monthData.get();
    let total = 0;
    _.each(monthData, (data) => {
        total += Number(data[attribute]);
    });
    return total;
};

let getLastMonths = function (lastMonths) {
    let months = [];
    moment.locale(TAPi18n.getLanguage().toLowerCase());
    let mRange = moment.range(moment().startOf('month').subtract(lastMonths, 'months'), moment().subtract(1, 'month'));
    for (let mMonth of mRange.by('months')) {
        months.push({
            id: mMonth.format('YYYY[M]') + Number(mMonth.format('MM')),
            label: mMonth.format('MMMM YYYY'),
            labelShort: mMonth.format('MMM'),
        });
    }

    return months;
};

const getEntity = function (invoice) {
    let entity = {};
    if (invoice.shopId) {
        entity = Shops.findOne({_id: invoice.shopId});
    } else if (invoice.companyId) {
        entity = Companies.findOne({_id: invoice.companyId});
    }
    return entity;
};

Template.view_dashboard.helpers({
    isPartner() {
        const invoice = Invoices.findOne({_id: FlowRouter.getParam('invoiceId')}) || {};
        let entity = getEntity(invoice);

        return entity.partnershipStatus === 'partner';
    },

    isCompanyInvoice() {
        const invoice = Invoices.findOne({_id: FlowRouter.getParam('invoiceId')}) || {};
        return invoice.companyId && !invoice.shopId;
    },

    getAllTimeText() {
        const invoice = Invoices.findOne({_id: FlowRouter.getParam('invoiceId')}) || {};
        let entity = getEntity(invoice);
        if (entity && entity.partnerDashboardFrom) {
            return Vatfree.translate("Since __date_from__", {date_from: moment(entity.partnerDashboardFrom).format(Vatfree.translate('_date_format'))});
        }

        return "";
    },

    getPotentialTaxRevenue() {
        return Vatfree.numbers.formatCurrency(9.2 * getTotal('amount'), 0);
    },

    getTaxRevenue() {
        return Vatfree.numbers.formatCurrency(getTotal('amount'), 0);
    },

    getNumberOfReceipts() {
        return getTotal('nr');
    },
    getNumberOfReceiptsPerType() {
        const nr = getTotal('nr');
        const natoNr = getTotal('natoNr');
        const cargoNr = getTotal('cargoNr');

        if (natoNr || cargoNr) {
            const regular = nr - natoNr - cargoNr;
            let returnText =  `Traveller ${regular}`;
            if (natoNr) {
                returnText +=  `, NATO ${natoNr}`;
            }
            if (cargoNr) {
                returnText +=  `, Cargo ${cargoNr}`;
            }

            return returnText;
        }

        return '';
    },
    getNumberOfRecurringReceipts() {
        return '23.8%';
    },
    getReceiptDoughnutData() {
        return {
            labels: [ "Recurring", "New" ],
            datasets: [{
                data: [ 23.8, 76.2 ],
                backgroundColor: [ "#23c6c8", "#94e3d4" ],
                borderWidth: 0,
                hoverBorderWidth: 0
            }]
        };
    },

    getTaxRefund() {
        return Vatfree.numbers.formatCurrency(getTotal('vat'), 0);
    },
    getTaxRefundPercent() {
        let nr = getTotal('nr');
        let nrPaid = getTotal('nrP');

        return Math.round(100 * (nrPaid / nr)).toString() + '%';
    },
    getTaxRefundNumber() {
        let nr = getTotal('nr');
        let nrPaid = getTotal('nrP');

        return nr - nrPaid;
    },
    getRefundDoughnutData() {
        let nr = getTotal('nr');
        let nrPaid = getTotal('nrP');

        let refunded = nrPaid;
        let open = nr - nrPaid;

        return {
            labels: [ TAPi18n.__("Refunded"), TAPi18n.__("Open") ],
            datasets: [{
                data: [refunded,open],
                backgroundColor: ["#23c6c8","#dd0000"],
                borderWidth: 0,
                hoverBorderWidth: 0
            }]
        };
    },

    getOpenPayouts() {
        let refund = getTotal('refund');
        let refundPaid = getTotal('refundP');

        return Vatfree.numbers.formatCurrency(refund - refundPaid, 0);
    },

    getOpenPipeline() {
        let vatPipeline = getTotal('vatPipeline');
        return Vatfree.numbers.formatCurrency(vatPipeline, 0);
    },

    getOpenPipelineNumber() {
        let vatPipelineN = getTotal('vatPipelineN');
        return vatPipelineN;
    },

    getChartData() {
        let months = getLastMonths(12);
        let monthData = Template.instance().monthData.get();
        let labels = [];
        let receiptData = [];
        let refundData = [];
        _.each(months, (month) => {
            labels.push(month.labelShort);

            let receipt = _.find(monthData, (monthD) => { if (monthD._id === month.id) { return true; } } );
            receiptData.push(receipt ? receipt.nr : null);
            refundData.push(receipt ? Vatfree.numbers.formatAmount(receipt.vat) : null);
        });

        return {
            labels: labels,
            series: [
                {
                    label: TAPi18n.__("# receipts"),
                    axis: 'y1Axis',
                    data: receiptData
                },
                {
                    label: TAPi18n.__("VAT amount"),
                    axis: 'y2Axis',
                    data: refundData
                }
            ]
        };
    },

    getLastMonths(nrMonths, attribute) {
        let monthData = Template.instance().monthData.get();
        let months = getLastMonths(nrMonths);

        let total = 0;
        _.each(months, (month) => {
            let receipt = _.find(monthData, (monthD) => { if (monthD._id === month.id) { return true; } } );
            if (receipt) {
                total += Number(receipt[attribute]);
            }
        });

        return total;
    },

    getAvgLastMonths(nrMonths, attribute) {
        let monthData = Template.instance().monthData.get();
        let months = getLastMonths(nrMonths);

        let total = 0;
        let nr = 0;
        _.each(months, (month) => {
            let receipt = _.find(monthData, (monthD) => { if (monthD._id === month.id) { return true; } } );
            if (receipt) {
                total += Number(receipt[attribute]);
                nr += Number(receipt['nr'])
            }
        });

        return total / nr;

    },

    getInvoices() {
        return Invoices.find({
            status: "sentToRetailer"
        });
    },

    getShopRegisterWebText() {
        const invoice = Invoices.findOne({_id: FlowRouter.getParam('invoiceId')}) || {};
        let entity = {};
        if (invoice.shopId) {
            entity = Shops.findOne({_id: invoice.shopId});
        } else if (invoice.companyId) {
            entity = Companies.findOne({_id: invoice.companyId});
        }

        return (this.toString()).replace('{{shopId}}', entity._id);
    }
});

Template.view_dashboard.events({
});

import { selectMapLocationCallback } from '../../shop-helpers';

Template.view_register_shop_info.helpers({
    getValue(attribute) {
        return this.shopData[attribute];
    },
    getGeo() {
        let geo = this.shopData.geo;
        if (geo) {
            return JSON.parse(geo);
        }
    },
    selectMapLocationCallback() {
        return selectMapLocationCallback();
    },
    isCountrySelected(code) {
        let shopData = Template.instance().data.shopData;
        let country = Countries.findOne({_id: shopData.countryId });
        return (country && country.code ? country.code === code : code === 'NL');
    },
    isCurrencySelected(code) {
        let shopData = Template.instance().data.shopData;
        let currency = Currencies.findOne({_id: shopData.currencyId });
        return (currency && currency.code ? currency.code === code : code === 'EUR');
    },
    isLanguageSelected(code) {
        let shopData = Template.instance().data.shopData;
        return (shopData.language ? shopData.language === code : code === 'nl');
    }
});

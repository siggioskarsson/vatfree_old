import { signaturePad } from './sign';
import { validateNewShopData } from 'meteor/partner/lib/register-shop';

Template.view_register.onCreated(function() {
    this.shopData = new ReactiveVar({});
    this.isLoading = new ReactiveVar(false);
    this.isRegistering = new ReactiveVar();
    this.errors = new ReactiveVar();

    this.steps = ['info', 'subscription',   'sign', 'confirm'];
    this.currentStep = new ReactiveVar('info');

    let language = FlowRouter.getQueryParam('language');
    if (language && _.contains(['nl', 'en'], language)) {
        TAPi18n.setLanguage(language);
    }
});

Template.view_register.onRendered(function() {
    this.isLoading.set(true);
    this.autorun(() => {
        this.subscribe('countries');
    });

    const shopId = FlowRouter.getParam('shopId');
    if (shopId) {
        Meteor.call('get-shop', shopId, (err, shop) => {
            if (err) {
                console.error(err);
            } else if (shop) {
                if (shop.geo) {
                    shop.geo = JSON.stringify(shop.geo);
                }
                this.shopData.set(shop);
            }
            this.isLoading.set(false);
        });
        Meteor.call('get-company', shopId, (err, shop) => {
            if (err) {
                console.error(err);
            } else if (shop) {
                if (shop.geo) {
                    shop.geo = JSON.stringify(shop.geo);
                }
                this.shopData.set(shop);
            }
            this.isLoading.set(false);
        });
    } else {
        this.isLoading.set(false);
    }
});

Template.view_register.helpers({
    isActiveLanguage(language) {
        return TAPi18n.getLanguage() === language ? 'active' : false;
    },
    isLoading() {
        return Template.instance().isLoading.get();
    },
    getShopData() {
        return Template.instance().shopData.get() || {};
    },
    getStep() {
        return Template.instance().currentStep.get();
    },
    isStep(step) {
        return Template.instance().currentStep.get() === step;
    },
    isRegistering() {
        return Template.instance().isRegistering.get();
    },
    getErrors() {
        return Template.instance().errors.get();
    }
});

Template.view_register.events({
    'click .language-selector .language-option'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).find('.flag-icon').hasClass('flag-icon-nl')) {
            TAPi18n.setLanguage('nl');
        } else {
            TAPi18n.setLanguage('en');
        }
    },
    'click .cancel-add-shop'(e, template) {
        e.preventDefault();
        template.shopData.set({});
        template.currentStep.set(template.steps[0]);
        FlowRouter.go('/');
    },
    'click .add-shop-next-step'(e, template) {
        e.preventDefault();
        saveShopData(template);
        const currentStep = template.steps.indexOf(template.currentStep.get());
        validateStep(currentStep);

        const nextStep = template.steps[currentStep + 1];
        if (nextStep) {
            template.currentStep.set(nextStep);
        }
        $('html').scrollTop(0);
    },
    'click .add-shop-previous-step'(e, template) {
        e.preventDefault();
        saveShopData(template);
        const currentStep = template.steps.indexOf(template.currentStep.get());
        validateStep(currentStep);

        const previousStep = template.steps[currentStep - 1];
        if (previousStep) {
            template.currentStep.set(previousStep);
        }
        $('html').scrollTop(0);
    },
    'submit form[name="add-shop-form"]'(e, template) {
        e.preventDefault();
        saveShopData(template);
        const currentStep = template.steps.indexOf(template.currentStep.get());
        validateStep(currentStep);

        const nextStep = template.steps[currentStep + 1];
        if (nextStep) {
            template.currentStep.set(nextStep);
        }
        $('html').scrollTop(0);
    },
    'click .register-new-shop'(e, template) {
        e.preventDefault();
        const shopData = template.shopData.get();

        let errors = validateNewShopData(shopData);
        if (errors.length > 0) {
            template.errors.set(errors);
            return false;
        }

        template.isRegistering.set(true);
        Meteor.call('register-new-shop', shopData, (err, result) => {
            if (err) {
                alert(err.reason || err.message);
            } else {
                template.currentStep.set('registered');
            }
            template.isRegistering.set(false);
        });
    },
    'click .close-window'(e, template) {
        e.preventDefault();
        document.location.href = "https://www.vatfree.com/";
    }
});

const saveShopData = function(template) {
    let formData = Vatfree.templateHelpers.getFormData(template);
    let shopData = template.shopData.get();
    _.each(formData, (dataValue, dataKey) => {
        shopData[dataKey] = dataValue;
    });

    if ($('#signatureCanvas').length > 0) {
        shopData.signature = signaturePad.toDataURL();
    }

    template.shopData.set(shopData);
};

const validateStep = function(currentStep) {
    return true;
};

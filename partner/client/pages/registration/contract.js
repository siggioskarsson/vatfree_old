/* global TAPi18n:true */

export const contractHelpers = {
    getContractTextTemplate() {
        return "view_register_contract_text_" + TAPi18n.getLanguage();
    },
    getLocalLawTemplate() {
        return "view_register_shop_law_" + TAPi18n.getLanguage();
    },
    getTermsTemplate() {
        return "vatfree_terms_" + TAPi18n.getLanguage();
    }
};

Template.view_register_contract.helpers(contractHelpers);
Template.view_register_contract.helpers({
    getValue(attribute) {
        return this.shopData[attribute];
    },
});

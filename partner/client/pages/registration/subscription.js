Template.view_register_subscription.onCreated(function() {
    let setBillingData = {
        billingName: 'name',
        billingEmail: 'email',
        billingAddressFirst: 'addressFirst',
        billingPostCode: 'postCode',
        billingCity: 'city',
        billingCountryId: 'countryId',
    };

    _.each(setBillingData, (shopData, billingData) => {
        if (!_.has(this.data.shopData, billingData)) {
            this.data.shopData[billingData] = this.data.shopData[shopData];
        }
    });
});

Template.view_register_subscription.helpers({
    getValue(attribute) {
        return this.shopData[attribute];
    },
    isSelectedValue(attribute, value) {
        return this.shopData[attribute] === value;
    },
    isBillingCountrySelected(code) {
        let shopData = Template.instance().data.shopData;
        let country = Countries.findOne({_id: shopData.billingCountryId });
        return (country && country.code ? country.code === code : code === 'NL');
    }
});

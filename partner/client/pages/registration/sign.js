import SignaturePad from 'signature_pad';
import { contractHelpers } from './contract';

export let signaturePad;
Template.view_register_sign.onRendered(function() {
    let template = this;

   Tracker.afterFlush(() => {
       const canvas = document.getElementById("signatureCanvas");
       signaturePad = new SignaturePad(canvas);

       function resizeCanvas() {
           let ratio =  Math.max(window.devicePixelRatio || 1, 1);
           canvas.width = canvas.offsetWidth * ratio;
           canvas.height = canvas.offsetHeight * ratio;
           canvas.getContext("2d").scale(ratio, ratio);
           signaturePad.clear(); // otherwise isEmpty() might return incorrect value

           if (template.data.shopData.signature) {
               signaturePad.fromDataURL(template.data.shopData.signature);
           }
       }

       window.addEventListener("resize", resizeCanvas);
       resizeCanvas();
   });
});

Template.view_register_sign.helpers(contractHelpers);
Template.view_register_sign.helpers({
    getValue(attribute) {
        return this.shopData[attribute];
    },
    now() {
        return new Date();
    }
});

Template.view_register_sign.events({
    'click .clear-signature'(e, template) {
        e.preventDefault();
        signaturePad.clear();
    }
});

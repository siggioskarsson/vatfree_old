import moment from "moment/moment";

Template.view_invoice.onCreated(function() {
    this.activeItem = new ReactiveVar(Session.get(this.view.name + '.activeItem'));
    this.creatingImagesPDF = new ReactiveVar();
    this.sort = new ReactiveVar({
        receiptNr: -1
    });
});

Template.view_invoice.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('my-invoice', FlowRouter.getParam('invoiceId'), FlowRouter.getParam('secret'));
    });

    this.autorun(() => {
        if (this.subscriptionsReady()) {
            if (!this.activeItem.get()) {
                let item = Receipts.findOne({}, {
                    sort: {
                        receiptNr: -1
                    }
                });
                if (item) {
                    this.activeItem.set(item._id);
                }
            }
        }
    });

    this.autorun(() => {
        if (this.creatingImagesPDF.get()) {
            Meteor.setTimeout(() => {
                this.creatingImagesPDF.set();
            }, 30000);
        }
    });

    this.autorun(() => {
        if (Files.find({
                itemId: FlowRouter.getParam('invoiceId'),
                target: "invoices",
                subType: "images"
            }).count() > 0) {
            this.creatingImagesPDF.set();
        }
    });
});

Template.view_invoice.helpers({
    getInvoice() {
        let invoice = Invoices.findOne({
            _id: FlowRouter.getParam('invoiceId')
        }) || {};
        invoice.sorting = Template.instance().sort.get();

        if (invoice.language) {
            TAPi18n.setLanguage(invoice.language);
        }

        return invoice;
    },
    receipts() {
        return Receipts.find({
            invoiceId: FlowRouter.getParam('invoiceId')
        },{
            sort: Template.instance().sort.get()
        });
    },
    getTotalAmount() {
        let total = 0;
        Receipts.find({
            invoiceId: FlowRouter.getParam('invoiceId')
        }).forEach((receipt) => {
            total += receipt.amount;
        });

        return total;
    },
    getTotalVat() {
        let total = 0;
        Receipts.find({
            invoiceId: FlowRouter.getParam('invoiceId')
        }).forEach((receipt) => {
            total += receipt.totalVat;
        });

        return total;
    },
    getShop() {
        return Shops.findOne({
            _id: this.shopId
        });
    },
    getActiveItem() {
        let template = Template.instance();
        let itemId = template.activeItem.get();
        if (itemId) {
            return Receipts.findOne({
                _id: itemId
            });
        }
    },
    isActiveItem() {
        let activeItem = Template.instance().activeItem;
        if (!activeItem) {
            activeItem = Template.instance().parentTemplate().activeItem;
        }
        if (activeItem) {
            return activeItem.get() === this._id ? 'active' : '';
        }
    },
    getFiles() {
        return Files.find({
            itemId: FlowRouter.getParam('invoiceId'),
            target: 'invoices'
        });
    },
    isHeaderSorting(colId) {
        if (this.sorting && this.sorting[colId]) {
            return this.sorting[colId] === -1 ? 'sorting-desc' : 'sorting-asc';
        }
    },
    daysOpen() {
        return moment().diff(this.invoiceDate, 'days');
    },
    getDueDate() {
        return moment(this.invoiceDate).add(this.paymentTerms, 'days');
    },
    creatingImagesPDF() {
        return Template.instance().creatingImagesPDF.get();
    },
    imagesPDFExists() {
        return Files.find({
            itemId: this._id,
            target: "invoices",
            subType: "images"
        }).count() > 0;
    }
});

Template.view_invoice.events({
    'click .item-row'(e, template) {
        template.activeItem.set(this._id);
    },
    'click .item-list table th.sort-header'(e, template) {
        let data = $(e.currentTarget).data();
        if (data && data.id && template.sort) {
            let currentSort = template.sort.get();
            let sortOrder = -1;
            if (currentSort && currentSort[data.id] === -1) {
                sortOrder = 1;
            }
            template.sort.set({
                [data.id]: sortOrder
            });
        }
    },
    'click .invoice-create-pdf-images'(e, template) {
        e.preventDefault();
        template.creatingImagesPDF.set(true);
        Meteor.call('partner-create-invoice-pdf-images', this._id, (err, job) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                toastr.info(TAPi18n.__('Images PDF creation started in the background'));
            }
        });
    },
    'click .file'(e, template) {
        if (this.target === "invoices") {
            e.preventDefault();
            let fileObj = this;
            Meteor.call('getPartnerDirectUrl', fileObj._id, FlowRouter.getParam('invoiceId'), FlowRouter.getParam('secret'), (err, directUrl) => {
                if (err) {
                    console.error(err);
                    window.open(fileObj.url(), '_blank');
                } else {
                    window.open(directUrl, '_blank');
                }
            });
        }
    }
});

import moment from "moment";

Template.view_invoices.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('my-invoices', FlowRouter.getParam('invoiceId'), FlowRouter.getParam('secret'));
    });
});

Template.view_invoices.onCreated(function() {
    this.sort = new ReactiveVar({
        createdAt: -1
    });
});

Template.view_invoices.helpers({
    getInvoices() {
        return Invoices.find({},{
            sort: Template.instance().sort.get()
        });
    },
});

Template.view_invoices_list.helpers({
    daysOpen() {
        return moment().diff(this.invoiceDate, 'days');
    },
    getDueDate() {
        return moment(this.invoiceDate).add(this.paymentTerms, 'days');
    }
});

Template.view_invoices_list.events({
    'click .item-row'(e, template) {
        e.preventDefault();
        FlowRouter.go('/invoice/' + this._id + '/' + this.secret);
    }
});

import moment from 'moment';
import { receiptHelpers } from '../_receipts/helpers';

Template.receiptInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('receipt-files', data.receipt._id);
        this.subscribe('traveller-types');
        this.subscribe('my-profile');
    });
});

Template.receiptInfo.helpers(receiptHelpers);
Template.receiptInfo.helpers({
    getFiles() {
        let receiptId = this._id;
        return Files.find({
            itemId: receiptId
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getCountry() {
        return Countries.findOne({
            _id: this.profile.countryId
        });
    },
    passportExpired() {
        return moment(this.profile.passportExpirationDate).isBefore(moment());
    },
    userIsVerified() {
        return this.private && this.private.status ? this.private.status === 'userVerified' : false;
    },
    getStatusDescription() {
        if (this.private && this.private.status) {
            return TAPi18n.__('traveller_status_description_' + this.private.status) || "";
        }
    },
    statusRejected() {
        return this.status === 'rejected';
    },
    getReceiptRejections() {
        let selector = {};
        if (this.receiptRejectionIds) {
            selector['_id'] = {
                $in: this.receiptRejectionIds
            }
        }
        return ReceiptRejections.find(selector, {
            sort: {
                name: 1
            }
        });
    },
    getReceiptRejectionText() {
        return this.text['en'];
    },
    getDirectUrl() {
        return ReactiveMethod.call('getPartnerDirectUrl', this._id, FlowRouter.getParam('invoiceId'), FlowRouter.getParam('secret'));
    }
});

Template.receiptInfo.events({
});

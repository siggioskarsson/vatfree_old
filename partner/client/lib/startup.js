Meteor.startup(() => {
    Meteor.subscribe('currencies');

    Tracker.autorun(() => {
        let user = Meteor.user();
        let language = TAPi18n.getLanguage() || 'en';
        Meteor.subscribe('web-texts-site', 'partner', language);
    });
});

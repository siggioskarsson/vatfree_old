Template.topNavbar.onRendered(function() {
    this.autorun(() => {
        this.subscribe('my-invoice', FlowRouter.getParam('invoiceId'), FlowRouter.getParam('secret'));
    });
});

Template.topNavbar.helpers({
    showMenu() {
        return !!Meteor.userId() || (FlowRouter.getParam('invoiceId') && FlowRouter.getParam('secret'));
    },
    getInvoiceId() {
        return FlowRouter.getParam('invoiceId');
    },
    getInvoiceSecret() {
        return FlowRouter.getParam('secret');
    },
    getEntityName() {
        let invoice = Invoices.findOne({_id: FlowRouter.getParam('invoiceId')}) || {};
        if (invoice.shopId) {
            let shop = Shops.findOne({_id: invoice.shopId}) || {};
            return shop.name;
        } else {
            let company = Companies.findOne({_id: invoice.companyId}) || {};
            return company.name;
        }
    },
    getLogo() {
        return Files.findOne({
            category: 'logo'
        });
    },
    getTitle() {
        let title = _.isFunction(this.title) ? this.title() : false;
        if (title) {
            return title;
        }

        return false;
    }
});

Template.topNavbar.events({
    'click .logout'(e) {
        e.preventDefault();
        Meteor.logout();
    }
});

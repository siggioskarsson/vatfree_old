import '../imports/ui/pages/home/router';
import '../imports/ui/pages/dashboard/router';
import '../imports/ui/pages/receipts/router';
import '../imports/ui/pages/search/router';
import '../imports/ui/pages/travellers/router';
import '../imports/ui/pages/payouts/router';

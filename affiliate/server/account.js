Accounts.validateLoginAttempt(function (loginAttempt) {
    if (loginAttempt.error && loginAttempt.error.reason) {
        throw new Meteor.Error(404, loginAttempt.error.reason);
    }

    if (!loginAttempt.user) {
        throw new Meteor.Error(404, "Invalid login attempt");
    }

    if (!_.contains(loginAttempt.user.roles.__global_roles__, 'affiliate')) {
        throw new Meteor.Error(403, 'Not a valid affiliate user');
    }

    return true;
});

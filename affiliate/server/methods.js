import '../imports/api/dashboard/methods';
import '../imports/api/receipts/methods';
import '../imports/api/search/methods';
import '../imports/api/travellers/methods';

Meteor.methods({
    'setUserLanguage'(language) {
        if (!this.userId) {
            throw new Meteor.Error(404, 'Not logged in');
        }

        Meteor.users.update({
            _id: this.userId
        },{
            $set: {
                'profile.language': language
            }
        });
    },
    'save-my-user-profile'(profile, language) {
        if (!this.userId) {
            throw new Meteor.Error(404, 'Not logged in');
        }

        let user = Meteor.users.findOne({
            _id: this.userId,
            'roles.__global_roles__': 'affiliate'
        });

        if (!user) {
            throw new Meteor.Error(404, "Could not find user");
        }

        // we curate the fields that are allowed to change
        let updateUser = {
            'profile.gender': profile.gender,
            'profile.name': profile.name,
            'profile.addressFirst': profile.addressFirst,
            'profile.postalCode': profile.postalCode,
            'profile.city': profile.city,
            'profile.addressCountryId': profile.addressCountryId || profile.countryId,
            'profile.countryId': profile.countryId || profile.addressCountryId,
            'profile.birthDate': profile.birthDate,
            'profile.tel': profile.tel,
            'profile.tel2': profile.tel2
        };

        if (profile.language && _.find(Vatfree.languages.getShopLanguages(), (lang) => { return lang.code === profile.language; })) {
            updateUser['profile.language'] = profile.language
        }

        Meteor.users.update({
            _id: this.userId
        },{
            $set: updateUser
        });

        // reload user doc
        user = Meteor.users.findOne({_id: this.userId});
    },
    'savePushStatus'(userId, deviceId, pushStatus) {
        this.unblock();
        check(userId, String);
        check(deviceId, String);
        check(pushStatus, Object);
        if (!this.userId) {
            throw new Meteor.Error(404, 'Not logged in');
        }

        pushStatus.updatedAt = new Date();
        Meteor.users.update({
            _id: userId
        }, {
            $set: {
                [`private.pushStatus.${deviceId}`]: pushStatus
            }
        });
    },
    'unsetPushStatus'(userId, deviceId) {
        this.unblock();
        check(userId, String);
        check(deviceId, String);
        if (!this.userId) {
            throw new Meteor.Error(404, 'Not logged in');
        }

        Meteor.users.update({
            _id: userId
        }, {
            $unset: {
                [`private.pushStatus.${deviceId}`]: true
            }
        });
    },
    'sendPasswordReset'(email) {
        this.unblock();
        check(email, String);

        email = email.toLowerCase();
        let user = Meteor.users.findOne({"emails.address": email});
        if (user) {
            sendPasswordResetCode(user, email);
        }

        return true;
    },
    'resetPasswordFromCode'(email, password, password2, code) {
        this.unblock();
        check(email, String);
        check(password, String);
        check(password2, String);

        email = email.toLowerCase();
        let dbCode = Accounts.passwordLessCodes.findOne({ email: email + '_passwordReset', code: Number(code) });
        let user = Meteor.users.findOne({
            "emails.address": email,
            'roles.__global_roles__': 'affiliate'
        });
        console.log(user, dbCode);
        if (!user || !dbCode) {
            throw new Meteor.Error(500, "Could not find matching email and reset code, please verify your input and try again");
        }

        if (password !== password2) {
            throw new Meteor.Error(500, "Passwords do not match");
        }

        Accounts.setPassword(user._id, password);
        Accounts.passwordLessCodes.remove({ email: email + '_passwordReset' });
        Meteor.users.update({
            _id: user._id
        },{
            $set: {
                passwordSet: new Date()
            }
        });

        return true;
    },
    'accept-terms-and-conditions'() {
        if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.users.update({
            _id: this.userId,
            'profile.terms': {
                $exists: false
            }
        },{
            $set: {
                'profile.terms': new Date()
            }
        });
    },
    'affiliate-add-email'(email) {
        check(email, String);
        if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        email = email.toLowerCase();

        let existingUser = Meteor.users.findOne({
            'emails.address': email,
            'roles.__global_roles__': 'affiliate'
        });
        if (existingUser) {
            throw new Meteor.Error(500, "Email is already registered in our system");
        }

        Accounts.addEmail(this.userId, email, false);
        Accounts.sendVerificationEmail(this.userId, email);
    },
    'affiliate-resend-verification-email'(email) {
        check(email, String);
        if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        email = email.toLowerCase();

        let existingUser = Meteor.users.findOne({
            _id: this.userId,
            'emails.address': email
        });
        if (!existingUser) {
            throw new Meteor.Error(500, "Email address not found in our system");
        }

        let emailObject = _.find(existingUser.emails, (emailObj) => {
            if (emailObj.address === email) {
                return emailObj;
            }
        });
        if (emailObject && emailObject.verified !== true) {
            Accounts.sendVerificationEmail(this.userId, email);
        } else {
            throw new Meteor.Error(500, "Email address has already been verified");
        }

        return true;
    }
});

let sendPasswordResetCode = function (user, email) {
    if (user) {
        let affiliateStatusNotify = {
            emailTextId: Meteor.settings.defaults.emailPasswordResetTextId,
            emailTemplateId: Meteor.settings.defaults.emailTemplateId
        };
        let templateData = {
            traveller: user.profile
        };
        let toAddress = email;
        let fromAddress = "support@vatfree.com";
        let activityIds = {
            travellerId: user._id
        };
        let emailLanguage = user.profile.language || 'en';

        // send magic link email - using the notification class
        const notificationOptions = {
            suppressMobileNotification: true,
            hideInMessageBox: true
        };
        return Vatfree.notify.sendNotification(affiliateStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage, false, notificationOptions);
    }
};

Package.describe({
    name: 'affiliate',
    summary: 'Affiliate import package',
    version: '0.0.2',
    git: ''
});

Package.onUse(function (api) {
    // This needs to be loaded first as the core package uses this
    api.use([
        'simonsimcity:job-collection@1.6.1',
    ], 'server');

    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'dburles:google-maps@1.1.5',
        'flemay:less-autoprefixer@1.2.0',
        'cfs:standard-packages@0.5.9',
        'cfs:gridfs@0.0.33',
        'cfs:ui@0.1.3',
        'cfs:s3@0.1.3',
        'cfs:graphicsmagick@0.0.18',
        'vatfree:core',
        'vatfree:core-templates',
    ]);

    api.use([
        'vatfree:countries',
        'vatfree:currencies',
        'vatfree:dashboard',
        'vatfree:receipts',
        'vatfree:receipt-rejections',
        'vatfree:travellers',
        'vatfree:traveller-types',
        'vatfree:traveller-rejections',
        'vatfree:shops',
        'vatfree:companies',
        'vatfree:payouts',
        'vatfree:affiliates',
        'vatfree:affiliate-payouts',
        'vatfree:invoices',
        'vatfree:activity-logs',
        'vatfree:notify',
        'vatfree:email-templates',
        'vatfree:email-texts',
        'vatfree:web-texts'
    ], 'server');

    // shared files
    api.addFiles([
    ]);

    // server files
    api.addFiles([
        'server/methods.js'
    ], 'server');

    // client files
    api.addFiles([
    ], 'client');

    api.export([
        'Vatfree',
        'RemindersSchema',
        'CreatedUpdatedSchema',
        'TranslatableSchema',
        'StatusDates',
        'BillingSchema',
        'Receipts',
        'ReceiptRejections',
        'Travellers',
        'TravellerTypes',
        'TravellerRejections',
        'Companies',
        'Payouts',
        'Affiliates',
        'AffiliatePayouts',
        'Countries',
        'Currencies',
        'EmailTexts',
        'EmailTemplates',
        'ActivityLogs',
        'Shops',
        'Invoices',
        'invoiceJobs',
        'WebTexts',
        'FS',
        'Files',
        'SimpleSchema'
    ]);
});

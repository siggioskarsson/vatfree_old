#!/usr/bin/env bash
# node-canvas requires external libs
# brew install pkg-config cairo libpng jpeg giflib
meteor npm install --no-optional
MONGO_URL=mongodb://localhost:6002/meteor ROOT_URL=http://local.vatfree.com:6041/ meteor --port 6041 --settings settings.json

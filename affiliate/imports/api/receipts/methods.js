Meteor.methods({
    'affiliate-get-receipt-stats'(selector) {
        if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }
        selector.affiliateId = this.userId;
        const pipeline = [
            {
                $match: selector,
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 },
                    amount: { $sum: { $ifNull: ['$euroAmounts.amount', 0] } },
                    exVat: { $sum: { $subtract: ["$vamount", "$euroAmounts.totalVat"] } },
                    vat: { $sum: { $ifNull: ['$euroAmounts.totalVat', 0] } },
                    fee: { $sum: { $ifNull: ['$euroAmounts.serviceFee', 0] } },
                    affiliateFee: { $sum: { $ifNull: ['$euroAmounts.affiliateServiceFee', 0] } },
                    refund: { $sum: { $ifNull: ['$euroAmounts.refund', 0] } }
                }
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    }
});

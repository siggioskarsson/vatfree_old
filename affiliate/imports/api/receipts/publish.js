Meteor.publishComposite('affiliate-my-receipts', function(searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    selector.affiliateId = this.userId;

    const children = [
        {
            collectionName: "travellers",
            find: function (receipt) {
                return Meteor.users.find({
                    _id: receipt.userId
                });
            }
        },
        {
            find: function (receipt) {
                return Shops.find({
                    _id: receipt.shopId
                });
            }
        },
        {
            find: function (receipt) {
                return ReceiptRejections.find({
                    _id: {
                        $in: receipt.receiptRejectionIds || []
                    }
                });
            }
        }
    ];

    return {
        find: function () {
            return Receipts.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset,
                fields: {
                    reminders: false
                }
            });
        },
        children: children
    };
});

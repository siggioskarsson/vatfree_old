Meteor.methods({
    'affiliate-search'(searchTerm) {
        if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {
            affiliateId: this.userId
        };
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let entities = {};
        entities.travellers = Meteor.users;
        entities.receipts = Receipts;
        entities.payouts = AffiliatePayouts;

        let results = {};
        _.each(entities, (collection, entityId) => {
            results[entityId] = [];

            let collectionSelector = _.clone(selector);
            if (entityId === 'travellers') {
                collectionSelector['roles.__global_roles__'] = 'traveller';
                collectionSelector['private.affiliateId'] = this.userId;
                delete collectionSelector['affiliateId'];
            }

            collection.find(collectionSelector, {
                sort: {
                    createdAt: -1
                },
                limit: 25
            }).forEach((entity) => {
                results[entityId].push(entity);
            });
        });

        return results;
    }
});

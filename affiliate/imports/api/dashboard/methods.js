import moment from 'moment';

Meteor.methods({
    'affiliate-dashboard-count-receipts-last-weeks' (weeks) {
        if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let map = function () {
            var status = this.status;
            switch (status) {
                case 'visualValidationPending':
                case 'waitingForDoubleCheck':
                case 'shopPending':
                case 'waitingForAdministrativeCheck':
                    status = 'visualValidationPending';
                    break;
                case 'userPendingForPayment':
                    status = 'userPending';
                    break;
                case 'rejected':
                case 'deleted':
                case 'notCollectable':
                case 'depreciated':
                    status = 'rejected';
                    break;
            }
            var key = '' + this.createdWeek + ':' + status;
            emit(key, 1);
        };
        let reduce = function (status, counts) {
            return Array.sum(counts);
        };

        return Receipts.rawCollection().mapReduce(map, reduce, {
            query: {
                affiliateId: this.userId,
                createdAt: {
                    $gte: moment().startOf('isoWeek').subtract(weeks, 'weeks').toDate()
                }
            },
            out: {
                inline: 1
            }
        });
    },
    'affiliate-dashboard-totals' () {
        if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        const aggregationKey = null;
        const pipeline = [
            {
                $match: {
                    affiliateId: this.userId
                },
            },
            {
                $group: {
                    _id : aggregationKey,
                    count: {$sum: 1},
                    amount: {$sum: {$ifNull: ['$euroAmounts.amount', 0]}},
                    exVat: {$sum: {$subtract: ['$euroAmounts.amount', '$euroAmounts.totalVat']}},
                    vat: {$sum: {$ifNull: ['$euroAmounts.totalVat', 0]}},
                    affiliateFee: {$sum: {$ifNull: ['$euroAmounts.affiliateServiceFee', 0]}},
                    refund: {$sum: {$ifNull: ['$euroAmounts.refund', 0]}}
                }
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    }
});

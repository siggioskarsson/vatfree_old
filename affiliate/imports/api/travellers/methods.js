Meteor.methods({
    'affiliate-get-traveller-stats'(selector) {
        if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }
        selector['roles.__global_roles__'] = 'traveller';
        selector['private.affiliateId'] = this.userId;

        const pipeline = [
            {
                $match: selector,
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 },
                }
            },
        ];
        return Travellers.rawCollection().aggregate(pipeline).toArray();
    }
});

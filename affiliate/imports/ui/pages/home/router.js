FlowRouter.route('/', {
    name: "vatfree.com",
    triggersEnter() {
        FlowRouter.go('/dashboard');
    },
    action: function() {
        import("/imports/ui/pages/home")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "affiliate_home"});
            });
    }
});

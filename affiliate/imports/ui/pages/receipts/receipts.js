import './helpers';
import './receipts.html';
import { receiptHelpers } from './helpers';

Template.affiliate_receipts.onCreated(Vatfree.templateHelpers.onCreated(Receipts, '/receipts/', '/receipt/:itemId'));
Template.affiliate_receipts.onRendered(Vatfree.templateHelpers.onRendered());
Template.affiliate_receipts.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.affiliate_receipts.helpers(Vatfree.templateHelpers.helpers);
Template.affiliate_receipts.helpers(receiptHelpers);
Template.affiliate_receipts.events(Vatfree.templateHelpers.events());

Template.affiliate_receipts.onCreated(function() {
    this.subscription = 'affiliate-my-receipts';
});

Template.affiliate_receipts.onRendered(function() {
    this.autorun(() => {
        this.subscribe('receipt-rejections');
        this.subscribe('receipt-types');
    });
});

Template.affiliate_receipts.helpers({
    getReceiptStats() {
        let template = Template.instance();
        let selector = receiptHelpers.getReceiptSelector(template);

        let stats = ReactiveMethod.call('affiliate-get-receipt-stats', selector);
        return stats && stats[0] ? stats[0] : false;
    }
});

import moment from 'moment';
import '../shops/card';
import '../travellers/card';
import './info.html';
import { receiptHelpers } from './helpers';
import './receipt-rejection-list';

Template.receiptInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.receiptInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('receipts_item', data.receipt._id);
    });
});

Template.receiptInfo.helpers(receiptHelpers);
Template.receiptInfo.helpers({
    getCountry() {
        return Countries.findOne({
            _id: this.profile.countryId
        });
    },
    passportExpired() {
        return moment(this.profile.passportExpirationDate).isBefore(moment());
    },
    showActions() {
        let template = Template.instance();
        return template.data.showActions && (Vatfree.receipts.isMyTask(this, Meteor.userId()) || this.status === 'waitingForOriginal');
    },
    addHideButtons() {
        this.hideButtons = true;
        return this;
    }
});

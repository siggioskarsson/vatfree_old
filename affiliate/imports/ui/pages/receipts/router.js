FlowRouter.route('/receipts', {
    name: "vatfree.com",
    action: function() {
        import("/imports/ui/pages/receipts")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "affiliate_receipts"});
            });
    }
});

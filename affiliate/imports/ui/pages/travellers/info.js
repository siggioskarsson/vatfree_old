import './info.html';

Template.travellerInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.travellerInfo.helpers(travellerHelpers);
Template.travellerInfo.helpers({
    alternativeTravellerType() {
        return this.private.travellerTypeId && this.private.travellerTypeId !== Vatfree.defaults.TravellerTypeId;
    }
});

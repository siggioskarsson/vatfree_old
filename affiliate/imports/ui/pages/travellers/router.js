FlowRouter.route('/travellers', {
    name: "vatfree.com",
    action: function() {
        import("/imports/ui/pages/travellers")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "affiliate_travellers"});
            });
    }
});

import moment from 'moment';
import './travellers.html';
import './helpers'

Template.affiliate_travellers.onCreated(Vatfree.templateHelpers.onCreated(Travellers, '/travellers/', '/traveller/:itemId'));
Template.affiliate_travellers.onCreated(function() {
    this.subscription = 'affiliate-my-travellers';
    this.statusAttribute = 'private.status';

    this.selectorFunction = (selector) => {
    }
});

Template.affiliate_travellers.onRendered(Vatfree.templateHelpers.onRendered());
Template.affiliate_travellers.onDestroyed(Vatfree.templateHelpers.onDestroyed());
Template.affiliate_travellers.helpers(Vatfree.templateHelpers.helpers);
Template.affiliate_travellers.helpers(travellerHelpers);
Template.affiliate_travellers.helpers({
    getTravellerStats() {
        let template = Template.instance();
        let selector = travellerHelpers.getTravellersSelector(template);

        let stats = ReactiveMethod.call('affiliate-get-traveller-stats', selector);
        return stats && stats[0] ? stats[0] : false;
    }
});

Template.affiliate_travellers.events(Vatfree.templateHelpers.events());
Template.affiliate_travellers.events({
    'click .task-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.taskTravellerId.set(this._id);
        template.doingTask.set(true);
    }
});

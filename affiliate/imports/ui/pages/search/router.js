FlowRouter.route('/search', {
    action: function() {
        import("/imports/ui/pages/search")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "affiliate_search"});
            });
    }
});

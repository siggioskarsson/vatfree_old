import './search.html';
import '../travellers/list';
import '../receipts/list';
import '../payouts/list';

Template.affiliate_search.onCreated(function () {
    this.searching = new ReactiveVar();

    this.travellers = new ReactiveVar();
    this.receipts = new ReactiveVar();
    this.payouts = new ReactiveVar();
});

Template.affiliate_search.onRendered(function () {
    this.autorun(() => {
        let searchTerm = FlowRouter.getQueryParam('term');
        if (!searchTerm) return;

        this.searching.set(true);
        Meteor.call('affiliate-search', searchTerm, (err, result) => {
            console.log(result);
            if (err) {
                toastr.error(err.reason);
            } else {
                this.travellers.set(result.travellers);
                this.receipts.set(result.receipts);
                this.payouts.set(result.payouts);
            }

            this.searching.set();
        });
    });
});

Template.affiliate_search.helpers({
    getSearchTerm() {
        return FlowRouter.getQueryParam('term');
    },
    isSearching() {
        return Template.instance().searching.get();
    },
    getTravellers() {
        let template = Template.instance();
        return template.travellers.get();
    },
    getReceipts() {
        let template = Template.instance();
        return template.receipts.get();
    },
    getPayouts() {
        let template = Template.instance();
        return template.payouts.get();
    },
});

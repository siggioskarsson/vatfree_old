import './dashboard.html';
import './components/count-receipts-last-12-weeks';
import './components/pill';

Template.affiliate_dashboard.onCreated(function() {
    this.totalData = new ReactiveVar();
});

Template.affiliate_dashboard.onRendered(function() {
    Meteor.call('affiliate-dashboard-totals', (err, data) => {
        if (err) {
            toastr.error(err.reason || err.message);
        } else {
            this.totalData.set(data);
        }
    });
});

Template.affiliate_dashboard.helpers({
    getTotalEarned() {
        const data = Template.instance().totalData.get();
        return Vatfree.numbers.formatCurrency(data ? data[0].affiliateFee : 0);
    },
    getTotalRefunds() {
        const data = Template.instance().totalData.get();
        return Vatfree.numbers.formatCurrency(data ? data[0].refund : 0);
    },
    getTotalVat() {
        const data = Template.instance().totalData.get();
        return Vatfree.numbers.formatCurrency(data ? data[0].vat : 0);
    },
    getTotalNumberOfReceipts() {
        const data = Template.instance().totalData.get();
        return data ? data[0].count : 0;
    }
});

Template.affiliate_dashboard.events({
});

FlowRouter.route('/dashboard', {
    name: "vatfree.com",
    action: function() {
        import("/imports/ui/pages/dashboard")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "affiliate_dashboard"});
            });
    }
});

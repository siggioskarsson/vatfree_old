import Chart from 'chart.js';
import './pill.html';

Template.dashboard_pill.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        Tracker.afterFlush(() => {
            if (data.doughnutData) {
                const doughnutOptions = {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    layout: {
                        padding: {
                            left: 30,
                            right: 30,
                            top: 30,
                            bottom: 30
                        }
                    }
                };

                if (this.$("#doughnutChart").length > 0) {
                    const ctx4 = this.$("#doughnutChart")[0].getContext("2d");
                    new Chart(ctx4, { type: 'doughnut', data: data.doughnutData, options:doughnutOptions });
                }
            }
        });
    });
});

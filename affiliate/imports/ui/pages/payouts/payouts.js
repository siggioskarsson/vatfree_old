import './payouts.html';
import './helpers';

Template.affiliate_payouts.onCreated(Vatfree.templateHelpers.onCreated(Payouts, '/payouts/', '/payout/:itemId'));
Template.affiliate_payouts.onRendered(Vatfree.templateHelpers.onRendered());
Template.affiliate_payouts.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.affiliate_payouts.helpers(Vatfree.templateHelpers.helpers);
Template.affiliate_payouts.helpers(payoutHelpers);
Template.affiliate_payouts.events(Vatfree.templateHelpers.events());

Template.affiliate_payouts.onCreated(function() {
    this.creatingPayouts = new ReactiveVar();
    this.showJobs = new ReactiveVar();
    this.paymentMethodFilter = new ReactiveArray();
    _.each(Session.get(this.view.name + '.paymentMethodFilter') || [], (paymentMethod) => {
        this.paymentMethodFilter.push(paymentMethod);
    });

    this.partnerFilter = new ReactiveVar(Session.get(this.view.name + '.partnerFilter'));

    this.autorun(() => {
        Session.setPersistent(this.view.name + '.paymentMethodFilter', this.paymentMethodFilter.array());
        Session.setPersistent(this.view.name + '.partnerFilter', this.partnerFilter.get());
    });

    this.selectorFunction = (selector) => {
        let paymentMethodFilter = this.paymentMethodFilter.array() || [];
        if (paymentMethodFilter.length > 0) {
            selector.paymentMethod = {
                $in: paymentMethodFilter
            };
        }

        let partnerFilter = this.partnerFilter.get();
        if (partnerFilter) {
            if (partnerFilter === 'active') {
                selector.partnerId = {
                    $exists: true
                }
            }
            if (partnerFilter === 'inactive') {
                selector.partnerId = {
                    $exists: false
                }
            }
        }
    };

    this.todoStatus = [
        'new'
    ];
    this.sort.set({
        payoutNr: -1
    });

    this.subscribe('debt-collection-lines', "");
});

Template.affiliate_payouts.helpers({
    creatingPayouts() {
        return Template.instance().creatingPayouts.get();
    },
    showJobs() {
        return Template.instance().showJobs.get();
    },
    getDebtCollectionLines() {
        return DebtCollectionLines.find();
    },
    isActivePaymentMethodFilter(method) {
        let paymentMethodFilter = Template.instance().paymentMethodFilter.array() || [];
        return _.contains(paymentMethodFilter, method) ? 'active' : '';
    },
    getPayoutStats() {
        let template = Template.instance();
        let selector = payoutHelpers.getPayoutSelector(template);

        console.log('payout selector', selector);
        let stats = ReactiveMethod.call('get-payout-stats', selector);
        return stats && stats[0] ? stats[0] : false;
    },
    isActivePartnerFilter() {
        return Template.instance().partnerFilter.get() === 'active';
    },
    isInactivePartnerFilter() {
        return Template.instance().partnerFilter.get() === 'inactive';
    }
});

Template.affiliate_payouts.events({
    'click .filter-icon.partner-filter-icon'(e, template) {
        e.preventDefault();
        const partnerFilter = template.partnerFilter.get();
        if (partnerFilter === 'active') {
            template.partnerFilter.set('inactive');
        } else if (partnerFilter === 'inactive') {
            template.partnerFilter.set(false);
        } else {
            template.partnerFilter.set('active');
        }
    },
    'click .filter-icon.payment-method-filter-icon'(e, template) {
        e.preventDefault();
        let method = $(e.currentTarget).attr('id');
        let paymentMethodFilter = template.paymentMethodFilter.array();

        if (_.contains(paymentMethodFilter, method)) {
            template.paymentMethodFilter.clear();
            _.each(paymentMethodFilter, (_method) => {
                if (method !== _method) {
                    template.paymentMethodFilter.push(_method);
                }
            });
        } else {
            template.paymentMethodFilter.push(method);
        }
    },
    'click .payout-create-new'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).attr('disabled') === 'disabled') {
            return false;
        }

        if (confirm('Do you want to create an export file and mark the payouts as sent?')) {
            Meteor.call('create-payout-batch', false, (err, payoutBatchIds) => {
                if (err) {
                    alert(err.reason || err.message);
                } else {
                    if (payoutBatchIds) {
                        FlowRouter.go('/payout/payout-batch/' + payoutBatchIds.join(','));
                    } else {
                        toastr.warning('No payouts found');
                    }
                }
            });
        }
    },
    'click .payout-create-new-partner'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).attr('disabled') === 'disabled') {
            return false;
        }

        if (confirm('Do you want to create an export file and mark the payouts as sent?')) {
            Meteor.call('create-payout-batch', true, (err, payoutBatchIds) => {
                if (err) {
                    alert(err.reason || err.message);
                } else {
                    if (payoutBatchIds) {
                        FlowRouter.go('/payout/payout-batch/' + payoutBatchIds.join(','));
                    } else {
                        toastr.warning('No payouts found');
                    }
                }
            });
        }
    }
});

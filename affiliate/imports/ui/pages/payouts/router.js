FlowRouter.route('/payouts', {
    name: "vatfree.com",
    action: function() {
        import("/imports/ui/pages/payouts")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "affiliate_payouts"});
            });
    }
});

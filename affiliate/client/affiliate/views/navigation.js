Template.navigation.onCreated(function() {
});

Template.navigation.onRendered(function() {
    // Initialize metisMenu
    Tracker.afterFlush(() => {
        $('#side-menu').metisMenu();
    });
});

Template.navigation.helpers({
    newVersionAvailable() {
        return Reload.isWaitingForResume();
    },
});

Template.navigation.events({
    'click .update-app'(e) {
        e.preventDefault();
        Reload.migrate();
    }
});

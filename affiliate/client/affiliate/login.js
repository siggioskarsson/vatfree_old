Template.login.onCreated(function() {
    this.forgotPassword = new ReactiveVar();
    this.forgotPasswordSet = new ReactiveVar();
    this.isSendingPasswordRestCode = new ReactiveVar();
    this.isSettingPassword = new ReactiveVar();
    this.isLoggingIn = new ReactiveVar();
});

Template.login.helpers({
    forgotPassword() {
        return Template.instance().forgotPassword.get();
    },
    forgotPasswordSet() {
        return Template.instance().forgotPasswordSet.get();
    },
    isSettingPassword() {
        return Template.instance().isSettingPassword.get();
    },
    isLoggingIn() {
        return Template.instance().isLoggingIn.get();
    },
});

Template.login.events({
    'click .forgot-password'(e, template) {
        e.preventDefault();
        template.forgotPassword.set(true);
    },
    'click .cancel-forgot-password'(e, template) {
        e.preventDefault();
        template.forgotPassword.set();
        template.forgotPasswordSet.set();
    },
    'submit form[name="password-forgot-form"]'(e, template) {
        e.preventDefault();
        let email = $('input[name="email"]').val();

        if (!email) {
            alert(Vatfree.translate("Please fill in an email address"));
        }

        template.isSendingPasswordRestCode.set(true);
        Meteor.call('sendPasswordReset', email, (err, result) => {
            if (err) {
                alert(err.reason);
            } else {
                template.forgotPasswordSet.set(email);
                template.isSendingPasswordRestCode.set();
                Tracker.afterFlush(() => {
                    alert(Vatfree.translate('We sent you a reset code by email. Please fill in the code in the field below and reset your password'));
                });
            }
        });
    },
    'submit form[name="password-forgot-set-form"]'(e, template) {
        e.preventDefault();
        e.preventDefault();
        let email = $('input[name="email"]').val();
        let code = $('input[name="code"]').val();

        let password = $('input[name="password"]').val();
        let password2 = $('input[name="password2"]').val();

        if (!email) {
            alert("Please fill in an email address.");
            return false;
        }
        if (!password || password !== password2) {
            alert(Vatfree.translate("Passwords do not match"));
            return false;
        }

        template.isSettingPassword.set(true);
        Meteor.call('resetPasswordFromCode', email, password, password2, code, (err, result) => {
            if (err) {
                alert(err.reason);
                template.isSettingPassword.set();
            } else {
                Meteor.loginWithPassword(email, password, (err, result) => {
                    if (err) {
                        alert(err.reason);
                    } else {
                        template.forgotPassword.set();
                        template.forgotPasswordSet.set();
                    }
                    template.isSettingPassword.set();
                });
            }
        });
    },
    'submit form[name="password-login"]'(e, template) {
        e.preventDefault();
        let email = $('input[name="email"]').val();
        let password = $('input[name="password"]').val();

        template.isLoggingIn.set(true);
        Meteor.loginWithPassword(email, password, (err, result) => {
            if (err) {
                alert(Vatfree.translate("Could nog log on. Please check your email and password and try again."));
            }
            template.isLoggingIn.set();
        });
    }
});

#!/usr/bin/env bash

echo "Fill in the mlab password, followed by [ENTER]:"
read -s password

mongodump -u heroku_ljq5j0xx -p $password -h ds161800-a0.mlab.com:61800 -d "heroku_ljq5j0xx" --out=../heroku_ljq5j0xx/
mongorestore --drop -h localhost:6002 -d "meteor" ../heroku_ljq5j0xx/heroku_ljq5j0xx/
rm -rf ../heroku_ljq5j0xx/

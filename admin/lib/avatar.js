// https://github.com/meteor-utilities/avatar
Avatar.setOptions({
    fallbackType: "initials",
    emailHashProperty: "profile.email",
    backgroundColor: '#0069b4',
    imageSizes: {
        listing: 24,
        header: 32,
        info: 150,
        'nav-header': 64
    }
});

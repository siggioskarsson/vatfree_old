#!/usr/bin/env bash

# this is needed for building jpeglib.h, needed by node-canvas
CPLUS_INCLUDE_PATH=/usr/local/include:/usr/local/lib
export CPLUS_INCLUDE_PATH

meteor --settings settings.json --driver-package meteortesting:mocha --port 10015  test-packages $1

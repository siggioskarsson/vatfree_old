#!/usr/bin/env bash
# node-canvas requires external libs
# brew install pkg-config cairo libpng jpeg giflib
meteor npm install --no-optional
meteor --port 6001 --settings settings.json

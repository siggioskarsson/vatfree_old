#!/usr/bin/env bash

echo "Fill in the PROD mlab password, followed by [ENTER]:"
read -s password

echo "Fill in the TEST mlab password, followed by [ENTER]:"
read -s passwordTest

mongodump -u heroku_7vrc112b -p $password  -h "rs-ds145542/ds145542-a0.mlab.com:45542,ds145542-a1.mlab.com:45542" -d "heroku_7vrc112b" --out=../heroku_7vrc112b/ --excludeCollectionsWithPrefix=activity
mongorestore --drop -u vatfree-test -p $passwordTest -h "rs-ds145542/ds145542-a0.mlab.com:45542,ds145542-a1.mlab.com:45542" -d "vatfree_test" ../heroku_7vrc112b/heroku_7vrc112b/
rm -rf ../heroku_7vrc112b/

#/bin/sh

aws --profile vatfree s3 sync --exclude logs/* s3://vatfree-prod s3://vatfree-test --sse --acl authenticated-read --region eu-central-1

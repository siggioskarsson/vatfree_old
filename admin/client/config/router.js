let checkLogin = function() {
    Tracker.autorun(function() {
        if (!Meteor.user() && !Accounts.loggingIn()) {
            FlowRouter.go('/login');
        }
    });
};

FlowRouter.triggers.enter([
    checkLogin
]);

FlowRouter.route('/', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "dashboard"});
    }
});

FlowRouter.route('/login', {
    action: function() {
        Tracker.autorun(function() {
            if (!Meteor.user()) {
                BlazeLayout.render("blankLayout", {content: "login"});
            } else if (!Accounts.loggingIn()) {
                FlowRouter.go('/');
            }
        });
    }
});

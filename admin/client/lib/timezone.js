import moment from "moment-timezone";

Meteor.startup(() => {
    moment.tz.setDefault("Europe/Amsterdam");
});

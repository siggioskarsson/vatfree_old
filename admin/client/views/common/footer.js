Template.footer.onCreated(function () {
    this.currentVersion = new ReactiveVar();
});

Template.footer.helpers({
    getCurrentVersion: function() {
        var template = Template.instance();
        var version = template.currentVersion.get();
        if (!version) {
            Meteor.call('getCurrentVersion', function(err, version) {
                if (version) {
                    template.currentVersion.set(version);
                }
            });
        }
        return version;
    }
});

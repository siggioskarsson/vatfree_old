Template.mainLayout.onCreated(function(){
    this.versionInfo = new ReactiveVar();
    Meteor.call('get-version-info', (err, version) => {
        if (version) {
            this.versionInfo.set(version);
        }
    });
});

Template.mainLayout.onRendered(function(){
    // subscribe -> searchTerm, selector, sort, limit, offset
    this.subscribe('countries', "", {}, {_id: 1}, 1000, 0);
    this.subscribe('currencies', "", {}, {_id: 1}, 1000, 0);
    this.subscribe('traveller-types', "", {}, {_id: 1}, 1000, 0);
    this.subscribe('receipt-types', "", {}, {_id: 1}, 1000, 0);
    this.subscribe('receipt-rejections-list', "", {}, {_id: 1}, 1000, 0);

    this.subscribe('locks');
    this.subscribe('admins');

    // Minimalize menu when screen is less than 768px
    $(window).bind("resize load", function () {
        if ($(this).width() < 769) {
            $('body').addClass('body-small')
        } else {
            $('body').removeClass('body-small')
        }
    });

    // Fix height of layout when resize, scroll and load
    $(window).bind("load resize scroll", function() {
        if(!$("body").hasClass('body-small')) {

            var navbarHeigh = $('nav.navbar-default').height();
            var wrapperHeigh = $('#page-wrapper').height();

            if(navbarHeigh > wrapperHeigh){
                $('#page-wrapper').css("min-height", navbarHeigh + "px");
            }

            if(navbarHeigh < wrapperHeigh){
                $('#page-wrapper').css("min-height", $(window).height()  + "px");
            }

            if ($('body').hasClass('fixed-nav')) {
                if (navbarHeigh > wrapperHeigh) {
                    $('#page-wrapper').css("min-height", navbarHeigh + "px");
                } else {
                    $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
                }
            }
        }
    });
});

Template.mainLayout.helpers({
    getVersionInfo() {
        return Template.instance().versionInfo.get();
    }
});

Template.mainLayout.events({
    'dropped #wrapper'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    }
});

Template.navigation.onCreated(function() {
    this.physicalLocation = new ReactiveVar(localStorage.getItem('physical-location'));

    this.autorun(() => {
        let physicalLocation = this.physicalLocation.get();
        localStorage.setItem('physical-location', (physicalLocation || "MO"));
    });
});

Template.navigation.onRendered(function() {
    // Initialize metisMenu
    Tracker.afterFlush(() => {
        $('#side-menu').metisMenu();
    });

    this.autorun(() => {
        this.subscribe('menu-stats');
        this.subscribe('office-locations');
    });
});

Template.navigation.helpers({
    newVersionAvailable() {
        return Reload.isWaitingForResume();
    },
    physicalLocation() {
        return Template.instance().physicalLocation.get();
    },
    officeLocations() {
        return OfficeLocations.find({deleted: {$exists: false}}, {sort: {locationCode: 1}});
    },
    shopCallsAndIncompletes() {
        let stats = Stats.findOne({_id: 'shop_incompletes'});
        if (stats && stats.count > 0) {
            let stats2 = Stats.findOne({_id: 'shop_calls'});
            if (stats2 && stats2.count > 0) {
                return true;
            }
        }

        return false;
    }
});

Template.navigation.events({
    'change select[name="physical-location"]'(e, template) {
        template.physicalLocation.set($(e.currentTarget).val());
    },
    'click .update-app'(e) {
        e.preventDefault();
        Reload.migrate();
    }
});

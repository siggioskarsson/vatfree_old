Template.login.events({
    'submit form[name="login-form"]'(e) {
        e.preventDefault();
        Meteor.loginWithGoogle({
            requestPermissions: ['email', 'profile']
        }, (err) => {
            if (err) {
                console.log(err);
                alert(err.reason);
            } else {
                // successful login!
                console.log('successful login!');
            }
        });
    }
});

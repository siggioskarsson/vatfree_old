FlowRouter.route('/files', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "files"});
    }
});

FlowRouter.route('/file/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "fileEdit"});
    }
});

Meteor.publishComposite('files_item', function(itemId) {
    check(itemId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Files.find({
                _id: itemId
            });
        },
        children: [
            {
                find: function (file) {
                    return Receipts.find({
                        _id: file.itemId
                    });
                }
            },
            {
                collectionName: "travellers",
                find: function (file) {
                    return Meteor.users.find({
                        _id: file.itemId
                    });
                }
            },
            {
                find: function (file) {
                    return Invoices.find({
                        _id: file.itemId
                    });
                }
            },
            {
                find: function (file) {
                    return Payments.find({
                        _id: file.itemId
                    });
                }
            },
            {
                find: function (file) {
                    return Payouts.find({
                        _id: file.itemId
                    });
                }
            },
            {
                find: function (file) {
                    return Companies.find({
                        _id: file.itemId
                    });
                }
            },
            {
                find: function (file) {
                    return Shops.find({
                        _id: file.itemId
                    });
                }
            }
        ]
    }
});

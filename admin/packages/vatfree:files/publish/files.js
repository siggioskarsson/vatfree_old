Meteor.publish('vatfree-files', function(searchTerm, selector, sort, limit, offset) {
    if (!this.userId) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};
    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return Files.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset
    });
});

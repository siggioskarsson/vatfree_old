Template.fileEdit.onCreated(function () {

});

Template.fileEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('files_item', FlowRouter.getParam('itemId'));
    });
});

Template.fileEdit.helpers({
    itemDoc() {
        return Files.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    getBreadcrumbTitle() {
        return this.original.name;
    }
});

Template.fileEditForm.helpers({
    getRawJSON() {
        return JSON.stringify(this, false, 2);
    }
});

Template.fileEditForm.events({
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/files');
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);
        formData['original.name'] = formData.name;
        delete formData.name;

        const unset = {};
        _.each(formData, (fd, key) => {
            if (!fd) { // falsey check is fine here
                unset[key] = true;
                delete formData[key];
            }
        });

        const updateData = {
            $set: formData
        };

        if (_.size(unset)) {
            updateData['$unset'] = unset;
        }

        Files.update({
            _id: this._id
        }, updateData, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('File saved!')
            }
        });
    }
});

Template.fileInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.fileInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('files_item', data._id);
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.fileInfo.helpers(fileHelpers);
Template.fileInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

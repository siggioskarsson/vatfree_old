Template.files.onCreated(Vatfree.templateHelpers.onCreated(Files, '/files/', '/file/:itemId'));
Template.files.onRendered(Vatfree.templateHelpers.onRendered());
Template.files.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.files.helpers(Vatfree.templateHelpers.helpers);
Template.files.helpers(fileHelpers);

Template.files.onCreated(function() {
    if (!this.sort.get()) {
        this.sort.set({
            createdAt: -1
        });
    }

    this.entityFilter = new ReactiveArray();
    _.each(Session.get(this.view.name + '.entityFilter') || [], (entity) => {
        this.entityFilter.push(entity);
    });
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.entityFilter', this.entityFilter.array());
    });

    this.selectorFunction = function(selector) {
        let entityFilter = this.entityFilter.array() || [];
        if (entityFilter.length > 0) {
            _.each(entityFilter, (entityId) => {
                switch(entityId) {
                    case "travellers":
                        selector.target = {$in: ['travellers', 'travellers_internal', 'travellers_trash']};
                        break;
                    case "receipts":
                        selector.target = {$in: ['receipts', 'receipts_internal']};
                        break;
                    default:
                        selector.target = entityId;
                        break;
                }
            });
        }
    };
});

Template.files.helpers({
    isEntityFilter(entity) {
        let entityFilter = Template.instance().entityFilter.array() || [];
        return _.contains(entityFilter, entity) ? 'active' : '';
    }
});

Template.files.events(Vatfree.templateHelpers.events());
Template.files.events({
    'click .filter-icon.entity-filter-icon'(e, template) {
        e.preventDefault();
        let entity = $(e.currentTarget).attr('id');
        let entityFilter = template.entityFilter.array();

        if (_.contains(entityFilter, entity)) {
            template.entityFilter.clear();
            _.each(entityFilter, (_type) => {
                if (entity !== _type) {
                    template.entityFilter.push(_type);
                }
            });
        } else {
            template.entityFilter.push(entity);
        }
    }
});

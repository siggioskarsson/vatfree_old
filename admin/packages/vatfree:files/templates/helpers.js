/* global fileHelpers: true */

fileHelpers = {
    getFiles() {
        let template = Template.instance();
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }
        if (template.selectorFunction) {
            template.selectorFunction(selector);
        }

        return Files.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    }
};

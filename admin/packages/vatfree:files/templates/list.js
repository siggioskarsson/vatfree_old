Template.filesList.helpers(Vatfree.templateHelpers.helpers);
Template.filesList.helpers(fileHelpers);
Template.filesList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/file/:itemId', {itemId: this._id});
    }
});

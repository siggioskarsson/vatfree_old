UI.registerHelper('getFileName', function(fileId) {
    if (!fileId) fileId = this.fileId;

    let file = Files.findOne({_id: fileId});
    if (file) {
        return file.name;
    }

    return "";
});

UI.registerHelper('getFileCode', function(fileId) {
    if (!fileId) fileId = this.fileId;

    let file = Files.findOne({_id: fileId});
    if (file) {
        return file.code;
    }

    return "";
});

UI.registerHelper('getFileSymbol', function(fileId) {
    if (!fileId) fileId = this.fileId;

    let file = Files.findOne({_id: fileId});
    if (file) {
        return file.symbol;
    }

    return "";
});

UI.registerHelper('getFiles', function() {
    return Files.find({}, {
        sort: {
            name: 1
        }
    });
});

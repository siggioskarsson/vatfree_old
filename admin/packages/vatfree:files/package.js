Package.describe({
    name: 'vatfree:files',
    summary: 'Vatfree files package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'flemay:less-autoprefixer@1.2.0',
        'manuel:reactivearray@1.0.5',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core'
    ]);

    // shared files
    api.addFiles([
    ]);

    // server files
    api.addFiles([
        'publish/files.js',
        'publish/file.js',
        'server/import.js',
        'server/methods.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/files.less',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/files.html',
        'templates/files.js'
    ], 'client');

    api.export([
    ]);
});

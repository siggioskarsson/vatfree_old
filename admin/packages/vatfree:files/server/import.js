/* global Vatfree: true */
const csv = require('csvtojson');
const AWS = require('aws-sdk');
import { getReceiptImages } from 'meteor/vatfree:receipts/server/lib/import-downloads';
import mime from 'mime-types';

const DEBUGGING = !!Meteor.settings.DEBUGGING;

const colMapping = {
    'Id': 'salesforceId',
    'IsDeleted': 'deleted',
    'ParentId': 'linkId',
    'AccountId': false,
    'Name': 'name',
    'IsPrivate': 'private',
    'ContentType': 'type',
    'BodyLength': 'size',
    'BodyLengthCompressed': 'compressed',
    'OwnerId': 'ownerId',
    'CreatedDate': 'createdAt',
    'CreatedById': 'createdBy',
    'LastModifiedDate': 'updatedAt',
    'LastModifiedById': 'updatedBy',
    'SystemModstamp': 'moddedAt',
    'Description': 'notes',
    'FeedItemId': 'feedId'
};

const convert = {
    'createdAt': 'date',
    'updatedAt': 'date',
    'moddedAt': 'date',
};

const getExistingFile = function (updateData) {
    let target = updateData['target'].replace('_internal', '');
    let targets = [target, target + '_internal', target + '_trash'];
    return Files.findOne({
        target: { $in: targets },
        'original.name': updateData['name'],
        check: true,
        $or: [
            {
                'linkId': updateData['linkId']
            },
            {
                'salesforceId': updateData['salesforceId']
            },
            {
                itemId: updateData['itemId']
            }
        ]
    });
};

export const getS3FileAsync = function (fileKey, fileName, contextIds, target, type, callback) {
    if (!type) {
        type = mime.lookup(fileName) || "application/octet-stream";
    }
    if (DEBUGGING) console.log('getS3File', fileKey, fileName, contextIds, target, type);
    AWS.config.update({
        accessKeyId: Meteor.settings.AWSSF.accessKeyId,
        secretAccessKey: Meteor.settings.AWSSF.secretAccessKey,
        region: Meteor.settings.AWSSF.region,
        bucket: 'vatfree-salesforce',
    });
    let s3 = new AWS.S3();
    let options = {
        Bucket: 'vatfree-salesforce',
        Key: 'Attachments/' + fileKey
    };
    s3.getObject(options, Meteor.bindEnvironment(function (err, data) {
        if (err) {
            let error = JSON.parse(JSON.stringify(err));
            console.error(error);
            if (callback) callback(error);
        } else {
            let file = new FS.File();
            file.name(fileName);
            file.createdAt = new Date();
            file.originalName = fileName;
            file.check = true; // add flag to check files
            _.each(contextIds, (contextValue, contextKey) => {
                file[contextKey] = contextValue;
            });
            file.target = target;
            file.attachData(data.Body, {type: type}, (err) => {
                if (err) {
                    console.error(err);
                    if (callback) callback(err);
                } else {
                    let updateData = {
                        linkId: contextIds['linkId'],
                        salesforceId: contextIds['salesforceId'],
                        itemId: contextIds['itemId'],
                        name: fileName,
                        target: target
                    };
                    let existingFile = getExistingFile(updateData);
                    if (existingFile) {
                        if (DEBUGGING) console.log('FILE IMPORT: Found existing file', existingFile._id);
                        Files.update({
                            _id: existingFile._id
                        },{
                            $set: {
                                target: 'failed',
                                previousTarget: existingFile.target,
                                check: true
                            }
                        });
                    }
                    if (DEBUGGING) console.log('FILE IMPORT: Inserting file', file.target, file.linkId, file.salesforceId, file.itemId);
                    Files.insert(file, (err, fileObj) => {
                        if (err) {
                            callback(err);
                        } else {
                            callback(null, fileObj._id);
                        }
                    });
                }
            });
        }
    }));
};
export const getS3File = Meteor.wrapAsync(getS3FileAsync);

const cleanupUpdateData = function (updateData, links) {
    let link = links[updateData['linkId'].substr(0, 15)];
    if (!link) {
        console.error('Could not find parent item for', updateData['salesforceId'], updateData['linkId']);

        updateData['target'] = 'files';
        updateData['contextIds'] = {
            linkId: updateData['linkId'],
            salesforceId: updateData['salesforceId']
        };
        return updateData;
    }

    updateData['target'] = link.type + '_internal';
    updateData['salesforceId'] = updateData['salesforceId'];
    switch(link.type) {
        case 'travellers':
            updateData['contextIds'] = {
                itemId: link._id,
                userId: link._id
            };
            break;
        case 'receipts':
            updateData['contextIds'] = {
                itemId: link._id,
                receiptId: link._id
            };
            break;
        case 'invoices':
            updateData['target'] = link.type;
            updateData['contextIds'] = {
                itemId: link._id,
                invoiceId: link._id
            };
            break;
        case 'shops':
            updateData['target'] = link.type;
            updateData['contextIds'] = {
                itemId: link._id,
                shopId: link._id
            };
            break;
        case 'companies':
            updateData['target'] = link.type;
            updateData['contextIds'] = {
                itemId: link._id,
                companyId: link._id
            };
            break;
        case 'contacts':
            updateData['target'] = link.type;
            updateData['contextIds'] = {
                itemId: link._id,
                contactId: link._id
            };
            break;
    }
    updateData['contextIds'].linkId = updateData['linkId'];
    updateData['contextIds'].salesforceId = updateData['salesforceId'];

    return updateData;
};

const saveFile = function (updateData) {
    let file = getExistingFile(updateData);

    if (file && file.copies) {
        // do nothing, already imported
        console.info('FILE IMPORT: Skipping file, already uploaded', file._id);

        if (!file.salesforceId) {
            Files.files.direct.update({
                _id: file._id
            }, {
                $set: {
                    salesforceId: updateData['salesforceId']
                }
            });
        }
        return file._id;
    } else {
        let job = new Job(importFiles, 's3ImportDownloads', updateData);
        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 1
            })
            .save();
    }
};

Vatfree['import-vatfree-files'] = function (importFile, fileObject, callback) {
    Vatfree.setImportFileProgress(10, 'Populating lookup data', fileObject);

    // traveller / receipt / shop / company / invoice / contacts ?
    const links = {};
    Meteor.users.find({'roles.__global_roles__': 'traveller', 'profile.contactId': {$exists: true}},{fields: {'profile.contactId': 1}}).forEach((item) => {
        links[item.profile.contactId] = {
            _id: item._id,
            type: 'travellers'
        };
    });
    Receipts.find({salesforceId: {$exists: true}},{fields: {salesforceId: 1}}).forEach((item) => {
        links[item.salesforceId] = {
            _id: item._id,
            type: 'receipts'
        };
    });
    Invoices.find({salesforceId: {$exists: true}},{fields: {salesforceId: 1}}).forEach((item) => {
        links[item.salesforceId] = {
            _id: item._id,
            type: 'invoices'
        };
    });
    Shops.find({id: {$exists: true}},{fields: {id: 1}}).forEach((item) => {
        links[item.id] = {
            _id: item._id,
            type: 'shops'
        };
    });
    Companies.find({id: {$exists: true}},{fields: {id: 1}}).forEach((item) => {
        links[item.id] = {
            _id: item._id,
            type: 'companies'
        };
    });
    Contacts.find({'roles.__global_roles__': 'contact', 'profile.contactId': {$exists: true}},{fields: {'profile.contactId': 1}}).forEach((item) => {
        links[item.profile.contactId] = {
            _id: item._id,
            type: 'contacts'
        };
    });

    let totalLines = Vatfree.importFileLines(importFile);
    let lines = 0;
    Vatfree.setImportFileProgress(12, 'Initializing csv', fileObject);
    console.log('Starting csv import for', importFile);
    csv({
        delimiter: ',',
        noheader: false,
        trim: true,
        flatKeys: true,
        constructResult: false
    }).fromFile(importFile)
        .on('json', Meteor.bindEnvironment((row) => {
            lines++;
            if (row && _.size(row) > 2) {
                if (lines % 100 === 0) {
                    console.log('progress', 12 + Math.round(89 * (lines / totalLines)), 'Importing ' + lines + ' of ' + totalLines);
                    Vatfree.setImportFileProgress(12 + Math.round(89 * (lines / totalLines)), 'Importing ' + lines + ' of ' + totalLines, fileObject);
                }

                let updateData = Vatfree.import.processColumns(colMapping, convert, row);
                cleanupUpdateData(updateData, links);
                if (updateData['target']) {
                    saveFile(updateData);
                }
            }
        }))
        .on('done', Meteor.bindEnvironment((error) => {
            console.log('CSV import done', importFile);
            Vatfree.setImportFileProgress(100, 'Done', fileObject);
            if (callback) {
                callback();
            }
        }));
};

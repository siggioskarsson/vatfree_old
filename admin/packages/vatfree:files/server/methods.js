import { Meteor } from "meteor/meteor";

Meteor.methods({
    'fix-file-itemIds'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Files.find({
            itemId: {
                $exists: false
            }
        }).forEach((file) => {
            let itemId = (file.userId || '') +
                (file.shopId || '') +
                (file.companyId || '') +
                (file.receiptId || '') +
                (file.invoiceId || '') +
                (file.paymentId || '') +
                (file.payoutId || '') +
                (file.contactId || '') +
                (file.affiliateId || '') +
                (file.activityId || '');

            Files.update({
                _id: file._id
            },{
                $set: {
                    itemId: itemId
                }
            });
        });
    }
});

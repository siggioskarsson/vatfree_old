import './add.html';

Template.addPartnerModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-partner').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-partner')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-partner').on('shown.bs.modal', function () {
            //
        });
        $('#modal-add-partner').modal('show');
    });
});

Template.addPartnerModal.onDestroyed(function() {
    this.hideModal();

    $('body').removeClass('modal-open');
    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});


Template.addPartnerModal.events({
    'click .cancel-add-partner'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-partner-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        Meteor.call('partners-insert', formData.profile, (err, contactId) => {
            if (err) {
                toastr.error(err.message);
            } else {
                template.hideModal();
                FlowRouter.go('/partner/' + contactId);
            }
        });
    }
});

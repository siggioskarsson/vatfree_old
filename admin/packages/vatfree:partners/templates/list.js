import { partnerHelpers } from './helpers';
import './list.html';

Template.partnersList.helpers(Vatfree.templateHelpers.helpers);
Template.partnersList.helpers(partnerHelpers);
Template.partnersList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/partner/:itemId', {itemId: this._id});
    }
});

import { partnerHelpers } from './helpers';
import './partners.html';

Template.partners.onCreated(Vatfree.templateHelpers.onCreated(Partners, '/partners/', '/partner/:itemId'));
Template.partners.onRendered(Vatfree.templateHelpers.onRendered());
Template.partners.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.partners.helpers(Vatfree.templateHelpers.helpers);
Template.partners.helpers(partnerHelpers);
Template.partners.events(Vatfree.templateHelpers.events());

Template.partners.onCreated(function() {
    this.statusAttribute = 'partner.status';
    this.todoStatus = [
        'new'
    ];
});

import { partnerHelpers } from './helpers';
import './info.html';

Template.partnerInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.partnerInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {partnerId: data._id}, null, limit, offset);
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.partnerInfo.helpers(partnerHelpers);
Template.partnerInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            $or: [
                {
                    'securityContext.partnerId': this._id
                },
                {
                    'activity.target.itemId': this._id
                }
            ]
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

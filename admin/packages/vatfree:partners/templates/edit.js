import './edit.html';

Template.partnerEdit.onCreated(function () {

});

Template.partnerEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('partners_item', FlowRouter.getParam('partnerId') || '');
    });
});

Template.partnerEdit.helpers({
    partnerDoc() {
        return Partners.findOne({
            _id: FlowRouter.getParam('partnerId')
        });
    },
    getBreadcrumbTitle() {
        return this.profile.name + ', ' + this.profile.addressFirst + ', ' + this.profile.postalCode + ' ' + this.profile.city;
    }
});

Template.partnerEditForm.onCreated(function () {
    this.addingReceipt = new ReactiveVar();
    this.addingInvoice = new ReactiveVar();
    this.addingContact = new ReactiveVar();
    this.addingActivityLog = new ReactiveVar();
});

Template.partnerEditForm.events(Vatfree.templateHelpers.events());
Template.partnerEditForm.onRendered(function () {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('partner-receipts', data._id);
        this.subscribe('partner-payments', data._id);
        this.subscribe('activity-logs', "", {partnerId: data._id});
    });

    let formatPartnershipStatus = function (status) {
        let button = '';
        switch (status.id) {
            case 'pledger':
                button = 'VFCircleBlue.svg';
                break;
            case 'partner':
                button = 'VFCircleShade.svg';
                break;
            case 'uncooperative':
            default:
                button = 'VFCircleNonrefund.svg';
                break;
        }

        if (button) {
            return $(
                '<span class="select2-item"><img src="/Logos/' + button + '" class="img-responsive" /> ' + status.text + '</span>'
            );
        } else {
            return status.text;
        }
    };
    Tracker.afterFlush(() => {
        this.$('select[name="partnershipStatus"]').select2({
            templateSelection: formatPartnershipStatus,
            templateResult: formatPartnershipStatus
        });

        this.$('select[name="countryId"]').select2();
    });
});

Template.partnerEditForm.helpers({
    isDisabled() {
        return !Vatfree.userIsInRole(Meteor.userId(), 'partners-update');
    },
    addingReceipt() {
        return Template.instance().addingReceipt.get();
    },
    addingInvoice() {
        return Template.instance().addingInvoice.get();
    },
    addingContact() {
        return Template.instance().addingContact.get();
    },
    getReceipts() {
        return Receipts.find({
            partnerId: this._id
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getInvoices() {
        return Invoices.find({
            partnerId: this._id
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getContacts() {
        return Contacts.find({
            'profile.partnerId': this._id
        },{
            sort: {
                'profile.name': 1
            }
        });
    },
    isCountrySelected() {
        let template = Template.instance();
        return template.data.profile.countryId === this._id;
    },
    addingActivityLog() {
        return Template.instance().addingActivityLog.get();
    },
    getActivityLog() {
        return ActivityLogs.find({
            partnerId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getFileContext() {
        return {
            itemId: this._id,
            target: 'partners'
        };
    },
    getStandardServiceFee() {
        return Vatfree.partners.defaultServiceFee;
    },
    getStandardDiscount() {
        return Vatfree.partners.defaultTravellerDiscount;
    }
});

Template.partnerEditForm.events({
    'click .add-activity-log'(e, template) {
        e.preventDefault();
        template.addingActivityLog.set(true);
    },
    'click .add-contact'(e, template) {
        e.preventDefault();
        template.addingContact.set(true);
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/partners');
    },
    'submit form[name="partner-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (this.profile.email && formData.profile.email !== this.profile.email) {
            if (!confirm("Email address has been changed. Are you sure you want to continue. The partner will get an email te verify the new email.")) {
                return false;
            }
        }

        const unsetData = {};
        if (_.has(formData.partner, 'serviceFee')) {
            if (formData.partner.serviceFee) {
                formData.partner.serviceFee = Math.round(formData.partner.serviceFee);
            } else {
                unsetData['partner.serviceFee'] = true;
            }
        }
        if (_.has(formData.partner, 'travellerDiscount')) {
            if (formData.partner.travellerDiscount) {
                formData.partner.travellerDiscount = Math.round(formData.partner.travellerDiscount);
            } else {
                unsetData['partner.travellerDiscount'] = true;

            }
        }

        let setData = {
            $set: formData
        };

        if (_.size(unsetData)) {
            setData['$unset'] = unsetData;
        }

        Meteor.call('partners-update', this._id, setData, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Partner saved!')
            }
        });
    },
    'click .add-receipt'(e, template) {
        e.preventDefault();
        template.addingReceipt.set(true);
    },
    'click .add-invoice'(e, template) {
        e.preventDefault();
        template.addingInvoice.set(true);
    }
});

UI.registerHelper('getPartnerDescription', function(partnerId) {
    if (!partnerId) partnerId = this.partnerId;

    let partner = Partners.findOne({_id: partnerId});
    if (partner) {
        let country = Countries.findOne({_id: partner.profile.countryId}) || {};
        return partner.profile.name + ' (' + partner.partner.code + ') - ' + partner.profile.addressFirst + ', ' + partner.profile.postalCode + ' ' + partner.profile.city + ', ' + country.name;
    }

    return "";
});

UI.registerHelper('getPartnerName', function(partnerId) {
    if (!partnerId) partnerId = this.partnerId;

    let partner = Partners.findOne({_id: partnerId});
    if (partner) {
        return partner.profile.name;
    }

    return "";
});

UI.registerHelper('getPartnerStatusIcon', function(status) {
    let icon = Vatfree.partners.icons();
    return _.has(icon, status) ? icon[status] : "circle-o";
});

UI.registerHelper('getPartnerStatusOptions', function() {
    return _.clone(Meteor.users.simpleSchema()._schema['partner.status'].allowedValues);
});

Meteor.publishComposite('partners_item', function(partnerId) {
    check(partnerId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        collectionName: 'partners',
        find: function () {
            return Partners.find({
                _id: partnerId,
                'roles.__global_roles__': 'partner'
            });
        },
        children: [
            {
                collectionName: 'contacts',
                find: function (item) {
                    return Contacts.find({
                        'profile.partnerId': item._id
                    });
                }
            },
            {
                find: function (item) {
                    return Files.find({
                        itemId: item._id,
                        target: 'partners'
                    });
                }
            }
        ]
    };
});

Package.describe({
    name: 'vatfree:partners',
    summary: 'Vatfree partners package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core'
    ]);

    // shared files
    api.addFiles([
        'lib/partners.js'
    ]);

    // server files
    api.addFiles([
        'server/partners.js',
        'server/methods.js',
        'server/notify.js',
        'publish/partners.js',
        'publish/partner.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'client/lib/partners.js',
        'templates/ui-helpers.js'
    ], 'client');

    api.export([
        'Partners'
    ]);
});

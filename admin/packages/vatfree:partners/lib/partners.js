Vatfree.partners = {};

Vatfree.partners.defaultServiceFee = 10;
Vatfree.partners.defaultTravellerDiscount = 10;

Vatfree.partners.icons = function() {
    return {
        'new': 'icon icon-new',
        'active': 'icon icon-refcompetitor',
        'silver': 'icon icon-refcompetitor',
        'gold': 'icon icon-refcompetitor'
    };
};

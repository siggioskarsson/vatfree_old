Partners = Meteor.users;
Partners._ensureIndex({'partner.status': 1});

// Add index to activity stream collection for partners
ActivityStream.collection._ensureIndex({'securityContext.partnerId': 1});

Partners.after.insert(function (userId, doc) {
    Meteor.defer(() => {
        if (_.contains(doc.roles.__global_roles__, 'partner')) {
            Vatfree.notify.processNotificationTriggers(userId, 'partners', '- insert -', false, doc._id);
        }
    });
});

Partners.after.update(function (userId, doc, fieldNames, modifier, options) {
    if (_.contains(doc.roles.__global_roles__, 'partner')) {
        Meteor.defer(() => {
            if (options.notify !== false) {
                if (_.contains(fieldNames, 'partner') && modifier.$set && modifier.$set['partner.status'] && modifier.$set['partner.status'] !== this.previous.partner.status) {
                    Vatfree.notify.processNotificationTriggers(userId, 'partners', modifier.$set['partner.status'], this.previous.partner.status, doc._id);
                }
            }
        });
    }
});

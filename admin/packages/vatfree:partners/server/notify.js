Vatfree.notify.sendStatusNotification['partners'] = function (userId, partnerId, partnerStatusNotify) {
    try {
        let partner = Partners.findOne({_id: partnerId}) || {};
        let emailLanguage = partner.profile.language || "en";

        let country = Countries.findOne({_id: partner.profile.countryId }) || {};

        let activityIds = {
            travellerId: partnerId
        };

        let partnerInfo = {...partner.profile, ...partner.partner};
        partner.profile.country = country;
        let templateData = {
            language: emailLanguage,
            partner: partnerInfo
        };

        let toAddress = partner.profile.email;
        let fromAddress = 'Vatfree.com partner program <partner@vatfree.com>';

        Vatfree.notify.sendNotification(partnerStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage, partner);
    } catch (e) {
        ActivityLogs.insert({
            type: 'task',
            userId: userId,
            subject: 'Sending of status update to partner failed',
            status: 'new',
            partnerId: partnerId,
            notes: "Error: " + e.message
        });
    }
};

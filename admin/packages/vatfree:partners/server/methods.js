import { Meteor } from "meteor/meteor";

const getNewPartnerCode = function(length) {
    let code = "";
    let chars = "abcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < length; i++) {
        code += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return code;
};

Meteor.methods({
    'search-partners'(searchTerm, limit, offset) {
        this.unblock();
        if (!Vatfree.userIsInRole(this.userId, 'partners-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {};
        selector['roles.__global_roles__'] = 'partner';
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let partners = [];
        Partners.find(selector, {
            sort: {
                'profile.name': 1
            },
            limit: limit,
            offset: offset
        }).forEach((partner) => {
            if (!partner.partner) partner.partner = {};
            partners.push({
                text: partner.profile.name + ' (' + partner.partner.code + ') - ' + (partner.profile.addressFirst || "") + ', ' + (partner.profile.postCode || "") + ' ' + (partner.profile.city || ""),
                id: partner._id
            });
        });

        return partners;
    },
    'partners-insert'(profile) {
        if (!Vatfree.userIsInRole(this.userId, 'partners-create')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const existingUser = Meteor.users.findOne({
            'emails.address': profile.email
        });

        if (existingUser) {
            Meteor.users.update({
                _id: existingUser._id
            }, {
                $set: {
                    partner: {
                        code: getNewPartnerCode(7),
                        status: 'new'
                    },
                },
                $addToSet: {
                    'roles.__global_roles__': 'partner'
                }
            });

            return existingUser._id;
        } else {
            let privateData = {};
            if (profile.notes) {
                privateData.notes = profile.notes;
                delete profile.notes;
            }
            return Meteor.users.insert({
                profile: profile,
                private: privateData,
                partner: {
                    code: getNewPartnerCode(7),
                    status: 'new'
                },
                emails: [
                    {
                        address: profile.email,
                        verified: false
                    }
                ],
                roles: {
                    __global_roles__: ['partner']
                }
            });
        }
    },
    'partners-update'(partnerId, profileData) {
        if (!Vatfree.userIsInRole(this.userId, 'partners-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const partner = Meteor.users.findOne({_id: partnerId});

        const setData = {};
        _.each(profileData['$set'], (values, docKey) => {
            _.each(values, (value, key) => {
                setData[docKey + '.' + key] = value;
            });
        });
        if (setData['profile.notes']) {
            setData['private.notes'] = setData['profile.notes'];
            delete setData['profile.notes'];
        }

        const unsetData = {};
        _.each(profileData['$unset'], (values, docKey) => {
            _.each(values, (value, key) => {
                unsetData[docKey + '.' + key] = value;
            });
        });

        if (setData['profile.email'] && setData['profile.email'] !== partner.profile.email) {
            const existingUser = Meteor.users.findOne({
                'emails.address': setData['profile.email']
            });
            if (existingUser) {
                throw new Meteor.Error(500, "Email address is alread registered for another user.");
            }

            setData['emails'] = [
                {
                    address: setData['profile.email'],
                    verified: false
                }
            ];
        }

        return Contacts.update({
            _id: partnerId
        }, {
            $set: setData,
            $unset: unsetData
        });
    }
});

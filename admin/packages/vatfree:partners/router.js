FlowRouter.route('/partners', {
    action: function() {
        import("meteor/vatfree:partners/templates")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "partners"});
            });
    }
});

FlowRouter.route('/partner/:partnerId', {
    action: function() {
        import("meteor/vatfree:partners/templates")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "partnerEdit"});
            });
    }
});

import moment from 'moment';
import StubCollections from 'meteor/hwillson:stub-collections';
import { getSetDataForPaymentForInvoice } from '../../server/lib/payments';
import { payment } from '../data/payments';
import { invoice } from '../data/invoices';

let setInvoicePaid = function (invoice) {
    const existingInvoice = JSON.parse(JSON.stringify(invoice));
    existingInvoice.status = 'paid';
    existingInvoice.paidAt = moment().subtract(7, 'days').toDate();
    existingInvoice.paymentDifference = 0;
    existingInvoice.paymentIds = [
        "jo7z5KvMyvAg5eMtv"
    ];
    existingInvoice.invoiceAmounts = [
        {
            invoiceId: 'zcDgspSZzAHDydCG8',
            amount: 61562
        }
    ];

    return existingInvoice;
};

let setInvoicePaidBack = function (invoice) {
    const existingInvoice = JSON.parse(JSON.stringify(invoice));
    existingInvoice.status = 'paymentDifference';
    existingInvoice.paidAt = moment().subtract(7, 'days').toDate();
    existingInvoice.paymentDifference = -61562;
    existingInvoice.paymentIds = [
        "jo7z5KvMyvAg5eMtv"
    ];
    existingInvoice.invoiceAmounts = [
        {
            invoiceId: 'zcDgspSZzAHDydCG8',
            amount: -61562
        }
    ];

    return existingInvoice;
};

describe('getSetDataForPaymentForInvoice', function () {
    it('payment full', function (done) {
        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: 61562
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(invoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('paid');
        expect(setData.paymentDifference).to.be.equal(0);
        expect(setData.paidAt).to.be.a('date');

        done();
    });

    it('payment not enough', function (done) {
        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: 61500
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(invoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('partiallyPaid');
        expect(setData.paymentDifference).to.be.equal(-62);
        expect(setData.paidAt).to.be.a('date');

        done();
    });

    it('payment too much', function (done) {
        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: 61962
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(invoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('paymentDifference');
        expect(setData.paymentDifference).to.be.equal(400);
        expect(setData.paidAt).to.be.a('date');

        done();
    });
});

describe('getSetDataForPaymentForInvoice second payment', function () {
    beforeEach(function () {
        StubCollections.add([Payments]);
        StubCollections.stub();

        Payments.insert(payment);
    });

    afterEach(function () {
        StubCollections.restore();
    });

    it('payment too much', function (done) {
        const existingInvoice = setInvoicePaid(invoice);

        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: 300
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(existingInvoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('paymentDifference');
        expect(setData.paymentDifference).to.be.equal(300);
        expect(setData.paidAt).to.be.a('date');

        done();
    });

    it('payment small pay back', function (done) {
        const existingInvoice = setInvoicePaid(invoice);

        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: -300
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(existingInvoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('partiallyPaid');
        expect(setData.paymentDifference).to.be.equal(-300);
        expect(setData.paidAt).to.be.a('date');

        done();
    });

    it('payment full pay back', function (done) {
        const existingInvoice = setInvoicePaid(invoice);

        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: -61562
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(existingInvoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('sentToRetailer');
        expect(setData.paymentDifference).to.be.equal(0);
        expect(setData.paidAt).to.be.equal(null);

        done();
    });
});

describe('getSetDataForPaymentForInvoice second payment - first negative', function () {
    beforeEach(function () {
        StubCollections.add([Payments]);
        StubCollections.stub();

        const newPayment = JSON.parse(JSON.stringify(payment));
        // overwrite invoice payments, we are only testing this 1
        newPayment.invoiceAmounts = [
            {
                "invoiceId": "zcDgspSZzAHDydCG8",
                "amount": -61562
            }
        ];

        Payments.insert(newPayment);
    });

    afterEach(function () {
        StubCollections.restore();
    });

    it('payment too little', function (done) {
        const existingInvoice = setInvoicePaidBack(invoice);

        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: 300
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(existingInvoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('partiallyPaid');
        expect(setData.paymentDifference).to.be.equal(-122824);
        expect(setData.paidAt).to.be.a('date');

        done();
    });

    it('payment small pay back', function (done) {
        const existingInvoice = setInvoicePaidBack(invoice);

        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: -300
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(existingInvoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('partiallyPaid');
        expect(setData.paymentDifference).to.be.equal(-123424);
        expect(setData.paidAt).to.be.a('date');

        done();
    });

    it('payment payment', function (done) {
        const existingInvoice = setInvoicePaidBack(invoice);

        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: 61562
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(existingInvoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('sentToRetailer');
        expect(setData.paymentDifference).to.be.equal(0);
        expect(setData.paidAt).to.be.equal(null);

        done();
    });

    it('payment full payment', function (done) {
        const existingInvoice = setInvoicePaidBack(invoice);

        const paidAt = new Date();
        const invoiceAmounts = [
            {
                invoiceId: "zcDgspSZzAHDydCG8",
                amount: 61562*2
            }
        ];
        const euroInvoiceAmounts = {};
        const setData = getSetDataForPaymentForInvoice(existingInvoice, invoiceAmounts, euroInvoiceAmounts, paidAt);

        expect(setData).to.be.a('object');
        expect(setData.status).to.be.equal('paid');
        expect(setData.paymentDifference).to.be.equal(0);
        expect(setData.paidAt).to.be.a('date');

        done();
    });
});

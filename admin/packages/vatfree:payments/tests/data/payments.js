export const payment = {
    "_id": "jo7z5KvMyvAg5eMtv",
    "transactionId": "ffdd6a69c049f2bb73b8fc192a82f57881f9da621dba47035a88a317e1edc370",
    "accountId": "486111725",
    "amount": 1787855,
    "closingBalance": 2395709,
    "closingBalanceDate": new Date("2019-05-02T00:00:00+0200"),
    "code": "N247",
    "createdAt": new Date(1556879850585),
    "createdBy": "eLpZB6st5A5y6fvrT",
    "createdMonth": 5,
    "createdWeek": 201918,
    "createdYear": 2019,
    "currency": "EUR",
    "currencyId": "NYG8kkBuAzp8ph6Kn",
    "description": "/TRTP/SEPA INCASSO BATCH/PREF/INC201900008/NRTX/0000078/PIND/BRUTO/",
    "entryDate": new Date("2019-05-02T00:00:00+0200"),
    "iterator": 45,
    "number": "12201",
    "openingBalance": 0,
    "openingBalanceDate": new Date("2019-05-01T00:00:00+0200"),
    "referenceNumber": "ABN AMRO BANK NV",
    "valueDate": new Date("2019-05-02T00:00:00+0200"),
    "valueMonth": 5,
    "valueWeek": 201918,
    "valueYear": 2019,
    "euroInvoiceAmounts": [],
    "invoiceAmounts": [
        {
            "invoiceId": "FD5cpdGpRHYtKdhQu",
            "amount": 9978
        },
        {
            "invoiceId": "KHaP3B9sGYmTtw4Az",
            "amount": 1700
        },
        {
            "invoiceId": "zcDgspSZzAHDydCG8",
            "amount": 61562
        },
        {
            "invoiceId": "fkbrK5QaNP4cn5r8f",
            "amount": 2751
        },
        {
            "invoiceId": "vHKeewdjkXf9GpYnE",
            "amount": 6833
        },
        {
            "invoiceId": "CvmSdRLh8BJ6zSYZ4",
            "amount": 28010
        },
        {
            "invoiceId": "danCu4baqioaQX9TM",
            "amount": 583973
        },
        {
            "invoiceId": "7vLT6pPgGSfFKGnyC",
            "amount": 1718
        },
        {
            "invoiceId": "uG2rLbWGM5jZi8YXp",
            "amount": 27751
        },
        {
            "invoiceId": "CPBd9GSvrjPwqp6tF",
            "amount": 26104
        },
        {
            "invoiceId": "mybY5gwRzRyBA8MBW",
            "amount": 88375
        },
        {
            "invoiceId": "iAHk7mrqm7NN6Fk59",
            "amount": 7115
        },
        {
            "invoiceId": "oh4SWfhvMjDRFMHaw",
            "amount": 31204
        },
        {
            "invoiceId": "ue4YffXkxaiJi7ZfH",
            "amount": 1736
        },
        {
            "invoiceId": "2PTSDmRX2MTAjP55q",
            "amount": 30666
        },
        {
            "invoiceId": "uyj25d9Ete3bBCraC",
            "amount": 2603
        },
        {
            "invoiceId": "eH2gTXYNzcd36FkNu",
            "amount": 24054
        },
        {
            "invoiceId": "CMSghmxnJsfcYwb8n",
            "amount": 7952
        },
        {
            "invoiceId": "MmfeDNdBxnLohuaJG",
            "amount": 46982
        },
        {
            "invoiceId": "sYKSe4odzdb4ehTfh",
            "amount": 2864
        },
        {
            "invoiceId": "Lky8EgxTciFPvWWfi",
            "amount": 1908
        },
        {
            "invoiceId": "KXMvgJKnE6BzthRdx",
            "amount": 7809
        },
        {
            "invoiceId": "ovQjygRYiTJkgvvj8",
            "amount": 1128
        },
        {
            "invoiceId": "TmT8CMWsmxKv39Cfv",
            "amount": 3419
        },
        {
            "invoiceId": "cWMgHiqBmsA7M4ERh",
            "amount": 62021
        },
        {
            "invoiceId": "EZEYLfsk97AWYYHQe",
            "amount": 5203
        },
        {
            "invoiceId": "PR9HdAokAvaG4FyaL",
            "amount": 13261
        },
        {
            "invoiceId": "LaNTejQ4gfnH6K4QT",
            "amount": 51822
        },
        {
            "invoiceId": "F9Fyj3KZRE2CXDAGf",
            "amount": 6930
        },
        {
            "invoiceId": "KT6ECGuLJCio5z6qP",
            "amount": 4512
        },
        {
            "invoiceId": "oaDhtDcdgzPoPKx2c",
            "amount": 2847
        },
        {
            "invoiceId": "JKhimFFpbCWhKBr3T",
            "amount": 24203
        },
        {
            "invoiceId": "AtefGCZYRzYeKxPFB",
            "amount": 9713
        },
        {
            "invoiceId": "yCFBEzc2ysjHeCs9E",
            "amount": 4511
        },
        {
            "invoiceId": "MjDaPoAE9Dt4sJmCt",
            "amount": 36268
        },
        {
            "invoiceId": "MbfpwA7qSGvp9Xh5q",
            "amount": 2637
        },
        {
            "invoiceId": "hQG86SdATBfRkNt97",
            "amount": 44658
        },
        {
            "invoiceId": "tFEoJMXAympaioSGZ",
            "amount": 17950
        },
        {
            "invoiceId": "LghnGZt8ed5QrPPLN",
            "amount": 6517
        },
        {
            "invoiceId": "PRExN8WRes9c7DFHj",
            "amount": 5206
        },
        {
            "invoiceId": "spxC8hsE4fYA693BT",
            "amount": 8795
        },
        {
            "invoiceId": "pM7E3MSJGKW99NHGE",
            "amount": 12643
        },
        {
            "invoiceId": "2nLc9pvsJEs74eDZW",
            "amount": 53379
        },
        {
            "invoiceId": "jWRJErWw5LiegZk5v",
            "amount": 3036
        },
        {
            "invoiceId": "dZRS3To7Pry4PddZj",
            "amount": 8574
        },
        {
            "invoiceId": "hMAXXjytQY7ugaWyy",
            "amount": 870
        },
        {
            "invoiceId": "hAJRgptHGXS2c8Hja",
            "amount": 11194
        },
        {
            "invoiceId": "y3kMuLomzK7W4Yvxp",
            "amount": 1128
        },
        {
            "invoiceId": "6kdgXjS328PjGmBLd",
            "amount": 5775
        },
        {
            "invoiceId": "sGstMDLhSrKm7xS2h",
            "amount": 6126
        },
        {
            "invoiceId": "v7uKT2J7tsiwqPeqa",
            "amount": 8674
        },
        {
            "invoiceId": "xsKb3ajFkz6QRTc7F",
            "amount": 4165
        },
        {
            "invoiceId": "JXbkPgz8AWBAhKgqR",
            "amount": 2933
        },
        {
            "invoiceId": "G4jqnDKWherqD3MYC",
            "amount": 45550
        },
        {
            "invoiceId": "3ruxpzEkKkXnY2nfs",
            "amount": 14344
        },
        {
            "invoiceId": "S6m9bwkybXead8AYF",
            "amount": 11757
        },
        {
            "invoiceId": "Rgfx2gohBFSQGKua9",
            "amount": 2459
        },
        {
            "invoiceId": "YF76kDBXtEWy93DPb",
            "amount": 6194
        },
        {
            "invoiceId": "sd5chgixqQ7RatWSx",
            "amount": 1320
        },
        {
            "invoiceId": "cPso2gviFES9jwmLq",
            "amount": 1102
        },
        {
            "invoiceId": "8pjhvGp99fAXiiJQ6",
            "amount": 13678
        },
        {
            "invoiceId": "rW8fqKchHdeTukDvk",
            "amount": 2039
        },
        {
            "invoiceId": "96rBB9LbihWaoBQmm",
            "amount": 3228
        },
        {
            "invoiceId": "wFxSMvHD9s5LRqiLw",
            "amount": 868
        },
        {
            "invoiceId": "Rra97epQkxNwzoJwF",
            "amount": 5698
        },
        {
            "invoiceId": "9nzFGSoewDgTmzfvr",
            "amount": 3195
        },
        {
            "invoiceId": "YJqdZq5ve87DpfZ8b",
            "amount": 4684
        },
        {
            "invoiceId": "q8EuJjmdkDF8P6BM6",
            "amount": 186906
        },
        {
            "invoiceId": "QozwjihomsJrcsaZF",
            "amount": 2082
        },
        {
            "invoiceId": "vtHCzWPDeDyNMaXQu",
            "amount": 1048
        },
        {
            "invoiceId": "2XYMycuSwZSnJ2Ydp",
            "amount": 30893
        },
        {
            "invoiceId": "gWeyfRwaW9Qw7bd2n",
            "amount": 1040
        },
        {
            "invoiceId": "wxaKqLseeM5pQgShk",
            "amount": 3796
        },
        {
            "invoiceId": "u2QwXbaALMvMDLFv4",
            "amount": 2256
        },
        {
            "invoiceId": "kymA2R9cvnjzbvGTZ",
            "amount": 4686
        },
        {
            "invoiceId": "vtbvoF5GqnQbrF6tZ",
            "amount": 3298
        },
        {
            "invoiceId": "MnwGbCCXtTZCE6pfj",
            "amount": 1467
        },
        {
            "invoiceId": "i68wBd9wqbGcaax3T",
            "amount": 521
        }
    ],
    "invoiceIds": [
        "FD5cpdGpRHYtKdhQu",
        "KHaP3B9sGYmTtw4Az",
        "zcDgspSZzAHDydCG8",
        "fkbrK5QaNP4cn5r8f",
        "vHKeewdjkXf9GpYnE",
        "CvmSdRLh8BJ6zSYZ4",
        "danCu4baqioaQX9TM",
        "7vLT6pPgGSfFKGnyC",
        "uG2rLbWGM5jZi8YXp",
        "CPBd9GSvrjPwqp6tF",
        "mybY5gwRzRyBA8MBW",
        "iAHk7mrqm7NN6Fk59",
        "oh4SWfhvMjDRFMHaw",
        "ue4YffXkxaiJi7ZfH",
        "2PTSDmRX2MTAjP55q",
        "uyj25d9Ete3bBCraC",
        "eH2gTXYNzcd36FkNu",
        "CMSghmxnJsfcYwb8n",
        "MmfeDNdBxnLohuaJG",
        "sYKSe4odzdb4ehTfh",
        "Lky8EgxTciFPvWWfi",
        "KXMvgJKnE6BzthRdx",
        "ovQjygRYiTJkgvvj8",
        "TmT8CMWsmxKv39Cfv",
        "cWMgHiqBmsA7M4ERh",
        "EZEYLfsk97AWYYHQe",
        "PR9HdAokAvaG4FyaL",
        "LaNTejQ4gfnH6K4QT",
        "F9Fyj3KZRE2CXDAGf",
        "KT6ECGuLJCio5z6qP",
        "oaDhtDcdgzPoPKx2c",
        "JKhimFFpbCWhKBr3T",
        "AtefGCZYRzYeKxPFB",
        "yCFBEzc2ysjHeCs9E",
        "MjDaPoAE9Dt4sJmCt",
        "MbfpwA7qSGvp9Xh5q",
        "hQG86SdATBfRkNt97",
        "tFEoJMXAympaioSGZ",
        "LghnGZt8ed5QrPPLN",
        "PRExN8WRes9c7DFHj",
        "spxC8hsE4fYA693BT",
        "pM7E3MSJGKW99NHGE",
        "2nLc9pvsJEs74eDZW",
        "jWRJErWw5LiegZk5v",
        "dZRS3To7Pry4PddZj",
        "hMAXXjytQY7ugaWyy",
        "hAJRgptHGXS2c8Hja",
        "y3kMuLomzK7W4Yvxp",
        "6kdgXjS328PjGmBLd",
        "sGstMDLhSrKm7xS2h",
        "v7uKT2J7tsiwqPeqa",
        "xsKb3ajFkz6QRTc7F",
        "JXbkPgz8AWBAhKgqR",
        "G4jqnDKWherqD3MYC",
        "3ruxpzEkKkXnY2nfs",
        "S6m9bwkybXead8AYF",
        "Rgfx2gohBFSQGKua9",
        "YF76kDBXtEWy93DPb",
        "sd5chgixqQ7RatWSx",
        "cPso2gviFES9jwmLq",
        "8pjhvGp99fAXiiJQ6",
        "rW8fqKchHdeTukDvk",
        "96rBB9LbihWaoBQmm",
        "wFxSMvHD9s5LRqiLw",
        "Rra97epQkxNwzoJwF",
        "9nzFGSoewDgTmzfvr",
        "YJqdZq5ve87DpfZ8b",
        "q8EuJjmdkDF8P6BM6",
        "QozwjihomsJrcsaZF",
        "vtHCzWPDeDyNMaXQu",
        "2XYMycuSwZSnJ2Ydp",
        "gWeyfRwaW9Qw7bd2n",
        "wxaKqLseeM5pQgShk",
        "u2QwXbaALMvMDLFv4",
        "kymA2R9cvnjzbvGTZ",
        "vtbvoF5GqnQbrF6tZ",
        "MnwGbCCXtTZCE6pfj",
        "i68wBd9wqbGcaax3T"
    ],
    "paymentDifference": 0,
    "updatedAt": new Date(1556882155912),
    "updatedBy": "eLpZB6st5A5y6fvrT"
};

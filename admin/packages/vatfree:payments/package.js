Package.describe({
    name: 'vatfree:payments',
    summary: 'Vatfree payments package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core',
        'vatfree:countries',
        'vatfree:email-templates',
        'vatfree:invoices'
    ]);

    api.addAssets([
    ], 'server');

    // shared files
    api.addFiles([
        'payments.js'
    ]);

    // server files
    api.addFiles([
        'server/lib/payments.js',
        'server/methods.js',
        'server/payments.js',
        'publish/payments.js',
        'publish/payment.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/payments.less',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/link-debt-collection.html',
        'templates/link-debt-collection.js',
        'templates/list.html',
        'templates/list.js',
        'templates/payments.html',
        'templates/payments.js',
        'templates/reconcile.html',
        'templates/reconcile.js'
    ], 'client');

    api.export([
        'Payments'
    ]);
});

Package.onTest(function(api) {
    api.use([
        'ecmascript',
        'http',
        'check',
        'underscore',
        'random',
        'accounts-password',
        'meteortesting:mocha',
        'practicalmeteor:sinon',
        'practicalmeteor:chai',
        'hwillson:stub-collections',
        'vatfree:core',
        'vatfree:payments',
    ]);

    api.addAssets([
    ], 'server');

    // data
    api.addFiles([
        'tests/data/payments.js',
        'tests/data/invoices.js'
    ], 'server');

    api.addFiles([
        'tests/lib/payments.js'
    ], 'server');

    api.export([
    ]);
});

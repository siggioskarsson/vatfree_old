Meteor.publishComposite('payments', function(searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        find: function () {
            return Payments.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                find: function (item) {
                    return Invoices.find({
                        _id: {
                            $in: item.invoiceIds || []
                        }
                    },{
                        fields: {
                            _id: 1,
                            invoiceNr: 1
                        }
                    });
                }
            }
        ]
    }
});

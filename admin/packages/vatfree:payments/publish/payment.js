Meteor.publishComposite('payments_item', function (itemId) {
    check(itemId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Payments.find({
                _id: itemId
            });
        },
        children: [
            {
                find: function (item) {
                    return Invoices.find({
                        _id: {
                            $in: item.invoiceIds || []
                        }
                    });
                }
            }
        ]
    }
});

import moment from 'moment';

Payments = new Meteor.Collection('payments');
let InvoiceAmounts = new SimpleSchema({
    invoiceId: {
        type: String,
        label: "The invoice id",
        optional: false
    },
    amount: {
        type: Number,
        label: "The amount paid for this invoice by this transaction",
        optional: false
    }
});
Payments.attachSchema(new SimpleSchema({
    accountId: {
        type: String,
        label: "Bank account the payment was made",
        optional: false
    },
    transactionId: {
        type: String,
        label: "Transaction id",
        optional: false
    },
    referenceNumber: {
        type: String,
        label: "Reference number of the payment",
        optional: false
    },
    number: {
        type: String,
        label: "Number of the payment",
        optional: false
    },
    iterator: {
        type: Number,
        label: "The iterator of the transaction within the payment from the bank",
        optional: false
    },
    openingBalance: {
        type: Number,
        label: "Opening balance of the account",
        optional: false
    },
    openingBalanceDate: {
        type: Date,
        label: "Date of the bank transfer",
        optional: false
    },
    closingBalance: {
        type: Number,
        label: "Cosing balance of the account",
        optional: false
    },
    closingBalanceDate: {
        type: Date,
        label: "Date of the bank transfer",
        optional: false
    },
    code: {
        type: String,
        label: "Code",
        optional: false
    },
    currency: {
        type: String,
        label: "Currency code",
        optional: false
    },
    currencyId: {
        type: String,
        label: "Currency id",
        optional: false
    },
    description: {
        type: String,
        label: "Description of the bank transfer",
        optional: false
    },
    accountNr: {
        type: String,
        label: "Account number of the account the transfer came from / went to",
        optional: true
    },
    amount: {
        type: Number,
        label: "Amount of the bank transfer",
        optional: false
    },
    valueDate: {
        type: Date,
        label: "Date of the bank transfer",
        optional: false
    },
    valueWeek: {
        type: Number,
        label: "Week of value date, used in stats",
        autoValue: function() {
            const valueDate = this.field('valueDate').value;
            let week = Number(moment(valueDate).format('GGGGWW'));
            if (this.isInsert) {
                return week;
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: week
                };
            } else {
                this.unset();
            }
        }
    },
    valueMonth: {
        type: Number,
        label: "Week of value date, used in stats",
        autoValue: function() {
            const valueDate = this.field('valueDate').value;
            let month = Number(moment(valueDate).format('MM'));
            if (this.isInsert) {
                return month;
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: month
                };
            } else {
                this.unset();
            }
        }
    },
    valueYear: {
        type: Number,
        label: "Week of value date, used in stats",
        autoValue: function() {
            const valueDate = this.field('valueDate').value;
            let year = Number(moment(valueDate).format('YYYY'));
            if (this.isInsert) {
                return year;
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: year
                };
            } else {
                this.unset();
            }
        }
    },
    entryDate: {
        type: Date,
        label: "Date of the bank transfer",
        optional: true
    },
    invoiceIds: {
        type: [String],
        label: "Invoices this payment is for",
        optional: true
    },
    invoiceAmounts: {
        type: [InvoiceAmounts],
        label: "Invoice amounts per invoice paid by this transaction",
        optional: true
    },
    euroInvoiceAmounts: {
        type: [InvoiceAmounts],
        label: "Invoice amounts per invoice paid by this transaction in euros",
        optional: true
    },
    paymentDifference: {
        type: Number,
        label: "Difference of payment and invoices",
        optional: true
    },
    ignorePayment: {
        type: Boolean,
        label: "Whether this payment is ignored and should not be linked to invoices",
        optional: true
    },
    notes: {
        type: String,
        label: "Notes on this Payment",
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
Payments.attachSchema(CreatedUpdatedSchema);

Payments.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return false;
    },
    remove: function (userId, doc) {
        return false;
    }
});

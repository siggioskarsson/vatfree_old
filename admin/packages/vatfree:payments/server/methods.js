import * as mt940 from 'mt940-js';
import moment from "moment";
import { SHA256 } from 'meteor/sha';

/* Better parser ?
var _mt940js = require('mt940js');
var mt940Parser  = new _mt940js.Parser();
*/

Meteor.methods({
    'get-payment'(paymentId) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Payments.findOne({_id: paymentId});
    },
    'search-payments'(searchTerm, limit, offset) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {};
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let payments = [];
        Payments.find(selector, {
            sort: {
                name: 1
            },
            limit: limit,
            offset: offset
        }).forEach((payment) => {
            payments.push({
                text: payment.name + ' - ' + payment.addressFirst + ', ' + payment.postalCode + ' ' + payment.city,
                id: payment._id
            });
        });

        return payments;
    },
    'upload-payment-files'(files) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        var fileBuffer = Buffer.from(files[0].content, 'utf8');

        /*
        let statements = mt940Parser.parse(fileBuffer);
        console.log(statements);
        return statements;
        */

        return processMT940(fileBuffer);
    },
    'link-payment-to-invoices'(paymentId, amounts, euroAmounts, paymentDifference) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let payment = Payments.findOne({_id: paymentId});
        if (!payment) {
            throw new Meteor.Error(500, 'Could not find payment');
        }

        let totalAmount = 0;
        let invoiceAmounts = [];
        let euroInvoiceAmounts = [];
        _.each(amounts, (amount, invoiceId) => {
            if (_.has(euroAmounts, invoiceId)) {
                totalAmount += euroAmounts[invoiceId];
                euroInvoiceAmounts.push({
                    invoiceId: invoiceId,
                    amount: euroAmounts[invoiceId]
                });
            } else {
                totalAmount += amount;
            }

            invoiceAmounts.push({
                invoiceId: invoiceId,
                amount: amount
            });
        });

        if (paymentDifference + totalAmount !== payment.amount) {
            throw new Meteor.Error(500, 'Payment amount does not match all invoice amounts');
        }

        Payments.update({
            _id: paymentId
        },{
            $set: {
                invoiceIds: _.keys(amounts),
                invoiceAmounts: invoiceAmounts,
                euroInvoiceAmounts: euroInvoiceAmounts,
                paymentDifference: paymentDifference
            }
        });
    },
    'ignore-payment'(paymentId) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let payment = Payments.findOne({_id: paymentId});
        if (!payment) {
            throw new Meteor.Error(500, 'Could not find payment');
        }

        Payments.update({
            _id: paymentId
        },{
            $set: {
                ignorePayment: true
            }
        });
    },
    'get-payments-for-reconciliation'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let payments = [];
        Payments.find({
            invoiceIds: {
                $exists: false
            },
            ignorePayment: {
                $ne: true
            }
        },{
            sort: {
                createdAt: -1
            },
            limit: 25
        }).forEach((payment) => {
            // 170000014
            let amount = payment.amount;
            let invoiceSelector = {
                $or: [
                    {
                        salesforceId: {
                            $exists: false
                        },
                        amount: amount,
                        status: {$in: ['sentToRetailer', 'promisedToPay'] },
                        deleted: {
                            $ne: true
                        }
                    }
                ]
            };

            let invoiceNrs = payment.description.match(/1[78]0*[1-9]{1,7}/g);
            if (invoiceNrs && invoiceNrs.length > 0) {
                invoiceSelector['$or'].push({
                    invoiceNr: {
                        $in: _.map(invoiceNrs, (invoiceNr) => { return Number(invoiceNr); })
                    }
                });
            }

            let invoices = [];
            Invoices.find(invoiceSelector, {
                fields: {
                    _id: 1,
                    invoiceNr: 1,
                    amount: 1
                }
            }).forEach((invoice) => {
                if (invoice.amount === payment.amount && payment.description.match(invoice.invoiceNr)) {
                    invoice.selected =  true;
                }
                invoices.push(invoice);
            });
            payment.invoices = invoices;
            payments.push(payment);
        });

        return payments;
    },
    'reopen-payment'(paymentId) {
        check(paymentId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Payments.update({
            _id: paymentId
        },{
            $unset: {
                ignorePayment: 1
            }
        });
    },
    'reconcile-payments'(ignorePayments, reconcilePayments) {
        check(ignorePayments, Array);
        check(reconcilePayments, Object);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (ignorePayments && ignorePayments.length > 0) {
            Payments.update({
                _id: {
                    $in: ignorePayments
                }
            },{
                $set: {
                    ignorePayment: true
                }
            },{
                multi: 1
            });
        }

        if (reconcilePayments && _.size(reconcilePayments) > 0) {
            _.each(reconcilePayments, (invoiceIds, paymentId) => {
                let payment = Payments.findOne({
                    _id: paymentId
                });

                let invoiceAmounts = [];
                let paymentDifference = payment.amount;
                let invoices = Invoices.find({
                    _id: {
                        $in: invoiceIds
                    }
                },{
                    fields: {
                        _id: 1,
                        amount: 1
                    }
                }).forEach((invoice) => {
                    invoiceAmounts.push({
                        invoiceId: invoice._id,
                        amount: invoice.amount
                    });
                    paymentDifference -= invoice.amount;
                });

                Payments.update({
                    _id: paymentId
                },{
                    $set: {
                        invoiceIds: invoiceIds,
                        invoiceAmounts: invoiceAmounts,
                        paymentDifference: paymentDifference
                    }
                });
            });
        }
    }
});

var processMT940Async = function (fileBuffer, callback) {
    mt940.read(fileBuffer).then(Meteor.bindEnvironment((statements) => {
        let result = {
            success: 0,
            failed: 0,
            duplicate: 0
        };
        _.each(statements, (statement) => {
            let iterator = 1;
            _.each(statement.transactions, (transaction) => {
                try {
                    let currency = Currencies.findOne({code: transaction.currency});
                    const transactionId = SHA256(transaction.id + transaction.description);
                    let insertResult = Payments.upsert({
                        transactionId: transactionId
                    },{
                        $set: {
                            accountId: statement.accountId,
                            transactionId: transactionId,
                            referenceNumber: statement.referenceNumber,
                            number: statement.number,
                            iterator: iterator++,
                            openingBalance: statement.openingBalance.isCredit == true ? Math.round(statement.openingBalance.value * 100) : Math.round(statement.openingBalance.value * -100),
                            openingBalanceDate: moment(statement.openingBalance.date, 'YYYY-MM-DD').toDate(),
                            closingBalance: statement.closingBalance.isCredit == true ? Math.round(statement.closingBalance.value * 100) : Math.round(statement.closingBalance.value * -100),
                            closingBalanceDate: moment(statement.closingBalance.date, 'YYYY-MM-DD').toDate(),
                            code: transaction.code,
                            currency: transaction.currency,
                            currencyId: currency._id,
                            description: transaction.description,
                            amount: transaction.isCredit ? Math.round(transaction.amount * 100) : Math.round(transaction.amount * -100),
                            valueDate: moment(transaction.valueDate, 'YYYY-MM-DD').toDate(),
                            entryDate: moment(transaction.entryDate, 'YYYY-MM-DD').toDate()
                        }
                    });
                    if (insertResult.insertedId) {
                        result.success++;
                    } else {
                        result.duplicate++;
                    }
                } catch (e) {
                    console.error(e);
                    result.duplicate++;
                }
            });
        });
        callback(null, result);
    })).catch((err) => {
        console.error(err);
        callback(err);
    });
};
var processMT940 = Meteor.wrapAsync(processMT940Async);

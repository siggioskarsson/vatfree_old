import { checkTotalPayments, getSetDataForPaymentForInvoice } from 'meteor/vatfree:payments/server/lib/payments';

try { Payments._dropIndex('accountId_1_transactionId_1_number_1_iterator_1'); } catch(e) {};
Meteor.setTimeout(function() {
    // We need to temporarily keep this in a timeout as the migration needs to finish before updating the index
    try { Payments._ensureIndex({accountId: 1, transactionId: 1}, {unique: 1}); } catch(e) {};
}, 30000);
Payments._ensureIndex({valueDate: -1});
Payments._ensureIndex({number: -1, iterator: 1});
Payments._ensureIndex({invoiceIds: 1, createdAt: -1}, {sparse: 1});
Payments._ensureIndex({textSearch: "text"});

ActivityStream.attachHooks(Payments);
// Add index to activity stream collection for payments
ActivityStream.collection._ensureIndex({'securityContext.paymentId': 1});

Payments.updateTextSearch = function(doc) {
    let textSearch = (
        (doc.accountId || "") + ' ' +
        (doc.transactionId || "") + ' ' +
        (doc.amount || 0) + ' ' + 'amount:' + (doc.amount || 0) + ' ' +
        (doc.number  + '-' + doc.iterator) + ' ' +
        (doc.currency || "") + ' ' +
        (doc.notes || "") + ' ' +
        (doc.description.split('/').join(' '))
    ).toLowerCase().latinize();

    Payments.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch
        }
    });
};

export const getIBAN = function(description) {
    let match = description.match("IBAN/([^/]+)");
    if (match && match[1]) {
        return match[1];
    }

    return null;
};

Payments.before.insert(function(userId, doc) {
    doc.accountNr = getIBAN(doc.description);
});

Payments.after.insert(function(userId, doc) {
    Payments.updateTextSearch(doc);
});

Payments.before.update(function(userId, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'invoiceIds')) {
        // check for valid invoice update
        if (modifier.$set && modifier.$set.invoiceIds && modifier.$set.invoiceAmounts) {
            let totalAmount = 0;
            _.each(modifier.$set.invoiceIds, (invoiceId) => {
                let invoiceAmount = _.find(modifier.$set.invoiceAmounts, (amount) => {
                    return amount.invoiceId === invoiceId;
                });
                let euroInvoiceAmount = _.find(modifier.$set.euroInvoiceAmounts, (euroAmount) => {
                    return euroAmount.invoiceId === invoiceId;
                });
                if (euroInvoiceAmount) {
                    invoiceAmount = euroInvoiceAmount;
                }

                if (!invoiceAmount || invoiceAmount.invoiceId !== invoiceId || !invoiceAmount.amount) {
                    throw new Meteor.Error(500, 'Missing or invalid invoice amount for invoice ' + invoiceId);
                } else {
                    totalAmount += invoiceAmount.amount;
                }
            });

            let paymentDifference = modifier.$set.paymentDifference || 0;
            if (paymentDifference + totalAmount !== doc.amount) {
                throw new Meteor.Error(500, 'Total amount of payment does not match invoice amounts');
            }
        } else if (modifier.$set && !modifier.$set.ignorePayment) {
            throw new Meteor.Error(500, 'Not a valid invoice update for payment');
        }
    }
});

Payments.after.update(function(userId, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'invoiceIds')) {
        // update invoices - all data has been checked in the before.update hook
        _.each(modifier.$set.invoiceIds, (invoiceId) => {
            // check that the invoice has been paid in full, otherwise check if other payments were made, otherwise mark partially paid
            const invoice = Invoices.findOne({_id: invoiceId});
            const invoiceAmounts = modifier.$set.invoiceAmounts;
            const euroInvoiceAmounts = modifier.$set.euroInvoiceAmounts;

            const setData = getSetDataForPaymentForInvoice(invoice, invoiceAmounts, euroInvoiceAmounts, doc.valueDate);

            Invoices.update({
                _id: invoiceId
            },{
                $set: setData,
                $addToSet: {
                    paymentIds: doc._id
                }
            });
        });
    }

    Meteor.defer(() => {
        Payments.updateTextSearch(doc);
    });
});

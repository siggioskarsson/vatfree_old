export const checkTotalPayments = function (invoice) {
    let totalPaid = 0;
    let euroTotalPaid = 0;
    let invoiceId = invoice._id;
    Payments.find({
        _id: {
            $in: invoice.paymentIds
        }
    }).forEach((payment) => {
        let paymentAmount = _.find(payment.invoiceAmounts, (amount) => {
            return amount.invoiceId === invoiceId;
        });

        let euroPaymentAmount = _.find(payment.euroInvoiceAmounts, (euroInvoiceAmount) => {
            return euroInvoiceAmount.invoiceId === invoiceId;
        });
        if (euroPaymentAmount) {
            euroTotalPaid += euroPaymentAmount;
        }

        if (paymentAmount) {
            totalPaid += paymentAmount.amount;
        }
    });
    return {totalPaid, euroTotalPaid};
};

export const getSetDataForPaymentForInvoice = function (invoice, invoiceAmounts, euroInvoiceAmounts, paidAt) {
    let status = 'partiallyPaid';
    let paymentAmount = _.find(invoiceAmounts, (amount) => {
        return amount.invoiceId === invoice._id;
    });
    let euroPaymentAmount = _.find(euroInvoiceAmounts, (euroAmount) => {
        return euroAmount.invoiceId === invoice._id;
    });

    const totalPayments = checkTotalPayments(invoice);

    let euroTotalPaid = 0;
    if (invoice.status === 'paid' && paymentAmount.amount > 0) {
        // the invoice is marked as paid, but we are getting a new payment. Overpaying ...
        status = 'paymentDifference';
        euroTotalPaid = totalPayments.euroTotalPaid;
    } else {
        if (invoice.amount === paymentAmount.amount && !totalPayments.totalPaid) {
            status = 'paid';
            if (euroPaymentAmount) {
                euroTotalPaid = euroPaymentAmount.amount;
            }
        } else if (invoice.amount < paymentAmount.amount && !totalPayments.totalPaid) {
            // too much paid
            status = 'paymentDifference';
            if (euroPaymentAmount) {
                euroTotalPaid = euroPaymentAmount.amount;
            }
        } else {
            if (invoice.paymentIds && invoice.paymentIds.length > 0) {
                // old payments, check whether we are now fully paid
                let totalPaid = totalPayments.totalPaid;
                euroTotalPaid = totalPayments.euroTotalPaid;

                if (totalPaid + paymentAmount.amount === invoice.amount) {
                    status = 'paid';
                } else if (totalPaid + paymentAmount.amount > invoice.amount) {
                    status = 'paymentDifference';
                } else if (totalPaid + paymentAmount.amount === 0) {
                    // set invoice back to waiting for payment in case of charge back, total === 0
                    status = 'sentToRetailer';
                    paidAt = null;
                }

                if (euroPaymentAmount) {
                    euroTotalPaid += euroPaymentAmount.amount;
                }
            }
        }
    }

    let setData = {
        status: status,
        paidAt: paidAt
    };

    if (euroTotalPaid) {
        setData.euroAmount = euroTotalPaid;
    }

    if (status === 'paymentDifference' || status === 'partiallyPaid') {
        setData.paymentDifference = (totalPayments.totalPaid + paymentAmount.amount) - invoice.amount;
    } else if (status === 'paid' || status === 'sentToRetailer') {
        setData.paymentDifference = 0;
    }

    return setData;
};

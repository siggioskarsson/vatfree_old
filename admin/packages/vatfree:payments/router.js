FlowRouter.route('/payments', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "Payments"});
    }
});

FlowRouter.route('/payment/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "PaymentEdit"});
    }
});

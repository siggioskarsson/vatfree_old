UI.registerHelper('getPaymentDescription', function(paymentId) {
    if (!paymentId) paymentId = this.paymentId || this._id;

    let payment = Payments.findOne({_id: paymentId});
    if (payment) {
        return payment.accountId + ', ' + payment.number + '-' + payment.iterator + ', ' + payment.description;
    }

    return "";
});

UI.registerHelper('getPaymentName', function(paymentId) {
    if (!paymentId) paymentId = this.paymentId || this._id;

    let payment = Payments.findOne({_id: paymentId});
    if (payment) {
        return payment.accountId + ', ' + payment.number + '-' + payment.iterator;
    }

    return "";
});

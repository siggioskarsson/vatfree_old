Template.PaymentInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.PaymentInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.PaymentInfo.helpers(paymentHelpers);
Template.PaymentInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    },
    parseDescription() {
        return this.description.split('/').join('<br/>');
    }
});

Template.payment_link_debt_collection.onCreated(function() {
    this.debtCollectionId = new ReactiveVar();
});

let initFields = function () {
    this.$('select[name="debtCollectionId"]').select2({
        minimumInputLength: 1,
        multiple: false,
        allowClear: true,
        placeholder: {
            id: "",
            placeholder: "Select debt collection ..."
        },
        ajax: {
            transport: function (params, success, failure) {
                let limit = 20;
                let offset = 0;
                Meteor.call('search-debt-collection', params.data.q || "", limit, offset, (err, res) => {
                    if (err) {
                        failure(err);
                    } else {
                        success({results: res});
                    }
                });
            },
            delay: 500
        }
    });
};

Template.payment_link_debt_collection.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        console.log(data);
    });

    this.autorun(() => {
        this.subscribe('debt-collection', this.debtCollectionId.get());
    });

    Tracker.afterFlush(() => {
        initFields.call(this);
    });
});

Template.payment_link_debt_collection.helpers({
    modalOnShown() {
        let template = Template.instance();
        return function() {
            initFields.call(template);
        };
    },
    debtCollectionId() {
        return Template.instance().debtCollectionId.get();
    }
});

Template.payment_link_debt_collection.events({
    'change select[name="debtCollectionId"]'(e, template) {
        e.preventDefault();
        template.debtCollectionId.set($(e.currentTarget).val());
    },
    'submit form[name="link-debt-collection-form"]'(e, template) {
        e.preventDefault();
        let parentTemplate = template.parentTemplate(2);
        console.log(parentTemplate);

        let debtCollection = DebtCollection.findOne({_id: template.debtCollectionId.get()});
        _.each(debtCollection.invoiceIds, (invoiceId) => {
            let invoice = Invoices.findOne({_id: invoiceId});
            parentTemplate.$('select[name="invoiceIds"]')
                .append($("<option></option>")
                    .attr("value", invoiceId)
                    .attr("selected", "selected")
                    .text(invoice.invoiceNr)
                );
        });
        parentTemplate.$('select[name="invoiceIds"]').select2();
        parentTemplate.$('select[name="invoiceIds"]').trigger('change');

        $('.close-modal').trigger('click');
    }
});

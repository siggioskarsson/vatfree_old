Template.addPaymentModal.onCreated(function() {
    let template = this;
    let parentVariable = this.data.parentVariable || 'addingItem';

    this.files = new ReactiveVar();
    this.isUploading = new ReactiveVar();

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-payment').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-payment')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-add-payment').on('shown.bs.modal', function () {
        });
        $('#modal-add-payment').modal('show');
    });
});

Template.addPaymentModal.helpers({
    withFiles() {
        return Template.instance().files.get();
    },
    isUploading() {
        return Template.instance().isUploading.get();
    }
});

Template.addPaymentModal.events({
    'click .cancel-add-payment'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'change input[name="fileUpload"]'(e, template) {
        e.preventDefault();
        let file = e.target.files[0];
        var reader = new FileReader();
        reader.onload = function(){
            file.content = reader.result;
            template.files.set([file]);
        };
        reader.readAsText(file);
    },
    'submit form[name="add-payment-form"]'(e, template) {
        e.preventDefault();

        template.isUploading.set(true);
        Tracker.afterFlush(() => {
            Meteor.call('upload-payment-files', template.files.get(), (err, result) => {
                if (err) {
                    toastr.error(err.reason);
                } else {
                    if (result) {
                        if (result.success) {
                            toastr.success('Imported ' + result.success + ' transactions');
                        }
                        if (result.duplicate) {
                            toastr.warning('Ignored ' + result.duplicate + ' duplicate transactions');
                        }
                    }
                    template.hideModal();
                }
                template.isUploading.set();
            });
        });
    }
});

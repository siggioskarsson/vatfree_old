import './reconcile.html';
import 'meteor/vatfree:invoices/templates/list';

Template.reconcilePaymentsModal.onCreated(function() {
    this.processing = new ReactiveVar(false);
    this.gettingPayments = new ReactiveVar(true);
    this.payments = new ReactiveDict();
    this.paymentIds = new ReactiveArray();
    this.reconcilingPayments = new ReactiveVar();

    this.activeItem = new ReactiveVar();

    this.getPayments = function() {
        // we get the receipts we are invoicing once, so reactivity doesn't mess up while we are busy
        this.payments.clear();
        this.gettingPayments.set(true);
        Meteor.call('get-payments-for-reconciliation', (err, payments) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                _.each(payments, (payment) => {
                    this.payments.set(payment._id, payment);
                    this.paymentIds.push(payment._id);
                });
                this.gettingPayments.set(false);
            }
        });
    };
});

Template.reconcilePaymentsModal.onRendered(function() {
    this.autorun((comp) => {
        if (this.subscriptionsReady()) {
            this.getPayments.call(this);
            comp.stop(); // stop current autorun computation
        }
    });

    this.autorun(() => {
        let invoiceIds = [];
        _.each(this.payments.all(), (payment) => {
            _.each(payment.invoices, (invoice) => {
                invoiceIds.push(invoice._id);
            });
        });
        this.subscribe('invoices', "", {_id: {$in: invoiceIds}});
    });

    this.autorun(() => {
        let gettingPayments = this.gettingPayments.get();
        if (gettingPayments === false) {
            Tracker.afterFlush(() => {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square icheckbox_square-blue',
                    radioClass: 'iradio_square iradio_square-blue'
                });
            });
        }
    });
});

Template.reconcilePaymentsModal_sidebar.helpers({
    getInvoice() {
        return Invoices.findOne({_id: this.options.activeInvoice.get()});
    }
});

Template.reconcilePaymentsModal.helpers({
    processing() {
        return Template.instance().processing.get();
    },
    getOptions() {
        let template = Template.instance();
        return {
            lockId: "reconcile_payments",
            lockMessage: "Payments are being worked on by someone else",
            sidebarTemplate: "reconcilePaymentsModal_sidebar",
            parentTemplateVariable: "reconcilingPayments",
            activeItem: 'invoice_create',
            activeInvoice: template.activeItem,
            payments: template.payments,
            paymentIds: template.paymentIds
        }
    },
    creatingInvoices() {
        return Template.instance().reconcilingPayments.get();
    },
    gettingReceipts() {
        return Template.instance().gettingPayments.get();
    },
    getPayments() {
        return _.values(Template.instance().payments.all());
    },
    getInvoices() {
        return Invoices.find({
            _id: {
                $in: _.map(this.invoices, (invoice) => { return invoice._id; })
            }
        });
    },
    getSelected() {
        return _.map(this.invoices, (invoice) => { if (invoice.selected) { return invoice._id; } })
    },
    getParsedDescription() {
        let parsedData = Vatfree.payments.parseMt940Description(this.description);

        return parsedData;
    }
});

Template.reconcilePaymentsModal.events({
    'dblclick .ibox-content .item-row'(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
    },
    'click .ibox-content .item-row'(e, template) {
        e.preventDefault();
        e.stopImmediatePropagation();
        if (template.activeItem.get() === this._id) {
            template.activeItem.set();
        } else {
            template.activeItem.set(this._id);
        }
    },
    'submit form[name="tasks-form"]'(e, template) {
        e.preventDefault();

        let ignorePayments = [];
        let reconcilePayments = {};
        _.each(template.payments.all(), (payment) => {
            // check whether ignored
            if (template.$('input[name="ignorePayment.' + payment._id + '"]').prop('checked') === true) {
                ignorePayments.push(payment._id);
            } else {
                let invoices = [];
                _.each(template.$('#payment_' + payment._id).find('input[type="checkbox"]:checked'), (checkbox) => {
                    let name = $(checkbox).attr('name');
                    if (name.match('invoices.')) {
                        invoices.push(name.replace('invoices.', ''));
                    }
                });
                if (invoices.length > 0) {
                    reconcilePayments[payment._id] = invoices;
                }
            }

        });

        template.processing.set(true);
        Tracker.afterFlush(() => {
            Meteor.call('reconcile-payments', ignorePayments, reconcilePayments, (err, result) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    template.getPayments.call(template);
                }
                template.processing.set(false);
            });
        });

        return false;
    }
});

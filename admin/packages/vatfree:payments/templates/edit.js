import './edit.html';
import 'meteor/vatfree:invoices/templates/list';
import 'meteor/vatfree:invoices/templates/add';

Template.PaymentEdit.onCreated(function () {
});

Template.PaymentEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('payments_item', FlowRouter.getParam('itemId'));
    });
});

Template.PaymentEdit.helpers({
    itemDoc() {
        return Payments.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    showBreadcrumb() {
        return this.ancestors && this.ancestors.length > 1;
    }
});

Template.paymentEditForm.onCreated(function() {
    this.addingActivityLog = new ReactiveVar();
    this.linkDebtCollection = new ReactiveVar();
    this.selectedInvoices = new ReactiveArray();
    this.invoiceLabels = new ReactiveDict();
});

Template.paymentEditForm.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('activity-logs', "", {paymentId: data._id});
    });

    this.autorun(() => {
        let data = Template.currentData();
        _.each(data.invoiceIds, (invoiceId) => {
            this.subscribe('invoices_item', invoiceId || '');
        });
    });
    this.autorun(() => {
        _.each(this.selectedInvoices.array(), (invoiceId) => {
            this.subscribe('invoices_item', invoiceId || '');
        });
    });

    this.autorun(() => {
        let data = Template.currentData();
        Tracker.afterFlush(() => {
            this.$('select[name="invoiceIds"]').select2({
                minimumInputLength: 1,
                multiple: true,
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-invoices', params.data.q || "", limit, offset, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });
        });
    });
});

Template.paymentEditForm.helpers(paymentHelpers);
Template.paymentEditForm.helpers({
    addingActivityLog() {
        return Template.instance().addingActivityLog.get();
    },
    linkDebtCollection() {
        return Template.instance().linkDebtCollection.get();
    },
    getActivityLog() {
        return ActivityLogs.find({
            paymentId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getSelectedInvoices() {
        return Template.instance().selectedInvoices.array();
    },
    getInvoice() {
        return Invoices.findOne({_id: this.toString()});
    },
    getInvoiceLabel(invoiceId) {
        let invoice = Invoices.findOne({_id: invoiceId});
        if (invoice) {
            let entity = {};
            if (invoice.shopId) {
                entity = Shops.findOne({_id: invoice.shopId}) || {};
            } else {
                entity = Companies.findOne({_id: invoice.companyId}) || {};
            }
            let currency = Currencies.findOne({_id: invoice.currencyId}) || {};
            return invoice.invoiceNr + ' - ' + entity.name + ', ' + entity.city + ' - ' + Vatfree.numbers.formatCurrency(invoice.amount, 2, currency.symbol);
        } else {
            return Template.instance().invoiceLabels.get(invoiceId);
        }
    },
    getInitialInvoiceAmount() {
        let invoiceLabel = Template.instance().invoiceLabels.get(this.toString());
        return Vatfree.numbers.unformatAmount(invoiceLabel.match(/[0-9 ,.]+$/)[0], TAPi18n.__('_decimal_separator'));
    },
    getInvoices() {
        return Invoices.find({
            _id: {
                $in: this.invoiceIds || []
            }
        });
    },
    getInvoiceAmount() {
        let invoiceAmounts = Template.instance().data.invoiceAmounts;
        let invoiceId = this._id;
        let invoiceAmount = _.find(invoiceAmounts, (amount) => {
            return amount.invoiceId === invoiceId;
        });

        if (invoiceAmount) {
            return invoiceAmount.amount;
        }
    },
    isDefaultCurrency(currencyId) {
        return currencyId === Vatfree.defaults.currencyId;
    }
});

Template.paymentEditForm.events(Vatfree.templateHelpers.events());
Template.paymentEditForm.events({
    'change select[name="invoiceIds"]'(e, template) {
        let invoiceIds = $(e.currentTarget).val();
        template.selectedInvoices.clear();
        _.each(invoiceIds, (invoiceId) => {
            template.selectedInvoices.push(invoiceId);
            template.invoiceLabels.set(invoiceId, $('select[name="invoiceIds"] option[value="' + invoiceId + '"]').text());
        });
    },
    'click .add-activity-log'(e, template) {
        e.preventDefault();
        template.addingActivityLog.set(true);
    },
    'click .calculate-payment-difference'(e, template) {
        e.preventDefault();
        let invoiceAmount = Number(this.amount)/100;
        let totalAssigned = 0;

        _.each(template.selectedInvoices.array(), (invoiceId) => {
            let amount = template.$('input[name="amount_' + invoiceId + '"]').val();
            if (template.$('input[name="euroAmount_' + invoiceId + '"]').length > 0) {
                amount = template.$('input[name="euroAmount_' + invoiceId + '"]').val();
            }
            totalAssigned += Number(amount);
        });
        template.$('input[name="payment-difference"]').val(Math.round((invoiceAmount - totalAssigned)*100)/100);
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/payments');
    },
    'click .link-debt-collection'(e, template) {
        e.preventDefault();
        template.linkDebtCollection.set(true);
    },
    'click .update-invoices'(e, template) {
        e.preventDefault();
        // first get the invoices Ids
        let invoiceIds = $('select[name="invoiceIds"]').val();
        if (!invoiceIds || invoiceIds.length <= 0) {
            toastr.error('You must select invoices for which this payment was for');
            return false;
        }
        // get the amounts for each invoice
        let totalAmount = 0;
        let amounts = {};
        let euroAmounts = {};
        _.each(invoiceIds, (invoiceId) => {
            let amount = Math.round(Number(template.$('input[name="amount_' + invoiceId + '"]').val()) * 100);
            amounts[invoiceId] = amount;

            if (template.$('input[name="euroAmount_' + invoiceId + '"]').length > 0) {
                let euroAmount = Math.round(Number($('input[name="euroAmount_' + invoiceId + '"]').val()) * 100);
                euroAmounts[invoiceId] = euroAmount;
                totalAmount += euroAmount;
            } else {
                totalAmount += amount;
            }
        });

        let paymentDifference = Math.round(Number($('input[name="payment-difference"]').val() || 0) * 100);

        if (paymentDifference + totalAmount !== this.amount) {
            toastr.error('Payment amount does not match all invoice amounts');
            return false;
        }

        Meteor.call('link-payment-to-invoices', this._id, amounts, euroAmounts, paymentDifference, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                toastr.success('Payment processed');
            }
        });
    },
    'click .ignore-payment'(e, template) {
        e.preventDefault();
        if (confirm('Are your sure you want to ignore this payment for invoices?')) {
            Meteor.call('ignore-payment', this._id, (err, result) => {
                if (err) {
                    toastr.error(err.reason);
                } else {
                    toastr.success('Payment marked as ignored');
                }
            });
        }
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
    },
    'click .reopen-invoice'(e, template) {
        e.preventDefault();
        Meteor.call('reopen-payment', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            }
        });
    }
});

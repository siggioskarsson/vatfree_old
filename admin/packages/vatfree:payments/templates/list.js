Template.paymentsList.helpers(Vatfree.templateHelpers.helpers);
Template.paymentsList.helpers(paymentHelpers);
Template.paymentsList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/payment/:itemId', {itemId: this._id});
    }
});

Template.paymentsList.helpers({
    getInvoiceNrs() {
        let invoiceNrs = [];
        Invoices.find({
            _id: {
                $in: this.invoiceIds || []
            }
        }).forEach((invoice) => {
            invoiceNrs.push(invoice.invoiceNr);
        });

        return invoiceNrs.join(', ');
    },
    getInvoiceAmount() {
        let invoiceId = Template.instance().data.invoiceId;
        if (invoiceId) {
            let invoiceAmount = _.find(this.invoiceAmounts, (amount) => {
                if (amount.invoiceId === invoiceId) {
                    return amount;
                }
            }) || {};

            return invoiceAmount.amount;
        }
    }
});

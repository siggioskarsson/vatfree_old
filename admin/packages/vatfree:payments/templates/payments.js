Template.Payments.onCreated(Vatfree.templateHelpers.onCreated(Payments, '/payments/', '/payment/:itemId'));
Template.Payments.onRendered(Vatfree.templateHelpers.onRendered());
Template.Payments.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.Payments.onCreated(function() {
    this.reconcilingPayments = new ReactiveVar();
    if (!this.sort.get()) {
        this.sort.set({
            number: -1,
            iterator: 1
        });
    }
    this.selectorFunction = (selector) => {
        if (selector.status && selector.status['$in']) {
            let selectedFilters = _.clone(selector.status['$in']);
            delete selector.status;

            selector['$or'] = [];
            if (_.contains(selectedFilters, 'ignored')) {
                selector['$or'].push({ignorePayment: true});
            }
            if (_.contains(selectedFilters, 'reconciled')) {
                selector['$or'].push({invoiceIds: {$exists: true}});
            }
            if (_.contains(selectedFilters, 'open')) {
                selector['$or'].push({
                    $and: [
                        {
                            ignorePayment: {
                                $exists: false
                            }
                        },
                        {
                            invoiceIds: {
                                $exists: false
                            }
                        }
                    ]
                });
            }
        }
    };
});

Template.Payments.helpers(Vatfree.templateHelpers.helpers);
Template.Payments.helpers(paymentHelpers);
Template.Payments.helpers({
    reconcilingPayments() {
        return Template.instance().reconcilingPayments.get();
    },
    getPaymentFilterOptions() {
        return [
            'ignored',
            'reconciled',
            'open'
        ];
    },
    getPaymentIcon() {
        let icons = {
            'ignored': "icon icon-archive",
            'reconciled': "icon icon-moneyexchange",
            'open': "icon icon-payments"
        };

        return icons[this.toString()];
    }
});
Template.Payments.events(Vatfree.templateHelpers.events());
Template.Payments.events({
    'click .reconcile-payments'(e, template) {
        e.preventDefault();
        template.reconcilingPayments.set(true);
    },
    'click .todos-filter'(e, template) {
        e.preventDefault();
        let statusFilter = template.statusFilter.array();
        if (statusFilter && statusFilter.length === 1 && statusFilter[0] === 'open') {
            template.statusFilter.clear();
        } else {
            template.statusFilter.clear();
            template.statusFilter.push('open');
        }
    }
});

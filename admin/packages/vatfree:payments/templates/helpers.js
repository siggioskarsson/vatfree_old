/* global paymentHelpers: true */

paymentHelpers = {
    getPayments() {
        let template = Template.instance();
        let selector = {};
        Vatfree.search.addStatusFilter.call(template, selector);
        template.selectorFunction(selector);

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
            let searchTerms = searchTerm.split(' ');

            selector['$and'] = [];
            _.each(searchTerms, (s) => {
                selector['$and'].push({textSearch: new RegExp(s)});
            });
        }

        return Payments.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    getShopName(shopId) {
        let shop = Shops.findOne({_id: shopId || this.shopId});
        if (shop) {
            return shop.name + ' - ' + (shop.addressFirst || "") + ', ' + (shop.postCode || "") + ' ' + (shop.city || "");
        }
    },
    getInvoices() {
        if (this.invoiceIds) {
            return Invoices.find({
                _id: {
                    $in: this.invoiceIds
                }
            });
        }
    }
};

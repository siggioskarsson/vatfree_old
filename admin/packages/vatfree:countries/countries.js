Countries = new TAPi18n.Collection('countries');
Countries.attachSchema(new SimpleSchema({
    id: {
        type: Number,
        label: "Internal country id",
        optional: false
    },
    name: {
        type: String,
        label: "Name of the country",
        optional: false
    },
    code: {
        type: String,
        label: "2 letter country code",
        optional: false
    },
    needVisa: {
        type: Boolean,
        label: "Whether travellers from this country need a visa",
        autoValue: function () {
            if (this.field('needVisa').isSet) {
                return !!this.field('needVisa').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    currencyId: {
        type: String,
        label: "Currency of this country",
        optional: true
    },
    minimumReceiptAmount: {
        type: Number,
        label: "Minimum receipt amount",
        optional: true
    },
    receiptValidityDays: {
        type: Number,
        label: "How many days is the receipt valid for refund",
        optional: true
    },
    vatRates: {
        type: [Object],
        label: "All VAT rates that are active in the country",
        optional: true,
        blackbox: true
    },
    telCodes: {
        type: [ String ],
        label: "International phone codes",
        optional: true
    },
    notes: {
        type: String,
        label: "Notes on this country",
        optional: true
    },
    textSearch: {
        type: String,
        autoValue: function() {
            if (this.field("name").value) {
                return ((this.field("name").value || "") + ' ' + (this.field("code").value || "")).toLowerCase().latinize();
            } else {
                this.unset();
            }
        },
        optional: true
    }
}));
Countries.attachSchema(CreatedUpdatedSchema);
Countries.attachSchema(TranslatableSchema);

Countries.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

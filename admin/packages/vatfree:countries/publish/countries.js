Meteor.publish('countries', function(searchTerm, selector, sort, limit, offset) {
    this.unblock();
    selector = selector || {};

    if (searchTerm) {
        searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
        let searchTerms = searchTerm.split(' ');

        selector['$and'] = [];
        _.each(searchTerms, (s) => {
            selector['$and'].push({textSearch: new RegExp(s)});
        });
    }

    return Countries.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset
    });
});

Meteor.publish('country-tel-codes', function() {
    return Countries.find({}, {
        fields: {
            code: 1,
            telCodes: 1
        }
    });
});

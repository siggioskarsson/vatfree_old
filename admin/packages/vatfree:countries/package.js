Package.describe({
    name: 'vatfree:countries',
    summary: 'Vatfree countries package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'hermanitos:activity-stream@0.0.1',
        'tap:i18n',
        'tap:i18n-db',
        'vatfree:core'
    ]);

    api.addAssets([
        'countries.json'
    ], 'server');

    // shared files
    api.addFiles([
        'countries.js'
    ]);

    // server files
    api.addFiles([
        'server/countries.js',
        'publish/countries.js',
        'publish/countries-i18n.js',
        'publish/country.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/countries.html',
        'templates/countries.js'
    ], 'client');

    api.export([
        'Countries'
    ]);
});

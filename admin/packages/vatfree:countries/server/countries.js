ActivityStream.attachHooks(Countries);

Meteor.methods({
    'import-countries'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let data = JSON.parse(Assets.getText('countries.json'));
        _.each(data, (country) => {
            Countries.insert({
                id: country.id,
                name: country.name,
                code: country.code
            });
        });
    }
});

UI.registerHelper('getCountryName', function(countryId) {
    if (!countryId) countryId = this.countryId;

    let country = Countries.findOne({_id: countryId});
    if (country) {
        return country.name;
    }

    return "";
});

UI.registerHelper('getCountryCode', function(countryId) {
    if (!countryId) countryId = this.countryId;

    let country = Countries.findOne({_id: countryId});
    if (country) {
        return country.code;
    }

    return "";
});

UI.registerHelper('getCountries', function() {
    return Countries.find({}, {
        sort: {
            name: 1
        }
    });
});

UI.registerHelper('getEUCountries', function() {
    return Countries.find({
        "needVisa": true
    }, {
        sort: {
            name: 1
        }
    });
});

UI.registerHelper('getNonEUCountries', function() {
    return Countries.find({
        "needVisa": {
            $ne: true
        }
    }, {
        sort: {
            name: 1
        }
    });
});

Template.countries.onCreated(Vatfree.templateHelpers.onCreated(Countries, '/countries/', '/country/:itemId'));
Template.countries.onRendered(Vatfree.templateHelpers.onRendered());
Template.countries.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.countries.helpers(Vatfree.templateHelpers.helpers);
Template.countries.helpers(countryHelpers);
Template.countries.events(Vatfree.templateHelpers.events());

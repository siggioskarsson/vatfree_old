Template.countryEdit.onCreated(function () {

});

Template.countryEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('countries_item', FlowRouter.getParam('itemId'));
    });

    Tracker.afterFlush(() => {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square icheckbox_square-blue',
            radioClass: 'iradio_square iradio_square-blue'
        });
    });
});

Template.countryEdit.helpers({
    itemDoc() {
        return Countries.findOne({
            _id: FlowRouter.getParam('itemId')
        }, {
            transform: false
        });
    },
    getBreadcrumbTitle() {
        return this.name + ' (' + this.code + ')';
    }
});

Template.countryEditForm.onCreated(function() {
    this.vatRates = new ReactiveArray();
    this.translations = [
        {
            name: "Name",
            key: "name"
        }
    ];
});

Template.countryEditForm.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.vatRates.clear();
        if (data.vatRates) {
            let key = 0;
            _.each(data.vatRates, (rate) => {
                this.vatRates.push({
                    key: key++,
                    rate: rate.rate,
                    description: rate.description
                });
            });
        }
    });
});

Template.countryEditForm.helpers(Vatfree.templateHelpers.helpers);
Template.countryEditForm.helpers({
    getVatRates() {
        return Template.instance().vatRates.array();
    }
});

Template.countryEditForm.events({
    'click .add-vat-rate'(e, template) {
        e.preventDefault();
        template.vatRates.push({
            key: template.vatRates.length,
            rate: "",
            description: ""
        });
    },
    'click .delete-vat-rate'(e, template) {
        e.preventDefault();
        if (confirm('Remove VAT rate?')) {
            let vatRates = template.vatRates.array();
            template.vatRates.clear();

            let newKey = 0;
            _.each(vatRates, (vatRate) => {
                if (this.key !== vatRate.key) {
                    vatRate.key = newKey++;
                    template.vatRates.push(vatRate);
                }
            });
        }
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/countries');
    },
    'submit form[name="item-edit-form"], submit form[name="item-edit-translations-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template, $(e.currentTarget));

        if (_.has(formData, 'minimumReceiptAmount')) {
            formData.minimumReceiptAmount = Math.round(Number(formData.minimumReceiptAmount)*100);
        }

        if (_.has(formData, 'rate')) {
            let rates = [];
            _.each(formData.rate, (rate, key) => {
                rates.push({
                    rate: rate,
                    description: formData.rateDescription[key]
                });
            });
            formData.vatRates = rates;
            delete formData.rate;
            delete formData.rateDescription;
        }

        if (_.has(formData, 'telCodes') && formData.telCodes) {
            formData.telCodes = formData.telCodes.split(',');
        }

        Countries.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Country saved!')
            }
        });
    }
});

Template.countriesList.helpers(Vatfree.templateHelpers.helpers);
Template.countriesList.helpers(countryHelpers);
Template.countriesList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/country/:itemId', {itemId: this._id});
    }
});

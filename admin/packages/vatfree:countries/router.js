FlowRouter.route('/countries', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "countries"});
    }
});

FlowRouter.route('/country/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "countryEdit"});
    }
});

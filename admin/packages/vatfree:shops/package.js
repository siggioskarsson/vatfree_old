Package.describe({
    name: 'vatfree:shops',
    summary: 'Vatfree shops package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'email',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'simonsimcity:job-collection',
        'vatfree:core',
        'vatfree:companies',
        'vatfree:contacts',
        'vatfree:notify',
        'vatfree:countries',
        'vatfree:email-templates',
        'vatfree:invoices'
    ]);

    // shared files
    api.addFiles([
        'shops.js',
        'shop-search.js',
        'jobs.js',
        'lib/shops.js'
    ]);

    // server files
    api.addFiles([
        'server/lib/shops.js',
        'server/import.js',
        'server/jobs.js',
        'server/methods.js',
        'server/notify.js',
        'server/shops.js',
        'server/shop-search.js', // needs to be defined after shops
        'server/shop-search-algolia.js', // needs to be defined after shops
        'publish/company-shops.js',
        'publish/files.js',
        'publish/receipt-files.js',
        'publish/shops.js',
        'publish/shop.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/card.less',
        'css/shops.less',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/card.html',
        'templates/card.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/import.html',
        'templates/import.js',
        'templates/info.html',
        'templates/info.js',
        'templates/intro-mail.html',
        'templates/intro-mail.js',
        'templates/list.html',
        'templates/list.js',
        'templates/merge.html',
        'templates/merge.js',
        'templates/open-invoice-mail.html',
        'templates/open-invoice-mail.js',
        'templates/shops.html',
        'templates/shops.js',
        'templates/tasks/call/tasks.html',
        'templates/tasks/call/tasks.js',
        'templates/tasks/incomplete/left-sidebar.html',
        'templates/tasks/incomplete/left-sidebar.js',
        'templates/tasks/incomplete/tasks.html',
        'templates/tasks/incomplete/tasks.js'
    ], 'client');

    api.addAssets([
        'templates/pdf/registration.nl.html',
        'templates/pdf/registration.en.html',
        'templates/pdf/rules-regulations-en.pdf',
        'templates/pdf/rules-regulations-nl.pdf',
        'templates/pdf/vatfree-terms-en.pdf',
        'templates/pdf/vatfree-terms-nl.pdf',
        'assets/Dutch Tax Administration ruling Vatfree.com.pdf',
        'assets/VATfreeNL-bevestigingvandeBelastingdienst.pdf'
    ], 'server');

    api.export([
        'Shops',
        'ShopSearch',
        'shopJobs'
    ]);
});

Meteor.publish('company-shops', function (companyId) {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    check(companyId, String);

    return Shops.find({
        companyId: companyId,
        deleted: {
            $exists: false
        }
    });
});

Meteor.publish('shop-files', function (shopId) {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    check(shopId, String);

    return Files.find({
        itemId: shopId,
        target: "shops"
    });
});

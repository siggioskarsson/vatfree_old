Meteor.publishComposite('shop-receipt-files', function(shopId) {
    check(shopId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Receipts.find({
                shopId: shopId
            },{
                fields: {
                    _id: 1,
                    shopId: 1
                }
            });
        },
        children: [
            {
                find: function (receipt) {
                    return Files.find({
                        itemId: receipt._id,
                        target: 'receipts'
                    });
                }
            }
        ]
    }
});

Meteor.publishComposite('shops_item', function(shopId) {
    check(shopId, String);
    return {
        find: function () {
            return Shops.find({
                _id: shopId
            });
        },
        children: [
            {
                find: function (shop) {
                    let company = Companies.findOne({_id: shop.companyId}) || {};
                    return Companies.find({
                        _id: {
                            $in: company.ancestors || []
                        }
                    });
                }
            },
            {
                collectionName: 'contacts',
                find: function (shop) {
                    return Contacts.find({
                        'profile.shopId': shop._id
                    });
                }
            },
            {
                find: function (shop) {
                    return Files.find({
                        itemId: shop._id,
                        target: {
                            $in: ['shops', 'shopping-guide']
                        }
                    });
                }
            }
        ]
    }
});

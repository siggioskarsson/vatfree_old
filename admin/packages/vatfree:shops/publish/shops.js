Meteor.publishComposite('shops', function(searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        if (!Vatfree.userIsInRole(this.userId, 'shops-read')) {
            throw new Meteor.Error(404, 'access denied');
        }
    }

    selector = selector || {};
    if (!_.has(selector, 'deleted')) {
        selector.deleted = {
            $exists: false
        };
    }

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        find: function () {
            return Shops.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                find: function (shop) {
                    let company = Companies.findOne({_id: shop.companyId}) || {};
                    return Companies.find({
                        _id: {
                            $in: company.ancestors || []
                        }
                    });
                }
            }
        ]
    }
});

Vatfree.shops = {};

Vatfree.shops.isActive = function (shopId) {
    let shop = Shops.findOne({_id: shopId});
    return shop.status === 'active';
};

Vatfree.shops.getName = function(shopId, shop = false) {
    shop = shop || Shops.findOne({_id: shopId});
    if (shop) {
        return shop.name;
    }

    return "";
};

Vatfree.shops.getDescription = function(shopId, shop = false) {
    shop = shop || Shops.findOne({_id: shopId});
    if (shop) {
        return shop.name + ' - ' + (shop.addressFirst || '') + ', ' + (shop.postCode || '') + ' ' + (shop.city || '');
    }

    return "";
};

Vatfree.shops.getObject = function(shopId, shop = false) {
    shop = shop || Shops.findOne({_id: shopId});
    if (shop) {
        let country = Countries.findOne({_id: shop.countryId}, {fields: {name: 1}});
        return {
            _id: shop._id,
            name: shop.name,
            status: shop.status,
            partnershipStatus: shop.partnershipStatus,
            address: shop.addressFirst,
            postalCode: shop.postCode,
            city: shop.city,
            countryId: country._id,
            country: country
        };
    }

    return {};
};

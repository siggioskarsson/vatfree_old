Shops = new Meteor.Collection('shops');
Shops.attachSchema(new SimpleSchema({
    id: {
        type: String,
        label: "Internal shop id",
        optional: true
    },
    poprId: {
        type: [String],
        label: "Popr id's of this shop",
        optional: true
    },
    oldName: {
        type: String,
        label: "Old Salesforce name of the shop",
        optional: true
    },
    name: {
        type: String,
        label: "Name of the shop",
        optional: false
    },
    status: {
        type: String,
        label: "The status of this shop",
        allowedValues: [
            'new',
            'incomplete',
            'toCall',
            'toMerge',
            'bankrupt',
            'moved',
            'dissolved',
            'takenOver',
            'active',
            'inactive'
        ],
        optional: false,
        defaultValue: 'new'
    },
    subscription: {
        type: String,
        label: "The subscription of this shop",
        allowedValues: [
            'none',
            'free',
            'basic',
            'blend',
            'boost',
            'boutique',
            'premium'
        ],
        optional: false,
        defaultValue: 'none'
    },
    affiliatedWith: {
        type: [String],
        label: "Affiliated with",
        optional: true
    },
    "affiliatedWith.$": {
        type: String,
        label: "Affiliated with",
        allowedValues: Vatfree.competitors
    },
    partneredBy: {
        type: String,
        label: "The user that partnered this shop",
        optional: true
    },
    partneredAt: {
        type: Date,
        label: "The date that this shop was partnered",
        optional: true
    },
    email: {
        type: String,
        label: "General email address",
        optional: true
    },
    tel: {
        type: String,
        label: "General telephone number",
        optional: true
    },
    fax: {
        type: String,
        label: "General telephone number",
        optional: true
    },
    website: {
        type: String,
        label: "General website",
        optional: true
    },
    addressFirst: {
        type: String,
        label: "Address of the shop",
        optional: true
    },
    postCode: {
        type: String,
        label: "Postal code of the shop",
        optional: true
    },
    city: {
        type: String,
        label: "City of the shop",
        optional: true
    },
    currencyId: {
        type: String,
        label: "Currency of the shop",
        optional: true
    },
    countryId: {
        type: String,
        label: "Country Id of the shop",
        optional: false
    },
    chamberOfCommerce: {
        type: String,
        label: "Chamber of commerce identifier",
        optional: true
    },
    vatNumber: {
        type: String,
        label: "VAT number",
        optional: true
    },
    companyId: {
        type: String,
        label: "Parent Company Id of the shop",
        optional: true
    },
    geo: {
        type: Object,
        label: "Geo information about the loation of the shop",
        blackbox: true,
        optional: true
    },
    partnershipStatus: {
        type: String,
        label: "Partnership status",
        allowedValues: [
            'partner',
            'pledger',
            'affiliate',
            'new',
            'uncooperative'
        ],
        optional: false,
        defaultValue: 'new'
    },
    partnershipStatusOrder: {
        type: Number,
        label: "Partnership status order",
        autoValue: function() {
            if (this.field("partnershipStatus").value) {
                let partnershipOrder = [ 'partner', 'affiliate', 'pledger', 'new', 'uncooperative' ];
                return partnershipOrder.indexOf(this.field("partnershipStatus").value);
            } else {
                this.unset();
            }
        },
        optional: true
    },
    language: {
        type: String,
        label: "Language code",
        optional: true
    },
    emailTemplateId: {
        type: String,
        label: "Preferred email template",
        optional: true
    },
    visitorsPerDay: {
        type: Number,
        label: "Number of visitors per day in the shop",
        optional: true
    },
    nps: {
        type: Number,
        label: "Last NPS score given",
        optional: true
    },
    npsReason: {
        type: String,
        label: "Last NPS reason given",
        optional: true
    },
    stats: {
        type: Object,
        label: "Stats about this shop",
        optional: true,
        blackbox: true
    },
    synchronization: {
        type: [Vatfree.synchronizationSchema],
        label: "Synchronizations of the item to/from other systems",
        optional: true
    },
    notes: {
        type: String,
        label: "Shop notes",
        optional: true
    },
    inShoppingGuide: {
        type: Boolean,
        label: "Whether this shop is in the shopping guide",
        defaultValue: true,
        optional: true
    },
    useParentShoppingGuideInfo: {
        type: Boolean,
        label: "Whether to use the parent shopping guide info",
        defaultValue: false,
        optional: true
    },
    shoppingGuideDescription: {
        type: String,
        label: "Shopping guide descriptive text",
        optional: true
    },
    communicationTemplate: {
        type: String,
        label: "Communication template to use",
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    },
    deleted: {
        type: Date,
        optional: true
    }
}));
Shops.attachSchema(CreatedUpdatedSchema);
Shops.attachSchema(BillingSchema);

Shops.allow({
    insert: function (userId, doc) {
        return Vatfree.userIsInRole(userId, 'shops-create');
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Vatfree.userIsInRole(userId, 'shops-update');
    },
    remove: function (userId, doc) {
        return false;
    }
});

Shops.before.insert(function(userId, doc) {
    doc.inShoppingGuide = !!doc.inShoppingGuide;
    doc.useParentShoppingGuideInfo = !!doc.useParentShoppingGuideInfo;
    doc.status = 'new';
});

Shops.before.update(function (userId, doc, fieldNames, modifier, options) {
    if (_.has(modifier, '$set') && _.has(modifier.$set, 'inShoppingGuide')) {
        modifier.$set.inShoppingGuide = !!modifier.$set.inShoppingGuide;
    }
    if (_.has(modifier, '$set') && _.has(modifier.$set, 'useParentShoppingGuideInfo')) {
        modifier.$set.useParentShoppingGuideInfo = !!modifier.$set.useParentShoppingGuideInfo;
    }
});

Shops.isPartner = function(shopId) {
    let shop = Shops.findOne({_id: shopId}) || {};
    return shop.partnershipStatus === 'partner';
};

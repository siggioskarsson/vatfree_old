Template.shops_intro_mail.onCreated(function() {
    let template = this;

    this.isLoaded = new ReactiveVar();
    this.isSending = new ReactiveVar();
    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-shops-intro-mail').modal('hide');
    };

    this.emailTextId = new ReactiveVar(Session.get(this.view.name + '.emailTextId'));

    Tracker.afterFlush(() => {
        $('#modal-shops-intro-mail').on('hidden.bs.modal', function () {
            if (template.parentTemplate && template.parentTemplate.sendIntroMail) template.parentTemplate.sendIntroMail.set(false);
        });
        $('#modal-shops-intro-mail').on('shown.bs.modal', function () {
            template.isLoaded.set(true);
        });
        $('#modal-shops-intro-mail').modal('show');
    });
});

Template.shops_intro_mail.onRendered(function() {
    this.subscribe('email-texts-list');

    this.autorun(() => {
        Session.setPersistent(this.view.name + '.emailTextId', this.emailTextId.get());
    });
});

Template.shops_intro_mail.onDestroyed(function() {
    this.hideModal();

    if (this.parentTemplateVariable && this.parentTemplate[this.parentTemplateVariable]) {
        this.parentTemplate[this.parentTemplateVariable].set(false);
    }

    $('body').removeClass('modal-open');
    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.shops_intro_mail.helpers({
    isSending() {
        return Template.instance().isSending.get();
    },
    getShop() {
        return Shops.findOne({_id: Template.instance().data.shopId});
    },
    getEmails() {
        // gather all emails
        let emails = [];

        let addEmails = function (emailString) {
            if (!_.isString(emailString) || !emailString) return;
            emailString = emailString.split(',');
            _.each(emailString, (email) => {
                emails.push({
                    email: email,
                    description: email
                });
            });
        };

        addEmails(this.email);
        addEmails(this.billingEmail);
        addEmails(this.billingEmailCC);
        addEmails(this.billingReminderEmail);
        addEmails(this.billingReminderEmailCC);

        Contacts.find({
            'profile.shopId': this._id
        }).forEach((contact) => {
            if (contact && contact.profile.email) {
                emails.push({
                    email: contact.profile.email,
                    description: contact.profile.name ? contact.profile.name + ' (' + contact.profile.email + '))' : contact.profile.email
                });
            }
        });

        return emails;
    },
    isEmailTextSelected() {
        return this._id === Template.instance().emailTextId.get();
    },
    getSubject() {
        let template = Template.instance();
        let emailText = EmailTexts.findOne({_id: template.emailTextId.get()});
        if (emailText) {
            let text = emailText.subject[this.language];
            import handlebars from 'handlebars';
            Vatfree.notify.registerHandlebarsHelpers(handlebars);
            let templateData = {
                language: this.language
            };
            return handlebars.compile(text)(templateData);
        }
    },
    getHTMLPreview() {
        let template = Template.instance();
        let emailText = EmailTexts.findOne({_id: template.emailTextId.get()});
        if (emailText) {
            let text = emailText.text[this.language];
            import handlebars from 'handlebars';
            Vatfree.notify.registerHandlebarsHelpers(handlebars);
            let templateData = {
                language: this.language
            };
            return handlebars.compile(text)(templateData);
        }
    }
});

Template.shops_intro_mail.events({
    'change select[name="emailTextId"]'(e, template) {
        template.emailTextId.set($(e.currentTarget).val());
    },
    'click .cancel-intro-mail'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.hideModal();
    },
    'submit form[name="send-intro-mail-form"]'(e, template) {
        e.preventDefault();
        template.isSending.set(true);
        let useEmail = template.$('select[name="useEmail"]').val();
        Meteor.call('send-intro-mail', this, useEmail, template.emailTextId.get(), (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                template.hideModal();
            }
            template.isSending.set(false);
        });
    }
});

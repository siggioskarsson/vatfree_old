import { shopHelpers } from "./helpers";

Template.shopInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.shopInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {shopId: data._id}, null, limit, offset);
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.shopInfo.helpers(shopHelpers);
Template.shopInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    },
    billingEntityDoc() {
        return Vatfree.billing.getBillingEntityForCompany(this.companyId);
    }
});

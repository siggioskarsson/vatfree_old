import { shopHelpers } from "./helpers";

Template.shopsList.helpers(Vatfree.templateHelpers.helpers);
Template.shopsList.helpers(shopHelpers);
Template.shopsList.events({
    'click input[name^="shops."], click div.pretty-checks'(e) {
        e.stopPropagation();
    },
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/shop/:itemId', {itemId: this._id});
    }
});

Template.shopsList.helpers({
    billingEntityDoc() {
        return Vatfree.billing.getBillingEntityForCompany(this.companyId);
    }
});

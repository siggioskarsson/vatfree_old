UI.registerHelper('getShop', function(shopId) {
    shopId = shopId || this.shopId || this._id;
    return Shops.findOne({_id: shopId});
});

UI.registerHelper('getShopStatusIcon', function(status) {
    var status = status || this.status;
    let statusIcons = {
        'new': 'icon icon-shopnew',
        'incomplete': 'icon icon-shopwaitingforverification',
        'toCall': 'icom icon-shoptobecalled',
        'bankrupt': 'icon icon-shopbankrupt',
        'moved': 'icon icon-shopmoved',
        'dissolved': 'icon icon-shopoutofbusiness',
        'takenOver': 'icon icon-shoptakenover',
        'active': 'icon icon-shopactive',
        'inactive': 'icon icon-shopinactive'
    };

    return statusIcons[status] || statusIcons['new'];
});

UI.registerHelper('getShopStatusOptions', function() {
    return _.clone(Shops.simpleSchema()._schema.status.allowedValues);
});

UI.registerHelper('getShopSubscriptionOptions', function() {
    return _.clone(Shops.simpleSchema()._schema.subscription.allowedValues);
});

UI.registerHelper('getAffiliatedWithOptions', function() {
    return _.clone(Shops.simpleSchema()._schema['affiliatedWith.$'].allowedValues);
});

UI.registerHelper('getShopPartnershipStatusIcon', function(status) {
    status = status || this.partnershipStatus;

    let button = 'VFCircleUnkown.svg';
    switch (status) {
        case 'new':
            button = 'VFCircleUnkown.svg';
            break;
        case 'pledger':
            button = 'VFCircleBlue.svg';
            break;
        case 'affiliate':
            button = 'vfcircleaffiliate.svg';
            break;
        case 'partner':
            button = 'VFCircleShade.svg';
            break;
        case 'uncooperative':
            button = 'VFCircleNonrefund.svg';
            break;
    }

    return button;
});

UI.registerHelper('getShopPartnershipStatusOptions', function() {
    return _.clone(Shops.simpleSchema()._schema.partnershipStatus.allowedValues);
});

UI.registerHelper('getShopDescription', function(shopId) {
    if (!shopId) shopId = this.shopId;

    let shop = Shops.findOne({_id: shopId});
    if (shop) {
        let country = Countries.findOne({_id: shop.countryId}) || {};
        return shop.name + ' - ' + shop.addressFirst + ', ' + shop.postCode + ' ' + shop.city + ', ' + country.name;
    }

    return "";
});

UI.registerHelper('getShopName', function(shopId) {
    if (!shopId) shopId = this.shopId;

    let shop = Shops.findOne({_id: shopId});
    if (shop) {
        return shop.name;
    }

    return "";
});

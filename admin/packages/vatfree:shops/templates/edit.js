import moment from 'moment';
import { editFormOnCreated, editFormHelpers, editFormEvents, setSubListSorting } from 'meteor/vatfree:core-templates/templates/edit-form-helpers';
import './edit.html';
import 'meteor/vatfree:invoices/templates/filtered-list';
import 'meteor/vatfree:invoices/templates/add';

Template.shopEdit.onCreated(function () {
    this.activeReceiptId = new ReactiveVar();
    this.activeInvoiceId = new ReactiveVar();
});

Template.shopEdit.onRendered(function () {
    this.autorun(() => {
        let shopId = FlowRouter.getParam('shopId');
        this.subscribe('shops_item', shopId);
        this.subscribe('email-templates-list', "", {}, {_id: 1}, 1000, 0); // get all email templates
        this.subscribe('activity-stream', {shopId: shopId}, null, 10000, 0);
    });

    this.autorun(() => {
        let shop = Shops.findOne({
            _id: FlowRouter.getParam('shopId')
        }, {
            fields: {
                companyId: 1
            }
        }) || {};
        this.subscribe('companies_item', shop.companyId || "");
    });
});

Template.shopEdit.helpers({
    shopDoc () {
        return Shops.findOne({
            _id: FlowRouter.getParam('shopId')
        });
    },
    activeReceiptId () {
        return Template.instance().activeReceiptId.get();
    },
    addActiveReceiptId () {
        this.activeReceiptId = Template.instance().activeReceiptId.get();
        return this;
    },
    getSelectedReceipt () {
        let receiptId = Template.instance().activeReceiptId.get();
        if (receiptId) {
            return Receipts.findOne({_id: receiptId});
        }
    },
    activeInvoiceId () {
        return Template.instance().activeInvoiceId.get();
    },
    getSelectedInvoice () {
        return Invoices.findOne({
            _id: Template.instance().activeInvoiceId.get()
        });
    },
    addActiveInvoiceId () {
        this.activeInvoiceId = Template.instance().activeInvoiceId.get();
        return this;
    },
    getBreadcrumbTitle () {
        return this.name + ', ' + (this.addressFirst || "") + ', ' + (this.postCode || "") + ' ' + (this.city || "");
    },
    showBreadcrumb () {
        let company = Companies.findOne({_id: this.companyId});
        return company && company.ancestors;
    },
    getCompanyDoc () {
        return Companies.findOne({_id: this.companyId});
    },
    isLoaded () {
        return Template.instance().isLoaded.get();
    }
});

Template.shopEdit.events({
    'click .tab-shops table.table.receipt-list .item-row' (e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeReceiptId) {
            template.activeInvoiceId.set();
            if (template.activeReceiptId.get() === this._id) {
                template.activeReceiptId.set();
            } else {
                template.activeReceiptId.set(this._id);
            }
        }
    },
    'click table.invoice-list .item-row' (e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeInvoiceId) {
            template.activeReceiptId.set();
            if (template.activeInvoiceId.get() === this._id) {
                template.activeInvoiceId.set();
            } else {
                template.activeInvoiceId.set(this._id);
            }
        }
    }
});

Template.shopEditForm.onCreated(editFormOnCreated());
Template.shopEditForm.onCreated(function () {
    this.addingReceipt = new ReactiveVar();
    this.addingInvoice = new ReactiveVar();
    this.addingContact = new ReactiveVar();
    this.sendIntroMail = new ReactiveVar();
    this.sendOpenInvoiceMail = new ReactiveVar();
    this.mergeShop = new ReactiveVar();

    this.receiptSort = new ReactiveVar({
        receiptNr: -1
    });
    this.invoiceSort = new ReactiveVar({
        invoiceNr: -1
    });
    this.contactSort = new ReactiveVar({
        createdAt: -1
    });
});

Template.shopEditForm.onRendered(function () {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('shop-files', data._id);
        this.subscribe('activity-logs', '', {shopId: data._id});
    });

    let formatPartnershipStatus = function (status) {
        let button = '';
        switch (status.id) {
            case 'new':
                button = 'VFCircleUnkown.svg';
                break;
            case 'pledger':
                button = 'VFCircleBlue.svg';
                break;
            case 'affiliate':
                button = 'vfcircleaffiliate.svg';
                break;
            case 'partner':
                button = 'VFCircleShade.svg';
                break;
            case 'uncooperative':
            default:
                button = 'VFCircleNonrefund.svg';
                break;
        }

        if (button) {
            return $(
                '<span class="select2-item"><img src="/Logos/' + button + '" class="img-responsive" /> ' + status.text + '</span>'
            );
        } else {
            return status.text;
        }
    };
    Tracker.afterFlush(() => {
        this.$('select[name="affiliatedWith"]').select2();
        this.$('select[name="partnershipStatus"]').select2({
            templateSelection: formatPartnershipStatus,
            templateResult: formatPartnershipStatus
        });

        let endDate = moment().format(TAPi18n.__('_date_format'));
        Vatfree.templateHelpers.initDatepicker.call(this, null, endDate);
        this.$('select[name="countryId"]').select2();
        Vatfree.templateHelpers.select2Search.call(this, 'search-companies', 'companyId', false, true);
    });

    this.autorun(() => {
        if (this.tabLoading.get('tab-2')) {
            let data = Template.currentData();
            this.subscribe('shop-receipts', data._id, (ready) => {
                this.tabLoading.set('tab-2', true);
            });
        }
    });

    this.autorun(() => {
        if (this.tabLoading.get('tab-3')) {
            let data = Template.currentData();
            this.subscribe('shop-invoices', data._id, (ready) => {
                this.tabLoading.set('tab-3', true);
            });
        }
    });
});

Template.shopEditForm.helpers(editFormHelpers());
Template.shopEditForm.helpers({
    addingReceipt () {
        return Template.instance().addingReceipt.get();
    },
    addingInvoice () {
        return Template.instance().addingInvoice.get();
    },
    addingContact () {
        return Template.instance().addingContact.get();
    },
    sendIntroMail () {
        return Template.instance().sendIntroMail.get();
    },
    sendOpenInvoiceMail () {
        return Template.instance().sendOpenInvoiceMail.get();
    },
    mergeShop () {
        return Template.instance().mergeShop.get();
    },
    isDisabled () {
        return this.deleted;
    },
    isEnabled () {
        return !this.deleted;
    },
    getReceipts () {
        let template = Template.instance();
        let sort = template.receiptSort.get();
        return Receipts.find({
            shopId: this._id
        }, {
            sort: sort
        });
    },
    getReceiptSorting () {
        return Template.instance().receiptSort.get();
    },
    getInvoices () {
        let template = Template.instance();
        let sort = template.invoiceSort.get();
        return Invoices.find({
            shopId: this._id
        }, {
            sort: sort
        });
    },
    getInvoicesSelector () {
        return {
            shopId: this._id
        };
    },
    getInvoiceSorting () {
        return Template.instance().invoiceSort.get();
    },
    isCountrySelected () {
        let template = Template.instance();
        return template.data.countryId === this._id;
    },
    isEmailTemplateSelected () {
        let template = Template.instance();
        return template.data.emailTemplateId === this._id;
    },
    getUsers () {
        return Meteor.users.find({});
    },
    getContacts () {
        return Contacts.find({
            'profile.shopId': this._id
        }, {
            sort: Template.instance().contactSort.get()
        });
    },
    getContactSorting() {
        return Template.instance().contactSort.get();
    },
    getActivityLog () {
        return ActivityLogs.find({
            shopId: this._id
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getActivity () {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        }, {
            sort: {
                date: -1
            }
        });
    },
    getFileContext () {
        return {
            itemId: this._id,
            target: 'shops'
        };
    },
    getFileContextShoppingGuide () {
        return {
            itemId: this._id,
            target: 'shopping-guide'
        };
    },
    getContactActions() {
        return [
            {
                icon: 'icon icon-trash',
                action: function(id) {
                    import swal from 'sweetalert2';
                    swal({
                        title: 'Are you sure?',
                        text: "Remove this contact from this shop?",
                        showCloseButton: true,
                        showCancelButton: true,
                        cancelButtonText: 'Cancel',
                        confirmButtonText: 'Yes, delete contact',
                        confirmButtonColor: '#ed5565',
                        reverseButtons: true,
                        closeOnConfirm: true
                    }).then(function () {
                        Meteor.call('delete-shop-contact', id, (err) => {
                            if (err) {
                                toastr.error(err.reason || err.message);
                            }
                        });
                    }, function (dismiss) {
                    });
                }
            }
        ]
    }
});

Template.shopEditForm.events(Vatfree.templateHelpers.events());
Template.shopEditForm.events(editFormEvents());
Template.shopEditForm.events({
    'click .delete-shop' (e, template) {
        e.preventDefault();
        Meteor.call('shop-get-linked-elements', this._id, (err, counts) => {
            let shop = this;
            let shopName = shop.name + '\n' + shop.addressFirst + ', ' + shop.postCode + ' ' + shop.city;

            let htmlText = '<b>' + shopName + '</b><br/>';
            htmlText += 'Will be deleted from the system.<br/><br/>';
            htmlText += '<table style=\'width: 80%; margin-left: 10%;\'>';
            htmlText += '<tr><td style=\'text-align: left\'>Receipts</td><td style=\'text-align: right\'>' + counts.receipts + '</td></tr>';
            htmlText += '<tr><td style=\'text-align: left\'>Invoices</td><td style=\'text-align: right\'>' + counts.invoices + '</td></tr>';
            htmlText += '<tr><td style=\'text-align: left\'>Contacts</td><td style=\'text-align: right\'>' + counts.contacts + '</td></tr>';
            htmlText += '</table>';

            import swal from 'sweetalert2';
            swal({
                title: 'Are you sure?',
                html: htmlText,
                showCloseButton: true,
                showCancelButton: true,
                cancelButtonText: 'Cancel',
                confirmButtonText: 'Yes, delete shop',
                confirmButtonColor: '#ed5565',
                reverseButtons: true,
                closeOnConfirm: false
            }).then(function () {
                Shops.update({
                    _id: shop._id
                }, {
                    $set: {
                        deleted: new Date()
                    },
                    $unset: {
                        id: 1,
                        poprId: 1
                    }
                });

                if (Session.get('Template.shops.activeItem') === shop._id) {
                    Session.setPersistent('Template.shops.activeItem');
                }

                FlowRouter.go('/shops');
                swal(shopName + ' deleted!');
            }, function (dismiss) {
            });
        });
    },
    'click table.receipt-list th.sort-header' (e, template) {
        e.preventDefault();
        e.stopPropagation();
        setSubListSorting(e, template, 'receiptSort');
    },
    'click table.invoice-list th.sort-header' (e, template) {
        e.preventDefault();
        e.stopPropagation();
        setSubListSorting(e, template, 'invoiceSort');
    },
    'click table.contact-list th.sort-header' (e, template) {
        e.preventDefault();
        e.stopPropagation();
        setSubListSorting(e, template, 'contactSort');
    },
    'click .cancel' (e) {
        e.preventDefault();
        FlowRouter.go('/shops');
    },
    'submit form[name="shop-edit-form"], submit form[name="billing-edit-form"], submit form[name="shopping-guide-edit-form"]' (e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template, $(e.currentTarget));

        if (_.has(formData, 'geo') && formData.geo) {
            formData.geo = JSON.parse(formData.geo);
        }

        if (_.has(formData, 'minFee')) {
            formData.minFee = formData.minFee > 0 ? formData.minFee * 100 : null;
        }
        if (_.has(formData, 'maxFee')) {
            formData.maxFee = formData.maxFee > 0 ? formData.maxFee * 100 : null;
        }

        // fix dates
        if (_.has(formData, 'partneredAt')) {
            if (formData.partneredAt) {
                formData.partneredAt = moment(formData.partneredAt, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.partneredAt = null;
            }
        }
        if (_.has(formData, 'debtCollectionDate')) {
            if (formData.debtCollectionDate) {
                formData.debtCollectionDate = moment(formData.debtCollectionDate, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.debtCollectionDate = null;
            }
        }
        if (_.has(formData, 'partnerDashboardFrom')) {
            if (formData.partnerDashboardFrom) {
                formData.partnerDashboardFrom = moment(formData.partnerDashboardFrom, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.partnerDashboardFrom = null;
            }
        }

        // Check whether we have all debt collection fields, if we have 1 of the fields
        if (formData['debtCollectionAccount'] || formData['debtCollectionMandate'] || formData['debtCollectionDate']) {
            if (!formData['debtCollectionAccount'] || !formData['debtCollectionMandate'] || !formData['debtCollectionDate']) {
                toastr.error('Please fill in all the debt collection fields');
                return false;
            }
        }

        let setter = {
            $set: formData
        };

        if (_.has(formData, 'companyId') && !formData.companyId) {
            setter['$unset'] = {
                companyId: 1
            };
        }

        if (_.has(formData, 'poprId')) {
            if (formData.poprId) {
                formData.poprId = formData.poprId.replace(/\s/g, '').split(',');
            } else {
                setter['$unset'] = {
                    poprId: 1
                };
            }
        }

        Shops.update({
            _id: this._id
        }, setter, function (err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Shop saved!');
            }
        });
    },
    'click .add-receipt' (e, template) {
        e.preventDefault();
        template.addingReceipt.set(true);
    },
    'click .add-contact' (e, template) {
        e.preventDefault();
        template.addingContact.set(true);
    },
    'click .add-invoice' (e, template) {
        e.preventDefault();
        template.addingInvoice.set(true);
    },
    'click .send-intro-mail' (e, template) {
        e.preventDefault();
        template.sendIntroMail.set(true);
    },
    'click .send-open-invoice-mail' (e, template) {
        e.preventDefault();
        template.sendOpenInvoiceMail.set(true);
    },
    'click .merge-shop' (e, template) {
        e.preventDefault();
        template.mergeShop.set(true);
    }
});

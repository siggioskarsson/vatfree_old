Template.shops_merge.onCreated(function() {
    let template = this;

    this.isLoaded = new ReactiveVar();
    this.isMerging = new ReactiveVar();
    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-shops-merge').modal('hide');
    };

    this.shopId = new ReactiveVar();
    this.counts = new ReactiveVar();

    Tracker.afterFlush(() => {
        $('#modal-shops-merge').on('hidden.bs.modal', function () {
            if (template.parentTemplate && template.parentTemplate.mergeShop) template.parentTemplate.mergeShop.set(false);
        });
        $('#modal-shops-merge').on('shown.bs.modal', function () {
            template.isLoaded.set(true);

            template.$('select[name="shopId"]').select2({
                minimumInputLength: 1,
                multiple: false,
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-shops', params.data.q || "", limit, offset, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });
        });
        $('#modal-shops-merge').modal('show');
    });
});

Template.shops_merge.onRendered(function() {
    let template = this;

    this.autorun(() => {
        let shopId = this.data.shopId;
        if (shopId) {
            Meteor.call('shop-get-linked-elements', shopId, (err, counts) => {
                template.counts.set(counts);
            });
        }
    });

    this.autorun(() => {
        let shopId = this.shopId.get();
        if (shopId) {
            this.subscribe('shops_item', shopId);
        }
    });
});

Template.shops_merge.onDestroyed(function() {
    this.hideModal();

    if (this.parentTemplateVariable && this.parentTemplate[this.parentTemplateVariable]) {
        this.parentTemplate[this.parentTemplateVariable].set(false);
    }

    $('body').removeClass('modal-open');
    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.shops_merge.helpers({
    isMerging() {
        return Template.instance().isMerging.get();
    },
    getShop() {
        return Shops.findOne({_id: Template.instance().shopId.get() });
    },
    getCounts() {
        return Template.instance().counts.get();
    }
});

Template.shops_merge.events({
    'change select[name="shopId"]'(e, template) {
        template.shopId.set($(e.currentTarget).val());
    },
    'click .cancel-merge-shop'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.hideModal();
    },
    'submit form[name="send-merge-shop-form"]'(e, template) {
        e.preventDefault();
        let shopId = this.shopId;
        let mergeWithShopId = template.$('select[name="shopId"]').val();

        if (shopId === mergeWithShopId) {
            alert('Cannot merge shop with self');
            return false;
        }

        if (!confirm('Merge shops?')) {
            return false;
        }

        template.isMerging.set(true);
        Meteor.call('merge-shop', shopId, mergeWithShopId, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                FlowRouter.go('/shop/' + mergeWithShopId);
                Shops.update({
                    _id: shopId
                },{
                    $set: {
                        deleted: new Date()
                    },
                    $unset: {
                        poprId: 1
                    }
                });
                template.hideModal();
            }
            template.isMerging.set(false);
        });
    }
});

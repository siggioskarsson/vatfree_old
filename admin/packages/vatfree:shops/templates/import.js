Template.shopsImport.events(Vatfree.templateHelpers.events());
Template.shopsImport.onCreated(function() {
    this.handleFileUpload = function(file) {
        let newFile = new FS.File(file);
        newFile.name(file.name.replace(/[^a-z0-9-_.]/ig, '_'));
        newFile.target = 'imports';
        newFile.subType = 'shops';
        newFile.createdAt = new Date();
        newFile.uploadedBy = Meteor.userId();
        newFile.importInfo = {
            running: false,
            progress: 0,
            progressMessage: ''
        };
        Files.insert(newFile, function (err) {
            if (err) {
                toastr.error(err);
            }
        });
    }
});

Template.shopsImport.onRendered(function() {
    this.autorun(() => {
        this.subscribe('imports', 'shops');
    });
});

Template.shopsImport.helpers({
    getImports() {
        return Files.find({
            target: 'imports',
            subType: 'shops'
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getStatus() {
        if (this.importInfo.progress === 100) {
            return "Done";
        } else if (this.importInfo.running) {
            return "Running";
        } else {
            return "-";
        }
    },
    getError() {
        if (_.isObject(this.error)) {
            return this.error.code;
        } else {
            return this.error;
        }
    }
});

Template.shopsImport.events({
    'click .import-file'(e, template) {
        e.preventDefault();
        Meteor.call('import-shops', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.info("Import started");
            }
        });
    }
});

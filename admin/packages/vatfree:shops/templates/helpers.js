/* global shopHelpers: true */

/**
 * NOTE: THIS FILE LIVES IN THE ADMIN AND I RE-USED IN MULTIPLE LOCATIONS !
 * @param template
 */

export const shopHelpers = {
    getPartnershipStatusLogo(partnershipStatus) {
        partnershipStatus = partnershipStatus || this.partnershipStatus;
        switch (partnershipStatus) {
            case 'new':
                return 'VFCircleUnkown.svg';
            case 'affiliate':
                return 'vfcircleaffiliate.svg';
            case 'pledger':
                return 'VFCircleBlue.svg';
            case 'partner':
                return 'VFCircleShade.svg';
            case 'uncooperative':
                return 'VFCircleNonrefund.svg';
            default:
                return 'VFCircleUnkown.svg';
        }
    },
    getShopSelector: function (template) {
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        Vatfree.search.addStatusFilter.call(template, selector);
        template.selectorFunction(selector);

        Vatfree.search.addListFilter.call(template, selector);

        return selector;
    },
    getShops() {
        let template = Template.instance();
        let selector = shopHelpers.getShopSelector(template);

        return Shops.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    getContactType() {
        switch (this.contactType) {
            case 'tel':
                return 'phone';
            case 'email':
            default:
                return 'envelope';

        }
    },
    getContactDetails() {
        return "-";
    }
};

export const selectMapLocationCallback = function () {
    let template = Template.instance();
    return function (place) {

        if (place && place.address_components) {
            let name = place.name;
            if (name) {
                template.$('input[name="name"]').val(name);
            }

            if (place.website) {
                template.$('input[name="website"]').val(place.website);
            }

            if (place.formatted_phone_number) {
                template.$('input[name="phone"]').val(place.formatted_phone_number);
            }

            let address = (_.find(place.address_components, (component) => { return _.contains(component.types, 'route'); }) || {}).long_name;
            if (address) {
                template.$('input[name="addressFirst"]').val(address);
            }

            let street_number = (_.find(place.address_components, (component) => { return _.contains(component.types, 'street_number'); }) || {}).long_name;
            if (street_number) {
                template.$('input[name="addressFirst"]').val(template.$('input[name="addressFirst"]').val() + ' ' + street_number);
            }

            let postal_code = (_.find(place.address_components, (component) => { return _.contains(component.types, 'postal_code'); }) || {}).long_name;
            if (postal_code) {
                template.$('input[name="postCode"]').val(postal_code);
            }

            let city = (_.find(place.address_components, (component) => { return _.contains(component.types, 'locality'); }) || {}).long_name;
            if (city) {
                template.$('input[name="city"]').val(city);
            }

            let countryCode = (_.find(place.address_components, (component) => { return _.contains(component.types, 'country'); }) || {}).short_name;
            if (countryCode) {
                let country = Countries.findOne({code: countryCode});
                if (country) {
                    template.$('select[name="countryId"]').val(country._id).trigger('change');

                    if (country.currencyId) {
                        template.$('select[name="currencyId"]').val(country.currencyId).trigger('change');
                    }
                }
            }
        }
    };
};

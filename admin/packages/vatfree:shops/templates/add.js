import { selectMapLocationCallback } from './helpers';

Template.addShopModal.onCreated(function() {
    let template = this;

    this.isLoaded = new ReactiveVar();
    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-shop').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-shop')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-shop').on('shown.bs.modal', function () {
            template.isLoaded.set(true);
        });
        $('#modal-add-shop').modal('show');
    });
});

Template.addShopModal.helpers({
    isLoaded() {
        return Template.instance().isLoaded.get();
    }
});

Template.addShopModal.events({
    'click .cancel-add-shop'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-shop-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (_.has(formData, 'geo') && formData.geo) {
            formData.geo = JSON.parse(formData.geo);
        }

        Shops.insert(formData, (err, shopId) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                template.hideModal();
                Meteor.setTimeout(() => {
                    FlowRouter.go('/shop/' + shopId);
                }, 200);
            }
        });
    }
});

Template.addShopModal_form.onRendered(function() {
    let template = this;

    Tracker.afterFlush(() => {
        template.$('select[name="countryId"]').select2();
        template.$('select[name="companyId"]').select2({
            minimumInputLength: 1,
            multiple: false,
            allowClear: true,
            placeholder: {
                id: "",
                placeholder: "Select company ..."
            },
            ajax: {
                transport: function (params, success, failure) {
                    let limit = 20;
                    let offset = 0;
                    Meteor.call('search-companies', params.data.q || "", limit, offset, (err, res) => {
                        if (err) {
                            failure(err);
                        } else {
                            console.log('get search companies', res);
                            success({results: res});
                        }
                    });
                },
                delay: 500
            }
        });

        template.$('input[id="_map_addressbar"]').focus();
    });
});

Template.addShopModal_form.helpers({
    selectMapLocationCallback() {
        return selectMapLocationCallback();
    }
});

import moment from 'moment';
import { shopHelpers } from "./helpers";
import './card.html';

Template.shopCard.helpers(shopHelpers);
Template.shopCard.helpers({
    getCountry() {
        return Countries.findOne({_id: this.countryId});
    }
});

Template.shopCard.events({
    'click .request-material'(e) {
        e.preventDefault();
        e.stopPropagation();

        const shop = this;
        const country = Countries.findOne({_id: shop.countryId});
        import swal from "sweetalert2";
        swal({
            title: "Create envelop activity?",
            showCloseButton: true,
            showCancelButton: true,
            cancelButtonText: "Cancel",
            confirmButtonText: "Yes, create it",
            confirmButtonColor: '#ed5565',
            reverseButtons: true,
            closeOnConfirm: true
        }).then(function () {
            const activityLogId = ActivityLogs.insert({
                type: 'task',
                status: 'new',
                subject: `ENVELOPPEN ${shop.name}, ${shop.addressFirst}, ${shop.city} FORMS`,
                shopId: shop._id,
                userId: "mMXRAJdMjxvod8hdt", // Michiel
                due: moment().add(1, 'day').toDate(),
                notes: `${shop.name}\n${shop.addressFirst}\n${shop.postCode} ${shop.city}\n${country.name}\n\n`
            });

            FlowRouter.go(`/activity-log/${activityLogId}`);
        }, function (dismiss) {
        });
    }
});

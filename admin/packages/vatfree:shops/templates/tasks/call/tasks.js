var checkForNewShopToCall = function() {
    if (this.debug) console.log('checking for new shop to call');
    if (this.view && this.view.isDestroyed === true) return;

    Meteor.call('getNextShopTask', 'toCall', (err, shopId) => {
        if (this.debug) console.log('getNextShopToCall', err, shopId);
        if (err) {
            toastr.error(err.reason);
        } else {
            if (shopId) {
                this.shopId.set(shopId);
            } else {
                Meteor.setTimeout(() => {
                    checkForNewShopToCall.call(this);
                }, 5000);
            }
        }
    });
};

Template.shop_call_task.onCreated(function() {
    this.shopId = new ReactiveVar(this.data.receiptId);
    this.selectedFile = new ReactiveVar();
    this.lockId = false;
    this.lock = false;
    this.debug = true;

    this.autorun(() => {
        if (this.shopId.get()) {
            this.subscribe('shops_item', this.shopId.get() || "");
            this.lockId = 'shop_to_call_';

            Tracker.nonreactive(() => {
                if (this.lock) {
                    if (this.debug) console.log('releasing lock');
                    this.lock.release();
                }
                if (this.debug) console.log('creating lock', this.lockId);
                this.lock = new Lock(this.lockId, Meteor.userId());
            });
        } else {
            if (this.lockId && this.lock) {
                this.lock.release();
            }
            checkForNewShopToCall.call(this);
        }
    });
});

Template.shop_call_task.helpers({
    getOptions() {
        let template = Template.instance();
        return {
            lockId: template.lockId,
            parentTemplateVariable: 'shopCalls',
            lockMessage: "Task is locked by another user",
            sidebarTemplate: "shop_call_task_sidebar",
            activeItem: template.shopId.get(),
            leftSidebarTemplate: false
        }
    },
    getShop() {
        return Shops.findOne({
            _id: Template.instance().shopId.get()
        });
    },
    selectedFile() {
        let template = Template.instance();
        let shopId = template.shopId.get();
        let fileId = template.selectedFile.get();
        if (shopId && fileId) {
            return Files.findOne({
                _id: fileId,
                itemId: shopId,
                target: 'shops'
            });
        }
    }
});

var checkForNewIncompleteShop = function() {
    if (this.debug) console.log('checking for new incomplete shop');
    if (this.view && this.view.isDestroyed === true) return;

    Meteor.call('getNextShopTask', 'incomplete', (err, shopId) => {
        if (this.debug) console.log('getNextIncompleteShop', err, shopId);
        if (err) {
            toastr.error(err.reason);
        } else {
            if (shopId) {
                this.shopId.set(shopId);
            } else {
                Meteor.setTimeout(() => {
                    checkForNewIncompleteShop.call(this);
                }, 5000);
            }
        }
    });
};

Template.shop_incomplete_task.onCreated(function() {
    this.shopId = new ReactiveVar(this.data.receiptId);
    this.selectedFile = new ReactiveVar();
    this.lockId = false;
    this.lock = false;
    this.debug = false;

    this.autorun(() => {
        if (this.shopId.get()) {
            this.subscribe('shops_item', this.shopId.get() || "");
            this.lockId = 'shop_to_call_';

            Tracker.nonreactive(() => {
                if (this.lock) {
                    if (this.debug) console.log('releasing lock');
                    this.lock.release();
                }
                if (this.debug) console.log('creating lock', this.lockId);
                this.lock = new Lock(this.lockId, Meteor.userId());
            });
        } else {
            if (this.lockId && this.lock) {
                this.lock.release();
            }
            checkForNewIncompleteShop.call(this);
        }
    });
});

Template.shop_incomplete_task.onRendered(function() {
    this.autorun(() => {
        if (!this.selectedFile.get()) {
            let fileId = Files.findOne({
                target: 'receipts'
            },{
                sort: {
                    createdAt: -1
                }
            });
            if (fileId) {
                this.selectedFile.set(fileId);
            }
        }
    });
});

Template.shop_incomplete_task.helpers({
    getOptions() {
        let template = Template.instance();
        return {
            lockId: template.lockId,
            parentTemplateVariable: 'shopIncompletes',
            lockMessage: "Task is locked by another user",
            sidebarTemplate: "shop_incomplete_task_sidebar",
            sidebarSize: 'x-large',
            activeItem: template.shopId.get(),
            selectedFile: template.selectedFile,
            leftSidebarTemplate: "shop_incomplete_task_left_sidebar"
        }
    },
    getShop() {
        return Shops.findOne({
            _id: Template.instance().shopId.get()
        });
    },
    selectedFile() {
        let template = Template.instance();
        return template.selectedFile.get();
    }
});

Template.shop_incomplete_task_sidebar.events({
    'submit form[name="shop-edit-form"]'(e, template) {
        e.preventDefault();
        template.parentTemplate(4).shopId.set();
        template.parentTemplate(4).selectedFile.set();
    }
});

Template.shop_incomplete_task_sidebar.helpers({
    getShop() {
        return Shops.findOne({
            _id: this.options.activeItem
        });
    },
    addHideCancelButtons() {
        this.hideCancelButtons = true;
        return this;
    }
});

Template.shop_incomplete_task_left_sidebar.onCreated(function() {
    this.parentTemplate = this.parentTemplate(4);
    this.receiptIds = new ReactiveVar();
    this.selectedFile = this.data.options.selectedFile;

    this.subscribe('shop-receipt-files', this.data.options.activeItem);
});

Template.shop_incomplete_task_left_sidebar.helpers({
    getFiles() {
        return Files.find({
            target: 'receipts'
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    isActiveFile(id) {
        let template = Template.instance();
        let fileId = template.parentTemplate.selectedFile.get();
        return fileId === id ? 'active' : false;
    }
});

var FileSaver = require('file-saver');
import { shopHelpers } from "./helpers";
import moment from 'moment';

Template.shops.onCreated(Vatfree.templateHelpers.onCreated(Shops, '/shops/', '/shop/:itemId'));
Template.shops.onRendered(Vatfree.templateHelpers.onRendered());
Template.shops.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.shops.helpers(Vatfree.templateHelpers.helpers);
Template.shops.helpers(shopHelpers);
Template.shops.events(Vatfree.templateHelpers.events());

Template.shops.onCreated(function() {
    this.shopCalls = new ReactiveVar();
    this.shopIncompletes = new ReactiveVar();

    this.partnershipStatusFilter = new ReactiveArray();
    this.billingEntityFilter = new ReactiveVar(Session.get(this.view.name + '.billingEntityFilter'));
    _.each(Session.get(this.view.name + '.partnershipStatusFilter') || [], (status) => {
        this.partnershipStatusFilter.push(status);
    });
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.partnershipStatusFilter', this.partnershipStatusFilter.array());
        Session.setPersistent(this.view.name + '.billingEntityFilter', this.billingEntityFilter.get());
    });

    this.todoStatus = [
        'incomplete', 'toCall'
    ];

    this.selectorFunction = (selector) => {
        let partnershipStatusFilter = this.partnershipStatusFilter.array() || [];
        if (partnershipStatusFilter.length > 0) {
            selector.partnershipStatus = {
                $in: partnershipStatusFilter
            };
        }

        let billingEntityFilter = this.billingEntityFilter.get() || false;
        if (billingEntityFilter) {
            selector['$and'] = [
                {
                    billingEntity: {
                        $ne: true
                    }
                },
                {
                    billingCompanyId: {
                        $exists: false
                    }
                }
            ];
        }
    };
});

Template.shops.onRendered(function() {
    this.autorun(() => {
        if (FlowRouter.getQueryParam('tasks')) {
            Tracker.afterFlush(() => {
                this.statusFilter.clear();
                _.each(this.todoStatus, (status) => {
                    this.statusFilter.push(status);
                });
            });
        }
    });
});

Template.shops.helpers({
    shopCalls() {
        return Template.instance().shopCalls.get();
    },
    shopIncompletes() {
        return Template.instance().shopIncompletes.get();
    },
    isActivePartnershipStatusFilter() {
        let partnershipStatusFilter = Template.instance().partnershipStatusFilter.array() || [];
        return _.contains(partnershipStatusFilter, this.toString()) ? 'active' : '';
    },
    isActiveBillingEntityFilter() {
        return Template.instance().billingEntityFilter.get() ? 'active' : '';
    },
    getShopStats() {
        let template = Template.instance();
        let selector = shopHelpers.getShopSelector(template);

        let stats = ReactiveMethod.call('get-shop-stats', selector);
        return stats && stats[0] ? stats[0] : false;
    }
});

Template.shops.events({
    'click .shops-export'(e, template) {
        e.preventDefault();
        template.runningExport.set(true);

        Meteor.call('shops-export', template.searchTerm.get(), template.statusFilter.array(), template.listFilters.get(), template.partnershipStatusFilter.array(), template.billingEntityFilter.get(), (err, csv) => {
            var blob = new Blob([csv], {
                type: "text/plain;charset=utf-8"
            });
            FileSaver.saveAs(blob, "shops_export_" + moment().format(TAPi18n.__('_date_time_format')) + ".csv");
            template.runningExport.set(false);
        });
    },
    'click .shops-debt-collection-export'(e, template) {
        e.preventDefault();
        template.runningExport.set(true);

        Meteor.call('shops-debt-collection-export', template.searchTerm.get(), template.statusFilter.array(), template.listFilters.get(), template.partnershipStatusFilter.array(), template.billingEntityFilter.get(), (err, csv) => {
            var blob = new Blob([csv], {
                type: "text/plain;charset=utf-8"
            });
            FileSaver.saveAs(blob, "shops_export_" + moment().format(TAPi18n.__('_date_time_format')) + ".csv");
            template.runningExport.set(false);
        });
    },
    'click .shop-calls'(e, template) {
        e.preventDefault();
        template.shopCalls.set(true);
    },
    'click .shop-incompletes'(e, template) {
        e.preventDefault();
        template.shopIncompletes.set(true);
    },
    'click .filter-icon.partnership-status-filter-icon'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        let status = this.toString();
        let partnershipStatusFilter = template.partnershipStatusFilter.array();

        if (_.contains(partnershipStatusFilter, status)) {
            template.partnershipStatusFilter.clear();
            _.each(partnershipStatusFilter, (_status) => {
                if (status !== _status) {
                    template.partnershipStatusFilter.push(_status);
                }
            });
        } else {
            template.partnershipStatusFilter.push(status);
        }
    },
    'click .filter-icon.billing-entity-filter-icon'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        let billingEntityFilter = template.billingEntityFilter.get();
        template.billingEntityFilter.set(!billingEntityFilter);
    }
});

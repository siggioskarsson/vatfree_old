Template.shops_open_invoice_mail.onCreated(function() {
    this.isLoaded = new ReactiveVar();
    this.isSending = new ReactiveVar();
    this.emailTextId = new ReactiveVar(Session.get(this.view.name + '.emailTextId'));
    this.attachments = new ReactiveArray();
});

Template.shops_open_invoice_mail.onRendered(function() {
    this.subscribe('email-texts-list');

    this.autorun(() => {
        Session.setPersistent(this.view.name + '.emailTextId', this.emailTextId.get());
    });

    // get the attachments of the invoices
    let invoiceIds = _.map(Invoices.find({status: 'sentToRetailer'}).fetch(), (invoice) => { return invoice._id; });
    _.each(invoiceIds, (invoiceId) => {
        this.subscribe('invoices_item', invoiceId || '');
    });

    this.autorun(() => {
        this.attachments.clear();
        Files.find({itemId: {$in: invoiceIds}, target: "invoices", subType: "invoice"}).forEach((file) => {
            this.attachments.push(file);
        });
    });
});

Template.shops_open_invoice_mail.helpers({
    getBillingEmailTemplateId() {
        return this.billingEmailTemplateId || Vatfree.defaults.billingEmailTemplateId;
    },
    isSending() {
        return Template.instance().isSending.get();
    },
    getShop() {
        let data = Template.instance().data;
        if (data.shopId) {
            return Shops.findOne({_id: data.shopId});
        } else {
            return Companies.findOne({_id: data.companyId});
        }
    },
    getEmails() {
        // gather all emails
        let emails = [];

        let addEmails = function (emailString) {
            if (!_.isString(emailString) || !emailString) return;
            emailString = emailString.split(',');
            _.each(emailString, (email) => {
                emails.push({
                    email: email,
                    description: email
                });
            });
        };

        addEmails(this.email);
        addEmails(this.billingEmail);
        addEmails(this.billingEmailCC);
        addEmails(this.billingReminderEmail);
        addEmails(this.billingReminderEmailCC);

        let selector = {
            'profile.shopId': this._id
        };
        if (Template.instance().data.companyId) {
            selector = {
                'profile.companyId': this._id
            };
        }
        Contacts.find(selector).forEach((contact) => {
            if (contact && contact.profile.email) {
                emails.push({
                    email: contact.profile.email,
                    description: contact.profile.name ? contact.profile.name + ' (' + contact.profile.email + '))' : contact.profile.email
                });
            }
        });

        return emails;
    },
    isEmailTextSelected() {
        return this._id === Template.instance().emailTextId.get();
    },
    getAttachments() {
        return Template.instance().attachments.array();
    },
    getSubject() {
        let template = Template.instance();
        let emailText = EmailTexts.findOne({_id: template.emailTextId.get()});
        if (emailText) {
            let text = emailText.subject[this.language];
            return text;
        }
    },
    getEmailText() {
        let template = Template.instance();
        let emailText = EmailTexts.findOne({_id: template.emailTextId.get()});
        if (emailText) {
            let text = emailText.text[this.language];
            return text;
        }
    }
});

Template.shops_open_invoice_mail.events({
    'change select[name="emailTextId"]'(e, template) {
        template.emailTextId.set($(e.currentTarget).val());
    },
    'click .cancel-intro-mail'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        $('.close-modal').trigger('click');
    },
    'click .remove-attachment-file'(e, template) {
        e.preventDefault();
        let index = template.attachments.indexOf(this);
        template.attachments.splice(index, 1);
    },
    'submit form[name="send-intro-mail-form"]'(e, template) {
        e.preventDefault();
        template.isSending.set(true);

        let emailData = Vatfree.templateHelpers.getFormData(template, 'form[name="send-intro-mail-form"]');
        emailData.attachmentIds = _.map(template.attachments.array(), (file) => { return file._id; });

        emailData.openInvoiceAmount = 0;
        emailData.invoiceNrs = [];
        emailData.invoiceId = '';
        emailData.invoiceSecret = '';
        Invoices.find({status: 'sentToRetailer'}).forEach((invoice) => {
            emailData.openInvoiceAmount += invoice.amount;
            emailData.invoiceNrs.push(invoice.invoiceNr);

            // just take any
            emailData.invoiceId = invoice._id;
            emailData.invoiceSecret = invoice.secret;
            emailData.invoiceCurrencyId = invoice.currencyId;
        });

        if (emailData.emailAddresses.length <= 0) {
            toastr.error('No email recipients selected');
            template.isSending.set();
            return false;
        }
        if (!emailData.emailTemplateId) {
            toastr.error('No email template selected');
            template.isSending.set();
            return false;
        }
        if (!emailData.emailSubject) {
            toastr.error('Email subject cannot be empty');
            template.isSending.set();
            return false;
        }
        if (!emailData.emailText) {
            toastr.error('Email subject cannot be empty');
            template.isSending.set();
            return false;
        }

        if (!confirm("Send mail?")) {
            template.isSending.set();
            return false;
        }

        emailData.from = 'Vatfree.com finance <finance@vatfree.com>';

        const activityIds = {};
        if (template.data.companyId) {
            activityIds['companyId'] = template.data.companyId;
        } else {
            activityIds['shopId'] = template.data.shopId;
        }
        Meteor.call('send-mail', emailData, activityIds, (err, result) => {
            if (err) {
                console.log(err);
                toastr.error(err.reason || err.message);
            } else {
                toastr.info("Email sent");
                $('.close-modal').trigger('click');
            }
            template.isSending.set(false);
        });
    }
});

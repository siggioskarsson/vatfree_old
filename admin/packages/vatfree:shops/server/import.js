/* global Vatfree: true */

const csv = require('csvtojson');
const DEBUGGING = false;
import {
    colMapping,
    convert,
    partnerShipStatusConvert,
    affiliateMapping,
    statusMapping,
    subscriptionMapping,
    cleanupUpdateData,
    saveCompanies,
    saveCompany } from 'meteor/vatfree:companies/server/import';

Vatfree['import-shops'] = function (importFile, fileObject, callback) {
    Vatfree.setImportFileProgress(10, 'Populating lookup data', fileObject);

    let countries = {};
    Countries.find().forEach(function (country) {
        countries[country.code] = country;
    });

    let lines = 0;
    let parentCompanies = {};

    Vatfree.setImportFileProgress(12, 'Initializing csv', fileObject);
    csv({
        delimiter: ';',
        noheader: false,
        trim: true,
        flatKeys: true,
        constructResult: false
    }).fromFile(importFile)
        .on('json', Meteor.bindEnvironment((row) => {
            lines++;
            if (row &&  _.size(row) > 2) {
                // first process parent account if applicable
                let parentAccountId = row["Parent Account ID"];
                if (parentAccountId) {
                    if (!parentCompanies[parentAccountId]) {
                        parentCompanies[parentAccountId] = {
                            children: []
                        }
                    }
                    parentCompanies[parentAccountId].children.push(row['Account ID']);
                }

                let updateData = Vatfree.import.processColumns(colMapping, convert, row);
                if (updateData['sgShop'] !== "yes" || !updateData['name']) {
                    // mag niet meer : return;
                } else {
                    cleanupUpdateData(updateData, countries);

                    updateData['name'] = updateData['shopName'] || updateData['name'];

                    updateData['partnershipStatus'] = partnerShipStatusConvert[updateData['partnershipStatus']];
                    //console.log('updateData', JSON.stringify(updateData));
                    saveShop(updateData);
                }
            }
        }))
        .on('done', Meteor.bindEnvironment((error) => {
            Vatfree.setImportFileProgress(50, 'Shops done - doing companies', fileObject);

            Meteor.setTimeout(() => {
                saveCompanies(csv, importFile, parentCompanies, countries, fileObject, function() {
                    console.log('end', error);
                    Vatfree.setImportFileProgress(100, 'Done', fileObject);
                    if (callback) {
                        callback();
                    }
                });
            }, 5000);
        }));
};

Meteor.methods({
    getAffiliatedFromImportFile() {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let shopsFile = '/Users/oskarsson/Downloads/account_export_v2.1.csv';
        let affiliates = {};
        csv({
            delimiter: ';',
            noheader: false,
            trim: true,
            flatKeys: true,
            constructResult: false
        }).fromFile(shopsFile)
            .on('json', Meteor.bindEnvironment((row) => {
                let affiliated = row['Affiliated with'];
                if (affiliated) {
                    affiliates[affiliated] = 1;
                }
            }))
            .on('done', Meteor.bindEnvironment((error) => {
                if (DEBUGGING) console.log('end', JSON.stringify(_.keys(affiliates)));
            }));
    },
    importShopsLocally() {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }
        Vatfree.importShops('/Users/oskarsson/Downloads/account_export_v2.1.csv');
    }
});

var saveShop = function (updateData) {
    let shop = Shops.findOne({
        'id': updateData['id']
    });

    updateData.status = statusMapping[updateData.accountStatus] || 'active';
    if (updateData.partnershipStatus === 'partner' || updateData.partnershipStatus === 'affiliate') {
        updateData.subscription = subscriptionMapping[updateData.accountStatus] || 'none';
    } else {
        updateData.subscription = 'none';
    }
    updateData.textSearch =  (
        (updateData.name || "") + ' ' +
        (updateData.addressFirst || "") + ' ' +
        (updateData.postCode || "") + ' ' +
        (updateData.city || "")
    ).toLowerCase().latinize();

    // set all languages to English
    if (shop) {
        Shops.direct.update({
            _id: shop._id
        }, {
            $set: updateData
        });
        if (DEBUGGING) console.log('saving shop', shop._id);
    } else {
        updateData['createdAt'] = new Date();
        if (DEBUGGING) console.log('saving new shop', updateData['id']);
        Shops.insert(updateData);
    }
};

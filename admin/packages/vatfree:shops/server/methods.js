import { Meteor } from "meteor/meteor";
import { getEntityExportSelector } from 'meteor/vatfree:companies/server/methods';

Meteor.methods({
    'get-shop'(shopId) {
        this.unblock();
        return Shops.findOne({_id: shopId});
    },
    'get-shop-affiliated-with'(shopId) {
        this.unblock();
        const shop = Shops.findOne({_id: shopId}) || {};
        if (shop.billingCompanyId) {
            const company = Companies.findOne({_id: shop.billingCompanyId});
            if (company.affiliatedWith) {
                return company.affiliatedWith;
            }
        }
        return shop.affiliatedWith || false;
    },
    'search-shops'(searchTerm, limit, offset) {
        this.unblock();
        if (!this.userId) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {
            deleted: {
                $exists: false
            }
        };
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector, 't');
        }

        let shops = [];
        ShopSearch.find(selector, {
            sort: {
                r: -1,
                n: 1
            },
            limit: limit,
            offset: offset
        }).forEach((shop) => {
            shops.push({
                text: shop.n + ' - ' + shop.a.replace('<br/>', ', '),
                id: shop._id,
                partnershipStatus: shop.p
            });
        });

        return shops;
    },
    getNextShopTask(status) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Vatfree.userIsInRole(this.userId, 'shops-tasks')) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        let shopId = false;
        Shops.find({
            status: status,
            deleted: {
                $exists: false
            }
        },{
            sort: {
                createdAt: 1
            }
        }).forEach((shop) => {
            // check for lock
            if (!shopId && !Locks.findOne({
                    lockId: 'shop_' + status + '_' + shop._id,
                    userId: {
                        $ne: this.userId
                    }
                })) {
                shopId = shop._id;
                return shopId;
            }
        });

        return shopId;
    },
    'shop-get-linked-elements'(shopId) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Vatfree.userIsInRole(this.userId, 'shops-read')) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        let contacts = Contacts.find({'profile.shopId': shopId}, {fields: {_id: 1}}).count();
        let receipts = Receipts.find({shopId: shopId}, {fields: {_id: 1}}).count();
        let invoices = Invoices.find({shopId: shopId}, {fields: {_id: 1}}).count();
        let activityLogs = ActivityLogs.find({shopId: shopId}, {fields: {_id: 1}}).count();

        return {
            contacts: contacts,
            receipts: receipts,
            invoices: invoices,
            activityLogs: activityLogs
        };
    },
    'fix-shop-geo'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Vatfree.userIsInRole(this.userId, 'shops-admin')) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        console.log('updating shop geo');
        Shops.find({
            geo: {
                $exists: true
            }
        },{
            fields: {
                geo: 1
            }
        }).forEach((shop) => {
            if (shop.geo && shop.geo.coordinates) {
                Shops.update({
                    _id: shop._id
                },{
                    $set: {
                        geo: {
                            "type": "Point",
                            "coordinates": [
                                shop.geo.coordinates[1],
                                shop.geo.coordinates[0]
                            ]
                        }
                    }
                });
            }
        });
        console.log('done');
    },
    'fix-shop-billing-company'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Vatfree.userIsInRole(this.userId, 'shops-admin')) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        console.log('updating shop billing company');
        Shops.find({
            companyId: {
                $exists: true
            },
            billingCompanyId: {
                $exists: false
            }
        }).forEach((shop) => {
            let billingCompany = Vatfree.billing.getBillingEntityForCompany(shop.companyId);
            if (billingCompany) {
                Shops.update({
                    _id: shop._id
                },{
                    $set: {
                        billingCompanyId: billingCompany._id
                    }
                });
            }
        });
        console.log('done');
    },
    'get-shop-fees'(shopIds) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Vatfree.userIsInRole(this.userId, 'shops-read')) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        let shopFees = {};
        _.each(shopIds, (shopId) => {
            shopFees[shopId] = Vatfree.receipts.getFees(shopId);
        });
        return shopFees;
    },
    'send-intro-mail'(entity, useEmail, emailTextId, emailTemplateId) {
        check(entity, Object);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Vatfree.userIsInRole(this.userId, 'shops-admin')) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        let toAddress = useEmail || entity.email;
        if (!toAddress) {
            throw new Meteor.Error(404, 'No email address defined');
        }

        let emailText = EmailTexts.findOne({_id: emailTextId});
        if (emailText) {
            let subject = emailText.subject[entity.language];
            let text = emailText.text[entity.language];

            import handlebars from 'handlebars';
            Vatfree.notify.registerHandlebarsHelpers(handlebars);

            let templateData = {
                language: entity.language
            };
            let emailSubject = handlebars.compile(subject)(templateData);
            let emailBody = handlebars.compile(text)(templateData);
            let emailBodyText = Vatfree.notify.stripHtmlTags(emailBody);

            let template = EmailTemplates.findOne({_id: entity.emailTemplateId});
            if (!template) {
                throw new Meteor.Error(404, 'Could not find email template');
            }
            let renderedEmailHtml = handlebars.compile(template.templateHTML)({text: emailBody});
            let renderedEmailText = handlebars.compile(template.templateText)({text: emailBodyText});

            let activityLog = {
                type: 'email',
                subject: emailSubject,
                address: toAddress,
                status: 'done',
                notes: renderedEmailText,
                html: renderedEmailText,
                shopId: entity._id
            };
            ActivityLogs.insert(activityLog);

            let pdfAttachment = "Dutch Tax Administration ruling Vatfree.com.pdf";
            if (entity.language === "nl") {
                pdfAttachment = "VATfreeNL-bevestigingvandeBelastingdienst.pdf";
            }
            let attachments = [
                {
                    filename: pdfAttachment,
                    content: Assets.getBinary('assets/' + pdfAttachment)
                }
            ];

            Email.send({
                to: toAddress,
                from: "support@vatfree.com",
                subject: emailSubject,
                text: renderedEmailText,
                html: renderedEmailHtml,
                attachments: attachments
            });
        } else {
            throw new Meteor.Error(404, "No email text found");
        }
    },
    'merge-shop'(shopId, mergeWithShopId) {
        check(shopId, String);
        check(mergeWithShopId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Vatfree.userIsInRole(this.userId, 'shops-update')) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        let newShop = Shops.findOne({_id: mergeWithShopId});
        if (!newShop) {
            throw new Meteor.Error(404, 'Could not find shop to merge into');
        }

        Receipts.find({
            shopId: shopId
        }).forEach((receipt) => {
            let allowUpdateStatus = [
                'userPending',
                'waitingForOriginal',
                'visualValidationPending',
                'waitingForDoubleCheck',
                'shopPending',
                'readyForInvoicing'
            ];
            if (_.contains(allowUpdateStatus, receipt.status)) {
                // we can do a normal update
                Receipts.update({
                    _id: receipt._id
                },{
                    $set: {
                        shopId: mergeWithShopId
                    }
                });
            } else {
                // we must do a direct update and also update the companyId if applicable
                let setData = {
                    shopId: mergeWithShopId
                };

                if (newShop.companyId) {
                    setData.companyId = newShop.companyId;
                    let billingCompany = Vatfree.billing.getBillingEntityForCompany(newShop.companyId);
                    if (billingCompany) setData.billingCompanyId = billingCompany._id;
                    else setData.billingCompanyId = null;
                } else {
                    setData.companyId = null;
                    setData.billingCompanyId = null;
                }

                Receipts.direct.update({
                    _id: receipt._id
                },{
                    $set: setData
                });
            }
        });

        Invoices.find({
            shopId: shopId
        }).forEach((invoice) => {
            Invoices.update({
                _id: invoice._id
            },{
                $set: {
                    shopId: mergeWithShopId
                }
            });
        });

        Contacts.find({
            'profile.shopId': shopId
        }).forEach((contact) => {
            Contacts.update({
                _id: contact._id
            },{
                $set: {
                    'profile.shopId': mergeWithShopId
                }
            });
        });

        ActivityLogs.find({
            shopId: shopId
        }).forEach((activityLog) => {
            ActivityLogs.update({
                _id: activityLog._id
            },{
                $set: {
                    shopId: mergeWithShopId
                }
            });
        });

        return true;
    },
    createShopSearch() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Vatfree.userIsInRole(this.userId, 'shops-admin')) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        ShopSearch.direct.remove({});

        let selector = {
            deleted: {
                $exists: false
            }
        };
        if (Meteor.settings.shopSearch && _.isObject(Meteor.settings.shopSearch.selector)) {
            selector = _.clone(Meteor.settings.shopSearch.selector);
        }

        Vatfree.shops.updateShopSearchIndex(selector, true);
    },
    'get-shop-stats'(selector) {
        if (!Vatfree.userIsInRole(this.userId, 'shops-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const pipeline = [
            {
                $match: selector,
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 }
                }
            },
        ];
        return Shops.rawCollection().aggregate(pipeline).toArray();
    },
    'shops-export'(searchTerm, status, listFilters, partnershipStatusFilter, billingEntityFilter) {
        if (!Vatfree.userIsInRole(this.userId, 'shops-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const Baby = require('babyparse');
        let selector = getEntityExportSelector(searchTerm, status, listFilters, partnershipStatusFilter, billingEntityFilter);

        let data = [];
        Shops.find(selector, {
            sort: {
                createdAt: 1
            },
            fields: {
                textSearch: false
            }
        }).forEach((shop) => {
            shop.geo = JSON.stringify(shop.geo);
            shop.stats = JSON.stringify(shop.stats);
            data.push(shop);
        });
        let csv = Baby.unparse(data);

        return csv;
    },
    'shops-debt-collection-export'(searchTerm, status, listFilters, partnershipStatusFilter, billingEntityFilter) {
        if (!Vatfree.userIsInRole(this.userId, 'shops-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const Baby = require('babyparse');
        let selector = getEntityExportSelector(searchTerm, status, listFilters, partnershipStatusFilter, billingEntityFilter);


        let data = [];
        Shops.find(selector, {
            sort: {
                createdAt: 1
            },
            fields: {
                textSearch: false
            }
        }).forEach((shop) => {
            if (!shop.stats) shop.stats = {};
            let billingEmail = shop.billingEmail;
            if (!billingEmail) {
                const contactEmails = [];
                Contacts.find({
                    'roles.__global_roles__': 'contact',
                    'profile.shopId': shop._id
                }).forEach((contact) => {
                    if (contact.profile.email) {
                        contactEmails.push(contact.profile.email);
                    }
                });
                if (contactEmails.length > 0) {
                    billingEmail = _.uniq(contactEmails).join(', ');
                }
            }
            if (!billingEmail) {
                billingEmail = shop.email;
            }
            const shopExport = {
                name: shop.name,
                address: shop.addressFirst + ', ' + shop.city,
                email: billingEmail,
                phone: shop.tel,
                partnershipStatus: shop.partnershipStatus,
                nrInvoices: shop.stats.numberOfInvoices || 0,
                spend: shop.stats.totalRevenue ? Vatfree.numbers.formatAmount(shop.stats.totalRevenue) : '',
                fee: shop.stats.totalServiceFee ? Vatfree.numbers.formatAmount(shop.stats.totalServiceFee) : ''
            };
            data.push(shopExport);
        });
        let csv = Baby.unparse(data);

        return csv;
    }
});

import { Meteor } from "meteor/meteor";

ShopSearch._ensureIndex({"t": "text"}, {"language_override": "_text_language"});
ShopSearch._ensureIndex({"g": "2dsphere"});
ShopSearch._ensureIndex({"p": 1});

Shops.after.insert(function(userId, doc) {
    updateShopSearchIndex(doc);
});

Shops.after.update(function(userId, doc) {
    updateShopSearchIndex(doc);
});

Shops.after.remove(function(userId, doc) {
    ShopSearch.remove({_id: doc._id});
});

var updateShopSearchIndex = function (doc) {
    let selector = {
        _id: doc._id,
        deleted: {
            $exists: false
        }
    };
    if (Meteor.settings.shopSearch && _.isObject(Meteor.settings.shopSearch.selector)) {
        selector = _.extend(selector, Meteor.settings.shopSearch.selector);
    }
    if (Shops.findOne(selector)) {
        Vatfree.shops.updateShopSearchIndex({_id: doc._id});
    } else {
        ShopSearch.remove({_id: doc._id});
    }
};

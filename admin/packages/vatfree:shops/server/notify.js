Vatfree.notify.sendStatusNotification['shops'] = function (userId, shopId, travellerStatusNotify) {
    try {
        let shop = Shops.findOne({_id: shopId});
        let emailLanguage = shop.language || "en";

        let company = Companies.findOne({_id: shop.companyId}) || {};
        let currency = Currencies.findOne({_id: shop.currencyId });
        let country = Countries.findOne({_id: shop.countryId });

        let activityIds = {
            shopId: shopId
        };

        let templateData = {
            language: emailLanguage,
            shop: shop,
            company: company,
            country: country,
            currency: currency
        };

        let toAddress = shop.email;
        let fromAddress = 'Vatfree.com support <support@vatfree.com>';

        Vatfree.notify.sendNotification(travellerStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage);
    } catch (e) {
        ActivityLogs.insert({
            type: 'task',
            userId: userId,
            subject: 'Sending of invoice to company failed',
            status: 'new',
            shopId: shopId,
            notes: "Error: " + e.message
        });
    }
};

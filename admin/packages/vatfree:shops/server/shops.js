try {
    Shops._ensureIndex({"textSearch": "text"}, {"language_override": "_text_language"});
    Shops._ensureIndex({'poprId': 1}, {'unique': 1, 'sparse': 1});
    Shops._ensureIndex({'companyId': 1});
    Shops._ensureIndex({'id': 1});
    Shops._ensureIndex({'geo': "2dsphere"});
    Shops._ensureIndex({'status': 1});
    Shops._ensureIndex({'partnershipStatus': 1, 'stats.numberOfRequests': -1});
} catch (e) {
    console.error(e);
}

ActivityStream.attachHooks(Shops);

Shops.after.insert(function(userId, doc) {
    updateTextSearch(doc);

    if (doc.billingEntity) {
        delete doc.billingCompanyId;
    } else if (doc.companyId) {
        let billingCompany = Vatfree.billing.getBillingEntityForCompany(doc.companyId);
        if (billingCompany) doc.billingCompanyId = billingCompany._id;
    }

    Meteor.defer(() => {
        Vatfree.notify.processNotificationTriggers(userId, 'shops', '- insert -', false, doc._id);
        Vatfree.adminNotify('New shop: ' + doc.name + ' - ' + (doc.addressFirst || '') + ', ' + (doc.postCode || '') + ' ' + (doc.city || ''));
    });
});

Shops.before.update(function(userId, doc, fieldNames, modifier, options) {
    if (modifier.$set && _.has(modifier.$set, 'billingEntity')) {
        if (modifier.$set.billingEntity) {
            if (!modifier.$unset) modifier.$unset = {};
            modifier.$unset.billingCompanyId = 1;
        } else {
            let billingCompany = Vatfree.billing.getBillingEntityForCompany(doc.companyId);
            if (billingCompany) modifier.$set.billingCompanyId = billingCompany._id;
        }
    } else if (modifier.$set && modifier.$set.companyId) {
        if (!doc.billingEntity) {
            let billingCompany = Vatfree.billing.getBillingEntityForCompany(doc.companyId);
            if (billingCompany) modifier.$set.billingCompanyId = billingCompany._id;
        }
    }

    if (modifier.$set && _.has(modifier.$set, 'partnershipStatus') && doc.partnershipStatus !== modifier.$set.partnershipStatus) {
        if (modifier.$set.partnershipStatus === 'uncooperative' && Meteor.settings.defaults.uncooperativeBillingEmailTextId) {
            modifier.$set.billingEmailTextId = Meteor.settings.defaults.uncooperativeBillingEmailTextId;
        }
    }
});

Shops.after.update(function(userId, doc, fieldNames, modifier, options) {
    Meteor.defer(() => {
        updateTextSearch(doc);

        let allowedStatus = [
            'userPending',
            'shopPending',
            'waitingForOriginal',
            'visualValidationPending',
            'waitingForDoubleCheck',
            'readyForInvoicing'
        ];

        if (_.contains(fieldNames, 'companyId') && modifier.$set && modifier.$set.companyId && modifier.$set.companyId !== this.previous.companyId) {
            // setting company, must make sure all receipts are updated properly
            // but only the ones that have not been invoiced yet !
            Receipts.update({
                shopId: doc._id,
                invoiceId: {
                    $exists: false
                },
                status: {
                    $in: allowedStatus
                }
            },{
                $set: {
                    companyId: modifier.$set.companyId
                }
            },{
                multi: true
            });
        }

        if (_.contains(fieldNames, 'companyId') && modifier.$unset && modifier.$unset.companyId) {
            // setting company, must make sure all receipts are updated properly
            // but only the ones that have not been invoiced yet !
            Receipts.update({
                shopId: doc._id,
                invoiceId: {
                    $exists: false
                },
                status: {
                    $in: allowedStatus
                }
            },{
                $unset: {
                    companyId: 1
                }
            },{
                multi: true
            });
        }

        let shopActivated = _.contains(fieldNames, 'status') && modifier['$set'] && modifier.$set.status === 'active' && modifier.$set.status !== this.previous.status;
        let shopPartnered = _.contains(fieldNames, 'partnershipStatus') && modifier['$set'] && modifier.$set.partnershipStatus === 'partner' && modifier.$set.partnershipStatus !== this.previous.partnershipStatus;
        if (shopActivated || shopPartnered) {
            Receipts.find({
                shopId: doc._id,
                status: 'shopPending'
            }).forEach((receipt) => {
                let newStatus = 'readyForInvoicing';
                Receipts.update({
                    _id: receipt._id
                },{
                    $set: {
                        status: newStatus
                    }
                });
            });
        }
        if (shopPartnered) {
            Receipts.find({
                shopId: doc._id,
                status: 'userPending'
            }).forEach((receipt) => {
                let newStatus = 'waitingForOriginal';
                if (receipt.originalsCheckedBy) {
                    newStatus = 'visualValidationPending';
                }
                Receipts.update({
                    _id: receipt._id
                },{
                    $set: {
                        status: newStatus
                    }
                });
            });
        }

        // Update invoice stats that point back here
        if (
            _.contains(fieldNames, 'partnershipStatus') && modifier['$set'] && modifier.$set.partnershipStatus && modifier.$set.partnershipStatus !== this.previous.partnershipStatus
            ||
            _.contains(fieldNames, 'countryId') && modifier['$set'] && modifier.$set.countryId && modifier.$set.countryId !== this.previous.countryId
        ) {
            // update saved partnershipStatus in invoices
            Invoices.update({
                shopId: doc._id
            },{
                $set: {
                    countryId: (modifier.$set.countryId || this.previous.countryId),
                    partnershipStatus: (modifier.$set.partnershipStatus || this.previous.partnershipStatus)
                }
            },{
                multi: true
            });
            Receipts.update({
                shopId: doc._id
            },{
                $set: {
                    partnershipStatus: (modifier.$set.partnershipStatus || this.previous.partnershipStatus)
                }
            },{
                multi: true
            });
        }

        Vatfree.notify.checkStatusChange('shops', userId, this.previous, doc, fieldNames, modifier);
    });
});

var updateTextSearch = function(doc) {
    let textSearch =  (
        (doc.name || "") + ' ' +
        (doc.id || "") + ' ' +
        (doc.poprId || "") + ' ' +
        (doc.addressFirst || "") + ' ' +
        (doc.postCode || "") + ' ' +
        (doc.city || "")
    ).toLowerCase().latinize();

    Shops.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch
        }
    });
};

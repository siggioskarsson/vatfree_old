import { sendShopRegistrationInfo } from './lib/shop-registration';

if (!Meteor.settings.disableJobs) {
    shopJobs.allow({
        // Grant full permission to any authenticated user
        admin: function (userId, method, params) {
            return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
        }
    });

    Meteor.publish('shop-jobs', function () {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return shopJobs.find({
            status: {
                $ne: 'completed'
            }
        });
    });

    if (Meteor.settings.jobs && Meteor.settings.jobs.shops) {
        Meteor.startup(function () {
            // Start the myJobs queue running
            shopJobs.startJobServer();

            if (Meteor.settings.jobs.shops.registration) {
                console.log('JOB activated: shops.registration');
                shopJobs.processJobs('shopRegistration', Meteor.settings.jobs.shops.registration, (job, cb) => {
                    let data = job.data;

                    console.log('Running shop registration job', data.shopId);
                    sendShopRegistrationInfo(data.shopId, data.contactId, data.signature, (err, fileId) => {
                        if (err) {
                            console.error(err);
                            job.fail(err, {
                                fatal: true
                            });
                            job.done();
                        } else {
                            job.log('created shop registration pdf for ' + data.shopId + ' - created file ' + fileId);
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                });
            }
        });
    }
}

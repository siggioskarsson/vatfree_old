import { Meteor } from "meteor/meteor";

const algoliasearch = require("algoliasearch/src/server/builds/node");
let algoliaIndex = false;
if (Meteor.settings.algolia && Meteor.settings.algolia.applicationId) {
    let algoliaClient = algoliasearch(Meteor.settings.algolia.applicationId, Meteor.settings.algolia.apiKey);
    algoliaIndex = algoliaClient.initIndex(Meteor.settings.algolia.shopIndex);
}

export const clearAlgoliaIndex = function() {
    console.log('-------------- clear Algolia Index --------------');
    if (algoliaIndex) {
        try {
            algoliaIndex.clearIndex((err) => {
                if (err) {
                    console.error(err);
                }
            });
        } catch(e) {
            console.error(e);
        }
    }
};

export const initAlgoliaIndex = function() {
    console.log('-------------- init Algolia Index --------------');
    if (algoliaIndex) {
        try {
            clearAlgoliaIndex();

            let count = 1;
            let docs = [];
            ShopSearch.find({}).forEach((shop) => {
                docs.push(getAlgoliaDoc(shop));
                if ( count % 1000 === 0 ) {
                    algoliaIndex.addObjects(docs, function(err) {
                        if (err) {
                            console.error(err);
                        }
                    });
                    docs = [];
                }
                count++;
            });
            if (docs.length) {
                algoliaIndex.addObjects(docs, function(err) {
                    if (err) {
                        console.error(err);
                    }
                });
            }
            docs = [];
        } catch(e) {
            console.error(e);
        }
    }
};

let partnershipOrder = ['partner', 'affiliate', 'pledger', 'new', 'uncooperative'];
const getAlgoliaDoc = function(doc) {
    let algoliaDoc = JSON.parse(JSON.stringify(doc));
    delete algoliaDoc.g;
    algoliaDoc.objectID = doc._id;
    if (doc.g && doc.g.coordinates) {
        algoliaDoc._geoloc = {
            "lat": doc.g.coordinates[0],
            "lng": doc.g.coordinates[1]
        };
    }
    algoliaDoc.po = partnershipOrder.indexOf(algoliaDoc.p);

    return algoliaDoc;
};

ShopSearch.after.insert(function(userId, doc) {
    if (algoliaIndex) {
        console.log('addObject algolia index', userId, doc._id);
        try {
            let algoliaDoc = getAlgoliaDoc(doc);
            algoliaIndex.addObject(algoliaDoc, function(err) {
                if (err) {
                    console.error(err);
                }
            });
        } catch(e) {
            console.error(e);
        }
    }
});

ShopSearch.after.update(function(userId, doc) {
    if (algoliaIndex) {
        console.log('saveObject algolia index', userId, doc._id);
        try {
            algoliaIndex.saveObject(getAlgoliaDoc(doc), function(err, content) {
                if (err) {
                    console.error(err);
                }
            });
        } catch(e) {
            console.error(e);
        }
    }
});

ShopSearch.after.remove(function(userId, doc) {
    if (algoliaIndex) {
        console.log('deleteObject algolia index', userId, doc._id);
        try {
            algoliaIndex.deleteObject(doc._id, function(err) {
                if (err) {
                    console.error(err);
                }
            });
        } catch(e) {
            console.error(e);
        }
    }
});

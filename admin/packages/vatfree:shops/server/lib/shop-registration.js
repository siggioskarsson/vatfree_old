import handlebars from 'handlebars';

export const sendShopRegistrationInfo = function (shopId, contactId, signature, callback) {
    try {
        const shopData = Shops.findOne({_id: shopId});
        shopData.contact = Contacts.findOne({_id: contactId});
        shopData.country = Countries.findOne({_id: shopData.countryId});
        shopData.signature = signature;

        const template = Assets.getText('templates/pdf/registration.' + shopData.language + '.html');

        let renderedHtml = handlebars.compile(template)(shopData);

        // create PDF with all data and signature and send
        createNewShopSignedPDF(shopData, renderedHtml, function(error, pdfFileId) {
            let attachments = [];
            Files.find({
                _id: pdfFileId,
            }).forEach((file) => {
                attachments.push({
                    filename: file.original.name,
                    path: file.getDirectUrl()
                });
            });

            if (shopData.language === 'nl') {
                attachments.push({
                    filename: 'rules-regulations-nl.pdf',
                    content: Assets.getBinary('templates/pdf/rules-regulations-nl.pdf')
                });
                attachments.push({
                    filename: 'vatfree-terms-nl.pdf',
                    content: Assets.getBinary('templates/pdf/vatfree-terms-nl.pdf')
                });
            } else {
                attachments.push({
                    filename: 'rules-regulations-en.pdf',
                    content: Assets.getBinary('templates/pdf/rules-regulations-en.pdf')
                });
                attachments.push({
                    filename: 'vatfree-terms-en.pdf',
                    content: Assets.getBinary('templates/pdf/vatfree-terms-en.pdf')
                });
            }

            // send PDF with all info to shop owner and vatfree sales
            let mailOptions = {
                from: 'Vatfree support <support@vatfree.com>',
                to: shopData.email,
                attachments: attachments
            };
            let activityIds = {
                shopId: shopData._id,
                contactId: contactId,
            };
            const emailTemplateId = Meteor.settings.defaults.shopRegistrationEmailTemplateId || Meteor.settings.defaults.emailTemplateId;

            let emailTextDoc = EmailTexts.findOne({_id: Meteor.settings.defaults.shopRegistrationEmailTextId});
            if (!emailTextDoc) {
                if (callback) {
                    callback('Email subject and/or text could not be found');
                } else {
                    throw new Meteor.Error(500, 'Email subject and/or text could not be found');
                }
            }

            const emailSubject = emailTextDoc.subject[shopData.language] || emailTextDoc.subject['nl'];
            const emailText = emailTextDoc.text[shopData.language] || emailTextDoc.text['nl'];

            let subject = handlebars.compile(emailSubject)(shopData);
            let text = handlebars.compile(emailText)(shopData);

            Vatfree.sendEmail(mailOptions, emailTemplateId, subject, text, activityIds);

            if (callback) {
                callback(null, pdfFileId);
            }
        });
    } catch(e) {
        console.error(e);
        callback(JSON.parse(JSON.stringify(e, Object.getOwnPropertyNames(e))));
    }
};

const createNewShopSignedPDF = function(shopData, renderedHtml, callback) {
    let pdfFile = new FS.File();
    pdfFile.name('Shop contract ' + shopData.name.replace(/[^a-z]/ig, '_') + '.pdf');
    pdfFile.itemId = shopData._id;
    pdfFile.target = 'shops';
    pdfFile.subType = 'contract';

    return Vatfree.creatPdfAsync(renderedHtml, pdfFile, Vatfree.defaultPhantomJSOptions, callback);
};

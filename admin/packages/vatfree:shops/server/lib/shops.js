import { initAlgoliaIndex } from '../shop-search-algolia';

Vatfree.shops.updateShopSearchIndex = function (selector, bulkInsert = false) {
    let bulk = ShopSearch.rawCollection().initializeOrderedBulkOp();
    let count = 0;
    Shops.find(selector, {
        fields: {
            _id: 1,
            name: 1,
            addressFirst: 1,
            postCode: 1,
            city: 1,
            status: 1,
            partnershipStatus: 1,
            geo: 1,
            stats: 1
        }
    }).forEach((shop) => {
        let insertShop = {
            _id: shop._id,
            n: shop.name,
            a: (shop.addressFirst || '') + '<br/>' + (shop.postCode || '') + ' ' + (shop.city || ''),
            p: shop.partnershipStatus,
            g: shop.geo,
            r: shop.stats ? (shop.stats.numberOfRequests || 0) : 0
        };
        insertShop.t =  (
            (shop.name || "") + ' ' +
            (shop.addressFirst || "") + ' ' +
            (shop.postCode || "") + ' ' +
            (shop.city || "")
        ).toLowerCase().latinize();

        if (bulkInsert) {
            bulk.insert(insertShop);
            if ( count % 1000 === 0 ) {
                bulk.execute();
                bulk = ShopSearch.rawCollection().initializeOrderedBulkOp();
            }
        } else {
            // check whether the doc is really different
            let currentDoc = ShopSearch.findOne({_id: shop._id});
            if (!currentDoc) {
                ShopSearch.insert(insertShop);
            } else if (!_.isEqual(currentDoc, insertShop)) {
                ShopSearch.update({_id: shop._id}, insertShop);
            }
        }
        count++;
    });

    if (bulkInsert) {
        bulk.execute();
    }

    if (bulkInsert) {
        initAlgoliaIndex();
    }
};

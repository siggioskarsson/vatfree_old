FlowRouter.route('/shops', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "shops"});
    }
});

FlowRouter.route('/shop/:shopId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "shopEdit"});
    }
});

Meteor.methods({
    'get-faq-groups'() {
        let faqGroups = {};
        FAQs.find().forEach((faq) => {
            if (!_.has(faqGroups, faq.group)) {
                faqGroups[faq.group] = 1;
            }
        });
        return _.keys(faqGroups);
    },
    'delete-faq-group'(faqGroupId) {
        if (!Vatfree.userIsInRole(this.userId, 'faqs-read')) {
            throw new Meteor.Error(404, "access denied");
        }

        const faqGroupCount = FAQs.find({groupId: faqGroupId}).count();
        console.log('faqGroupCount', faqGroupCount);
        if (faqGroupCount > 0) {
            throw new Meteor.Error(500, "This group is still in use, cannot remove");
        }

        FAQGroups.remove({
            _id: faqGroupId
        });
    }
});

ActivityStream.attachHooks(FAQGroups);
FAQGroups.after.insert(function(userId, doc) {
    updateTextSearch(doc);
});
FAQGroups.after.update(function(userId, doc, fieldNames, modifier, options) {
    updateTextSearch(doc);
});

var updateTextSearch = function(doc) {
    let textSearch =  (
        (doc.description || "")
    );

    _.each(doc.name, (languageText, language) => {
        textSearch += ' ' + languageText;
    });

    FAQGroups.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch.toLowerCase().latinize()
        }
    });
};

ActivityStream.attachHooks(FAQs);

FAQs.before.insert(function(userId, doc) {
    if (doc.groupId) {
        const fagGroup = FAQGroups.findOne({_id: doc.groupId});
        doc.group = fagGroup.name.en;
    }
});

FAQs.after.insert(function(userId, doc) {
    updateTextSearch(doc);
});

FAQs.before.update(function(userId, doc, fieldNames, modifier, options) {
    if (modifier.$set && modifier.$set.groupId) {
        const faqGroup = FAQGroups.findOne({_id: modifier.$set.groupId});
        modifier.$set.group = faqGroup.name.en;
    }
});

FAQs.after.update(function(userId, doc, fieldNames, modifier, options) {
    updateTextSearch(doc);
});

var updateTextSearch = function(doc) {
    let textSearch =  (
        (doc.name || "") + ' ' +
        (doc.description || "")
    );

    _.each(doc.subject, (languageText, language) => {
        textSearch += ' ' + languageText;
    });
    _.each(doc.text, (languageText, language) => {
        textSearch += ' ' + languageText;
    });

    FAQs.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch.toLowerCase().latinize()
        }
    });
};

FAQGroups = new Meteor.Collection('faq-groups');
FAQGroups.attachSchema(new SimpleSchema({
    name: {
        type: Object,
        label: "All the translation names for this group",
        optional: false,
        blackbox: true
    },
    description: {
        type: String,
        label: "Description of this faq",
        optional: true
    },
    order: {
        type: Number,
        label: "Order of the group in the faq list",
        optional: false,
        autoValue: function() {
            if (this.isInsert) {
                return FAQGroups.find().count();
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: FAQGroups.find().count()
                };
            }
        }
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
FAQGroups.attachSchema(CreatedUpdatedSchema);

FAQGroups.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

FAQs = new Meteor.Collection('faqs');
FAQs.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Name of the faq",
        optional: false
    },
    description: {
        type: String,
        label: "Description of this faq",
        optional: true
    },
    group: {
        type: String,
        label: "The group this FAQ is a part of",
        defaultValue: 'General'
    },
    groupId: {
        type: String,
        label: "The group this FAQ is a part of",
        optional: false
    },
    subject: {
        type: Object,
        label: "All the translations for this subject",
        optional: true,
        blackbox: true
    },
    text: {
        type: Object,
        label: "All the translations for this text",
        optional: true,
        blackbox: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
FAQs.attachSchema(CreatedUpdatedSchema);

FAQs.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

import { FAQHelpers } from './helpers';

Template.FAQList.helpers(Vatfree.templateHelpers.helpers);
Template.FAQList.helpers(FAQHelpers);
Template.FAQList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/faq/:itemId', {itemId: this._id});
    }
});

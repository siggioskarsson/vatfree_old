import { FAQHelpers } from './helpers';

Template.FAQInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.FAQInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.FAQInfo.helpers(FAQHelpers);
Template.FAQInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

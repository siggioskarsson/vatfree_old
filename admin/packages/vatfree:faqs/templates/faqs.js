import { FAQHelpers } from './helpers';

Template.FAQ.onCreated(Vatfree.templateHelpers.onCreated(FAQs, '/faqs/', '/faq/:itemId'));
Template.FAQ.onRendered(Vatfree.templateHelpers.onRendered());
Template.FAQ.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.FAQ.helpers(Vatfree.templateHelpers.helpers);
Template.FAQ.helpers(FAQHelpers);
Template.FAQ.events(Vatfree.templateHelpers.events());

import { FAQGroupHelpers } from './helpers';

Template.FAQGroup.onCreated(Vatfree.templateHelpers.onCreated(FAQGroups, '/faq-groups/', '/faq-group/:itemId'));
Template.FAQGroup.onCreated(function() {
    this.sort.set({order: 1});
});
Template.FAQGroup.onRendered(Vatfree.templateHelpers.onRendered());
Template.FAQGroup.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.FAQGroup.helpers(Vatfree.templateHelpers.helpers);
Template.FAQGroup.helpers(FAQGroupHelpers);
Template.FAQGroup.events(Vatfree.templateHelpers.events());

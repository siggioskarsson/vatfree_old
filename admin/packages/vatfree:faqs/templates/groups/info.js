import { FAQGroupHelpers } from './helpers';

Template.FAQGroupInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.FAQGroupInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.FAQGroupInfo.helpers(FAQGroupHelpers);
Template.FAQGroupInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

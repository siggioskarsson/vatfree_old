Template.addFAQGroupModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-faq').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-faq')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-faq').on('shown.bs.modal', function () {
            //
        });
        $('#modal-add-faq').modal('show');
    });
});

Template.addFAQGroupModal.onDestroyed(function() {
    $('#modal-add-faq').modal('hide');
});

Template.addFAQGroupModal.helpers({
});

Template.addFAQGroupModal.events({
    'click .cancel-add-faq'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-faq-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        FAQGroups.insert(formData);
        template.hideModal();
    }
});

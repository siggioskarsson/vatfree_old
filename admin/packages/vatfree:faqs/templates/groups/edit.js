import autosize from 'meteor/vatfree:core-templates/templates/autosize';

Template.FAQGroupEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('faq-groups_item', FlowRouter.getParam('itemId'));
    });
});

Template.FAQGroupEdit.helpers({
    itemDoc() {
        return FAQGroups.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    }
});

Template.FAQGroupEditForm.onRendered(function() {
    Tracker.afterFlush(() => {
        autosize(this.$('textarea'));
    });
});

Template.FAQGroupEditForm.helpers({
    getFAQGroupLanguages() {
        return Vatfree.languages.getTravellerLanguages();
    },
    getName() {
        const template = Template.instance();
        if (!template.data) return "";

        return template.data.name[this.code] || "";
    }
});

Template.FAQGroupEditForm.events({
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/faq-groups');
    },
    'click .delete-faq-group'(e, template) {
        e.preventDefault();
        Meteor.call('delete-faq-group', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success("FAQ group deleted");
                FlowRouter.go('/faq-groups');
            }
        });
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (!formData.name.en) {
            toastr.error("Name in English is required");
            return false;
        }

        FAQGroups.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('FAQ Group saved!')
            }
        });
    }
});

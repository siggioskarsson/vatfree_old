import { FAQGroupHelpers } from './helpers';

Template.FAQGroupList.helpers(Vatfree.templateHelpers.helpers);
Template.FAQGroupList.helpers(FAQGroupHelpers);
Template.FAQGroupList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/faq-group/:itemId', {itemId: this._id});
    }
});

Template.FAQGroupList.helpers({
    getMissingTranslations() {
        let FAQGroup = this;
        let missingTranslations = [];
        let languages = Vatfree.languages.getTravellerLanguages();
        _.each(languages, (language) => {
            if (!FAQGroup.name || !FAQGroup.name[language.code]) {
                missingTranslations.push(language.code);
            }
        });

        return missingTranslations.join(', ');
    }
});

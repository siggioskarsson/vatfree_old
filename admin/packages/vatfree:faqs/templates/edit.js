import autosize from 'meteor/vatfree:core-templates/templates/autosize';

Template.FAQEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('faqs_item', FlowRouter.getParam('itemId'));
        this.subscribe('faq-groups');
    });
});

Template.FAQEdit.helpers({
    itemDoc() {
        return FAQs.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    getBreadcrumbTitle() {
        return this.name + ' (' + this.code + ')';
    }
});

Template.FAQEditForm.onRendered(function() {
    this.autorun(() => {
        this.subscribe('faqs_item', this.data._id);
    });

    Tracker.afterFlush(() => {
        autosize(this.$('textarea'));
    });
});

Template.FAQEditForm.helpers({
    getFAQLanguages() {
        return Vatfree.languages.getTravellerLanguages();
    },
    getTextForLanguage() {
        let data = Template.instance().data;
        if (data.text && data.text[this.code]) {
            return data.text[this.code];
        }
    },
    getSubjectForLanguage() {
        let data = Template.instance().data;
        if (data.subject && data.subject[this.code]) {
            return data.subject[this.code];
        }
    }
});

Template.FAQEditForm.events({
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/faqs');
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (!formData.groupId) {
            toastr.error("Group is mandatory");
            return false;
        }

        FAQs.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('FAQ saved!')
            }
        });
    }
});

UI.registerHelper('getFAQName', function(FAQId) {
    if (!FAQId) FAQId = this.FAQId;

    let FAQ = FAQs.findOne({_id: FAQId});
    if (FAQ) {
        return FAQ.name;
    }

    return "";
});

UI.registerHelper('getFAQs', function(group) {
    let selector = {};
    if (group) {
        selector.group = group;
    }
    return FAQs.find(selector, {
        sort: {
            name: 1
        }
    });
});

UI.registerHelper('getFAQGroupName', function(FAQGroupId) {
    if (!FAQGroupId) FAQGroupId = this.FAQGroupId;

    let FAQGroup = FAQGroups.findOne({_id: FAQGroupId});
    if (FAQGroup) {
        return FAQGroup.name.en;
    }

    return "";
});

UI.registerHelper('getFAQGroups', function() {
    let selector = {};
    return FAQGroups.find(selector, {
        sort: {
            order: 1
        }
    });
});

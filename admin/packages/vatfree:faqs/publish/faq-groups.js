Meteor.publish('faq-groups', function(searchTerm, selector, sort, limit, offset) {
    if (!Vatfree.userIsInRole(this.userId, 'faqs-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
        let searchTerms = searchTerm.split(' ');

        selector['$and'] = [];
        _.each(searchTerms, (s) => {
            selector['$and'].push({textSearch: new RegExp(s)});
        });
    }

    return FAQGroups.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset
    });
});

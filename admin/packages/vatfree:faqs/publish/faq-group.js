Meteor.publish('faq-groups_item', function(itemId) {
    check(itemId, String);
    if (!Vatfree.userIsInRole(this.userId, 'faqs-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    return FAQGroups.find({
       _id: itemId
    });
});

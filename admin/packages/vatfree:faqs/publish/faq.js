Meteor.publish('faqs_item', function(itemId) {
    check(itemId, String);
    if (!Vatfree.userIsInRole(this.userId, 'faqs-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    return FAQs.find({
       _id: itemId
    });
});

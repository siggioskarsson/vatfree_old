FlowRouter.route('/faqs', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "FAQ"});
    }
});

FlowRouter.route('/faq/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "FAQEdit"});
    }
});

FlowRouter.route('/faq-groups', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "FAQGroup"});
    }
});

FlowRouter.route('/faq-group/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "FAQGroupEdit"});
    }
});

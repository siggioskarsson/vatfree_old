Package.describe({
    name: 'vatfree:faqs',
    summary: 'Vatfree faq package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'flemay:less-autoprefixer@1.2.0',
        'rubaxa:sortable@1.3.0',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core'
    ]);

    // shared files
    api.addFiles([
        'faq-groups.js',
        'faqs.js',
    ]);

    // server files
    api.addFiles([
        'server/faqs.js',
        'server/methods.js',
        'publish/faqs.js',
        'publish/faq-groups.js',
        'publish/faqs-list.js',
        'publish/faq.js',
        'publish/faq-group.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/faq.less',
        'templates/ui-helpers.js',
        'templates/helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/faqs.html',
        'templates/faqs.js',
        'templates/groups/helpers.js',
        'templates/groups/add.html',
        'templates/groups/add.js',
        'templates/groups/edit.html',
        'templates/groups/edit.js',
        'templates/groups/info.html',
        'templates/groups/info.js',
        'templates/groups/list.html',
        'templates/groups/list.js',
        'templates/groups/faqs.html',
        'templates/groups/faqs.js'
    ], 'client');

    api.export([
        'FAQs',
        'FAQGroups'
    ]);
});

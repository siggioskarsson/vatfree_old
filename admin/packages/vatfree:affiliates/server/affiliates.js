Affiliates = Meteor.users;
Affiliates._ensureIndex({'affiliate.code': 1},  {unique: true, sparse: true});
Affiliates._ensureIndex({'affiliate.status': 1});

// Add index to activity stream collection for affiliates
ActivityStream.collection._ensureIndex({'securityContext.affiliateId': 1});

Affiliates.after.insert(function (userId, doc) {
    Meteor.defer(() => {
        if (_.contains(doc.roles.__global_roles__, 'affiliate')) {
            Vatfree.notify.processNotificationTriggers(userId, 'affiliates', '- insert -', false, doc._id);
        }
    });
});

Affiliates.after.update(function (userId, doc, fieldNames, modifier, options) {
    if (_.contains(doc.roles.__global_roles__, 'affiliate')) {
        Meteor.defer(() => {
            if (options.notify !== false) {
                if (_.contains(fieldNames, 'affiliate') && modifier.$set && modifier.$set['affiliate.status'] && modifier.$set['affiliate.status'] !== this.previous.affiliate.status) {
                    Vatfree.notify.processNotificationTriggers(userId, 'affiliates', modifier.$set['affiliate.status'], this.previous.affiliate.status, doc._id);
                }
            }
        });
    }
});

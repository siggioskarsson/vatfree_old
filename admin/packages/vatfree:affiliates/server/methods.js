import { Meteor } from "meteor/meteor";

const getNewAffiliateCode = function(length) {
    let code = "";
    let chars = "abcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < length; i++) {
        code += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return code;
};

Meteor.methods({
    'search-affiliates'(searchTerm, limit, offset) {
        this.unblock();
        if (!Vatfree.userIsInRole(this.userId, 'affiliates-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {};
        selector['roles.__global_roles__'] = 'affiliate';
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let affiliates = [];
        Affiliates.find(selector, {
            sort: {
                'profile.name': 1
            },
            limit: limit,
            offset: offset
        }).forEach((affiliate) => {
            if (!affiliate.affiliate) affiliate.affiliate = {};
            affiliates.push({
                text: affiliate.profile.name + ' (' + affiliate.affiliate.code + ') - ' + (affiliate.profile.addressFirst || "") + ', ' + (affiliate.profile.postCode || "") + ' ' + (affiliate.profile.city || ""),
                id: affiliate._id
            });
        });

        return affiliates;
    },
    'affiliates-insert'(profile) {
        if (!Vatfree.userIsInRole(this.userId, 'affiliates-create')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const existingUser = Meteor.users.findOne({
            'emails.address': profile.email
        });

        if (existingUser) {
            const setData = {
                $set: {
                    affiliate: {
                        code: getNewAffiliateCode(7),
                            status: 'new'
                    },
                }
            };

            if (existingUser.roles.__global_roles__) {
                if (_.isString(existingUser.roles.__global_roles__)) {
                    setData.$set['roles.__global_roles__'] = [existingUser.roles.__global_roles__, 'affiliate'];
                } else if (_.isArray(existingUser.roles.__global_roles__)) {
                    setData.$addToSet = {
                        'roles.__global_roles__': 'affiliate'
                    };
                } else {
                    throw new Meteor.Error(500, 'Users security roles are not OK. Contact IT support.');
                }
            } else {
                setData.$set['roles.__global_roles__'] = ['affiliate'];
            }

            Meteor.users.update({
                _id: existingUser._id
            }, setData);

            Meteor.defer(() => {
                Vatfree.notify.processNotificationTriggers(this.userId, 'affiliates', '- insert -', false, existingUser._id);
            });

            return existingUser._id;
        } else {
            let privateData = {};
            if (profile.notes) {
                privateData.notes = profile.notes;
                delete profile.notes;
            }

            if (!_.has(profile, 'countryId')) {
                profile.countryId = Meteor.settings.defaults.countryId;
            }

            return Meteor.users.insert({
                profile: profile,
                private: privateData,
                affiliate: {
                    code: getNewAffiliateCode(7),
                    status: 'new'
                },
                emails: [
                    {
                        address: profile.email,
                        verified: false
                    }
                ],
                roles: {
                    __global_roles__: ['affiliate']
                }
            });
        }
    },
    'affiliates-update'(affiliateId, profileData) {
        if (!Vatfree.userIsInRole(this.userId, 'affiliates-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const affiliate = Meteor.users.findOne({_id: affiliateId});

        const setData = {};
        _.each(profileData['$set'], (values, docKey) => {
            _.each(values, (value, key) => {
                setData[docKey + '.' + key] = value;
            });
        });
        if (setData['profile.notes']) {
            setData['private.notes'] = setData['profile.notes'];
            delete setData['profile.notes'];
        }

        const unsetData = {};
        _.each(profileData['$unset'], (values, docKey) => {
            _.each(values, (value, key) => {
                unsetData[docKey + '.' + key] = value;
            });
        });

        if (setData['profile.email'] && setData['profile.email'] !== affiliate.profile.email) {
            const existingUser = Meteor.users.findOne({
                'emails.address': setData['profile.email']
            });
            if (existingUser) {
                throw new Meteor.Error(500, "Email address is alread registered for another user.");
            }

            setData['emails'] = [
                {
                    address: setData['profile.email'],
                    verified: false
                }
            ];
        }

        return Contacts.update({
            _id: affiliateId
        }, {
            $set: setData,
            $unset: unsetData
        });
    }
});

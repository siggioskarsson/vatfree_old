Vatfree.notify.sendStatusNotification['affiliates'] = function (userId, affiliateId, affiliateStatusNotify) {
    try {
        let affiliate = Affiliates.findOne({_id: affiliateId}) || {};
        let emailLanguage = affiliate.profile.language || "en";

        let country = Countries.findOne({_id: affiliate.profile.countryId }) || {};

        let activityIds = {
            affiliateId: affiliateId
        };

        let affiliateInfo = {...affiliate.profile, ...affiliate.affiliate};
        affiliate.profile.country = country;
        let templateData = {
            language: emailLanguage,
            affiliate: affiliateInfo
        };

        let toAddress = affiliate.profile.email;
        let fromAddress = 'Vatfree.com affiliate program <affiliate@vatfree.com>';

        Vatfree.notify.sendNotification(affiliateStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage, affiliate);
    } catch (e) {
        ActivityLogs.insert({
            type: 'task',
            userId: userId,
            subject: 'Sending of status update to affiliate failed',
            status: 'new',
            affiliateId: affiliateId,
            notes: "Error: " + e.message
        });
    }
};

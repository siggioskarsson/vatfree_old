FlowRouter.route('/affiliates', {
    action: function() {
        import("meteor/vatfree:affiliates/templates")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "affiliates"});
            });
    }
});

FlowRouter.route('/affiliate/:affiliateId', {
    action: function() {
        import("meteor/vatfree:affiliates/templates")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "affiliateEdit"});
            });
    }
});

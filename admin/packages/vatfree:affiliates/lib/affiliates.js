Vatfree.affiliates = {};

Vatfree.affiliates.defaultServiceFee = 10;
Vatfree.affiliates.defaultTravellerDiscount = 10;

Vatfree.affiliates.getServiceFee = function(affiliateId) {
    let affiliate = Affiliates.findOne({_id: affiliateId}) || {};
    if (!affiliate.affiliate) affiliate.affiliate = {};
    return (_.has(affiliate.affiliate, 'serviceFee') ? affiliate.affiliate.serviceFee : Vatfree.affiliates.defaultServiceFee);
};

Vatfree.affiliates.getTravellerDiscount = function(affiliateId) {
    let affiliate = Affiliates.findOne({_id: affiliateId}) || {};
    if (!affiliate.affiliate) affiliate.affiliate = {};
    return (_.has(affiliate.affiliate, 'travellerDiscount') ? affiliate.affiliate.travellerDiscount : Vatfree.affiliates.defaultTravellerDiscount);
};

Vatfree.affiliates.icons = function() {
    return {
        'new': 'icon icon-new',
        'active': 'icon icon-affiliates',
        'silver': 'icon icon-affiliates',
        'gold': 'icon icon-affiliates'
    };
};

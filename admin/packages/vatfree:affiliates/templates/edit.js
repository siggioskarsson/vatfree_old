import './edit.html';

Template.affiliateEdit.onCreated(function () {

});

Template.affiliateEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('affiliates_item', FlowRouter.getParam('affiliateId') || '');
    });
});

Template.affiliateEdit.helpers({
    affiliateDoc() {
        return Affiliates.findOne({
            _id: FlowRouter.getParam('affiliateId')
        });
    },
    getBreadcrumbTitle() {
        return this.profile.name + ', ' + this.profile.addressFirst + ', ' + this.profile.postalCode + ' ' + this.profile.city;
    }
});

Template.affiliateEditForm.onCreated(function () {
    this.addingReceipt = new ReactiveVar();
    this.addingInvoice = new ReactiveVar();
    this.addingContact = new ReactiveVar();
    this.addingActivityLog = new ReactiveVar();
});

Template.affiliateEditForm.events(Vatfree.templateHelpers.events());
Template.affiliateEditForm.onRendered(function () {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('affiliate-receipts', data._id);
        this.subscribe('affiliate-payments', data._id);
        this.subscribe('activity-logs', "", {affiliateId: data._id});
    });

    let formatPartnershipStatus = function (status) {
        let button = '';
        switch (status.id) {
            case 'pledger':
                button = 'VFCircleBlue.svg';
                break;
            case 'partner':
                button = 'VFCircleShade.svg';
                break;
            case 'uncooperative':
            default:
                button = 'VFCircleNonrefund.svg';
                break;
        }

        if (button) {
            return $(
                '<span class="select2-item"><img src="/Logos/' + button + '" class="img-responsive" /> ' + status.text + '</span>'
            );
        } else {
            return status.text;
        }
    };
    Tracker.afterFlush(() => {
        this.$('select[name="partnershipStatus"]').select2({
            templateSelection: formatPartnershipStatus,
            templateResult: formatPartnershipStatus
        });

        this.$('select[name="countryId"]').select2();
    });
});

Template.affiliateEditForm.helpers({
    isDisabled() {
        return !Vatfree.userIsInRole(Meteor.userId(), 'affiliates-update');
    },
    addingReceipt() {
        return Template.instance().addingReceipt.get();
    },
    addingInvoice() {
        return Template.instance().addingInvoice.get();
    },
    addingContact() {
        return Template.instance().addingContact.get();
    },
    getReceipts() {
        return Receipts.find({
            affiliateId: this._id
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getInvoices() {
        return Invoices.find({
            affiliateId: this._id
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getContacts() {
        return Contacts.find({
            'profile.affiliateId': this._id
        },{
            sort: {
                'profile.name': 1
            }
        });
    },
    isCountrySelected() {
        let template = Template.instance();
        return template.data.profile.countryId === this._id;
    },
    addingActivityLog() {
        return Template.instance().addingActivityLog.get();
    },
    getActivityLog() {
        return ActivityLogs.find({
            affiliateId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getFileContext() {
        return {
            itemId: this._id,
            target: 'affiliates'
        };
    },
    getStandardServiceFee() {
        return Vatfree.affiliates.defaultServiceFee;
    },
    getStandardDiscount() {
        return Vatfree.affiliates.defaultTravellerDiscount;
    }
});

Template.affiliateEditForm.events({
    'click .add-activity-log'(e, template) {
        e.preventDefault();
        template.addingActivityLog.set(true);
    },
    'click .add-contact'(e, template) {
        e.preventDefault();
        template.addingContact.set(true);
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/affiliates');
    },
    'submit form[name="affiliate-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (this.profile.email && formData.profile.email !== this.profile.email) {
            if (!confirm("Email address has been changed. Are you sure you want to continue. The affiliate will get an email te verify the new email.")) {
                return false;
            }
        }

        const unsetData = {};
        if (_.has(formData.affiliate, 'serviceFee')) {
            if (formData.affiliate.serviceFee) {
                formData.affiliate.serviceFee = Math.round(formData.affiliate.serviceFee);
            } else {
                unsetData['affiliate.serviceFee'] = true;
            }
        }
        if (_.has(formData.affiliate, 'travellerDiscount')) {
            if (formData.affiliate.travellerDiscount) {
                formData.affiliate.travellerDiscount = Math.round(formData.affiliate.travellerDiscount);
            } else {
                unsetData['affiliate.travellerDiscount'] = true;

            }
        }

        let setData = {
            $set: formData
        };

        if (_.size(unsetData)) {
            setData['$unset'] = unsetData;
        }

        Meteor.call('affiliates-update', this._id, setData, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Affiliate saved!')
            }
        });
    },
    'click .add-receipt'(e, template) {
        e.preventDefault();
        template.addingReceipt.set(true);
    },
    'click .add-invoice'(e, template) {
        e.preventDefault();
        template.addingInvoice.set(true);
    }
});

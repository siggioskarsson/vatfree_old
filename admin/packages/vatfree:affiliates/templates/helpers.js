export const affiliateHelpers = {
    getSelector: function (template) {
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
            let searchTerms = searchTerm.split(' ');

            selector['$and'] = [];
            _.each(searchTerms, (s) => {
                selector['$and'].push({textSearch: new RegExp(s)});
            });
        }

        Vatfree.search.addStatusFilter.call(template, selector);

        return selector;
    },
    getAffiliates() {
        let template = Template.instance();
        let selector = affiliateHelpers.getSelector(template);

        return Affiliates.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    }
};

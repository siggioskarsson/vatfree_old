import { affiliateHelpers } from './helpers';
import './info.html';

Template.affiliateInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.affiliateInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {affiliateId: data._id}, null, limit, offset);
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.affiliateInfo.helpers(affiliateHelpers);
Template.affiliateInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            $or: [
                {
                    'securityContext.affiliateId': this._id
                },
                {
                    'activity.target.itemId': this._id
                }
            ]
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

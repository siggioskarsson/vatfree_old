import './add.html';

Template.addAffiliateModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-affiliate').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-affiliate')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-affiliate').on('shown.bs.modal', function () {
            //
        });
        $('#modal-add-affiliate').modal('show');
    });
});

Template.addAffiliateModal.onDestroyed(function() {
    this.hideModal();

    $('body').removeClass('modal-open');
    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});


Template.addAffiliateModal.events({
    'click .cancel-add-affiliate'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-affiliate-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        Meteor.call('affiliates-insert', formData.profile, (err, contactId) => {
            if (err) {
                toastr.error(err.message);
            } else {
                template.hideModal();
                FlowRouter.go('/affiliate/' + contactId);
            }
        });
    }
});

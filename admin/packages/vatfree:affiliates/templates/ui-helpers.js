UI.registerHelper('getAffiliateDescription', function(affiliateId) {
    if (!affiliateId) affiliateId = this.affiliateId;

    let affiliate = Affiliates.findOne({_id: affiliateId});
    if (affiliate) {
        let country = Countries.findOne({_id: affiliate.profile.countryId}) || {};
        return affiliate.profile.name + ' (' + affiliate.affiliate.code + ') - ' + affiliate.profile.addressFirst + ', ' + affiliate.profile.postalCode + ' ' + affiliate.profile.city + ', ' + country.name;
    }

    return "";
});

UI.registerHelper('getAffiliateName', function(affiliateId) {
    if (!affiliateId) affiliateId = this.affiliateId;

    let affiliate = Affiliates.findOne({_id: affiliateId});
    if (affiliate) {
        return affiliate.profile.name;
    }

    return "";
});

UI.registerHelper('getAffiliateStatusIcon', function(status) {
    let icon = Vatfree.affiliates.icons();
    return _.has(icon, status) ? icon[status] : "circle-o";
});

UI.registerHelper('getAffiliateStatusOptions', function() {
    return _.clone(Meteor.users.simpleSchema()._schema['affiliate.status'].allowedValues);
});

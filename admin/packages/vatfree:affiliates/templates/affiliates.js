import { affiliateHelpers } from './helpers';
import './affiliates.html';

Template.affiliates.onCreated(Vatfree.templateHelpers.onCreated(Affiliates, '/affiliates/', '/affiliate/:itemId'));
Template.affiliates.onRendered(Vatfree.templateHelpers.onRendered());
Template.affiliates.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.affiliates.helpers(Vatfree.templateHelpers.helpers);
Template.affiliates.helpers(affiliateHelpers);
Template.affiliates.events(Vatfree.templateHelpers.events());

Template.affiliates.onCreated(function() {
    this.statusAttribute = 'affiliate.status';
    this.todoStatus = [
        'new'
    ];
});

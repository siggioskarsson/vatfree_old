import { affiliateHelpers } from './helpers';
import './list.html';

Template.affiliatesList.helpers(Vatfree.templateHelpers.helpers);
Template.affiliatesList.helpers(affiliateHelpers);
Template.affiliatesList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/affiliate/:itemId', {itemId: this._id});
    }
});

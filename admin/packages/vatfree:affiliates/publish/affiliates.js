Meteor.publishComposite('affiliates', function(searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};
    selector['roles.__global_roles__'] = 'affiliate';

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        collectionName: "affiliates",
        find: function () {
            return Affiliates.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset,
                fields: {
                    profile: 1,
                    private: 1,
                    affiliate: 1,
                    status: 1,
                    "services.twitter.profile_image_url_https": 1,
                    "services.facebook.id": 1,
                    "services.google.picture": 1,
                    "services.github.username": 1,
                    "services.instagram.profile_picture": 1,
                    "services.linkedin.pictureUrl": 1,
                    createdAt: 1,
                    updatedAt: 1,
                    textSearch: 1
                }
            });
        },
        children: [
            {
                find: function (item) {
                    return Files.find({
                        itemId: item._id,
                        target: 'affiliates',
                        category: 'logo'
                    });
                }
            }
        ]
    }
});

Meteor.publishComposite('affiliates_item', function(affiliateId) {
    check(affiliateId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        collectionName: 'affiliates',
        find: function () {
            return Affiliates.find({
                _id: affiliateId,
                'roles.__global_roles__': 'affiliate'
            });
        },
        children: [
            {
                collectionName: 'contacts',
                find: function (item) {
                    return Contacts.find({
                        'profile.affiliateId': item._id
                    });
                }
            },
            {
                find: function (item) {
                    return Files.find({
                        itemId: item._id,
                        target: 'affiliates'
                    });
                }
            }
        ]
    };
});

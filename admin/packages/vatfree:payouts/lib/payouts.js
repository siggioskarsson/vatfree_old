/* global Vatfree: true */

Vatfree.payouts = {};

Vatfree.payouts.status = function() {
    return {
        'preCheck': 'default',
        'new': 'default',
        'sentToPaymentProvider': 'info',
        'sentToApiProvider': 'info',
        'partiallyPaid': 'warning',
        'paid': 'primary',
        'cancelled': 'danger',
        'error': 'danger'
    };
};

Vatfree.payouts.icons = function() {
    return {
        'preCheck': 'icon icon-waiting4admincheck',
        'new': 'icon icon-payoutnew',
        'sentToPaymentProvider': 'icon icon-payoutsenttopayoutprov',
        'sentToApiProvider': 'icon icon-payout-to-api',
        'partiallyPaid': 'icon icon-payoutproblem',
        'paid': 'icon icon-payoutok',
        'cancelled': 'icon icon-payoutcancel',
        'error': 'icon icon-payout-error'
    };
};

Vatfree.payouts.validatePayout = function(payoutMethod, payoutNr) {
    if (!_.contains(Vatfree.payments.methods, payoutMethod)) {
        return false;
    }

    switch(payoutMethod) {
        case 'creditcard':
            return Vatfree.payouts.validateCreditcardPayout(payoutNr);
            break;
        case 'paypal':
            return Vatfree.payouts.validatePaypalPayout(payoutNr);
            break;
        case 'sepa':
            return Vatfree.payouts.validateSepaPayout(payoutNr);
            break;
        case 'wechat':
            return Vatfree.payouts.validateWeChatPayout(payoutNr);
            break;
        case 'alipay':
            return Vatfree.payouts.validateAliPayPayout(payoutNr);
            break;
        case 'unionPay':
            return Vatfree.payouts.validateUnionPayPayout(payoutNr);
            break;
    }
};

Vatfree.payouts.validateCreditcardPayout = function(payoutNr) {
    let valid = require('card-validator');
    let numberValidation = valid.number(payoutNr);
    return numberValidation.isValid && _.contains(['mastercard', 'master-card', 'visa'], numberValidation.card.type);
};

Vatfree.payouts.validatePaypalPayout = function (payoutNr) {
    return IsValidEmail(payoutNr);
};

Vatfree.payouts.validIbanCountries = [
 'FI', 'AT', 'BE', 'BG', 'ES', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'FR', 'DE', 'GI', 'GR', 'FR', 'HU', 'IS', 'IE', 'IT', 'LV', 'LI', 'LT', 'LU', 'MT', 'FR', 'FR', 'MC', 'NL', 'NO', 'PL', 'PT', 'FR', 'RO', 'FR', 'FR', 'FR', 'SK', 'SI', 'ES', 'SE', 'CH', 'GB'
];
Vatfree.payouts.validateSepaPayout = function(payoutNr) {
    let IBAN = require('iban');

    let country = payoutNr.substr(0,2).toUpperCase();
    if (!_.contains(Vatfree.payouts.validIbanCountries, country)) {
        console.log('invalid country', country);
        return false;
    }

    return IBAN.isValid(payoutNr);
};

/*
    WeChat IDs must be between 6 to 20 characters and must begin with an alphabetical character.
    Acceptable characters include letters, numbers, underscores ("_") and dashes ("-").
 */
Vatfree.payouts.validateWeChatPayout = function(payoutNr) {
    return !!payoutNr.match(/^[a-z][a-z0-9_-]{5,19}$/i);
};

Vatfree.payouts.validateAliPayPayout = function(payoutNr) {
    return true;
};

Vatfree.payouts.validateUnionPayPayout = function(payoutNr) {
    return true;
};

FlowRouter.route('/payouts', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "payouts"});
    }
});

FlowRouter.route('/payout/:payoutId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "payoutEdit"});
    }
});

FlowRouter.route('/payout-batches', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "payoutBatches"});
    }
});

FlowRouter.route('/payout-batch/:payoutBatchId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "payoutBatch"});
    }
});

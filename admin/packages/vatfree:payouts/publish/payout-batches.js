Meteor.publish('payout-batches', function (searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return PayoutBatches.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset
    },{
        fields: {
            statusDates: false,
            notes: false,
        }
    });
});

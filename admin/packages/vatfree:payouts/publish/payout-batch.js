Meteor.publishComposite('payout-batch', function(payoutBatchId) {
    check(payoutBatchId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return PayoutBatches.find({
                _id: payoutBatchId
            });
        },
        children: [
            {
                find: function (payoutBatch) {
                    return Payouts.find({
                        _id: {
                            $in: payoutBatch.payoutIds
                        }
                    });
                },
                children: [
                    {
                        collectionName: "travellers",
                        find: function (payout) {
                            return Meteor.users.find({
                                _id: payout.travellerId
                            },{
                                fields: {
                                    profile: 1
                                }
                            });
                        }
                    },
                    {
                        find: function (payout) {
                            return Receipts.find({
                                _id: {
                                    $in: payout.receiptIds || payout.originalReceiptIds || []
                                }
                            },{
                                fields: {
                                    receiptNr: 1
                                }
                            });
                        }
                    }
                ]
            }
        ]
    }
});

Meteor.publishComposite('payouts', function (searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        find: function () {
            return Payouts.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset
            },{
                fields: {
                    paymentNr: false,
                    statusDates: false,
                    api: false,
                    notes: false,
                }
            });
        },
        children: [
            {
                collectionName: "travellers",
                find: function (payout) {
                    return Meteor.users.find({
                        _id: payout.travellerId
                    });
                }
            },
            {
                find: function (payout) {
                    return Receipts.find({
                        _id: {
                            $in: payout.receiptIds || payout.originalReceiptIds || []
                        }
                    });
                },
                children: [
                    {
                        find: function (receipt) {
                            return Shops.find({
                                _id: receipt.shopId
                            });
                        }
                    }
                ]
            }
        ]
    };
});

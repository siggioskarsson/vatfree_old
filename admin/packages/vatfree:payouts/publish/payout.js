Meteor.publishComposite('payouts_item', function(payoutId) {
    check(payoutId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Payouts.find({
                _id: payoutId
            },{
                fields: {
                    textSearch: false,
                    statusDates: false,
                },
                transform(payout) {
                    if (payout.paymentMethod === 'creditcard' && !_.contains(['preCheck',' new'], payout.status)) {
                        payout.paymentNr = 'xxxx xxxx xxxx ' + payout.paymentNr.substr(-4);
                    }
                    return payout;
                }
            });
        },
        children: [
            {
                collectionName: "travellers",
                find: function (payout) {
                    return Meteor.users.find({
                        _id: payout.travellerId
                    });
                }
            },
            {
                collectionName: "partners",
                find: function (payout) {
                    return Meteor.users.find({
                        _id: payout.partnerId
                    });
                }
            },
            {
                find: function (payout) {
                    return PayoutBatches.find({
                        _id: payout.payoutBatchId
                    });
                }
            },
            {
                find: function (payout) {
                    return Receipts.find({
                        _id: {
                            $in: payout.receiptIds || payout.originalReceiptIds || []
                        }
                    });
                },
                children: [
                    {
                        find: function (receipt) {
                            return Shops.find({
                                _id: receipt.shopId
                            });
                        }
                    }
                ]
            }
        ]
    };
});

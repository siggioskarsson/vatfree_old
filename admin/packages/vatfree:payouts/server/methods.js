/* global Vatfree: true, PayoutBatches: true */
import { payoutsApiIsValid } from './lib/apis/is-valid';
import { processFintraxPayoutBatch } from './lib/fintrax';
import { validatePayout } from './lib/validate-payout';

Meteor.methods({
    'request-payout'(paymentMethod, paymentNr, paymentAccountName) {
        // request payout by user / traveller
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (!Vatfree.payouts.validatePayout(paymentMethod, paymentNr)) {
            throw new Meteor.Error(500, "Invalid payout method");
        }

        let costs = Meteor.settings.public.payoutCosts[paymentMethod] || 0;
        let euroCurrency = Currencies.findOne({code: "EUR"});

        let receiptIds = {};
        let amount = {};
        Receipts.find({
            userId: this.userId,
            payoutId: {
                $exists: false
            },
            invoiceId: {
                $exists: true
            },
            status: 'paidByShop'
        }).forEach((receipt) => {
            if (receipt.euroRefund) {
                receipt.refund = receipt.euroRefund;
                receipt.currencyId = euroCurrency._id;
            }
            if (!receiptIds[receipt.currencyId]) {
                receiptIds[receipt.currencyId] = [];
                amount[receipt.currencyId] = 0;
            }

            receiptIds[receipt.currencyId].push(receipt._id);
            amount[receipt.currencyId] += receipt.refund;
        });

        _.each(receiptIds, (receipts, currencyId) => {
            Payouts.insert({
                status: "new",
                travellerId: this.userId,
                receiptIds: receipts,
                amount: amount[currencyId] - costs,
                costs: costs,
                currencyId: currencyId,
                paymentMethod: paymentMethod,
                paymentNr: paymentNr,
                paymentAccountName: paymentAccountName
            });
        });
    },
    'create-payout-batch'(paymentMethods, includePartners = false) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        const payoutSelector = {
            status: 'new',
            payoutBatchId: {
                $exists: false
            },
            partnerId: {
                $exists: includePartners
            }
        };

        if (paymentMethods && paymentMethods.length > 0) {
            payoutSelector.paymentMethod = {
                $in: paymentMethods
            };
        }

        let amount = {};
        let payoutIds = {};
        Payouts.find(payoutSelector).forEach((payout) => {
            if (!payout.partnerId) {
                // cannot validate partner receipts (yet)
                validatePayout(payout);
            }

            const partnerId = payout.partnerId || "vatfree";
            if (!_.has(amount, partnerId)) {
                amount[partnerId] = {};
                payoutIds[partnerId] = {};
            }

            if (!_.has(amount[partnerId], payout.paymentMethod)) {
                amount[partnerId][payout.paymentMethod] = 0;
                payoutIds[partnerId][payout.paymentMethod] = [];
            }

            amount[partnerId][payout.paymentMethod] += payout.amount;
            payoutIds[partnerId][payout.paymentMethod].push(payout._id);
        });

        let payoutBatches = [];
        _.each(payoutIds, (partnerPayout, partnerId) => {
            _.each(partnerPayout, (payout, paymentMethod) => {
                const payoutBatch = {
                    amount: amount[partnerId][paymentMethod],
                    paymentMethod: paymentMethod,
                    payoutIds: payout
                };

                if (partnerId && partnerId !== 'vatfree') {
                    payoutBatch.partnerId = partnerId;
                }

                payoutBatches.push(PayoutBatches.insert(payoutBatch));
            });
        });

        return payoutBatches;
    },
    'get-payout-stats'(selector) {
        if (!Vatfree.userIsInRole(this.userId, 'payouts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const pipeline = [
            {
                $match: selector,
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 },
                    amount: { $sum: { $ifNull: ['$amount', 0] } }
                }
            },
        ];
        return Payouts.rawCollection().aggregate(pipeline).toArray();
    },
    'get-sepa-xml-template'() {
        if (!Vatfree.userIsInRole(this.userId, 'payouts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Assets.getText('templates/sepa.xml');
    },
    'process-payout-batch-via-api'(payoutBatchId) {
        if (!Vatfree.userIsInRole(this.userId, 'payouts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const payoutBatch = PayoutBatches.findOne({_id: payoutBatchId});
        if (payoutBatch) {
            Vatfree.payouts.processBatchViaApi(payoutBatch);
        } else {
            throw new Meteor.Error(404, 'Could not find payout batch');
        }
    },
    'is-valid-payout'(paymentMethod, paymentNr, paymentAccountName) {
        if (!this.userId) return false;

        return payoutsApiIsValid(paymentMethod, paymentNr, paymentAccountName);
    },
    'upload-to-fintrax'(fileContents, payoutBatchId) {
        if (!Vatfree.userIsInRole(this.userId, 'payouts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (!Meteor.settings.fintrax) {
            throw new Meteor.Error(500, "Fintrax settings not configured properly");
        }

        processFintraxPayoutBatch(this.userId, payoutBatchId, fileContents);
    }
});

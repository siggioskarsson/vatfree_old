ActivityStream.attachHooks(PayoutBatches);
try {
    // Add index to activity stream collection for payouts
    ActivityStream.collection._ensureIndex({'securityContext.payoutBatchId': 1});
    Vatfree.synchronizationSchemaIndex(PayoutBatches);
} catch(e) {
    console.error(e);
}

PayoutBatches.after.insert(function(userId, doc) {
    // update all debt collectionIds
    Payouts.update({
        _id: {
            $in: doc.payoutIds
        }
    },{
        $set: {
            payoutBatchId: doc._id,
            status: 'sentToPaymentProvider'
        }
    },{
        multi: 1
    });
});

PayoutBatches.after.update(function(userId, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'paidAt') && modifier['$set'] && modifier['$set']['paidAt']) {
        // update all debt collectionIds
        Payouts.update({
            _id: {
                $in: doc.payoutIds
            },
            status: {
                $nin: ['paid', 'error',' cancelled']
            }
        },{
            $set: {
                status: 'paid',
                paidAt: doc.paidAt,
                paidBy: doc.paidBy
            }
        },{
            multi: 1
        });
    }
});

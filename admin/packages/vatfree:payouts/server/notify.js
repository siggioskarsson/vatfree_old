Vatfree.notify.sendStatusNotification['payouts'] = function (userId, payoutId, travellerStatusNotify) {
    try {
        let payout = Payouts.findOne({_id: payoutId});
        let currency = Currencies.findOne({_id: payout.currencyId});
        let traveller = Meteor.users.findOne({_id: payout.travellerId});
        if (traveller && traveller.profile) {
            traveller.country = Countries.findOne({_id: traveller.profile.countryId }) || {};
        }

        let receipts = Receipts.find({_id: {$in: (payout.receiptIds || [])}}, {sort: { receiptNr: 1 }}).fetch();

        let emailLanguage = traveller.profile.language || "en";

        let activityIds = {
            payoutId: payoutId,
            travellerId: traveller._id
        };

        let templateData = {
            language: emailLanguage,
            payout: payout,
            currency: currency,
            traveller: traveller.profile,
            receipts: receipts
        };

        let toAddress = traveller.profile.email;
        let fromAddress = 'Vatfree.com support <support@vatfree.com>';

        Vatfree.notify.sendNotification(travellerStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage, traveller);
    } catch (e) {
        ActivityLogs.insert({
            type: 'task',
            userId: userId,
            subject: 'Sending of payout to traveller failed',
            status: 'new',
            payoutId: payoutId,
            notes: "Error: " + e.message
        });
    }
};

import './lib/apis'
import { syncPayoutsToExact } from '../../vatfree:payouts/server/synch/exact';

if (Meteor.settings && Meteor.settings.cron) {
    if (Meteor.settings.cron.apiPayoutsCron) {
        SyncedCron.add({
            name: 'Check all open api payouts for final status',
            schedule: function(parser) {
                // 6:45 every day
                return parser.cron(Meteor.settings.cron.apiPayoutsCron);
            },
            job: function() {
                Vatfree.payouts.api.checkOpenPayouts((err, numberClosed) => {
                    if (err) {
                        ActivityLogs.insert({
                            type: 'task',
                            subject: 'ERROR: checking open api payouts',
                            status: 'new',
                            error: err
                        });
                    } else {
                        ActivityLogs.insert({
                            type: 'burny',
                            subject: 'Open API payouts closed',
                            status: 'done',
                            notes: "Closed " + (numberClosed ? numberClosed : 0) + " api payouts"
                        });
                    }
                });
            }
        });
    }

    if (Meteor.settings.cron.exact && Meteor.settings.cron.exact.payouts) {
        SyncedCron.add({
            name: 'Send new payouts to book keeping',
            schedule: function (parser) {
                // parser is a later.parse object
                // every day at 23:23 am
                // '23 23 * * *'
                return parser.cron(Meteor.settings.cron.exact.payouts);
            },
            job: function (intendedAt) {
                syncPayoutsToExact((err, numberSynched) => {
                    if (err) {
                        ActivityLogs.insert({
                            type: 'task',
                            subject: 'ERROR: synching payouts to Exact',
                            status: 'new',
                            error: err
                        });
                    } else if (numberSynched > 0) {
                        ActivityLogs.insert({
                            type: 'burny',
                            subject: 'Payouts synched to Exact',
                            status: 'done',
                            notes: "Synched " + (numberSynched ? numberSynched : 0) + " payouts"
                        });
                    }
                });
            }
        });
    }
}

Meteor.methods({
    'jobs.checkOpenPayouts'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Vatfree.payouts.api.checkOpenPayouts();
    },
    'jobs.syncPayoutsToExact': function() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        console.log('jobs.syncPayoutsToExact -> syncPayoutsToExact');
        syncPayoutsToExact(console.log);
    }
});

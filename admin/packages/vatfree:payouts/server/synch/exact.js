import moment from 'moment';
import { createClient } from 'meteor/vatfree:invoices/server/lib/exact-online';
import { SERVICE_ID, DEBUGGING, Ledger, getExactClient, getAuthorizedExactClient } from 'meteor/vatfree:invoices/server/synch/exact';

const payoutGlaAccount = {
    'sepa': Ledger['2540'],
    'paypal': Ledger['2545'],
    'creditcard': Ledger['2550'],
    'alipay': Ledger['2560'],
    'wechat': Ledger['2570']
};

Vatfree.synch.addPlugin('exact-payouts', {
    whoami(callback) {
        // Get current user
        getAuthorizedExactClient((err, exactClient) => {
            exactClient.sys.me((err, result) => {
                callback(err, !err ? result.d.results : null);
            });
        });
    },
    to(callback) {
        syncPayoutsToExact(callback);
    },
    from(callback) {
        throw new Meteor.Error(500, 'Not implemented');
    }
});


export const syncPayoutsToExact = function(callback) {
    if (!Meteor.settings.exact) {
        throw new Meteor.Error(500, "Exact settings not defined");
    }

    try {
        if (DEBUGGING) console.log('Synching payouts to Exact', 'starting exact client');
        getAuthorizedExactClient((err, exactClient) => {
            if (err) {
                throw new Meteor.Error(500, err.reason);
            }

            exactClient.sys.me((err, userResult) => {
                if (err) {
                    throw new Meteor.Error(500, err.reason);
                }
                const userData = userResult.d.results;

                if (DEBUGGING) console.log('Synching payouts to Exact', 'getting payouts');
                let nrSynched = 0;
                PayoutBatches.find({
                    'synchronization.serviceId': {
                        $ne: SERVICE_ID
                    },
                    paidAt: {
                        $gte: new Date("2018-01-01 00:00:00")
                    }
                },{
                    limit: Meteor.settings.exact.limit
                }).forEach((payoutBatch) => {
                    // sync invoice to Exact
                    if (DEBUGGING) console.log('Synching payout', payoutBatch._id, payoutBatch.payoutBatchNumber);

                    const generalJournalEntry = searchGeneralJournalEntryLines(exactClient, "Notes eq '" + payoutBatch._id + "'");
                    if (generalJournalEntry && generalJournalEntry.length > 0) {
                        // Found an old entry, update sync status
                        if (DEBUGGING) console.log('Found an old entry', generalJournalEntry);
                        Vatfree.synch.upateSynchStatus(PayoutBatches, payoutBatch._id, SERVICE_ID, generalJournalEntry[0].EntryID, generalJournalEntry[0]);
                    } else {
                        // currency was not set properly on old payouts
                        const currencyId = payoutBatch.currencyId || Meteor.settings.defaults.currencyId;
                        const currency = Currencies.findOne({_id: currencyId}, {fields: { code: 1 } });
                        const generalJournalEntry = {
                            "Currency" : currency.code,
                            "FinancialPeriod" : moment(payoutBatch.paidAt).format('Q'),
                            "FinancialYear" : moment(payoutBatch.paidAt).format('YYYY'),
                            "JournalCode" : "UIT",
                            "GeneralJournalEntryLines": [
                                {
                                    "AmountFC" : Vatfree.numbers.formatAmount(payoutBatch.amount),
                                    "Date": moment(payoutBatch.paidAt).format('YYYY-MM-DD'),
                                    "Description" : "Payout batch " + payoutBatch.payoutBatchNumber,
                                    "GLAccount" : Ledger['2530'],
                                    "Notes" : payoutBatch._id
                                },
                                {
                                    "AmountFC" : -1 * Vatfree.numbers.formatAmount(payoutBatch.amount),
                                    "Date": moment(payoutBatch.paidAt).format('YYYY-MM-DD'),
                                    "Description" : "Payout batch " + payoutBatch.payoutBatchNumber,
                                    "GLAccount" : payoutGlaAccount[payoutBatch.paymentMethod],
                                    "Notes" : payoutBatch._id
                                }
                            ]
                        };

                        if (DEBUGGING) console.log('generalJournalEntry', JSON.stringify(generalJournalEntry));
                        const generalJournalEntryId = syncExactGeneralJournalEntry(exactClient, payoutBatch._id, generalJournalEntry);
                        nrSynched++;
                    }
                });
                callback(null, nrSynched);
            });
        });
    } catch(e) {
        console.error(e);
        callback(e);
    }
};

const searchGeneralJournalEntryLinesAsync = function(exactClient, searchTerm, callback) {
    try {
        exactClient.generaljournalentry.searchGeneralJournalEntryLines(searchTerm, function(err, result) {
            if (err) {
                console.error(err);
                callback(err);
            } else {
                callback(null, result.d.results || null);
            }
        });
    } catch(e) {
        console.error(e);
        callback(e);
    }
};
export const searchGeneralJournalEntryLines = Meteor.wrapAsync(searchGeneralJournalEntryLinesAsync);

const syncExactGeneralJournalEntryAsync = function(exactClient, payoutBatchId, generalJournalEntry, callback) {
    try {
        exactClient.generaljournalentry.createGeneralJournalEntry(generalJournalEntry, function (err, result) {
            if (err) {
                console.error(err);
                callback(err);
            } else {
                if (DEBUGGING) console.log('createSalesEntry', result);
                const generalJournalEntryId = result.d.EntryID;

                Vatfree.synch.upateSynchStatus(PayoutBatches, payoutBatchId, SERVICE_ID, generalJournalEntryId, result.d);

                callback(null, generalJournalEntryId);
            }
        });
    } catch(e) {
        console.error(e);
        callback(e);
    }
};
export const syncExactGeneralJournalEntry = Meteor.wrapAsync(syncExactGeneralJournalEntryAsync);

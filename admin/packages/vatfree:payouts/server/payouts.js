try { Payouts._dropIndex('receiptIds_1'); } catch(e) {};
try {
    Payouts._ensureIndex({'payoutNr': 1}, {unique: true});
    Payouts._ensureIndex({'receiptIds': 1, 'partnerId': 1}, {unique: true, sparse: true});
    Payouts._ensureIndex({'travellerId': 1});
    Payouts._ensureIndex({'textSearch': "text"}, {"language_override": "_text_language"});
} catch(e) {
    console.error(e);
}

ActivityStream.attachHooks(Payouts);
// Add index to activity stream collection for payouts
ActivityStream.collection._ensureIndex({'securityContext.payoutId': 1});

Payouts.before.insert(function(userId, doc) {
    // set the status to preCheck if it contains a salesforce receipt
    Receipts.find({
        _id: {
            $in: doc.receiptIds
        }
    }).forEach((r) => {
        if (r.salesforceId) {
            doc.status = 'preCheck';
        }
    });
});

Payouts.after.insert(function(userId, doc) {
    Payouts.updateTextSearch(doc);

    // update receipts with payoutId
    Receipts.update({
        _id: {
            $in: doc.receiptIds || []
        },
        payoutId: {
            $exists: false
        },
        status: 'paidByShop'
    },{
        $set: {
            payoutId: doc._id,
            status: 'requestPayout'
        }
    },{
        multi: 1
    });

    Meteor.defer(() => {
        Vatfree.notify.processNotificationTriggers(userId, 'payouts', '- insert -', false, doc._id);
    });
});

Payouts.before.update(function(userId, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status && modifier.$set.status === 'cancelled') {
        Receipts.update({
            _id: {
                $in: doc.receiptIds || []
            },
            status: {
                $in: ['requestPayout', 'paid']
            }
        },{
            $set: {
                status: 'paidByShop'
            },
            $unset: {
                payoutId: 1
            }
        },{
            multi: true
        });
    }
});

Payouts.after.update(function(userId, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status && modifier.$set.status === 'paid') {
        Receipts.update({
            _id: {
                $in: doc.receiptIds || []
            },
            status: 'requestPayout'
        },{
            $set: {
                status: 'paid'
            }
        },{
            multi: true
        });
    }

    Meteor.defer(() => {
        Payouts.updateTextSearch(doc);
        Vatfree.notify.checkStatusChange('payouts', userId, this.previous, doc, fieldNames, modifier);
    });
});

Payouts.updateTextSearch = function(doc) {
    let textSearch = doc.payoutNr.toString();
    textSearch+= " " + (doc.paymentMethod || "") + " payment:" + (doc.paymentMethod || "");
    textSearch+= " " + (doc.paymentNr || "").replace(/ /g, '') + " nr:" + (doc.paymentNr || "").replace(/ /g, '');
    textSearch+= " " + (doc.paymentAccountName || "") + " name:" + (doc.paymentAccountName || "");
    textSearch+= " " + (doc.notes || "");
    textSearch+= " " + (doc.amount || 0) + " amount:" + (doc.amount || 0);
    textSearch+= " " + (doc.costs || 0) + " costs:" + (doc.costs || 0);

    if (doc.payoutBatchId) {
        let payoutBatch = PayoutBatches.findOne({_id: doc.payoutBatchId});
        if (payoutBatch) {
            textSearch += ' ' + payoutBatch.payoutBatchNumber;
        }
    }

    Receipts.find({
        _id: {
            $in: doc.receiptIds || doc.originalReceiptIds || []
        }
    },{
        fields: {
            textSearch: 1
        }
    }).forEach((receipt) => {
        textSearch += ' ' + receipt.textSearch;
    });

    Payouts.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch.toLowerCase().latinize()
        }
    });
};

Vatfree.removeReceiptFromPayout = function(payoutId, receiptId) {
    let payout = Payouts.findOne({_id: payoutId});
    if (!payout) {
        throw new Meteor.Error(500, 'Payout could not be found');
    }
    let receipt = Receipts.findOne({_id: receiptId});
    if (!receipt) {
        throw new Meteor.Error(500, 'Receipt could not be found');
    }

    if (payout.status === 'paid') {
        throw new Meteor.Error(500, 'Payout has already been processed');
    }

    if (payout.receiptIds.length === 1 && payout.receiptIds[0] === receiptId) {
        Payouts.update({
            _id: payoutId
        },{
            $set: {
                status: "cancelled",
                originalReceiptIds: [receiptId]
            },
            $unset: {
                receiptIds: 1
            }
        });
    } else {
        Payouts.update({
            _id: payoutId
        },{
            $set: {
                amount: payout.amount - receipt.refund
            },
            $pull: {
                receiptIds: receiptId
            }
        });
        Receipts.update({
            _id: receiptId
        },{
            $unset: {
                payoutId: 1
            }
        });
    }
};

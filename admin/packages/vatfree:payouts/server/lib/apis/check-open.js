const DEBUGGING = (Meteor.settings.payouts ? Meteor.settings.payouts.apiDebugging : false) || false;

Vatfree.payouts.api.checkOpenPayouts = function(callback = null) {
    const apiPayoutProviderCache = {};

    let numberClosed = 0;
    Payouts.find({
        status: 'sentToApiProvider',
        api: {
            $exists: true
        }
    }).forEach((payout) => {
        const payoutType = payout.api._apiType;
        if (DEBUGGING) console.log('payout', payoutType, payout);

        // Check and initialize provider api class and init connection
        if (!_.has(apiPayoutProviderCache, payoutType) && _.has(Vatfree.payouts.api.classes, payoutType)) {
            apiPayoutProviderCache[payoutType] = Vatfree.payouts.api.classes[payoutType]();

            if (!apiPayoutProviderCache[payoutType]) {
                if (callback) {
                    callback("Could not initialize api class for " + payoutType);
                    return false;
                } else {
                    throw new Meteor.Error(500, "Could not initialize api class for " + payoutType);
                }
            }

            apiPayoutProviderCache[payoutType].login();
        }

        const apiStatus = apiPayoutProviderCache[payoutType].getStatus(payout.api);
        if (DEBUGGING) console.log('apiStatus', apiStatus);
        if (apiStatus.status === 'paid') {
            Payouts.update({
                _id: payout._id
            }, {
                $set: {
                    status: 'paid'
                }
            });
            numberClosed++;
        } else if (apiStatus.status === 'rejected') {
            Payouts.update({
                _id: payout._id
            }, {
                $set: {
                    status: 'error',
                    notes: (payout.notes ? payout.notes + "\n" : '') + JSON.stringify(apiStatus.message)
                }
            });
            ActivityLogs.insert({
                type: 'task',
                subject: 'API payout has failed: ' + payout.paymentMethod + ' - ' + payout.payoutNr,
                status: 'new',
                payoutId: payout._id,
                notes: 'Error: ' + JSON.stringify(apiStatus.message)
            });
            numberClosed++;
        }

        if (callback) callback(null, numberClosed);
        return numberClosed;
    });
};

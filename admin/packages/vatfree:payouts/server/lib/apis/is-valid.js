import { SafetyTaxFreeApi } from './safety-tax-free-api';

export const payoutsApiIsValid = function(paymentMethod, paymentNr, paymentAccountName) {
    switch (paymentMethod) {
        case 'creditcard':
            return Vatfree.payouts.validateCreditcardPayout(paymentNr);
            break;
        case 'paypal':
            return Vatfree.payouts.validatePaypalPayout(paymentNr);
            break;
        case 'sepa':
            return Vatfree.payouts.validateSepaPayout(paymentNr);
            break;
        case 'wechat':
        case 'alipay':
            const api = new SafetyTaxFreeApi(Meteor.settings.safetyTaxFree.clientId, Meteor.settings.safetyTaxFree.clientSecret, Meteor.settings.safetyTaxFree.sandbox);
            api.login();
            if (api.isLoggedIn()) {
                return api.isValidPayout(paymentMethod, paymentNr);
            } else {
                return false;
            }
            break;
    }
};

import { Base64 } from 'meteor/base64';
const serverUrl = (Meteor.settings.safetyTaxFree ? Meteor.settings.safetyTaxFree.url : '') || "http://dev.ms.safetytaxfree.net";
const DEBUGGING = (Meteor.settings.safetyTaxFree ? Meteor.settings.safetyTaxFree.DEBUGGING : false) || false;

export const SafetyTaxFreeApiPayoutStatus = {
    init: 'init',
    rejected: 'rejected',
    handling: 'handling',
    success: 'success',
    failed: 'failed',
};

export const SafetyTaxFreeApiPayoutType = {
    wechat: 'wechat',
    alipay: 'alipay',
    unionPay: 'unionPay'
};

Meteor.startup(() => {
    // Comment the line below to run a console test
    return false;

    if (Meteor.settings.safetyTaxFree) {
        let api = new SafetyTaxFreeApi(Meteor.settings.safetyTaxFree.clientId, Meteor.settings.safetyTaxFree.clientSecret, Meteor.settings.safetyTaxFree.sandbox);
        api.login();

        // for each payout ....
        const ticketNum = api.getPayoutTicket();
        if (ticketNum) {
            console.log('ticketNum', ticketNum);

            const payout = api.unionPayPayout('6214830129178066', 2.00, ticketNum);
            if (payout) {
                console.log('payout', payout);

                const status = api.payoutStatus(ticketNum);
                if (status) {
                    console.log('status', status);
                } else {
                    console.error('status', api.getLastError());
                }
            } else {
                console.error('payout', api.getLastError());
            }
        } else {
            console.error('ticketNum', api.getLastError());
        }

        api.logout();
    }
});

export const SafetyTaxFreeApi = class safetytaxfreeapi {
    constructor(clientId, clientSecret, sandbox = false) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;

        this.accessToken = '';
        this.loggedIn = false;

        this.serverPath = sandbox ? '/sandbox/open-payout' : '/open-payout';
        this.lastError = "";
    }

    type() {
        return 'safetyTaxFree';
    }

    isRejected(apiPayout) {
        return apiPayout.payoutStatus === SafetyTaxFreeApiPayoutStatus.rejected || apiPayout.payoutStatus === SafetyTaxFreeApiPayoutStatus.failed;
    }

    isLoggedIn() {
        return this.loggedIn && !!this.accessToken;
    }

    getLastError() {
        return this.lastError;
    }

    /*
    curl -X POST \
        http://localhost:8770/uaa/oauth/token \
        -H 'Authorization: Basic ...' \
        -H 'Cache-Control: no-cache' \
        -H 'Content-Type: application/x-www-form-urlencoded' \
        -H 'Postman-Token: 8002f9b8-7af1-97f8-d690-37ae9dcf4ce3' \
        -d 'scope=openid%20write&grant_type=client_credentials'
     */
    login() {
        const restUrl = '/uaa/oauth/token';
        try {
            const basicAuthentication = Base64.encode('' + this.clientId + ':' + this.clientSecret);
            if (DEBUGGING) console.log('url', serverUrl + restUrl);
            const accessTokenRequest = HTTP.post(serverUrl + restUrl, {
                params: {
                    scope: "openid write",
                    grant_type: "client_credentials"
                },
                headers: {
                    'Cache-Control': 'no-cache',
                    'Authorization': 'Basic ' + basicAuthentication
                }
            });

            if (DEBUGGING) console.log({accessTokenRequest});
            if (accessTokenRequest && accessTokenRequest.statusCode === 200) {
                if (accessTokenRequest.data && accessTokenRequest.data.access_token) {
                    this.accessToken = accessTokenRequest.data.access_token;
                    this.loggedIn = true;
                }
            }
        } catch(e) {
            this.lastError = Object.getOwnPropertyNames(e);
            console.error(e);
        }
    }

    logout() {
        this.accessToken = "";
        this.loggedIn = false;
    }

    /*
    curl -X GET \
        http://dev.ms.safetytaxfree.net/sandbox/open-payout/rest/ticket \
        -H 'Authorization: Bearer ...' \
        -H 'cache-control: no-cache'
     */
    getPayoutTicket() {
        const restUrl = '/rest/ticket';
        if (!this.loggedIn) return false;

        try {
            if (DEBUGGING) console.log('url', serverUrl + this.serverPath + restUrl);
            const payoutTicketRequest = HTTP.get(serverUrl + this.serverPath + restUrl, {
                headers: {
                    'Authorization': 'Bearer ' + this.accessToken,
                    'cache-control': 'no-cache'
                }
            });

            if (DEBUGGING) console.log({payoutTicketRequest});
            if (payoutTicketRequest && payoutTicketRequest.statusCode === 200) {
                if (payoutTicketRequest.data && payoutTicketRequest.data.message === "ok") {
                    return payoutTicketRequest.data.content.ticketNum;
                }
            } else if (payoutTicketRequest && payoutTicketRequest.statusCode) {
                // error
                this.lastError = payoutTicketRequest.data.message;
            }
        } catch(e) {
            this.lastError = Object.getOwnPropertyNames(e);
            console.error(e);
        }
    }

    /*
    curl -X POST \
        http://localhost:8004/rest/payout \
        -H 'Authorization: Bearer ...' \
        -H 'Content-Type: application/json' \
        -H 'cache-control: no-cache' \
        -d '{"ticketNum":"...","payoutType":"alipay","account":"17090072375","amount":1.00}'
     */
    payoutRequest(payoutType, account, amount, ticketNum = false) {
        const restUrl = '/rest/payout';
        if (!this.loggedIn) return false;

        ticketNum = ticketNum || this.getPayoutTicket();
        if (!ticketNum) return false;

        try {
            const requestData = {
                data: {
                    ticketNum: ticketNum,
                    payoutType: payoutType,
                    account: account,
                    amount: amount
                },
                headers: {
                    'Authorization': 'Bearer ' + this.accessToken,
                    'cache-control': 'no-cache'
                }
            };
            if (DEBUGGING) console.log('payoutRequest:', serverUrl + this.serverPath + restUrl);
            if (DEBUGGING) console.log(requestData);

            const payoutRequest = HTTP.post(serverUrl + this.serverPath + restUrl, requestData);

            if (DEBUGGING) console.log({payoutRequest});
            if (payoutRequest && payoutRequest.statusCode === 200) {
                if (payoutRequest.data && payoutRequest.data.message === "ok") {
                    return payoutRequest.data.content;
                } else {
                    this.lastError = payoutRequest.data;
                }
            } else if (payoutRequest && payoutRequest.statusCode) {
                // error
                this.lastError = payoutRequest.data;
            }
            return false;
        } catch(e) {
            this.lastError = Object.getOwnPropertyNames(e);
            console.error(e);
        }
    }

    /*
    curl -X POST \
        http://localhost:8004/rest/payout/query \
        -H 'Authorization: Bearer ...' \
        -H 'Content-Type: application/json' \
        -H 'cache-control: no-cache' \
        -d '{"ticketNum":"..."}'
     */
    payoutStatus(ticketNum = false, payoutUuid = false) {
        const restUrl = '/rest/payout/query';
        if (!this.loggedIn) return false;

        try {
            const data = {};
            if (payoutUuid) {
                data.payoutUuid = payoutUuid;
            } else {
                data.ticketNum = ticketNum;
            }

            if (DEBUGGING) console.log('url', serverUrl + this.serverPath + restUrl);
            const payoutRequest = HTTP.post(serverUrl + this.serverPath + restUrl, {
                data: data,
                headers: {
                    'Authorization': 'Bearer ' + this.accessToken,
                    'cache-control': 'no-cache'
                }
            });

            if (DEBUGGING) console.log({payoutRequest});
            if (payoutRequest && payoutRequest.statusCode === 200) {
                if (payoutRequest.data && payoutRequest.data.message === "ok") {
                    return payoutRequest.data.content;
                }
            }
        } catch(e) {
            this.lastError = Object.getOwnPropertyNames(e);
            console.error(e);
        }
    }

    /*
        Function that takes the internal api output of this class and calls the internal payoutStatus function
     */
    getStatus(apiStatus) {
        const apiPayoutStatus = this.payoutStatus(apiStatus.ticketNum);

        let status = 'pending';
        switch (apiPayoutStatus.payoutStatus) {
            case 'init':
            case 'handling':
                status = 'pending';
                break;
            case 'rejected':
            case 'failed':
                status = 'rejected';
                break;
            case 'success':
                status = 'paid';
                break;
        }

        return {
            status: status,
            message: apiPayoutStatus.errorDesc || apiPayoutStatus.message,
            apiStatus: apiPayoutStatus
        }
    }

    isValidPayout(payoutType, account) {
        const restUrl = '/rest/payout/account';
        if (!this.loggedIn) return false;

        try {
            if (DEBUGGING) console.log('url', serverUrl + this.serverPath + restUrl);
            const payoutAccount = HTTP.post(serverUrl + this.serverPath + restUrl, {
                data: {
                    payoutType,
                    account
                },
                headers: {
                    'Authorization': 'Bearer ' + this.accessToken,
                    'cache-control': 'no-cache'
                }
            });

            if (DEBUGGING) console.log({payoutRequest: payoutAccount});
            if (payoutAccount && payoutAccount.statusCode === 200) {
                if (payoutAccount.data && payoutAccount.data.message === "ok" && payoutAccount.data.content) {
                    return payoutAccount.data.content.legal === true;
                }
            }

            return false;
        } catch(e) {
            this.lastError = Object.getOwnPropertyNames(e);
            console.error(e);
        }
    }

    aliPayPayout(account, amount, ticketNum = false) {
        return this.payoutRequest('alipay', account, amount, ticketNum);
    }
    wechatPayout(account, amount, ticketNum = false) {
        return this.payoutRequest('wechat', account, amount, ticketNum);
    }
    unionPayPayout(account, amount, ticketNum = false) {
        return this.payoutRequest('unionPay', account, amount, ticketNum);
    }
};

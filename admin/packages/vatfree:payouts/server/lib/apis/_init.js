/* global Vatfree: true */
const DEBUGGING = (Meteor.settings.payouts ? Meteor.settings.payouts.apiDebugging : false) || false;

Vatfree.payouts.api = {};
Vatfree.payouts.api.classes = {};

Vatfree.payouts.api._processPayoutBatchItem = function (apiPayout, api, payout) {
    if (apiPayout) {
        apiPayout._apiType = api.type();
        const setData = {
            api: apiPayout,
            status: 'sentToApiProvider'
        };

        if (api.isRejected(apiPayout)) {
            setData.status = 'error';
            setData.notes = (payout.notes ? payout.notes + '\n' : '') + (api.getLastError() || apiPayout.errorDesc);
        }

        Payouts.update({
            _id: payout._id
        }, {
            $set: setData
        });
    } else {
        if (DEBUGGING) console.log('apiPayout ERROR', api.getLastError());
        Payouts.update({
            _id: payout._id
        }, {
            $set: {
                status: 'error',
                'api.error': api.getLastError(),
                notes: (payout.notes ? payout.notes + '\n' : '') + JSON.stringify(api.getLastError())
            }
        });
        ActivityLogs.insert({
            type: 'task',
            subject: 'API payout call failed for ' + api.type() + ': ' + payout.paymentMethod + ' - ' + payout.payoutNr,
            status: 'new',
            payoutId: payout._id,
            notes: 'Error: ' + JSON.stringify(api.getLastError())
        });
    }
};

Vatfree.payouts.api._processPayoutBatch = function (payoutBatch, api, payoutType) {
    Payouts.find({
        payoutBatchId: payoutBatch._id,
        status: 'sentToPaymentProvider',
        api: {
            $exists: false
        }
    }).forEach((payout) => {
        if (DEBUGGING) console.log('Vatfree.payouts.api.' + api.type() + ' processing payout', payout.payoutNr);

        let apiPayout = api.payoutRequest(payoutType, payout.paymentNr, Vatfree.numbers.formatAmount(payout.amount));
        if (DEBUGGING) console.log('apiPayout', apiPayout);

        Vatfree.payouts.api._processPayoutBatchItem(apiPayout, api, payout);
    });

    PayoutBatches.update({
        _id: payoutBatch._id
    }, {
        $set: {
            uploadedAt: new Date(),
            uploadedBy: this.userId
        }
    });
};

import { SafetyTaxFreeApi } from './safety-tax-free-api';

Vatfree.payouts.api.classes.safetyTaxFree = function() {
    return new SafetyTaxFreeApi(Meteor.settings.safetyTaxFree.clientId, Meteor.settings.safetyTaxFree.clientSecret, Meteor.settings.safetyTaxFree.sandbox);
};

const safetyTaxFree = function (payoutBatch, payoutType) {
    const api = Vatfree.payouts.api.classes.safetyTaxFree();
    api.login();

    if (api.isLoggedIn()) {
        Vatfree.payouts.api._processPayoutBatch(payoutBatch, api, payoutType);
    } else {
        throw new Meteor.Error(500, api.getLastError());
    }

    return true;
};

Vatfree.payouts.api.wechat = function(payoutBatch) {
    return safetyTaxFree(payoutBatch, 'wechat');
};

Vatfree.payouts.api.alipay = function(payoutBatch) {
    return safetyTaxFree(payoutBatch, 'alipay');
};

Vatfree.payouts.api.unionPay = function(payoutBatch) {
    return safetyTaxFree(payoutBatch, 'unionPay');
};

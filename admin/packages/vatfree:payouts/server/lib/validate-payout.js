import { VatfreeCrypto } from 'meteor/vatfree:crypto/crypto';

export const validatePayout = function(payout) {
    if (_.isString(payout)) {
        payout = Payouts.findOne({_id: payout});
    }
    if (!payout || !payout._id || !payout.paymentMethod) {
        throw new Meteor.Error(404, "Could not find payout");
    }

    if (!_.contains(Vatfree.payments.methods, payout.paymentMethod)) {
        throw new Meteor.Error(500, "Not a valid payment method: " + payout.paymentMethod);
    }

    let refunds = 0;
    let salesforceReceiptPresent = false;
    _.each(payout.receiptIds, (receiptId) => {
        const receipt = Receipts.findOne({_id: receiptId});
        if (!receipt) {
            throw new Meteor.Error(500, `Could not find receipt for payout ${payout.payoutNr}, receipt ${receiptId}`);
        }

        salesforceReceiptPresent = salesforceReceiptPresent || !!receipt.salesforceId;

        if (receipt.shaSum !== VatfreeCrypto.calculateReceiptHash(receipt)) {
            throw new Meteor.Error(500, `Could not verify hash check for payout ${payout.payoutNr}, receipt ${receipt.receiptNr}`);
        }

        refunds += receipt.refund;

        // TODO: check signature of vattie on the receipt to make sure it is OK
    });

    if (!salesforceReceiptPresent && refunds !== (payout.amount + payout.costs)) {
        throw new Meteor.Error(500, `Payout amount is not equal to all receipt refunds for payout ${payout.payoutNr} (${payout.amount} !== ${refunds})`);
    }
};

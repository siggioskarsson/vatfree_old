import './apis';

Vatfree.payouts.processBatchViaApi = function(payoutBatch) {
    check(payoutBatch, Object);

    const paymentMethod = payoutBatch.paymentMethod;
    if (_.has(Vatfree.payouts.api, paymentMethod)) {
        return Vatfree.payouts.api[paymentMethod](payoutBatch);
    } else {
        throw new Meteor.Error(404, 'Could not find processor for payment method ' + paymentMethod);
    }
};

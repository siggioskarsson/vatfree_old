import Client from 'ssh2-sftp-client';
import urlParser from 'url';

export const sftpUploadAsync = function(url, privateKey, fileBuffer, fileName, callback) {
    const parsedUrl = urlParser.parse(url);
    const [username, password] = parsedUrl.auth.split(':');

    const sftp = new Client();
    sftp.connect({
        host: parsedUrl.hostname,
        port: parsedUrl.port || 22,
        username: username,
        passphrase: password,
        privateKey: privateKey
    }).then(() => {
        return sftp.list(parsedUrl.path);
    }).then((data) => {
        const uploadedFile = _.find(data, (d) => {
            return d.name === fileName;
        });

        if (uploadedFile) {
            throw new Meteor.Error(500, "File already uploaded");
        } else {
            return sftp.put(fileBuffer, parsedUrl.path + '/' + fileName);
        }
    }).then((data) => {
        callback(null, data);
    }).catch((err) => {
        callback(err);
    });
};

export const sftpUpload = Meteor.wrapAsync(sftpUploadAsync);

/* global Vatfree: true */

export const processFintraxPayoutBatch = function (userId, payoutBatchId, fileContents) {
    const payoutBatch = PayoutBatches.findOne({_id: payoutBatchId});
    if (payoutBatch && payoutBatch.paymentMethod === 'creditcard') {
        import { sftpUpload } from './sftp-upload';

        const fileBuffer = Buffer.from(fileContents);
        let fileName = payoutBatch.payoutBatchNumber + '-' + payoutBatch.paymentMethod;
        switch (payoutBatch.paymentMethod) {
            case 'creditcard':
                fileName += '.SET';
                break;
            default:
                fileName += '.csv';
        }

        const result = sftpUpload(Meteor.settings.fintrax.sftpUrl, Meteor.settings.fintrax.privateKey, fileBuffer, fileName);

        if (result.indexOf('Uploaded data stream') === 0) {
            Payouts.update({
                payoutBatchId: payoutBatchId
            },{
                $set: {
                    status: 'sentToApiProvider'
                }
            },{
                multi: true
            });

            PayoutBatches.update({
                _id: payoutBatchId
            }, {
                $set: {
                    uploadedAt: new Date(),
                    uploadedBy: userId
                }
            });

            sendFintraxPayoutEmail(payoutBatch);

            if (payoutBatch.partnerId) {
                sendPartnerPayoutEmail(payoutBatch);
            }
        }

        //return result;
    } else {
        throw new Meteor.Error(500, 'Payout batch is not a credit card batch');
    }
};

const sendFintraxPayoutEmail = function (payoutBatch) {
    const payoutBatchId = payoutBatch._id;
    let numberOfPayouts = 0;
    let amountOfPayouts = 0;
    Payouts.find({
        payoutBatchId: payoutBatchId
    }).forEach((payout) => {
        numberOfPayouts++;
        amountOfPayouts += payout.amount;
    });

    let mailOptions = {
        from: 'Vatfree finance <finance@vatfree.com>',
        to: Meteor.settings.fintrax.toAddress,
        cc: Meteor.settings.fintrax.bccAddress
    };
    const emailTemplateId = Meteor.settings.defaults.emailTemplateId;
    const subject = 'vatfree.com weekly payout ' + payoutBatch.payoutBatchNumber;
    const emailText = `Hi Grace,

We have just uploaded payment file with a total of ${numberOfPayouts} transactions and a value of ${Vatfree.numbers.formatCurrency(amountOfPayouts, 2, '€')}.

Could you please process this payment file?

Please note, this email is automatically generated. Please contact finance@vatfree.com in case of any issues.
`;

    let activityIds = {
        payoutBatchId: payoutBatchId,
    };
    if (Meteor.settings.fintrax.activityIds) {
        _.each(Meteor.settings.fintrax.activityIds, (activityValue, activityId) => {
            activityIds[activityId] = activityValue;
        });
    }
    Vatfree.sendEmail(mailOptions, emailTemplateId, subject, emailText, activityIds);
};

export const sendPartnerPayoutEmail = function (payoutBatch) {
    const payoutBatchId = payoutBatch._id;
    const partner = Meteor.users.findOne({_id: payoutBatch.partnerId});
    if (partner && partner.emails && partner.emails.length > 0) {
        const mailTo = [];
        _.each(partner.emails, (email) => {
            mailTo.push(email.address);
        });
        if (mailTo.length > 0) {
            let numberOfPayouts = 0;
            let amountOfPayouts = 0;
            let payouts = [];
            Payouts.find({
                payoutBatchId: payoutBatchId
            },{
                sort: {
                    payoutNr: 1
                }
            }).forEach((payout) => {
                numberOfPayouts++;
                amountOfPayouts += payout.amount;
                payouts.push(`${payout.payoutNr},${payout.paymentAccountName},${Vatfree.numbers.formatCurrency(payout.amount)},xxxx xxxx xxxx ${payout.paymentNr.substr(-4)}`);
            });

            let mailOptions = {
                from: 'Vatfree finance <finance@vatfree.com>',
                to: mailTo.join(','),
                cc: 'Vatfree finance <finance@vatfree.com>'
            };
            const emailTemplateId = Meteor.settings.defaults.emailTemplateId;
            const subject = 'vatfree.com payout ' + payoutBatch.payoutBatchNumber;
            const emailText = `Hi,

We have just uploaded payment file for payout with a total of ${numberOfPayouts} transactions and a value of ${Vatfree.numbers.formatCurrency(amountOfPayouts, 2, '€')}.

Payouts of batch ${payoutBatch.payoutBatchNumber}:

${payouts.join("\n")}

Regards,

Vatfree.com
`;

            const activityIds = {
                payoutBatchId: payoutBatchId,
                partnerId: payoutBatch.partnerId
            };
            Vatfree.sendEmail(mailOptions, emailTemplateId, subject, emailText, activityIds);

        }
    }
};

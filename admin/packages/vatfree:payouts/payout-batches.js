import moment from 'moment';

PayoutBatches = new Meteor.Collection('payout-batches');
PayoutBatches.attachSchema(new SimpleSchema({
    payoutBatchNumber: {
        type: String,
        label: "Unique name of this payout batch",
        autoValue: function() {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return getNextPayoutBatchNumber();
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: getNextPayoutBatchNumber()
                    };
                } else {
                    this.unset();
                }
            } else {
                this.unset();
            }
        },
        optional: true
    },
    partnerId: {
        type: String,
        label: "Partner if applicable",
        optional: true
    },
    amount: {
        type: String,
        label: "Total payout batch amount",
        optional: false
    },
    paymentMethod: {
        type: String,
        label: "The payment method for this batch",
        optional: false
    },
    payoutIds: {
        type: [String],
        label: "All payout Ids in this batch",
        optional: false
    },
    uploadedAt: {
        type: Date,
        label: "When this batch was uploaded to api provider",
        optional: true
    },
    uploadedBy: {
        type: String,
        label: "By whom this batch was uploaded",
        optional: true
    },
    paidAt: {
        type: Date,
        label: "When this batch was marked as paid",
        optional: true
    },
    paidBy: {
        type: String,
        label: "By whom this batch was marked as paid",
        optional: true
    },
    synchronization: {
        type: [Vatfree.synchronizationSchema],
        label: "Synchronizations of the item to/from other systems",
        optional: true
    }
}));
PayoutBatches.attachSchema(CreatedUpdatedSchema);

PayoutBatches.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

var getNextPayoutBatchNumber = function() {
    let year = moment().format('YYYY').toString();
    let counter = Number(incrementCounter(Counters, 'payoutBatchNumber_' + year)).padLeft(5).toString();

    let nextPayoutBatchNumber = year + counter;

    return "PAY" + nextPayoutBatchNumber;
};

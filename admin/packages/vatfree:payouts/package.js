Package.describe({
    name: 'vatfree:payouts',
    summary: 'Vatfree payouts package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'email',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'konecty:mongo-counter@0.0.5_3',
        'reywood:publish-composite@1.5.1',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'simonsimcity:job-collection',
        'vatfree:core',
        'vatfree:countries',
        'vatfree:notify'
    ]);

    // shared files
    api.addFiles([
        'payouts.js',
        'payout-batches.js',
        'lib/payouts.js'
    ]);

    // server files
    api.addFiles([
        'server/lib/process-batch-via-api.js',
        'server/lib/remove-receipt-from-payout.js',
        'server/cron.js',
        'server/methods.js',
        'server/notify.js',
        'server/payout-batches.js',
        'server/payouts.js',
        'server/synch/exact.js',
        'publish/payouts.js',
        'publish/payout-batch.js',
        'publish/payout-batches.js',
        'publish/payout.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/payouts.less',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/sepa-xml.html',
        'templates/sepa-xml.js',
        'templates/payout-batch.html',
        'templates/payout-batch.js',
        'templates/payout-batch-info.html',
        'templates/payout-batch-info.js',
        'templates/payout-batches.html',
        'templates/payout-batches.js',
        'templates/payout-batches-list.html',
        'templates/payout-batches-list.js',
        'templates/payouts.html',
        'templates/payouts.js'
    ], 'client');

    api.addAssets([
        'templates/sepa.xml'
    ], 'server');

    api.export([
        'Payouts',
        'PayoutBatches',
    ]);
});

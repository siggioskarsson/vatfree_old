import moment from 'moment';

Payouts = new Meteor.Collection('payouts');
Payouts.attachSchema(new SimpleSchema({
    payoutNr: {
        type: String,
        label: "Internally generated unique payout number",
        autoValue: function() {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return getNextPayoutNumber();
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: getNextPayoutNumber()
                    };
                } else {
                    this.unset();
                }
            } else {
                this.unset();
            }
        },
        optional: false
    },
    partnerId: {
        type: String,
        label: "Partner this payout is coming from",
        optional: true
    },
    travellerId: {
        type: String,
        label: "Traveller that request this payout",
        optional: false
    },
    receiptIds: {
        type: [String],
        label: "A list of receipts included on this payout",
        optional: true
    },
    originalReceiptIds: {
        type: [String],
        label: "A list of the original receipts included on this payout, if status = cancelled",
        optional: true
    },
    currencyId: {
        type: String,
        label: "Currency of this payout",
        optional: false
    },
    paymentMethod: {
        type: String,
        label: "Type of payout requested",
        allowedValues: Vatfree.payments.methods,
        optional: false
    },
    paymentNr: {
        type: String,
        label: "Payment nr / identifier",
        optional: false
    },
    paymentAccountName: {
        type: String,
        label: "Optional account name if applicable",
        optional: true
    },
    amount: {
        type: Number,
        label: "Total amount of payout",
        optional: false
    },
    costs: {
        type: Number,
        label: "Total costs of the payout",
        optional: true
    },
    status: {
        type: String,
        label: "Status of the payout",
        optional: false,
        defaultValue: 'new',
        allowedValues: [
            'preCheck',
            'new',
            'sentToPaymentProvider',
            'sentToApiProvider',
            'partiallyPaid',
            'paid',
            'cancelled',
            'error'
        ]
    },
    statusDates: {
        type: Object,
        label: "All the last dates of the status",
        optional: true
    },
    payoutDate: {
        type: Date,
        label: "Payout date of this payout",
        optional: true
    },
    paidAt: {
        type: Date,
        label: "When the payout was marked as paid",
        optional: true
    },
    paidBy: {
        type: String,
        label: "By whom this batch was marked as paid",
        optional: true
    },
    payoutBatchId: {
        type: String,
        label: "The payout batch this payout was a part of",
        optional: true
    },
    notes: {
        type: String,
        label: "Payout notes",
        optional: true
    },
    api: {
        type: Object,
        label: "API info if sent via api to payment provider",
        blackbox: true,
        optional: true
    },
    deleted: {
        type: Boolean,
        label: "Whether the payout was deleted",
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
Payouts.attachSchema(CreatedUpdatedSchema);
StatusDates.attachToSchema(Payouts);

var getNextPayoutNumber = function() {
    let year = moment().format('YY').toString();
    let counter = Number(incrementCounter(Counters, 'payoutNr_' + year)).padLeft(7).toString();

    return "P" + year + counter;
};

Payouts.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

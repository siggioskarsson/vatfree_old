import moment from 'moment';

Template.payoutEdit.onCreated(function () {
});

Template.payoutEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('payouts_item', FlowRouter.getParam('payoutId'));
    });
});

Template.payoutEdit.helpers({
    payoutDoc() {
        return Payouts.findOne({
            _id: FlowRouter.getParam('payoutId')
        });
    }
});

Template.payoutEditForm.onCreated(function () {
    this.disableButtons = new ReactiveVar();
    this.receiptSort = new ReactiveVar({
        receiptNr: -1
    });
});

Template.payoutEditForm.onRendered(function () {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('payout-receipts', data._id);
        this.subscribe('activity-logs', "", {payoutId: data._id});
    });
});

Template.payoutEditForm.helpers({
    isStatus(status) {
        let template = Template.instance();
        return template.data.status === status;
    },
    isDisabled() {
        return this.status !== 'preCheck';
    },
    getReceipts() {
        let template = Template.instance();
        let sort = template.receiptSort.get();
        return Receipts.find({
            _id: {
                $in: (this.receiptIds || this.originalReceiptIds || [])
            }
        }, {
            sort: sort
        });
    },
    getReceiptSorting() {
        return Template.instance().receiptSort.get();
    },
    getActivityLog() {
        return ActivityLogs.find({
            payoutId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    disableButtons() {
        return Template.instance().disableButtons.get();
    },
    getPaymentMethods() {
        return Vatfree.payments.methods;
    }
});

Template.payoutEditForm.events({
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/payouts');
    },
    'click .cancel-payout'(e) {
        e.preventDefault();
        let payoutId = this._id;
        let receiptIds = this.receiptIds;

        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "The payout will be cancelled!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: true
        }, function () {
            Payouts.update({
                _id: payoutId
            },{
                $set: {
                    status: 'cancelled',
                    originalReceiptIds: receiptIds
                },
                $unset: {
                    receiptIds: 1
                }
            });
        });
    },
    'submit form[name="edit-payout-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        // fix dates
        if (_.has(formData, 'payoutDate')) {
            if (formData.payoutDate) {
                formData.payoutDate = moment(formData.payoutDate, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.payoutDate = null;
            }
        }

        // fix numbers
        if (_.has(formData, 'amount')) formData.amount = Math.round(Number(formData.amount)*100);
        if (_.has(formData, 'totalVat')) formData.totalVat = Math.round(Number(formData.totalVat)*100);
        if (_.has(formData, 'serviceFee')) formData.serviceFee = Math.round(Number(formData.serviceFee)*100);
        if (_.has(formData, 'refund')) formData.refund = Math.round(Number(formData.refund)*100);

        Payouts.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Payout saved!')
            }
        });
    }
});

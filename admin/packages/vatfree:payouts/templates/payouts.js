import './payouts.html';

Template.payouts.onCreated(Vatfree.templateHelpers.onCreated(Payouts, '/payouts/', '/payout/:itemId'));
Template.payouts.onRendered(Vatfree.templateHelpers.onRendered());
Template.payouts.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.payouts.helpers(Vatfree.templateHelpers.helpers);
Template.payouts.helpers(payoutHelpers);
Template.payouts.events(Vatfree.templateHelpers.events());

Template.payouts.onCreated(function() {
    this.creatingPayouts = new ReactiveVar();
    this.showJobs = new ReactiveVar();
    this.paymentMethodFilter = new ReactiveArray();
    _.each(Session.get(this.view.name + '.paymentMethodFilter') || [], (paymentMethod) => {
        this.paymentMethodFilter.push(paymentMethod);
    });

    this.partnerFilter = new ReactiveVar(Session.get(this.view.name + '.partnerFilter'));

    this.autorun(() => {
        Session.setPersistent(this.view.name + '.paymentMethodFilter', this.paymentMethodFilter.array());
        Session.setPersistent(this.view.name + '.partnerFilter', this.partnerFilter.get());
    });

    this.selectorFunction = (selector) => {
        let paymentMethodFilter = this.paymentMethodFilter.array() || [];
        if (paymentMethodFilter.length > 0) {
            selector.paymentMethod = {
                $in: paymentMethodFilter || []
            };
        }

        let partnerFilter = this.partnerFilter.get();
        if (partnerFilter) {
            if (partnerFilter === 'active') {
                selector.partnerId = {
                    $exists: true
                }
            }
            if (partnerFilter === 'inactive') {
                selector.partnerId = {
                    $exists: false
                }
            }
        }
    };

    this.todoStatus = [
        'new'
    ];
    this.sort.set({
        payoutNr: -1
    });

    this.subscribe('debt-collection-lines', "");
});

Template.payouts.helpers({
    creatingPayouts() {
        return Template.instance().creatingPayouts.get();
    },
    showJobs() {
        return Template.instance().showJobs.get();
    },
    getDebtCollectionLines() {
        return DebtCollectionLines.find();
    },
    getPayoutStats() {
        let template = Template.instance();
        let selector = payoutHelpers.getPayoutSelector(template);

        console.log('payout selector', selector);
        let stats = ReactiveMethod.call('get-payout-stats', selector);
        return stats && stats[0] ? stats[0] : false;
    },
    isActivePartnerFilter() {
        return Template.instance().partnerFilter.get() === 'active';
    },
    isInactivePartnerFilter() {
        return Template.instance().partnerFilter.get() === 'inactive';
    }
});

Template.payouts.events({
    'click .payout-batches'(e) {
        e.preventDefault();
        FlowRouter.go('/payout-batches');
    },
    'click .filter-icon.partner-filter-icon'(e, template) {
        e.preventDefault();
        const partnerFilter = template.partnerFilter.get();
        if (partnerFilter === 'active') {
            template.partnerFilter.set('inactive');
        } else if (partnerFilter === 'inactive') {
            template.partnerFilter.set(false);
        } else {
            template.partnerFilter.set('active');
        }
    },
    'click .filter-icon.payment-method-filter-icon'(e, template) {
        e.preventDefault();
        let method = $(e.currentTarget).attr('id');
        let paymentMethodFilter = template.paymentMethodFilter.array();

        if (_.contains(paymentMethodFilter, method)) {
            template.paymentMethodFilter.clear();
            _.each(paymentMethodFilter, (_method) => {
                if (method !== _method) {
                    template.paymentMethodFilter.push(_method);
                }
            });
        } else {
            template.paymentMethodFilter.push(method);
        }
    },
    'click .payout-create-new'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).attr('disabled') === 'disabled') {
            return false;
        }

        template.creatingPayouts.set('normal');

        const paymentMethods = template.paymentMethodFilter.array();
        const paymentMethodsText = paymentMethods.length > 0 ? paymentMethods.join(' & ') : "all payment methods";
        if (confirm(`Do you want to create an export file for ${paymentMethodsText} and mark the payouts as sent?`)) {
            Meteor.call('create-payout-batch', paymentMethods, false, (err, payoutBatchIds) => {
                if (err) {
                    alert(err.reason || err.message);
                } else {
                    if (payoutBatchIds) {
                        FlowRouter.go('/payout-batch/' + payoutBatchIds.join(','));
                    } else {
                        toastr.warning('No payouts found');
                    }
                }
                template.creatingPayouts.set(false);
            });
        } else {
            template.creatingPayouts.set(false);
        }
    },
    'click .payout-create-new-partner'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).attr('disabled') === 'disabled') {
            return false;
        }

        template.creatingPayouts.set('partner');

        const paymentMethods = template.paymentMethodFilter.array();
        const paymentMethodsText = paymentMethods.length > 0 ? paymentMethods.join(' & ') : "all payment methods";
        if (confirm(`Do you want to create an export file for ${paymentMethodsText} and mark the payouts as sent?`)) {
            Meteor.call('create-payout-batch', paymentMethods, true, (err, payoutBatchIds) => {
                if (err) {
                    alert(err.reason || err.message);
                } else {
                    if (payoutBatchIds) {
                        FlowRouter.go('/payout-batch/' + payoutBatchIds.join(','));
                    } else {
                        toastr.warning('No payouts found');
                    }
                }
                template.creatingPayouts.set(false);
            });
        } else {
            template.creatingPayouts.set(false);
        }
    }
});

Template.sepa_xml.helpers({
    numberOfPayouts() {
        return Payouts.find({
            payoutBatchId: this._id
        }).count();
    },
    sumOfPayouts() {
        let sum = 0;
        Payouts.find({
            payoutBatchId: this._id
        }).forEach((payout) => {
            sum += payout.amount;
        });

        return sum;
    },
    getPayouts() {
        let payouts = [];
        let index = 0;
        Payouts.find({
            payoutBatchId: this._id
        },{
            sort: {
                createdAt: 1
            }
        }).forEach((payout) => {
            payout.index = index++;
            payouts.push(payout);
        });
        return payouts.length > 0 ? payouts : false;
    },
    getTravellerName() {
        let traveller = Travellers.findOne({_id: this.travellerId});
        if (traveller && traveller.profile) {
            return traveller.profile.name;
        }

        return "";
    },
    getReceiptNrs() {
        let receiptNrs = [];
        Receipts.find({
            _id: {
                $in: this.receiptIds || []
            }
        }).forEach((receipt) => {
            receiptNrs.push(receipt.receiptNr);
        });

        return receiptNrs.join(', ');
    }
});

import moment from "moment";

UI.registerHelper('getPayoutIcon', function(status) {
    status = status || this.status;

    let icon = Vatfree.payouts.icons();
    return _.has(icon, status) ? icon[status] : "circle-o";
});

UI.registerHelper('getPayoutStatusOptions', function() {
    return _.clone(Payouts.simpleSchema()._schema.status.allowedValues);
});

UI.registerHelper('getPayoutName', function(payoutId) {
    if (!payoutId) payoutId = this.payoutId;

    let payout = Payouts.findOne({_id: payoutId});
    if (payout) {
        return payout.payoutNr;
    }

    return "";
});

UI.registerHelper('getPayoutDescription', function(payoutId) {
    if (!payoutId) payoutId = this.payoutId;

    let payout = Payouts.findOne({_id: payoutId});
    if (payout) {
        return payout.payoutNr + ' - ' + moment(payout.createdAt).format(TAPi18n.__('_date_format'));
    }

    return "";
});

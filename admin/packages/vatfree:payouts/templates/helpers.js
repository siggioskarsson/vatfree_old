/* global payoutHelpers: true */
import moment from 'moment';

payoutHelpers = {
    getPayoutSelector(template) {
        let selector = {
            deleted: {
                $ne: true
            }
        };

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }
        Vatfree.search.addStatusFilter.call(template, selector);
        Vatfree.search.addListFilter.call(template, selector);
        if (_.isFunction(template.selectorFunction)) template.selectorFunction(selector);

        let paymentMethodFilter = template.paymentMethodFilter.array() || [];
        if (paymentMethodFilter.length > 0) {
            selector.paymentMethod = {
                $in: paymentMethodFilter || []
            };
        }

        return selector
    },
    getPayouts() {
        let template = Template.instance();
        let selector = payoutHelpers.getPayoutSelector(template);

        return Payouts.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    getPayoutBatches() {
        let template = Template.instance();
        let selector = payoutHelpers.getPayoutSelector(template);

        return PayoutBatches.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    isActivePaymentMethodFilter(method) {
        let paymentMethodFilter = Template.instance().paymentMethodFilter.array() || [];
        return _.contains(paymentMethodFilter, method) ? 'active' : '';
    },
    getTraveller() {
        return Travellers.findOne({_id: this.travellerId});
    },
    daysOpen() {
        let mOpen = this.paidAt ? moment(this.paidAt) : moment(this.createdAt);
        return (this.payoutDate ? moment(this.payoutDate) : moment()).diff(mOpen, 'days');
    },
    getPaymentMethodIcon() {
        switch (this.paymentMethod) {
            case 'creditcard':
                return 'creditcard';
            case 'paypal':
                return 'paypal';
            case 'sepa':
                return 'bank';
            case 'wechat':
                return 'wechatpay';
            case 'alipay':
                return 'alipay';
        }
    },
    getOpenPayoutAmount() {
        return this.amount;
    }
};

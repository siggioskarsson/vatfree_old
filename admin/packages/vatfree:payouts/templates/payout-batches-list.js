import './payout-batches-list.html';
import './helpers';

Template.payoutBatchesList.onCreated(function() {
    this.detailUrl = '/payout-batch/:itemId';
});
Template.payoutBatchesList.helpers(Vatfree.templateHelpers.helpers);
Template.payoutBatchesList.helpers(payoutHelpers);
Template.payoutBatchesList.events(Vatfree.templateHelpers.events());

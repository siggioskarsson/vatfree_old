import './list.html';
import './helpers';

Template.payoutsList.onCreated(function() {
    this.detailUrl = '/payout/:itemId';
});
Template.payoutsList.helpers(Vatfree.templateHelpers.helpers);
Template.payoutsList.helpers(payoutHelpers);
Template.payoutsList.events(Vatfree.templateHelpers.events());

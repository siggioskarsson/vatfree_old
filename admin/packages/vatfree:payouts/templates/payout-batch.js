import moment from "moment";

Template.payoutBatch.onCreated(function() {
    let payoutBatchIds = FlowRouter.getParam('payoutBatchId').split(',');
    _.each(payoutBatchIds, (payoutBatchId) => {
        this.subscribe('payout-batch', payoutBatchId);
    });
});

Template.payoutBatch.helpers({
    getPayoutBatches() {
        let payoutBatchIds = FlowRouter.getParam('payoutBatchId').split(',');
        return PayoutBatches.find({
            _id: {
                $in: payoutBatchIds || []
            }
        });
    },
    totalStats() {
        let payoutBatchIds = FlowRouter.getParam('payoutBatchId').split(',');
        if (payoutBatchIds.length > 1) {
            let stats = {
                count: 0,
                amount: 0
            };

            PayoutBatches.find({
                _id: {
                    $in: payoutBatchIds || []
                }
            }).forEach((payoutBatch) => {
                stats.count += payoutBatch.payoutIds.length;
                stats.amount += Number(payoutBatch.amount);
            });

            return stats;
        }

        return false;
    }
});

Template.payoutBatch_details.onCreated(function() {
    this.uploadingViaApi = new ReactiveVar();
});

Template.payoutBatch_details.helpers({
    uploadingViaApi() {
        return Template.instance().uploadingViaApi.get();
    },
    payouts() {
        let payouts = [];
        let index = 0;
        Payouts.find({
            payoutBatchId: this._id
        },{
            sort: {
                createdAt: 1
            }
        }).forEach((payout) => {
            payout.index = index++;
            payouts.push(payout);
        });
        return payouts.length > 0 ? payouts : false;
    },
    getReceiptNr() {
        let receipt = Receipts.findOne({
            _id: this.receiptId
        });
        if (receipt) {
            return receipt.receiptNr;
        }
    },
    getDataContext() {
        return {
            paymentMethod: Template.instance().data.paymentMethod,
            payouts: this,
            payoutBatch: Template.instance().data
        }
    },
    getPayoutBatchTemplate() {
        return "payoutBatch_details_" + Template.instance().data.paymentMethod;
    },
    isType(type) {
        return this.paymentMethod === type;
    },
    amountWithFee() {
        return Math.ceil(this.amount*1.02);
    },
    apiPresent() {
        return _.contains(['alipay', 'wechat', 'unionPay'], this.paymentMethod) && !this.paidAt;
    }
});

Template.payoutBatch_details.events({
    'click .process-payout-batch-via-api'(e, template) {
        e.preventDefault();

        template.uploadingViaApi.set(true);
        Meteor.call('process-payout-batch-via-api', this._id, (err) => {
            if (err) {
                alert(err.reason || err.message);
            }
            template.uploadingViaApi.set();
        });
    },
    'click .save-payout-batch'(e, template) {
        e.preventDefault();

        let textToWrite = template.$('textarea').val();
        let textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
        let fileNameToSaveAs = template.data.payoutBatchNumber + '-' + template.data.paymentMethod;
        switch (template.data.paymentMethod) {
            case 'creditcard':
                fileNameToSaveAs += '.SET';
                break;
            default:
                fileNameToSaveAs += '.csv';
        }

        Vatfree.templateHelpers.downloadFile(fileNameToSaveAs, textFileAsBlob);
    },
    'click .upload-to-fintrax'(e, template) {
        e.preventDefault();

        template.uploadingViaApi.set(true);
        let textToWrite = template.$('textarea').val();
        Meteor.call('upload-to-fintrax', textToWrite, this._id, (err) => {
            if (err) {
                alert(err.reason || err.message);
            }
            template.uploadingViaApi.set();
        });

    },
    'click .save-payout-batch-sepa-xml'(e, template) {
        e.preventDefault();
        import handlebars from 'handlebars';
        handlebars.registerHelper('formatDate', Vatfree.dates.formatDate);
        handlebars.registerHelper('formatAmount', Vatfree.numbers.formatAmount);
        handlebars.registerHelper('toUpperCase', function(string) {
            return (string || "").toString().toUpperCase();
        });
        handlebars.registerHelper('getTravellerName', function() {
            let user = Travellers.findOne({_id: this.travellerId});
            if (user) {
                return user.profile.name || "";
            }
        });
        handlebars.registerHelper('getReceiptNrs', function() {
            return this.payoutNr;
        });

        Meteor.call('get-sepa-xml-template', (err, xmlTemplate) => {
            let templateData = template.data;

            templateData.getPayouts = [];
            let index = 0;
            let sumOfPayouts = 0;
            Payouts.find({
                payoutBatchId: templateData._id
            },{
                sort: {
                    createdAt: 1
                }
            }).forEach((payout) => {
                payout.index = index++;
                sumOfPayouts += payout.amount;
                templateData.getPayouts.push(payout);
            });
            templateData.numberOfPayouts = templateData.getPayouts.length;
            templateData.sumOfPayouts = sumOfPayouts;

            const textToWrite = handlebars.compile(xmlTemplate)(templateData);

            let textFileAsBlob = new Blob([textToWrite], {type:'application/xml'});
            let fileNameToSaveAs = template.data.payoutBatchNumber + '-' + template.data.paymentMethod + '.xml';

            Vatfree.templateHelpers.downloadFile(fileNameToSaveAs, textFileAsBlob);
        });
    },
    'click .mark-as-paid'(e, template) {
        e.preventDefault();
        let payoutBatchId = template.data._id;
        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "Do you want to mark the payout as paid?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: false
        }, function () {
            PayoutBatches.update({
                _id: payoutBatchId
            },{
                $set: {
                    paidAt: new Date(),
                    paidBy: Meteor.userId()
                }
            });
            swal("Paid!", "Payout has been marked as paid.", "success");
        });
        }
});

let payoutPaymentTypeHelpers = {
    getTravellerName() {
        let traveller = Travellers.findOne({_id: this.travellerId});
        if (traveller && traveller.profile) {
            return traveller.profile.name;
        }
    },
    getTravellerCity() {
        let traveller = Travellers.findOne({_id: this.travellerId});
        if (traveller && traveller.profile) {
            return traveller.profile.city || "";
        }
    },
    formatAmountForExport() {
        import accounting from 'accounting';
        return accounting.formatNumber(this.amount/100, 2, "", ",");
    },
    getReceiptNrs() {
        let receiptNrs = [];
        Receipts.find({
            _id: {
                $in: this.receiptIds || []
            }
        }).forEach((receipt) => {
            receiptNrs.push(receipt.receiptNr);
        });

        return receiptNrs.join(', ');
    },
    getCreditCardLine() {
        let line = "VDR";
        // line number - pad left 7
        line += Number(this.index + 2).padLeft(7).toString(); //"0000002";
        // credit card number - pad left - length 19
        line += Number(this.paymentNr.toString().replace(/[^0-9]/g,'')).padLeft(19).toString(); //"4053747003320623";
        // expiration date - something fake
        line += "E2" + moment().add(3, 'years').format('MMYY');
        // amount in cents - pad left 11
        line += Number(this.amount).padLeft(11).toString(); //"00000005699";
        // currency - 3 letter code
        let currency = Currencies.findOne({_id: this.currencyId}) || {};
        line += currency.code || "EUR";
        // Refund description - always 25 chars long
        line += "VAT refund " + this.payoutNr + "****"; //"VAT refund P2017-58354***";
        // Sender description - 13 characters
        line += "*vatfree.com*";
        // close line
        line += "00zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz+";

        return line;
    }
};
Template.payoutBatch_details_creditcard.helpers(payoutPaymentTypeHelpers);
Template.payoutBatch_details_creditcard.helpers({
    batchNr() {
        return Number(this.payoutBatch.payoutBatchNumber.substr(7)).padLeft(6);
    },
    footerLength() {
        // 0000097
        return Number(this.payouts.length + 2).padLeft(7).toString();
    },
    numberOfPayouts() {
        return Number(this.payouts.length).padLeft(7).toString();
    },
    totalValue() {
        let totalAmount = 0;
        _.each(this.payouts, (payout) => {
            totalAmount += payout.amount;
        });
        return Number(totalAmount).padLeft(14).toString();
    }
});
Template.payoutBatch_details_paypal.helpers(payoutPaymentTypeHelpers);
Template.payoutBatch_details_sepa.helpers(payoutPaymentTypeHelpers);
Template.payoutBatch_details_wechat.helpers(payoutPaymentTypeHelpers);
Template.payoutBatch_details_alipay.helpers(payoutPaymentTypeHelpers);

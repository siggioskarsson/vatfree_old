Template.payoutInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.payoutInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {payoutId: data._id}, null, limit, offset);
        this.subscribe('activity-stream', {}, data._id, limit, offset);
        this.subscribe('companies_item', data.companyId || "");
    });
});

Template.payoutInfo.helpers(payoutHelpers);
Template.payoutInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            $or: [
                {
                    'securityContext.payoutId': this._id
                },
                {
                    'activity.target.itemId': this._id
                }
            ]
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    },
    getPayoutBatch() {
        return PayoutBatches.findOne({_id: this.payoutBatchId});
    },
    getTravellerReceiptsFilter() {
        return {
            _id: {
                $in: this.receiptIds || []
            }
        }
    }
});

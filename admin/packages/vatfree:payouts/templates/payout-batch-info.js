import './helpers';

Template.payoutBatchInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.payoutBatchInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {payoutBatchId: data._id}, null, limit, offset);
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.payoutBatchInfo.helpers(payoutHelpers);
Template.payoutBatchInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            $or: [
                {
                    'securityContext.payoutBatchId': this._id
                },
                {
                    'activity.target.itemId': this._id
                }
            ]
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

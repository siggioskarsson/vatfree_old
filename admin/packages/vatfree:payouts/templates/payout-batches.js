import './payout-batches.html';
import './helpers';

Template.payoutBatches.onCreated(Vatfree.templateHelpers.onCreated(PayoutBatches, '/payout-batches', '/payout-batch/:itemId'));
Template.payoutBatches.onRendered(Vatfree.templateHelpers.onRendered());
Template.payoutBatches.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.payoutBatches.helpers(Vatfree.templateHelpers.helpers);
Template.payoutBatches.helpers(payoutHelpers);
Template.payoutBatches.events(Vatfree.templateHelpers.events());

Template.payoutBatches.onCreated(function() {
    this.paymentMethodFilter = new ReactiveArray();
    _.each(Session.get(this.view.name + '.paymentMethodFilter') || [], (paymentMethod) => {
        this.paymentMethodFilter.push(paymentMethod);
    });

    this.autorun(() => {
        Session.setPersistent(this.view.name + '.paymentMethodFilter', this.paymentMethodFilter.array());
    });

    this.selectorFunction = (selector) => {
        let paymentMethodFilter = this.paymentMethodFilter.array() || [];
        if (paymentMethodFilter.length > 0) {
            selector.paymentMethod = {
                $in: paymentMethodFilter || []
            };
        }
    };

    this.sort.set({
        payoutBatchNumber: -1
    });
});

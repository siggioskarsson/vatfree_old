Meteor.publish('travellers', function (searchTerm, selector, sort, limit, offset) {
    if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};
    selector['roles.__global_roles__'] = 'traveller';
    if (!_.has(selector, 'deleted')) {
        selector.deleted = {
            $exists: false
        };
    }

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    let cursor = Meteor.users.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset,
        fields: {
            profile: 1,
            private: 1,
            status: 1,
            "services.password.isSet": 1,
            "services.twitter.profile_image_url_https": 1,
            "services.facebook.id": 1,
            "services.google.picture": 1,
            "services.github.username": 1,
            "services.instagram.profile_picture": 1,
            "services.linkedin.pictureUrl": 1,
            "services.wechat.nickname": 1,
            createdAt: 1,
            updatedAt: 1,
            textSearch: 1
        }
    });
    Mongo.Collection._publishCursor(cursor, this, 'travellers');
    this.ready();
});

Meteor.publishComposite('travellers_item', function (travellerId) {
    check(travellerId, String);
    if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        collectionName: "travellers",
        find: function () {
            return Meteor.users.find({
                _id: travellerId
            },{
                fields: {
                    profile: 1,
                    private: 1,
                    emails: 1,
                    status: 1,
                    "services.twitter.profile_image_url_https": 1,
                    "services.facebook.id": 1,
                    "services.google.picture": 1,
                    "services.github.username": 1,
                    "services.instagram.profile_picture": 1,
                    "services.linkedin.pictureUrl": 1,
                    "services.wechat.nickname": 1,
                    roles: 1
                }
            });
        },
        children: [
            {
                find: function (traveller) {
                    return Payouts.find({
                        travellerId: traveller._id
                    });
                }
            },
            {
                collectionName: "affiliates",
                find: function (traveller) {
                    let affiliateId = traveller.private ? traveller.private.affiliateId : undefined;
                    if (traveller.private && traveller.private.affiliateId) {
                        return Affiliates.find({
                            _id: affiliateId
                        });
                    }
                }
            }
        ]
    };
});

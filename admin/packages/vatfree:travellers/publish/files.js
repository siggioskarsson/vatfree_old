Meteor.publish('traveller-files', function (travellerId) {
    this.unblock();
    if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    check(travellerId, String);

    return Files.find({
        itemId: travellerId,
        target: {
            $in: ['travellers', 'travellers_internal', 'travellers_trash']
        }
    });
});

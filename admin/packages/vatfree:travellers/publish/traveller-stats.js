Meteor.publish("traveller-stats", function () {
    if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    let self = this;
    let initializing = true;

    let countTravellers = 0;

    let handleTravellers = Meteor.users.find(Vatfree.travellers.reminders.querySelector(), {
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countTravellers++;
            if (!initializing)
                self.changed("stats", 'travellersToSendReminders', {count: countTravellers});
        },
        removed: function (id, fields) {
            countTravellers--;
            if (!initializing)
                self.changed("stats", 'travellersToSendReminders', {count: countTravellers});
        }
    });

    // Observe only returns after the initial added callbacks have
    // run.  Now return an initial value and mark the subscription
    // as ready.
    initializing = false;
    self.added("stats", 'travellersToSendReminders', {count: countTravellers});
    self.ready();

    // Stop observing the cursor when client unsubs.
    // Stopping a subscription automatically takes
    // care of sending the client any removed messages.
    self.onStop(function () {
        handleTravellers.stop();
    });
});

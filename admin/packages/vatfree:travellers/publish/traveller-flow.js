Meteor.publishComposite('traveller-flow', function(status, selector) {
    if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};
    selector['private.status'] = {
        $in: status
    };

    let travellerIds = [];
    Travellers.find(selector).forEach((traveller) => {
        if (travellerIds.length < 50) {
            const ncpFilledIn = Vatfree.travellers.ncpFilledIn(traveller);
            if (ncpFilledIn) {
                const nrOfFiles = Files.find({target: 'travellers', itemId: traveller._id, subType: {$ne: 'form'}}).count();
                if (nrOfFiles > 0) {
                    travellerIds.push(traveller._id);
                }
            }
        }
    });

    return {
        collectionName: 'travellers',
        find: function () {
            return Meteor.users.find({
                _id: {
                    $in: travellerIds
                }
            });
        },
        children: [
            {
                find: function (traveller) {
                    return Files.find({
                        itemId: traveller._id,
                        target: 'travellers'
                    });
                }
            }
        ]
    };
});

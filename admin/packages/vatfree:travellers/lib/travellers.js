/* global Vatfree: true */
import moment from 'moment-timezone';

Vatfree.travellers = {};

Vatfree.travellers.status = function() {
    return {
        'userUnverified': 'default',
        'waitingForDocuments': 'info',
        'documentationIncomplete': 'warning',
        'documentationExpired': 'warning',
        'notEligibleForRefund': 'danger',
        'userVerified': 'success'
    };
};

Vatfree.travellers.icons = function() {
    return {
        'userUnverified': 'icon icon-userpending',
        'waitingForDocuments': 'icon icon-passportclockwaiting',
        'documentationIncomplete': 'icon icon-passportincomplete',
        'documentationExpired': 'icon icon-passportattention',
        'notEligibleForRefund': 'icon icon-usernoneligible',
        'userVerified': 'icon icon-usereligible'
    };
};

Vatfree.travellers.needsVisa = function(travellerTypeId, countryId) {
    let travellerType = TravellerTypes.findOne({_id: travellerTypeId || Vatfree.defaults.TravellerTypeId}) || [];
    let documentsNeeded = Vatfree.travellers.getDocumentsNeeded(travellerType, countryId);
    return !!_.find(documentsNeeded, (documentNeeded) => { return documentNeeded.subType === 'visa'; });
};

Vatfree.travellers.getDocumentsNeeded = function (travellerType, countryId) {
    let documentsNeeded = [];
    _.each(travellerType.documentsNeeded, (doc, subType) => {
        if (doc.condition) {
            let country = Countries.findOne({_id: countryId}) || {};
            if (doc.condition === 'EUresident') {
                if (country.needVisa) {
                    doc.subType = subType;
                    documentsNeeded.push(doc);
                }
            } else if (doc.condition === country.code) {
                doc.subType = subType;
                documentsNeeded.push(doc);
            }
        } else {
            doc.subType = subType;
            documentsNeeded.push(doc);
        }
    });
    return documentsNeeded;
};

Vatfree.travellers.areDocumentsOK = function (travellerId) {
    let traveller = Travellers.findOne({_id: travellerId});
    let travellerType = TravellerTypes.findOne({_id: traveller.private.travellerTypeId || Vatfree.defaults.TravellerTypeId});
    let documentsNeeded = Vatfree.travellers.getDocumentsNeeded(travellerType, traveller.profile.countryId);
    let documentError = false;
    _.each(documentsNeeded, (documentNeeded) => {
        if (documentNeeded.required && !Files.findOne({
                itemId: travellerId,
                target: "travellers",
                subType: documentNeeded.subType
            })) {
            documentError = true;
        }
    });

    if (documentError) {
        return false;
    }

    return true;
};

Vatfree.travellers.isVerificationValid = function(travellerId, onCheckDate = false) {
    let traveller = Travellers.findOne({_id: travellerId});
    if (traveller.private.status !== "userVerified") {
        return false;
    }

    let mCheckDate = onCheckDate ? moment(onCheckDate) : moment();

    let travellerType = TravellerTypes.findOne({_id: traveller.private.travellerTypeId || Vatfree.defaults.TravellerTypeId});
    let documentsNeeded = Vatfree.travellers.getDocumentsNeeded(travellerType, traveller.profile.countryId) || [];
    let documentError = false;
    _.each(documentsNeeded, (documentNeeded) => {
        if (documentNeeded.subType === 'passport') {
            // check passport nr and expiration
            if (!traveller.profile.passportNumber) {
                documentError = true;
            }
            if (!traveller.profile.passportExpirationDate || moment(traveller.profile.passportExpirationDate).isBefore(mCheckDate)) {
                documentError = true;
            }
        } else if (documentNeeded.subType === 'visa') {
            // check visa nr and expiration
            if (!traveller.profile.visaNumber) {
                documentError = true;
            }
            if (!traveller.profile.visaExpirationDate || moment(traveller.profile.visaExpirationDate).isBefore(mCheckDate)) {
                documentError = true;
            }
        }
    });

    if (documentError) {
        return false;
    }

    return true;
};

Vatfree.travellers.ncpChanged = function(user, newProfile) {
    let changedNCP = user.profile.name !== newProfile.name;

    changedNCP = changedNCP || (user.profile.passportNumber || "") !== newProfile.passportNumber;
    if (_.isDate(user.profile.passportExpirationDate) || _.isDate(newProfile.passportExpirationDate)) {
        changedNCP = changedNCP || moment(user.profile.passportExpirationDate).format('YYYY-MM-DD') !== moment(newProfile.passportExpirationDate).format('YYYY-MM-DD');
    }

    changedNCP = changedNCP || (user.profile.visaNumber || "") !== newProfile.visaNumber;
    if (_.isDate(user.profile.visaExpirationDate) || _.isDate(newProfile.visaExpirationDate)) {
        changedNCP = changedNCP || moment(user.profile.visaExpirationDate).format('YYYY-MM-DD') !== moment(newProfile.visaExpirationDate).format('YYYY-MM-DD');
    }

    changedNCP = changedNCP || (user.profile.countryId || "") !== newProfile.countryId;
    changedNCP = changedNCP || (user.profile.visaCountryId || "") !== newProfile.visaCountryId;
    changedNCP = changedNCP || (user.profile.addressCountryId || "") !== newProfile.addressCountryId;
    changedNCP = changedNCP || user.private.travellerTypeId !== newProfile.travellerTypeId;

    return changedNCP;
};

Vatfree.travellers.ncpFilledIn = function(user) {
    return !!(user && user.profile &&
        user.profile.name &&
        (user.profile.addressCountryId || user.profile.countryId) &&
        user.profile.passportNumber);
};

Vatfree.travellers.getTravellerType = function(traveller) {
    if (traveller && traveller.private && traveller.private.travellerTypeId) {
        return TravellerTypes.findOne({_id: traveller.private.travellerTypeId || Vatfree.defaults.TravellerTypeId}) || {};
    }

    return {};
};

Package.describe({
    name: 'vatfree:travellers',
    summary: 'Vatfree travellers package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'underscore',
        'random',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'tap:i18n@1.8.2',
        'hermanitos:activity-stream@0.0.1',
        'percolate:synced-cron@1.3.2',
        'flemay:less-autoprefixer@1.2.0',
        'simonsimcity:job-collection',
        'vatfree:core',
        'vatfree:notify',
        'vatfree:countries',
        'vatfree:currencies',
        'vatfree:traveller-types',
        'vatfree:traveller-rejections'
    ]);

    // shared files
    api.addFiles([
        'lib/travellers.js',
        'jobs.js'
    ]);

    // server files
    api.addFiles([
        'server/lib/all-data.js',
        'server/lib/reminders.js',
        'server/cron.js',
        'server/import.js',
        'server/jobs.js',
        'server/methods.js',
        'server/notify.js',
        'server/travellers.js',
        'publish/files.js',
        'publish/travellers.js',
        'publish/traveller-flow.js',
        'publish/traveller-stats.js',
        'publish/traveller.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/card.less',
        'css/passport.less',
        'css/travellers.less',
        'client/travellers.js',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/card.html',
        'templates/card.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/edit/receipts.html',
        'templates/edit/receipts.js',
        'templates/import.html',
        'templates/import.js',
        'templates/info.html',
        'templates/info.js',
        'templates/last-receipts.html',
        'templates/last-receipts.js',
        'templates/list.html',
        'templates/list.js',
        'templates/merge.html',
        'templates/merge.js',
        'templates/passport-images.html',
        'templates/passport-images.js',
        'templates/tasks/tasks.html',
        'templates/tasks/tasks.js',
        'templates/tasks/sidebar.html',
        'templates/tasks/sidebar.js',
        'templates/tasks/sidebar-form.html',
        'templates/tasks/sidebar-form.js',
        'templates/tasks/left-sidebar.html',
        'templates/tasks/left-sidebar.js',
        'templates/tasks/waiting-for-docs.html',
        'templates/tasks/waiting-for-docs.js',
        'templates/svg-form.html',
        'templates/svg-form.js',
        'templates/travellers.html',
        'templates/travellers.js'
    ], 'client');

    api.export([
        'Travellers',
        'TravellerTypes'
    ]);
});

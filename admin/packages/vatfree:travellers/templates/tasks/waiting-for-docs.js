Template.travellers_modal_waiting_for_docs.onCreated(function() {
    this.activeItem = new ReactiveVar();
    this.updatingTravellers = new ReactiveVar();
    this.checkOriginalsIn = new ReactiveVar(true); // TODO create a checkbox from this
});

Template.travellers_modal_waiting_for_docs.onRendered(function() {
    let selector = this.data.selector || {};
    this.autorun(() => {
        if (!this.updatingTravellers.get()) {
            this.subscribe('traveller-flow', ['waitingForDocuments'], selector);
        }
    });
});

Template.travellers_modal_waiting_for_docs.helpers({
    getOptions() {
        let template = Template.instance();
        return {
            lockId: "traveller_waiting_for_docs_flow",
            lockMessage: "Traveller flow is locked by another user",
            sidebarTemplate: "travellers_modal_waiting_for_docs_sidebar",
            parentTemplateVariable: false,
            activeItem: '',
            activeTraveller: template.activeItem,
            onHide: template.data.onHide
        }
    },
    getTravellers() {
        const template = Template.instance();
        const data = template.data;
        let selector = data.selector || {};
        selector['private.status'] = {
            $in: ['waitingForDocuments']
        };

        let travellers = [];
        Travellers.find(selector, {
            sort: {
                createdAt: 1
            }
        }).forEach((traveller) => {
            let files = Files.find({target: 'travellers', itemId: traveller._id, subType: {$ne: 'form'}}).count();
            if (Vatfree.travellers.ncpFilledIn(traveller) && files > 0) {
                travellers.push(traveller);
            }
        });

        return travellers.length ? travellers : false;
    },
    activeItem() {
        return Template.instance().activeItem.get();
    },
    updatingTravellers() {
        return Template.instance().updatingTravellers.get();
    }
});

Template.travellers_modal_waiting_for_docs.events({
    'click .ibox-content .item-row input[type="checkbox"]' (e, template) {
        e.stopPropagation();
        e.stopImmediatePropagation();
    },
    'click .ibox-content .item-row' (e, template) {
        e.preventDefault();
        e.stopImmediatePropagation();
        if (template.activeItem.get() === this._id) {
            template.activeItem.set();
        } else {
            template.activeItem.set(this._id);
        }
    },
    'click .select-all-global'(e, template) {
        e.preventDefault();
        template.$('input[type="checkbox"]').each(function() { $(this).prop('checked', true) } )
    },
    'click .select-none-global'(e, template) {
        e.preventDefault();
        template.$('input[type="checkbox"]').each(function() { $(this).prop('checked', false) } )
    },
    'submit form[name="tasks-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        let travellerIds = [];
        _.each(formData.travellers, (on, travellerId) => {
            if (on !== false) {
                travellerIds.push(travellerId);
            }
        });

        template.updatingTravellers.set(true);
        Meteor.setTimeout(() => {
            Meteor.call('travellers-forward-waiting-for-docs-flow', travellerIds, (err, result) => {
                if (err) {
                    toastr.error(err.reason);
                } else {
                    template.updatingTravellers.set(false);
                    toastr.info('Travellers have been forwarded');
                }
            });
        }, 1);
    }
});

Template.travellers_modal_waiting_for_docs_sidebar.helpers({
    getTraveller() {
        return Travellers.findOne({_id: this.options.activeTraveller.get()}) || false;
    }
});

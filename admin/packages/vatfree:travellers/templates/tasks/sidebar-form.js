import moment from 'moment';

Template.tasksTravellersModal_sidebar_form.onCreated(function() {
    this.countryId = new ReactiveVar();

    this.autorun(() => {
        let traveller = Travellers.findOne({
            _id: this.data.options.activeItem
        });
        if (traveller) {
            this.countryId.set(traveller.profile.countryId);
        }
    });

    this.autorun(() => {
        let countryId = this.countryId.get();
        Tracker.afterFlush(() => {
            Vatfree.templateHelpers.initDatepicker.call(this);
        });
    });
});

Template.tasksTravellersModal_sidebar_form.helpers({
    getTraveller() {
        return Travellers.findOne({
            _id: this.options.activeItem
        });
    },
    getTravellerCountry() {
        return Template.instance().countryId.get();
    },
    alternativeTravellerType() {
        return this.private.travellerTypeId && this.private.travellerTypeId !== Vatfree.defaults.TravellerTypeId;
    },
    isNato() {
        if (this.private) {
            let travellerType = TravellerTypes.findOne({
                _id: this.private.travellerTypeId
            }) || {};

            return travellerType.isNato;
        }
    },
    needsVisa() {
        let template = Template.instance();
        return Vatfree.travellers.needsVisa(this.private.travellerTypeId, template.countryId.get());
    },
    getActivity() {
        return ActivityStream.collection.find({
            $or: [
                {
                    'securityContext.userId': this._id
                },
                {
                    'activity.target.itemId': this._id
                }
            ]
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

Template.tasksTravellersModal_sidebar_form.events({
    'change select[name="countryId"]'(e, template) {
        e.preventDefault();
        let countryId = $(e.currentTarget).val();
        template.countryId.set(countryId);

        let profile = {
            countryId: countryId
        };
        Meteor.call('update-traveller-profile', template.data.options.activeItem, profile, {}, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            }
        });
    }
});

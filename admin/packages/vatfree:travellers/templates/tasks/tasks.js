var checkForNewTraveller = function() {
    if (this.debug) console.log('checking for new traveller');
    if (this.view && this.view.isDestroyed === true) return;
    this.loadingTraveller.set(true);

    let parentTemplate = this.parentTemplate();
    let searchTerm = parentTemplate.searchTerm.get();
    let selector = {};
    Vatfree.search.addStatusFilter.call(parentTemplate, selector);

    Meteor.call('getNextTravellerTask', searchTerm, selector, (err, travellerId) => {
        if (this.debug) console.log('getNextTravellerTask', err, travellerId);
        if (err) {
            toastr.error(err.reason);
        } else {
            if (travellerId) {
                this.travellerId.set(travellerId);
            } else {
                Meteor.setTimeout(() => {
                    checkForNewTraveller.call(this);
                }, 5000);
            }
        }
        this.loadingTraveller.set();
    });
};

Template.tasksTravellersModal.onCreated(function() {
    this.updatingTraveller = new ReactiveVar(false);
    this.travellerId = new ReactiveVar(this.data.travellerId);
    this.loadingTraveller = new ReactiveVar();
    this.selectedFile = new ReactiveVar();
    this.imageMode = new ReactiveVar('zoom');
    this.lockId = 'travellers_';
    this.lock = false;
    this.debug = false;

    this.showTraveller = new ReactiveVar(false);
});

Template.tasksTravellersModal.onRendered(function() {
    let template = this;
    this.autorun(() => {
        let travellerId = this.travellerId.get();
        if (travellerId) {
            let file = Files.findOne({
                itemId: travellerId,
                target: 'travellers'
            },{
                sort: {
                    subType: 1,
                    createdAt: 1
                }
            });
            Tracker.nonreactive(() => {
                if (!this.selectedFile.get()) {
                    if (file) {
                        Meteor.call('getDirectUrl', file._id, ['vatfree', 'vatfree-thumbs'], (err, directUrls) => {
                            let selectedFile = _.clone(file);
                            selectedFile.directUrls = directUrls;
                            template.selectedFile.set(selectedFile);
                        });
                    }
                }
            });
        }
    });

    this.autorun(() => {
        let travellerId = this.travellerId.get();
        if (travellerId) {
            this.subscribe('travellers_item', travellerId);
            this.subscribe('traveller-files', travellerId);
        } else {
            checkForNewTraveller.call(this);
        }
    });

    this.autorun(() => {
        this.autorun(() => {
            Meteor.call('set-qr-upload-context', {
                userId: template.travellerId.get(),
                itemId: template.travellerId.get(),
                target: 'travellers'
            }, console.log);
        });
    });
});

Template.tasksTravellersModal.onDestroyed(function() {
    Meteor.call('unset-qr-upload-context', console.log);
});

Template.tasksTravellersModal.helpers({
    loadingTraveller() {
        return Template.instance().loadingTraveller.get();
    },
    showTraveller() {
        return Template.instance().showTraveller.get();
    },
    isImageMode(mode) {
        let template = Template.instance();
        return template.imageMode.get() === mode;
    },
    getOptions() {
        let template = Template.instance();
        return {
            lockId: template.lockId,
            lockMessage: "Task is locked by another user",
            sidebarTemplate: "tasksTravellersModal_sidebar",
            activeItem: template.travellerId.get(),
            leftSidebarTemplate: "tasksTravellersModal_leftSidebar",
        }
    },
    getImageUrl() {
        if (this.directUrls) {
            return this.directUrls.vatfree;
        } else {
            return this.url();
        }
    },
    getTraveller() {
        return Travellers.findOne({
            _id: Template.instance().travellerId.get()
        });
    },
    selectedFile() {
        let selectedFile = Template.instance().selectedFile.get();
        if (_.isString(selectedFile)) {
            return Files.findOne({_id: selectedFile});
        } else {
            return selectedFile;
        }
    },
    maskPhotoCallback() {
        let template = Template.instance();
        return function(newFileId) {
            if (newFileId) {
                template.selectedFile.set(newFileId);
                template.imageMode.set('zoom');
            }
        }
    }
});

Template.tasksTravellersModal.events({
    'click .image-mode-zoom'(e, template) {
        e.preventDefault();
        template.imageMode.set('zoom');
    },
    'click .image-mode-crop'(e, template) {
        e.preventDefault();
        template.imageMode.set('crop');
    },
    'click .image-mode-mask'(e, template) {
        e.preventDefault();
        template.imageMode.set('mask');
    },
    'click .traveller-card'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.showTraveller.set(true);
    },
    'click .traveller-edit-form .cancel'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.showTraveller.set();
    },
    'click .task-left-sidebar .file'(e, template) {
        template.showTraveller.set();
    }
});

Template.tasksTravellersModal_leftSidebar.onCreated(function() {
    this.parentTemplate = this.parentTemplate(4);
});

Template.tasksTravellersModal_leftSidebar.helpers({
    getTravellerId() {
        let template = Template.instance();
        return template.data.options.activeItem;
    },
    getFiles() {
        let template = Template.instance();
        let travellerId = template.data.options.activeItem;
        let traveller = Travellers.findOne({
            _id: travellerId
        });
        if (!traveller) return;

        let travellerType = TravellerTypes.findOne({
                _id: traveller.private.travellerTypeId
            }) || {};

        let documentsNeeded = Vatfree.travellers.getDocumentsNeeded(travellerType, traveller.profile.countryId);
        // add the form subType - is not an official document needed as this will mess with the traveller site
        documentsNeeded.push({subType: 'form'});
        let ignoreSubTypes = _.map(documentsNeeded, (doc) => { return doc.subType; });

        return Files.find({
            itemId: travellerId,
            target: 'travellers',
            subType: {
                $nin: ignoreSubTypes
            }
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getInternalFiles() {
        let template = Template.instance();
        let travellerId = template.data.options.activeItem;
        return Files.find({
            itemId: travellerId,
            target: 'travellers_internal',
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getTrashFiles() {
        let template = Template.instance();
        let travellerId = template.data.options.activeItem;
        return Files.find({
            itemId: travellerId,
            target: 'travellers_trash',
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    isActiveFile(id) {
        let template = Template.instance();
        let fileId = template.parentTemplate.selectedFile.get();
        return fileId === id ? 'active' : false;
    },
    selectedFile() {
        let template = Template.instance();
        let travellerId = template.data.options.activeItem;
        let fileId = template.selectedFile.get();
        if (travellerId && fileId) {
            return Files.findOne({
                _id: fileId,
                itemId: travellerId,
                target: 'travellers'
            });
        }
    }
});

Template.tasksTravellersModal_leftSidebar.events({
    'dragstart .draggable': function(e) {
        (e.dataTransfer || e.originalEvent.dataTransfer).setData('targetId', e.target.id);
    },
    'click .task-left-sidebar .file'(e, template) {
        e.preventDefault();
        template.parentTemplate.selectedFile.set();
        template.parentTemplate.showTraveller.set();

        Tracker.afterFlush(() => {
            Meteor.call('getDirectUrl', this._id, ['vatfree', 'vatfree-thumbs'], (err, directUrls) => {
                let file = _.clone(this);
                file.directUrls = directUrls;
                template.parentTemplate.selectedFile.set(file);
                Tracker.afterFlush(() => {
                    if (template.parentTemplate.imageMode.get() === 'mask') {
                        console.log('masking booyah !');
                    } else {
                        // skip all the jQuery stuff
                        Meteor.setTimeout(() => {
                            if (document.querySelector('.task-zoom')) {
                                document.querySelector('.task-zoom').dispatchEvent(new CustomEvent('wheelzoom.destroy'));
                                wheelzoom(document.querySelector('.task-zoom'));
                            }
                        }, 600);
                    }
                });
            });
        });
    }
});

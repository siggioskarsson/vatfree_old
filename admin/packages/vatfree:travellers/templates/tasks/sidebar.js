import moment from 'moment';

Template.tasksTravellersModal_sidebar.onCreated(function() {
    this.isRejecting = new ReactiveVar();
    this.parentTemplate = this.parentTemplate(4);

    this.subscribe('traveller-rejections');
});

Template.tasksTravellersModal_sidebar.onRendered(function() {
    Tracker.afterFlush(() => {
        Vatfree.templateHelpers.initDatepicker.call(this);
    });
});

Template.tasksTravellersModal_sidebar.helpers({
    getTraveller() {
        return Travellers.findOne({
            _id: this.options.activeItem
        });
    },
    getTravellerRejections() {
        return TravellerRejections.find({}, {
            sort: {
                name: 1
            }
        });
    },
    isRejecting() {
        return Template.instance().isRejecting.get();
    }
});

const handleRejection = function (template, reason, formElement) {
    let formData = Vatfree.templateHelpers.getFormData(template, formElement);

    let rejectionIds = [];
    _.each(formData.rejectionIds, (includeRejection, rejectionId) => {
        if (includeRejection === 'on') {
            rejectionIds.push(rejectionId);
        }
    });

    if (rejectionIds.length <= 0) {
        toastr.error('Select 1 or more reasons for the rejection');
        return false;
    }

    Meteor.call('traveller-reject', template.parentTemplate.travellerId.get(), reason, rejectionIds, (err, result) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            template.parentTemplate.travellerId.set();
            template.parentTemplate.selectedFile.set();
        }
    });
};

let taskApprove = function (template) {
    let travellerId = template.parentTemplate.travellerId.get();
    let traveller = Travellers.findOne({_id: travellerId});
    let travellerType = TravellerTypes.findOne({_id: traveller.private.travellerTypeId}) || {};

    let name = template.$('input[name="name"]').val();
    if (!name || name.length <= 0) {
        toastr.error('Traveller name is mandatory');
        return false;
    }
    let passportNr = template.$('input[name="passportNumber"]').val();
    if (!passportNr || passportNr.length <= 0) {
        toastr.error('Traveller passport number is mandatory');
        return false;
    }
    let passportExpiration = template.$('input[name="passportExpirationDate"]').val();
    let passportExpirationDate = moment(passportExpiration, TAPi18n.__('_date_format'));
    if (!passportExpiration || passportExpiration.length <= 0) {
        toastr.error('Traveller passport date is mandatory');
        return false;
    } else {
        if (passportExpirationDate.isBefore(moment())) {
            toastr.error('Traveller passport must be valid on verification');
            return false;
        }
    }
    let countryId = template.$('select[name="countryId"]').val();
    if (!countryId || countryId.length <= 0) {
        toastr.error('Traveller country is mandatory');
        return false;
    }
    let country = Countries.findOne({_id: countryId});

    let documentsError = false;
    let documentsNeeded = Vatfree.travellers.getDocumentsNeeded(travellerType, countryId);
    _.each(documentsNeeded, (documentNeeded) => {
        if (documentNeeded.subType === 'visa') {
            if (country.needVisa && !travellerType.isNato) {
                if (documentNeeded.required && !Files.findOne({
                        itemId: travellerId,
                        target: 'travellers',
                        subType: 'visa'
                    })) {
                    toastr.error(documentNeeded.name + ' image not linked');
                    documentsError = true;
                }

            }
        } else {
            if (documentNeeded.required && !Files.findOne({
                    itemId: travellerId,
                    target: 'travellers',
                    subType: documentNeeded.subType
                })) {
                toastr.error(documentNeeded.name + ' image not linked');
                documentsError = true;
            }
        }
    });
    if (documentsError) {
        return false;
    }

    let profileData = {
        name: name,
        passportNumber: passportNr,
        passportExpirationDate: passportExpirationDate.toDate(),
        countryId: countryId
    };

    let needVisa = Vatfree.travellers.needsVisa(traveller.private.travellerTypeId, countryId);
    if (needVisa && !travellerType.isNato) {
        let visaNr = template.$('input[name="visaNumber"]').val();
        if (!visaNr || visaNr.length <= 0) {
            toastr.error('Traveller visa number is mandatory');
            return false;
        }
        let visaExpiration = template.$('input[name="visaExpirationDate"]').val();
        let visaExpirationDate = moment(visaExpiration, TAPi18n.__('_date_format'));
        if (!visaExpiration || visaExpiration.length <= 0) {
            toastr.error('Traveller visa date is mandatory');
            return false;
        } else {
            if (visaExpirationDate.isBefore(moment())) {
                toastr.error('Traveller visa must be valid on verification');
                return false;
            }
        }
        let visaCountryId = template.$('select[name="visaCountryId"]').val();
        if (!visaCountryId || visaCountryId.length <= 0) {
            toastr.error('Traveller visa country is mandatory');
            return false;
        }

        profileData.visaNumber = visaNr;
        profileData.visaExpirationDate = visaExpirationDate.toDate();
        profileData.visaCountryId = visaCountryId;
    }

    if (confirm('Make the traveller verified?')) {
        template.parentTemplate.updatingTraveller.set(true);
        Meteor.call('traveller-approve', travellerId, profileData, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                template.parentTemplate.travellerId.set();
                template.parentTemplate.selectedFile.set();
                template.parentTemplate.updatingTraveller.set(false);
            }
        });
    }
};

Template.tasksTravellersModal_sidebar.events({
    'click .task-reject'(e, template) {
        e.preventDefault();
        // show receipt rejection reasons
        template.isRejecting.set(true);

        let name = template.$('input[name="name"]').val();
        let passportNr = template.$('input[name="passportNumber"]').val();
        let passportExpiration = template.$('input[name="passportExpirationDate"]').val();
        let passportExpirationDate = moment(passportExpiration, TAPi18n.__('_date_format')).toDate();
        let countryId = template.$('select[name="countryId"]').val();

        let profileData = {
            name: name,
            passportNumber: passportNr,
            passportExpirationDate: passportExpirationDate,
            countryId: countryId
        };

        if (template.$('input[name="visaNumber"]').length > 0) {
            profileData.visaNumber = template.$('input[name="visaNumber"]').val();
            let visaExpiration = template.$('input[name="visaExpirationDate"]').val();
            profileData.visaExpirationDate = moment(visaExpiration, TAPi18n.__('_date_format')).toDate();
            profileData.visaCountryId = template.$('select[name="visaCountryId"]').val();
        }

        // update profile data, but do not change status
        // fire and forget
        Meteor.call('update-traveller-profile', template.parentTemplate.travellerId.get(), profileData, {});

        Tracker.afterFlush(() => {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square icheckbox_square-blue',
                radioClass: 'iradio_square iradio_square-blue'
            });
        });
    },
    'click .task-reject-do'(e, template) {
        e.preventDefault();
        handleRejection(template, 'documentationIncomplete', template.$('.task-rejections'));
    },
    'click .task-reject-eligible'(e, template) {
        e.preventDefault();
        handleRejection(template, 'notEligibleForRefund', template.$('.task-rejections'));
    },
    'click .task-reject-cancel'(e, template) {
        e.preventDefault();
        template.isRejecting.set();
    },
    'click .task-requeue'(e, template) {
        e.preventDefault();
        let travellerId = template.parentTemplate.travellerId.get();

        template.parentTemplate.updatingTraveller.set(true);
        Meteor.call('traveller-requeue', travellerId, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                template.parentTemplate.travellerId.set();
                template.parentTemplate.selectedFile.set();
                template.parentTemplate.updatingTraveller.set(false);
            }
        });
    },
    'click .task-approve'(e, template) {
        e.preventDefault();

        let $maskOpen = $('button#generate-output');
        let $cropOpen = $('button.save-image');
        if ($maskOpen && $maskOpen.length > 0) {
            if (confirm('Save masked image?')) {
                $maskOpen.trigger('click');
                Meteor.setTimeout(() => {
                    taskApprove(template);
                }, 1200);
            } else {
                return taskApprove(template);
            }
        } else if ($cropOpen && $cropOpen.length > 0) {
            if (confirm('Save croppped image?')) {
                $cropOpen.trigger('click');
                Meteor.setTimeout(() => {
                    taskApprove(template);
                }, 1200);
            } else {
                return taskApprove(template);
            }
        } else {
            return taskApprove(template);
        }
    }
});

import './list.html'
import './helpers';

Template.travellersList.helpers(Vatfree.templateHelpers.helpers);
Template.travellersList.helpers(travellerHelpers);
Template.travellersList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/traveller/:itemId', {itemId: this._id});
    }
});

import moment from 'moment';

Template.addTravellerModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-traveller').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-traveller')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-traveller').on('shown.bs.modal', function () {
            $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: TAPi18n.__('_date_format').toLowerCase()
            });
            template.$('select[name="profile.countryId"]').select2();
            template.$('select[name="private.affiliateId"]').select2({
                minimumInputLength: 1,
                multiple: false,
                allowClear: true,
                placeholder: {
                    id: "",
                    placeholder: "Select affiliate ..."
                },
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-affiliates', params.data.q || "", limit, offset, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });

            template.$('input[name="profile.email"]').focus();
        });
        $('#modal-add-traveller').modal('show');
    });
});

Template.addTravellerModal.onDestroyed(function() {
    $('#modal-add-traveller').modal('hide');
});

Template.addTravellerModal.helpers({
});

Template.addTravellerModal.events({
    'click .cancel-add-traveller'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-traveller-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (_.has(formData.profile, 'passportExpirationDate') && formData.profile.passportExpirationDate) {
            formData.profile.passportExpirationDate = moment(formData.profile.passportExpirationDate, TAPi18n.__('_date_format')).toDate();
        }

        if (!formData.profile.countryId && formData.profile.addressCountryId) {
            formData.profile.countryId = formData.profile.addressCountryId;
        }

        Meteor.call('add-traveller', formData, (err, travellerId) => {
            if (err) {
                toastr.error(err.reason, false, {timeOut: 5000});
            } else {
                template.hideModal();
                Meteor.setTimeout(() => {
                    FlowRouter.go('/traveller/' + travellerId);
                }, 300);
            }
        });
    }
});

Template.traveller_last_receipts.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        if (data && data.travellerId) {
            this.subscribe('user-receipts', data.travellerId);
        }

        if (!data.limit) {
            this.data.limit = 10;
        }
    });

    this.autorun(() => {
        this.subscribe('receipt-types');
});
});

Template.traveller_last_receipts.helpers({
    getReceipts() {
        let selector = {};

        if (this.filter) {
            selector = _.clone(this.filter);
        }

        if (this.travellerId) {
            selector.userId = this.travellerId;
        }

        return Receipts.find(selector, {
            sort: {
                createdAt: -1
            },
            limit: this.limit || 10
        });
    },
    getReceiptFiles() {
        return Files.find({
            itemId: this._id,
            target: "receipts"
        });
    }
});

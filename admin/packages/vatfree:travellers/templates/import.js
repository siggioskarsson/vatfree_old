Template.travellersImport.events(Vatfree.templateHelpers.events());
Template.travellersImport.onCreated(function() {
    this.handleFileUpload = function(file) {
        let newFile = new FS.File(file);
        newFile.name(file.name.replace(/[^a-z0-9-_.]/ig, '_'));
        newFile.target = 'imports';
        newFile.subType = 'travellers';
        newFile.createdAt = new Date();
        newFile.uploadedBy = Meteor.userId();
        newFile.importInfo = {
            running: false,
            progress: 0,
            progressMessage: ''
        };
        Files.insert(newFile, function (err) {
            if (err) {
                toastr.error(err);
            }
        });
    }
});

Template.travellersImport.onRendered(function() {
    this.autorun(() => {
        this.subscribe('imports', 'travellers');
    });
});

Template.travellersImport.helpers({
    getImports() {
        return Files.find({
            target: 'imports',
            subType: 'travellers'
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getStatus() {
        if (this.importInfo.progress === 100) {
            return "Done";
        } else if (this.importInfo.running) {
            return "Running";
        } else {
            return "-";
        }
    },
    getError() {
        if (_.isObject(this.error)) {
            return this.error.code;
        } else {
            return this.error;
        }
    }
});

Template.travellersImport.events({
    'click .import-file'(e, template) {
        e.preventDefault();
        Meteor.call('import-travellers', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.info("Import started");
            }
        });
    }
});

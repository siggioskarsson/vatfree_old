Template.travellers_merge.onCreated(function() {
    let template = this;

    this.isLoaded = new ReactiveVar();
    this.isMerging = new ReactiveVar();
    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-travellers-merge').modal('hide');
    };

    this.travellerId = new ReactiveVar();
    this.counts = new ReactiveVar();

    Tracker.afterFlush(() => {
        $('#modal-travellers-merge').on('hidden.bs.modal', function () {
            if (template.parentTemplate && template.parentTemplate.mergeTraveller) template.parentTemplate.mergeTraveller.set(false);
        });
        $('#modal-travellers-merge').on('shown.bs.modal', function () {
            template.isLoaded.set(true);

            template.$('select[name="travellerId"]').select2({
                minimumInputLength: 1,
                multiple: false,
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-travellers', params.data.q || "", limit, offset, true, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });
        });
        $('#modal-travellers-merge').modal('show');
    });
});

Template.travellers_merge.onRendered(function() {
    let template = this;

    this.autorun(() => {
        let travellerId = this.data.travellerId;
        if (travellerId) {
            Meteor.call('traveller-get-linked-elements', travellerId, (err, counts) => {
                template.counts.set(counts);
            });
        }
    });

    this.autorun(() => {
        let travellerId = this.travellerId.get();
        if (travellerId) {
            this.subscribe('travellers_item', travellerId);
        }
    });
});

Template.travellers_merge.onDestroyed(function() {
    this.hideModal();

    if (this.parentTemplateVariable && this.parentTemplate[this.parentTemplateVariable]) {
        this.parentTemplate[this.parentTemplateVariable].set(false);
    }

    $('body').removeClass('modal-open');
    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.travellers_merge.helpers({
    isMerging() {
        return Template.instance().isMerging.get();
    },
    getTraveller() {
        return Travellers.findOne({_id: Template.instance().travellerId.get() });
    },
    getCounts() {
        return Template.instance().counts.get();
    }
});

Template.travellers_merge.events({
    'change select[name="travellerId"]'(e, template) {
        template.travellerId.set($(e.currentTarget).val());
    },
    'click .cancel-merge-traveller'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.hideModal();
    },
    'submit form[name="send-merge-traveller-form"]'(e, template) {
        e.preventDefault();
        let travellerId = this.travellerId;
        let mergeWithTravellerId = template.$('select[name="travellerId"]').val();

        if (travellerId === mergeWithTravellerId) {
            alert('Cannot merge traveller with self');
            return false;
        }

        if (!confirm('Merge travellers?')) {
            return false;
        }

        template.isMerging.set(true);
        Meteor.call('merge-traveller', travellerId, mergeWithTravellerId, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                FlowRouter.go('/traveller/' + mergeWithTravellerId);
                template.hideModal();
            }
            template.isMerging.set(false);
        });
    }
});

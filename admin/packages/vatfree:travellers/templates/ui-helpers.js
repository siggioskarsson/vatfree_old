import moment from 'moment';

UI.registerHelper('getTravellerIcon', function(status) {
    status = status || this.status;

    let icon = Vatfree.travellers.icons();
    return _.has(icon, status) ? icon[status] : "circle-o";
});

UI.registerHelper('getTravellerPassportIcon', function(expirationDate) {
    expirationDate = expirationDate || this.expirationDate;

    if (!expirationDate) {
        return "icon icon-passportincomplete";
    } else {
        let passportExpiration = moment(expirationDate);
        if (moment().isBefore(passportExpiration)) {
            return "icon icon-passport";
        } else {
            return "icon icon-passportexpired";
        }
    }
});

UI.registerHelper('getTravellerStatusOptions', function() {
    return _.clone(Meteor.users.simpleSchema()._schema['private.status'].allowedValues);
});

UI.registerHelper('userIsEligible', function() {
    if (this.private && this.private.status) {
        return this.private.status === 'userVerified';
    }
});

UI.registerHelper('getTravellerName', function(travellerId) {
    travellerId = travellerId || this.travellerId || this.userId;
    let traveller = Travellers.findOne({_id: travellerId});
    if (traveller) {
        return traveller.profile.name || traveller.profile.email;
    }
});

UI.registerHelper('getTravellerAge', function(travellerId) {
    travellerId = travellerId || this.travellerId;
    let traveller = Travellers.findOne({_id: travellerId});
    if (traveller && traveller.profile && traveller.profile.birthDate) {
        return moment().diff(moment(traveller.profile.birthDate), 'years');
    }
});

UI.registerHelper('getTravellerType', function() {
    if (this.private && this.private.travellerTypeId) {
        let travellerType = TravellerTypes.findOne({
            _id: this.private.travellerTypeId
        });
        if (travellerType) {
            return travellerType.name;
        }
    }
});

UI.registerHelper('getTravellerTypeName', function() {
    if (this.private && this.private.travellerTypeId) {
        let travellerType = TravellerTypes.findOne({
            _id: this.private.travellerTypeId
        });
        if (travellerType) {
            return travellerType.name;
        }
    }
});

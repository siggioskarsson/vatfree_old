import moment from 'moment';
import QRCode from 'qrcode';
import { editFormOnCreated, editFormHelpers, editFormEvents } from 'meteor/vatfree:core-templates/templates/edit-form-helpers';

Template.travellerEdit.onCreated(function () {
    this.activeReceiptId = new ReactiveVar();
});

Template.travellerEdit.onRendered(function () {
    this.autorun(() => {
        let travellerId = FlowRouter.getParam('travellerId');
        this.subscribe('travellers_item', travellerId);

        Meteor.call('set-qr-upload-context', {
            userId: travellerId,
            itemId: travellerId,
            target: 'travellers'
        }, console.log);
    });
});

Template.travellerEdit.onDestroyed(function () {
    Meteor.call('unset-qr-upload-context', console.log);
});

Template.travellerEdit.helpers({
    itemDoc() {
        return Travellers.findOne({
            _id: FlowRouter.getParam('travellerId'),
            'roles.__global_roles__': 'traveller'
        });
    },
    getItemId() {
        return FlowRouter.getParam('travellerId');
    },
    isContact() {
        let user = Travellers.findOne({
            _id: FlowRouter.getParam('travellerId')
        });
        if (user && user.roles) {
            return _.contains(user.roles.__global_roles__, 'contact');
        }
    },
    isAffiliate() {
        let user = Travellers.findOne({
            _id: FlowRouter.getParam('travellerId')
        });
        if (user && user.roles) {
            return _.contains(user.roles.__global_roles__, 'affiliate');
        }
    },
    getBreadcrumbTitle() {
        return (this.profile.name ? this.profile.name + ', ' : '') + (this.profile.email || "<i>no email</i>");
    },
    activeReceiptId() {
        return Template.instance().activeReceiptId.get();
    },
    addActiveReceiptId() {
        this.activeReceiptId = Template.instance().activeReceiptId.get();
        return this;
    },
    getSelectedReceipt() {
        let receiptId = Template.instance().activeReceiptId.get();
        if (receiptId) {
            return Receipts.findOne({_id: receiptId});
        }
    }
});

Template.travellerEdit.events({
    'click .row.receipts table.table.receipt-list .item-row'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeReceiptId) {
            if (template.activeReceiptId.get() === this._id) {
                template.activeReceiptId.set();
            } else {
                template.activeReceiptId.set(this._id);
            }
        }
    }
});

Template.travellerEditForm.onCreated(editFormOnCreated());
Template.travellerEditForm.onCreated(function () {
    let template = this;
    this.countryId = new ReactiveVar();
    this.travellerTypeId = new ReactiveVar();
    this.editImageFile = new ReactiveVar();
    this.mergeTraveller = new ReactiveVar();
    this.activeReceiptId = new ReactiveVar();
    this.travellerId = new ReactiveVar(this.data._id);

    this.handleFileUpload = function (file) {
        Vatfree.templateHelpers.handleFileUpload(file, {
            itemId: template.travellerId.get(),
            userId: template.travellerId.get(),
            target: 'travellers'
        });
    };

    this.autorun(() => {
        let data = Template.currentData();
        this.travellerId.set(data._id);
        this.activeReceiptId.set(data.activeReceiptId);
    });

    this.autorun(() => {
        let travellerId = template.travellerId.get();
        if (travellerId) {
            this.subscribe('traveller-files', travellerId);
        }
    });
});

Template.travellerEditForm.onRendered(function () {
    const template = this;

    this.autorun(() => {
        let data = Template.currentData();
        this.countryId.set(data.profile.countryId);
        this.travellerTypeId.set(data.private.travellerTypeId);
    });

    Tracker.afterFlush(() => {
        $('select[name="profile.addressCountryId"]').select2({
            width: '100%'
        });
        $('select[name="profile.countryId"]').select2({
            width: '100%'
        });
        $('select[name="profile.visaCountryId"]').select2({
            width: '100%'
        });
        this.$('select[name="private.affiliateId"]').select2({
            minimumInputLength: 1,
            multiple: false,
            allowClear: true,
            placeholder: {
                id: "",
                placeholder: "Select affiliate ..."
            },
            ajax: {
                transport: function (params, success, failure) {
                    let limit = 20;
                    let offset = 0;
                    Meteor.call('search-affiliates', params.data.q || "", limit, offset, (err, res) => {
                        if (err) {
                            failure(err);
                        } else {
                            success({results: res});
                        }
                    });
                },
                delay: 500
            }
        });
        Vatfree.templateHelpers.initDatepicker.call(this);
    });

    this.autorun(() => {
        let data = Template.currentData();
        if (this.subscriptionsReady()) {
            Tracker.afterFlush(() => {
                let qrcodeCanvas = document.getElementById('qrcode-upload');
                let uploadContext = {
                    userId: data._id,
                    itemId: data._id,
                    target: 'travellers'
                };
                QRCode.toCanvas(qrcodeCanvas, 'vatfree://image-upload/?context=' + JSON.stringify(uploadContext), {
                    margin: 2,
                    errorCorrectionLevel: 'Q'
                }, (err) => {
                    if (err) {
                        toastr.error(err);
                    }
                });
                let qrcodeCanvasInternal = document.getElementById('qrcode-upload-internal');
                let uploadContextInternal = {
                    userId: data._id,
                    itemId: data._id,
                    target: 'travellers_internal'
                };
                QRCode.toCanvas(qrcodeCanvasInternal, 'vatfree://image-upload/?context=' + JSON.stringify(uploadContextInternal), {
                    margin: 2,
                    errorCorrectionLevel: 'Q'
                }, (err) => {
                    if (err) {
                        toastr.error(err);
                    }
                });
            });
        }
    });

    this.autorun(() => {
        if (this.tabLoading.get('tab-4')) {
            this.subscribe('activity-logs', "", {travellerId: template.travellerId.get()}, (ready) => {
                this.tabLoading.set('tab-4', true);
            });
        }
    });
    this.autorun(() => {
        if (this.tabLoading.get('tab-6')) {
            this.subscribe('activity-stream', {userId: template.travellerId.get()}, (ready) => {
                this.tabLoading.set('tab-6', true);
            });
        }
    });
    this.autorun(() => {
        if (this.tabLoading.get('tab-2')) {
            // no-op, subscription is in template
            this.tabLoading.set('tab-2', true);
        }
    });
});


Template.travellerEditForm.helpers(editFormHelpers());
Template.travellerEditForm.helpers({
    isDisabled() {
        return !Vatfree.userIsInRole(Meteor.userId(), 'travellers-update');
    },
    getTravellerId() {
        return this._id;
    },
    getCountryId() {
        return Template.instance().countryId.get();
    },
    mergeTraveller() {
        return Template.instance().mergeTraveller.get();
    },
    getAffiliateName() {
        let affiliate = Affiliates.findOne({_id: this.private.affiliateId});
        if (affiliate) {
            return affiliate.profile.name + ' (' + affiliate.affiliate.code + ') - ' + (affiliate.profile.addressFirst || "") + ', ' + (affiliate.profile.postalCode || "") + ' ' + (affiliate.profile.city || "");
        }
    },
    getFiles() {
        let travellerId = this._id;
        let traveller = Travellers.findOne({_id: travellerId});
        if (!traveller) return;

        let travellerType = TravellerTypes.findOne({
            _id: traveller.private.travellerTypeId
        }) || {};

        let documentsNeeded = Vatfree.travellers.getDocumentsNeeded(travellerType, traveller.profile.countryId);
        // add the form subType - is not an official document needed as this will mess with the traveller site
        documentsNeeded.push({subType: 'form'});
        let ignoreSubTypes = _.map(documentsNeeded, (doc) => { return doc.subType; });

        return Files.find({
            itemId: travellerId,
            target: 'travellers',
            subType: {
                $nin: ignoreSubTypes
            }
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getInternalFiles() {
        let travellerId = this._id;
        return Files.find({
            itemId: travellerId,
            target: 'travellers_internal'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getTrashFiles() {
        let travellerId = this._id;
        return Files.find({
            itemId: travellerId,
            target: 'travellers_trash'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    isStatus(status) {
        let template = Template.instance();
        return template.data.private.status === status;
    },
    isTravellerType(travellerTypeId) {
        let template = Template.instance();
        return template.data.private.travellerTypeId === travellerTypeId;
    },
    isCountrySelected() {
        let template = Template.instance();
        return template.data.profile.countryId === this._id;
    },
    getActivityLog() {
        return ActivityLogs.find({
            travellerId: this._id
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    isRejectionStatus() {
        return this.private.status === 'documentationIncomplete' || this.private.status === 'notEligibleForRefund';
    },
    getPaymentMethods() {
        return Vatfree.payments.methods;
    },
    editImageFile() {
        return Template.instance().editImageFile.get();
    },
    needsVisa() {
        const template = Template.instance();
        return Vatfree.travellers.needsVisa(template.travellerTypeId.get(), template.countryId.get());
    },
    getLoginServices() {
        if (this.services) {
            return _.keys(this.services);
        }
    },
    getPayouts() {
        return Payouts.find({
            travellerId: this._id
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    secondaryEmails() {
        let emails = [];
        _.each(this.emails, (email) => {
            if (email.address !== this.profile.email) {
                emails.push(email);
            }
        });

        return emails;
    },
    addActiveReceiptId() {
        this.activeReceiptId = Template.instance().activeReceiptId.get();
        return this;
    },
    disallowPassportImagesEdits() {
        return !Vatfree.userIsInRole(Meteor.userId(), 'travellers-update');
    },
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            }
        });
    }
});

Template.travellerEditForm.events(editFormEvents());
Template.travellerEditForm.events(Vatfree.templateHelpers.events());
Template.travellerEditForm.events({
    'change select[name="profile.countryId"]'(e, template) {
        e.preventDefault();
        template.countryId.set($(e.currentTarget).val());
    },
    'change select[name="private.travellerTypeId"]'(e, template) {
        e.preventDefault();
        template.travellerTypeId.set($(e.currentTarget).val());
    },
    'dragstart .draggable': function(e) {
        (e.dataTransfer || e.originalEvent.dataTransfer).setData('targetId', e.target.id);
    },
    'click .set-file-passport'(e) {
        e.preventDefault();
        let travellerId = template.travellerId.get();
        Vatfree.files.setTravellerSubType(travellerId, this._id, 'passport');
    },
    'click .set-file-visa'(e) {
        e.preventDefault();
        let travellerId = template.travellerId.get();
        Vatfree.files.setTravellerSubType(travellerId, this._id, 'visa');
    },
    'click .move-file-from-trash'(e, template) {
        e.preventDefault();
        Files.update({
            _id: this._id
        },{
            $set: {
                target: "travellers_internal"
            }
        });
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/travellers');
    },
    'click .dropzone-layout .add-file-internal': function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('input[name="dropzone-file-internal"]').trigger('click');
    },
    'change input[name="dropzone-file-internal"]': function(e, template) {
        e.preventDefault();
        e.stopPropagation();
        let travellerId = template.travellerId.get();
        let context = {
            itemId: travellerId,
            userId: travellerId,
            target: 'travellers_internal'
        };
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, context);
        });
    },
    'dropped .dropzone-layout.file-internal': function (e, template) {
        e.preventDefault();
        e.stopPropagation();
        $('.dropzone').removeClass('visible');
        let travellerId = template.travellerId.get();
        let context = {
            itemId: travellerId,
            userId: travellerId,
            target: 'travellers_internal'
        };
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, context);
        });
    },
    'click .change-email'(e, template) {
        e.preventDefault();
        let traveller = this;
        import swal from 'sweetalert';
        import 'swal-forms';
        swal.withForm({
            title: 'Change email?',
            text: 'Type in the new email address',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Change email!',
            closeOnConfirm: true,
            formFields: [
                { id: 'email', placeholder: 'Email', type: 'email', value: traveller.profile.email }
            ]
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('change-traveller-email', traveller._id, this.swalForm.email, (err) => {
                    if (err) {
                        toastr.error(err.reason || err.message);
                    }
                });
            }
        });
    },
    'click .send-traveller-magic-link'(e, template) {
        e.preventDefault();
        let traveller = this;
        import swal from 'sweetalert';
        swal({
            title: 'Seend magic link?',
            text: 'Traveller will be able to automatically login to the traveller site',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Send it!',
            closeOnConfirm: true,
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('send-traveller-magic-link-email', traveller._id, (err, result) => {
                    if (err) {
                        toastr.error(err.reason || err.message);
                    } else if (result) {
                        toastr.success("Magic link sent to traveller");
                    } else {
                        toastr.warning("Hmmmmm, this should not happen. Not sure what happened. Please check the Activity of the traveller.");
                    }
                });
            }
        });
    },
    'click .merge-traveller'(e, template) {
        e.preventDefault();
        template.mergeTraveller.set(true);
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (_.has(formData.profile, 'passportExpirationDate') && formData.profile.passportExpirationDate) {
            formData.profile.passportExpirationDate = moment(formData.profile.passportExpirationDate, TAPi18n.__('_date_format')).toDate();
        } else {
            formData.profile.passportExpirationDate = null;
        }
        if (_.has(formData.profile, 'visaExpirationDate') && formData.profile.visaExpirationDate) {
            formData.profile.visaExpirationDate = moment(formData.profile.visaExpirationDate, TAPi18n.__('_date_format')).toDate();
        } else {
            formData.profile.visaExpirationDate = null;
        }
        if (_.has(formData.profile, 'birthDate') && formData.profile.birthDate) {
            formData.profile.birthDate = moment(formData.profile.birthDate, TAPi18n.__('_date_format')).toDate();
        } else {
            formData.profile.birthDate = null;
        }
        if (_.has(formData.profile, 'contactId') && formData.profile.contactId) {
            formData.profile.contactId = formData.profile.contactId.split(',');
        }

        Meteor.call('update-traveller-profile', this._id, formData.profile, formData.private, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                toastr.success('Traveller saved!')
            }
        });
    },
    'click div.row.receipts .task-item'(e) {
        e.preventDefault();
        e.stopPropagation();
        FlowRouter.go('/receipts?tasks=1&receiptId=' + this._id);
    }
});

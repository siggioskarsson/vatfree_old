var FileSaver = require('file-saver');
import moment from 'moment';

Template.travellers.onCreated(Vatfree.templateHelpers.onCreated(Travellers, '/travellers/', '/traveller/:itemId'));
Template.travellers.onCreated(function() {
    this.showImports = new ReactiveVar();
    this.doingTask = new ReactiveVar();
    this.runningExport = new ReactiveVar();
    this.taskTravellerId = new ReactiveVar();
    if (!this.sort.get()) {
        this.sort.set({'createdAt': -1});
    }
    this.statusAttribute = 'private.status';

    this.typeFilter = new ReactiveArray();

    _.each(Session.get(this.view.name + '.typeFilter') || [], (status) => {
        this.typeFilter.push(status);
    });
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.typeFilter', this.typeFilter.array());
    });

    this.selectorFunction = (selector) => {
        let typeFilter = this.typeFilter.array() || [];
        if (typeFilter.length > 0) {
            selector['private.travellerTypeId'] = {
                $in: typeFilter
            };
        }
    }
});

Template.travellers.onRendered(Vatfree.templateHelpers.onRendered());
Template.travellers.onRendered(function() {
    this.autorun(() => {
        this.subscribe('traveller-stats');
    });

    this.autorun(() => {
        if (FlowRouter.getQueryParam('tasks')) {
            Tracker.afterFlush(() => {
                this.doingTask.set(true);
            });
        }
    });

    this.todoStatus = [
        'userUnverified'
    ];
});

Template.travellers.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.travellers.helpers(Vatfree.templateHelpers.helpers);
Template.travellers.helpers(travellerHelpers);
Template.travellers.helpers({
    showImports() {
        return Template.instance().showImports.get();
    },
    doingTask() {
        return Template.instance().doingTask.get();
    },
    getTaskTravellerId() {
        return Template.instance().taskTravellerId.get();
    },
    getTravellerStats() {
        let template = Template.instance();
        let selector = travellerHelpers.getTravellersSelector(template);

        let stats = ReactiveMethod.call('get-travellers-stats', selector);
        return stats && stats[0] ? stats[0].value : false;
    },
    isActiveTravellerTypeFilter() {
        let typeFilter = Template.instance().typeFilter.array() || [];
        return _.contains(typeFilter, this._id) ? 'active' : '';
    }
});

Template.travellers.events(Vatfree.templateHelpers.events());
Template.travellers.events({
    'click .filter-icon.traveller-type-filter-icon'(e, template) {
        e.preventDefault();
        let type = this._id;
        let typeFilter = template.typeFilter.array();

        if (_.contains(typeFilter, type)) {
            template.typeFilter.clear();
            _.each(typeFilter, (_type) => {
                if (type !== _type) {
                    template.typeFilter.push(_type);
                }
            });
        } else {
            template.typeFilter.push(type);
        }
    },
    'click .check-travellers-waiting-for-docs'(e, template) {
        e.preventDefault();
        let selector = travellerHelpers.getTravellersSelector(template);
        Vatfree.templateHelpers.openModal('travellers_modal_waiting_for_docs', {fullscreen: true, selector: selector});
    },
    'click .travellers-import'(e, template) {
        e.preventDefault();
        template.showImports.set(!template.showImports.get());
    },
    'click .traveller-tasks'(e, template) {
        e.preventDefault();
        template.taskTravellerId.set();
        template.doingTask.set(true);
    },
    'click .task-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.taskTravellerId.set(this._id);
        template.doingTask.set(true);
    },
    'click .traveller-email-export'(e, template) {
        e.preventDefault();
        template.runningExport.set(true);

        Meteor.setTimeout(() => {
            Meteor.call('traveller-email-export', template.searchTerm.get(), template.statusFilter.array(), (err, csv) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    var blob = new Blob([csv], {
                        type: "text/plain;charset=utf-8"
                    });
                    FileSaver.saveAs(blob, "traveller_email_export_" + moment().format(TAPi18n.__('_date_time_format')) + ".csv");
                }
                template.runningExport.set(false);
            });
        }, 1);
    },
    'click .travellers-send-reminders'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).attr('disabled') !== "disabled") {
            if (confirm('Do you want to send all traveller reminders?')) {
                Meteor.call('send-traveller-reminders', (err, count) => {
                    if (err) {
                        alert(err.reason || err.message);
                    } else {
                        import swal from 'sweetalert2';
                        swal("Sending reminders", "Traveller reminder sending has been started in the background.", "success");
                    }
                });
            }
        }
    }
});

import moment from 'moment';

Template.travellerInfo.onCreated(function() {
    this.showFile = new ReactiveVar();
});

Template.travellerInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.travellerInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('traveller-files', data.traveller._id);
        this.subscribe('activity-stream', {userId: data.traveller._id}, null, limit, offset);
        this.subscribe('activity-stream', {}, data.traveller._id, limit, offset);
    });
});

Template.travellerInfo.helpers(travellerHelpers);
Template.travellerInfo.helpers({
    getShowFile() {
        return Template.instance().showFile.get();
    },
    getContactIds() {
        let contactId = this.profile && this.profile.contactId;
        return (_.isArray(contactId) ? contactId : (_.isString(contactId)) ? [ contactId ] : false);
    },
    getActivity() {
        return ActivityStream.collection.find({
            $or: [
                {
                    'securityContext.userId': this._id
                },
                {
                    'activity.target.itemId': this._id
                }
            ]
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    },
    getFiles() {
        let travellerId = this._id;
        return Files.find({
            itemId: travellerId,
            target: 'travellers'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    alternativeTravellerType() {
        return this.private.travellerTypeId && this.private.travellerTypeId !== Vatfree.defaults.TravellerTypeId;
    }
});

Template.travellerInfo.events({
    'click .traveller-files .file a'(e, template) {
        if (template.data.openImagesInInPane) {
            e.preventDefault();
            e.stopPropagation();
            template.showFile.set(this.fileObj || this);
        }
    },
    'click .close-show-file'(e, template) {
        e.preventDefault();
        template.showFile.set();
    }
});

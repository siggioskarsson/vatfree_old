Template.travellerPassportImages.onCreated(function () {
    this.travellerTypeId = new ReactiveVar();
});

Template.travellerPassportImages.onRendered(function () {
    this.autorun(() => {
        let data = Template.currentData();
        if (data.travellerId) {
            this.subscribe('travellers_item', data.travellerId);

            let traveller = Travellers.findOne({_id: data.travellerId}) || {private: {}};
            this.travellerTypeId.set(traveller.private.travellerTypeId);
        }
    });
});

Template.travellerPassportImages.helpers(travellerHelpers);
Template.travellerPassportImages.helpers({
    getIdFile() {
        let travellerId = Template.instance().data.travellerId;
        return Files.findOne({
            itemId: travellerId,
            target: 'travellers',
            subType: this.subType
        });
    }
});

Template.travellerPassportImages.events({
    'click .unset-file-type'(e) {
        e.preventDefault();
        Files.update({
            _id: this._id
        },{
            $unset: {
                subType: 1
            }
        });
    },
    'dropped .file-dropzone': function (e, template) {
        e.preventDefault();
        let travellerId = template.data.travellerId;
        let fileId = (e.dataTransfer || e.originalEvent.dataTransfer).getData('targetId');
        let subType = $(e.currentTarget).data('subType');
        if (travellerId && fileId && subType) {
            Vatfree.files.setTravellerSubType(travellerId, fileId, subType);
        }
    }
});

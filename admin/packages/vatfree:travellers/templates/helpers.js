/* global travellerHelpers: true */

travellerHelpers = {
    getTravellersSelector: function (template) {
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }
        Vatfree.search.addStatusFilter.call(template, selector);
        Vatfree.search.addListFilter.call(template, selector);
        template.selectorFunction(selector);

        return selector;
    },
    getTravellers() {
        let template = Template.instance();
        let selector = travellerHelpers.getTravellersSelector(template);

        return Travellers.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    isMyTask() {
        return this.private && (!this.private.status || this.private.status === 'userUnverified');
    },
    getContactType() {
        switch (this.profile.contactType) {
            case 'tel':
                return 'phone';
            case 'email':
            default:
                return 'envelope';

        }
    },
    getContactDetails() {
        return this.profile.email;
    },
    getPartnershipStatusLogo() {
        switch (this.partnershipStatus) {
            case 'pledger':
                return 'VFCircleBlue.svg';
            case 'partner':
                return 'VFCircleShade.svg';
            default:
                return 'CircleShadeGreyNonRefund.svg';
        }
    },
    hasPassportError(travellerId, travellerTypeId) {
        travellerTypeId = _.isString(travellerTypeId) ? travellerTypeId : "";
        let traveller = Travellers.findOne({_id: travellerId});
        if (!traveller || !traveller.private) {
            return true;
        }

        let travellerType = TravellerTypes.findOne({
            _id: travellerTypeId || traveller.private.travellerTypeId
        });
        if (!travellerType) {
            return true;
        }

        let countryId = traveller.profile.countryId;
        let template = Template.instance();
        if (template && template.countryId && template.countryId.get) {
            countryId = template.countryId.get();
        }

        let country = Countries.findOne({_id: countryId});
        if (!country) {
            return true;
        }

        let documentsNeeded = Vatfree.travellers.getDocumentsNeeded(travellerType, countryId);

        let error = false;
        _.each(documentsNeeded, (doc) => {
            if (doc.required && !Files.findOne({
                    itemId: travellerId,
                    target: 'travellers',
                    subType: doc.subType
                })) {
                error = true;
            }
        });
        return error;
    },
    getIdNeeded(travellerTypeId) {
        let template = Template.instance();
        let travellerCountryId = '';

        if (!travellerTypeId) {
            travellerTypeId = template.travellerTypeId.get();
            if (travellerTypeId === "nato") {
                travellerTypeId = template.natoTravellerTypeId.get();
            }
        }

        if (template.data && template.data.travellerId) {
            let traveller = Travellers.findOne({_id: template.data.travellerId}) || {};
            travellerCountryId = traveller.profile.countryId;
        }
        let countryId = travellerCountryId || (template.countryId ? template.countryId.get() : false);

        let travellerType = TravellerTypes.findOne({
            _id: travellerTypeId
        });
        if (travellerType) {
            return Vatfree.travellers.getDocumentsNeeded(travellerType, countryId);
        }
    },
    getForm() {
        return {
            subType: "form",
            name: "Form",
            required: true
        }
    },
    allowedToDelete() {
        let traveller = Travellers.findOne({_id: (this.userId || this.itemId)}) || {};
        if (traveller.private && traveller.private.status) {
            if (_.contains(['userUnverified', 'waitingForDocuments', 'documentationExpired', 'documentationIncomplete', 'notEligibleForRefund'], traveller.private.status)) {
                return true;
            }
        }

        return false;
    },
    canUploadNewDocuments() {
        if (this.private && this.private.status) {
            return _.contains(['userUnverified', 'waitingForDocuments', 'documentationExpired', 'documentationIncomplete', 'notEligibleForRefund'], this.private.status);
        }
    },
    allowDraggable() {
        if (this.private && this.private.status) {
            return _.contains(['userUnverified', 'waitingForDocuments', 'documentationExpired', 'documentationIncomplete', 'notEligibleForRefund'], this.private.status) ? "draggable" : "";
        }
    }
};

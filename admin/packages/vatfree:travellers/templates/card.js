import moment from 'moment/moment';
import './card.html';

Template.travellerCard.helpers({
    getCountry(countryId) {
        let travellerProfile = this.profile || {};
        return Countries.findOne({
            _id: countryId || travellerProfile.countryId
        });
    },
    passportExpired() {
        let travellerProfile = this.profile || {};
        return moment(travellerProfile.passportExpirationDate).isBefore(moment());
    },
    isNato() {
        let travellerPrivate = this.private || {};
        let travellerType = TravellerTypes.findOne({_id: travellerPrivate.travellerTypeId });
        return travellerType && travellerType.isNato;
    }
});

Template.traveller_svg_form.helpers({
    getPassportNumberArray() {
        let passportNumber = this.traveller.profile.passportNumber || "";
        return passportNumber.split("");
    }
});

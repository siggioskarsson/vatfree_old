Template.travellerEditReceipts.onCreated(function () {
    this.addingReceipt = new ReactiveVar();
    this.activeReceiptId = new ReactiveVar();
    this.useListview = new ReactiveVar(true);
    this.detailUrl = '/receipt/:itemId';

    this.autorun(() => {
        let data = Template.currentData();
        if (data && data._id) {
            this.subscribe('user-receipts', data._id);
        }
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.activeReceiptId.set(data.activeReceiptId);
    });
});

Template.travellerEditReceipts.helpers({
    getPanelReceiptStatus() {
        let status = Vatfree.receipts.status();
        return _.has(status, this.status) ? status[this.status] : "default";
    },
    getPanelReceiptIcon() {
        let icon = Vatfree.receipts.icons();
        return _.has(icon, this.status) ? icon[this.status] : "circle-o";
    },
    addingReceipt() {
        return Template.instance().addingReceipt.get();
    },
    activeReceiptId() {
        return Template.instance().activeReceiptId.get();
    },
    useListview() {
        return Template.instance().useListview.get();
    },
    getReceipts() {
        return Receipts.find({
            userId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getShopName() {
        let shop = Shops.findOne({_id: this.shopId});
        if (shop) {
            return shop.name;
        }
    }
});

Template.travellerEditReceipts.events({
   'click .icon-toggle.listview'(e, template) {
        e.preventDefault();
        template.useListview.set(true);
    },
    'click .icon-toggle.cardview'(e, template) {
        e.preventDefault();
        template.useListview.set(false);
    },
    'click .add-receipt'(e, template) {
        e.preventDefault();
        template.addingReceipt.set(true);
    }
});

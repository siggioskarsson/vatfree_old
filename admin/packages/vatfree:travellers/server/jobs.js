if (!Meteor.settings.disableJobs) {
    travellerJobs.allow({
        // Grant full permission to any authenticated user
        admin: function (userId, method, params) {
            return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
        }
    });

    Meteor.publish('traveller-jobs', function () {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return travellerJobs.find({
            status: {
                $ne: 'completed'
            }
        });
    });

    if (Meteor.settings.jobs && Meteor.settings.jobs.travellers) {
        Meteor.startup(function () {
            // Start the myJobs queue running
            travellerJobs.startJobServer();

            if (Meteor.settings.jobs.travellers.reminders) {
                travellerJobs.processJobs('sendTravellerReminders', Meteor.settings.jobs.travellers.reminders, (job, cb) => {
                    Vatfree.travellers.reminders.sendAll((err, numberSent) => {
                        if (err) {
                            job.fail(err, {
                                fatal: false
                            });
                        } else {
                            job.log('Sent ' + numberSent + ' reminders for travellers');
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                });
            }
        });
    }
}

export const getAllTravellerData = function(travellerId) {
    return {
        user: getAllTravellerUserData(travellerId),
        receipts: getAllTravellerReceiptData(travellerId),
        payouts: getAllTravellerPayoutData(travellerId),
        activity: getAllTravellerActivityData(travellerId)
    };
};

const getAllTravellerUserData = function(travellerId) {
    return Meteor.users.findOne({
        _id: travellerId
    },{
        fields: {
            services: false,
            private: false,
            roles: false,
            textSearch: false
        }
    });
};

const getAllTravellerReceiptData = function(travellerId) {
    return Receipts.find({
        userId: travellerId
    },{
        fields: {
            textSearch: false
        }
    }).fetch();
};

const getAllTravellerPayoutData = function(travellerId) {
    return Payouts.find({
        userId: travellerId
    },{
        fields: {
            textSearch: false
        }
    }).fetch();
};

const getAllTravellerActivityData = function(travellerId) {
    return ActivityLogs.find({
        userId: travellerId,
        type: {
            $ne: 'task'
        }
    },{
        fields: {
            textSearch: false
        }
    }).fetch();
};

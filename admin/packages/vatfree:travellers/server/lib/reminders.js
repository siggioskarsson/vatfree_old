import moment from "moment/moment";

Vatfree.travellers.reminders = {
    createdWaitDays: 30,
    reminderWaitDays: 14,
    status: [
        'waitingForDocuments',
        'documentationIncomplete'
    ],
    querySelector: function() {
        return {
            'profile.email': {
                $exists: true
            },
            'private.status': {
                $in: Vatfree.travellers.reminders.status
            },
            'private.numberOfRequests': {
                $gt: 0
            },
            createdAt: {
                $lte: moment().subtract(Vatfree.travellers.reminders.createdWaitDays, 'days').toDate()
            },
            $or: [
                {
                    'private.lastReminderSent': {
                        $exists: false
                    }
                },
                {
                    'private.lastReminderSent': {
                        $lte: moment().subtract(Vatfree.travellers.reminders.reminderWaitDays, 'days').toDate()
                    }
                }
            ],
            'private.reminders.8': {
                $exists: false
            }
        };
    },
    sendAll: function(callback) {
        let remindersCount = 0;
        let errors = [];
        let travellerReminderTextId = Meteor.settings.defaults.travellerReminderTextId;
        if (!travellerReminderTextId) {
            if (callback) {
                callback('No reminder text defined for travellers');
            } else {
                throw new Meteor.Error(500, 'No reminder text defined for travellers');
            }
        }

        Meteor.users.find(Vatfree.travellers.reminders.querySelector(), {}).forEach((traveller) => {
            try {
                // send reminder email
                Vatfree.travellers.reminders.send(traveller, travellerReminderTextId, (err, result) => {
                    if (err) {
                        errors.push(err);
                    } else {
                        remindersCount++;
                    }
                })
            } catch (e) {
                console.error(e);
                errors.push(e);
            }
        });

        if (callback) {
            callback(errors.length > 0 ? {errors: errors} : null, remindersCount);
        }

        return remindersCount;
    },
    send: function(traveller, travellerReminderTextId, callback) {
        let emailTemplateId = Meteor.settings.defaults.emailTemplateId;
        let emailLanguage = "en"; // traveller.profile.language ||

        let emailTextDoc = EmailTexts.findOne({_id: travellerReminderTextId});
        if (!emailTextDoc) {
            throw new Meteor.Error(404, "Could not find email text " + travellerReminderTextId);
        }
        let emailText = emailTextDoc.text[emailLanguage] || emailTextDoc.text["en"];
        let emailSubject = emailTextDoc.subject[emailLanguage] || emailTextDoc.subject["en"];

        if (!emailSubject || !emailText) {
            throw new Meteor.Error(500, "Email subject and/or text could not be found");
        }

        let travellerData = JSON.parse(JSON.stringify(traveller.profile));
        travellerData.status = traveller.private.status;

        let templateData = {
            language: emailLanguage,
            traveller: travellerData
        };

        import handlebars from 'handlebars';
        Vatfree.notify.registerHandlebarsHelpers(handlebars);

        Vatfree.notify.createMagicLink(emailText, traveller.profile.email, templateData);
        Vatfree.notify.createPasswordResetCode(emailText, traveller.profile.email, templateData);

        let text = handlebars.compile(emailText)(templateData);
        let subject = handlebars.compile(emailSubject)(templateData);

        let mailOptions = {
            to: traveller.profile.email
        };

        let activityIds = {
            travellerId: traveller._id,
        };

        let returnValue = Vatfree.sendEmail(mailOptions, emailTemplateId, subject, text, activityIds);
        if (callback) {
            callback(null, returnValue);
        }

        if (returnValue) {
            Meteor.users.update({
                _id: traveller._id
            }, {
                $set: {
                    'private.lastReminderSent': new Date()
                },
                $addToSet: {
                    'private.reminders': {
                        sentAt: new Date(),
                        type: "email"
                    }
                }
            });
        }

        return returnValue;
    }
};

Meteor.methods({
    'send-traveller-reminders'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let job = new Job(travellerJobs, 'sendTravellerReminders', {});
        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 1
            })
            .save();
    }
});

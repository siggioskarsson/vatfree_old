export const sendMagicLinkLogin = function (email) {
    email = email.toLowerCase();
    let user = Meteor.users.findOne({"emails.address": email});
    if (user) {
        let travellerStatusNotify = {
            emailTextId: Meteor.settings.defaults.emailLoginTextId,
            emailTemplateId: Meteor.settings.defaults.emailTemplateId
        };
        let templateData = {
            traveller: user.profile
        };
        let toAddress = email;
        let fromAddress = "support@vatfree.com";
        let activityIds = {
            travellerId: user._id
        };
        let emailLanguage = user.profile.language || 'en';

        // send magic link email - using the notification class
        const notificationOptions = {
            suppressMobileNotification: true,
            hideInMessageBox: true
        };
        return Vatfree.notify.sendNotification(travellerStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage, false, notificationOptions);
    }

    throw new Meteor.Error(404, 'Email address could not be found');
};

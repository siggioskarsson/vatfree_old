import moment from 'moment';

Meteor.methods({
    'import-travellers'(fileId) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        // Create a job:
        let job = new Job(importJobs, 'imports',
            {
                importFunction: "importTravellers",
                fileId: fileId
            }
        );
        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 2,
                wait: 5 * 60 * 1000
            })
            .save();
    }
});

Vatfree.importTravellers = function (travellerFile, fileObject, callback) {
    Vatfree.setImportFileProgress(5, 'Populating lookup data', fileObject);

    let countries = {};
    Countries.find().forEach(function (country) {
        countries[country.code] = country;
    });

    let totalLines = Vatfree.importFileLines(travellerFile);
    console.log(totalLines);
    Vatfree.setImportFileProgress(7, 'Initializing csv', fileObject);
    let csvData = Vatfree.csv2json(travellerFile);
    Vatfree.setImportFileProgress(11, 'Importing data', fileObject);
    _.each(csvData, (row, rowIndex) => {
        if (row && _.size(row) > 2) {
            if (rowIndex % 100 === 0) {
                console.log('progress', 11 + Math.round(89 * (rowIndex/totalLines)), 'Importing ' + rowIndex + ' of ' + totalLines);
                Vatfree.setImportFileProgress(11 + Math.round(89 * (rowIndex/totalLines)), 'Importing ' + rowIndex + ' of ' + totalLines, fileObject);
            }

            let updateData = Vatfree.import.processColumns(colMapping, convert, row);

            updateData['profile.name'] = updateData['profile.firstName'] + ' ' + updateData['profile.lastName'];
            let country = updateData['profile.country'] || "NL";
            let userCountry = countries[country.substr(0, 2)];
            if (userCountry) {
                updateData['profile.countryId'] = userCountry._id;
                updateData['profile.addressCountryId'] = userCountry._id;
            }

            //console.log('add traveller', rowIndex, updateData['profile.name']);
            saveUser(updateData);
        }
    });

    delete Accounts.doNotRunTravellerUpdateTriggers;
    callback(null, "done");
};

var header = `
Contact ID	Salutation	First Name	Last Name	Email	Asperion_leveranciernr	Birthdate	language	Phone	Home Phone	Mobile	Innercircle	Fan	Comments	Passportnumber	Passport_expiration_date	B_holder	B_number	B_IBAN	B_BIC	B_CHC	B_name	B_city	B_country	PP_email	CC_type	CC_experation_monthyear	Mailing Street	Mailing Zip/Postal Code	Mailing City	Mailing State/Province	Mailing Country	Total no. of REQs	Date last REQ approved	Total Paid	Total VAT of REQs	Total Refund of REQs	Total ServiceFee of REQs	Total OB1	Total OB2
"Contact ID","Created Date","Salutation","First Name","Last Name","Passportnumber","Passport_expiration_date","Mailing Street","Mailing City","Mailing State/Province","Mailing Zip/Postal Code","Mailing Country","Phone","Mobile","Email","Birthdate","Client segment","Fan","Innercircle","Comments","language","EUpassport","Total no. of REQs","Total Refund of REQs","Total ServiceFee of REQs","Date last REQ approved"
"00320000006ysAC","21/04/2006","Mr.","Vincent","Chapuis","C2296522","05/02/2016","Mourattes 23 a","Courrendlin","","2830","CH Switzerland","+31 88 828 3733","","chapuisv@gmail.com","12/05/1967","Tourist flirt","0","0","","","no","2","20.89","0.00","26/02/2007"
"0032000000560xU","13/08/2006","Mr.","","Jan Willem Smeenk","","","Yacht Club Road 882","Dar Es Salaam","","1000","TZ Tanzania","+31 88 828 3733","","janwillem@smeenkies.com","","Innercircle","0","1","","EN","no","23","3620.27","746.52","21/04/2010"
`;

var colMapping = {
    "Contact ID": "profile.contactId",
    "Created Date": "createdAt",
    "Salutation": "profile.salutation",
    "First Name": "profile.firstName",
    "Last Name": "profile.lastName",
    "Asperion_leveranciernr": "profile.asperionNr",
    "Passportnumber": "profile.passportNumber",
    "Passport_expiration_date": "profile.passportExpirationDate",
    "Mailing Street": "profile.addressFirst",
    "Mailing City": "profile.city",
    "Mailing State/Province": "profile.state",
    "Mailing Zip/Postal Code": "profile.postalCode",
    "Mailing Country": "profile.country",
    "Phone": "profile.phone",
    "Home Phone": "profile.phone2",
    "Mobile": "profile.mobile",
    "Email": "profile.email",
    "Birthdate": "profile.birthDate",
    "Client segment": "profile.clientSegment",
    "Fan": "profile.fan",
    "Innercircle": "profile.innerCircle",
    "Comments": "profile.comments",
    "language": "profile.language",
    "EUpassport": "profile.euPassport",
    "Total no. of REQs": "private.numberOfRequests",
    "Total Refund of REQs": "private.totalRefunds",
    "Total ServiceFee of REQs": "private.totalServiceFee",
    "Date last REQ approved": "private.lastRequestDate"
};

var convert = {
    "createdAt": "date",
    "profile.birthDate": "date",
    "profile.passportExpirationDate": "date",
    "private.lastRequestDate": "date",
    "private.numberOfRequests": "number",
    "private.totalRefunds": "currency",
    "private.totalServiceFee": "currency"
};

var saveUser = function (updateData) {
    // first check on salesforce id === salesforce import
    let user = Meteor.users.findOne({
        'profile.contactId': updateData['profile.contactId']
    });
    if (!user) {
        // check on email if not found by sales force if for instance added via Phoenix separately
        user = Meteor.users.findOne({
            'profile.email': updateData['profile.email']
        });
    }

    let expDate = moment(updateData['profile.passportExpirationDate']);
    updateData['private.status'] = 'userVerified';
    /*
    if (updateData['profile.passportNumber'] && expDate.isValid() && expDate.isAfter(moment())) {
        updateData['private.status'] = 'userVerified';
    } else {
        updateData['private.status'] = 'userUnverified';
    }
    */

    // set all languages to English
    updateData['profile.language'] = "en";
    if (user) {
        if (updateData.profile && updateData.profile.email && !user.emails) {
            updateData.emails = [
                {
                    address: updateData.profile.email,
                    verified: false
                }
            ];
        }

        Accounts.doNotRunTravellerUpdateTriggers = true;

        // only update salesforce id if user already exists
        let sfUpdateData = {
            'profile.contactId': updateData['profile.contactId']
        };
        Meteor.users.update({
            _id: user._id
        }, {
            $addToSet: sfUpdateData
        },{
            notify: false
        });
    } else {
        updateData['createdAt'] = new Date();
        updateData['roles'] = {
            __global_roles__: [
                "traveller"
            ]
        };
        updateData['private.travellerTypeId'] = Meteor.settings.defaults.TravellerTypeId;

        if (updateData.profile && updateData.profile.email) {
            updateData.emails = [
                {
                    address: updateData.profile.email,
                    verified: false
                }
            ];
        }

        // transform profile and private to proper objects :-(
        let insertData = {
            profile: {},
            private: {}
        };
        _.each(updateData, (value, key) => {
            if (key.indexOf('profile.') === 0) {
                let subKey = key.replace('profile.', '');
                insertData.profile[subKey] = value;
            } else if (key.indexOf('private.') === 0) {
                let subKey = key.replace('private.', '');
                insertData.private[subKey] = value;
            } else {
                insertData[key] = value;
            }
        });

        // contactId needs to be an array, there are a lot of duplicate user accounts in salesforce
        insertData.profile['contactId'] = [ insertData.profile['contactId'] ];

        Meteor.users.insert(insertData);
    }
};

import moment from "moment";

if (Meteor.settings && Meteor.settings.cron) {
    if (Meteor.settings.cron.travellerTrash) {
        SyncedCron.add({
            name: 'Remove all old stuff from traveller trash',
            schedule: function(parser) {
                // 5:15 every day
                return parser.cron('15 5 ? * *');
            },
            job: function() {
                Files.update({
                    target: 'travellers_trash',
                    updatedAt: {
                        $lt: moment().subtract(Meteor.settings.cron.travellerTrash, 'days').toDate()
                    }
                },{
                    $set: {
                        deleted: true
                    }
                }, (error, numberOfRemovedDocuments) => {
                    if (error) {
                        ActivityLogs.insert({
                            type: 'task',
                            subject: 'ERROR: emptying traveller trash',
                            status: 'new',
                            error: error
                        });
                    } else {
                        ActivityLogs.insert({
                            type: 'burny',
                            subject: 'Travellers trash emptied',
                            status: 'done',
                            notes: "Removed " + (numberOfRemovedDocuments ? numberOfRemovedDocuments : 0) + " docs"
                        });
                    }
                });
            }
        });
    }

    if (Meteor.settings.cron.travellerReminders) {
        SyncedCron.add({
            name: 'Send all traveller reminders',
            schedule: function(parser) {
                // 6:15 every day
                return parser.cron('15 6 ? * *');
            },
            job: function() {
                Vatfree.travellers.reminders.sendAll((errors, numberSent) => {
                    if (errors) {
                        ActivityLogs.insert({
                            type: 'task',
                            subject: 'ERROR: sending traveller reminders',
                            status: 'new',
                            error: errors
                        });
                    } else {
                        ActivityLogs.insert({
                            type: 'burny',
                            subject: 'Traveller reminders sent',
                            status: 'done',
                            notes: "Sent " + (numberSent ? numberSent : 0) + " reminders"
                        });
                    }
                });
            }
        });
    }
}

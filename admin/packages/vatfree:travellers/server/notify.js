Vatfree.notify.sendStatusNotification['travellers'] = function (userId, travellerId, travellerStatusNotify) {
    try {
        let traveller = Travellers.findOne({_id: travellerId}) || {};
        let emailLanguage = traveller.profile.language || "en";

        let country = Countries.findOne({_id: traveller.profile.countryId }) || {};
        traveller.profile.country = country;

        let activityIds = {
            travellerId: travellerId
        };

        let travellerInfo = traveller.profile;
        travellerInfo.rejectionIds = traveller.private.rejectionIds;

        if (traveller.createdBy) {
            travellerInfo.createdById = traveller.createdBy;
            const createdByUser = Meteor.users.findOne({_id: traveller.createdBy});
            if (createdByUser) {
                travellerInfo.createdBy = createdByUser.profile;
            }
        }

        let affiliate = false;
        if (traveller.private.affiliateId) {
            const affiliateObj = Meteor.users.findOne({_id: traveller.private.affiliateId});
            if (affiliateObj) {
                affiliate = {...affiliateObj.profile,...affiliateObj.affiliate};
            }
        }

        let templateData = {
            language: emailLanguage,
            traveller: travellerInfo,
            affiliate: affiliate,
        };

        let toAddress = traveller.profile.email;
        let fromAddress = 'Vatfree.com support <support@vatfree.com>';

        Vatfree.notify.sendNotification(travellerStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage, traveller);
    } catch (e) {
        ActivityLogs.insert({
            type: 'task',
            userId: userId,
            subject: 'Sending of status update to traveller failed',
            status: 'new',
            travellerId: travellerId,
            notes: "Error: " + e.message
        });
    }
};

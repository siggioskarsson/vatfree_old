import moment from 'moment';
import { getAllTravellerData } from './lib/all-data';
import { Meteor } from "meteor/meteor";
import { sendMagicLinkLogin } from 'meteor/vatfree:travellers/server/lib/magic-link';

Meteor.methods({
    getNextTravellerTask(searchTerm, selector) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (!selector) selector = {};
        selector['private.status'] = 'userUnverified';
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let travellerId = false;
        Meteor.users.find(selector, {
            sort: {
                taskedAt: 1
            },
            limit: 20
        }).forEach((traveller) => {
            // check for lock
            if (!travellerId && !Locks.findOne({
                    lockId: 'travellers_' + traveller._id,
                    userId: {
                        $ne: this.userId
                    }
                })) {
                travellerId = traveller._id;
            }
        });

        return travellerId;
    },
    'search-travellers'(searchTerm, limit, offset, showEmail) {
        this.unblock();
        if (!Vatfree.userIsInRole(this.userId, 'travellers-read,receipts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {
            'roles.__global_roles__' : 'traveller'
        };
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let travellers = [];
        Meteor.users.find(selector, {
            sort: {
                'profile.name': 1
            },
            limit: limit,
            offset: offset
        }).forEach((traveller) => {
            let text = traveller.profile.name;
            if (!text) {
                text = traveller.profile.email;
            } else if (showEmail) {
                text += ' (' + traveller.profile.email + ')';
            }
            if (traveller.profile.addressFirst) {
                text += ' - ' + (traveller.profile.addressFirst || "") + ', ' + (traveller.profile.postalCode || "") + ' ' + (traveller.profile.city || "");
            }
            travellers.push({
                text: text,
                id: traveller._id
            });
        });

        return travellers;
    },
    'update-traveller-profile'(travellerId, profileData, privateData) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let updateData = {};
        _.each(profileData, (profileValue, profileKey) => {
            updateData['profile.' + profileKey] = profileValue;
        });
        _.each(privateData, (privateValue, privateKey) => {
            updateData['private.' + privateKey] = privateValue;
        });

        Meteor.users.update({
            _id: travellerId
        }, {
            $set: updateData
        });
    },

    'fix-traveller-type'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (Meteor.settings.defaults.TravellerTypeId) {
            Meteor.users.update({
                'roles.__global_roles__' : ['traveller'],
                'private.travellerTypeId': {
                    $exists: false
                }
            },{
                $set: {
                    'private.travellerTypeId': Meteor.settings.defaults.TravellerTypeId
                }
            },{
                multi: true
            });
        }
    },

    'import-travellers-locally'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let fileName = '/Users/oskarsson/Downloads/travellers.csv';
        Vatfree.importTravellers(fileName);
    },
    'add-traveller'(travellerInfo) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-create')) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (travellerInfo.profile && travellerInfo.profile.email) {
            const user = Meteor.users.findOne({
                $or: [
                    {
                        'profile.email': travellerInfo.profile.email
                    },
                    {
                        'emails.address': travellerInfo.profile.email
                    }
                ]
            }, {
                fields: {
                    profile: 1
                }
            });
            if (user) {
                throw new Meteor.Error(404, 'User already exists: ' + user.profile.email);
            }
        }

        //
        // We cannot use Account.createUser as this is not allowed on this site (only @vatfree.com users allowed)
        //

        travellerInfo.private.status = 'waitingForDocuments';

        let newUser = {
            createdAt: new Date(),
            createdBy: this.userId,
            roles: {
                '__global_roles__': ['traveller']
            },
            profile: travellerInfo.profile || {},
            private: travellerInfo.private || {}
        };

        if (!newUser.private['travellerTypeId'] && Meteor.settings.defaults.TravellerTypeId) {
            newUser.private['travellerTypeId'] = Meteor.settings.defaults.TravellerTypeId;
        }

        if (travellerInfo.profile && travellerInfo.profile.email) {
            travellerInfo.profile.email = travellerInfo.profile.email.toLowerCase();
            newUser.emails = [
                {
                    address: travellerInfo.profile.email,
                    verified: false
                }
            ];
        }

        return Meteor.users.insert(newUser);
    },
    'fix-phone-numbers'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.users.find({}).forEach((user) => {
            if (user.profile && (user.profile.phone || user.profile.mobile)) {
                let phone = (user.profile.phone || "").replace(/[^0-9]/g, '');
                let mobile = (user.profile.mobile || "").replace(/[^0-9]/g, '');
                Meteor.users.update({
                    _id: user._id
                },{
                    $set: {
                        'profile.phone': phone,
                        'profile.mobile': mobile
                    }
                });
            }
        });

    },
    'traveller-reject'(travellerId, statusReason, rejectionIds) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-update,traveller-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let traveller = Meteor.users.findOne({
            _id: travellerId
        });
        if (traveller) {
            let setData = {
                'private.status': (statusReason === 'notEligibleForRefund' ? 'notEligibleForRefund' : 'documentationIncomplete'),
                'private.checkedAt': new Date(),
                'private.checkedBy': this.userId,
                'private.rejectionIds': rejectionIds
            };

            Meteor.users.update({
                _id: travellerId
            },{
                $set: setData
            });

            return true;
        } else {
            throw new Meteor.Error('Could not find traveller to reject');
        }
    },
    'change-traveller-email'(travellerId, email) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let user = Meteor.users.findOne({
            'emails.address': email.toLowerCase()
        });
        if (user && user._id === travellerId) {
            // email address already registered to user
            Meteor.users.update({
                _id: travellerId,
                "roles.__global_roles__": "traveller"
            }, {
                $set: {
                    'profile.email': email.toLowerCase()
                }
            });
        } else if (user) {
            // other user
            throw new Meteor.Error(500, 'This email address is already registered to another user');
        } else {
            Meteor.users.update({
                _id: travellerId,
                "roles.__global_roles__": "traveller"
            },{
                $set: {
                    'profile.email': email.toLowerCase(),
                    emails: [
                        {
                            "address": email.toLowerCase(),
                            "verified": false
                        }
                    ]
                },
                $unset: {
                    services: 1
                }
            });
        }

    },
    'send-traveller-magic-link-email'(travellerId) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const traveller = Meteor.users.findOne({_id: travellerId});
        if (traveller && traveller.profile && traveller.profile.email) {
            return sendMagicLinkLogin(traveller.profile.email);
        }

        return false;
    },
    'traveller-requeue'(travellerId) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.users.update({
            _id: travellerId
        },{
            $set: {
                taskedAt: new Date()
            }
        });
    },
    'traveller-approve'(travellerId, profileData) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let traveller = Meteor.users.findOne({
            _id: travellerId
        });
        if (traveller) {
            let setData = {
                'private.status': 'userVerified',
                'private.checkedAt': new Date(),
                'private.checkedBy': this.userId
            };

            if (profileData) {
                _.each(profileData, (profileValue, profileKey) => {
                    setData['profile.' + profileKey] = profileValue;
                });
            }

            Meteor.users.update({
                _id: travellerId
            },{
                $set: setData,
                $unset: {
                    'private.rejectionIds': 1
                }
            });

            return true;
        } else {
            throw new Meteor.Error('Could not find receipt to approve');
        }
    },
    'export-travellers'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let users = '"Name","Email"' + "\n";
        Meteor.users.find({
            "roles.__global_roles__": "traveller"
        },{
            fields: {
                "profile.email": 1,
                "profile.name": 1
            }
        }).forEach((user) => {
            users += '"' + (user.profile.name || "") + '","' + user.profile.email + '"' + "\n";
        });

        return users;

        //console.log(users);
        var fs = require('fs');
        fs.writeFile("/tmp/users.csv", users, function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    },
    'fix-user-emails'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.users.find({
            emails: {
                $exists: false
            },
            'profile.email': {
                $exists: true
            }
        }).forEach(function(user) {
            Meteor.users.direct.update({
                _id: user._id
            },{
                $set: {
                    emails: [
                        {
                            address: user.profile.email,
                            verified: false
                        }
                    ]
                }
            });
        });
    },
    'traveller-get-linked-elements'(travellerId) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receipts = Receipts.find({userId: travellerId}, {fields: {_id: 1}}).count();
        let files = Files.find({itemId: travellerId}, {fields: {_id: 1}}).count();
        let activityLog = ActivityLogs.find({travellerId: travellerId}, {fields: {_id: 1}}).count();
        let payouts = Payouts.find({travellerId: travellerId}, {fields: {_id: 1}}).count();

        return {
            receipts: receipts,
            files: files,
            activityLog: activityLog,
            payouts: payouts
        };
    },
    'merge-traveller'(travellerId, mergeWithTravellerId) {
        check(travellerId, String);
        check(mergeWithTravellerId, String);
        if (!Vatfree.userIsInRole(this.userId, 'travellers-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let oldTraveller = Meteor.users.findOne({_id: travellerId});
        if (!oldTraveller) {
            throw new Meteor.Error(404, 'Could not find traveller');
        }
        let newTraveller = Meteor.users.findOne({_id: mergeWithTravellerId});
        if (!newTraveller) {
            throw new Meteor.Error(404, 'Could not find traveller to merge into');
        }

        Receipts.find({
            userId: travellerId
        }).forEach((receipt) => {
            Receipts.direct.update({
                _id: receipt._id
            },{
                $set: {
                    userId: mergeWithTravellerId
                }
            });
        });

        ActivityLogs.find({
            travellerId: travellerId
        }).forEach((activityLog) => {
            ActivityLogs.update({
                _id: activityLog._id
            },{
                $set: {
                    travellerId: mergeWithTravellerId
                }
            });
        });

        Payouts.find({
            travellerId: travellerId
        }).forEach((payout) => {
            Payouts.direct.update({
                _id: payout._id
            },{
                $set: {
                    travellerId: mergeWithTravellerId
                }
            });
        });

        Files.find({
            target: {
                $ne: false
            },
            itemId: travellerId
        }).forEach((file) => {
            let fileTarget = (file.target === 'travellers' ? 'travellers_internal' : file.target);
            Files.update({
                _id: file._id
            },{
                $set: {
                    itemId: mergeWithTravellerId,
                    target: fileTarget
                },
                $unset: {
                    subType: 1
                }
            });
        });

        Meteor.users.remove({
            _id: travellerId
        });

        if (oldTraveller.emails && _.isArray(oldTraveller.emails) && oldTraveller.emails.length > 0) {
            Meteor.users.update({
                _id: mergeWithTravellerId
            },{
                $addToSet: {
                    emails: {
                        $each: oldTraveller.emails
                    }
                }
            });
        }

        return true;
    },
    'create-magic-link'(travellerId) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let emailText = '{{createMagicLink}}';
        let templateData = {};
        let traveller = Meteor.users.findOne({_id: travellerId});
        Vatfree.notify.createMagicLink(emailText, traveller.profile.email, templateData);

        return templateData.magicLink;
    },
    'mark-message-read'(messageId) {
        check(messageId, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        ActivityLogs.update({
            _id: messageId,
            travellerId: this.userId
        },{
            $set: {
                readOn: new Date()
            }
        });
    },
    'export-all-traveller-data'(travellerId) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return getAllTravellerData(travellerId);
    },
    'get-travellers-stats'(selector) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        selector['roles.__global_roles__'] =  "traveller";
        let count = Meteor.users.find(selector).count();
        return [
            {
                _id: "travellers",
                value: {
                    count: count
                }
            }
        ];
    },
    'traveller-email-export'(searchTerm, status) {
        if (!Vatfree.userIsInRole(this.userId, 'admin')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const Baby = require('babyparse');
        let selector = {
            "roles.__global_roles__": "traveller",
            "profile.email": {
                $exists: true
            }
        };
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        if (status && status.length > 0) {
            selector['private.status'] = {
                $in: status
            };
        }

        let data = [];
        Meteor.users.find(selector, {
            sort: {
                createdAt: 1
            },
            fields: {
                'profile.name': true,
                'profile.email': true
            }
        }).forEach((traveller) => {
            let csvTraveller = {
                name: traveller.profile.name,
                email: traveller.profile.email
            };
            data.push(csvTraveller);
        });

        let csv = Baby.unparse(data);

        return csv;
    },
    'travellers-forward-waiting-for-docs-flow'(travellerIds) {
        check(travellerIds, Array);
        if (!Vatfree.userIsInRole(this.userId, 'travellers-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        Travellers.find({
            _id: {
                $in: travellerIds
            },
            'private.status': {
                $in: ['waitingForDocuments']
            }
        }).forEach((traveller) => {
            Travellers.update({
                _id: traveller._id
            }, {
                $set: {
                    'private.status': 'userUnverified'
                }
            });
        });
    }
});

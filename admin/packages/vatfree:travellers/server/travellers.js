// Travellers is an alias for Meteor.users, but only on the client
Travellers = Meteor.users;

ActivityStream.attachHooks(Meteor.users);
// Add index to activity stream collection for travellers
ActivityStream.collection._ensureIndex({'securityContext.travellerId': 1});

Meteor.users.after.insert(function (userId, doc) {
    Meteor.defer(() => {
        if (_.contains(doc.roles.__global_roles__, 'traveller')) {
            // do not send this link if the user has self signed up (on traveller.vatfree.com)
            let vattie = doc.profile.email ? doc.profile.email.indexOf('@vatfree.com') > 0 : false;
            let traveller = _.contains(doc.roles['__global_roles__'], 'traveller');
            // if we have a profile.contactId we are dealing with an import from salesforce, then ignore
            if (traveller && !vattie && !doc.profile.contactId) {
                Vatfree.notify.processNotificationTriggers(userId, 'travellers', '- insert -', false, doc._id);
                if (doc.profile && doc.profile.email) {
                    Vatfree.adminNotify('New user: ' + doc.profile.email);
                }
            }
        }
    });
});

Meteor.users.after.update(function (userId, doc, fieldNames, modifier, options) {
    if (_.contains(doc.roles.__global_roles__, 'traveller')) {
        // check for eligibility change for user and update all the receipts accordingly
        if (_.contains(fieldNames, 'private') && modifier['$set'] && modifier.$set['private.status'] === 'userVerified' && modifier.$set['private.status'] !== this.previous.private.status) {
            Receipts.find({
                userId: doc._id,
                status: {
                    $in: [
                        'userPending',
                        'userPendingForPayment'
                    ]
                }
            }).forEach((receipt) => {
                // defaults
                let newStatus = 'waitingForOriginal';
                if (receipt.originalsCheckedBy) {
                    newStatus = 'visualValidationPending';
                }

                // if we are in step 2 verification of user
                if (receipt.status === 'userPendingForPayment') {
                    newStatus = 'paidByShop';
                }

                Receipts.update({
                    _id: receipt._id
                }, {
                    $set: {
                        status: newStatus,
                        travellerCountryId: doc.profile.countryId
                    }
                });
            });
        }

        Meteor.defer(() => {
            if (!Accounts.doNotRunTravellerUpdateTriggers && options.notify !== false) {
                if (_.contains(fieldNames, 'private') && modifier.$set && modifier.$set['private.status'] && modifier.$set['private.status'] !== this.previous.private.status) {
                    Vatfree.notify.processNotificationTriggers(userId, 'travellers', modifier.$set['private.status'], this.previous.private.status, doc._id);
                }
            }
        });
    }
});

FlowRouter.route('/travellers', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "travellers"});
    }
});

FlowRouter.route('/traveller/:travellerId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "travellerEdit"});
    }
});

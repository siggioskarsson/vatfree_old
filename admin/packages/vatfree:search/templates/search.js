import './search.html';
import 'meteor/vatfree:invoices/templates/list';
import 'meteor/vatfree:affiliates/templates/list';

Template.search.onCreated(function () {
    this.searching = new ReactiveVar();

    this.travellers = new ReactiveVar();
    this.receipts = new ReactiveVar();
    this.invoices = new ReactiveVar();
    this.payouts = new ReactiveVar();
    this.shops = new ReactiveVar();
    this.companies = new ReactiveVar();
    this.contacts = new ReactiveVar();
    this.affiliates = new ReactiveVar();

    this.userIds = new ReactiveVar();
    this.shopIds = new ReactiveVar();
    this.companyIds = new ReactiveVar();
});

Template.search.onRendered(function () {
    this.autorun(() => {
        let searchTerm = FlowRouter.getQueryParam('term');
        if (!searchTerm) return;

        this.searching.set(true);
        Meteor.call('search', searchTerm, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                console.log(result);
                this.travellers.set(result.travellers);
                this.receipts.set(result.receipts);
                this.invoices.set(result.invoices);
                this.payouts.set(result.payouts);
                this.shops.set(result.shops);
                this.companies.set(result.companies);
                this.contacts.set(result.contacts);
                this.affiliates.set(result.affiliates);

                this.userIds.set(result.userIds);
                this.shopIds.set(result.shopIds);
                this.companyIds.set(result.companyIds);
            }

            this.searching.set();
        });
    });

    this.autorun(() => {
        // we add the deleted $ne to overwrite the default "delete" filter in the publish functions
        let userSelector = {
            _id: {
                $in: this.userIds.get()
            },
            deleted: {
                $ne: 'fake'
            }
        };
        this.subscribe('travellers', '', userSelector);

        let shopSelector = {
            _id: {
                $in: this.shopIds.get()
            },
            deleted: {
                $ne: 'fake'
            }
        };
        this.subscribe('shops', '', shopSelector);

        let companySelector = {
            _id: {
                $in: this.companyIds.get()
            },
            deleted: {
                $ne: 'fake'
            }
        };
        this.subscribe('companies', '', companySelector);
    });
});

Template.search.helpers({
    getSearchTerm() {
        return FlowRouter.getQueryParam('term');
    },
    isSearching() {
        return Template.instance().searching.get();
    },
    getTravellers() {
        let template = Template.instance();
        return template.travellers.get();
    },
    getReceipts() {
        let template = Template.instance();
        return template.receipts.get();
    },
    getInvoices() {
        let template = Template.instance();
        return template.invoices.get();
    },
    getPayouts() {
        let template = Template.instance();
        return template.payouts.get();
    },
    getShops() {
        let template = Template.instance();
        return template.shops.get();
    },
    getCompanies() {
        let template = Template.instance();
        return template.companies.get();
    },
    getContacts() {
        let template = Template.instance();
        return template.contacts.get();
    },
    getAffiliates() {
        let template = Template.instance();
        return template.affiliates.get();
    },
});

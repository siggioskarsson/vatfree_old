Meteor.methods({
    search(searchTerm) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        const DEBUGGING = !!(Meteor.settings.debug && Meteor.settings.debug.globalSearch);
        if (DEBUGGING) console.time('global search');

        let selector = {};
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let entities = {};
        if (Vatfree.userIsInRole(Meteor.userId(), "travellers-read")) entities.travellers = Meteor.users;
        if (Vatfree.userIsInRole(Meteor.userId(), "receipts-read")) entities.receipts = Receipts;
        if (Vatfree.userIsInRole(Meteor.userId(), "invoices-read")) entities.invoices = Invoices;
        if (Vatfree.userIsInRole(Meteor.userId(), "payouts-read")) entities.payouts = Payouts;
        if (Vatfree.userIsInRole(Meteor.userId(), "shops-read")) entities.shops = Shops;
        if (Vatfree.userIsInRole(Meteor.userId(), "companies-read")) entities.companies = Companies;
        if (Vatfree.userIsInRole(Meteor.userId(), "contacts-read")) entities.contacts = Contacts;
        if (Vatfree.userIsInRole(Meteor.userId(), "affiliates-read")) entities.affiliates = Affiliates;

        let results = {};
        results['userIds'] = [];
        results['shopIds'] = [];
        results['companyIds'] = [];
        _.each(entities, (collection, entityId) => {
            if (DEBUGGING) console.time(`search ${entityId}`);
            results[entityId] = [];

            let collectionSelector = _.clone(selector);
            if (entityId === 'travellers') {
                collectionSelector['roles.__global_roles__'] = 'traveller';
            } else if (entityId === 'contacts') {
                collectionSelector['roles.__global_roles__'] = 'contact';
            } else if (entityId === 'affiliates') {
                collectionSelector['roles.__global_roles__'] = 'affiliate';
            }

            collection.find(collectionSelector, {
                sort: {
                    createdAt: -1
                },
                limit: 25
            }).forEach((entity) => {
                if (entity.userId) {
                    results['userIds'].push(entity.userId);
                }
                if (entity.shopId) {
                    results['shopIds'].push(entity.shopId);
                }
                if (entity.companyId) {
                    results['companyIds'].push(entity.companyId);
                }
                results[entityId].push(entity);
            });
            if (DEBUGGING) console.timeEnd(`search ${entityId}`);
        });

        if (DEBUGGING) console.timeEnd('global search');
        return results;
    }
});

Package.describe({
    name: 'vatfree:search',
    summary: 'Vatfree search package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'dburles:google-maps@1.1.5',
        'flemay:less-autoprefixer@1.2.0',
        'kadira:flow-router@2.12.1',
        'vatfree:core',
        'vatfree:travellers',
        'vatfree:receipts',
        'vatfree:shops',
        'vatfree:invoices',
        'vatfree:payouts',
        'vatfree:companies',
        'vatfree:contacts',
        'vatfree:affiliates'
    ]);

    // server files
    api.addFiles([
        'server/methods.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'templates/search.html',
        'templates/search.js'
    ], 'client');

    api.export([
    ]);
});

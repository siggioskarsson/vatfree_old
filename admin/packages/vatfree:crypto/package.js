Package.describe({
    name: 'vatfree:crypto',
    summary: 'Vatfree crypto package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'ecmascript',
        'check',
        'underscore',
        'lamhieu:unblock@1.0.0'
    ]);

    // shared files
    api.addFiles([
    ]);

    // server files
    api.addFiles([
        'crypto.js'
    ], 'server');

    // client files
    api.addFiles([
    ], 'client');

    api.export([
    ]);
});

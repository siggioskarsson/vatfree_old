import moment from 'moment';
import sha from 'meteor/sha';
import bitcoin from 'bitcoinjs-lib';
import bitcoinMessage from 'bitcoinjs-message';

export const VatfreeCrypto = {
    messagePrefix: '\u000cvatfree.com:',
    calculateReceiptHashString(receipt) {
        let shaString = receipt.receiptNr + '_' +
            moment(receipt.purchaseDate).format('YYYYMMDD') + '_' +
            moment(receipt.customsDate).format('YYYYMMDD') + '_' +
            receipt.currencyId + '_' +
            (receipt.amount || 0).toString() + '_' +
            (receipt.refund || 0).toString() + '_' +
            (receipt.totalVat || 0).toString() + '_' +
            (receipt.serviceFee || 0).toString() + '_' +
            receipt.shopId + '_' +
            receipt.userId;

        return shaString;
    },

    calculateReceiptHash(receipt) {
        let shaString = this.calculateReceiptHashString(receipt);
        let sha256 = sha.SHA256(shaString);

        return sha256;
    },

    verifyReceiptHash(receipt, hash) {
        let receiptHash = this.calculateReceiptHash(receipt);
        return receiptHash === hash;
    },

    getAddress(privateKey) {
        const keyPair = bitcoin.ECPair.fromPrivateKey(Buffer.from(privateKey, 'hex'));
        const { address } = bitcoin.payments.p2pkh({ pubkey: keyPair.publicKey });

        return address;
    },

    signReceipt(receipt, privateKey) {
        let receiptHash = this.calculateReceiptHash(receipt);

        return this.signReceiptHash(receiptHash, privateKey);
    },

    signReceiptHash(receiptHash, privateKey) {
        const keyPair = bitcoin.ECPair.fromPrivateKey(Buffer.from(privateKey, 'hex'));

        let privateKeyBuffer = keyPair.__d;
        let signature = bitcoinMessage.sign(receiptHash, privateKeyBuffer, keyPair.compressed, this.messagePrefix);

        return signature.toString('base64');
    },

    verifyReceiptSignature(receipt, signature, address) {
        let receiptHash = this.calculateReceiptHash(receipt);
        return this.verifyReceiptHashSignature(receiptHash, signature, address);
    },

    verifyReceiptHashSignature(receiptHash, signature, address) {
        try {
            return bitcoinMessage.verify(receiptHash, address, signature, this.messagePrefix);
        } catch(e) {
            console.log(e);
        }

        return false;
    }
};

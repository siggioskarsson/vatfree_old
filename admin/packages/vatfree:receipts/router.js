FlowRouter.route('/receipts', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "receipts"});
    }
});

FlowRouter.route('/receipt/:receiptId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "receiptEdit"});
    }
});

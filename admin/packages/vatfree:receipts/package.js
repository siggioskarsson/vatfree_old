Package.describe({
    name: 'vatfree:receipts',
    summary: 'Vatfree receipts package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'konecty:mongo-counter@0.0.5_3',
        'reywood:publish-composite@1.5.1',
        'simonsimcity:job-collection',
        'flemay:less-autoprefixer@1.2.0',
        'manuel:reactivearray@1.0.5',
        'reactive-dict@1.1.8',
        'simple:reactive-method',
        'hermanitos:activity-stream@0.0.1',
        'percolate:synced-cron@1.3.2',
        'vatfree:core',
        'vatfree:crypto',
        'vatfree:shops',
        'vatfree:countries',
        'vatfree:receipt-rejections',
        'vatfree:receipt-types',
        'vatfree:notify'
    ]);

    // shared files
    api.addFiles([
        'receipts.js',
        'jobs.js',
        'lib/receipts.js'
    ]);

    // server files
    api.addFiles([
        'server/lib/check-validity.js',
        'server/lib/import-downloads.js',
        'server/lib/popr.js',
        'server/lib/reminders.js',
        'server/check-salesforce-receipts.js',
        'server/methods.js',
        'server/cron.js',
        'server/import.js',
        'server/jobs.js',
        'server/notify.js',
        'server/popr.js',
        'server/receipts.js',
        'publish/affiliate-receipts.js',
        'publish/files.js',
        'publish/all-files.js',
        'publish/company-receipts.js',
        'publish/receipts.js',
        'publish/receipts-flow.js',
        'publish/receipt.js',
        'publish/receipt-stats.js',
        'publish/invoice-receipts.js',
        'publish/shop-receipts.js',
        'publish/user-receipts.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/original-modal.html',
        'templates/original-modal.js',
        'templates/receipt-rejections.html',
        'templates/receipt-rejections.js',
        'templates/receipts.html',
        'templates/receipts.js',
        'templates/reject-receipt.html',
        'templates/reject-receipt.js',
        'templates/tasks.html',
        'templates/tasks.js',
        'templates/shop-tasks.html',
        'templates/shop-tasks.js',
        'templates/similar-receipts.html',
        'templates/similar-receipts.js',
        'templates/tasks_form.html',
        'templates/tasks_form.js',
        'templates/update.html',
        'templates/update.js',
        'css/archive.less',
        'css/receipts.less',
        'css/tasks.less',
        'templates/modals/admin-receipts.html',
        'templates/modals/admin-receipts.js',
        'templates/modals/partner-receipts.html',
        'templates/modals/partner-receipts.js'
    ], 'client');

    api.export([
        'Receipts'
    ]);
});

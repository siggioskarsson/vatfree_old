/*
[
            'userPending',
            'waitingForOriginal',
            'visualValidationPending',
            'waitingForDoubleCheck',
            'shopPending',
            'readyForInvoicing',
            'waitingForPayment',
            'userPendingForPayment',
            'paidByShop',
            'requestPayout',
            'paid',
            'rejected',
            'notCollectable',
            'depreciated',
            'waitingForAdministrativeCheck'
        ]
 */
Meteor.methods({
    'check-salesforce-requests'() {
        if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let requests = {};
        Receipts.find({
            salesforceId: {
                $exists: true
            }
        }).forEach((receipt) => {
            if (!requests[receipt.requestId]) {
                requests[receipt.requestId] = [];
            }
            requests[receipt.requestId].push(receipt);
        });

        let serviceFeeDiff = 0;
        _.each(requests, (receipts, requestId) => {
            if (receipts.length <= 1) return; // we don't need to check requests with only 1 receipt

            let validServiceFee = 0;
            let fullServiceFee = 0;
            let paidServiceFee = 0;
            _.each(receipts, (receipt) => {
                fullServiceFee += receipt.serviceFee;

                if (!_.contains(['rejected', 'deleted', 'depreciated', 'notCollectable'], receipt.status)) {
                    validServiceFee += receipt.serviceFee;
                }

                if (receipt.status === "paid") {
                    paidServiceFee += receipt.serviceFee;
                }
            });

            if (validServiceFee === 0) return; // nothing to collect
            if (validServiceFee === paidServiceFee) return; // nothing to pay anymore

            if (fullServiceFee > validServiceFee) {
                if (fullServiceFee - validServiceFee > validServiceFee) {
                    console.error('service fee is less than the difference', fullServiceFee - validServiceFee, validServiceFee, requestId);
                    return;
                }

                console.log('service fee diff', fullServiceFee, validServiceFee, ' -> ', fullServiceFee - validServiceFee, requestId);
                serviceFeeDiff += fullServiceFee - validServiceFee;
            }
        });
        console.log('full service fee difference', serviceFeeDiff);
        //console.log(_.keys(requests));

        return serviceFeeDiff;
    }
});

/* global Vatfree: true */

Meteor.methods({
    'get-popr-info'(poprId, poprHash) {
        check(poprId, String);
        check(poprHash, String);

        let poprInfo = Vatfree.receipts.getPoprInfo(poprId, poprHash);
        if (poprInfo) {
            if (poprInfo.claims) {
                let claim = _.find(poprInfo.claims, (receiptClaim) => {
                    return receiptClaim.claimType === "vat-refund";
                });
                if (claim) {
                    throw new Meteor.Error(407, 'This receipt has already been claimed for vat refund');
                }
            }
            return poprInfo;
        } else {
            throw new Meteor.Error(404, 'Receipt could not be found, try again or register the receipt manually');
        }
    }
});

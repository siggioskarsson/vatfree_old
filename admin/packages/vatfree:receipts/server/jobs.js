if (!Meteor.settings.disableJobs) {
    receiptJobs.allow({
        // Grant full permission to any authenticated user
        admin: function (userId, method, params) {
            return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
        }
    });

    Meteor.publish('receipt-jobs', function () {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return receiptJobs.find({
            status: {
                $ne: 'completed'
            }
        });
    });

    if (Meteor.settings.jobs && Meteor.settings.jobs.receipts) {
        Meteor.startup(function () {
            // Start the myJobs queue running
            receiptJobs.startJobServer();

            if (Meteor.settings.jobs.receipts.reminders) {
                console.log('JOB activated: sendReceiptReminders');
                receiptJobs.processJobs('sendReceiptReminders', Meteor.settings.jobs.receipts.reminders, (job, cb) => {
                    Vatfree.receipts.reminders.sendAll((err, numberSent) => {
                        if (err) {
                            job.fail(err, {
                                fatal: false
                            });
                        } else {
                            job.log('Sent ' + numberSent + ' reminders for receipt originals');
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                });

                console.log('JOB activated: sendReceiptCustomsStampReminders');
                receiptJobs.processJobs('sendReceiptCustomsStampReminders', Meteor.settings.jobs.receipts.reminders, (job, cb) => {
                    Vatfree.receipts.reminders.sendCustomsStampReminders((err, numberSent) => {
                        if (err) {
                            job.fail(err, {
                                fatal: false
                            });
                            let activityLog ={
                                type: 'task',
                                subject: 'Job error',
                                status: 'new',
                                notes: "Error: " + JSON.stringify(err, Object.getOwnPropertyNames(err))
                            };
                            ActivityLogs.insert(activityLog);
                        } else {
                            job.log('Sent ' + numberSent + ' reminders for receipt customs stamps');
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                });
            }
        });
    }
}

if (Meteor.settings && Meteor.settings.cron) {
    if (Meteor.settings.cron.receiptRemindersCron) {
        SyncedCron.add({
            name: 'Send all receipt reminders',
            schedule: function(parser) {
                // 6:45 every day
                return parser.cron(Meteor.settings.cron.receiptRemindersCron);
            },
            job: function() {
                Vatfree.receipts.reminders.sendAll((err, numberSent) => {
                    if (err) {
                        ActivityLogs.insert({
                            type: 'task',
                            subject: 'ERROR: sending receipt reminders',
                            status: 'new',
                            error: err
                        });
                    } else {
                        ActivityLogs.insert({
                            type: 'burny',
                            subject: 'Receipt reminders sent',
                            status: 'done',
                            notes: "Sent " + (numberSent ? numberSent : 0) + " reminders"
                        });
                    }
                });
            }
        });
    }
    if (Meteor.settings.cron.receiptCustomsRemindersCron) {
        SyncedCron.add({
            name: 'Send all reminders for today for receipts without a customs stamp',
            schedule: function(parser) {
                return parser.cron(Meteor.settings.cron.receiptCustomsRemindersCron);
            },
            job: function() {
                Vatfree.receipts.reminders.sendCustomsStampReminders((err, numberSent) => {
                    if (err) {
                        ActivityLogs.insert({
                            type: 'task',
                            subject: 'ERROR: sending receipt customs stamp reminders',
                            status: 'new',
                            error: err
                        });
                    } else {
                        ActivityLogs.insert({
                            type: 'burny',
                            subject: 'Receipt customs stamp reminders sent',
                            status: 'done',
                            notes: "Sent " + (numberSent ? numberSent : 0) + " reminders"
                        });
                    }
                });
            }
        });
    }
}

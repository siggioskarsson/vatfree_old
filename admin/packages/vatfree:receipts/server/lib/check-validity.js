import { VatfreeCrypto } from 'meteor/vatfree:crypto/crypto';

Vatfree.receipts.checkDocValidity = function(receiptDoc) {
    let shaShum = VatfreeCrypto.calculateReceiptHash(receiptDoc);
    console.log('shaSum', shaShum, receiptDoc.shaSum);
    if (shaShum) {
        return shaShum === receiptDoc.shaSum;
    }

    return false;
};

Vatfree.receipts.checkValidity = function(receiptId) {
    let receiptDoc = Receipts.findOne({_id: receiptId});
    return Vatfree.receipts.checkDocValidity(receiptDoc);
};


import moment from 'moment/moment';

Vatfree.receipts.reminders = {
    createdWaitDays: 30,
    reminderWaitDays: 30,
    status: [
        'waitingForOriginal'
    ],
    querySelector: function () {
        return {
            status: {
                $in: Vatfree.receipts.reminders.status
            },
            createdAt: {
                $lte: moment().subtract(Vatfree.receipts.reminders.createdWaitDays, 'days').toDate()
            },
            $or: [
                {
                    'lastReminderSent': {
                        $exists: false
                    }
                },
                {
                    'lastReminderSent': {
                        $lte: moment().subtract(Vatfree.receipts.reminders.reminderWaitDays, 'days').toDate()
                    }
                }
            ]
        };
    },
    sendAll: function (callback) {
        let remindersCount = 0;
        let errors = [];
        let receiptOriginalsReminderTextId = Meteor.settings.defaults.receiptOriginalsReminderTextId;
        if (!receiptOriginalsReminderTextId) {
            if (callback) {
                callback('No reminder text defined for receipt originals');
                return;
            } else {
                throw new Meteor.Error(500, 'No reminder text defined for receipt originals');
            }
        }

        Receipts.find(Vatfree.receipts.reminders.querySelector()).forEach(function (receipt) {
            console.log('send reminder for receipt originals', receipt._id);
            try {
                // send reminder email
                if (Vatfree.receipts.reminders.send(receipt, receiptOriginalsReminderTextId)) {
                    remindersCount++;
                }
            } catch (e) {
                errors.push(JSON.stringify(e, Object.getOwnPropertyNames(e)));
            }
        });

        if (callback) {
            callback(errors.length > 0 ? {errors: errors} : null, remindersCount);
        }

        return remindersCount;
    },
    markReminderSent: function (receipt, type = 'email') {
        Receipts.update({
            _id: receipt._id
        }, {
            $set: {
                'lastReminderSent': new Date()
            },
            $addToSet: {
                'reminders': {
                    sentAt: new Date(),
                    type: type
                }
            }
        });
    },
    sendCustomsStampReminders (callback) {
        let remindersCount = 0;
        let errors = [];
        let receiptCustomsStampReminderTextId = Meteor.settings.defaults.receiptCustomsStampReminderTextId;
        if (!receiptCustomsStampReminderTextId) {
            if (callback) {
                callback('No reminder text defined for receipt customs stamp');
                return;
            } else {
                throw new Meteor.Error(500, 'No reminder text defined for receipt customs stamp');
            }
        }

        const travellerReceipts = {};
        Receipts.find({
            status: 'waitingForCustomsStamp',
            customsDate: {
                $gte: moment().startOf('day').toDate(),
                $lt: moment().endOf('day').toDate()
            },
            customsReminderSent: {
                $exists: false
            }
        }).forEach(function (receipt) {
            if (!_.has(travellerReceipts, receipt.userId)) {
                travellerReceipts[receipt.userId] = {
                    userId: receipt.userId,
                    customsCountryId: receipt.customsCountryId,
                    receipts: []
                };
            }
            travellerReceipts[receipt.userId].receipts.push(receipt);
        });

        _.each(travellerReceipts, (travellerReceipt, userId) => {
            console.log('send reminder for receipt customs stamp for traveller', userId);
            try {
                // send reminder email
                if (Vatfree.receipts.reminders.send(travellerReceipt, receiptCustomsStampReminderTextId)) {
                    remindersCount++;
                }
            } catch (e) {
                errors.push(JSON.stringify(e, Object.getOwnPropertyNames(e)));
            }
        });

        if (callback) {
            callback(errors.length > 0 ? {errors: errors} : null, remindersCount);
        }

        return remindersCount;
    },
    send: function (receipt, receiptOriginalsReminderTextId) {
        let traveller = Travellers.findOne({_id: receipt.userId}) || {};
        if (!traveller) {
            throw new Meteor.Error(404, 'Could not find traveller for reminder: ' + receipt.userId);
        }
        if (!traveller.profile || !traveller.profile.email) {
            // cannot send reminder if no email
            if (_.has(receipt, 'receipts')) {
                // this is a group sending of receipts defined in the receipts attribute
                _.each(receipt.receipts, (r) => {
                    Vatfree.receipts.reminders.markReminderSent(r, 'no-email');
                });
            } else {
                Vatfree.receipts.reminders.markReminderSent(receipt, 'no-email');
            }
            return false;
        }

        let travellerData = JSON.parse(JSON.stringify(traveller.profile));
        travellerData.status = traveller.private.status;

        let emailTemplateId = Meteor.settings.defaults.emailTemplateId;
        let emailLanguage = Vatfree.languages.getValidTravellerLanguage(traveller.profile.language);

        let emailTextDoc = EmailTexts.findOne({_id: receiptOriginalsReminderTextId});
        if (!emailTextDoc) {
            throw new Meteor.Error(404, 'Could not find email text ' + receiptOriginalsReminderTextId);
        }
        let emailText = emailTextDoc.text[emailLanguage] || emailTextDoc.text['en'];
        let emailSubject = emailTextDoc.subject[emailLanguage] || emailTextDoc.subject['en'];

        if (!emailSubject || !emailText) {
            throw new Meteor.Error(500, 'Email subject and/or text could not be found');
        }

        let shop = Shops.findOne({_id: receipt.shopId}) || {};
        shop.country = Countries.findOne({_id: shop.countryId}) || {};
        const shops = [];
        if (_.has(receipt, 'receipts')) {
            _.each(receipt.receipts, (r) => {
                let rShop = Shops.findOne({_id: r.shopId}) || {};
                rShop.country = Countries.findOne({_id: shop.countryId}) || {};
                shops.push(rShop);
            });
        }

        const customsCountry = Countries.findOne({_id: receipt.customsCountryId}) || {};

        if (shop.partnershipStatus === 'uncooperative') {
            // we do not send reminders to travellers with receipts from uncooperative shops
            if (_.has(receipt, 'receipts')) {
                // this is a group sending of receipts defined in the receipts attribute
                _.each(receipt.receipts, (r) => {
                    Vatfree.receipts.reminders.markReminderSent(r, 'uncooperative');
                });
            } else {
                Vatfree.receipts.reminders.markReminderSent(receipt, 'uncooperative');
            }
            return false;
        }

        let templateData = {
            language: emailLanguage,
            traveller: travellerData,
            shop: shop,
            shops: shops,
            receipt: receipt,
            receipts: receipt.receipts,
            customsCountry: customsCountry
        };

        import handlebars from 'handlebars';
        Vatfree.notify.registerHandlebarsHelpers(handlebars);

        Vatfree.notify.createMagicLink(emailText, traveller.profile.email, templateData);
        Vatfree.notify.createPasswordResetCode(emailText, traveller.profile.email, templateData);

        let text = handlebars.compile(emailText)(templateData);
        let subject = handlebars.compile(emailSubject)(templateData);

        let mailOptions = {
            to: traveller.profile.email
        };

        let activityIds = {
            travellerId: traveller._id,
            receiptId: receipt._id
        };

        let messageId = Vatfree.sendEmail(mailOptions, emailTemplateId, subject, text, activityIds);
        if (messageId) {
            if (_.has(receipt, 'receipts')) {
                // this is a group sending of receipts defined in the receipts attribute
                _.each(receipt.receipts, (r) => {
                    Vatfree.receipts.reminders.markReminderSent(r);
                });
            } else {
                Vatfree.receipts.reminders.markReminderSent(receipt);
            }
        }

        if (traveller) {
            const travellerStatusNotify = {
                emailTextId: receiptOriginalsReminderTextId
            };
            Vatfree.notify.sendPushNotification(travellerStatusNotify, templateData, activityIds, traveller, messageId);
        }

        return messageId;
    }
};

Meteor.methods({
    'send-receipt-reminders' () {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let job = new Job(receiptJobs, 'sendReceiptReminders', {});
        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 0
            })
            .save();
    },
    'send-customs-stamp-reminders' () {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let job = new Job(receiptJobs, 'sendReceiptCustomsStampReminders', {});
        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 0
            })
            .save();
    }
});

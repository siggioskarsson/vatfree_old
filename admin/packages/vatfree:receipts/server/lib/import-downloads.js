var DEBUGGING = true;
var getReceiptImages = function(updateData, callback) {
    let imageUrl = updateData['imageUrl'];
    if (DEBUGGING) console.log('getting imageUrl', imageUrl, 'for receipt', updateData._id);
    if (imageUrl.match('api.vatfree.com')) {
        let realImageUrl = getApiImageUrl(imageUrl);
        if (realImageUrl) {
            let fileName = realImageUrl.match(/([^\/]+\.jpg)/)[1];
            saveFile(updateData._id, realImageUrl, fileName, callback);
        } else {
            console.error('Could not get real image url for', imageUrl);
            if (callback) {
                callback('Could not get real image url for' + imageUrl)
            }
        }
    } else if (imageUrl.match('securedata.vatfree.com')) {
        let pdfUrl = imageUrl.replace('download', 'sec_pdf');
        let fileName = pdfUrl.split('?f=')[1] + '.pdf';
        saveFile(updateData._id, pdfUrl, fileName, function(err) {
            if (err) {
                callback(err);
                return false;
            } else {
                let downloadImageUrl = imageUrl.replace('download', 'image');
                fileName = downloadImageUrl.split('?f=')[1] + '.jpg';
                saveFile(updateData._id, downloadImageUrl, fileName, callback);
            }
        });
    } else {
        console.error('Do not know how to handle image url', imageUrl);
        if (callback) {
            callback('Do not know how to handle image url' + imageUrl)
        }
    }
};


var getApiImageUrl = function(url) {
    if (DEBUGGING) console.log("Getting real image url from api", url);
    try {
        let result = HTTP.get(url, {
            npmRequestOptions: {
                rejectUnauthorized: false
            }
        });
        if (result && result.statusCode === 200) {
            if (DEBUGGING) console.log("Getting real image url from api result", result.statusCode, result.content.match(/<img src="(https:\/\/secure[^"]*)"/));
            return _.unescape(result.content.match(/<img src="(https:\/\/secure[^"]*)"/)[1]);
        }
    } catch(e) {
        console.error(e);
    }

    return false;
};

var saveFile = function(receiptId, fileUrl, fileName, callback) {
    if (DEBUGGING) console.log('save file', fileName, 'from url', fileUrl, 'for receipt', receiptId);
    try {
        if (!Files.findOne({"originalName": fileName, itemId: receiptId})) {
            let file = new FS.File();
            file.name(fileName);
            file.originalName = fileName;
            if (fileName.match(/.pdf$/i)) {
                file.target = "receipts_internal";
            } else {
                file.target = 'receipts';
            }
            file.receiptId = receiptId;
            file.itemId = receiptId;

            let result = HTTP.get(fileUrl, {
                npmRequestOptions: {
                    rejectUnauthorized: false,
                    encoding: null
                }
            });

            file.attachData(result.content, {type: (fileName.match(/.pdf$/i) ? "application/pdf" : "image/jpeg") }, (err) => {
                if (err) {
                    console.error(err);
                    if (callback) callback(err);
                } else {
                    Files.insert(file);
                    callback(null);
                }
            });
        } else {
            callback(null);
        }
    } catch(e) {
        console.error(e);
        if (callback) callback(e);
    }
};


export {
    getReceiptImages
};

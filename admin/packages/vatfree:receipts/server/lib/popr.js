import moment from 'moment';

Vatfree.receipts.processPoprReceipt = function (poprId, poprHash, receiptData) {
    let poprInfo = Vatfree.receipts.getPoprInfo(poprId, poprHash);
    if (poprInfo) {
        let claim = _.find(poprInfo.claims, (receiptClaim) => {
            return receiptClaim.claimType === 'vat-refund';
        });
        if (claim) {
            throw new Meteor.Error(407, 'This receipt has already been claimed for vat refund.');
        }

        const poprShopId = poprInfo.retailerId + '/' + poprInfo.shopId;
        let shop;
        if (receiptData.shopId) {
            shop = Shops.findOne({_id: receiptData.shopId});
        } else {
            shop = Shops.findOne({poprId: poprInfo.retailerId + '/' + poprInfo.shopId});
            if (shop) {
                receiptData.shopId = shop._id
            } else {
                // could not find shop
                console.error('Could not find POPR shop', poprInfo.retailerId, poprInfo.shopId);
                return false;
            }
        }

        if (shop.poprId) {
            if (!_.contains(shop.poprId, poprShopId)) {
                throw new Meteor.Error(407, 'The selected shop does not match the POPr shop.');
            }
        } else {
            Shops.update({
                _id: receiptData.shopId
            },{
                $set: {
                    poprId: poprShopId
                }
            });
        }

        // fix shop parameters, if missing
        receiptData.countryId = shop.countryId || Meteor.settings.defaults.countryId;
        receiptData.currencyId = shop.currencyId || Meteor.settings.defaults.currencyId;

        // we must create the id here to be able to set it in popr :-(
        receiptData._id = Random.id();
        receiptData.poprId = poprId;
        receiptData.poprSecret = poprHash;

        // will throw an exception if claiming fails
        verifyAndClaimPoprReceipt(poprId, poprHash, receiptData);

        // set popr info
        receiptData.amount = Number(poprInfo.totalValue);
        receiptData.purchaseDate = new Date(poprInfo.time * 1000);

        if (poprInfo.vatLines) {
            _.each(poprInfo.vatLines, (vatLine) => {
                let vatRate = vatLine.vat / 100;
                let vatAmount = vatLine.vatAmount;
                if (vatRate && vatAmount) {
                    receiptData.vat[vatRate.toString().replace('.', '_')] = vatAmount;
                }
            });
        }
        if (poprInfo.totalVAT) {
            receiptData.totalVat = Number(poprInfo.totalVAT);
        }

        return receiptData;
    } else {
        throw new Meteor.Error(404, 'Could not find digital receipt');
    }
};

const getPoprInfoPostProcess = function (poprReceipt) {
    if (poprReceipt && poprReceipt.data && poprReceipt.data.result && poprReceipt.data.status && poprReceipt.data.status === 'ok') {
        if (poprReceipt.data.result.invoice.revokedAt) {
            throw new Meteor.Error(407, 'This receipt has been annulled by the retailer.');
        }

        let poprInfo = poprReceipt.data.result.invoice;
        let shop = Shops.findOne({poprId: poprInfo.retailerId + '/' + poprInfo.shopId}, {
            fields: {
                _id: 1,
                name: 1,
                addressFirst: 1,
                postalCode: 1,
                city: 1,
                countryId: 1
            }
        });
        if (shop) {
            poprInfo.shop = shop;
            return poprInfo;
        } else {
            // we don't have the shop info yet :-( now what?
            // TODO ...
            poprInfo.shop = {};
            return poprInfo;
        }
    } else {
        throw new Meteor.Error(404, 'We could not find the qr code in our database. Try again or register the receipt manually.');
    }
};

Vatfree.receipts.getPoprInfo = function (poprId, poprHash) {
    let url = Vatfree.receipts.getPoprUrl() + 'invoice/' + poprId + '/' + poprHash;

    let poprReceipt;
    try {
        poprReceipt = HTTP.get(url);
    } catch (e) {
        throw new Meteor.Error(404, 'We could not find the qr code in our database. Try again or register the receipt manually.');
    }

    return getPoprInfoPostProcess(poprReceipt);
};

export const getPoprInfoAsAdmin = function(poprId) {
    let url = Vatfree.receipts.getPoprUrl() + 'admin/invoice/' + poprId;

    const token = poprAdminLogin();

    let poprReceipt;
    try {
        poprReceipt = HTTP.get(url, {
            headers: {
                'X-Auth-Token': token.authToken,
                'X-User-Id': token.userId
            }
        });
    } catch (e) {
        throw new Meteor.Error(404, 'We could not find the qr code in our database. Try again or register the receipt manually.');
    }

    poprAdminLogout(token);
    return getPoprInfoPostProcess(poprReceipt);
};

Vatfree.receipts.getPoprUrl = function () {
    return Meteor.settings.poprUrl || 'http://api.popr.io/';
};

Vatfree.receipts.claimPoprReceipt = function (poprId, poprHash, receiptData) {
    // lay tax claim to this receipt, if not possible abort
    let shopId = receiptData.shopId;
    let claimUrl = Vatfree.receipts.getPoprUrl() + 'invoice/claim';
    try {
        let poprClaim = HTTP.post(claimUrl, {
            data: {
                id: poprId + '/' + poprHash,
                claim: 'vat-refund',
                claimId: 'vatfree:' + receiptData._id,
                claimName: 'vatfree.com',
                claimData: {
                    receiptId: receiptData._id,
                    shopId: shopId
                }
            }
        });
        if (!poprClaim || !poprClaim.data || !poprClaim.data.result) {
            throw new Meteor.Error(500, 'Could not claim receipt for tax refund');
        }

        receiptData.poprClaimToken = poprClaim.data.result.claimToken;

        // We do not need popr receipts to be sent to us :-)
        receiptData.originalsCheckedBy = 'POPR';
        receiptData.originalsCheckedAt = new Date();
        receiptData.archiveCode = 'popr:' + poprId;
        receiptData.archiveBox = 'POPR';
    } catch (e) {
        if (e.response && e.response.statusCode === 407) {
            throw new Meteor.Error(407, 'Receipt has already been claimed for vat refund');
        } else {
            throw new Meteor.Error(500, 'Could not claim receipt for tax refund');
        }
    }
};

const claimPoprReceiptAsAdmin = function (poprId, receiptData) {
    // lay tax claim to this receipt, if not possible abort
    const token = poprAdminLogin();
    let shopId = receiptData.shopId;
    let claimUrl = Vatfree.receipts.getPoprUrl() + 'admin/invoice/claim';
    try {
        let poprClaim = HTTP.post(claimUrl, {
            headers: {
                'X-Auth-Token': token.authToken,
                'X-User-Id': token.userId
            },
            data: {
                id: poprId + '/admin',
                claim: 'vat-refund',
                claimId: 'vatfree:' + receiptData._id,
                claimName: 'vatfree.com',
                claimData: {
                    receiptId: receiptData._id,
                    shopId: shopId
                }
            }
        });
        if (!poprClaim || !poprClaim.data || !poprClaim.data.result) {
            throw new Meteor.Error(500, 'Could not claim receipt for tax refund');
        }

        receiptData.poprClaimToken = poprClaim.data.result.claimToken;

        // We do not need popr receipts to be sent to us :-)
        receiptData.originalsCheckedBy = 'POPR';
        receiptData.originalsCheckedAt = new Date();
        receiptData.archiveCode = 'popr:' + poprId;
        receiptData.archiveBox = 'POPR';
    } catch (e) {
        if (e.response && e.response.statusCode === 407) {
            throw new Meteor.Error(407, 'Receipt has already been claimed for vat refund');
        } else {
            throw new Meteor.Error(500, 'Could not claim receipt for tax refund');
        }
    }
};

const poprAdminLogin = function(callback) {
    const result = HTTP.post(Vatfree.receipts.getPoprUrl() + '/login', {
        data: Meteor.settings.popr.adminAuth
    });

    return result.data.data;
};
const poprAdminLogout = function(token) {
    // fire and forget
    HTTP.post(Vatfree.receipts.getPoprUrl() + '/logout', {
        headers: {
            'X-Auth-Token': token.authToken,
            'X-User-Id': token.userId
        }
    }, (err) => {
        if (err) console.error(err);
    });
};

export const searchPoprReceiptById = function(searchTerm, callback) {
    const token = poprAdminLogin();
    let receipts;
    const result = HTTP.post(Vatfree.receipts.getPoprUrl() + 'admin/invoice/search', {
        headers: {
            'X-Auth-Token': token.authToken,
            'X-User-Id': token.userId
        },
        data: {
            searchTerm
        }
    });

    receipts = result.data.result;

    poprAdminLogout(token);

    return receipts;
};


export const verifyAndClaimPoprReceipt = function (receiptId, poprId, poprHash) {
    let receipt = Receipts.findOne({_id: receiptId});
    let originalReceipt = JSON.parse(JSON.stringify(receipt));

    let poprInfo;
    if (poprHash === 'admin') {
        poprInfo = getPoprInfoAsAdmin(poprId, poprHash);
    } else {
        poprInfo = Vatfree.receipts.getPoprInfo(poprId, poprHash);
    }

    if (poprInfo.totalValue !== receipt.amount) {
        throw new Meteor.Error(500, 'Amount of POPr receipt (' + Vatfree.numbers.formatAmount(poprInfo.totalValue) + ') does not match the amount on the Phoenix receipt (' + Vatfree.numbers.formatAmount(receipt.amount) + ')');
    }
    if (poprInfo.totalVAT !== receipt.totalVat) {
        throw new Meteor.Error(500, 'VAT of POPr receipt (' + Vatfree.numbers.formatAmount(poprInfo.totalVAT) + ') does not match the VAT on the Phoenix receipt (' + Vatfree.numbers.formatAmount(receipt.totalVat) + ')');
    }
    if (moment.unix(poprInfo.time).format('YYYY-MM-DD') !== moment(receipt.purchaseDate).format('YYYY-MM-DD')) {
        throw new Meteor.Error(500, 'Purchase date of POPr receipt (' + moment.unix(poprInfo.time).format('YYYY-MM-DD') + ') does not match the purchase date on the Phoenix receipt (' + moment(receipt.purchaseDate).format('YYYY-MM-DD') + ')');
    }

    let shop = Shops.findOne({poprId: poprInfo.retailerId + '/' + poprInfo.shopId});
    if (shop) {
        if (shop._id !== receipt.shopId) {
            throw new Meteor.Error(500, 'Shop of POPr receipt (' + shop.name + ') does not match the shop on the Phoenix receipt');
        }
    } else {
        // no shop set for this combination, assume the one in the receipt is correct
        Shops.update({
            _id: receipt.shopId
        }, {
            $set: {
                poprId: poprInfo.retailerId + '/' + poprInfo.shopId
            }
        });
    }

    // this function will add popr specific attributes to the receipt document, normally before an insert
    if (poprHash === 'admin') {
        claimPoprReceiptAsAdmin(poprId, receipt);
    } else {
        Vatfree.receipts.claimPoprReceipt(poprId, poprHash, receipt);
    }

    if (receipt.poprClaimToken) {
        let setData = {
            poprId: poprId,
            poprSecret: poprHash,
            poprClaimToken: receipt.poprClaimToken,
            status: 'visualValidationPending'
        };

        if (!originalReceipt.originalsCheckedBy) {
            setData['originalsCheckedBy'] = receipt.originalsCheckedBy;
            setData['originalsCheckedAt'] = receipt.originalsCheckedAt;
        }
        if (!originalReceipt.archiveCode) {
            setData['archiveCode'] = receipt.archiveCode;
            setData['archiveBox'] = receipt.archiveBox;
        }

        Receipts.update({
            _id: receiptId
        }, {
            $set: setData
        });
    }
};

/* global Vatfree: true */

import moment from 'moment/moment';
const csv = require('csvtojson');
import { getReceiptImages } from 'meteor/vatfree:receipts/server/lib/import-downloads';

const DEBUGGING = true;

let colMapping = {
    "Receipt: ID": "salesforceId",
    "REQlookup: Record ID": "requestId",
    "EXPORT Contact ID": "_travellerId",
    "EXPORT Shop ID": "_shopId",
    "EXPORT VTB ID": "_invoiceId",
    "EXPORT VTB Account ID": false,
    "Receipt: BTWBonnr": "receiptNr",
    "REQlookup: REQ": false,
    "REQtype": "reqType",
    "EXPORT PAYABLE ID": "payoutId",
    "Partnercode": "_source",
    "btotal": "amount",
    "pBTW_hi": "vatHigh",
    "pBTW_lo": "vatLow",
    "pBTW_tot": "totalVat",
    "refund": "refund",
    "service fee": "serviceFee",
    "purchasedate": "purchaseDate",
    "Custom stamp": "customsCountryId",
    "Check remarks": "_checkRemarks",
    "remark": "notes",
    "archive box": "archiveCode",
    "PHNX Export Status": "status",
    "REQ_status": false,
    "Scanlookup": "Scanlookup"
};

const convert = {
    "amount": "currency",
    "totalVat": "currency",
    "vatLow": "currency",
    "vatHigh": "currency",
    "refund": "currency",
    "serviceFee": "currency",
    "language": "lowerCase",
    "purchaseDate": "date",
    "customsCountryId": "country"
};

const cleanupUpdateData = function(updateData) {
    let shop = Shops.findOne({id: updateData['_shopId']}) || {};
    updateData['shopId'] = shop._id;
    updateData['countryId'] = shop.countryId;
    updateData['language'] = shop.language;
    if (shop.companyId) {
        updateData.companyId = shop.companyId;
        let billingCompany = Vatfree.billing.getBillingEntityForCompany(shop.companyId);
        if (billingCompany) updateData.billingCompanyId = billingCompany._id;
    }

    let traveller = Meteor.users.findOne({'profile.contactId': updateData['_travellerId']},{fields: {_id: 1}}) || {};
    updateData['userId'] = traveller._id;

    let invoice = Invoices.findOne({'salesforceId': updateData['_invoiceId']},{fields: {_id: 1}}) || {};
    updateData['invoiceId'] = invoice._id;

    updateData['receiptNr'] = Number(updateData['receiptNr'].replace('-', '0'));

    updateData['archiveBox'] = 'salesforce';
    updateData['currencyId'] = Meteor.settings.defaults.currencyId;

    updateData['customsDate'] = updateData['purchaseDate'];
    updateData['customsCountryId'] = Meteor.settings.defaults.countryId;

    if (updateData['_source']) {
        // Site, App, Desk, Desk Client, Office
        switch(updateData['_source']) {
            case 'Office':
                updateData['source'] = 'office';
                break;
            case 'Desk':
                updateData['source'] = 'desk';
                updateData['channel'] = 'dropbox';
                break;
            case 'Desk Client':
                updateData['source'] = 'desk';
                updateData['channel'] = 'inperson';
                break;
            case 'App':
                updateData['source'] = 'mobile';
                break;
            case 'Site':
                updateData['source'] = 'web';
                break;
        }
    } else {
        updateData['source'] = 'desk';
    }

    if (updateData['archiveCode'] === "-1") updateData['archiveCode'] = "";

    updateData['vat'] = {
        "6": updateData['vatLow'],
        "21": updateData['vatHigh']
    };

    updateData['createdAt'] = updateData['purchaseDate'];
    updateData['updatedAt'] = new Date();

    if (updateData['_checkRemarks']) {
        updateData['checkRemarks'] = [ {userId: '', name: 'salesforce', date: updateData['createdAt'], remark: updateData['_checkRemarks'] } ];
    }

    // receiptType
    // REQ, ShopREQ, NATOREQ, Cargo, OldNavoREQ
    switch(updateData["reqType"]) {
        case 'NATOREQ':
        case 'OldNavoREQ':
            updateData['receiptTypeId'] = "nY7SoFKGFbHXBeHT6";
            break;
        case 'Cargo':
            updateData['receiptTypeId'] = "KuzFRkKwRbvR4275n";
            break;
        case 'REQ':
        case 'ShopREQ':
        default:
            break;
    }

    // set incorrect status from export to correct check status
    if (updateData['status'] === 'doublecheckOpenBalance') {
        updateData['status'] = 'waitingForAdministrativeCheck';
    }

    if (updateData['status'] === 'paid') {
        updateData['payoutId'] = "salesforce:" + updateData['payoutId'];
    } else {
        delete updateData['payoutId'];
    }

    return updateData;
};
const _saveReceipt = function(updateData, callback) {
    let receipt = Receipts.findOne({
        'salesforceId': updateData['salesforceId']
    });

    let imageUrl = JSON.parse(JSON.stringify(updateData['Scanlookup'])); // this is cleaned from the object
    updateData = Receipts.simpleSchema().clean(updateData);

    // add created dates back
    updateData['createdAt'] = updateData['purchaseDate'];
    updateData['createdWeek'] =  Number(moment(updateData['purchaseDate']).format('GGGGWW'));
    updateData['createdMonth'] =  Number(moment(updateData['purchaseDate']).format('MM'));
    updateData['createdYear'] =  Number(moment(updateData['purchaseDate']).format('YYYY'));

    let postProcess = function (updateData, imageUrl) {
        if (updateData['invoiceId']) {
            Invoices.update({
                _id: updateData['invoiceId']
            },{
                $addToSet: {
                    receiptIds: updateData._id
                }
            });
        }

        Receipts.updateReceiptTextSearch(updateData);
        Receipts.updateUserReceiptStatistics(updateData.userId);
        Receipts.updateShopReceiptStatistics(updateData.shopId);
        if (updateData.affiliateId) {
            Receipts.updateAffiliateReceiptStatistics(updateData.affiliateId);
        }

        let importData = JSON.parse(JSON.stringify(updateData));
        importData['imageUrl'] = imageUrl;
        if (importData['imageUrl']) {
            let job = new Job(importFiles, 'receiptImportDownloads', importData);
            // Set some properties of the job and then submit it
            job.priority('normal')
                .retry({
                    retries: 2
                })
                .save();
        }
    };

    // set all languages to English
    if (receipt) {
        Receipts.direct.update({
            _id: receipt._id
        }, {
            $set: updateData
        }, {

        }, function(err) {
            if (err) {
                console.log('ERROR inserting receipt', updateData['salesforceId']);
                console.error(err);
            } else {
                updateData._id = receipt._id;
                postProcess(updateData, imageUrl);
            }
            callback(err, receipt._id);
        });
        if (DEBUGGING) console.log('updating receipt', updateData['salesforceId'], receipt._id);
    } else {
        if (DEBUGGING) console.log('saving new receipt', updateData['salesforceId']);
        Receipts.direct.insert(updateData, (err, receiptId) => {
            if (err) {
                console.log('ERROR inserting receipt', updateData['salesforceId']);
                console.error(err);
                console.error(updateData);
            } else {
                updateData._id = receiptId;
                postProcess(updateData, imageUrl);
            }
            callback(err, receiptId);
        });
    }
};
const saveReceipt = Meteor.wrapAsync(_saveReceipt);

Vatfree['import-receipts'] = function (importFile, fileObject, callback) {
    Vatfree.setImportFileProgress(10, 'Populating lookup data', fileObject);

    let countries = {};
    Countries.find().forEach(function (country) {
        countries[country.code] = country;
    });

    let totalLines = Vatfree.importFileLines(importFile);
    let lines = 0;
    Vatfree.setImportFileProgress(12, 'Initializing csv', fileObject);
    csv({
        delimiter: ';',
        noheader: false,
        trim: true,
        flatKeys: true,
        constructResult: false
    }).fromFile(importFile)
        .on('json', Meteor.bindEnvironment((row) => {
            lines++;
            if (row &&  _.size(row) > 2) {
                if (lines % 100 === 0) {
                    console.log('progress', 12 + Math.round(89 * (lines/totalLines)), 'Importing ' + lines + ' of ' + totalLines);
                    Vatfree.setImportFileProgress(12 + Math.round(89 * (lines/totalLines)), 'Importing ' + lines + ' of ' + totalLines, fileObject);
                }

                let updateData = Vatfree.import.processColumns(colMapping, convert, row, countries);
                cleanupUpdateData(updateData);

                if (!updateData['shopId']) {
                    console.error('Could not find shop', updateData['_shopId'], "for receipt", updateData['salesforceId']);
                } else if (!updateData['userId']) {
                    console.error('Could not find traveller', updateData['_travellerId'], "for receipt", updateData['salesforceId']);
                } else if (updateData['amount'] > 0) {
                    saveReceipt(updateData);
                } else {
                    console.error('Amount too low', updateData['amount'], "for receipt", updateData['salesforceId']);
                }
            }
        }))
        .on('done', Meteor.bindEnvironment((error) => {
            Vatfree.setImportFileProgress(100, 'Done', fileObject);
            if (callback) {
                callback();
            }
        }));
};

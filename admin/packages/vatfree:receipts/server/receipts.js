import { VatfreeCrypto } from 'meteor/vatfree:crypto/crypto';
import moment from 'moment';
import { pruneEmpty } from 'meteor/vatfree:core/lib/prune';

Receipts._ensureIndex({'poprId': 1}, {unique: 1, sparse: 1});
Receipts._ensureIndex({'userId': 1});
Receipts._ensureIndex({'companyId': 1});
Receipts._ensureIndex({'invoiceId': 1});
Receipts._ensureIndex({'createdAt': -1});
Receipts._ensureIndex({'shopId': 1});
Receipts._ensureIndex({'affiliateId': 1});
Receipts._ensureIndex({'geo': "2dsphere"});
Receipts._ensureIndex({'status': 1, 'receiptNr': 1});
Receipts._ensureIndex({'textSearch': "text"}, {"language_override": "_text_language"});

ActivityStream.attachHooks(Receipts);
// Add index to activity stream collection for receipts
ActivityStream.collection._ensureIndex({'securityContext.receiptId': 1});

let checkUserStatus = function (user, doc, onCustomsDate) {
// First check whether we need to trigger something for the user
    if (user && user.private && user.private.status === 'userVerified') {
        if (!Vatfree.travellers.isVerificationValid(doc.userId, onCustomsDate)) {
            // documents expired
            user.private.status = 'documentationExpired';
            Meteor.users.update({
                _id: doc.userId
            }, {
                $set: {
                    'private.status': user.private.status
                }
            });
        } else if (!Vatfree.travellers.areDocumentsOK(doc.userId)) {
            ActivityLogs.insert({
                type: 'task',
                travellerId: doc.userId,
                subject: 'Salesforce docs missing, please upload',
                status: 'new'
            });
        }
    }
};

Receipts.updateEuroAmounts = function(receipt) {
    if (!receipt || !receipt._id) return;

    const date = moment(receipt.purchaseDate).format('YYYY-MM-DD');
    const values = {
        amount: receipt.amount || null,
        totalVat: receipt.totalVat || null,
        refund: receipt.refund || null,
        serviceFee: receipt.serviceFee || null,
        affiliateServiceFee: receipt.affiliateServiceFee || null,
        deferredServiceFee: receipt.deferredServiceFee || null
    };

    let exchangedValues;
    if (receipt.currencyId !== Meteor.settings.defaults.currencyId) {
        exchangedValues = Vatfree.exchangeRates.getEuroValuesById(date, values, receipt.currencyId);
    } else {
        exchangedValues = values;
    }

    Receipts.direct.update({
        _id: receipt._id
    },{
        $set: {
            euroAmounts: pruneEmpty(exchangedValues)
        }
    });
};

Receipts.before.insert(function(userId, doc) {
    let user = Meteor.users.findOne({
        _id: doc.userId
    });
    if (!doc.affiliateId && user && user.private && user.private.affiliateId) {
        doc.affiliateId = user.private.affiliateId;
    }

    // manipulate status based on user status
    if (doc.status !== 'rejected' && doc.status !== 'deleted') {
        if (user) {
            checkUserStatus(user, doc);
            doc.travellerCountryId = user.profile.addressCountryId || user.profile.visaCountryId || user.profile.countryId;
        }

        let shop = Shops.findOne({_id: doc.shopId});

        if (doc.status !== 'waitingForCustomsStamp') {
            doc.status = 'userPending';

            let userVerified = user && user.private && user.private.status === 'userVerified';
            if (userVerified) {
                if (doc.originalsCheckedBy) {
                    doc.status = 'visualValidationPending';
                } else {
                    doc.status = 'waitingForOriginal';
                }
            }
        }

        if (shop) {
            if (shop.companyId) {
                doc.companyId = shop.companyId;
                let billingCompany = Vatfree.billing.getBillingEntityForCompany(shop.companyId);
                if (billingCompany) doc.billingCompanyId = billingCompany._id;
            }
            doc.partnershipStatus = shop.partnershipStatus || "new";
        } else {
            doc.partnershipStatus = "new";
        }
    } else {
        doc.partnershipStatus = "new";
    }

    // check affiliate for this traveller
    if (user && doc.affiliateId) {
        let affiliate = Affiliates.findOne({_id: doc.affiliateId});
        if (affiliate && affiliate.affiliate) {
            let affiliateFeePercentage = (_.has(affiliate.affiliate, 'serviceFee') ? affiliate.affiliate.serviceFee : Vatfree.affiliates.defaultServiceFee) / 100;
            let userDiscountPercentage = (_.has(affiliate.affiliate, 'travellerDiscount') ? affiliate.affiliate.travellerDiscount : Vatfree.affiliates.defaultTravellerDiscount) / 100;
            if (user.private && (user.private.numberOfRequests || !userDiscountPercentage)) {
                // this is not the first receipt, affiliate gets part of fee, but only if userDiscountPercentage is not 0
                let affiliateFee = Math.floor(doc.serviceFee * affiliateFeePercentage);
                doc.serviceFee = Math.round(doc.serviceFee - affiliateFee);
                doc.affiliateServiceFee = affiliateFee;
            } else {
                let extraRefund = Math.floor(doc.serviceFee * userDiscountPercentage);
                doc.refund = Math.round(doc.refund + extraRefund);
                doc.serviceFee = Math.round(doc.serviceFee - extraRefund);
            }
        }
    }

    doc.statusDates = {
        [doc.status]: new Date()
    };
});


Receipts.after.insert(function(userId, doc) {
    Receipts.updateReceiptTextSearch(doc);
    checkShopStatus(doc);
    updateShaSum(doc);

    // update stats in users and shops
    Meteor.defer(() => {
        Receipts.updateEuroAmounts(doc);

        const notificationOptions = {
            suppressMobileNotification: doc.source === 'mobile'
        };
        Vatfree.notify.processNotificationTriggers(userId, 'receipts', '- insert -', false, doc._id, notificationOptions);

        Receipts.updateUserReceiptStatistics(doc.userId);
        Receipts.updateShopReceiptStatistics(doc.shopId);
        if (doc.affiliateId) {
            Receipts.updateAffiliateReceiptStatistics(doc.affiliateId);
        }
    });
});

Receipts.before.update(function(userId, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'shopId') && (doc.status === 'readyForInvoicing' || (modifier.$set && modifier.$set.shopId && modifier.$set.shopId !== doc.shopId))) {
        let shop = Shops.findOne({_id: modifier.$set.shopId}) || {};
        modifier.$set.partnershipStatus = shop.partnershipStatus || "new";

        // update the status of the receipt, if we have not yet invoiced
        let allowUpdateStatus = [
            'userPending',
            'waitingForOriginal',
            'visualValidationPending',
            'waitingForDoubleCheck',
            'shopPending',
            'readyForInvoicing'
        ];
        if (_.contains(allowUpdateStatus, doc.status)) {
            if (shop.partnershipStatus === 'partner') {
                if (doc.originalsCheckedBy) {
                    modifier.$set.status = 'visualValidationPending';
                } else {
                    modifier.$set.status = 'waitingForOriginal';
                }
            } else {
                let user = Meteor.users.findOne({
                    _id: doc.userId
                });
                let userVerified = user && user.private && user.private.status === 'userVerified';
                if (userVerified) {
                    if (doc.originalsCheckedBy) {
                        modifier.$set.status = 'visualValidationPending';
                    } else {
                        modifier.$set.status = 'waitingForOriginal';
                    }
                } else {
                    // set the receipt back to userPending as this is not a partner shop and the user is not verified
                    modifier.$set.status = 'userPending';
                }
            }

            // set country and currency to shop
            modifier.$set.countryId = shop.countryId;
            modifier.$set.currencyId = shop.currencyId;

            // setting shop, must make sure the company is still the same
            if (shop && shop.companyId) {
                modifier.$set.companyId = shop.companyId;
                let billingCompany = Vatfree.billing.getBillingEntityForCompany(shop.companyId);
                if (billingCompany) modifier.$set.billingCompanyId = billingCompany._id;
            } else {
                if (!modifier.$unset) modifier.$unset = {};
                modifier.$unset.companyId = 1;
                modifier.$unset.billingCompanyId = 1;
            }
        } else {
            throw new Meteor.Error(404, 'Not allowed to update shop in this status.');
        }
    }

    // status change
    if (_.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status && modifier.$set.status !== doc.status) {
        if (!_.contains(['rejected', 'deleted', 'notCollectable', 'depreciated'], modifier.$set.status)) {
            let user = Meteor.users.findOne({
                _id: doc.userId
            });
            checkUserStatus(user, doc, doc.customsDate);
        }

        // receipt is being marked as paid
        if (modifier.$set.status === "paidByShop" || modifier.$set.status === 'userPendingForPayment') {
            if (!doc.paidAt) {
                modifier.$set.paidAt = new Date();
            }
        }

        // receipt is being marked as paid out
        if (modifier.$set.status === "paid") {
            if (!doc.payoutAt) {
                modifier.$set.payoutAt = new Date();
            }
        }

        if (modifier.$set.status === 'waitingForPayment' && doc.status === "paidByShop") {
            // receipt moved backwards from paidByShop to waitingForPayment
            // we must make sure any payout requests get cancelled
            if (doc.payoutId) {
                // check whether we can cancel the payout, we cannot cancel if it has been paid
                let payout = Payouts.findOne({_id: doc.payoutId});
                if (!payout) {
                    throw new Meteor.Error(500, "Could not find payout for receipt");
                }
                if (payout.status !== 'paid') {
                    Vatfree.removeReceiptFromPayout(payoutId, doc._id);
                } else {
                    throw new Meteor.Error(500, 'Receipt has already been paid in full');
                }
            }
        }
    }

    if (_.contains(fieldNames, 'companyId') && modifier.$set && modifier.$set.companyId && !modifier.$set.billingCompanyId) {
        let billingCompany = Vatfree.billing.getBillingEntityForCompany(modifier.$set.companyId);
        if (billingCompany) modifier.$set.billingCompanyId = billingCompany._id;
    }

    if (_.contains(fieldNames, 'userId') && modifier.$set && modifier.$set.userId && modifier.$set.userId !== doc.userId) {
        let newUserId = modifier.$set.userId;
        let user = Meteor.users.findOne({_id: newUserId});
        if (user) {
            modifier.$set.travellerCountryId = user.profile.countryId;
        }

        if (_.contains([
            'userPending',
            'waitingForOriginal',
            'visualValidationPending',
            'waitingForDoubleCheck',
            'shopPending',
            'readyForInvoicing'
        ], doc.status)) {
            let shop = Shops.findOne({_id: modifier.$set.shopId}) || {};
            if (shop.partnershipStatus === 'partner' || user.private.status === 'userVerified') {
                if (doc.originalsCheckedBy) {
                    modifier.$set['status'] = 'visualValidationPending';
                } else {
                    modifier.$set['status'] = 'waitingForOriginal';
                }
            } else if (user.private.status === 'userUnverified') {
                modifier.$set['status'] = 'userPending';
            }
        } else if (doc.status === 'userPendingForPayment') {
            // receipt/invoice is paid, but user is not yet verified
            if (user.private.status === 'userVerified') {
                modifier.$set['status'] = 'paidByShop';
            }
        } else if (doc.status === 'paidByShop') {
            // receipt/invoice is paid, but user is not yet verified
            if (user.private.status !== 'userVerified') {
                modifier.$set['status'] = 'userPendingForPayment';
            }
        } else if (doc.status === 'requestPayout') {
            let payout = Payouts.findOne({receiptIds: doc._id});
            Vatfree.removeReceiptFromPayout(payout._id, doc._id);

            // receipt/invoice is paid, but user is not yet verified
            if (user.private.status === 'userVerified') {
                modifier.$set['status'] = 'paidByShop';
            } else {
                modifier.$set['status'] = 'userPendingForPayment';
            }
        } else {
            // 'waitingForPayment', 'paid', 'rejected', 'notCollectable'
            // do NOT change status
        }
    }
});

Receipts.after.update(function(userId, doc, fieldNames, modifier, options) {
    Receipts.updateReceiptTextSearch(doc);
    updateShaSum(doc);

    if (_.contains(fieldNames, 'status') || _.contains(fieldNames, 'originalsCheckedBy')) {
        checkShopStatus(doc);
    }

    let previous = this.previous;
    // update stats in users and shops
    Meteor.defer(() => {
        Receipts.updateEuroAmounts(doc);

        // TEMP TEMP TEMP
        // check op readyForInvoicing and unverified traveller
        //
        let user = Meteor.users.findOne({_id: doc.userId});
        let userVerified = user && user.private && user.private.status === "userVerified";
        let invoiceStatusChange = _.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status && this.previous.status !== modifier.$set.status && modifier.$set.status === "readyForInvoicing";
        if (!userVerified && invoiceStatusChange) {
            // do nothing ....
        } else {
            Vatfree.notify.checkStatusChange('receipts', userId, this.previous, doc, fieldNames, modifier);
        }

        Receipts.updateUserReceiptStatistics(doc.userId);
        Receipts.updateShopReceiptStatistics(doc.shopId);
        if (previous.userId !== doc.userId) {
            Receipts.updateUserReceiptStatistics(previous.userId);
        }
        if (previous.shopId !== doc.shopId) {
            Receipts.updateShopReceiptStatistics(previous.shopId);
        }

        if (doc.affiliateId) {
            Receipts.updateAffiliateReceiptStatistics(doc.affiliateId);
        }
        if (previous.affiliateId && previous.affiliateId !== doc.affiliateId) {
            Receipts.updateAffiliateReceiptStatistics(previous.affiliateId);
        }
    });
});

let checkShopStatus = function (doc) {
    // check shop status and update of applicable
    if (doc.shopId && (doc.status === 'shopPending')) {
        Shops.update({
            _id: doc.shopId,
            status: 'new'
        }, {
            $set: {
                status: 'incomplete'
            }
        });
    }
};


let updateShaSum = function (doc) {
    // check shop status and update of applicable
    Receipts.direct.update({
        _id: doc._id
    },{
        $set: {
            shaSum: VatfreeCrypto.calculateReceiptHash(doc)
        }
    });
};

Receipts.updateReceiptTextSearch = function(doc) {
    let textSearch = doc.receiptNr;

    textSearch += ' salesforceId:' + doc.salesforceId;
    textSearch += ' requestId:' + doc.requestId;

    textSearch += ' amount:' + doc.amount;
    textSearch += ' vat:' + doc.totalVat;
    if (doc.archiveCode) textSearch += ' archiveCode:' + doc.archiveCode;
    if (doc.archiveBox) textSearch += ' archiveCode:' + doc.archiveBox;
    if (doc.poprId) textSearch += ' archiveCode:' + doc.poprId;

    let userId = doc.userId;
    let user = Meteor.users.findOne({_id: userId});
    if (user) {
        textSearch += ' ' + user.textSearch;
    }

    let shopId = doc.shopId;
    let shop = Shops.findOne({
        _id: shopId
    });
    if (shop) {
        textSearch += ' ' + shop.textSearch;
    }

    Receipts.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch.toLowerCase().latinize()
        }
    });
};

Receipts.updateUserReceiptStatistics = function(userId) {
    let stats = {
        'private.numberOfRequests': 0,
        'private.totalRefunds': 0,
        'private.totalServiceFee': 0,
        'private.lastRequestDate': null
    };
    Receipts.find({
        userId: userId,
        status: {
            $nin: [
                'rejected',
                'deleted',
                'notCollectable'
            ]
        }
    }).forEach((receipt) => {
        stats['private.numberOfRequests']++;
        stats['private.totalRefunds'] += receipt.refund || 0;
        stats['private.totalServiceFee'] += receipt.serviceFee || 0;
        if (receipt.createdAt > stats['private.lastRequestDate']) {
            stats['private.lastRequestDate'] = receipt.createdAt;
        }
    });

    Meteor.users.direct.update({
        _id: userId
    },{
        $set: stats
    });
};
Receipts.updateShopReceiptStatistics = function(shopId) {
    let stats = {
        'stats.numberOfRequests': 0,
        'stats.totalRefunds': 0,
        'stats.totalServiceFee': 0,
        'stats.totalRevenue': 0,
        'stats.totalVat': 0,
        'stats.lastRequestDate': null
    };
    Receipts.find({
        shopId: shopId,
        status: {
            $nin: [
                'rejected',
                'deleted'
            ]
        }
    }).forEach((receipt) => {
        stats['stats.numberOfRequests']++;
        stats['stats.totalRefunds'] += (receipt.refund || 0);
        stats['stats.totalServiceFee'] += (receipt.serviceFee || 0);
        stats['stats.totalVat'] += (receipt.totalVat || 0);
        stats['stats.totalRevenue'] += (receipt.amount || 0);
        if (receipt.createdAt > stats['stats.lastRequestDate']) {
            stats['stats.lastRequestDate'] = receipt.createdAt;
        }
    });

    Shops.direct.update({
        _id: shopId
    },{
        $set: stats
    });
};
Receipts.updateAffiliateReceiptStatistics = function(affiliateId) {
    let stats = {
        'affiliate.stats.numberOfRequests': 0,
        'affiliate.stats.totalRefunds': 0,
        'affiliate.stats.totalServiceFee': 0,
        'affiliate.stats.totalAffiliateServiceFee': 0,
        'affiliate.stats.lastRequestDate': null
    };
    Receipts.find({
        affiliateId: affiliateId,
        status: {
            $nin: [
                'rejected',
                'deleted',
                'notCollectable'
            ]
        }
    }).forEach((receipt) => {
        stats['affiliate.stats.numberOfRequests']++;
        stats['affiliate.stats.totalRefunds'] += receipt.refund || 0;
        stats['affiliate.stats.totalServiceFee'] += receipt.serviceFee || 0;
        stats['affiliate.stats.totalAffiliateServiceFee'] += receipt.affiliateServiceFee || 0;
        if (receipt.createdAt > stats['affiliate.stats.lastRequestDate']) {
            stats['affiliate.stats.lastRequestDate'] = receipt.createdAt;
        }
    });

    Affiliates.direct.update({
        _id: affiliateId
    },{
        $set: stats
    });
};

Meteor.startup(() => {
    return false;
    let fs = Npm.require('fs');
    fs.readFile('/Users/oskarsson/Downloads/receipts.csv', 'utf8', Meteor.bindEnvironment(function (err, data) {
        if (err) {
            console.log('Error: ' + err);
            return;
        }

        let rows = data.replace("\r", "").split("\n");
        rows.shift();
        _.each(rows, (row) => {
            let line = row.split(",");
            let geo = null;
            if (line[6] && line[7]) {
                geo = {
                    type: "Point",
                    coordinates: [
                        Number(line[6]),
                        Number(line[7])
                    ],
                };
            }
            let receipt = {
                id: line[0],
                name: line[1],
                addressFirst: line[2],
                postCode: line[3],
                city: line[4],
                country: line[5],
                geo: geo,
                partnershipStatus: line[8]
            };
            Receipts.insert(receipt);
        });

        console.log('rows.length', rows.length);
    }));
});

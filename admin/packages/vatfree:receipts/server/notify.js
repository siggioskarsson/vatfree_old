Vatfree.notify.sendStatusNotification['receipts'] = function (userId, receiptId, travellerStatusNotify, options) {
    try {
        let receipt = Receipts.findOne({_id: receiptId});
        receipt.currency = Currencies.findOne({_id: receipt.currencyId});

        let traveller = Travellers.findOne({_id: receipt.userId}) || {};

        let emailLanguage = "en";
        if (traveller && traveller.profile) {
            traveller.country = Countries.findOne({_id: traveller.profile.countryId }) || {};
            emailLanguage = traveller.profile.language || "en";
        }

        let shop = Shops.findOne({_id: receipt.shopId}) || {};
        shop.country = Countries.findOne({_id: shop.countryId }) || {};

        let company = Companies.findOne({_id: receipt.companyId}) || {};
        if (company) {
            company.country = Countries.findOne({_id: company.countryId }) || {};
        }

        let billingEntity = shop;
        if (shop.companyId) {
            billingEntity = Vatfree.billing.getBillingEntityForCompany(shop.companyId) || {};
        }
        if (billingEntity && billingEntity.billingContacts && billingEntity.billingContacts.length > 0) {
            billingEntity.billingContacts = Contacts.find({_id: {$in: billingEntity.billingContacts || [] }}).fetch();
        } else {
            billingEntity.billingContacts = [];
        }

        let affiliate = Companies.findOne({_id: receipt.affiliateId}) || {};
        affiliate.country = Countries.findOne({_id: affiliate.countryId }) || {};

        let activityIds = {
            receiptId: receiptId,
            travellerId: receipt.userId
        };

        let travellerData = JSON.parse(JSON.stringify(traveller.profile));
        travellerData.status = traveller.private.status;

        let templateData = {
            language: emailLanguage,
            traveller: travellerData,
            receipt: receipt,
            shop: shop,
            company: company,
            billingEntity: billingEntity,
            affiliate: affiliate
        };

        let toAddress = traveller.profile.email;
        let fromAddress = 'Vatfree.com support <support@vatfree.com>';

        Vatfree.notify.sendNotification(travellerStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage, traveller, options);
    } catch (e) {
        ActivityLogs.insert({
            type: 'task',
            userId: userId,
            subject: 'Sending of email to traveller failed',
            status: 'new',
            receiptId: receiptId,
            notes: "Error: " + e.message
        });
    }
};

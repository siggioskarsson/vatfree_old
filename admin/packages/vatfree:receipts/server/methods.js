import moment from 'moment';
import { getCollectionDescription, getCollectionItem } from 'meteor/vatfree:core/lib/collections';
import { verifyAndClaimPoprReceipt, searchPoprReceiptById } from './lib/popr';

Meteor.methods({
    getNextReceiptTask(searchTerm, selector) {
        if (!Vatfree.userIsInRole(this.userId, 'receipts-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (!selector) selector = {};
        selector['$or'] = [
            {
                status: 'visualValidationPending'
            },
            {
                status: 'waitingForDoubleCheck',
                    checkedBy: {
                $ne: this.userId
            }
            }
        ];
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let receiptId = false;
        Receipts.find(selector, {
            sort: {
                taskedAt: 1
            }
        }).forEach((receipt) => {
            // check for lock
            if (!receiptId && receipt.createdBy !== this.userId && !Locks.findOne({
                    lockId: 'receipt-tasks_' + receipt._id,
                    userId: {
                        $ne: this.userId
                    }
                })) {
                receiptId = receipt._id;
                return receiptId;
            }
        });

        return receiptId;
    },
    getNextShopReceiptTask() {
        if (!Vatfree.userIsInRole(this.userId, 'receipts-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receiptId = false;
        Receipts.find({
            status: 'shopPending',
            shopId: {
                $exists: false
            }
        },{
            sort: {
                createdAt: 1
            }
        }).forEach((receipt) => {
            // check for lock
            if (!Locks.findOne({
                    lockId: 'receipts_shop_' + receipt._id,
                    userId: {
                        $ne: this.userId
                    }
                })) {
                receiptId = receipt._id;
                return receiptId;
            }
        });

        return receiptId;
    },
    'receipt-double-check'(receiptId) {
        check(receiptId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receipt = Receipts.findOne({
            _id: receiptId,
            status: {
                $in: [
                    'visualValidationPending',
                    'waitingForDoubleCheck'
                ]
            }
        });
        if (receipt) {
            Receipts.update({
                _id: receiptId
            },{
                $set: {
                    checkedAt: new Date(),
                    checkedBy: this.userId,
                    status: 'waitingForDoubleCheck'
                }
            });

            return true;
        } else {
            throw new Meteor.Error('Could not find receipt to approve');
        }
    },
    'receipt-requeue'(receiptId) {
        check(receiptId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        Receipts.update({
            _id: receiptId
        },{
            $set: {
                taskedAt: new Date()
            }
        });
    },
    'receipt-approve'(receiptId) {
        check(receiptId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receipt = Receipts.findOne({
            _id: receiptId,
            $or: [
                {
                    status: 'visualValidationPending'
                },
                {
                    status: 'waitingForDoubleCheck',
                    checkedBy: {
                        $ne: this.userId
                    }
                }
            ]
        });
        if (receipt) {
            let setData = {
                status: 'readyForInvoicing'
            };

            let shop = Shops.findOne({_id: receipt.shopId});
            if (shop.status !== 'active' && shop.partnershipStatus !== 'partner') {
                setData.status = 'shopPending';
            }

            if (receipt.status === 'visualValidationPending') {
                setData['checkedAt'] = new Date();
                setData['checkedBy'] = this.userId;
            } else {
                // double check approved
                setData['doubleCheckedAt'] = new Date();
                setData['doubleCheckedBy'] = this.userId;
            }

            Receipts.update({
                _id: receiptId
            },{
                $set: setData,
                $unset: {
                    receiptRejectionIds: 1
                }
            });

            return true;
        } else {
            throw new Meteor.Error('Could not find receipt to approve');
        }
    },
    'receipt-reject'(receiptId, receiptRejectionIds, sendTravellerUpdate = true) {
        check(receiptId, String);
        check(receiptRejectionIds, Array);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-tasks')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receipt = Receipts.findOne({
            _id: receiptId,
            $or: [
                {
                    status: 'visualValidationPending'
                },
                {
                    status: 'waitingForDoubleCheck',
                    checkedBy: {
                        $ne: this.userId
                    }
                },
                {
                    status: 'shopPending',
                    shopId: {
                        $exists: false
                    }
                }
            ]
        });
        if (receipt) {
            let setData = {
                status: 'rejected',
                checkedAt: new Date(),
                checkedBy: this.userId,
                receiptRejectionIds: receiptRejectionIds
            };

            if (sendTravellerUpdate) {
                Receipts.update({
                    _id: receiptId
                },{
                    $set: setData
                });
            } else {
                Receipts.direct.update({
                    _id: receiptId
                },{
                    $set: setData
                });
            }

            return true;
        } else {
            throw new Meteor.Error('Could not find receipt to reject');
        }
    },
    'receipt-reopen'(receiptId) {
        check(receiptId, String);
        // here we need to check for the receipt first, as a traveller might be trying to reopen the receipt
        // and we need to know if the receipt belongs to the traveller, before checking access rights
        let receipt = Receipts.findOne({
            _id: receiptId,
            status: 'rejected'
        });
        if (receipt) {
            if (!Vatfree.userIsInRole(this.userId, 'receipts-update') && !Vatfree.userIsInRole(this.userId, 'receipts-tasks')) {
                if (! (Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP) && receipt.userId === this.userId)) {
                    throw new Meteor.Error(404, 'access denied');
                }
            }
            let status = 'visualValidationPending';
            if (!receipt.originalsCheckedBy || !receipt.originalsCheckedAt) {
                status = 'waitingForOriginal';
            }

            let shop = Shops.findOne({_id: receipt.shopId}) || {};
            // partner shops are pushed into validation immediately
            if (shop.partnershipStatus !== 'partner') {
                if (!Vatfree.travellers.isVerificationValid(receipt.userId, receipt.customsDate)) {
                    status = 'userPending';
                }
                if (!Vatfree.shops.isActive(receipt.shopId)) {
                    status = 'shopPending';
                }
                if (!receipt.originalsCheckedBy || !receipt.originalsCheckedAt) {
                    status = 'waitingForOriginal';
                }
            }

            Receipts.update({
                _id: receiptId
            },{
                $set: {
                    status: status
                },
                $unset: {
                    checkedBy: 1,
                    checkedAt: 1,
                    receiptRejectionIds: 1
                }
            });

            return true;
        } else {
            throw new Meteor.Error('Could not find receipt to reopen');
        }
    },
    'receipt-original-ok'(receiptId) {
        check(receiptId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receipt = Receipts.findOne({
            _id: receiptId,
            status: 'waitingForOriginal'
        });
        if (receipt) {
            Receipts.update({
                _id: receiptId
            },{
                $set: {
                    status: 'visualValidationPending',
                    originalCheckedBy: this.userId,
                    originalCheckedAt: new Date
                }
            });

            return true;
        } else {
            throw new Meteor.Error('Could not find receipt to approve');
        }
    },
    'update-invoiced-receipt'(receiptId, newReceipt) {
        check(receiptId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let originalReceipt = Receipts.findOne({_id: receiptId});
        let invoice = Invoices.findOne({_id: originalReceipt.invoiceId});
        if (!invoice || (invoice.status !== 'new' && invoice.status !== 'readyToSend')) {
            throw new Meteor.Error(500, 'This receipt cannot be changed. Invoice is not editable.');
        }

        if (originalReceipt.amount !== newReceipt.amount ||
            originalReceipt.totalVat !== newReceipt.totalVat ||
            originalReceipt.serviceFee !== newReceipt.serviceFee ||
            originalReceipt.refund !== newReceipt.refund
        ) {
            Invoices.update({
                _id: invoice._id
            },{
                $set: {
                    amount: invoice.amount - originalReceipt.amount + newReceipt.amount,
                    totalVat: invoice.totalVat - originalReceipt.totalVat + newReceipt.totalVat,
                    serviceFee: invoice.serviceFee - originalReceipt.serviceFee + newReceipt.serviceFee,
                    refund: invoice.refund - originalReceipt.refund + newReceipt.refund
                }
            });
        }

        Receipts.update({
            _id: originalReceipt._id
        },{
            $set: newReceipt
        });
    },
    'receipts-export'(searchTerm, status, listFilters, typeFilters, poprFilter) {
        if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        var Baby = require('babyparse');
        let selector = {};
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        if (status && status.length > 0) {
            selector['status'] = {
                $in: status
            };
        }

        _.each(listFilters, (listFilterValue, listFilterId) => {
            selector[listFilterId] = listFilterValue;
        });


        if (typeFilters && typeFilters.length > 0) {
            selector.receiptTypeId = {
                $in: typeFilters
            }
        }
        if (poprFilter) {
            selector.poprId = {
                $exists: true
            }
        }

        // cache objects
        let users = {};
        let shops = {};
        let affiliates = {};
        let countries = {};
        let invoices = {};

        let data = [];
        Receipts.find(selector, {
            sort: {
                createdAt: 1
            },
            fields: {
                textSearch: false
            }
        }).forEach((receipt) => {
            // fix amounts
            receipt.amount = Vatfree.numbers.formatCurrency(receipt.amount);
            receipt.totalVat = Vatfree.numbers.formatCurrency(receipt.totalVat);
            receipt.serviceFee = Vatfree.numbers.formatCurrency(receipt.serviceFee);
            receipt.refund = Vatfree.numbers.formatCurrency(receipt.refund);

            // fix amounts
            receipt.userId = getCollectionDescription(Meteor.users, receipt.userId, users);
            receipt.shopId = getCollectionDescription(Shops, receipt.shopId, shops);
            if (receipt.affiliateId) {
                receipt.affiliateId = getCollectionDescription(Affiliates, receipt.affiliateId, affiliates);
            }
            receipt.createdBy = getCollectionDescription(Meteor.users, receipt.createdBy, users);
            if (receipt.updatedBy) receipt.updatedBy = getCollectionDescription(Meteor.users, receipt.createdBy, users);
            if (receipt.checkedBy) receipt.checkedBy = getCollectionDescription(Meteor.users, receipt.checkedBy, users);
            if (receipt.doubleCheckedBy) receipt.doubleCheckedBy = getCollectionDescription(Meteor.users, receipt.doubleCheckedBy, users);

            receipt.countryId = getCollectionDescription(Countries, receipt.countryId, countries);
            if (receipt.invoiceId) receipt.invoiceId = getCollectionDescription(Invoices, receipt.invoiceId, invoices);

            let vat = [];
            _.each(receipt.vat, (receiptVat, vatKey) => {
                vat.push(vatKey + ': ' + Vatfree.numbers.formatCurrency(receiptVat));
            });
            receipt.vat = vat.join(', ');
            data.push(receipt);
        });
        let csv = Baby.unparse(data);

        return csv;
    },
    'receipts-export-nato'(period, billingCompanyId) {
        //"billingCompanyId": "dKmvPW49xLrgtW73p"
        if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let Baby = require('babyparse');
        let selector = {
            status: {
                $in: [
                    'readyForInvoicing',
                    'waitingForPayment',
                    'paidByShop',
                    'requestPayout',
                    'paid'
                ]
            }
        };
        if (billingCompanyId) {
            selector.billingCompanyId = billingCompanyId;
        }

        // cache objects
        let users = {};
        let travellerTypes = {};
        let shops = {};
        let invoices = {};

        let data = [];
        Receipts.find(selector, {
            sort: {
                createdAt: 1
            },
            fields: {
                textSearch: false
            }
        }).forEach((receipt) => {
            let traveller = getCollectionItem(Meteor.users, receipt.userId, users);
            let travellerType = getCollectionItem(TravellerTypes, traveller.private.travellerTypeId, travellerTypes) || {};
            if (travellerType.isNato || receipt.receiptTypeId === "nY7SoFKGFbHXBeHT6") {
                let exportData = {};
                exportData.ReceiptNr = receipt.receiptNr;
                exportData.Traveller = (traveller.profile.name || traveller.profile.email);

                let shop = getCollectionItem(Shops, receipt.shopId, shops);
                exportData.Company = shop.name + ',' + shop.city;

                let invoice = getCollectionItem(Invoices, receipt.invoiceId, invoices);
                if (invoice) {
                    exportData.InvoiceNr = invoice.invoiceNr;
                    exportData.InvoiceDate = Vatfree.dates.formatDate(invoice.invoiceDate, "DD/MM/YYYY");
                } else {
                    exportData.InvoiceNr = "";
                    exportData.InvoiceDate = "";
                }

                // fix amounts
                exportData.Amount = Vatfree.numbers.formatCurrency(receipt.amount);
                exportData.TotalVat = Vatfree.numbers.formatCurrency(receipt.totalVat);
                exportData.ServiceFee = Vatfree.numbers.formatCurrency(receipt.serviceFee);
                exportData.Refund = Vatfree.numbers.formatCurrency(receipt.refund);

                data.push(exportData);
            }
        });
        let csv = Baby.unparse(data);

        return csv;
    },
    'get-next-envelop-number'(physicalLocation, shopIds) {
        if (!Vatfree.userIsInRole(this.userId, 'receipts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }
        physicalLocation = physicalLocation || "MO";
        shopIds = shopIds || [];

        let prefixNewShop = "";
        _.each(shopIds, (shopId) => {
            let shop = Shops.findOne({_id: shopId});
            if (shop && shop.partnershipStatus && _.contains(['new', 'uncooperative'], shop.partnershipStatus)) {
                prefixNewShop = "N";
            }
        });
        if (prefixNewShop) {
            physicalLocation += prefixNewShop;
        }

        let year = moment().format('YYYY').toString();
        return physicalLocation + moment().format('YYMMDD').toString() + "-" + incrementCounter(Counters, physicalLocation + 'envelopNr_' + year);
    },
    'archive-receipt'(receiptId, physicalLocation) {
        check(receiptId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }
        physicalLocation = physicalLocation || "MO";

        let receipt = Receipts.findOne({
            _id: receiptId
        });
        if (receipt) {
            let shop = Shops.findOne({_id: receipt.shopId});
            if (shop && shop.partnershipStatus && _.contains(['new', 'uncooperative'], shop.partnershipStatus)) {
                physicalLocation += "N";
            }

            let year = moment().format('YYYY').toString();
            let archiveNumber = physicalLocation + moment().format('YYMMDD').toString() + "-" + incrementCounter(Counters, physicalLocation + 'envelopNr_' + year);

            let user = Meteor.users.findOne({_id: receipt.userId});

            let setData = {
                archiveCode: archiveNumber,
                archiveBox: receipt.source,
                originalsCheckedBy: this.userId,
                originalsCheckedAt: new Date
            };
            if (receipt.status !== 'rejected' && receipt.status !== 'deleted') {
                if (receipt.status === "waitingForCustomsStamp") {
                    let userVerified = user && user.private && user.private.status === 'userVerified';
                    if (userVerified || (
                        _.contains(['partner'], shop.partnershipStatus)
                        &&
                        Vatfree.travellers.ncpFilledIn(user)
                    )
                    ) {
                        setData.status = 'visualValidationPending';
                    } else {
                        setData.status = 'userPending';
                    }
                } else if (receipt.status === "waitingForOriginal" ||
                    (
                        _.contains(['partner', 'pledger'], shop.partnershipStatus) &&
                        Vatfree.travellers.ncpFilledIn(user)
                    )
                ) {
                    setData.status = 'visualValidationPending';
                }
            }

            Receipts.update({
                _id: receiptId
            },{
                $set: setData
            });
            return archiveNumber;
        } else {
            throw new Meteor.Error('Could not find receipt to archive');
        }
    },
    'get-last-traveller-receipt'(travellerId) {
        check(travellerId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Receipts.findOne({
            userId: travellerId
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    'get-receipt-stats'(selector) {
        if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }
        const pipeline = [
            {
                $match: selector,
            },
            {
                $group: {
                    _id: null,
                    count: {$sum: 1},
                    amount: {$sum: {$ifNull: ['$euroAmounts.amount', 0]}},
                    exVat: {$sum: {$subtract: ['$euroAmounts.amount', '$euroAmounts.totalVat']}},
                    vat: {$sum: {$ifNull: ['$euroAmounts.totalVat', 0]}},
                    fee: {$sum: {$add: [{$ifNull: ['$euroAmounts.serviceFee', 0]}, {$ifNull: ['$euroAmounts.deferredServiceFee', 0]}]}},
                    affiliateFee: {$sum: {$ifNull: ['$euroAmounts.affiliateServiceFee', 0]}},
                    refund: {$sum: {$ifNull: ['$euroAmounts.refund', 0]}},
                    popr: {$sum: {$cond: {if: {$eq: [{$type: '$poprId'}, 'string']}, then: 1, else: 0}}}
                }
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'reject-receipt'(receiptId, receiptRejectionIds, sendTravellerUpdate) {
        check(receiptId, String);
        check(receiptRejectionIds, Array);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let setData = {
            $set: {
                status: "rejected",
                receiptRejectionIds: receiptRejectionIds
            }
        };

        if (sendTravellerUpdate) {
            Receipts.update({
                _id: receiptId
            }, setData);
        } else {
            Receipts.direct.update({
                _id: receiptId
            }, setData);
        }
    },
    'get-similar-receipts'(receiptId) {
        check(receiptId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receipt = Receipts.findOne({_id: receiptId}) || {};

        return Receipts.find({
            _id: {
                $ne: receiptId
            },
            shopId: receipt.shopId,
            purchaseDate: {
                $gte: moment(receipt.purchaseDate).startOf('day').toDate(),
                $lte: moment(receipt.purchaseDate).endOf('day').toDate()
            },
            amount: receipt.amount,
            status: {
                $nin: ['rejected', 'deleted']
            }
        }).fetch()
    },
    'claim-popr-receipt'(receiptId, poprId, poprHash) {
        check(receiptId, String);
        check(poprId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }
        verifyAndClaimPoprReceipt(receiptId, poprId, poprHash);
    },
    'register-popr-receipt'(receiptId, poprId) {
        check(receiptId, String);
        check(poprId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receipt = Receipts.findOne({_id: receiptId});
        let originalReceipt = JSON.parse(JSON.stringify(receipt));

        let setData = {
            poprId: poprId,
            poprSecret: poprId,
            poprClaimToken: poprId,
            status: "visualValidationPending"
        };

        if (!originalReceipt.originalsCheckedBy) {
            setData['originalsCheckedBy'] = receipt.originalsCheckedBy;
            setData['originalsCheckedAt'] = receipt.originalsCheckedAt;
        }
        if (!originalReceipt.archiveCode) {
            setData['archiveCode'] = receipt.archiveCode;
            setData['archiveBox'] = receipt.archiveBox;
        }

        Receipts.update({
            _id: receiptId
        },{
            $set: setData
        });
    },
    'search-popr-receipts'(searchTerm) {
        check(searchTerm, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        try {
            return searchPoprReceiptById(searchTerm);
        } catch(e) {
            throw new Meteor.Error(500, e);
        }
    },
    'pre-fill-receipt-add'(receiptData) {
        if (!Vatfree.userIsInRole(this.userId, 'receipts-create')) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (receiptData.poprShopId && !receiptData.shopId) {
            let shop = Shops.findOne({poprId: receiptData.poprShopId}, {fields: {shopId: true}});
            if (shop) {
                receiptData.shopId = shop._id;
            }
        }

        let setData = {
            $set: {
                'profile.receiptAddData': receiptData
            }
        };
        if (!receiptData) {
            setData = {
                $unset: {
                    'profile.receiptAddData': true
                }
            }
        }

        Meteor.users.update({
            _id: this.userId
        }, setData);
    },
    'insert-receipt'(receiptData) {
        if (!Vatfree.userIsInRole(this.userId, 'receipts-create')) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (receiptData.poprId) {
            Vatfree.receipts.processPoprReceipt(receiptData.poprId, receiptData.poprSecret, receiptData);
        }

        return Receipts.insert(receiptData);
    },
    'check-validity'(receiptId) {
        check(receiptId, String);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Vatfree.receipts.checkValidity(receiptId);
    },
    'receipt-delete'(receiptId) {
        check(receiptId, String);
        const receipt = Receipts.findOne({_id: receiptId});
        if (!receipt || !Vatfree.receipts.travellerAllowedDeleted(receipt)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Receipts.update({
            _id: receiptId
        },{
            $set: {
                status: 'deleted'
            },
            $unset: {
                poprId: true,
                poprSecret: true,
                poprClaimToken: true
            }
        });
    },
    'receipts-forward-partner-flow'(receiptIds) {
        check(receiptIds, Array);
        if (!Vatfree.userIsInRole(this.userId, 'receipts-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        Receipts.find({
            _id: {
                $in: receiptIds
            },
            status: {
                $in: ['userPending', 'waitingForOriginal']
            }
        }).forEach((receipt) => {
            Receipts.update({
                _id: receipt._id
            }, {
                $set: {
                    status: 'visualValidationPending'
                }
            });
        });
    },
    'fix-receipt-euro-amounts'() {
        Receipts.find({
            "euroAmounts.amount": {
                $exists: false
            }
        }).forEach((receipt) => {
            Receipts.updateEuroAmounts(receipt);
        });
    }
});


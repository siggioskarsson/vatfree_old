Meteor.publishComposite('receipts-flow', function(partnershipStatus, status, selector, originalsIn = null) {
    if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};
    selector.partnershipStatus = partnershipStatus || 'partner';
    selector.status = {
        $in: status
    };
    if (originalsIn === true) {
        selector.originalsCheckedBy = {
            $exists: true
        };
    }
    if (originalsIn === false) {
        selector.originalsCheckedBy = {
            $exists: false
        };
    }

    let receiptIds = [];
    Receipts.find(selector, {fields: { _id: 1, receiptNr: 1, userId: 1}}).forEach((receipt) => {
        if (receiptIds.length < 50) {
            let user = Travellers.findOne({_id: receipt.userId});
            const ncpFilledIn = Vatfree.travellers.ncpFilledIn(user);
            if (ncpFilledIn) {
                const nrOfFiles = Files.find({target: 'receipts', itemId: receipt._id, subType: {$ne: 'form'}}).count();
                if (nrOfFiles > 0) {
                    receiptIds.push(receipt._id);
                }
            }
        }
    });

    return {
        find: function () {
            return Receipts.find({
                _id: {
                    $in: receiptIds
                }
            }, {
                sort: {
                    receiptNr: 1
                }
            });
        },
        children: [
            {
                collectionName: "travellers",
                find: function (receipt) {
                    return Meteor.users.find({
                        _id: receipt.userId
                    });
                }
            },
            {
                find: function (receipt) {
                    return Shops.find({
                        _id: receipt.shopId
                    });
                }
            },
            {
                find: function (receipt) {
                    return Files.find({
                        itemId: receipt._id,
                        target: 'receipts'
                    });
                }
            }
        ]
    };
});

Meteor.publish("receipt-stats", function () {
    if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    let self = this;
    let initializing = true;

    let countReceipts = 0;

    let handleReceipts = Receipts.find(Vatfree.receipts.reminders.querySelector(), {
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countReceipts++;
            if (!initializing)
                self.changed("stats", 'receiptsToSendReminders', {count: countReceipts});
        },
        removed: function (id, fields) {
            countReceipts--;
            if (!initializing)
                self.changed("stats", 'receiptsToSendReminders', {count: countReceipts});
        }
    });

    // Observe only returns after the initial added callbacks have
    // run.  Now return an initial value and mark the subscription
    // as ready.
    initializing = false;
    self.added("stats", 'receiptsToSendReminders', {count: countReceipts});
    self.ready();

    // Stop observing the cursor when client unsubs.
    // Stopping a subscription automatically takes
    // care of sending the client any removed messages.
    self.onStop(function () {
        handleReceipts.stop();
    });
});

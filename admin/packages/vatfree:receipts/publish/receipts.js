Meteor.publishComposite('receipts', function(searchTerm, selector, sort, limit, offset, includeFiles) {
    if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    const children = [
        {
            collectionName: "travellers",
            find: function (receipt) {
                return Meteor.users.find({
                    _id: receipt.userId
                },{
                    fields: {
                        profile: 1
                    }
                });
            }
        },
        {
            find: function (receipt) {
                return Shops.find({
                    _id: receipt.shopId
                },{
                    fields: {
                        name: 1,
                        addressFirst: 1,
                        city: 1,
                        countryId: 1,
                        partnershipStatus: 1
                    }

                });
            }
        }
    ];

    if (includeFiles) {
        children.push({
            find: function (receipt) {
                return Files.find({
                    itemId: receipt._id,
                    target: 'receipts'
                });
            }
        });
    }

    return {
        find: function () {
            return Receipts.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset,
                fields: {
                    reminders: false
                }
            });
        },
        children: children
    };
});

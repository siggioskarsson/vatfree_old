Meteor.publishComposite('user-receipts', function(userId) {
    check(userId, String);
    this.unblock();
    if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Receipts.find({
                userId: userId
            });
        },
        children: [
            {
                find: function (receipt) {
                    return Shops.find({
                        _id: receipt.shopId
                    });
                }
            },
            {
                find: function (receipt) {
                    return Files.find({
                        itemId: receipt._id,
                        target: 'receipts'
                    });
                }
            }
        ]
    }
});

Meteor.publishComposite('receipts_item', function(receiptId) {
    check(receiptId, String);
    if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Receipts.find({
                _id: receiptId
            });
        },
        children: [
            {
                collectionName: "travellers",
                find: function (receipt) {
                    return Meteor.users.find({
                        _id: receipt.userId
                    });
                }
            },
            {
                find: function (receipt) {
                    return Shops.find({
                        _id: receipt.shopId
                    });
                },
                children: [
                    {
                        find: function (shop) {
                            let company = Companies.findOne({_id: shop.companyId}) || {};
                            return Companies.find({
                                _id: {
                                    $in: company.ancestors || []
                                }
                            });
                        }
                    }
                ]
            },
            {
                find: function (receipt) {
                    return Invoices.find({
                        receiptIds: receipt._id
                    });
                }
            },
            {
                find: function (receipt) {
                    return Payouts.find({
                        $or: [
                            {
                                receiptIds: receipt._id
                            },
                            {
                                originalReceiptIds: receipt._id
                            }
                        ]
                    });
                }
            },
            {
                collectionName: "affiliates",
                find: function (receipt) {
                    return Affiliates.find({
                        _id: receipt.affiliateId || ""
                    });
                }
            },
            {
                find: function (receipt) {
                    return Files.find({
                        itemId: receipt._id,
                        target: 'receipts'
                    });
                }
            }
        ]
    };
});

Meteor.publish('receipt-files', function (receiptId) {
    this.unblock();
    if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    check(receiptId, String);

    return Files.find({
        itemId: receiptId,
        target: {
            $in: ['receipts', 'receipts_internal']
        }
    });
});

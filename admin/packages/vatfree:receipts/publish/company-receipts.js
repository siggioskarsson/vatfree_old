Meteor.publishComposite('company-receipts', function(companyId, searchTerm, status, limit, offset) {
    check(companyId, String);
    let selector = {
        billingCompanyId: companyId
    };

    if (searchTerm) {
        check(searchTerm, String);
    }
    if (status) {
        check(status, Array);
        selector['status'] = {
            $in: status
        }
    }

    if (!limit) limit = 0;
    if (!offset) offset = 0;

    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Receipts.find(selector, {
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                collectionName: "travellers",
                find: function (receipt) {
                    return Meteor.users.find({
                        _id: receipt.userId
                    });
                }
            },
            {
                find: function (receipt) {
                    return Shops.find({
                        _id: receipt.shopId
                    });
                }
            }
        ]
    }
});

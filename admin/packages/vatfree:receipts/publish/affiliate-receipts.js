Meteor.publishComposite('affiliate-receipts', function(affiliateId, searchTerm, status, limit, offset) {
    check(affiliateId, String);
    let selector = {
        affiliateId: affiliateId
    };

    if (searchTerm) {
        check(searchTerm, String);
    }
    if (status) {
        check(status, Array);
        selector['status'] = {
            $in: status
        }
    }

    if (!limit) limit = 0;
    if (!offset) offset = 0;

    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Receipts.find(selector, {
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                collectionName: "travellers",
                find: function (receipt) {
                    return Meteor.users.find({
                        _id: receipt.userId
                    });
                }
            }
        ]
    }
});

Meteor.publishComposite('receipts-all-files', function (searchTerm, selector, sort, limit, offset) {
    this.unblock();

    check(selector, Object);
    if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        find: function () {
            return Receipts.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                find: function(receipt) {
                    return Files.find({
                        itemId: receipt._id,
                        target: 'receipts'
                    });
                }
            }
        ]
    };
});

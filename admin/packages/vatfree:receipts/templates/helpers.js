/* global receiptHelpers: true, receiptEvents: true */
import moment from 'moment';

/**
 * NOTE: THIS FILE LIVES IN THE ADMIN AND I RE-USED IN MULTIPLE LOCATIONS !
 * @param template
 */

initShopIdSelect2 = function (template) {
    template.$('select[name="shopId"]').select2({
        minimumInputLength: 1,
        multiple: false,
        ajax: {
            transport: function (params, success, failure) {
                let limit = 20;
                let offset = 0;
                Meteor.call('search-shops', params.data.q || "", limit, offset, (err, res) => {
                    if (err) {
                        failure(err);
                    } else {
                        success({results: res});
                    }
                });
            },
            delay: 500
        },
        "language": {
            "noResults": function () {
                return "<div class='select2-no-matches'>" + Vatfree.translate("No results_") + " <a href='#' onclick=\"$('.add-shop').val(1).trigger('change');\">" + Vatfree.translate("Add the shop_") + "</a></div>";
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    });
};

let getReceipt = function (template) {
    let receipt = JSON.parse(JSON.stringify(template.data.receiptTypeId ? template.data : this));
    if (template.receiptTypeId && template.receiptTypeId.get) {
        // template overwrite - for instance when we are editing and want immediate feedback in the UI
        receipt.receiptTypeId = template.receiptTypeId.get();
    }
    return receipt;
};
export const receiptHelpers = {
    allowReopenReceipt() {
        let allowReopen = false;
        let selector = {};
        if (this.receiptRejectionIds && this.receiptRejectionIds.length > 0) {
            selector['_id'] = {
                $in: this.receiptRejectionIds
            };
            ReceiptRejections.find(selector).forEach((receiptRejection) => {
                if (receiptRejection.allowReopen) {
                    allowReopen = true;
                }
            });
        }

        return allowReopen
    },
    getReceiptSelector: function (template) {
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }
        Vatfree.search.addStatusFilter.call(template, selector);
        Vatfree.search.addListFilter.call(template, selector);
        if (_.isFunction(template.selectorFunction)) template.selectorFunction(selector);

        if (selector.status && selector.status['$in'] && _.difference(selector.status['$in'], template.todoStatus).length === 0 && _.difference(template.todoStatus, selector.status['$in']).length === 0) {
            // todoStatus also needs checked by
            selector['$or'] = [
                {
                    status: 'visualValidationPending'
                },
                {
                    status: 'waitingForDoubleCheck',
                    checkedBy: {
                        $ne: Meteor.userId()
                    }
                }
            ];
        }
        return selector;
    },
    getReceipts() {
        let template = Template.instance();
        let selector = receiptHelpers.getReceiptSelector(template);

        return Receipts.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    isMyTask() {
        return Vatfree.receipts.isMyTask(this, Meteor.userId());
    },
    isWaitingForOriginal() {
        return this.status === 'waitingForOriginal';
    },
    showWaitingForOriginalButton() {
        return !this.originalsCheckedBy && _.contains(['userPending', 'shopPending', 'waitingForOriginal', 'waitingForCustomsStamp'], this.status);
    },
    getContactType() {
        switch (this.contactType) {
            case 'tel':
                return 'phone';
            case 'email':
            default:
                return 'envelope';

        }
    },
    getContactDetails() {
        return "-";
    },
    getPartnershipStatusLogo() {
        switch (this.partnershipStatus) {
            case 'new':
                return 'VFCircleUnkown.svg';
            case 'affiliate':
                return 'vfcircleaffiliate.svg';
            case 'pledger':
                return 'VFCircleBlue.svg';
            case 'partner':
                return 'VFCircleShade.svg';
            case 'uncooperative':
                return 'VFCircleNonrefund.svg';
            default:
                return 'VFCircleUnkown.svg';
        }
    },
    getTraveller() {
        // Travellers is not available, not known how to publishComposite into that collection
        return Travellers.findOne({_id: this.userId});
    },
    getShop() {
        return Shops.findOne({_id: this.shopId});
    },
    statusIsTask() {
        return this.status === 'visualValidationPending' || (this.status === 'waitingForDoubleCheck' && this.checkedBy !== Meteor.userId());
    },
    isReceiptType(type) {
        let template = Template.instance();
        return template.receiptTypeId ? template.receiptTypeId.get() === type : template.data.receiptTypeId === type;
    },
    selectedReceiptType() {
        let receiptTypeId = Template.instance().receiptTypeId.get();
        return ReceiptTypes.findOne({_id: receiptTypeId});
    },
    getExtraFields(receiptTypeId) {
        let extraFields = this.extraFields;
        if (receiptTypeId) {
            let receiptType = ReceiptTypes.findOne({_id: receiptTypeId}) || {};
            extraFields = receiptType.extraFields;
        }
        let fields = [];
        let index = 10;
        _.each(extraFields, (fieldDef, fieldKey) => {
            let field  = _.clone(fieldDef);
            field._id = this._id;
            field.fieldKey = fieldKey;
            field.index = index++;
            fields.push(field);
        });

        return fields;
    },
    getExtraFieldValue() {
        let template = Template.instance();
        return template.data['receiptTypeExtraFields'] ? template.data['receiptTypeExtraFields'][this.fieldKey] : null;
    },
    isExtraFieldSelected(fieldKey) {
        let data = Template.instance().data;
        let currentData = this.toString();
        if (currentData && data && data.receiptTypeExtraFields) {
            return currentData === data.receiptTypeExtraFields[fieldKey];
        }
    },
    getInvoiceDescription() {
        if (this.invoiceId) {
            let invoice = Invoices.findOne({_id: this.invoiceId});
            if (invoice) {
                return invoice.invoiceNr + ' - ' + moment(invoice.invoiceDate).format(TAPi18n.__('_date_format'));
            }
        }
    },
    getServiceFee() {
        let template = Template.instance();
        let shopId = template.shopId ? template.shopId.get() : this.shopId;
        let fees = Vatfree.receipts.getFees(shopId, getReceipt.call(this, template));
        return fees.serviceFee;
    },
    getMinServiceFee() {
        let template = Template.instance();
        let shopId = template.shopId ? template.shopId.get() : this.shopId;
        let fees = Vatfree.receipts.getFees(shopId, getReceipt.call(this, template));
        return fees.minFee;
    },
    getMaxServiceFee() {
        let template = Template.instance();
        let shopId = template.shopId ? template.shopId.get() : this.shopId;
        let fees = Vatfree.receipts.getFees(shopId, getReceipt.call(this, template));
        return fees.maxFee;
    },
    getFees() {
        let template = Template.instance();
        let shopId = template.shopId ? template.shopId.get() : this.shopId;
        return Vatfree.receipts.getFees(shopId, getReceipt.call(this, template));
    },
    isNotStandardFee() {
        let template = Template.instance();
        let fees = Vatfree.receipts.getFees(this.shopId, getReceipt.call(this, template));
        return fees.serviceFee !== Vatfree.receipts.standardServiceFee
            && fees.minFee !== Vatfree.receipts.standardMinFee
            && fees.maxFee !== Vatfree.receipts.standardMaxFee;
    },
    getParentForFees() {
        return Vatfree.receipts.getParentForFees(this.shopId, this);
    },
    getCountryVatRates() {
        let template = Template.instance();
        let countryId = (template.countryId ? template.countryId.get() : false) || this.countryId;
        let country = Countries.findOne({_id: countryId});
        if (country) {
            let rates = [];
            let index = 20;
            _.each(country.vatRates, (rate) => {
                rate = _.clone(rate);
                rate.index = index++;
                rates.push(rate);
            });

            return rates;
        }
    },
    getVatRateValue() {
        let template = Template.instance();
        let vat = template.data.vat;

        if (vat) {
            return vat[(this.rate).toString().replace('.', '_')];
        }
    }
};

let calcAffiliateFee = function (affiliateId, fee, template) {
    if (affiliateId) {
        const affiliate = (Affiliates.findOne({_id: affiliateId}) || {}).affiliate;
        if (affiliate) {
            let affiliateFeePercentage = (_.has(affiliate, 'serviceFee') ? affiliate.serviceFee : Vatfree.affiliates.defaultServiceFee) / 100;

            let affiliateFee = Math.floor(fee * affiliateFeePercentage);
            fee = Math.round(fee - affiliateFee);
            template.$('input[name="affiliateServiceFee"]').val(Vatfree.numbers.formatAmount(affiliateFee, 2));
        }
    }

    return fee;
};

receiptEvents = {
    'change input[name="amount"]'(e, template) {
        let amount = Number(template.$('input[name="amount"]').val()) * 100;
        let countryId = template.countryId ? template.countryId.get() : this.countryId;
        let vatRate = 21;
        if (countryId) {
            let country = Countries.findOne({_id: countryId});
            if (country) {
                vatRate = Number(Vatfree.vat.getDefaultRate(country.vatRates));
            }
        }
        let vat = Vatfree.vat.calculateVat(amount, vatRate) / 100;
        template.$('input[name="totalVat"]').val(vat);
        template.$('input[name="totalVat"]').trigger('change');
    },
    'change input[name="totalVat"]'(e, template) {
        let amount = Number(template.$('input[name="amount"]').val()) * 100;
        let totalVat = Math.round(Number(template.$('input[name="totalVat"]').val()) * 100);
        let deferredServiceFee = template.deferredServiceFee ? template.deferredServiceFee.get() : false;
        let fixedDeferredServiceFee = template.$('input[name="fixedDeferredServiceFee"]').prop('checked');

        let countryId = template.countryId ? template.countryId.get() : this.countryId;
        if (countryId) {
            let country = Countries.findOne({_id: countryId});
            if (country) {
                let rates = country.vatRates;
                if (rates) {
                    // check whether we have a full on vat rate thingy
                    let selectedRate = false;
                    _.each(rates, (rate) => {
                        let vatEstimate = Vatfree.vat.calculateVat(amount, Number(rate.rate));
                        if (vatEstimate === totalVat) {
                            selectedRate = rate.rate;
                        }
                    });
                    if (selectedRate) {
                        template.$('input[name="vat.' + selectedRate + '"]').val(Vatfree.numbers.formatAmount(totalVat, 2));
                        _.each(rates, (rate) => {
                            if (rate.rate !== selectedRate) {
                                template.$('input[name="vat.' + rate.rate + '"]').val(0);
                            }
                        });
                    }
                }
            }
        }

        const affiliateId = template.affiliateId ? template.affiliateId.get() : false;
        let shopId = template.shopId ? template.shopId.get() : this.shopId;
        let fees = Vatfree.receipts.getFees(shopId, getReceipt.call(this, template));

        let fee = Vatfree.vat.calculateFee(totalVat, fees.serviceFee, fees.minFee, fees.maxFee);
        let refund = Vatfree.vat.calculateRefund(totalVat, fees.serviceFee, fees.minFee, fees.maxFee);
        if (fees.deferredServiceFee || deferredServiceFee) {
            let deferredFee = fee;
            if (fees.deferredServiceFee) {
                deferredFee = Vatfree.vat.calculateFee(totalVat, fees.deferredServiceFee, fees.deferredMinFee, fees.deferredMaxFee);
            }
            if (Vatfree.receipts.isNato( getReceipt.call(this, template)) ) {
                deferredFee = (Meteor.settings.public.natoFee || 900);
            }

            if (!fixedDeferredServiceFee) {
                deferredFee = calcAffiliateFee(affiliateId, deferredFee, template);
                template.$('input[name="deferredServiceFee"]').val(Vatfree.numbers.formatAmount(deferredFee, 2));
            } else {
                calcAffiliateFee(affiliateId, deferredFee, template);
            }

            template.$('input[name="refund"]').val(Vatfree.numbers.formatAmount(totalVat, 2));
        } else {
            fee = calcAffiliateFee(affiliateId, fee, template);
            template.$('input[name="serviceFee"]').val(Vatfree.numbers.formatAmount(fee, 2));
            template.$('input[name="refund"]').val(Vatfree.numbers.formatAmount(refund, 2));
        }
    },
    'change input[name^="vat."]'(e, template) {
        let countryId = template.countryId ? template.countryId.get() : this.countryId;
        if (countryId) {
            let country = Countries.findOne({_id: countryId});
            if (country) {
                let rates = country.vatRates;
                if (rates) {
                    // check whether we have a full on vat rate thingy
                    let totalVat = 0;
                    _.each(rates, (rate) => {
                        totalVat += Number(template.$('input[name="vat.' + rate.rate + '"]').val());
                    });
                    template.$('input[name="totalVat"]').val(totalVat).trigger('change');
                }
            }
        }
    },
    'change input[name="deferredServiceFee"]'(e, template) {
        let fixedDeferredServiceFee = template.$('input[name="fixedDeferredServiceFee"]').prop('checked');
        if (!fixedDeferredServiceFee) {
            const affiliateId = template.affiliateId ? template.affiliateId.get() : false;
            if (affiliateId) {
                let totalVat = Number(template.$('input[name="totalVat"]').val()) * 100;
                let deferredServiceFee = Number(template.$('input[name="deferredServiceFee"]').val() || 0) * 100;

                let shopId = template.shopId ? template.shopId.get() : this.shopId;
                let fees = Vatfree.receipts.getFees(shopId, getReceipt.call(this, template));
                let fee = Vatfree.vat.calculateFee(totalVat, fees.serviceFee, fees.minFee, fees.maxFee);

                let affiliateServiceFee = fee - deferredServiceFee;
                template.$('input[name="affiliateServiceFee"]').val(Vatfree.numbers.formatAmount(affiliateServiceFee, 2));
            }
        }
    },
    'change input[name="serviceFee"]'(e, template) {
        let totalVat = Number(template.$('input[name="totalVat"]').val()) * 100;
        let serviceFee = Number(template.$('input[name="serviceFee"]').val()) * 100;
        let affiliateServiceFee = Number(template.$('input[name="affiliateServiceFee"]').val() || 0) * 100;

        let refund = totalVat - serviceFee - affiliateServiceFee;

        template.$('input[name="refund"]').val(Vatfree.numbers.formatAmount(refund, 2));
    },
    'change input[name="affiliateServiceFee"]'(e, template) {
        let deferServiceFee = template.deferredServiceFee ? template.deferredServiceFee.get() : false;

        let totalVat = Number(template.$('input[name="totalVat"]').val()) * 100;
        let refund = Number(template.$('input[name="refund"]').val()) * 100;
        let affiliateServiceFee = Number(template.$('input[name="affiliateServiceFee"]').val() || 0) * 100;

        if (deferServiceFee) {
            let shopId = template.shopId ? template.shopId.get() : this.shopId;
            let fees = Vatfree.receipts.getFees(shopId, getReceipt.call(this, template));
            let fee = Vatfree.vat.calculateFee(totalVat, fees.serviceFee, fees.minFee, fees.maxFee);

            let deferredServiceFee = fee - affiliateServiceFee;
            if (affiliateServiceFee > deferredServiceFee) {
                toastr.error('Affiliate service fee cannot be higher than the service fee.');
                return false;
            }

            let fixedDeferredServiceFee = template.$('input[name="fixedDeferredServiceFee"]').prop('checked');
            if (!fixedDeferredServiceFee) {
                template.$('input[name="deferredServiceFee"]').val(Vatfree.numbers.formatAmount(deferredServiceFee, 2));
            }
        } else {
            let serviceFee = totalVat - refund - affiliateServiceFee;
            if (serviceFee < 0) {
                toastr.error('Service fee cannot be negative.');
                return false;
            }
            template.$('input[name="serviceFee"]').val(Vatfree.numbers.formatAmount(serviceFee, 2));
        }
    },
    'change input[name="refund"]'(e, template) {
        let amount = Number(template.$('input[name="totalVat"]').val());
        let deferredServiceFee = template.deferredServiceFee ? template.deferredServiceFee.get() : false;
        const affiliateId = template.affiliateId ? template.affiliateId.get() : false;

        if (!deferredServiceFee) {
            let fee = (amount - Number(template.$('input[name="refund"]').val())) * 100;
            fee = calcAffiliateFee(affiliateId, fee, template);
            template.$('input[name="serviceFee"]').val(Vatfree.numbers.formatAmount(fee, 2));
        }
    }
};

receiptEditFormAfterFlush = function() {
    Vatfree.templateHelpers.initDatepicker.call(this);
    this.$('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square icheckbox_square-blue',
        radioClass: 'iradio_square iradio_square-blue'
    });
    this.$('select[name="userId"]').select2({
        minimumInputLength: 1,
        multiple: false,
        ajax: {
            transport: function (params, success, failure) {
                let limit = 20;
                let offset = 0;
                Meteor.call('search-travellers', params.data.q || "", limit, offset, (err, res) => {
                    if (err) {
                        failure(err);
                    } else {
                        success({results: res});
                    }
                });
            },
            delay: 500
        }
    });
    initShopIdSelect2(this);
    this.$('select[name="affiliateId"]').select2({
        minimumInputLength: 1,
        multiple: false,
        allowClear: true,
        placeholder: {
            id: "",
            placeholder: "Select affiliate ..."
        },
        ajax: {
            transport: function (params, success, failure) {
                let limit = 20;
                let offset = 0;
                Meteor.call('search-affiliates', params.data.q || "", limit, offset, (err, res) => {
                    if (err) {
                        failure(err);
                    } else {
                        success({results: res});
                    }
                });
            },
            delay: 500
        }
    });
};

addNewShop = function (template, formElement, callback) {
    let formData = Vatfree.templateHelpers.getFormData(template, formElement);
    if (!formData.geo && !formData.place) {
        toastr.error("Please select the shop by searching for it in the input field");
        //return false;
    }
    let geo = JSON.parse(formData.geo);
    let place = JSON.parse(formData.place);
    if (place && place.address_components) {
        let name = place.name;
        let address = (_.find(place.address_components, (component) => {
            return _.contains(component.types, 'route');
        }) || {}).long_name;
        let postal_code = (_.find(place.address_components, (component) => {
            return _.contains(component.types, 'postal_code');
        }) || {}).long_name;
        let city = (_.find(place.address_components, (component) => {
            return _.contains(component.types, 'locality');
        }) || {}).long_name;
        let countryCode = (_.find(place.address_components, (component) => {
            return _.contains(component.types, 'country');
        }) || {}).short_name;
        let countryId = Vatfree.defaults.countryId;
        let country = Countries.findOne({code: countryCode});
        if (country) {
            countryId = country._id
        } else {
            country = Countries.findOne({_id: countryId});
        }
        if (!country) {
            toastr.error('Country is not supported by vatfree.com');
        }

        let shopData = {
            name: name,
            addressFirst: address,
            postCode: postal_code,
            city: city,
            countryId: countryId,
            currencyId: country.currencyId,
            geo: geo
        };

        Shops.insert(shopData, (err, shopId) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                if (template.addingShop) template.addingShop.set(false);
                if (template.countryId) template.countryId.set(countryId);
                Tracker.afterFlush(() => {
                    let shopName = name + ' - ' + (address || "") + ', ' + (postal_code || "") + ' ' + (city || "");
                    let $shopId = $('select[name="shopId"]');
                    $shopId.append($("<option></option>")
                        .attr("value", shopId)
                        .text(shopName));
                    $shopId.val(shopId);
                    initShopIdSelect2(template);

                    if (callback) {
                        callback(shopId);
                    }
                });
            }
        });
    } else {
        toastr.error('No valid location found');
    }
};

export const handleFileUpload = function (file) {
    Vatfree.trackEvent('uploaded-file', 'receipts');
    let template = this;
    let receiptId = template.data.receipt._id;
    if (file.type.match(/image\/(jpg|jpeg|png|gif)/) || file.type === "application/pdf") {
        if (template.isUploadingFile) template.isUploadingFile.set(true);

        if (file.type === "application/pdf") {
            let context = {};
            Vatfree.handlePDFUpload(file, context, function (file, context) {
                let fileUpload = {
                    name: file.name,
                    type: file.type,
                    fileContent: file.base64Url
                };
                Meteor.call('upload-receipt-image', receiptId, fileUpload, (err, result) => {
                    if (err) {
                        toastr.error(err.reason || err.message);
                    }
                });
            });

            let reader = new FileReader();
            reader.onload = function (fileLoadEvent) {
                let fileUpload = {
                    name: file.name,
                    type: file.type,
                    fileContent: reader.result,
                    internal: file
                };
                Meteor.call('upload-receipt-image', receiptId, fileUpload, (err, result) => {
                    if (err) {
                        toastr.error(err.reason)
                    } else {
                        if (template.$) template.$('input[name="dropzone-file"]').val('');
                        if (template.isUploadingFile) template.isUploadingFile.set();
                    }
                });
            };
            reader.readAsDataURL(file);
        } else {
            let reader = new FileReader();
            reader.onload = function (fileLoadEvent) {
                let fileUpload = {
                    name: file.name,
                    type: file.type,
                    size: file.size,
                    fileContent: reader.result
                };
                Meteor.call('upload-receipt-image', receiptId, fileUpload, (err, result) => {
                    if (err) {
                        toastr.error(err.reason || err.message);
                    } else {
                        if (template.$) template.$('input[name="dropzone-file"]').val('');
                    }
                    if (template.isUploadingFile) template.isUploadingFile.set();
                });
            };
            reader.onerror = function (err) {
                toast.error(err.reason || err.message);
                if (template.isUploadingFile) template.isUploadingFile.set();
            };
            reader.readAsDataURL(file);
        }
    } else {
        toastr.error('Only image files (.jpeg, .jpg, .png, .gif) and PDF are supported');
        if (template.$) template.$('input[name="dropzone-file"]').val('');
    }
};

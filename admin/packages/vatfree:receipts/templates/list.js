import './list.html';
import { receiptHelpers } from './helpers';

Template.receiptsList.onCreated(function() {
    this.originalReceiptIn = new ReactiveVar();
    this.originalReceiptId = new ReactiveVar();
});

Template.receiptsList.helpers(Vatfree.templateHelpers.helpers);
Template.receiptsList.helpers(receiptHelpers);
Template.receiptsList.helpers({
    originalReceiptIn() {
        return Template.instance().originalReceiptIn.get();
    },
    originalReceiptId() {
        return Template.instance().originalReceiptId.get();
    },
    getTotal() {
        let total = {
            amount: {},
            totalVat: {},
            refund: {},
            serviceFee: {},
            affiliateServiceFee: {},
            currencyId: {}
        };

        let receipts = this.receipts;
        if (this.receipts && this.receipts.fetch) {
            receipts = this.receipts.fetch();
        }
        _.each(receipts, (receipt) => {
            if (_.contains(['rejected', 'deleted'], receipt.status)) return;

            if (!total.amount[receipt.currencyId]) {
                total.amount[receipt.currencyId] = 0;
            }
            total.amount[receipt.currencyId] += isNaN(receipt.amount) ? 0 : receipt.amount;

            if (!total.totalVat[receipt.currencyId]) {
                total.totalVat[receipt.currencyId] = 0;
            }
            total.totalVat[receipt.currencyId] += isNaN(receipt.totalVat) ? 0 : receipt.totalVat;

            if (!total.serviceFee[receipt.currencyId]) {
                total.serviceFee[receipt.currencyId] = 0;
            }
            total.serviceFee[receipt.currencyId] += isNaN(receipt.serviceFee) ? 0 : receipt.serviceFee;

            if (!total.affiliateServiceFee[receipt.currencyId]) {
                total.affiliateServiceFee[receipt.currencyId] = 0;
            }
            total.affiliateServiceFee[receipt.currencyId] += isNaN(receipt.affiliateServiceFee) ? 0 : receipt.affiliateServiceFee;

            if (receipt.euroRefund) {
                receipt.refund = receipt.euroRefund;
                receipt.currencyId = Vatfree.defaults.currencyId;
            }

            if (!total.refund[receipt.currencyId]) {
                total.refund[receipt.currencyId] = 0;
            }
            total.refund[receipt.currencyId] += isNaN(receipt.refund) ? 0 : receipt.refund;

            total.currencyId[receipt.currencyId] += receipt.currencyId;
        });

        return total;
    },
    getTotalPerCurrency(key) {
        return _.map(this[key], (amount, currencyId) => {
            return {
                amount: amount,
                currencyId: currencyId
            }
        });
    }
});
Template.receiptsList.events(receiptEvents);
Template.receiptsList.events({
    'click input[name^="receipts."]'(e) {
        e.stopPropagation();
    },
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/receipt/:itemId', {itemId: this._id});
    },
    'click .receipt-original-in'(e, template) {
        e.preventDefault();
        e.stopPropagation();

        template.originalReceiptId.set(this._id);
        template.originalReceiptIn.set(true);
    }
});

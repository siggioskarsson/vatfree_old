Template.receipts_modal_partner_receipts.onCreated(function() {
    this.activeItem = new ReactiveVar();
    this.updatingReceipts = new ReactiveVar();
    this.checkOriginalsIn = new ReactiveVar(true); // TODO create a checkbox from this
});

Template.receipts_modal_partner_receipts.onRendered(function() {
    let partnershipStatus = this.data.partnershipStatus || 'partner';
    let selector = this.data.selector || {};
    this.autorun(() => {
        if (!this.updatingReceipts.get()) {
            this.subscribe('receipts-flow', partnershipStatus, ['userPending', 'waitingForOriginal'], selector, this.checkOriginalsIn.get());
        }
    });
});

Template.receipts_modal_partner_receipts.helpers({
    originalsInChecked() {
        return Template.instance().checkOriginalsIn.get();
    },
    getOptions() {
        let template = Template.instance();
        return {
            lockId: "receipt_partner_flow",
            lockMessage: "Receipt partner flow is locked by another user",
            sidebarTemplate: "receipts_modal_partner_receipts_sidebar",
            parentTemplateVariable: false,
            activeItem: '',
            activeReceipt: template.activeItem,
            onHide: template.data.onHide
        }
    },
    getReceipts() {
        const template = Template.instance();
        const data = template.data;
        let selector = data.selector || {};
        selector.partnershipStatus = data.partnershipStatus || 'partner';
        selector.status = {
            $in: ['userPending', 'waitingForOriginal']
        };

        if (template.checkOriginalsIn.get() === true) {
            selector.originalsCheckedBy = {
                $exists: true
            };
        } else if (template.checkOriginalsIn.get() === false) {
            selector.originalsCheckedBy = {
                $exists: false
            };
        }

        let receipts = [];
        Receipts.find(selector, {
            sort: {
                receiptNr: 1
            }
        }).forEach((r) => {
            let user = Travellers.findOne({_id: r.userId});
            let files = Files.find({target: 'receipts', itemId: r._id, subType: {$ne: 'form'}}).count();
            if (Vatfree.travellers.ncpFilledIn(user) && files > 0) {
                receipts.push(r);
            }
        });

        return receipts.length ? receipts : false;
    },
    activeItem() {
        return Template.instance().activeItem.get();
    },
    updatingReceipts() {
        return Template.instance().updatingReceipts.get();
    }
});

Template.receipts_modal_partner_receipts.events({
    'change input[name="originalsIn"]'(e, template) {
        e.stopPropagation();
        template.checkOriginalsIn.set(!!$(e.currentTarget).prop('checked'));
    },
    'click .ibox-content .item-row input[type="checkbox"]' (e, template) {
        e.stopPropagation();
        e.stopImmediatePropagation();
    },
    'click .ibox-content .item-row' (e, template) {
        e.preventDefault();
        e.stopImmediatePropagation();
        if (template.activeItem.get() === this._id) {
            template.activeItem.set();
        } else {
            template.activeItem.set(this._id);
        }
    },
    'click .select-all-global'(e, template) {
        e.preventDefault();
        template.$('input[type="checkbox"]').each(function() { $(this).prop('checked', true) } )
    },
    'click .select-none-global'(e, template) {
        e.preventDefault();
        template.$('input[type="checkbox"]').each(function() { $(this).prop('checked', false) } )
    },
    'submit form[name="tasks-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        let receiptIds = [];
        _.each(formData.receipts, (on, receiptId) => {
            if (on !== false) {
                receiptIds.push(receiptId);
            }
        });

        template.updatingReceipts.set(true);
        Meteor.setTimeout(() => {
            Meteor.call('receipts-forward-partner-flow', receiptIds, (err, result) => {
                if (err) {
                    toastr.error(err.reason);
                } else {
                    template.updatingReceipts.set(false);
                    toastr.info('Receipts have been forwarded');
                }
            });
        }, 1);
    }
});

Template.receipts_modal_partner_receipts_sidebar.helpers({
    getReceipt() {
        return Receipts.findOne({_id: this.options.activeReceipt.get()}) || false;
    }
});

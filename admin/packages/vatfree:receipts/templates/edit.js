import uriParse from 'url-parse';
import { receiptHelpers } from './helpers';
import { editFormOnCreated, editFormHelpers, editFormEvents } from 'meteor/vatfree:core-templates/templates/edit-form-helpers';
import { handleOnlyIdInput } from './popr-helpers';

Template.receiptEdit.onRendered(function () {
    this.autorun(() => {
        let receiptId = FlowRouter.getParam('receiptId');
        if (receiptId) {
            this.subscribe('receipts_item', receiptId);
        }
        this.subscribe('receipt-types');
        this.subscribe('receipt-rejections-list');

        Meteor.call('set-qr-upload-context', {
            itemId: receiptId,
            receiptId: receiptId,
            target: 'receipts'
        }, console.log);
    });
});

Template.receiptEdit.onDestroyed(function () {
    Meteor.call('unset-qr-upload-context', console.log);
});

Template.receiptEdit.helpers({
    receiptDoc() {
        return Receipts.findOne({
            _id: FlowRouter.getParam('receiptId')
        });
    },
    getBreadcrumbTitle() {
        let shop = Shops.findOne({_id: this.shopId});
        if (shop) {
            return this.receiptNr + ' - ' + shop.name + ', ' + shop.city;
        } else {
            return this.receiptNr + ' - ...';
        }
    },
    getBreadcrumbTitleReversed() {
        let shop = Shops.findOne({_id: this.shopId});
        if (shop) {
            return "'" + shop.name + ', ' + shop.city + "'" + ' (' + this.receiptNr + ')';
        } else {
            return this.receiptNr + ' - ...';
        }
    }
});

Template.receiptEditForm.onCreated(editFormOnCreated());
Template.receiptEditForm.onCreated(function () {
    this.shopId = new ReactiveVar(this.data.shopId);
    this.countryId = new ReactiveVar(this.data.countryId);
    this.currencyId = new ReactiveVar(this.data.currencyId);
    this.receiptTypeId = new ReactiveVar();
    this.affiliateId = new ReactiveVar();

    this.updatingReceipt = new ReactiveVar();
    this.rejectingReceipt = new ReactiveVar();
    this.editImageFile = new ReactiveVar();
    this.deferredServiceFee = new ReactiveVar(!!this.data.deferredServiceFee);
    this.originalReceiptIn = new ReactiveVar();
    this.originalReceiptId = new ReactiveVar();
    this.addingShop = new ReactiveVar();

    this.lockId = '';
    this.lock = new ReactiveVar();
    this.handleFileUpload = function (file) {
        Vatfree.templateHelpers.handleFileUpload(file, {
            itemId: FlowRouter.getParam('receiptId'),
            receiptId: FlowRouter.getParam('receiptId'),
            target: 'receipts'
        });
    };
});

Template.receiptEditForm.onDestroyed(function () {
    let lock = this.lock.get();
    if (lock) {
        lock.release();
    }
    this.lock.set(false);
});

Template.receiptEditForm.onRendered(function () {
    this.autorun(() => {
        this.lockId = 'receipts_' + FlowRouter.getParam('receiptId');
        let userId = Meteor.userId();
        Tracker.nonreactive(() => {
            let lock = this.lock.get();
            if (lock) {
                lock.release();
                this.lock.set(false);
            }
            console.log('creating new lock');
            let newLock = new Lock(this.lockId, userId);
            console.log('new lock created', newLock);
            this.lock.set(newLock);
        });
    });

    Tracker.afterFlush(() => {
        receiptEditFormAfterFlush.call(this);
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('receipt-files', data._id);
        this.subscribe('receipt-invoices', data._id);
        this.subscribe('activity-logs', "", {receiptId: data._id});
    });

    this.autorun(() => {
        let shopId = this.shopId.get();
        this.subscribe('shops_item', shopId || "");

        let shop = Shops.findOne({_id: shopId});
        if (shop && shop.companyId) {
            this.subscribe('companies_item', shop.companyId || "");
        }
        if (!this.countryId.get() && shop && shop.countryId) {
            this.countryId.set(shop.countryId);
            this.currencyId.set(shop.currencyId);
        }
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.receiptTypeId.set(data.receiptTypeId);
        this.affiliateId.set(data.affiliateId);
    });

    this.autorun(() => {
        const affiliateId = this.affiliateId.get();
        if (affiliateId) {
            this.subscribe('affiliates_item', affiliateId);
        }
    });

    this.autorun(() => {
        let data = Template.currentData();
        if (this.tabLoading.get('tab-6')) {
            this.subscribe('activity-stream', {receiptId: data._id}, (ready) => {
                this.tabLoading.set('tab-6', true);
            });
        }
    });

    Tracker.afterFlush(() => {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square icheckbox_square-blue',
            radioClass: 'iradio_square iradio_square-blue'
        });
        this.$('select[name="customsCountryId"]').select2();

        if (this.data.deferredServiceFee) {
            this.$('input[name="deferredServiceFeeCheck"]').prop('checked', true);
        }
    });
});

Template.receiptEditForm.helpers(editFormHelpers());
Template.receiptEditForm.helpers(receiptHelpers);
Template.receiptEditForm.helpers({
    getShopName() {
        let shop = Shops.findOne({_id: this.shopId});
        if (shop) {
            return shop.name + ' - ' + (shop.addressFirst || "") + ', ' + (shop.postCode || "") + ' ' + (shop.city || "");
        }
    },
    getCountryId() {
        return Template.instance().countryId.get();
    },
    getCurrencyId() {
        return Template.instance().currencyId.get();
    },
    getAffiliateId() {
        return Template.instance().affiliateId.get();
    },
    originalReceiptIn() {
        return Template.instance().originalReceiptIn.get();
    },
    originalReceiptId() {
        return Template.instance().originalReceiptId.get();
    },
    addingShop() {
        return Template.instance().addingShop.get();
    },
    getQRCodeContext() {
        return {
            receiptId: this._id,
            itemId: this._id,
            target: 'receipts'
        };
    },
    getQRCodeContextInternal() {
        return {
            receiptId: this._id,
            itemId: this._id,
            target: 'receipts_internal'
        };
    },
    getCompanyDoc() {
        let shop = Shops.findOne({_id: this.shopId});
        if (shop) {
            return Companies.findOne({_id: shop.companyId})
        }
    },
    getTravellerName() {
        // Travellers is not available, not known how to publishComposite into that collection
        let traveller = Travellers.findOne({_id: this.userId});
        if (traveller) {
            let text = traveller.profile.name || "";
            if (!text) {
                text = traveller.profile.email;
            } else {
                text += ' (' + traveller.profile.email + ')';
            }
            if (traveller.profile.addressFirst) {
                text += ' - ' + (traveller.profile.addressFirst || "") + ', ' + (traveller.profile.postalCode || "") + ' ' + (traveller.profile.city || "");
            }

            return text;
        }
    },
    isDisabled() {
        let template = Template.instance();
        let lock = template.lock.get();
        return !lock || !lock.isMine(Meteor.userId()) || !!template.data.invoiceId || template.data.status === 'rejected' || template.data.status === 'deleted' || template.data.deleted || !Vatfree.userIsInRole(Meteor.userId(), 'receipts-update');
    },
    isExternallyDisabled() {
        let template = Template.instance();
        let lock = template.lock.get();
        return !lock || !lock.isMine(Meteor.userId()) || !!template.data.invoiceId || template.data.status === 'rejected' || template.data.status === 'deleted' || template.data.deleted || template.data.poprId;
    },
    isStatusFieldDisabled() {
        return this.status !== "waitingForAdministrativeCheck";
    },
    isLocked() {
        let template = Template.instance();
        let lock = template.lock.get();
        return !lock || !lock.isMine(Meteor.userId());
    },
    serviceFeeDisabled() {
        let template = Template.instance();
        let lock = template.lock.get();
        return !lock || !lock.isMine(Meteor.userId()) || !!template.data.invoiceId || template.data.status === 'rejected' || template.data.status === 'deleted' || template.data.deleted || Template.instance().deferredServiceFee.get();
    },
    deferServiceFee() {
        return Template.instance().deferredServiceFee.get();
    },
    getLockUser() {
        let template = Template.instance();
        let lock = template.lock.get();
        if (lock && !lock.isMine(Meteor.userId())) {
            return lock.getCurrentLockUserName();
        }
    },
    getFiles() {
        let receiptId = FlowRouter.getParam('receiptId');
        return Files.find({
            itemId: receiptId,
            target: 'receipts'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getInternalFiles() {
        let receiptId = FlowRouter.getParam('receiptId');
        return Files.find({
            itemId: receiptId,
            target: 'receipts_internal'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    isSource(source) {
        let template = Template.instance();
        return template.data.source === source;
    },
    isCountrySelected() {
        let template = Template.instance();
        return template.data.customsCountryId === this._id;
    },
    getActivityLog() {
        return ActivityLogs.find({
            receiptId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    invoiceIsOpen() {
        let invoice = Invoices.findOne({_id: this.invoiceId});
        return invoice && _.contains(['new', 'readyToSend'], invoice.status);
    },
    allowOriginalsIn() {
        return _.contains(['waitingForCustomsStamp', 'userPending', 'waitingForOriginal'], this.status);
    },
    updatingReceipt() {
        return Template.instance().updatingReceipt.get();
    },
    rejectingReceipt() {
        return Template.instance().rejectingReceipt.get();
    },
    allowRejectReceipt() {
        return Vatfree.receipts.allowReject(this);
    },
    allowDeleteReceipt() {
        return Vatfree.receipts.travellerAllowedDeleted(this);
    },
    isMainReceipt() {
        return this.subType === 'main';
    },
    editImageFile() {
        return Template.instance().editImageFile.get();
    },
    getPayouts() {
        return Payouts.find({
            $or: [
                {
                    receiptIds: this._id
                },
                {
                    originalReceiptIds: this._id
                }
            ]
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    selectMapLocationCallback() {
        let template = Template.instance();
        return function(place) {
            if (place && place.address_components) {
                template.$('input[name="place"]').val(JSON.stringify(place));
            }
        }
    },
    formExists() {
        return !!Files.findOne({
            itemId: this._id,
            target: 'receipts',
            subType: 'form'
        });
    },
    getAffiliateFee() {
        return Vatfree.affiliates.getServiceFee(this.affiliateId);
    },
    getAffiliateDiscount() {
        return Vatfree.affiliates.getTravellerDiscount(this.affiliateId);
    },
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            }
        });
    }
});

Template.receiptEditForm.events(editFormEvents());
Template.receiptEditForm.events(Vatfree.templateHelpers.events());
Template.receiptEditForm.events(receiptEvents);
Template.receiptEditForm.events({
    'change input[name="addShop"]'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).val()) {
            template.addingShop.set(true);
            try {
                template.$('select[name="shopId"]').select2('destroy');
            } catch(e) {
                console.log(e);
            }
            $(e.currentTarget).val("");
        }
    },
    'click .cancel-add-shop'(e ,template) {
        e.preventDefault();
        template.addingShop.set();
    },
    'click .add-new-shop'(e, template) {
        e.preventDefault();
        addNewShop(template);
    },
    'click .dropzone-layout .add-file-internal': function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('input[name="dropzone-file-internal"]').trigger('click');
    },
    'change input[name="deferredServiceFeeCheck"]'(e, template) {
        let defer = $(e.currentTarget).prop('checked');
        let vat = template.$('input[name="totalVat"]').val();
        let serviceFee = template.$('input[name="serviceFee"]').val();
        let deferredServiceFee = template.$('input[name="deferredServiceFee"]').val();
        template.deferredServiceFee.set(defer);
        Tracker.afterFlush(() => {
            if (defer) {
                // move service fee to deferred
                template.$('input[name="deferredServiceFee"]').val(serviceFee);
                template.$('input[name="serviceFee"]').val(0);
                template.$('input[name="refund"]').val(vat);
            } else {
                // move deferred to service fee
                template.$('input[name="serviceFee"]').val(deferredServiceFee);
                template.$('input[name="refund"]').val(vat - deferredServiceFee);
            }
        });

    },
    'change input[name="dropzone-file-internal"]': function(e, template) {
        e.preventDefault();
        e.stopPropagation();
        let context = {
            itemId: FlowRouter.getParam('receiptId'),
            receiptId: FlowRouter.getParam('receiptId'),
            target: 'receipts_internal'
        };
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, context);
        });
    },
    'dropped .dropzone-layout.file-internal': function (e, template) {
        e.preventDefault();
        e.stopPropagation();
        $('.dropzone').removeClass('visible');
        let context = {
            itemId: FlowRouter.getParam('receiptId'),
            receiptId: FlowRouter.getParam('receiptId'),
            target: 'receipts_internal'
        };
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, context);
        });
    },
    'change select[name="receiptTypeId"]'(e, template) {
        let receiptTypeId = $(e.currentTarget).val();
        template.receiptTypeId.set(receiptTypeId);
        Tracker.afterFlush(() => {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square icheckbox_square-blue',
                radioClass: 'iradio_square iradio_square-blue'
            });
        });
    },
    'change select[name="shopId"]'(e, template) {
        let shopId = $(e.currentTarget).val();
        template.shopId.set(shopId);
    },
    'change select[name="currencyId"]'(e, template) {
        let currencyId = $(e.currentTarget).val();
        template.currencyId.set(currencyId);
    },
    'change select[name="affiliateId"]'(e, template) {
        let affiliateId = $(e.currentTarget).val();
        template.affiliateId.set(affiliateId);
    },
    'click .receipt-update'(e, template) {
        e.preventDefault();
        template.updatingReceipt.set(true);
    },
    'click .receipt-reject'(e, template) {
        e.preventDefault();
        template.rejectingReceipt.set(true);
    },
    'click .receipt-delete'(e, template) {
        e.preventDefault();
        let receiptId = this._id;
        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "Deleting this receipt will not send a notification to the user.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function () {
            Meteor.call('receipt-delete', receiptId, (err, result) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    toastr.success('Receipt deleted!');
                }
            });
        });
    },
    'click .set-main-file'(e) {
        e.preventDefault();
        let fileId = this._id;

        let setter = {
            $set: {
                subType: 'main'
            }
        };
        if (this.subType === 'main') {
            setter = {
                $unset: {
                    subType: 1
                }
            };
        }
        Files.update({
            _id: fileId
        }, setter);
    },
    'click .create-traveller-form'(e, template) {
        e.preventDefault();
        let traveller = Travellers.findOne({_id: this.userId});
        if (traveller) {
            Vatfree.receipts.createTravellerForm(this._id, traveller);
        }
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/receipts');
    },
    'submit form[name="edit-receipt-form"]'(e, template) {
        e.preventDefault();
        let closeEdit = !!$(document.activeElement).data('close');
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (formData.deferredServiceFeeCheck === 'on') {
            // refund form input is disabled when deferred is checked
            formData.refund = template.$('input[name="refund"]').val();
        }

        if (!Vatfree.receipts.processReceiptForm.call(this, formData)) {
            return false;
        }

        if (this.status === 'readyForInvoicing') {
            if (!confirm('Save receipt? Since it is "Ready for invoicing" it will be pushed back to "Waiting for visual validation".')) {
                return false;
            }
        }

        Receipts.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Receipt saved!');
            }
            if (closeEdit) {
                FlowRouter.go('/receipts');
            }
        });
    },
    'click .reopen-receipt'(e) {
        e.preventDefault();
        Meteor.call('receipt-reopen', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            }
        });
    },
    'click .receipt-task-do'(e) {
        e.preventDefault();

    },
    'click .receipt-originals-ok'(e) {
        e.preventDefault();
        Meteor.call('receipt-original-ok', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            }
        });
    },
    'click .receipt-original-in'(e, template) {
        e.preventDefault();
        e.stopPropagation();

        template.originalReceiptId.set(this._id);
        template.originalReceiptIn.set(true);
    },
    'click .receipt-register-popr'(e, template) {
        e.preventDefault();
        let receipt = this;

        import swal from 'sweetalert2';
        swal({
            title: 'Paste the POPr code here',
            text: 'Please note, The code is case sensitive!',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            reverseButtons: true,
            showCancelButton: true,
            confirmButtonText: 'Register POPr'
        }).then((result) => {
            if (result) {
                if (result.length < 20) {
                    handleOnlyIdInput(result, receipt);
                } else {
                    let uri = uriParse(result, true);
                    let pathParts = uri.pathname.split('/');
                    let poprId = pathParts[1];
                    let poprHash = pathParts[2];
                    Meteor.call('claim-popr-receipt', receipt._id, poprId, poprHash, (err, result) => {
                        if (err) {
                            toastr.error(err.reason || err.message);
                        }
                    });
                }
            }
        });
    }
});

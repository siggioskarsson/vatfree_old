import { generateQRCode } from 'meteor/vatfree:core-templates/templates/qrcode';

var checkForNewReceipt = function() {
    let template = this;
    if (template.debug) console.log('checking for new receipt');
    if (template.view && template.view.isDestroyed === true) return;

    let parentTemplate = template.parentTemplate();
    let searchTerm = parentTemplate.searchTerm.get();
    let selector = {};
    Vatfree.search.addStatusFilter.call(parentTemplate, selector);
    Vatfree.search.addListFilter.call(parentTemplate, selector);
    parentTemplate.selectorFunction(selector);
    console.log('checkForNewTraveller', searchTerm, selector);

    Meteor.call('getNextReceiptTask', searchTerm, selector, (err, receiptId) => {
        if (template.debug) console.log('getNextReceiptTask', err, receiptId);
        if (err) {
            toastr.error(err.reason);
        } else {
            if (receiptId) {
                this.receiptId.set(receiptId);
            } else {
                Meteor.setTimeout(() => {
                    checkForNewReceipt.call(template);
                }, 5000);
            }
        }
    });
};

Template.receiptTasksModal.onCreated(function() {
    this.updatingReceipt = new ReactiveVar(false);
    this.receiptId = new ReactiveVar(this.data.receiptId);
    this.selectedFile = new ReactiveVar();
    this.imageMode = new ReactiveVar('zoom');
    this.isRejecting = new ReactiveVar();
    this.changedValues = new ReactiveVar();
    this.compareImage = new ReactiveVar();
    this.editTraveller = new ReactiveVar();
    this.lockId = false;
    this.lock = false;
    this.debug = false;

    this.subscribe('receipt-rejections-list');

    let template = this;

    this.autorun(() => {
        let receiptId = this.receiptId.get();
        if (receiptId) {
            this.subscribe('receipts_item', receiptId);
            this.subscribe('receipt-files', receiptId);
            this.lockId = 'receipt-tasks_' + receiptId;

            Tracker.nonreactive(() => {
                if (this.lock) {
                    if (this.debug) console.log('releasing lock');
                    this.lock.release();
                }
                if (this.debug) console.log('creating lock', this.lockId);
                this.lock = new Lock(this.lockId, Meteor.userId());
            });
        } else {
            if (this.lockId && this.lock) {
                this.lock.release();
            }
            checkForNewReceipt.call(template);
        }
    });

    this.autorun(() => {
        if (this.receiptId.get()) {
            this.changedValues.set();
            this.isRejecting.set();
            let file = Files.findOne({
                itemId: this.receiptId.get(),
                target: 'receipts'
            },{
                sort: {
                    createdAt: 1
                }
            });
            let imageMode = this.imageMode.get();
            Tracker.nonreactive(() => {
                if (!this.selectedFile.get()) {
                    if (file) {
                        this.selectedFile.set(file._id);
                    }
                }
            });
        }
    });

    this.autorun(() => {
        if (this.receiptId.get()) {
            let receipt = Receipts.findOne({_id: this.receiptId.get()}) || {};
            let shop = Shops.findOne({_id: receipt.shopId});
            if (shop && shop.companyId) {
                this.subscribe('companies_item', shop.companyId || "");
            }
        }
    });

    this.autorun(() => {
        let compareImage = this.compareImage.get();
        if (!compareImage) {
            // reset zoom
            this.imageMode.set('reset');
            Tracker.afterFlush(() => {
                this.imageMode.set('zoom');
            });
        } else {
            this.imageMode.set('reset');
            Tracker.afterFlush(() => {
                this.imageMode.set('zoom');
            });
        }
    });

    this.hideModal = () => {
        $('#modal-receipt-tasks').modal('hide');
    };

    Tracker.afterFlush(() => {
        this.$('#modal-receipt-tasks').on('hidden.bs.modal', function () {
            template.parentTemplate().receiptTasks.set(false);
            template.receiptId.set();
            template.selectedFile.set();
        });
        this.$('#modal-receipt-tasks').on('shown.bs.modal', function () {

        });
        this.$('#modal-receipt-tasks').modal('show');

        this.keyHandler = (e) => {
            let selectedFile = this.$('li.file.active').attr('id');
            if (e.keyCode === 38) {
                // up
                e.stopPropagation();
                let prev = $('#'+selectedFile).prev();
                if (prev.length > 0) {
                    this.selectedFile.set(prev.attr('id').replace('file_', ''));
                }
            } else if (e.keyCode === 40) {
                // down
                e.stopPropagation();
                let next = $('#'+selectedFile).next();
                if (next.length > 0) {
                    this.selectedFile.set(next.attr('id').replace('file_', ''));
                }
            }
        };
        $(document).on('keyup', null, this.keyHandler);
    });

    this.autorun(() => {
        Meteor.call('set-qr-upload-context', {
            receiptId: template.receiptId.get(),
            itemId: template.receiptId.get(),
            target: 'receipts'
        }, console.log);
    });
});

Template.receiptTasksModal.onRendered(function() {
    this.autorun(() => {
        const receiptId = this.data.receiptId;
        const receipt = Receipts.findOne({_id: receiptId});
        if (receipt) {
            const fileContext = {
                receiptId: receipt._id,
                itemId: receipt._id,
                target: 'receipts'
            };
            Tracker.afterFlush(() => {
                Meteor.setTimeout(() => {
                    generateQRCode(fileContext,  this.$('.qrcode-upload > canvas')[0]);
                }, 1000);
            });
        }
    });
});

Template.receiptTasksModal.onDestroyed(function() {
    if (this.lock) {
        this.lock.release();
    }
    Meteor.call('unset-qr-upload-context', console.log);

    $(document).off('keyup', null, this.keyHandler);

    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.receiptTasksModal.helpers({
    isImageMode(mode) {
        let template = Template.instance();
        return template.imageMode.get() === mode;
    },
    getReceipt() {
        return Receipts.findOne({
            _id: Template.instance().receiptId.get()
        });
    },
    changedValues() {
        return Template.instance().changedValues.get();
    },
    compareImage() {
        return Template.instance().compareImage.get();
    },
    editTraveller() {
        return Template.instance().editTraveller.get();
    },
    isLocked() {
        let template = Template.instance();
        return !template.lock.isMine(Meteor.userId());
    },
    getLockUser() {
        let template = Template.instance();
        if (template.lock && !template.lock.isMine(Meteor.userId())) {
            return template.lock.getCurrentLockUserName();
        }
    },
    getFiles() {
        let receiptId = Template.instance().receiptId.get();
        return Files.find({
            itemId: receiptId,
            target: 'receipts'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getInternalFiles() {
        let receiptId = Template.instance().receiptId.get();
        return Files.find({
            itemId: receiptId,
            target: 'receipts_internal'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    isActiveFile(id) {
        let template = Template.instance();
        let fileId = template.selectedFile.get();
        return fileId === id ? 'active' : false;
    },
    selectedFile() {
        let template = Template.instance();
        let receiptId = template.receiptId.get();
        let fileId = template.selectedFile.get();
        if (receiptId && fileId) {
            return Files.findOne({
                _id: fileId,
                itemId: receiptId,
                target: {
                    $in: ['receipts','receipts_internal']
                }
            });
        }
    },
    doubleCheckStatus() {
        return this.status === 'waitingForDoubleCheck';
    },
    isRejecting() {
        return Template.instance().isRejecting.get();
    },
    getReceiptCheckingNotes() {
        let notes = [];
        let shop = Shops.findOne({_id: this.shopId}) || {};
        if (shop) {
            if (shop.receiptNotes) {
                notes.push(shop.receiptNotes);
            }

            if (shop.companyId) {
                // check whether we should take the fees from the parent company
                let shopCompany = Companies.findOne({_id: shop.companyId});
                if (shopCompany && shopCompany.ancestors) {
                    Companies.find({
                        _id: {
                            $in: shopCompany.ancestors
                        },
                        receiptNotes: {
                            $exists: true
                        }
                    }, {
                        sort: {
                            ancestorsLength: -1
                        }
                    }).forEach((company) => {
                        notes.push(company.receiptNotes);
                    });
                }
            }
        }

        return notes;
    },
    isMainReceipt() {
        return this.subType === 'main';
    },
    maskPhotoCallback() {
        let template = Template.instance();
        return function(fileId) {
            template.selectedFile.set(fileId);
            template.imageMode.set('zoom');
        };
    },
    formExists() {
        return !!Files.findOne({
            itemId: Template.instance().receiptId.get(),
            target: 'receipts',
            subType: 'form'
        });
    },
    isTravellerVerified() {
        let receiptId = Template.instance().receiptId.get();
        let receipt = Receipts.findOne({_id: receiptId}) || {};
        let traveller = Travellers.findOne({_id: receipt.userId});
        return traveller && traveller.private.status && traveller.private.status === "userVerified";
    },
    showFormButton() {
        const receiptId = Template.instance().receiptId.get();
        const receipt = Receipts.findOne({_id: receiptId}) || {};
        const traveller = Travellers.findOne({_id: receipt.userId}) || {};
        const showFormButton = traveller.profile && traveller.profile.passportNumber && traveller.profile.name && traveller.profile.addressCountryId;

        if (receipt && traveller && showFormButton) {
            Vatfree.receipts.createTravellerForm(receipt._id, traveller);
        }

        return showFormButton;
    },
    getTraveller() {
        return Travellers.findOne({_id: this.userId});
    },
    receiptTypeError() {
        let traveller = Travellers.findOne({_id: this.userId});
        let travellerType = TravellerTypes.findOne({_id: traveller.private.travellerTypeId});
        if (travellerType && travellerType.forceReceiptTypeId && travellerType.forceReceiptTypeId !== this.receiptTypeId) {
            let receiptType = ReceiptTypes.findOne({_id: this.receiptTypeId}) || {name: 'Normal'};
            return {
                travellerType,
                receiptType
            }
        }

        return false;
    },
    getFilteredReceiptRejections() {
        let receiptTypeId = this.receiptTypeId ? this.receiptTypeId : 'normal';
        let selector = {
            $or: [
                {
                    receiptTypeIds: receiptTypeId
                },
                {
                    receiptTypeIds: []
                },
                {
                    receiptTypeIds: {
                        $exists: false
                    }
                }
            ],
            type: 'receipt',
            deleted: {
                $ne: true
            }
        };

        if (this.poprId) {
            selector.qrRejection = true;
        }

        return ReceiptRejections.find(selector, {
            sort: {
                name: 1
            }
        });
    },
    allowDeleteFiles() {
        const receipt = Receipts.findOne({
            _id: Template.instance().receiptId.get()
        });
        return receipt.status === 'waitingForDoubleCheck';
    }
});

Template.receiptTasksModal.events({
    'dropped .drop-file-here'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        let context = {
            receiptId: this._id,
            itemId: this._id,
            target: 'receipts'
        };
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, context);
        });
    },
    'click .image-mode-zoom'(e, template) {
        e.preventDefault();
        template.imageMode.set('zoom');
    },
    'click .image-mode-crop'(e, template) {
        e.preventDefault();
        template.imageMode.set('crop');
    },
    'click .image-mode-mask'(e, template) {
        e.preventDefault();
        template.imageMode.set('mask');
    },
    'change input, change select'(e, template) {
        template.changedValues.set(true);
    },
    'click .file'(e, template) {
        e.preventDefault();
        template.selectedFile.set();
        template.editTraveller.set();

        Meteor.setTimeout(() => {
            template.selectedFile.set(this._id);
        }, 1);
    },
    'click .similar-receipts-list .receipts-list table.receipt-list > tbody > tr > td > a'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.compareImage.set(this.fileObj);
    },
    'click .close-compare-image'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.compareImage.set();
    },
    'click .set-main-file'(e) {
        e.preventDefault();
        e.stopPropagation();

        let fileId = this._id;

        let setter = {
            $set: {
                subType: 'main'
            }
        };
        if (this.subType === 'main') {
            setter = {
                $unset: {
                    subType: 1
                }
            };
        }
        Files.update({
            _id: fileId
        }, setter);
    },
    'click .receipt-reject'(e, template) {
        e.preventDefault();
        // show receipt rejection reasons
        template.isRejecting.set(true);
        Tracker.afterFlush(() => {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square icheckbox_square-blue',
                radioClass: 'iradio_square iradio_square-blue'
            });
        });
    },
    'click .receipt-reject-do'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        let receiptRejectionIds = [];
        _.each(formData.receiptRejectionIds, (includeReceiptRejection, receiptRejectionId) => {
            if (includeReceiptRejection === 'on') {
                receiptRejectionIds.push(receiptRejectionId);
            }
        });

        if (receiptRejectionIds.length <= 0) {
            toastr.error('Select 1 or more reasons for the rejection');
            return false;
        }

        const sendTravellerUpdate = formData.sendTravellerUpdate === 'on';
        Meteor.call('receipt-reject', template.receiptId.get(), receiptRejectionIds, sendTravellerUpdate, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                template.receiptId.set();
                template.selectedFile.set();
            }
        });
    },
    'click .receipt-reject-cancel'(e, template) {
        e.preventDefault();
        template.isRejecting.set();
    },
    'click .receipt-double-check'(e, template) {
        e.preventDefault();
        if (saveReceiptInput.call(template)) {
            Meteor.call('receipt-double-check', template.receiptId.get(), (err, result) => {
                if (err) {
                    toastr.error(err.reason);
                } else {
                    template.receiptId.set();
                    template.selectedFile.set();
                }
            });
        }
    },
    'click .receipt-requeue'(e, template) {
        e.preventDefault();
        let receiptId = template.receiptId.get();

        template.updatingReceipt.set(true);
        Meteor.call('receipt-requeue', receiptId, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                template.receiptId.set();
                template.selectedFile.set();
                template.updatingReceipt.set(false);
            }
        });
    },
    'click .receipt-approve'(e, template) {
        e.preventDefault();
        if (template.changedValues.get()) {
            // redo double check
            if (saveReceiptInput.call(template)) {
                Meteor.call('receipt-double-check', template.receiptId.get(), (err, result) => {
                    if (err) {
                        toastr.error(err.reason);
                    } else {
                        template.receiptId.set();
                        template.selectedFile.set();
                    }
                });
            }
        } else {
            if (confirm('Make the receipt ready for invoicing?')) {
                template.updatingReceipt.set(true);
                if (saveReceiptInput.call(template)) {
                    Meteor.call('receipt-approve', template.receiptId.get(), (err, result) => {
                        if (err) {
                            toastr.error(err.reason);
                        } else {
                            template.receiptId.set();
                            template.selectedFile.set();
                            template.updatingReceipt.set(false);
                        }
                    });
                }
            }
        }
    },
    'click .close-tasks'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'click .create-traveller-form'(e, template) {
        e.preventDefault();
        let traveller = Travellers.findOne({_id: this.userId});
        if (traveller) {
            Vatfree.receipts.createTravellerForm(this._id, traveller);
        }
    },
    'click .receipt-task-traveller-card'(e, template) {
        e.preventDefault();
        template.editTraveller.set(!template.editTraveller.get());
    },
    'click .close-traveller-edit'(e, template) {
        e.preventDefault();
        template.editTraveller.set(false);
    }
});

var saveReceiptInput = function() {
    let receiptId = this.receiptId.get();
    var formData = $('form[name="receipt-tasks-amount-form"]').serializeArray().reduce(function (obj, item) {
        if (!item.name) {
            return false;
        }
        if (item.name.indexOf('.') > 0) {
            var el = item.name.split('.');
            if (el.length === 2) {
                if (!obj[el[0]]) {
                    obj[el[0]] = {};
                }
                obj[el[0]][el[1]] = item.value;
            } else if (el.length === 3) {
                if (!obj[el[0]]) {
                    obj[el[0]] = {};
                }
                if (!obj[el[0]][el[1]]) {
                    obj[el[0]][el[1]] = {};
                }
                obj[el[0]][el[1]][el[2]] = item.value;
            }
        } else {
            obj[item.name] = item.value;
        }
        return obj;
    }, {});

    // remove remarks, they are auto saved
    delete formData['checkRemark'];
    formData['totalVat'] = $('input[name="totalVat"]').val();

    if (Vatfree.receipts.processReceiptForm(formData)) {
        Receipts.update({
            _id: receiptId
        },{
            $set: formData
        });
        return true;
    } else {
        return false;
    }
};

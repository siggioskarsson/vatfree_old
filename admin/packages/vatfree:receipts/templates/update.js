import moment from 'moment';
import { receiptHelpers } from './helpers';

Template.updateReceiptModal.onCreated(function() {
    let template = this;
    let parentVariable = this.data.parentVariable || "updatingReceipt";

    this.countryId = new ReactiveVar();
    this.autorun(() => {
    });

    let shopId = this.data.shopId;
    this.subscribe('shops_item', shopId || "");
    Meteor.call('get-shop', shopId, (err, shop) => {
        if (shop) {
            this.countryId.set(shop.countryId);
        }
    });

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-update-receipt').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-update-receipt').on('hidden.bs.modal', function () {
            if (template.parentTemplate && template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-update-receipt').on('shown.bs.modal', function () {
            template.$('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                endDate: moment().format(TAPi18n.__('_date_format')),
                format: TAPi18n.__('_date_format').toLowerCase()
            });
            template.$('select[name="customsCountryId"]').select2();
        });
        $('#modal-update-receipt').modal('show');
    });
});

Template.updateReceiptModal.helpers(receiptHelpers);
Template.updateReceiptModal.helpers({
    getCountryId() {
        return Template.instance().countryId.get();
    }
});

Template.updateReceiptModal.events(receiptEvents);
Template.updateReceiptModal.events({
    'click .cancel-update-receipt'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="update-receipt-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        // fix dates
        if (_.has(formData, 'purchaseDate')) formData.purchaseDate = moment(formData.purchaseDate, TAPi18n.__('_date_format')).toDate();
        if (_.has(formData, 'customsDate')) formData.customsDate = moment(formData.customsDate, TAPi18n.__('_date_format')).toDate();
        // fix numbers
        if (_.has(formData, 'amount')) formData.amount = Math.round(Number(formData.amount)*100);
        if (_.has(formData, 'totalVat')) formData.totalVat = Math.round(Number(formData.totalVat)*100);
        if (_.has(formData, 'serviceFee')) formData.serviceFee = Math.round(Number(formData.serviceFee)*100);
        if (_.has(formData, 'refund')) formData.refund = Math.round(Number(formData.refund)*100);

        // Checks
        if (moment(formData.customsDate).isBefore(moment(formData.purchaseDate))) {
            toastr.error('Customs stamp date cannot be before purchase date');
            return false;
        }

        let vat = 0;
        _.each(formData.vat, (vatValue, vatKey) => {
            formData.vat[vatKey] = Math.round(Number(vatValue) * 100);
            vat += formData.vat[vatKey];
        });
        if (vat < formData.totalVat) {
            toastr.error('Vat rules do not add up to total VAT amount');
            return false;
        }

        Meteor.call('update-invoiced-receipt', this._id, formData, function(err, result) {
            if (err) {
                toastr.error(err.reason);
            } else {
                template.hideModal();
            }
        });
    }
});

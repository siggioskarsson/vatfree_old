import moment from 'moment';
import QRCode from 'qrcode';
import { receiptHelpers } from './helpers';

Template.addReceiptModal.onCreated(function() {
    let template = this;
    let parentVariable = this.data.parentVariable || "addingItem";
    this.addingPhotos = new ReactiveVar();
    this.rejectReceipt = new ReactiveVar();
    this.deferredServiceFee = new ReactiveVar();

    this.channel = new ReactiveVar(localStorage.getItem('receipt_add_channel'));
    this.referral = new ReactiveVar(localStorage.getItem('receipt_add_referral'));
    this.referralCompetitor = new ReactiveVar(localStorage.getItem('receipt_add_referral_competitor'));
    this.referralCustoms = new ReactiveVar(localStorage.getItem('receipt_add_referral_customs'));

    this.autorun(() => {
        let channel = this.channel.get();
        let referral = this.referral.get();
        let referralCompetitor = this.referralCompetitor.get();
        let referralCustoms = this.referralCustoms.get();

        localStorage.setItem('receipt_add_channel', channel || "");
        localStorage.setItem('receipt_add_referral', referral || "");
        localStorage.setItem('receipt_add_referral_competitor', referralCompetitor || "");
        localStorage.setItem('receipt_add_referral_customs', referralCustoms || "");
    });

    this.affiliateId = new ReactiveVar();
    this.shopId = new ReactiveVar(this.data.shopId);
    this.travellerId = new ReactiveVar(this.data.userId);
    this.addingShop = new ReactiveVar();
    this.receiptTypeId = new ReactiveVar();
    this.countryId = new ReactiveVar();
    this.currencySymbol = new ReactiveVar('€');
    this.poprId = new ReactiveVar();
    this.poprSecret = new ReactiveVar();

    this.autorun(() => {
        this.subscribe('receipt-types');
    });

    this.autorun(() => {
        let shopId = this.shopId.get();
        this.subscribe('shops_item', shopId || "");
        Meteor.call('get-shop', shopId, (err, shop) => {
            if (shop) {
                this.countryId.set(shop.countryId);
            }
        });
    });

    this.autorun(() => {
        let shop = Shops.findOne({_id: this.shopId.get()}, {
            fields: {
                companyId: 1,
                currencyId: 1
            }
        });
        if (shop && shop.companyId) {
            this.subscribe('companies_item', shop.companyId || "");
        }
        if (shop && shop.currencyId) {
            $('select[name="currencyId"]').val(shop.currencyId);
            $('select[name="currencyId"]').trigger('change');
        }

        if (shop) {
            let billingEntity = Vatfree.receipts.getParentForFees(shop._id);
            if (billingEntity && billingEntity.invoiceServiceFee) {
                template.$('input[name="deferredServiceFeeCheck"]').prop('checked', true);
                template.deferredServiceFee.set(true);
            }
        }
    });

    this.autorun(() => {
        let receiptTypeId = this.receiptTypeId.get();
        if (receiptTypeId) {
            if (Vatfree.receipts.isNato({receiptTypeId: receiptTypeId}) && Shops.isPartner(this.shopId.get())) {
                template.$('input[name="deferredServiceFeeCheck"]').prop('checked', true);
                template.deferredServiceFee.set(true);
                Tracker.afterFlush(function () {
                    template.$('input[name="fixedDeferredServiceFee"]').prop('checked', true);
                });
            } else {
                template.$('input[name="deferredServiceFeeCheck"]').prop('checked', false);
                template.deferredServiceFee.set(false);
            }
        }
    });

    this.autorun(() => {
        let travellerId = this.travellerId.get();
        if (travellerId) {
            Meteor.call('get-last-traveller-receipt', travellerId, (err, receipt) => {
                if (err) {
                    console.log(err);
                } else {
                    if (receipt && moment(receipt.createdAt).format('YYYYMMDD') === moment().format("YYYYMMDD")) {
                        // creating more receipt for the same traveller on the same day, pre-set customs date and country
                        template.$('input[name="customsDate"]').val(moment(receipt.customsDate).format(TAPi18n.__('_date_format')));
                        template.$('select[name="customsCountryId"]').val(receipt.customsCountryId).trigger('change');
                    }
                }
            });
        }
    });

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-receipt').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-receipt').on('hidden.bs.modal', function () {
            if (template.parentTemplate && template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-add-receipt').on('shown.bs.modal', function () {
            Vatfree.templateHelpers.initDatepicker.call(template, null, moment().format(TAPi18n.__('_date_format')));
            receiptEditFormAfterFlush.call(template);
            template.$('select[name="customsCountryId"]').select2();
        });
        $('#modal-add-receipt').modal('show');

        // delayto allow animations to finish
        Meteor.setTimeout(() => {
            try {
                if (!template.data.userId) {
                    template.$('select[name="userId"]').select2('open');
                } else if (!template.data.shopId) {
                    template.$('select[name="shopId"]').select2('open');
                } else {
                    template.$('input[name="purchaseDate"]').focus();
                }
            } catch(e) {
                console.log(e);
            }
        }, 600);
    });
});

Template.addReceiptModal.onRendered(function() {
    this.autorun(() => {
        let addingPhotos = this.addingPhotos.get();
        if (addingPhotos && addingPhotos.itemId) {
            this.subscribe('receipt-files', addingPhotos.itemId);
            Tracker.afterFlush(() => {
                Meteor.setTimeout(() => {
                    let uploadContext = {
                        itemId: addingPhotos.itemId,
                        target: addingPhotos.target
                    };
                    let qrcodeCanvas = document.getElementById('qrcode-upload-receipts');
                    Meteor.call('set-qr-upload-context', uploadContext, console.log);
                    QRCode.toCanvas(qrcodeCanvas, 'vatfree://image-upload/?context=' + JSON.stringify(uploadContext), {
                        margin: 2,
                        errorCorrectionLevel: 'Q'
                    }, (err) => {
                        if (err) {
                            toastr.error(err);
                        }
                    });
                }, 500);
            });
        }
    });

    this.autorun(() => {
        if (this.referral.get()) {
            Tracker.afterFlush(() => {
                receiptEditFormAfterFlush.call(this);
            });
        }
    });

    this.autorun(() => {
        const affiliateId = this.affiliateId.get();
        if (affiliateId) {
            this.subscribe('affiliates_item', affiliateId);
        }
    });

    this.autorun(() => {
        let user = Meteor.user() || {};
        if (user.profile && user.profile.receiptAddData) {
            // set data from receiptAddData and then remove it from the profile
            let receiptData = user.profile.receiptAddData;

            if (receiptData.shopId) {
                this.shopId.set(receiptData.shopId);
                Tracker.afterFlush(() => {
                    Meteor.call('get-shop', receiptData.shopId, (err, shop) => {
                        let shopName = shop.name + ' - ' + (shop.addressFirst || '') + ', ' + (shop.postCode || '') + ' ' + (shop.city || '');
                        let $shopId = $('select[name="shopId"]');
                        $shopId.append($("<option></option>")
                            .attr("value", receiptData.shopId)
                            .text(shopName));
                        $shopId.val(receiptData.shopId);
                        initShopIdSelect2(this);
                    });
                });
            }

            Tracker.afterFlush(() => {
                if (receiptData.poprId) {
                    this.poprId.set(receiptData.poprId);
                }
                if (receiptData.poprSecret) {
                    this.poprSecret.set(receiptData.poprSecret);
                }

                if (receiptData.amount) {
                    this.$('input[name="amount"]').val(receiptData.amount/100);
                }
                if (receiptData.amount) {
                    this.$('input[name="totalVat"]').val(receiptData.totalVat/100);
                    Meteor.setTimeout(() => {
                        this.$('input[name="totalVat"]').trigger('change');
                    }, 500);
                }

                if (receiptData.purchaseDate) {
                    this.$('input[name="purchaseDate"]').val(moment(receiptData.purchaseDate).format(TAPi18n.__('_date_format'))).trigger('change');
                    Meteor.setTimeout(() => {
                        this.$('input[name="customsDate"]').focus()
                    }, 500);
                }
            });

            Meteor.call('pre-fill-receipt-add', false);
        }
    });

    Meteor.call('set-qr-upload-context', {
        receiptId: "NEW",
        itemId: "NEW",
        target: 'receipts'
    }, console.log);
});

Template.addReceiptModal.onDestroyed(function () {
    Meteor.call('unset-qr-upload-context', console.log);
});

Template.addReceiptModal.helpers(receiptHelpers);
Template.addReceiptModal.helpers({
    addingPhotos () {
        return Template.instance().addingPhotos.get();
    },
    rejectReceipt () {
        return Template.instance().rejectReceipt.get();
    },
    deferServiceFee () {
        return Template.instance().deferredServiceFee.get();
    },
    isActiveChannel (channel) {
        return Template.instance().channel.get() === channel ? 'active' : false;
    },
    getChannel () {
        return Template.instance().channel.get() || "";
    },
    isActiveReferral (via) {
        return Template.instance().referral.get() === via ? 'active' : false;
    },
    getShopId () {
        return Template.instance().shopId.get();
    },
    getReferral () {
        return Template.instance().referral.get() || "";
    },
    getReferralCompetitor () {
        return Template.instance().referralCompetitor.get();
    },
    getReferralCustoms () {
        return Template.instance().referralCustoms.get();
    },
    affiliateId () {
        return Template.instance().affiliateId.get();
    },
    addingShop () {
        return Template.instance().addingShop.get();
    },
    getCountryId () {
        return Template.instance().countryId.get();
    },
    getReceiptTypeId () {
        return Template.instance().receiptTypeId.get();
    },
    getCurrencySymbol () {
        return Template.instance().currencySymbol.get();
    },
    getPoprId () {
        return Template.instance().poprId.get();
    },
    getPoprSecret () {
        return Template.instance().poprSecret.get();
    },
    newShopReceipt() {
        return this.archiveNumber.match('MO.?N[0-9]');
    },
    selectMapLocationCallback () {
        let template = Template.instance();
        return function (place) {
            if (place && place.address_components) {
                template.$('input[name="place"]').val(JSON.stringify(place));
            }
        };
    },
    getFiles () {
        let template = Template.instance();
        let context = template.addingPhotos.get() || {};
        return Files.find({
            itemId: context.itemId,
            target: 'receipts'
        }, {
            sort: {
                createdAt: 1
            }
        });
    },
    getDefaultSource () {
        return localStorage.getItem('receipt_add_source') || 'office';
    }
});

Template.addReceiptModal.events(receiptEvents);
Template.addReceiptModal.events({
    'change #reject-receipt' (e, template) {
        console.log($(e.currentTarget).prop('checked'));
        template.rejectReceipt.set($(e.currentTarget).prop('checked'));
    },
    'change input[name="deferredServiceFeeCheck"]' (e, template) {
        let defer = $(e.currentTarget).prop('checked');
        let vat = template.$('input[name="totalVat"]').val();
        let serviceFee = template.$('input[name="serviceFee"]').val();
        let deferredServiceFee = template.$('input[name="deferredServiceFee"]').val();
        template.deferredServiceFee.set(defer);
        Tracker.afterFlush(() => {
            if (defer) {
                // move service fee to deferred
                template.$('input[name="deferredServiceFee"]').val(serviceFee);
                template.$('input[name="serviceFee"]').val(0);
                template.$('input[name="refund"]').val(vat);
            } else {
                // move deferred to service fee
                template.$('input[name="serviceFee"]').val(deferredServiceFee);
                template.$('input[name="refund"]').val(vat - deferredServiceFee);
            }
        });
    },
    'click .add-header-buttons div.channel .icon' (e, template) {
        e.preventDefault();
        let id = $(e.currentTarget).attr('id');
        template.channel.set(id);
    },
    'click .add-header-buttons div.referral .icon' (e, template) {
        e.preventDefault();
        let id = $(e.currentTarget).attr('id');
        if (template.referral.get() === id) {
            template.referral.set();
        } else {
            template.referral.set(id);
        }
    },
    'change select[name="source"]' (e, template) {
        localStorage.setItem('receipt_add_source', $(e.currentTarget).val());
    },
    'change select[name="referralName"]' (e, template) {
        e.preventDefault();
        if (template.referral.get() === 'customs') {
            template.referralCustoms.set($(e.currentTarget).val());
        } else {
            template.referralCompetitor.set($(e.currentTarget).val());
        }
    },
    'change select[name="affiliateId"]' (e, template) {
        template.affiliateId.set($(e.currentTarget).val());
    },
    'change input[name="addShop"]' (e, template) {
        e.preventDefault();
        if ($(e.currentTarget).val()) {
            try {
                template.$('select[name="shopId"]').select2('destroy');
            } catch (e) {
                console.log(e);
            }
            template.addingShop.set(true);
            $(e.currentTarget).val("");
            Tracker.afterFlush(() => {
                template.$('#_map_addressbar').focus();
            });
        }
    },
    'click .add-new-shop' (e, template) {
        e.preventDefault();
        addNewShop(template);
    },
    'change select[name="shopId"]' (e, template) {
        template.shopId.set($(e.currentTarget).val());
    },
    'change select[name="userId"]' (e, template) {
        let travellerId = $(e.currentTarget).val();
        template.travellerId.set(travellerId);
    },
    'change select[name="receiptTypeId"]' (e, template) {
        template.receiptTypeId.set($(e.currentTarget).val());
    },
    'change select[name="currencyId"]' (e, template) {
        let currency = Currencies.findOne({
            _id: $(e.currentTarget).val()
        }) || {};
        template.currencySymbol.set(currency.symbol);
    },
    'change input[name="receiptTypeExtraFields.amount"]' (e, template) {
        let amount = template.$(e.currentTarget).val();
        template.$('input[name="amount"]').val(amount).trigger('change');
        template.$('input[name="totalVat"]').trigger('change');
    },
    'click .cancel-add-receipt' (e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'click .add-receipt-photos-done' (e, template) {
        e.preventDefault();
        const receiptId = template.addingPhotos.get().itemId;
        if (Files.find({itemId: receiptId}).count() <= 0) {
            toastr.error('No photos uploaded');
            return false;
        }

        template.hideModal();
    },
    'submit form[name="add-receipt-form"]' (e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);
        let archiveReceipt = formData['archive-receipt'] === 'on';

        // if we have a poprId, some fields have been made inactive and the getFormData function does not work
        if (formData.poprId) {
            formData.purchaseDate = template.$('input[name="purchaseDate"]').val();
            formData.amount = template.$('input[name="amount"]').val();
            formData.totalVat = template.$('input[name="totalVat"]').val();
            formData.shopId = template.$('select[name="shopId"]').val();
        }

        // fix dates
        if (formData.purchaseDate) {
            formData.purchaseDate = moment(formData.purchaseDate, TAPi18n.__('_date_format')).toDate();
        }
        if (formData.customsDate) {
            formData.customsDate = moment(formData.customsDate, TAPi18n.__('_date_format')).toDate();
        }
        // fix numbers
        formData.amount = Math.round(Number(formData.amount) * 100);
        formData.totalVat = Math.round(Number(formData.totalVat) * 100);
        formData.serviceFee = Math.round(Number(formData.serviceFee) * 100);
        formData.refund = Math.round(Number(formData.refund) * 100);

        if (formData.deferredServiceFee) {
            formData.serviceFee = 0;
            formData.refund = Number(template.$('input[name="refund"]').val()) * 100;
            formData.deferredServiceFee = Math.round(Number(formData.deferredServiceFee) * 100);
        }
        if (formData.fixedDeferredServiceFee === 'on') {
            formData.fixedDeferredServiceFee = true;
        } else {
            delete formData.fixedDeferredServiceFee;
        }

        formData.countryId = template.countryId.get();

        if (_.has(formData, 'vat')) {
            // vat rates of for instance 2.1 will becomde {vat: {2: {1: ....}}}
            // this is the most central place to fix this
            let newFormDataVat = {};
            _.each(formData.vat, (vatValue, vatKey) => {
                if (_.isObject(vatValue)) {
                    _.each(vatValue, (vatValue2, vatKey2) => {
                        newFormDataVat['' + vatKey + '_' + vatKey2] = Number(vatValue2);
                    });
                } else {
                    newFormDataVat[vatKey] = Number(vatValue);
                }
            });
            formData.vat = newFormDataVat;
        }

        if (formData['reject-receipt'] === 'on') {
            formData.status = 'rejected';
            let receiptRejectionIds = [];
            _.each(formData.receiptRejectionIds, (includeReceiptRejection, receiptRejectionId) => {
                if (includeReceiptRejection === 'on') {
                    receiptRejectionIds.push(receiptRejectionId);
                }
            });
            formData.receiptRejectionIds = receiptRejectionIds;
            if (formData.receiptRejectionIds.length === 0) {
                toastr.error('Select 1 or more reasons for the receipt rejection');
                return false;
            }
        } else {
            let minimumReceiptAmount = Vatfree.receipts.getMinimumAmount(formData);
            if (formData.amount < minimumReceiptAmount) {
                toastr.error(Vatfree.translate('Receipt amount is below minimum required by country (__minimum_amount__)', { minimum_amount: Vatfree.numbers.formatCurrency(minimumReceiptAmount) } ));
                return false;
            }

            // Checks
            if (!formData.purchaseDate || !moment(formData.purchaseDate).isValid()) {
                toastr.error('Purchase date is required');
                return false;
            }

            // Checks
            if (moment(formData.customsDate).isBefore(moment(formData.purchaseDate))) {
                toastr.error('Customs stamp date cannot be before purchase date');
                return false;
            }

            let vat = 0;
            _.each(formData.vat, (vatValue, vatKey) => {
                formData.vat[vatKey] = Math.round(Number(vatValue) * 100);
                vat += formData.vat[vatKey];
            });
            if (vat < formData.totalVat) {
                toastr.error('Vat rules do not add up to total VAT amount');
                return false;
            }

            formData.status = 'userPending';
        }

        Meteor.call('insert-receipt', formData, (err, receiptId) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                if (archiveReceipt) {
                    Meteor.call('archive-receipt', receiptId, localStorage.getItem('physical-location'), (err, archiveNumber) => {
                        if (err) {
                            toastr.error(err.reason || err.message);
                        } else {
                            let uploadContext = {
                                receiptId: receiptId,
                                itemId: receiptId,
                                target: 'receipts',
                                archiveNumber: archiveNumber
                            };
                            template.addingPhotos.set(uploadContext);
                        }
                    });
                } else {
                    let uploadContext = {
                        receiptId: receiptId,
                        itemId: receiptId,
                        target: 'receipts'
                    };
                    template.addingPhotos.set(uploadContext);
                }
            }
        });
    },
    'click .dropzone-layout .add-file': function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('input[name="dropzone-add-file"]').trigger('click');
    },
    'change input[name="dropzone-add-file"]': function (e, template) {
        e.preventDefault();
        e.stopPropagation();
        let context = template.addingPhotos.get();
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, context);
        });
    },
    'dropped .dropzone-layout.dropzone-add-file-div': function (e, template) {
        e.preventDefault();
        e.stopPropagation();
        let context = template.addingPhotos.get();
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, context);
        });
    }
});

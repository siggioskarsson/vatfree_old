export const handleOnlyIdInput = function (result, receipt) {
    Meteor.call('search-popr-receipts', result, (err, poprReceipts) => {
        if (err) {
            toastr.error(err.reason || err.message);
        } else {
            const inputOptions = {};
            _.each(poprReceipts, (poprReceipt) => {
                inputOptions[poprReceipt._id] = poprReceipt._id + ': ' + Vatfree.numbers.formatCurrency(poprReceipt.totalValue) + ', BTW ' + Vatfree.numbers.formatCurrency(poprReceipt.totalVAT);
            });

            import swal from 'sweetalert2';
            swal({
                title: 'Looks like only a POPr ID',
                text: 'Select the correct receipt from the list below.',
                input: 'select',
                inputOptions,
                showCancelButton: true,
                reverseButtons: true,
                confirmButtonText: 'Register POPr receipt'
            }).then((result) => {
                Meteor.call('claim-popr-receipt', receipt._id, result, 'admin', (err, result) => {
                    if (err) {
                        toastr.error(err.reason || err.message);
                    }
                });
            });
        }
    });
};

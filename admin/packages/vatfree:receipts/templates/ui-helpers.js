UI.registerHelper('getReceiptIcon', function(status) {
    status = status || this.status;

    let icon = Vatfree.receipts.icons();
    return _.has(icon, status) ? icon[status] : "circle-o";
});

UI.registerHelper('getSourceIcon', function(source) {
    source = source || this.source;

    let icon = Vatfree.receipts.sourceIcons();
    return _.has(icon, source) ? icon[source] : "circle-o";
});

UI.registerHelper('getSourceDescription', function(source) {
    source = source || this.source;

    return Vatfree.translate('source_' + source);
});

UI.registerHelper('getChannelIcon', function(channel) {
    channel = channel || this.channel;

    let icon = Vatfree.receipts.channelIcons();
    return _.has(icon, channel) ? icon[channel] : "circle-o";
});

UI.registerHelper('getChannelDescription', function(channel) {
    channel = channel || this.channel;

    return Vatfree.translate('channel_' + channel);
});
UI.registerHelper('getReferralDescription', function(source) {
    source = source || this.source;

    return Vatfree.translate('referral_' + source);
});

UI.registerHelper('getSourceOptions', function() {
    return _.clone(Receipts.simpleSchema()._schema.source.allowedValues);
});
UI.registerHelper('getChannelOptions', function() {
    return _.clone(Receipts.simpleSchema()._schema.channel.allowedValues);
});
UI.registerHelper('getReferralOptions', function() {
    return _.clone(Receipts.simpleSchema()._schema.referral.allowedValues);
});

UI.registerHelper('getReceiptStatusOptions', function() {
    return _.clone(Receipts.simpleSchema()._schema.status.allowedValues);
});
UI.registerHelper('getReceiptTypeOptions', function() {
    return ReceiptTypes.find({},{
        sort: {
            name: 1
        }
    });
});

UI.registerHelper('getReceiptName', function(receiptId) {
    receiptId = receiptId || this.receiptId;
    let receipt = Receipts.findOne({_id: receiptId});
    if (receipt) {
        return receipt.receiptNr;
    }
});

UI.registerHelper('uploadFileIsImage', function() {
    return this.file.type.match(/image/);
});

UI.registerHelper('uploadFileExtension', function() {
    // we only support pdf ...
    if (this.file.type.match(/pdf/)) {
        return "pdf";
    } else {
        return "_blank";
    }
});

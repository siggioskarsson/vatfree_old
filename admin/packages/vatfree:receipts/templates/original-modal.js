import swal from 'sweetalert2';
import QRCode from 'qrcode';
global.Buffer = global.Buffer || require("buffer").Buffer;

Template.receiptOriginalModal.onCreated(function() {
    this.qrCode = new ReactiveVar();

    this.subscribe('receipt-files', this.data.receiptId);
});

Template.receiptOriginalModal.onRendered(function() {
    let template = this;
    let uploadContext = {
        receiptId: this.data.receiptId,
        itemId: this.data.receiptId,
        target: 'receipts'
    };
    Meteor.call('set-qr-upload-context', uploadContext, console.log);

    QRCode.toDataURL('vatfree://image-upload/?context=' + JSON.stringify(uploadContext), {
        margin: 2,
        errorCorrectionLevel: 'Q'
    }, (err, qrDataUrl) => {
        template.qrCode.set(qrDataUrl);
    });
});

Template.receiptOriginalModal.onDestroyed(function () {
    Meteor.call('unset-qr-upload-context', console.log);
});

Template.receiptOriginalModal.helpers({
    getReceipt() {
        return Receipts.findOne({
            _id: this.receiptId
        })
    },
    shopName() {
        let shop = Shops.findOne({_id: this.shopId}) || {};
        let country = Countries.findOne({_id: shop.countryId}) || {};
        return shop.name + "<br/>\n" + shop.addressFirst + ', ' + shop.postCode + ' ' + shop.city + ', ' + country.name;
    },
    getFiles() {
        return Files.find({
            itemId: this.receiptId,
            target: 'receipts'
        });
    },
    qrCode() {
        return Template.instance().qrCode.get();
    }
});

Template.receiptOriginalModal.events({
    'click .receipt-go'(e ,template) {
        e.preventDefault();
        FlowRouter.go('/receipt/' + this.receiptId);
    },
    'click .archive-receipt-do'(e, template) {
        e.preventDefault();
        let parentTemplate = template.parentTemplate();

        Meteor.call('archive-receipt', this.receiptId, localStorage.getItem('physical-location'), (err, archiveNumber) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                parentTemplate.originalReceiptIn.set();
                parentTemplate.originalReceiptId.set();
                swal("Archived!", "Receipt archive number is: " + archiveNumber, "success");
            }
        });
    },
    'click .dropzone-modal > img': function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('input[name="dropzone-file"]').trigger('click');
    },
    'change input[name="dropzone-file"]': function(e, template) {
        e.preventDefault();
        e.stopPropagation();
        let context = {
            itemId: this.receiptId,
            receiptId: this.receiptId,
            target: 'receipts'
        };
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, context);
        });
    },
    'dropped .dropzone-modal': function (e, template) {
        e.preventDefault();
        e.stopPropagation();
        let context = {
            receiptId: this.receiptId,
            itemId: this.receiptId,
            target: 'receipts'
        };
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, context);
        });
    }
});

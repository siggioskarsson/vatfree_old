var FileSaver = require('file-saver');
import moment from 'moment';
import { receiptHelpers } from './helpers';

Template.receipts.onCreated(Vatfree.templateHelpers.onCreated(Receipts, '/receipts/', '/receipt/:itemId'));
Template.receipts.onRendered(Vatfree.templateHelpers.onRendered());
Template.receipts.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.receipts.helpers(Vatfree.templateHelpers.helpers);
Template.receipts.helpers(receiptHelpers);
Template.receipts.helpers({
    getReceiptStats() {
        let template = Template.instance();
        let selector = receiptHelpers.getReceiptSelector(template);

        let stats = ReactiveMethod.call('get-receipt-stats', selector);
        return stats && stats[0] ? stats[0] : false;
    }
});
Template.receipts.events(Vatfree.templateHelpers.events());

Template.receipts.onCreated(function() {
    this.receiptTasks = new ReactiveVar();
    this.receiptShopTasks = new ReactiveVar();
    this.taskReceiptId = new ReactiveVar();

    let sort = this.sort.get();
    if (!sort || _.has(sort, 'name')) {
        // reset the sort if sorting on name, is a remnant from the past
        this.sort.set({
            createdAt: -1
        });
    }

    this.todoStatus = [
        'visualValidationPending',
        'waitingForDoubleCheck'
    ];

    this.sourceFilter = new ReactiveArray();
    this.typeFilter = new ReactiveArray();
    this.poprFilter = new ReactiveVar(Session.get(this.view.name + '.poprFilter'));
    this.salesforceFilter = new ReactiveVar(Session.get(this.view.name + '.salesforceFilter'));

    _.each(Session.get(this.view.name + '.sourceFilter') || [], (status) => {
        this.sourceFilter.push(status);
    });
    _.each(Session.get(this.view.name + '.typeFilter') || [], (status) => {
        this.typeFilter.push(status);
    });
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.sourceFilter', this.sourceFilter.array());
        Session.setPersistent(this.view.name + '.typeFilter', this.typeFilter.array());
        Session.setPersistent(this.view.name + '.poprFilter', this.poprFilter.get());
        Session.setPersistent(this.view.name + '.salesforceFilter', this.salesforceFilter.get());
    });

    this.selectorFunction = (selector) => {
        let sourceFilter = this.sourceFilter.array() || [];
        if (sourceFilter.length > 0) {
            selector.source = {
                $in: sourceFilter
            };
        }

        let typeFilter = this.typeFilter.array() || [];
        if (typeFilter.length > 0) {
            selector.receiptTypeId = {
                $in: typeFilter
            };
        }

        let poprFilter = this.poprFilter.get();
        if (poprFilter) {
            selector.poprId = {
                $exists: true
            }
        }
        let salesforceFilter = this.salesforceFilter.get();
        if (salesforceFilter) {
            if (salesforceFilter === 'active') {
                selector.salesforceId = {
                    $exists: true
                }
            }
            if (salesforceFilter === 'inactive') {
                selector.salesforceId = {
                    $exists: false
                }
            }
        }

        if (_.has(selector, 'statusDates.paidByShop')) {
            selector['$or'] = [
                {
                    'statusDates.userPendingForPayment': selector['statusDates.paidByShop']
                },
                {
                    'statusDates.paidByShop': selector['statusDates.paidByShop']
                },
                {
                    'statusDates.paid': selector['statusDates.paidByShop']
                },
                {
                    $and: [
                        {
                            'statusDates.userPendingForPayment': {
                                $exists: false
                            }
                        },
                        {
                            'statusDates.paidByShop': {
                                $exists: false
                            }
                        },
                        {
                            'statusDates.paid': {
                                $exists: false
                            }
                        }
                    ]
                }
            ];
            delete selector['statusDates.paidByShop'];
        }
    };
});

Template.receipts.onRendered(function() {
    this.autorun(() => {
        this.subscribe('receipt-stats');
        this.subscribe('receipt-rejections-list');
        this.subscribe('receipt-types');
    });

    this.autorun(() => {
        if (FlowRouter.getQueryParam('tasks')) {
            Tracker.afterFlush(() => {
                let receiptId = FlowRouter.getQueryParam('receiptId');
                if (receiptId) {
                    this.taskReceiptId.set(receiptId);
                }
                this.receiptTasks.set(true);
            });
        }
    });
});

Template.receipts.helpers({
    receiptTasks() {
        return Template.instance().receiptTasks.get();
    },
    receiptShopTasks() {
        return Template.instance().receiptShopTasks.get();
    },
    getTaskReceiptId() {
        return Template.instance().taskReceiptId.get();
    },
    isActiveSourceFilter() {
        let sourceFilter = Template.instance().sourceFilter.array() || [];
        return _.contains(sourceFilter, this.toString()) ? 'active' : '';
    },
    isActiveReceiptTypeFilter() {
        let typeFilter = Template.instance().typeFilter.array() || [];
        return _.contains(typeFilter, this._id) ? 'active' : '';
    },
    isActivePoprFilter() {
        return Template.instance().poprFilter.get();
    },
    isActiveSalesforceFilter() {
        return Template.instance().salesforceFilter.get() === 'active';
    },
    isInactiveSalesforceFilter() {
        return Template.instance().salesforceFilter.get() === 'inactive';
    }
});

Template.receipts.events({
    'click .check-partner-receipts'(e, template) {
        e.preventDefault();
        let selector = receiptHelpers.getReceiptSelector(template);
        Vatfree.templateHelpers.openModal('receipts_modal_partner_receipts', {fullscreen: true, selector: selector});
    },
    'click .check-pledger-receipts'(e, template) {
        e.preventDefault();
        let selector = receiptHelpers.getReceiptSelector(template);
        Vatfree.templateHelpers.openModal('receipts_modal_partner_receipts', {fullscreen: true, partnershipStatus: 'pledger', selector: selector});
    },
    'click .check-admin-receipts'(e, template) {
        e.preventDefault();
        Vatfree.templateHelpers.openModal('receipts_modal_admin_receipts', {fullscreen: true});
    },
    'click .receipt-tasks'(e, template) {
        e.preventDefault();
        template.taskReceiptId.set();
        template.receiptTasks.set(true);
    },
    'click .receipt-shop-tasks'(e, template) {
        e.preventDefault();
        template.taskReceiptId.set();
        template.receiptShopTasks.set(true);
    },
    'click .task-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if ($(e.currentTarget).attr('disabled') !== 'disabled') {
            template.taskReceiptId.set(this._id);
            template.receiptTasks.set(true);
        }
    },
    'click .filter-icon.source-filter-icon'(e, template) {
        e.preventDefault();
        let source = this.toString();
        let sourceFilter = template.sourceFilter.array();

        if (_.contains(sourceFilter, source)) {
            template.sourceFilter.clear();
            _.each(sourceFilter, (_source) => {
                if (source !== _source) {
                    template.sourceFilter.push(_source);
                }
            });
        } else {
            template.sourceFilter.push(source);
        }
    },
    'click .filter-icon.receipt-type-filter-icon'(e, template) {
        e.preventDefault();
        let type = this._id;
        let typeFilter = template.typeFilter.array();

        if (_.contains(typeFilter, type)) {
            template.typeFilter.clear();
            _.each(typeFilter, (_type) => {
                if (type !== _type) {
                    template.typeFilter.push(_type);
                }
            });
        } else {
            template.typeFilter.push(type);
        }
    },
    'click .filter-icon.popr-filter-icon'(e, template) {
        e.preventDefault();
        template.poprFilter.set(!template.poprFilter.get());
    },
    'click .filter-icon.salesforce-filter-icon'(e, template) {
        e.preventDefault();
        const salesforceFilter = template.salesforceFilter.get();
        if (salesforceFilter === 'active') {
            template.salesforceFilter.set('inactive');
        } else if (salesforceFilter === 'inactive') {
            template.salesforceFilter.set(false);
        } else {
            template.salesforceFilter.set('active');
        }
    },
    'click .receipts-export'(e, template) {
        e.preventDefault();
        template.runningExport.set(true);

        Meteor.call('receipts-export', template.searchTerm.get(), template.statusFilter.array(), template.listFilters.get(), template.typeFilter.array(), template.poprFilter.get(), (err, csv) => {
            var blob = new Blob([csv], {
                type: "text/plain;charset=utf-8"
            });
            FileSaver.saveAs(blob, "receipts_export_" + moment().format(TAPi18n.__('_date_time_format')) + ".csv");
            template.runningExport.set(false);
        });
    },
    'click .receipts-send-reminders'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).attr('disabled') !== "disabled") {
            if (confirm('Do you want to send all receipt reminders?')) {
                Meteor.call('send-receipt-reminders', (err, count) => {
                    if (err) {
                        alert(err.reason || err.message);
                    } else {
                        import swal from 'sweetalert2';
                        swal("Sending reminders", "Receipt reminder sending has been started in the background.", "success");
                    }
                });
            }
        }
    }
});

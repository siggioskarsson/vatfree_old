import moment from 'moment';
import { receiptHelpers } from './helpers';

Template.receiptTasksModal_form.onCreated(function() {
    this.autorun(() => {
        this.subscribe('receipt-types');
    });
});

Template.receiptTasksModal_form.onRendered(function() {
    this.autorun(() => {
        let date = Template.currentData();
        Tracker.afterFlush(() => {
            receiptEditFormAfterFlush.call(this);
            this.$('select[name="customsCountryId"]').select2();

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square icheckbox_square-blue',
                radioClass: 'iradio_square iradio_square-blue'
            });
        });
    });

    this.autorun(() => {
        const receiptId = this.data._id;
        Meteor.call('set-qr-upload-context', {
            itemId: receiptId,
            receiptId: receiptId,
            target: 'receipts'
        }, console.log);
    });
});

Template.receiptTasksModal_form.onDestroyed(function() {
    Meteor.call('unset-qr-upload-context', console.log);
});

Template.receiptTasksModal_form.helpers(receiptHelpers);
Template.receiptTasksModal_form.helpers({
    selectedReceiptType() {
        return ReceiptTypes.findOne({_id: this.receiptTypeId});
    },
    isNatoReceipt() {
        let receiptType = ReceiptTypes.findOne({_id: this.receiptTypeId});
        if (receiptType) {
            return receiptType.name === 'NATO';
        }
    },
    getCheckRemarks() {
        if (this.checkRemarks) {
            let remarks = _.clone(this.checkRemarks);
            remarks.reverse();
            return remarks;
        }
    },
    isExternallyDisabled() {
        return !!this.poprId;
    },
    similarReceipts() {
        return ReactiveMethod.call('get-similar-receipts', this._id);
    },
    getReceiptFiles() {
        return Files.find({
            target: 'receipts',
            itemId: this._id
        });
    }
});

Template.receiptTasksModal.events(receiptEvents);
Template.receiptTasksModal_form.events({
    'change input[name^="vat."]'(e, template) {
        let name = $(e.currentTarget).attr('name');

        let totalVat = 0;
        template.$('input[name^="vat."]').each(function() {
            totalVat += Number($(this).val());
        });
        template.$('input[name="totalVat"]').val(totalVat);

        totalVat = Math.round(totalVat*100);

        Tracker.afterFlush(() => {
            let shopId = template.data.shopId;
            let fees = Vatfree.receipts.getFees(shopId, template.data);

            let fee = Vatfree.vat.calculateFee(totalVat, fees.serviceFee, fees.minFee, fees.maxFee) / 100;
            template.$('input[name="serviceFee"]').val(fee);

            let refund = Vatfree.vat.calculateRefund(totalVat, fees.serviceFee, fees.minFee, fees.maxFee) / 100;
            template.$('input[name="refund"]').val(refund);
        });
    },
    'change input, change select'(e, template) {
        let name = $(e.currentTarget).attr('name');
        let value = $(e.currentTarget).val();

        if ($(e.currentTarget).attr('type') === 'number' || $(e.currentTarget).attr('name') === 'totalVat') {
            value = Math.round(Number(value) * 100);
        } else if ($(e.currentTarget).parent().hasClass('date')) {
            value = moment(value, TAPi18n.__('_date_format')).toDate();
        }

        // Checks
        if (name === "customsDate" || name === "purchaseDate") {
            let customsDate = name === "customsDate" ? moment(value, TAPi18n.__('_date_format')) : moment($('input[name="customsDate"]').val(), TAPi18n.__('_date_format'));
            let purchaseDate = name === "purchaseDate" ? moment(value, TAPi18n.__('_date_format')) : moment($('input[name="purchaseDate"]').val(), TAPi18n.__('_date_format'));
            if (customsDate.isBefore(purchaseDate)) {
                toastr.error('Customs stamp date cannot be before purchase date');
                Tracker.afterFlush(() => {
                    if (name === "customsDate") {
                        $('input[name="customsDate"]').val(moment(this.customsDate).format(TAPi18n.__('_date_format')));
                    } else {
                        $('input[name="purchaseDate"]').val(moment(this.purchaseDate).format(TAPi18n.__('_date_format')));
                    }
                });
                return false;
            }
        }
    },
    'change textarea[name="checkRemark"]'(e, template) {
        e.preventDefault();
        e.stopPropagation();

        Receipts.update({
            _id: this._id
        },{
            $addToSet: {
                checkRemarks: {
                    userId: Meteor.userId(),
                    name: Meteor.user().profile.name,
                    date: new Date(),
                    remark: $(e.currentTarget).val()
                }
            }
        });
        $(e.currentTarget).val('');
    },
    'click .change-type'(e, template) {
        e.preventDefault();

        const receipt = this;
        const selectOptions = [
            {
                text: 'Normal',
                value: false,
                selected: false
            }
        ];
        ReceiptTypes.find({}).forEach((receiptType) => {
            selectOptions.push({
                text: receiptType.name,
                value: receiptType._id,
                selected: receipt.receiptTypeId === receiptType._id
            });
        });

        import swal from 'sweetalert';
        import 'swal-forms';
        swal.withForm({
            title: 'Change type?',
            text: 'Select the new type of the receipt',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Change type!',
            closeOnConfirm: true,
            formFields: [
                {
                    id: 'receiptTypeId',
                    placeholder: 'Type',
                    type: 'select',
                    options: selectOptions
                }
            ]
        }, function (isConfirm) {
            if (isConfirm) {
                Receipts.update({
                    _id: receipt._id
                }, {
                    $set: {
                        receiptTypeId: this.swalForm.receiptTypeId
                    }
                });
            }
        });
    }
});

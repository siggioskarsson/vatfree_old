var checkForNewReceiptShop = function() {
    if (this.debug) console.log('checking for new receipt');
    if (this.view && this.view.isDestroyed === true) return;

    Meteor.call('getNextShopReceiptTask', (err, receiptId) => {
        if (this.debug) console.log('getNextShopReceiptTask', err, receiptId);
        if (err) {
            toastr.error(err.reason);
        } else {
            if (receiptId) {
                this.receiptId.set(receiptId);
            } else {
                Meteor.setTimeout(() => {
                    checkForNewReceiptShop.call(this);
                }, 5000);
            }
        }
    });
};

Template.receiptShopTasksModal.onCreated(function() {
    this.updatingReceipt = new ReactiveVar(false);
    this.receiptId = new ReactiveVar(this.data.receiptId);
    this.selectedFile = new ReactiveVar();
    this.addingShop = new ReactiveVar();
    this.isRejecting = new ReactiveVar();
    this.changedValues = new ReactiveVar();
    this.lockId = false;
    this.lock = false;
    this.debug = true;

    this.subscribe('receipt-rejections-list');

    this.autorun(() => {
        const receiptId = this.receiptId.get();
        if (receiptId) {
            this.subscribe('receipts_item', receiptId);
            this.subscribe('receipt-files', receiptId);
            this.lockId = 'receipts_shop_' + receiptId;

            Tracker.nonreactive(() => {
                if (this.lock) {
                    if (this.debug) console.log('releasing lock');
                    this.lock.release();
                }
                if (this.debug) console.log('creating lock', this.lockId);
                this.lock = new Lock(this.lockId, Meteor.userId());
            });
        } else {
            if (this.lockId && this.lock) {
                this.lock.release();
            }
            checkForNewReceiptShop.call(this);
        }
    });

    this.autorun(() => {
        if (this.receiptId.get()) {
            this.changedValues.set();
            this.addingShop.set();
            this.isRejecting.set();
            let file = Files.findOne({
                itemId: this.receiptId.get(),
                target: 'receipts'
            },{
                sort: {
                    createdAt: 1
                }
            });
            Tracker.nonreactive(() => {
                if (!this.selectedFile.get()) {
                    if (file) {
                        this.selectedFile.set(file._id);
                        Tracker.afterFlush(() => {
                            Meteor.setTimeout(() => {
                                // skip all the jQuery stuff
                                let zoomImage = document.querySelector('.receipt-zoom');
                                if (zoomImage) {
                                    zoomImage.dispatchEvent(new CustomEvent('wheelzoom.destroy'));
                                    wheelzoom(zoomImage);
                                }
                            }, 600);
                        });
                    }
                }
            });
        }
    });

    this.autorun(() => {
        if (!this.receiptId.get()) {
            // get next receiptId
            if (this.debug) console.log('no receipt, getting next');
            Meteor.call('getNextShopReceiptTask', (err, receiptId) => {
                if (this.debug) console.log('getNextShopReceiptTask', err, receiptId);
                if (err) {
                    toastr.error(err.reason);
                } else {
                    if (receiptId) {
                        this.receiptId.set(receiptId);
                    }
                }
            });
        }
    });

    this.autorun(() => {
        if (this.receiptId.get()) {
            let receipt = Receipts.findOne({_id: this.receiptId.get()}) || {};
            let shop = Shops.findOne({_id: receipt.shopId});
            if (shop && shop.companyId) {
                this.subscribe('companies_item', shop.companyId || "");
            }
        }
    });

    let template = this;
    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-receipt-shop-tasks').modal('hide');
    };

    Tracker.afterFlush(() => {
        this.$('#modal-receipt-shop-tasks').on('hidden.bs.modal', function () {
            template.parentTemplate.receiptShopTasks.set(false);
            template.receiptId.set();
            template.selectedFile.set();
        });
        this.$('#modal-receipt-shop-tasks').on('shown.bs.modal', function () {
            receiptEditFormAfterFlush();
        });
        this.$('#modal-receipt-shop-tasks').modal('show');

        $(window).resize(() => {
            let zoomImage = document.querySelector('.receipt-zoom');
            if (zoomImage) {
                zoomImage.dispatchEvent(new CustomEvent('wheelzoom.destroy'));
                wheelzoom(zoomImage);
            }
        });

        this.keyHandler = (e) => {
            let selectedFile = this.$('li.file.active').attr('id');
            if (e.keyCode === 38) {
                // up
                e.stopPropagation();
                let prev = $('#'+selectedFile).prev();
                if (prev.length > 0) {
                    this.selectedFile.set(prev.attr('id').replace('file_', ''));
                }
            } else if (e.keyCode === 40) {
                // down
                e.stopPropagation();
                let next = $('#'+selectedFile).next();
                if (next.length > 0) {
                    this.selectedFile.set(next.attr('id').replace('file_', ''));
                }
            }
        };
        $(document).on('keyup', null, this.keyHandler);
        Meteor.setTimeout(() => {
            this.$('#modal-receipt-shop-tasks').focus();
        },100);
    });
});

Template.receiptShopTasksModal.onDestroyed(function() {
    if (this.lockId && this.lock) {
        this.lock.release();
    }

    $(document).off('keyup', null, this.keyHandler);

    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.receiptShopTasksModal.helpers({
    getReceipt() {
        return Receipts.findOne({
            _id: Template.instance().receiptId.get()
        });
    },
    changedValues() {
        return Template.instance().changedValues.get();
    },
    addingShop() {
        return Template.instance().addingShop.get();
    },
    isRejecting() {
        return Template.instance().isRejecting.get();
    },
    isLocked() {
        let template = Template.instance();
        return !template.lock.isMine(Meteor.userId());
    },
    getFiles() {
        let receiptId = Template.instance().receiptId.get();
        return Files.find({
            itemId: receiptId,
            target: 'receipts'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    isActiveFile(id) {
        let template = Template.instance();
        let fileId = template.selectedFile.get();
        return fileId === id ? 'active' : false;
    },
    selectedFile() {
        let template = Template.instance();
        let receiptId = template.receiptId.get();
        let fileId = template.selectedFile.get();
        if (receiptId && fileId) {
            return Files.findOne({
                _id: fileId,
                itemId: receiptId,
                target: 'receipts'
            });
        }
    }
});

Template.receiptShopTasksModal.events({
    'change input[name="addShop"]'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).val()) {
            try {
                $('select').select2('destroy');
            } catch(e) {}
            template.addingShop.set(true);
            $(e.currentTarget).val("");
        }
    },
    'click .create-shop'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (_.has(formData, 'geo') && formData.geo) {
            formData.geo = JSON.parse(formData.geo);
        }

        Shops.insert(formData, (err, shopId) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                Receipts.update({
                    _id: template.receiptId.get()
                },{
                    $set: {
                        shopId: shopId
                    }
                });
                template.receiptId.set();
            }
        });
    },
    'click .cancel-add-shop'(e ,template) {
        e.preventDefault();
        template.addingShop.set();
    },
    'change select[name="shopId"]'(e, template) {
        e.preventDefault();
        Receipts.update({
            _id: template.receiptId.get()
        },{
            $set: {
                shopId: $(e.currentTarget).val()
            }
        });
        template.receiptId.set();
    },
    'click .file'(e, template) {
        e.preventDefault();
        template.selectedFile.set();

        Tracker.afterFlush(() => {
            template.selectedFile.set(this._id);
            Meteor.setTimeout(() => {
                // skip all the jQuery stuff
                let zoomImage = document.querySelector('.receipt-zoom');
                if (zoomImage) {
                    zoomImage.dispatchEvent(new CustomEvent('wheelzoom.destroy'));
                    wheelzoom(zoomImage);
                }
            }, 600);
        });
    },
    'click .receipt-reject'(e, template) {
        e.preventDefault();
        // show receipt rejection reasons
        template.isRejecting.set(true);
        Tracker.afterFlush(() => {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square icheckbox_square-blue',
                radioClass: 'iradio_square iradio_square-blue'
            });
        });
    },
    'click .receipt-reject-cancel'(e, template) {
        e.preventDefault();
        template.isRejecting.set();
    },
    'click .receipt-reject-do'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        let receiptRejectionIds = [];
        _.each(formData.receiptRejectionIds, (includeReceiptRejection, receiptRejectionId) => {
            if (includeReceiptRejection === 'on') {
                receiptRejectionIds.push(receiptRejectionId);
            }
        });

        if (receiptRejectionIds.length <= 0) {
            toastr.error('Select 1 or more reasons for the rejection');
            return false;
        }

        const sendTravellerUpdate = formData.sendTravellerUpdate === 'on';
        Meteor.call('receipt-reject', template.receiptId.get(), receiptRejectionIds, sendTravellerUpdate, (err, result) => {
            if (err) {
                console.log(err);
                toastr.error(err.reason);
            } else {
                template.receiptId.set();
                template.selectedFile.set();
                template.addingShop.set();
                template.isRejecting.set();
            }
        });
    },
    'click .close-tasks'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'click .create-shop, submit form[name="add-shop-form"]'(e, template) {
        e.preventDefault();
        let self = this;
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (_.has(formData, 'geo') && formData.geo) {
            formData.geo = JSON.parse(formData.geo);
        }

        Shops.insert(formData, (err, shopId) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                Receipts.update({
                    _id: self._id
                },{
                    $set: {
                        shopId: shopId
                    }
                });
                template.receiptId.set();
                template.addingShop.set();
            }
        });
    }
});

Template.receiptShopTasksModal_shopForm.onRendered(function() {
    Tracker.afterFlush(() => {
        receiptEditFormAfterFlush();
    });
});

Template.similarReceiptsTr.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        const receiptIds = _.map(data.receipts, (receipt) => {
            return receipt._id;
        });
        _.each(receiptIds, (receiptId) => {
            this.subscribe('receipt-files', receiptId);
        });
    });
});

Template.similarReceiptsTr.helpers({
    getReceiptFiles() {
        return Files.find({
            target: 'receipts',
            itemId: this._id
        });
    }
});

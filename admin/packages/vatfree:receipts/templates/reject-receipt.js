import { receiptHelpers } from './helpers';

Template.receiptRejectModal.onCreated(function () {
    let template = this;
    let parentVariable = this.data.parentVariable || 'rejectingReceipt';
    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-reject-receipt').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-reject-receipt').on('hidden.bs.modal', function () {
            if (template.parentTemplate && template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-reject-receipt').on('shown.bs.modal', function () {
        });
        $('#modal-reject-receipt').modal('show');
    });
});

Template.receiptRejectModal.helpers(receiptHelpers);
Template.receiptRejectModal.events({
    'click .cancel-reject-receipt' (e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="reject-receipt-form"]' (e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        let sendTravellerUpdate = formData.sendTravellerUpdate === 'on';
        let receiptRejectionIds = [];
        _.each(formData.receiptRejectionIds, function (receiptRejection, key) {
            if (receiptRejection === 'on') {
                receiptRejectionIds.push(key);
            }
        });
        console.log(this, sendTravellerUpdate, receiptRejectionIds);

        if (sendTravellerUpdate) {
            Receipts.update({
                _id: this._id
            },{
                $set: {
                    status: "rejected",
                    receiptRejectionIds: receiptRejectionIds
                }
            });
            template.hideModal();
        } else {
            Meteor.call('reject-receipt', this._id, receiptRejectionIds, sendTravellerUpdate, (err) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    template.hideModal();
                }
            });
        }
    }
});

import moment from 'moment';
import { receiptHelpers } from './helpers';

Template.receiptInfo.onCreated(function() {
    this.editingTraveller = new ReactiveVar();
    this.editingShop = new ReactiveVar();
    this.addingShop = new ReactiveVar();
    this.editingAmounts = new ReactiveVar();
    this.showFile = new ReactiveVar();
});

Template.receiptInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.receiptInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;

        if (data && data.receipt && data.receipt._id) {
            this.subscribe('receipts_item', data.receipt._id);
            this.subscribe('receipt-files', data.receipt._id);

            this.subscribe('activity-stream', {receiptId: data.receipt._id}, null, limit, offset);
            this.subscribe('activity-stream', {}, data.receipt._id, limit, offset);
        }

        Tracker.afterFlush(() => {
            receiptEditFormAfterFlush();
        });
    });

    this.autorun(() => {
        let data = Template.currentData();
        let shop = Shops.findOne({_id: data.receipt.shopId });
        if (shop) {
            this.subscribe('companies_item', shop.companyId || "");
        }
    });
});

Template.receiptInfo.helpers(receiptHelpers);
Template.receiptInfo.helpers({
    getShowFile() {
        return Template.instance().showFile.get();
    },
    addingShop() {
        return Template.instance().addingShop.get();
    },
    getActivity() {
        return ActivityStream.collection.find({
            $or: [
                {
                    'securityContext.receiptId': this._id
                },
                {
                    'activity.target.itemId': this._id
                }
            ]
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    },
    getFiles() {
        let receiptId = this._id;
        return Files.find({
            itemId: receiptId,
            target: 'receipts'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getInternalFiles() {
        let receiptId = this._id;
        return Files.find({
            itemId: receiptId,
            target: 'receipts_internal'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    editingTraveller() {
        return Template.instance().editingTraveller.get();
    },
    editingShop() {
        return Template.instance().editingShop.get() || !this.shopId;
    },
    editingAmounts() {
        return Template.instance().editingAmounts.get();
    },
    getCountry() {
        return Countries.findOne({
            _id: this.profile.countryId
        });
    },
    passportExpired() {
        return moment(this.profile.passportExpirationDate).isBefore(moment());
    },
    showActions() {
        let template = Template.instance();
        return template.data.showActions && (Vatfree.receipts.isMyTask(this, Meteor.userId()) || this.status === 'waitingForOriginal');
    },
    billingEntityDoc() {
        return Vatfree.billing.getBillingEntityForCompany(this.companyId);
    },
    salesforcePayoutId() {
        if (this.payoutId && this.payoutId.match(/^salesforce:/)) {
            return this.payoutId.split(':')[1];
        }
    },
    similarReceipts() {
        return ReactiveMethod.call('get-similar-receipts', this._id);
    },
    selectMapLocationCallback() {
        let template = Template.instance();
        return function (place) {
            if (place && place.address_components) {
                template.$('input[name="place"]').val(JSON.stringify(place));
            }
        };
    }
});

Template.receiptInfo.events(receiptEvents);
Template.receiptInfo.events({
    'change input[name="addShop"]'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).val()) {
            try {
                $('select[name="shopId"]').select2('destroy');
            } catch(e) {}
            template.addingShop.set(true);
            $(e.currentTarget).val("");
        }
    },
    'click .add-new-shop' (e, template) {
        e.preventDefault();
        let receipt = this;
        addNewShop(template, $('form[name="receipt-tasks-form"]'), function(shopId) {
            Receipts.update({
                _id: receipt._id
            },{
                $set: {
                    shopId: shopId
                }
            });
            template.editingShop.set();
            template.addingShop.set();
        });
    },
    'click .cancel-add-shop'(e ,template) {
        e.preventDefault();
        template.addingShop.set();
    },
    'click .file a'(e, template) {
        if (template.data.openImagesInInPane) {
            e.preventDefault();
            e.stopPropagation();
            template.showFile.set(this.fileObj || this);
        }
    },
    'click .close-show-file'(e, template) {
        e.preventDefault();
        template.showFile.set();
    },
    'click .receipt-task-traveller-card'(e, template) {
        /*
        Disabled
        console.log('click receipt-task-traveller-card');
        if (template.data.allowEdit === true) {
            e.preventDefault();
            e.stopPropagation();
            template.editingTraveller.set(true);
            Tracker.afterFlush(() => {
                receiptEditFormAfterFlush();
            });
        }
        */
    },
    'click .edit-traveller-cancel'(e, template) {
        e.preventDefault();
        template.editingTraveller.set();
    },
    'change select[name="userId"]'(e, template) {
        e.preventDefault();
        Receipts.update({
            _id: this._id
        },{
            $set: {
                userId: $(e.currentTarget).val()
            }
        });
        template.editingTraveller.set();
    },
    'click .receipt-task-shop-card'(e, template) {
        if (template.data.allowEdit === true) {
            e.preventDefault();
            e.stopPropagation();
            template.editingShop.set(true);
            Tracker.afterFlush(() => {
                receiptEditFormAfterFlush();
            });
        }
    },
    'click .edit-shop-cancel'(e, template) {
        e.preventDefault();
        template.editingShop.set();
    },
    'change select[name="shopId"]'(e, template) {
        e.preventDefault();
        Receipts.update({
            _id: this._id
        },{
            $set: {
                shopId: $(e.currentTarget).val()
            }
        });
        template.editingShop.set();
    },
    'click .client-detail'(e, template) {
        if (template.data.allowEdit === true) {
            e.preventDefault();
            template.editingAmounts.set(true);
            Tracker.afterFlush(() => {
                receiptEditFormAfterFlush();
                $('select[name="customsCountryId"]').select2();
            });
        }
    },
    'click .cancel'(e, template) {
        e.preventDefault();
        template.editingAmounts.set();
    },
    'submit form[name="receipt-tasks-amount-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        // fix dates
        formData.purchaseDate = moment(formData.purchaseDate, TAPi18n.__('_date_format')).toDate();
        formData.customsDate = moment(formData.customsDate, TAPi18n.__('_date_format')).toDate();
        // fix numbers
        formData.amount = Math.round(Number(formData.amount)*100);
        formData.totalVat = Math.round(Number(formData.totalVat)*100);
        formData.serviceFee = Math.round(Number(formData.serviceFee)*100);
        formData.refund = Math.round(Number(formData.refund)*100);

        let minimumReceiptAmount = Vatfree.receipts.getMinimumAmount(formData);
        if (formData.amount < minimumReceiptAmount) {
            toastr.error(Vatfree.translate('Receipt amount is below minimum required by country (__minimum_amount__)', { minimum_amount: Vatfree.numbers.formatCurrency(minimumReceiptAmount) } ));
            return false;
        }

        // Checks
        if (moment(formData.customsDate).isBefore(moment(formData.purchaseDate))) {
            toastr.error('Customs stamp date cannot be before purchase date');
            return false;
        }

        let vat = 0;
        _.each(formData.vat, (vatValue, vatKey) => {
            formData.vat[vatKey] = Math.round(Number(vatValue) * 100);
            vat += formData.vat[vatKey];
        });
        if (vat < formData.totalVat) {
            toastr.error('Vat rules do not add up to total VAT amount');
            return false;
        }

        Receipts.update({
            _id: this._id
        }, {
            $set: formData
        });
        template.editingAmounts.set();
    }
});

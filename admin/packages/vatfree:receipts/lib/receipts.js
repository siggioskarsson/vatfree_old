/* global Vatfree: true */
import moment from "moment";

Vatfree.receipts = {};

Vatfree.receipts.status = function() {
    return {
        'waitingForCustomsStamp': 'default',
        'userPending': 'default',
        'waitingForOriginal': 'info',
        'visualValidationPending': 'warning',
        'waitingForDoubleCheck': 'info',
        'shopPending': 'default',
        'readyForInvoicing': 'info',
        'waitingForPayment': 'info',
        'userPendingForPayment': 'default',
        'paidByShop': 'success',
        'requestPayout': 'warning',
        'paid': 'primary',
        'rejected': 'danger',
        'deleted': 'danger',
        'notCollectable': 'danger'
    };
};

Vatfree.receipts.icons = function() {
    return {
        'waitingForCustomsStamp': 'icon icon-customs',
        'userPending': 'icon icon-userpending',
        'waitingForOriginal': 'icon icon-waiting4original',
        'visualValidationPending': 'icon icon-waiting4visualvalidation',
        'waitingForDoubleCheck': 'icon icon-visualvalidationsecond',
        'shopPending': 'icon icon-shopwaitingforverification',
        'readyForInvoicing': 'icon icon-vtbsend',
        'waitingForPayment': 'icon icon-vtbwaiting',
        'userPendingForPayment': 'icon icon-userpending',
        'paidByShop': 'icon icon-vtbpaidbyshop',
        'requestPayout': 'icon icon-handopen',
        'paid': 'icon icon-paid',
        'rejected': 'icon icon-receiptreject',
        'deleted': 'icon icon-receiptdeleted',
        'notCollectable': 'icon icon-vtbnoncollectable',
        'depreciated': 'icon icon-receipt-depreciated',
        'waitingForAdministrativeCheck': 'icon icon-waiting4admincheck'
    };
};

Vatfree.receipts.sourceIcons = function() {
    return {
        'mobile': 'icon icon-moblie',
        'web': 'icon icon-web',
        'desk': 'icon icon-deskclient',
        'office': 'icon icon-office',
        'officeViaMerchant': 'icon icon-officeviamerchant',
        'officeViaCompetitor': 'icon icon-officeviacompetitor',
        'affiliate': 'icon icon-officeviaaffiliate'
    };
};

Vatfree.receipts.channelIcons = function() {
    return {
        'mailbox': 'icon icon-chmail',
        'dropbox': 'icon icon-chdropbox',
        'inperson': 'icon icon-chinperson'
    };
};

Vatfree.receipts.standardServiceFee = 20;
Vatfree.receipts.standardMinFee = 500;
Vatfree.receipts.standardMaxFee = 15000;
if (Meteor.isClient) {
    Meteor.startup(() => {
        Tracker.autorun(() => {
            if (Meteor.user()) {
                Meteor.call('get-defaults', (err, defaults) => {
                    if (defaults) {
                        Vatfree.receipts.standardServiceFee = defaults.standardServiceFee || 30;
                        Vatfree.receipts.standardMinFee = defaults.standardMinFee || 0;
                        Vatfree.receipts.standardMaxFee = defaults.standardMaxFee || 8000;
                    }
                });
            }
        });
    });
} else {
    Vatfree.receipts.standardServiceFee = Meteor.settings.defaults.standardServiceFee || 30;
    Vatfree.receipts.standardMinFee = Meteor.settings.defaults.standardMinFee || 0;
    Vatfree.receipts.standardMaxFee = Meteor.settings.defaults.standardMaxFee || 8000;
}

Vatfree.receipts.getParentForFees = function (shopId, receipt) {
    if (receipt && Vatfree.receipts.isNato(receipt)) {
        return {
            name: "NATO"
        };
    }

    let shop = Shops.findOne({_id: shopId}) || {};
    shop._type = 'shop';
    if (shop && shop.companyId && !shop.billingEntity) {
        // check whether we should take the fees from the parent company
        let shopCompany = Companies.findOne({_id: shop.companyId});
        if (shopCompany && shopCompany.billingEntity) {
            shopCompany._type = 'company';
            shop = shopCompany;
        } else if (shopCompany && shopCompany.ancestors) {
            // check whether we should take the fees from a grand parent company
            let company = Companies.findOne({
                _id: {
                    $in: shopCompany.ancestors
                },
                billingEntity: true
            }, {
                sort: {
                    ancestorsLength: -1
                }
            });
            if (company) {
                company._type = 'company';
                shop = company;
            }
        }
    }
    return shop;
};

Vatfree.receipts.getFeesForInvoicing = function(shopId, receipt = {}) {
    return Vatfree.receipts.getFees(shopId, receipt, true);
};
Vatfree.receipts.getFees = function(shopId, receipt = {}, forInvoicing = false) {
    let shop = Vatfree.receipts.getParentForFees(shopId, receipt);
    const isNatoReceipt = Vatfree.receipts.isNato(receipt);
    const natoFee = Meteor.settings.public.natoFee || 900;
    if ((isNatoReceipt && Shops.isPartner(shopId)) || (shop.invoiceServiceFee && !forInvoicing)) {
        let fees = {
            serviceFee: 0,
            minFee: 0,
            maxFee: 0
        };

        if (isNatoReceipt || shop.invoiceServiceFee) {
            fees.deferredMinFee = (isNatoReceipt ? natoFee : false) || shop.minFee || Vatfree.receipts.standardMinFee;
            fees.deferredMaxFee = (isNatoReceipt ? natoFee : false) || shop.maxFee || Vatfree.receipts.standardMaxFee;
            fees.deferredServiceFee = (isNatoReceipt ? natoFee : false) || shop.serviceFee || Vatfree.receipts.standardServiceFee;
        }

        return fees;
    } else {
        return {
            serviceFee: shop.serviceFee || Vatfree.receipts.standardServiceFee,
            minFee: shop.minFee || Vatfree.receipts.standardMinFee,
            maxFee: shop.maxFee || Vatfree.receipts.standardMaxFee
        };
    }
};
Vatfree.receipts.getMinimumAmount = function(receipt = {}) {
    if (Vatfree.receipts.isNato(receipt)) {
        return 4600;
    } else {
        let country = Countries.findOne({_id: receipt.countryId}) || {};
        return country.minimumReceiptAmount || 5000; // 5000 cents - 50 euros
    }
};

Vatfree.receipts.checkMinimumAmount = function (receiptData) {
    let minimumReceiptAmount = Vatfree.receipts.getMinimumAmount(receiptData);
    if (receiptData.amount < minimumReceiptAmount) {
        return minimumReceiptAmount;
    }

    return true;
};

Vatfree.receipts.isNato = function(receipt) {
    receipt = receipt || {};
    let receiptType = ReceiptTypes.findOne({_id: receipt.receiptTypeId}) || {};
    return receiptType.name === 'NATO'; // TODO: make this check better
};

Vatfree.receipts.isCargo = function(receipt) {
    receipt = receipt || {};
    let receiptType = ReceiptTypes.findOne({_id: receipt.receiptTypeId}) || {};
    return receiptType.name === 'Cargo'; // TODO: make this check better
};

Vatfree.receipts.processReceiptForm = function (formData) {
    let traveller = Travellers.findOne({
        _id: formData.userId
    }) || {};
    let travellerType = {};
    if (traveller && traveller.private) {
        travellerType = TravellerTypes.findOne({_id: traveller.private.travellerTypeId || Vatfree.defaults.TravellerTypeId});
    }

    // fix dates
    if (_.has(formData, 'purchaseDate')) {
        if (formData.purchaseDate) {
            formData.purchaseDate = moment(formData.purchaseDate, TAPi18n.__('_date_format')).toDate();
        } else {
            formData.purchaseDate = null;
        }
    }
    if (_.has(formData, 'customsDate')) {
        if (formData.customsDate) {
            formData.customsDate = moment(formData.customsDate, TAPi18n.__('_date_format')).toDate();
        } else {
            formData.customsDate = null;
        }
    }

    // fix numbers
    if (_.has(formData, 'amount')) {
        formData.amount = Math.round(Number(formData.amount) * 100);
        let minimumReceiptAmount = Vatfree.receipts.getMinimumAmount(formData);
        if (formData.amount < minimumReceiptAmount) {
            toastr.error(Vatfree.translate('Receipt amount is below minimum required by country (__minimum_amount__)', { minimum_amount: Vatfree.numbers.formatCurrency(minimumReceiptAmount) } ));
            return false;
        }
    }

    let checkAmounts = false;
    if (_.has(formData, 'totalVat')) {
        formData.totalVat = Math.round(Number(formData.totalVat) * 100);
        checkAmounts = true;
    }
    if (_.has(formData, 'serviceFee')) {
        formData.serviceFee = Math.round(Number(formData.serviceFee) * 100);
        checkAmounts = true;
    } else  if (formData.deferredServiceFeeCheck === 'on') {
        formData.serviceFee = 0;
        checkAmounts = true;
    }

    if (_.has(formData, 'refund')) {
        formData.refund = Math.round(Number(formData.refund) * 100);
        checkAmounts = true;
    }
    if (_.has(formData, 'deferredServiceFee')) {
        formData.deferredServiceFee = Math.round(Number(formData.deferredServiceFee) * 100);
        checkAmounts = true;
    }
    if (_.has(formData, 'affiliateServiceFee')) {
        formData.affiliateServiceFee = Math.round(Number(formData.affiliateServiceFee) * 100);
        checkAmounts = true;
    }

    if (checkAmounts) {
        const refundAndFeesWithoutDeferredFee = (formData.serviceFee || 0) + (formData.refund || 0) + (formData.affiliateServiceFee || 0);
        if (!formData.deferredServiceFee && formData.totalVat !== refundAndFeesWithoutDeferredFee) {
            toastr.error('Amounts do not add up. Check VAT, refund and fees.');
            return false;
        }
    }

    // Checks
    if (!travellerType.isNato && _.has(formData, 'customsDate') && _.has(formData, 'purchaseDate')) {
        if (moment(formData.customsDate).isBefore(moment(formData.purchaseDate))) {
            toastr.error('Customs stamp date cannot be before purchase date');
            return false;
        }
    }

    if (_.has(formData, 'vat')) {
        // vat rates of for instance 2.1 will becomde {vat: {2: {1: ....}}}
        // this is the most central place to fix this
        let newFormDataVat = {};
        _.each(formData.vat, (vatValue, vatKey) => {
            if (_.isObject(vatValue)) {
                _.each(vatValue, (vatValue2, vatKey2) => {
                    newFormDataVat['' + vatKey + '_' + vatKey2] = vatValue2;
                });
            } else {
                newFormDataVat[vatKey] = vatValue;
            }
        });
        formData.vat = newFormDataVat;
    }

    let vat = 0;
    _.each(formData.vat, (vatValue, vatKey) => {
        formData.vat[vatKey] = Math.round(Number(vatValue) * 100);
        vat += formData.vat[vatKey];
    });
    // we only check the VAT rules if the shop is the same, otherwise we cannot change country
    if (vat < formData.totalVat && this.shopId === formData.shopId) {
        toastr.error('Vat rules do not add up to total VAT amount');
        return false;
    }

    return true;
};

Vatfree.receipts.allowReject = function(receipt) {
    let allowedStatus = [
        'waitingForCustomsStamp',
        'userPending',
        'waitingForOriginal',
        'visualValidationPending',
        'waitingForDoubleCheck',
        'shopPending',
        'readyForInvoicing'
    ];
    return _.contains(allowedStatus, receipt.status);
};

Vatfree.receipts.isMyTask = function(receipt, userId) {
    return receipt.status === 'visualValidationPending' || (receipt.status === 'waitingForDoubleCheck' && receipt.checkedBy !== userId);
};

Vatfree.receipts.createTravellerForm = function(receiptId, traveller) {
    // render SVG template
    const data = Blaze.toHTMLWithData(Template.traveller_svg_form, {
        traveller: traveller
    });
    const svgBlob = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});

    const DOMURL = window.URL || window.webkitURL || window;
    const url = DOMURL.createObjectURL(svgBlob);

    const canvas = document.createElement('canvas');
    canvas.width = 624;
    canvas.height = 322;

    const ctx = canvas.getContext('2d');
    const img = new Image();
    img.width = 624;
    img.height = 322;
    img.onload = function () {
        ctx.drawImage(img, 0, 0, 624, 322);
        DOMURL.revokeObjectURL(url);

        const imgURI = canvas.toDataURL('image/png');

        const file = new FS.File();
        file.name('traveller-form-' + (traveller.profile.passportNumber || "") + '.png');
        file.itemId = receiptId;
        file.receiptId = receiptId;
        file.target = 'receipts';
        file.subType = 'form';
        file.createdAt = new Date();
        file.attachData(imgURI);

        Files.insert(file);
    };

    img.src = url;
};

Vatfree.receipts.travellerAllowedDeleted = function(receipt) {
    return _.contains([
        'waitingForCustomsStamp',
        'userPending',
        'waitingForOriginal',
        'visualValidationPending',
    ], receipt.status) && !receipt.originalsCheckedAt;
};

Vatfree.receipts.getTravellerStatusForTraveller = function (receipt, traveller) {
    if (traveller.private && traveller.private.status !== 'userVerified' && _.contains([
        'userPending',
        'waitingForOriginal',
        'visualValidationPending',
        'waitingForDoubleCheck',
        'shopPending',
        'readyForInvoicing',
        'waitingForPayment',
        'userPendingForPayment'
    ], receipt.status)) {
        return TAPi18n.__("userPending");
    } else {
        let status = receipt.status;
        if (status === 'rejected') {
            let allowReopen = false;
            let selector = {};
            if (receipt.receiptRejectionIds && receipt.receiptRejectionIds.length > 0) {
                selector['_id'] = {
                    $in: receipt.receiptRejectionIds
                };
                ReceiptRejections.find(selector).forEach((receiptRejection) => {
                    if (receiptRejection.allowReopen) {
                        allowReopen = true;
                    }
                });
            }
            if (allowReopen) {
                return "⚠️ " + TAPi18n.__("Action needed on your part");
            }
        }

        return TAPi18n.__(receipt.status);
    }
};


Vatfree.receipts.processServiceFees = function (travellerTypeId, receiptData, shopId) {
    let travellerType = TravellerTypes.findOne({_id: travellerTypeId});
    if (travellerType && travellerType.forceReceiptTypeId) {
        receiptData.receiptTypeId = travellerType.forceReceiptTypeId;
    }

    if (travellerType && travellerType.isNato) {
        receiptData.serviceFee = 0;
        receiptData.deferredServiceFee = 900;
        receiptData.refund = receiptData.totalVat;
        receiptData.nonStandardVat = true;
        receiptData.originalsCheckedBy = 'nato';
        receiptData.originalsCheckedAt = new Date();
    } else {
        let fees = Vatfree.receipts.getFees(shopId, receiptData);
        if (fees.deferredServiceFee) {
            receiptData.serviceFee = 0;
            receiptData.deferredServiceFee = Vatfree.vat.calculateFee(receiptData.totalVat, fees.deferredServiceFee, fees.deferredMinFee, fees.deferredMaxFee);
            receiptData.refund = receiptData.totalVat;
        } else {
            receiptData.serviceFee = Vatfree.vat.calculateFee(receiptData.totalVat, fees.serviceFee, fees.minFee, fees.maxFee);
            receiptData.refund = Vatfree.vat.calculateRefund(receiptData.totalVat, fees.serviceFee, fees.minFee, fees.maxFee);
        }
    }
};

Vatfree.receipts.processVatLines = function (receiptData, country) {
    if (_.size(receiptData.vat) <= 0) {
        // fill the split vat rates in
        receiptData.vat = {};
        let defaultRate = Vatfree.vat.getDefaultRate(country.vatRates);
        _.each(country.vatRates, (vatRate) => {
            if (vatRate.rate === defaultRate) {
                receiptData.vat[vatRate.rate.toString().replace('.', '_')] = receiptData.totalVat;
            } else {
                receiptData.vat[vatRate.rate.toString().replace('.', '_')] = 0;
            }
        });
    }
};

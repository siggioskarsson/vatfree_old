import moment from 'moment';
const receiptRemark = new SimpleSchema({
    userId: {
        type: String
    },
    name: {
        type: String
    },
    date: {
        type: Date
    },
    remark: {
        type: String
    }
});

const EuroAmountsSchema = new SimpleSchema({
    amount: {
        type: Number,
        decimal: true,
        optional: true
    },
    totalVat: {
        type: Number,
        decimal: true,
        optional: true
    },
    refund: {
        type: Number,
        decimal: true,
        optional: true
    },
    serviceFee: {
        type: Number,
        decimal: true,
        optional: true
    },
    affiliateServiceFee: {
        type: Number,
        decimal: true,
        optional: true
    },
    deferredServiceFee: {
        type: Number,
        decimal: true,
        optional: true
    }
});

Receipts = new Meteor.Collection('receipts');
Receipts.attachSchema(new SimpleSchema({
    salesforceId: {
        type: String,
        label: "Internal salesforce id",
        optional: true
    },
    requestId: {
        type: String,
        label: "Internal salesforce request id",
        optional: true
    },
    receiptNr: {
        type: Number,
        label: "Internally generated unique receipt number",
        autoValue: function() {
            if (Meteor.isServer) {
                if (!this.field("receiptNr").value) {
                    if (this.isInsert) {
                        return getNextReceiptNumber();
                    } else if (this.isUpsert) {
                        return {
                            $setOnInsert: getNextReceiptNumber()
                        };
                    } else {
                        this.unset();
                    }
                }
            } else {
                this.unset();
            }
        },
        optional: true
    },
    receiptTypeId: {
        type: String,
        label: "The type of the receipt, if not normal",
        optional: true
    },
    receiptTypeExtraFields: {
        type: Object,
        label: "The extra fields the receipt types add to the receipt form",
        optional: true,
        blackbox: true
    },
    source: {
        type: String,
        label: "Source of the receipt",
        optional: false,
        defaultValue: 'desk',
        allowedValues: [
            'mobile',
            'web',
            'desk',
            'affiliate',
            'office'
        ]
    },
    sourceIdentifier: {
        type: String,
        label: "Free form identifier of the device used to submit the receipt",
        optional: true
    },
    channel: {
        type: String,
        label: "Channel through which the receipt arrived",
        optional: true,
        allowedValues: [
            'mailbox',
            'dropbox',
            'inperson'
        ]
    },
    referral: {
        type: String,
        label: "Referral for the receipt",
        optional: true,
        allowedValues: [
            'customs',
            'competitor',
            'affiliate',
            'shop'
        ]
    },
    referralName: {
        type: String,
        label: "The name of the identity that referred this receipt to us",
        optional: true
    },
    invoiceId: {
        type: String,
        label: "The invoice this receipt was invoiced to the shop",
        optional: true
    },
    payoutId: {
        type: String,
        label: "The payout id for this receipt",
        optional: true
    },
    affiliatePayoutId: {
        type: String,
        label: "The payout id for the affiliate for this receipt",
        optional: true
    },
    userId: {
        type: String,
        label: "User that submitted this receipt",
        optional: false
    },
    shopId: {
        type: String,
        label: "Shop this receipt was issued",
        optional: true
    },
    companyId: {
        type: String,
        label: "Company this receipt will be billed to",
        optional: true
    },
    poprId: {
        type: String,
        label: "POPr Id of this receipt, if receipt is available via POPr",
        optional: true
    },
    poprSecret: {
        type: String,
        label: "POPr secret of this receipt, if receipt is available via POPr",
        optional: true
    },
    poprClaimToken: {
        type: String,
        label: "POPr token to manipulate the claim of this receipt",
        optional: true
    },
    affiliateId: {
        type: String,
        label: "Affiliate Id of this receipt",
        optional: true
    },
    purchaseDate: {
        type: Date,
        label: "Purchase date of this receipt",
        optional: true
    },
    customsDate: {
        type: Date,
        label: "Customs stamp date of this receipt",
        optional: true
    },
    flightNumber: {
        type: String,
        label: "Optional flight number of the flight the traveller leaves the EU",
        optional: true
    },
    amount: {
        type: Number,
        label: "Total amount of receipt",
        optional: false
    },
    countryId: {
        type: String,
        label: "Country the purchase was made",
        autoValue: function() {
            if (Meteor.isServer) {
                if (this.field("shopId").value) {
                    let shop = Shops.findOne({
                        _id: this.field("shopId").value
                    });
                    if (shop) {
                        return shop.countryId;
                    } else {
                        this.unset();
                    }
                }
            } else {
                this.unset();
            }
        },
        optional: true
    },
    currencyId: {
        type: String,
        label: "Currency of this receipt",
        optional: false
    },
    customsCountryId: {
        type: String,
        label: "Customs stamp country",
        optional: true
    },
    vat: {
        type: Object,
        label: "VAT on receipt",
        optional: true,
        blackbox: true
    },
    totalVat: {
        type: Number,
        label: "Total of VAT on the receipt",
        optional: true
    },
    serviceFee: {
        type: Number,
        label: "Total service fee",
        optional: true
    },
    affiliateServiceFee: {
        type: Number,
        label: "Part of service fee that will go to affiliate",
        optional: true
    },
    euroServiceFee: {
        type: Number,
        label: "Total service fee in euro",
        optional: true
    },
    deferredServiceFee: {
        type: Number,
        label: "Total deferred service fee, if servie is supposed to be invoiced to shop/company",
        optional: true
    },
    fixedDeferredServiceFee: {
        type: Boolean,
        label: "Whether the deferred service fee is fixed, and thus not linked to vat/refund",
        autoValue: function () {
            if (this.field('fixedDeferredServiceFee').isSet) {
                return !!this.field('fixedDeferredServiceFee').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    refund: {
        type: Number,
        label: "Total refund",
        optional: true
    },
    euroRefund: {
        type: Number,
        label: "Total refund in euro",
        optional: true
    },
    euroAmounts: {
        type: EuroAmountsSchema,
        label: 'Euro amounts for all values for statistics',
        optional: true
    },
    nonStandardVat: {
        type: Boolean,
        label: "Whether this receipt has non standard vat",
        autoValue: function () {
            if (this.field('nonStandardVat').isSet) {
                return !!this.field('nonStandardVat').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    language: {
        type: String,
        label: "Language code",
        optional: true
    },
    originalsCheckedAt: {
        type: Date,
        label: "The date the original scans of the receipt was checked",
        optional: true
    },
    originalsCheckedBy: {
        type: String,
        label: "User id that checked the original scans of the receipt",
        optional: true
    },
    checkedAt: {
        type: Date,
        label: "The date the receipt was checked",
        optional: true
    },
    checkedBy: {
        type: String,
        label: "User id that checked this receipt",
        optional: true
    },
    doubleCheckedAt: {
        type: Date,
        label: "The date the receipt was checked",
        optional: true
    },
    doubleCheckedBy: {
        type: String,
        label: "User id that checked this receipt",
        optional: true
    },
    status: {
        type: String,
        label: "Status of the receipt",
        optional: false,
        allowedValues: [
            'waitingForCustomsStamp',
            'userPending',
            'waitingForOriginal',
            'visualValidationPending',
            'waitingForDoubleCheck',
            'shopPending',
            'readyForInvoicing',
            'waitingForPayment',
            'userPendingForPayment',
            'paidByShop',
            'requestPayout',
            'paid',
            'rejected',
            'deleted',
            'notCollectable',
            'depreciated',
            'waitingForAdministrativeCheck'
        ]
    },
    receiptRejectionIds: {
        type: [String],
        label: "The id's of the receipt-rejections of this receipt",
        optional: true
    },
    archiveCode: {
        type: String,
        label: "In which envelop this receipt was archived",
        optional: true
    },
    archiveCodeNr: {
        type: Number,
        label: "Which nr of the archived receipt",
        autoValue: function() {
            if (this.field('archiveCode').isSet) {
                let archiveCode = this.field('archiveCode').value.split('-');
                if (archiveCode.length === 2 && !isNaN(archiveCode[1])) {
                    if (this.isInsert) {
                        return Number(archiveCode[1])
                    } else {
                        return {
                            $set: Number(archiveCode[1])
                        };
                    }
                } else {
                    this.unset();
                }
            } else {
                this.unset();
            }
        },
        optional: true
    },
    archiveBox: {
        type: String,
        label: "In which box this receipt was archived",
        optional: true
    },
    checkRemarks: {
        type: [receiptRemark],
        label: "Remarks added during checking",
        optional: true
    },
    lastReminderSent: {
        type: Date,
        label: "When the last reminder was sent",
        optional: true
    },
    customsReminderSent: {
        type: Date,
        label: "When and if the customs stamp reminder was sent",
        optional: true
    },
    reminders: {
        type: [ RemindersSchema ],
        label: "Reminders sent to this traveller",
        optional: true
    },
    partnershipStatus: {
        type: String,
        label: "Partnership status of the entity this invoice was sent to",
        optional: true
    },
    taskedAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: new Date()
                };
            }
        }
    },
    travellerCountryId: {
        type: String,
        label: "The country the traveller linked to this receipt comes from",
        optional: true
    },
    paidAt: {
        type: Date,
        label: "The date this receipt was marked as paid in the system",
        optional: true
    },
    payoutAt: {
        type: Date,
        label: "The date this receipt was paid out to the traveller",
        optional: true
    },
    shaSum: {
        type: String,
        optional: true
    },
    signature: {
        type: String,
        optional: true
    },
    notes: {
        type: String,
        label: "Receipt notes",
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
Receipts.attachSchema(CreatedUpdatedSchema);
StatusDates.attachToSchema(Receipts);

var getNextReceiptNumber = function() {
    let year = moment().format('YYYY').toString();
    let counter = Number(incrementCounter(Counters, 'receiptNr_2018')).padLeft(7).toString();

    let nextReceiptNumber = year + counter;

    return Math.round(Number(nextReceiptNumber));
};

Receipts.allow({
    insert: function (userId, doc) {
        return Vatfree.userIsInRole(userId, 'receipts-create');
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Vatfree.userIsInRole(userId, 'receipts-update');
    },
    remove: function (userId, doc) {
        return false;
    }
});

import moment from 'moment';

AffiliatePayoutBatches = new Meteor.Collection('affiliate-payout-batches');
AffiliatePayoutBatches.attachSchema(new SimpleSchema({
    affiliatePayoutBatchNumber: {
        type: String,
        label: "Unique name of this affiliatePayout batch",
        autoValue: function() {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return getNextAffiliatePayoutBatchNumber();
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: getNextAffiliatePayoutBatchNumber()
                    };
                } else {
                    this.unset();
                }
            } else {
                this.unset();
            }
        },
        optional: true
    },
    partnerId: {
        type: String,
        label: "Partner if applicable",
        optional: true
    },
    amount: {
        type: String,
        label: "Total affiliatePayout batch amount",
        optional: false
    },
    paymentMethod: {
        type: String,
        label: "The payment method for this batch",
        optional: false
    },
    affiliatePayoutIds: {
        type: [String],
        label: "All affiliatePayout Ids in this batch",
        optional: false
    },
    paidAt: {
        type: Date,
        label: "When this batch was marked as paid",
        optional: true
    },
    paidBy: {
        type: String,
        label: "By whom this batch was marked as paid",
        optional: true
    }
}));
AffiliatePayoutBatches.attachSchema(CreatedUpdatedSchema);

AffiliatePayoutBatches.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

var getNextAffiliatePayoutBatchNumber = function() {
    let year = moment().format('YYYY').toString();
    let counter = Number(incrementCounter(Counters, 'affiliatePayoutBatchNumber_' + year)).padLeft(5).toString();

    let nextAffiliatePayoutBatchNumber = year + counter;

    return "PAY" + nextAffiliatePayoutBatchNumber;
};

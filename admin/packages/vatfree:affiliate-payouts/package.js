Package.describe({
    name: 'vatfree:affiliate-payouts',
    summary: 'Vatfree affiliate payouts package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'email',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'konecty:mongo-counter@0.0.5_3',
        'reywood:publish-composite@1.5.1',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'simonsimcity:job-collection',
        'vatfree:core',
        'vatfree:countries',
        'vatfree:notify',
        'vatfree:affiliates',
        'vatfree:payouts'
    ]);

    // shared files
    api.addFiles([
        'affiliate-payouts.js',
        'affiliate-payout-batches.js'
    ]);

    // server files
    api.addFiles([
        'server/lib/remove-receipt-from-affiliate-payout.js',
        'server/methods.js',
        'server/notify.js',
        'server/affiliate-payout-batches.js',
        'server/affiliate-payouts.js',
        'publish/affiliate-payouts.js',
        'publish/affiliate-payout-batch.js',
        'publish/affiliate-payout.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/affiliate-payout-batch.html',
        'templates/affiliate-payout-batch.js',
        'templates/affiliate-payouts.html',
        'templates/affiliate-payouts.js'
    ], 'client');

    api.export([
        'AffiliatePayouts',
        'AffiliatePayoutBatches',
    ]);
});

/* global AffiliatePayouts: true, AffiliatePayoutBatches: true */

Meteor.methods({
    'request-affiliate-payout'(paymentMethod, paymentNr, paymentAccountName) {
        // request affiliatePayout by affiliate
        if (!Roles.userIsInRole(this.userId, 'affiliate', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (!Vatfree.affiliatePayouts.validateAffiliatePayout(paymentMethod, paymentNr)) {
            throw new Meteor.Error(500, "Invalid affiliate payout method");
        }

        let costs = Meteor.settings.public.affiliatePayoutCosts[paymentMethod] || 0;
        let euroCurrency = Currencies.findOne({code: "EUR"});

        let receiptIds = {};
        let amount = {};
        Receipts.find({
            affiliateId: this.userId,
            affiliatePayoutId: {
                $exists: false
            },
            invoiceId: {
                $exists: true
            },
            status: 'paidByShop'
        }).forEach((receipt) => {
            if (receipt.euroRefund) {
                receipt.refund = receipt.euroRefund;
                receipt.currencyId = euroCurrency._id;
            }
            if (!receiptIds[receipt.currencyId]) {
                receiptIds[receipt.currencyId] = [];
                amount[receipt.currencyId] = 0;
            }

            receiptIds[receipt.currencyId].push(receipt._id);
            amount[receipt.currencyId] += receipt.refund;
        });

        _.each(receiptIds, (receipts, currencyId) => {
            AffiliatePayouts.insert({
                status: "new",
                affiliateId: this.userId,
                receiptIds: receipts,
                amount: amount[currencyId] - costs,
                costs: costs,
                currencyId: currencyId,
                paymentMethod: paymentMethod,
                paymentNr: paymentNr,
                paymentAccountName: paymentAccountName
            });
        });
    },
    'create-affiliate-payout-batch'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        const affiliatePayoutSelector = {
            status: 'new',
            affiliatePayoutBatchId: {
                $exists: false
            }
        };

        let amount = {};
        let affiliatePayoutIds = {};
        AffiliatePayouts.find(affiliatePayoutSelector).forEach((affiliatePayout) => {
            if (!_.has(amount, affiliatePayout.paymentMethod)) {
                amount[affiliatePayout.paymentMethod] = 0;
                affiliatePayoutIds[affiliatePayout.paymentMethod] = [];
            }

            amount[affiliatePayout.paymentMethod] += affiliatePayout.amount;
            affiliatePayoutIds[affiliatePayout.paymentMethod].push(affiliatePayout._id);
        });

        let affiliatePayoutBatches = [];
        _.each(affiliatePayoutIds, (affiliatePayout, paymentMethod) => {
            const affiliatePayoutBatch = {
                amount: amount[paymentMethod],
                paymentMethod: paymentMethod,
                affiliatePayoutIds: affiliatePayout
            };

            affiliatePayoutBatches.push(AffiliatePayoutBatches.insert(affiliatePayoutBatch));
        });

        return affiliatePayoutBatches;
    },
    'get-affiliate-payout-stats'(selector) {
        if (!Vatfree.userIsInRole(this.userId, 'affiliate-payouts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const pipeline = [
            {
                $match: selector,
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 },
                    amount: { $sum: { $ifNull: ['$amount', 0] } }
                }
            },
        ];
        return AffiliatePayouts.rawCollection().aggregate(pipeline).toArray();
    }
});

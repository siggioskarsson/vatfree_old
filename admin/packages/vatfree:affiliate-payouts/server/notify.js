Vatfree.notify.sendStatusNotification['affiliate-payouts'] = function (userId, affiliatePayoutId, affiliateStatusNotify) {
    try {
        let affiliatePayout = AffiliatePayouts.findOne({_id: affiliatePayoutId});
        let currency = Currencies.findOne({_id: affiliatePayout.currencyId});
        let affiliate = Affiliates.findOne({_id: affiliatePayout.affiliateId});
        if (affiliate && affiliate.profile) {
            affiliate.country = Countries.findOne({_id: affiliate.profile.countryId }) || {};
        }

        let receipts = Receipts.find({_id: {$in: (affiliatePayout.receiptIds || [])}}, {sort: { receiptNr: 1 }}).fetch();

        let emailLanguage = affiliate.profile.language || "en";

        let activityIds = {
            affiliatePayoutId: affiliatePayoutId,
            affiliateId: affiliate._id
        };

        let templateData = {
            language: emailLanguage,
            affiliatePayout: affiliatePayout,
            currency: currency,
            affiliate: affiliate.profile,
            receipts: receipts
        };

        let toAddress = affiliate.profile.email;
        let fromAddress = 'Vatfree.com support <support@vatfree.com>';

        Vatfree.notify.sendNotification(affiliateStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage, affiliate);
    } catch (e) {
        ActivityLogs.insert({
            type: 'task',
            userId: userId,
            subject: 'Sending of affiliate payout to affiliate failed',
            status: 'new',
            affiliatePayoutId: affiliatePayoutId,
            notes: "Error: " + e.message
        });
    }
};

/* global AffiliatePayouts: true, AffiliatePayoutBatches: true */

try {
    AffiliatePayouts._ensureIndex({'affiliatePayoutNr': 1}, {unique: true});
    AffiliatePayouts._ensureIndex({'receiptIds': 1}, {unique: true, sparse: true});
    AffiliatePayouts._ensureIndex({'affiliateId': 1});
    AffiliatePayouts._ensureIndex({'textSearch': "text"}, {"language_override": "_text_language"});
} catch(e) {
    console.error(e);
}

ActivityStream.attachHooks(AffiliatePayouts);
// Add index to activity stream collection for affiliatePayouts
ActivityStream.collection._ensureIndex({'securityContext.affiliatePayoutId': 1});

AffiliatePayouts.after.insert(function(userId, doc) {
    AffiliatePayouts.updateTextSearch(doc);

    // update receipts with affiliatePayoutId
    Receipts.update({
        _id: {
            $in: doc.receiptIds || []
        },
        affiliatePayoutId: {
            $exists: false
        },
        affiliateId: userId
    },{
        $set: {
            affiliatePayoutId: doc._id
        }
    },{
        multi: 1
    });

    Meteor.defer(() => {
        Vatfree.notify.processNotificationTriggers(userId, 'affiliate-payouts', '- insert -', false, doc._id);
    });
});

AffiliatePayouts.before.update(function(userId, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status && modifier.$set.status === 'cancelled') {
        Receipts.update({
            _id: {
                $in: doc.receiptIds || []
            }
        },{
            $unset: {
                affiliatePayoutId: 1
            }
        },{
            multi: true
        });
    }
});

AffiliatePayouts.after.update(function(userId, doc, fieldNames, modifier, options) {
    Meteor.defer(() => {
        AffiliatePayouts.updateTextSearch(doc);
        Vatfree.notify.checkStatusChange('affiliatePayouts', userId, this.previous, doc, fieldNames, modifier);
    });
});

AffiliatePayouts.updateTextSearch = function(doc) {
    let textSearch = doc.affiliatePayoutNr.toString();
    textSearch+= " " + (doc.paymentMethod || "") + " payment:" + (doc.paymentMethod || "");
    textSearch+= " " + (doc.paymentNr || "").replace(/ /g, '') + " nr:" + (doc.paymentNr || "").replace(/ /g, '');
    textSearch+= " " + (doc.paymentAccountName || "") + " name:" + (doc.paymentAccountName || "");
    textSearch+= " " + (doc.notes || "");
    textSearch+= " " + (doc.amount || 0) + " amount:" + (doc.amount || 0);
    textSearch+= " " + (doc.costs || 0) + " costs:" + (doc.costs || 0);

    if (doc.affiliatePayoutBatchId) {
        let affiliatePayoutBatch = AffiliatePayoutBatches.findOne({_id: doc.affiliatePayoutBatchId});
        if (affiliatePayoutBatch) {
            textSearch += ' ' + affiliatePayoutBatch.affiliatePayoutBatchNumber;
        }
    }

    Receipts.find({
        _id: {
            $in: doc.receiptIds || doc.originalReceiptIds || []
        }
    },{
        fields: {
            textSearch: 1
        }
    }).forEach((receipt) => {
        textSearch += ' ' + receipt.textSearch;
    });

    AffiliatePayouts.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch.toLowerCase().latinize()
        }
    });
};

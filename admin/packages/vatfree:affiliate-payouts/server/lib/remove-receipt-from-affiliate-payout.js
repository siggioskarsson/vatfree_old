Vatfree.removeReceiptFromAffiliatePayout = function(affiliatePayoutId, receiptId) {
    let affiliatePayout = AffiliatePayouts.findOne({_id: affiliatePayoutId});
    if (!affiliatePayout) {
        throw new Meteor.Error(500, 'AffiliatePayout could not be found');
    }
    let receipt = Receipts.findOne({_id: receiptId});
    if (!receipt) {
        throw new Meteor.Error(500, 'Receipt could not be found');
    }

    if (affiliatePayout.status === 'paid') {
        throw new Meteor.Error(500, 'AffiliatePayout has already been processed');
    }

    if (affiliatePayout.receiptIds.length === 1 && affiliatePayout.receiptIds[0] === receiptId) {
        AffiliatePayouts.update({
            _id: affiliatePayoutId
        },{
            $set: {
                status: "cancelled",
                originalReceiptIds: [receiptId]
            },
            $unset: {
                receiptIds: 1
            }
        });
    } else {
        AffiliatePayouts.update({
            _id: affiliatePayoutId
        },{
            $set: {
                amount: affiliatePayout.amount - receipt.refund
            },
            $pull: {
                receiptIds: receiptId
            }
        });
        Receipts.update({
            _id: receiptId
        },{
            $unset: {
                affiliatePayoutId: 1
            }
        });
    }
};

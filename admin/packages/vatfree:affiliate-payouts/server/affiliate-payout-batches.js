/* global AffiliatePayouts: true, AffiliatePayoutBatches: true */

AffiliatePayoutBatches.after.insert(function(userId, doc) {
    // update all debt collectionIds
    AffiliatePayouts.update({
        _id: {
            $in: doc.affiliatePayoutIds
        }
    },{
        $set: {
            affiliatePayoutBatchId: doc._id,
            status: 'sentToPaymentProvider'
        }
    },{
        multi: 1
    });
});

AffiliatePayoutBatches.after.update(function(userId, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'paidAt') && modifier['$set'] && modifier['$set']['paidAt']) {
        // update all debt collectionIds
        AffiliatePayouts.update({
            _id: {
                $in: doc.affiliatePayoutIds
            }
        },{
            $set: {
                status: 'paid',
                paidAt: doc.paidAt,
                paidBy: doc.paidBy
            }
        },{
            multi: 1
        });
    }
});

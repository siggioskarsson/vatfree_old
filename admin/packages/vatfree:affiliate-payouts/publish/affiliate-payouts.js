/* global AffiliatePayouts: true, AffiliatePayoutBatches: true */

Meteor.publishComposite('affiliate-payouts', function (searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        find: function () {
            return AffiliatePayouts.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                collectionName: "affiliates",
                find: function (affiliatePayout) {
                    return Affiliates.find({
                        _id: affiliatePayout.affiliateId
                    });
                }
            },
            {
                find: function (affiliatePayout) {
                    return Receipts.find({
                        _id: {
                            $in: affiliatePayout.receiptIds || affiliatePayout.originalReceiptIds || []
                        }
                    });
                },
                children: [
                    {
                        find: function (receipt) {
                            return Shops.find({
                                _id: receipt.shopId
                            });
                        }
                    }
                ]
            }
        ]
    };
});

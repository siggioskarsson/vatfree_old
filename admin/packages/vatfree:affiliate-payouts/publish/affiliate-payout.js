/* global AffiliatePayouts: true, AffiliatePayoutBatches: true */

Meteor.publishComposite('affiliate-payouts_item', function(affiliatePayoutId) {
    check(affiliatePayoutId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return AffiliatePayouts.find({
                _id: affiliatePayoutId
            });
        },
        children: [
            {
                collectionName: "affiliates",
                find: function (affiliatePayout) {
                    return Meteor.users.find({
                        _id: affiliatePayout.affiliateId
                    });
                }
            },
            {
                find: function (affiliatePayout) {
                    return AffiliatePayoutBatches.find({
                        _id: affiliatePayout.affiliatePayoutBatchId
                    });
                }
            },
            {
                find: function (affiliatePayout) {
                    return Receipts.find({
                        _id: {
                            $in: affiliatePayout.receiptIds || affiliatePayout.originalReceiptIds || []
                        }
                    });
                },
                children: [
                    {
                        find: function (receipt) {
                            return Shops.find({
                                _id: receipt.shopId
                            });
                        }
                    }
                ]
            }
        ]
    };
});

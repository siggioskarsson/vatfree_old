/* global AffiliatePayouts: true, AffiliatePayoutBatches: true */

Meteor.publishComposite('affiliate-payout-batch', function(affiliatePayoutBatchId) {
    check(affiliatePayoutBatchId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return AffiliatePayoutBatches.find({
                _id: affiliatePayoutBatchId
            });
        },
        children: [
            {
                find: function (affiliatePayoutBatch) {
                    return AffiliatePayouts.find({
                        _id: {
                            $in: affiliatePayoutBatch.affiliatePayoutIds
                        }
                    });
                },
                children: [
                    {
                        collectionName: "affiliates",
                        find: function (affiliatePayout) {
                            return Affiliates.find({
                                _id: affiliatePayout.affiliateId
                            },{
                                fields: {
                                    profile: 1
                                }
                            });
                        }
                    },
                    {
                        find: function (affiliatePayout) {
                            return Receipts.find({
                                _id: {
                                    $in: affiliatePayout.receiptIds || affiliatePayout.originalReceiptIds || []
                                }
                            },{
                                fields: {
                                    receiptNr: 1
                                }
                            });
                        }
                    }
                ]
            }
        ]
    }
});

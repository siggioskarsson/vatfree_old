FlowRouter.route('/affiliatePayouts', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "affiliatePayouts"});
    }
});

FlowRouter.route('/affiliatePayout/:affiliatePayoutId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "affiliatePayoutEdit"});
    }
});

FlowRouter.route('/affiliatePayout/affiliatePayout-batch/:affiliatePayoutBatchId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "affiliatePayoutBatch"});
    }
});

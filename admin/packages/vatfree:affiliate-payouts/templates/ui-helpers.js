import moment from "moment";

UI.registerHelper('getAffiliatePayoutIcon', function(status) {
    status = status || this.status;

    let icon = Vatfree.payouts.icons();
    return _.has(icon, status) ? icon[status] : "circle-o";
});

UI.registerHelper('getAffiliatePayoutStatusOptions', function() {
    return _.clone(AffiliatePayouts.simpleSchema()._schema.status.allowedValues);
});

UI.registerHelper('getAffiliatePayoutName', function(affiliatePayoutId) {
    if (!affiliatePayoutId) affiliatePayoutId = this.affiliatePayoutId;

    let affiliatePayout = AffiliatePayouts.findOne({_id: affiliatePayoutId});
    if (affiliatePayout) {
        return affiliatePayout.affiliatePayoutNr;
    }

    return "";
});

UI.registerHelper('getAffiliatePayoutDescription', function(affiliatePayoutId) {
    if (!affiliatePayoutId) affiliatePayoutId = this.affiliatePayoutId;

    let affiliatePayout = AffiliatePayouts.findOne({_id: affiliatePayoutId});
    if (affiliatePayout) {
        return affiliatePayout.affiliatePayoutNr + ' - ' + moment(affiliatePayout.createdAt).format(TAPi18n.__('_date_format'));
    }

    return "";
});

/* global affiliatePayoutHelpers: true */
import moment from 'moment';

affiliatePayoutHelpers = {
    getAffiliatePayoutSelector(template) {
        let selector = {
            deleted: {
                $ne: true
            }
        };

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }
        Vatfree.search.addStatusFilter.call(template, selector);
        Vatfree.search.addListFilter.call(template, selector);
        if (_.isFunction(template.selectorFunction)) template.selectorFunction(selector);

        let paymentMethodFilter = template.paymentMethodFilter.array() || [];
        if (paymentMethodFilter.length > 0) {
            selector.paymentMethod = {
                $in: paymentMethodFilter
            };
        }

        return selector
    },
    getAffiliatePayouts() {
        let template = Template.instance();
        let selector = affiliatePayoutHelpers.getAffiliatePayoutSelector(template);

        return AffiliatePayouts.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    getAffiliate() {
        return Affiliates.findOne({_id: this.affiliateId});
    },
    daysOpen() {
        let mOpen = this.paidAt ? moment(this.paidAt) : moment(this.createdAt);
        return (this.affiliatePayoutDate ? moment(this.affiliatePayoutDate) : moment()).diff(mOpen, 'days');
    },
    getPaymentMethodIcon() {
        switch (this.paymentMethod) {
            case 'creditcard':
                return 'creditcard';
            case 'paypal':
                return 'paypal';
            case 'sepa':
                return 'bank';
            case 'wechat':
                return 'wechatpay';
            case 'alipay':
                return 'alipay';
        }
    },
    getOpenAffiliatePayoutAmount() {
        return this.amount;
    }
};

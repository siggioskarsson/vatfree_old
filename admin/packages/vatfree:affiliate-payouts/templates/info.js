Template.affiliatePayoutInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.affiliatePayoutInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {affiliatePayoutId: data._id}, null, limit, offset);
        this.subscribe('activity-stream', {}, data._id, limit, offset);
        this.subscribe('companies_item', data.companyId || "");
    });
});

Template.affiliatePayoutInfo.helpers(affiliatePayoutHelpers);
Template.affiliatePayoutInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            $or: [
                {
                    'securityContext.affiliatePayoutId': this._id
                },
                {
                    'activity.target.itemId': this._id
                }
            ]
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    },
    getAffiliatePayoutBatch() {
        return AffiliatePayoutBatches.findOne({_id: this.affiliatePayoutBatchId});
    }
});

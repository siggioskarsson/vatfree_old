import moment from 'moment';

Template.affiliatePayoutEdit.onCreated(function () {
});

Template.affiliatePayoutEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('affiliatePayouts_item', FlowRouter.getParam('affiliatePayoutId'));
    });
});

Template.affiliatePayoutEdit.helpers({
    affiliatePayoutDoc() {
        return AffiliatePayouts.findOne({
            _id: FlowRouter.getParam('affiliatePayoutId')
        });
    }
});

Template.affiliatePayoutEditForm.onCreated(function () {
    this.disableButtons = new ReactiveVar();
    this.receiptSort = new ReactiveVar({
        receiptNr: -1
    });
});

Template.affiliatePayoutEditForm.onRendered(function () {
    Tracker.afterFlush(() => {
        affiliatePayoutEditFormAfterFlush.call(this);
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('affiliatePayout-receipts', data._id);
        this.subscribe('activity-logs', "", {affiliatePayoutId: data._id});
    });
});

Template.affiliatePayoutEditForm.helpers({
    isStatus(status) {
        let template = Template.instance();
        return template.data.status === status;
    },
    isDisabled() {
        return this.status !== 'preCheck';
    },
    getReceipts() {
        let template = Template.instance();
        let sort = template.receiptSort.get();
        return Receipts.find({
            _id: {
                $in: (this.receiptIds || this.originalReceiptIds)
            }
        }, {
            sort: sort
        });
    },
    getReceiptSorting() {
        return Template.instance().receiptSort.get();
    },
    getActivityLog() {
        return ActivityLogs.find({
            affiliatePayoutId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    disableButtons() {
        return Template.instance().disableButtons.get();
    },
    getPaymentMethods() {
        return Vatfree.payments.methods;
    }
});

Template.affiliatePayoutEditForm.events({
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/affiliatePayouts');
    },
    'click .cancel-affiliatePayout'(e) {
        e.preventDefault();
        let affiliatePayoutId = this._id;
        let receiptIds = this.receiptIds;

        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "The affiliatePayout will be cancelled!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: true
        }, function () {
            AffiliatePayouts.update({
                _id: affiliatePayoutId
            },{
                $set: {
                    status: 'cancelled',
                    originalReceiptIds: receiptIds
                },
                $unset: {
                    receiptIds: 1
                }
            });
        });
    },
    'submit form[name="edit-affiliatePayout-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        // fix dates
        if (_.has(formData, 'affiliatePayoutDate')) {
            if (formData.affiliatePayoutDate) {
                formData.affiliatePayoutDate = moment(formData.affiliatePayoutDate, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.affiliatePayoutDate = null;
            }
        }

        // fix numbers
        if (_.has(formData, 'amount')) formData.amount = Math.round(Number(formData.amount)*100);
        if (_.has(formData, 'totalVat')) formData.totalVat = Math.round(Number(formData.totalVat)*100);
        if (_.has(formData, 'serviceFee')) formData.serviceFee = Math.round(Number(formData.serviceFee)*100);
        if (_.has(formData, 'refund')) formData.refund = Math.round(Number(formData.refund)*100);

        AffiliatePayouts.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('AffiliatePayout saved!')
            }
        });
    }
});

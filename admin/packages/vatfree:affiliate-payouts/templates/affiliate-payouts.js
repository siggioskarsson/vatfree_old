import './affiliate-payouts.html';

Template.affiliatePayouts.onCreated(Vatfree.templateHelpers.onCreated(AffiliatePayouts, '/affiliatePayouts/', '/affiliatePayout/:itemId'));
Template.affiliatePayouts.onRendered(Vatfree.templateHelpers.onRendered());
Template.affiliatePayouts.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.affiliatePayouts.helpers(Vatfree.templateHelpers.helpers);
Template.affiliatePayouts.helpers(affiliatePayoutHelpers);
Template.affiliatePayouts.events(Vatfree.templateHelpers.events());

Template.affiliatePayouts.onCreated(function() {
    this.paymentMethodFilter = new ReactiveArray();
    _.each(Session.get(this.view.name + '.paymentMethodFilter') || [], (paymentMethod) => {
        this.paymentMethodFilter.push(paymentMethod);
    });

    this.autorun(() => {
        Session.setPersistent(this.view.name + '.paymentMethodFilter', this.paymentMethodFilter.array());
    });

    this.selectorFunction = (selector) => {
        let paymentMethodFilter = this.paymentMethodFilter.array() || [];
        if (paymentMethodFilter.length > 0) {
            selector.paymentMethod = {
                $in: paymentMethodFilter
            };
        }
    };

    this.todoStatus = [
        'new'
    ];
    this.sort.set({
        affiliatePayoutNr: -1
    });

    this.subscribe('debt-collection-lines', "");
});

Template.affiliatePayouts.helpers({
    isActivePaymentMethodFilter(method) {
        let paymentMethodFilter = Template.instance().paymentMethodFilter.array() || [];
        return _.contains(paymentMethodFilter, method) ? 'active' : '';
    },
    getAffiliatePayoutStats() {
        let template = Template.instance();
        let selector = affiliatePayoutHelpers.getAffiliatePayoutSelector(template);

        let stats = ReactiveMethod.call('get-affiliatePayout-stats', selector);
        return stats && stats[0] ? stats[0] : false;
    },
});

Template.affiliatePayouts.events({
    'click .filter-icon.payment-method-filter-icon'(e, template) {
        e.preventDefault();
        let method = $(e.currentTarget).attr('id');
        let paymentMethodFilter = template.paymentMethodFilter.array();

        if (_.contains(paymentMethodFilter, method)) {
            template.paymentMethodFilter.clear();
            _.each(paymentMethodFilter, (_method) => {
                if (method !== _method) {
                    template.paymentMethodFilter.push(_method);
                }
            });
        } else {
            template.paymentMethodFilter.push(method);
        }
    },
    'click .affiliatePayout-create-new'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).attr('disabled') === 'disabled') {
            return false;
        }

        if (confirm('Do you want to create an export file and mark the affiliatePayouts as sent?')) {
            Meteor.call('create-affiliatePayout-batch', (err, affiliatePayoutBatchIds) => {
                if (err) {
                    alert(err.reason || err.message);
                } else {
                    if (affiliatePayoutBatchIds) {
                        FlowRouter.go('/affiliatePayout/affiliatePayout-batch/' + affiliatePayoutBatchIds.join(','));
                    } else {
                        toastr.warning('No affiliatePayouts found');
                    }
                }
            });
        }
    }
});

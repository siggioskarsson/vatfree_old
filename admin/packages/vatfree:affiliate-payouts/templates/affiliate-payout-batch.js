Template.affiliatePayoutBatch.onCreated(function() {
    let affiliatePayoutBatchIds = FlowRouter.getParam('affiliatePayoutBatchId').split(',');
    _.each(affiliatePayoutBatchIds, (affiliatePayoutBatchId) => {
        this.subscribe('affiliatePayout-batch', affiliatePayoutBatchId);
    });
});

Template.affiliatePayoutBatch.helpers({
    getAffiliatePayoutBatches() {
        let affiliatePayoutBatchIds = FlowRouter.getParam('affiliatePayoutBatchId').split(',');
        return AffiliatePayoutBatches.find({
            _id: {
                $in: affiliatePayoutBatchIds
            }
        });
    },
    totalStats() {
        let affiliatePayoutBatchIds = FlowRouter.getParam('affiliatePayoutBatchId').split(',');
        if (affiliatePayoutBatchIds.length > 1) {
            let stats = {
                count: 0,
                amount: 0
            };

            AffiliatePayoutBatches.find({
                _id: {
                    $in: affiliatePayoutBatchIds
                }
            }).forEach((affiliatePayoutBatch) => {
                stats.count += affiliatePayoutBatch.affiliatePayoutIds.length;
                stats.amount += Number(affiliatePayoutBatch.amount);
            });

            return stats;
        }

        return false;
    }
});

Template.affiliatePayoutBatch_details.helpers({
    affiliatePayouts() {
        let affiliatePayouts = [];
        let index = 0;
        AffiliatePayouts.find({
            affiliatePayoutBatchId: this._id
        },{
            sort: {
                createdAt: 1
            }
        }).forEach((affiliatePayout) => {
            affiliatePayout.index = index++;
            affiliatePayouts.push(affiliatePayout);
        });
        return affiliatePayouts.length > 0 ? affiliatePayouts : false;
    },
    getReceiptNr() {
        let receipt = Receipts.findOne({
            _id: this.receiptId
        });
        if (receipt) {
            return receipt.receiptNr;
        }
    },
    getDataContext() {
        return {
            paymentMethod: Template.instance().data.paymentMethod,
            affiliatePayouts: this,
            affiliatePayoutBatch: Template.instance().data
        }
    },
    getAffiliatePayoutBatchTemplate() {
        return "payoutBatch_details_" + Template.instance().data.paymentMethod;
    },
    isType(type) {
        return this.paymentMethod === type;
    },
    amountWithFee() {
        return Math.ceil(this.amount*1.02);
    }
});

Template.affiliatePayoutBatch_details.events({
    'click .save-affiliatePayout-batch'(e, template) {
        e.preventDefault();

        let textToWrite = template.$('textarea').val();
        let textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
        let fileNameToSaveAs = template.data.affiliatePayoutBatchNumber + '-' + template.data.paymentMethod;
        switch (template.data.paymentMethod) {
            case 'creditcard':
                fileNameToSaveAs += '.SET';
                break;
            default:
                fileNameToSaveAs += '.csv';
        }

        Vatfree.templateHelpers.downloadFile(fileNameToSaveAs, textFileAsBlob);
    },
    'click .save-affiliatePayout-batch-sepa-xml'(e, template) {
        e.preventDefault();
        import handlebars from 'handlebars';
        handlebars.registerHelper('formatDate', Vatfree.dates.formatDate);
        handlebars.registerHelper('formatAmount', Vatfree.numbers.formatAmount);
        handlebars.registerHelper('toUpperCase', function(string) {
            return (string || "").toString().toUpperCase();
        });
        handlebars.registerHelper('getAffiliateName', function() {
            let user = Affiliates.findOne({_id: this.affiliateId});
            if (user) {
                return user.profile.name || "";
            }
        });
        handlebars.registerHelper('getReceiptNrs', function() {
            return this.affiliatePayoutNr;
        });

        Meteor.call('get-sepa-xml-template', (err, xmlTemplate) => {
            let templateData = template.data;

            templateData.getAffiliatePayouts = [];
            let index = 0;
            let sumOfAffiliatePayouts = 0;
            AffiliatePayouts.find({
                affiliatePayoutBatchId: templateData._id
            },{
                sort: {
                    createdAt: 1
                }
            }).forEach((affiliatePayout) => {
                affiliatePayout.index = index++;
                sumOfAffiliatePayouts += affiliatePayout.amount;
                templateData.getAffiliatePayouts.push(affiliatePayout);
            });
            templateData.numberOfAffiliatePayouts = templateData.getAffiliatePayouts.length;
            templateData.sumOfAffiliatePayouts = sumOfAffiliatePayouts;

            const textToWrite = handlebars.compile(xmlTemplate)(templateData);

            let textFileAsBlob = new Blob([textToWrite], {type:'application/xml'});
            let fileNameToSaveAs = template.data.affiliatePayoutBatchNumber + '-' + template.data.paymentMethod + '.xml';

            Vatfree.templateHelpers.downloadFile(fileNameToSaveAs, textFileAsBlob);
        });
    },
    'click .mark-as-paid'(e, template) {
        e.preventDefault();
        let affiliatePayoutBatchId = template.data._id;
        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "Do you want to mark the affiliatePayout as paid?!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: false
        }, function () {
            AffiliatePayoutBatches.update({
                _id: affiliatePayoutBatchId
            },{
                $set: {
                    paidAt: new Date(),
                    paidBy: Meteor.userId()
                }
            });
            swal("Paid!", "AffiliatePayout has been marked as paid.", "success");
        });
        }
});

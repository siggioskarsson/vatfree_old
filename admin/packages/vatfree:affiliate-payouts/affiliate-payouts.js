/* global AffiliatePayouts: true */
import moment from 'moment';

AffiliatePayouts = new Meteor.Collection('affiliate-payouts');
AffiliatePayouts.attachSchema(new SimpleSchema({
    affiliatePayoutNr: {
        type: String,
        label: "Internally generated unique affiliate payout number",
        autoValue: function() {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return getNextAffiliatePayoutNumber();
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: getNextAffiliatePayoutNumber()
                    };
                } else {
                    this.unset();
                }
            } else {
                this.unset();
            }
        },
        optional: false
    },
    affiliateId: {
        type: String,
        label: "Affiliate that request this payout",
        optional: false
    },
    receiptIds: {
        type: [String],
        label: "A list of receipts included on this affiliatePayout",
        optional: true
    },
    originalReceiptIds: {
        type: [String],
        label: "A list of the original receipts included on this affiliatePayout, if status = cancelled",
        optional: true
    },
    currencyId: {
        type: String,
        label: "Currency of this affiliatePayout",
        optional: false
    },
    paymentMethod: {
        type: String,
        label: "Type of affiliatePayout requested",
        allowedValues: Vatfree.payments.methods,
        optional: false
    },
    paymentNr: {
        type: String,
        label: "Payment nr / identifier",
        optional: false
    },
    paymentAccountName: {
        type: String,
        label: "Optional account name if applicable",
        optional: true
    },
    amount: {
        type: Number,
        label: "Total amount of affiliatePayout",
        optional: false
    },
    costs: {
        type: Number,
        label: "Total costs of the affiliatePayout",
        optional: true
    },
    status: {
        type: String,
        label: "Status of the affiliatePayout",
        optional: false,
        defaultValue: 'new',
        allowedValues: [
            'preCheck',
            'new',
            'sentToPaymentProvider',
            'partiallyPaid',
            'paid',
            'cancelled'
        ]
    },
    statusDates: {
        type: Object,
        label: "All the last dates of the status",
        optional: true
    },
    payoutDate: {
        type: Date,
        label: "AffiliatePayout date of this affiliatePayout",
        optional: true
    },
    paidAt: {
        type: Date,
        label: "When the affiliatePayout was marked as paid",
        optional: true
    },
    paidBy: {
        type: String,
        label: "By whom this batch was marked as paid",
        optional: true
    },
    affiliatePayoutBatchId: {
        type: String,
        label: "The affiliatePayout batch this affiliatePayout was a part of",
        optional: true
    },
    notes: {
        type: String,
        label: "AffiliatePayout notes",
        optional: true
    },
    deleted: {
        type: Boolean,
        label: "Whether the affiliatePayout was deleted",
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
AffiliatePayouts.attachSchema(CreatedUpdatedSchema);
StatusDates.attachToSchema(AffiliatePayouts);

var getNextAffiliatePayoutNumber = function() {
    let year = moment().format('YY').toString();
    let counter = Number(incrementCounter(Counters, 'affiliatePayoutNr_' + year)).padLeft(7).toString();

    return "AP" + year + counter;
};

AffiliatePayouts.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

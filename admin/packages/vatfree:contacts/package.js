Package.describe({
    name: 'vatfree:contacts',
    summary: 'Vatfree contacts package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core',
        'vatfree:countries',
        'vatfree:email-templates'
    ]);

    api.addAssets([
    ], 'server');

    // shared files
    api.addFiles([
    ]);

    // server files
    api.addFiles([
        'server/methods.js',
        'server/contacts.js',
        'server/import.js',
        'publish/contacts.js',
        'publish/contact.js'
    ], 'server');

    // client files
    api.addFiles([
        'client/lib/contacts.js',
        'router.js',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/import.html',
        'templates/import.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/contacts.html',
        'templates/contacts.js'
    ], 'client');

    api.export([
        'Contacts'
    ]);
});

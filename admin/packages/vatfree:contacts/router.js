FlowRouter.route('/contacts', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "Contacts"});
    }
});

FlowRouter.route('/contact/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "ContactEdit"});
    }
});

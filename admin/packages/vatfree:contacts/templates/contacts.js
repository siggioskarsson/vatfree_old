var FileSaver = require('file-saver');
import moment from 'moment';
import { contactHelpers } from './helpers';

Template.Contacts.onCreated(Vatfree.templateHelpers.onCreated(Contacts, '/contacts/', '/contact/:itemId'));
Template.Contacts.onCreated(function() {
    this.showImports = new ReactiveVar();
    this.runningExport = new ReactiveVar();

    this.typeFilter = new ReactiveArray();
    _.each(Session.get(this.view.name + '.typeFilter') || [], (type) => {
        this.typeFilter.push(type);
    });
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.typeFilter', this.typeFilter.array());
    });

    this.selectorFunction = function(selector) {
        let typeFilter = this.typeFilter.array() || [];
        if (typeFilter.length > 0) {
            _.each(typeFilter, (tFilter) => {
                selector['profile.' + tFilter] = {
                    $exists: true
                };
            });
        }
    };
});

Template.Contacts.onRendered(Vatfree.templateHelpers.onRendered());
Template.Contacts.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.Contacts.helpers(Vatfree.templateHelpers.helpers);
Template.Contacts.helpers(contactHelpers);
Template.Contacts.helpers({
    showImports() {
        return Template.instance().showImports.get();
    },
    isContactTypeFilter(type) {
        const typeFilter = Template.instance().typeFilter.array() || [];
        return _.contains(typeFilter, type) ? 'active' : '';
    },
    getContactStats() {
        const template = Template.instance();
        const selector = contactHelpers.getContactsSelector(template);

        const stats = ReactiveMethod.call('get-contacts-stats', selector);
        return stats && stats[0] ? stats[0].value : false;
    }
});

Template.Contacts.events(Vatfree.templateHelpers.events());
Template.Contacts.events({
    'click .filter-icon.type-filter-icon'(e, template) {
        e.preventDefault();
        let type = $(e.currentTarget).attr("id");
        let typeFilter = template.typeFilter.array();

        if (_.contains(typeFilter, type)) {
            template.typeFilter.clear();
            _.each(typeFilter, (_type) => {
                if (type !== _type) {
                    template.typeFilter.push(_type);
                }
            });
        } else {
            template.typeFilter.push(type);
        }
    },
    'click .contacts-import'(e, template) {
        e.preventDefault();
        template.showImports.set(!template.showImports.get());
    },
    'click .contact-email-export'(e, template) {
        e.preventDefault();
        template.runningExport.set(true);

        const getContactExport = function (includeShopsAndCompanies) {
            let selector = contactHelpers.getContactsSelector(template);
            Meteor.setTimeout(() => {
                Meteor.call('contact-email-export', selector, includeShopsAndCompanies, (err, csv) => {
                    if (err) {
                        toastr.error(err.reason || err.message);
                    } else {
                        var blob = new Blob([csv], {
                            type: 'text/plain;charset=utf-8'
                        });
                        FileSaver.saveAs(blob, 'contact_email_export_' + moment().format(TAPi18n.__('_date_time_format')) + '.csv');
                    }
                    template.runningExport.set(false);
                });
            }, 1);
        };

        import swal from "sweetalert2";
        swal({
            title: "Include shops and companies?",
            text: "Do you want to include all shop and company emails that match your search?",
            type: "info",
            showCloseButton: true,
            showCancelButton: true,
            cancelButtonText: "No, thank you",
            confirmButtonText: "Yes!",
            confirmButtonColor: '#ed5565',
            reverseButtons: true,
            closeOnConfirm: false
        }).then(function (isConfirm) {
            getContactExport(true);
        }, function(dismiss) {
            if (dismiss === 'cancel') {
                getContactExport(false);
            } else {
                template.runningExport.set(false);
            }
        });
    }
});

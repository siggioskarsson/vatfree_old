import { contactHelpers } from './helpers';

Template.ContactInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.ContactInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('shops_item', data.profile.shopId || "");
        this.subscribe('companies_item', data.profile.companyId || "");
    });
});

Template.ContactInfo.helpers(contactHelpers);
Template.ContactInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

UI.registerHelper('getContactDescription', function(contactId) {
    if (!contactId) contactId = this.contactId || this.profile.contactId;

    let contact = Contacts.findOne({_id: contactId});
    if (contact) {
        let country = Countries.findOne({_id: contact.profile.countryId}) || {};
        return contact.profile.name + ' - ' + contact.profile.addressFirst + ', ' + contact.profile.postalCode + ' ' + contact.profile.city + ', ' + country.name;
    }

    return "";
});

UI.registerHelper('getContactName', function(contactId) {
    if (!contactId) contactId = this.contactId || this.profile.contactId;

    let contact = Contacts.findOne({_id: contactId});
    if (contact) {
        return contact.profile.name;
    }

    return "";
});

/* global contactHelpers: true */

export const contactHelpers = {
    getContactsSelector(template) {
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
            let searchTerms = searchTerm.split(' ');

            selector['$and'] = [];
            _.each(searchTerms, (s) => {
                selector['$and'].push({textSearch: new RegExp(s)});
            });
        }

        Vatfree.search.addListFilter.call(template, selector);
        template.selectorFunction(selector);

        console.log(selector);

        return selector;
    },
    getContacts() {
        const template = Template.instance();
        const selector = contactHelpers.getContactsSelector(template);

        return Contacts.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    getShopName(shopId) {
        let shop = Shops.findOne({_id: shopId || this.profile.shopId});
        if (shop) {
            return shop.name + ' - ' + (shop.addressFirst || "") + ', ' + (shop.postCode || "") + ' ' + (shop.city || "");
        }
    }
};

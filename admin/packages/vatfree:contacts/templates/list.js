import { contactHelpers } from './helpers';

Template.contactsList.helpers(Vatfree.templateHelpers.helpers);
Template.contactsList.helpers(contactHelpers);
Template.contactsList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/contact/:itemId', {itemId: this._id});
    },
    'click .list-action'(e, template) {
        e.preventDefault();
        if (_.isFunction(this.action)) {
            this.action($(e.currentTarget).attr('id'))
        }
    }
});

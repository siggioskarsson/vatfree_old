Template.addContactModal.onCreated(function() {
    let template = this;
    let parentVariable = this.data.parentVariable || 'addingItem';

    this.ignoreShop = new ReactiveVar();
    this.ignoreCompany = new ReactiveVar();
    this.ignoreAffiliate = new ReactiveVar();

    this.autorun(() => {
        let data = Template.currentData();
        this.ignoreShop.set(data.shopId === false);
        this.ignoreCompany.set(data.companyId === false);
        this.ignoreAffiliate.set(data.affiliateId === false);
    });

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-contact').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-contact')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-add-contact').on('shown.bs.modal', function () {
            template.$('select[name="countryId"]').select2();
            template.$('select[name="shopId"]').select2({
                minimumInputLength: 1,
                multiple: false,
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-shops', params.data.q || "", limit, offset, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });
            template.$('select[name="companyId"]').select2({
                minimumInputLength: 1,
                multiple: false,
                allowClear: true,
                placeholder: {
                    id: "",
                    placeholder: "Select company ..."
                },
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-companies', params.data.q || "", limit, offset, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });
            template.$('select[name="affiliateId"]').select2({
                minimumInputLength: 1,
                multiple: false,
                allowClear: true,
                placeholder: {
                    id: "",
                    placeholder: "Select affiliate ..."
                },
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-affiliates', params.data.q || "", limit, offset, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });

            template.$('input[name="name"]').focus();
        });
        $('#modal-add-contact').modal('show');
    });
});

Template.addContactModal.onDestroyed(function() {
    this.hideModal();

    $('body').removeClass('modal-open');
    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.addContactModal.helpers({
    ignoreShop() {
        return Template.instance().ignoreShop.get();
    },
    ignoreCompany() {
        return Template.instance().ignoreCompany.get();
    },
    ignoreAffiliate() {
        return Template.instance().ignoreAffiliate.get();
    }
});

Template.addContactModal.events({
    'click .cancel-add-contact'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-contact-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        Meteor.call('contacts-insert', formData, (err, contactId) => {
            if (err) {
                toastr.error(err.message);
            } else {
                template.hideModal();
                if (template.data.doNotRedirect !== true) {
                    FlowRouter.go('/contact/' + contactId);
                }
            }
        });
    }
});

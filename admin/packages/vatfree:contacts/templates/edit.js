import { contactHelpers } from './helpers';

Template.ContactEdit.onCreated(function () {

});

Template.ContactEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('contacts_item', FlowRouter.getParam('itemId'));
        this.subscribe('email-templates-list', "", {}, {_id: 1}, 1000, 0); // get all email templates
    });
});

Template.ContactEdit.helpers({
    itemDoc() {
        return Contacts.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    }
});

Template.contactEditForm.onCreated(function() {
    this.shopId = new ReactiveVar(this.data.profile.shopId);
    this.companyId = new ReactiveVar(this.data.profile.companyId);
    this.vatRates = new ReactiveArray();
    this.addingActivityLog = new ReactiveVar();

    this.handleFileUpload = function (file) {
        Vatfree.templateHelpers.handleFileUpload(file, {
            itemId: FlowRouter.getParam('itemId'),
            target: 'contacts',
            category: 'logo'
        });
    };
});

Template.contactEditForm.onRendered(function() {
    this.autorun(() => {
        this.subscribe('shops_item', this.shopId.get() || "");
    });
    this.autorun(() => {
        this.subscribe('companies_item', this.companyId.get() || "");
    });
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('activity-logs', "", {contactId: data._id});
    });

    this.autorun(() => {
        let data = Template.currentData();
        let shopId = this.shopId.get();
        let companyId = this.companyId.get();
        if (this.subscriptionsReady()) {
            Tracker.afterFlush(() => {
                this.$('select[name="countryId"]').select2();

                this.$('select[name="shopId"]').select2({
                    minimumInputLength: 1,
                    multiple: false,
                    allowClear: true,
                    placeholder: {
                        id: "",
                        placeholder: "Select shop ..."
                    },
                    ajax: {
                        transport: function (params, success, failure) {
                            let limit = 20;
                            let offset = 0;
                            Meteor.call('search-shops', params.data.q || "", limit, offset, (err, res) => {
                                if (err) {
                                    failure(err);
                                } else {
                                    success({results: res});
                                }
                            });
                        },
                        delay: 500
                    }
                });
                this.$('select[name="companyId"]').select2({
                    minimumInputLength: 1,
                    multiple: false,
                    allowClear: true,
                    placeholder: {
                        id: "",
                        placeholder: "Select company ..."
                    },
                    ajax: {
                        transport: function (params, success, failure) {
                            let limit = 20;
                            let offset = 0;
                            Meteor.call('search-companies', params.data.q || "", limit, offset, (err, res) => {
                                if (err) {
                                    failure(err);
                                } else {
                                    success({results: res});
                                }
                            });
                        },
                        delay: 500
                    }
                });
                this.$('select[name="affiliateId"]').select2({
                    minimumInputLength: 1,
                    multiple: false,
                    allowClear: true,
                    placeholder: {
                        id: "",
                        placeholder: "Select affiliate ..."
                    },
                    ajax: {
                        transport: function (params, success, failure) {
                            let limit = 20;
                            let offset = 0;
                            Meteor.call('search-affiliates', params.data.q || "", limit, offset, (err, res) => {
                                if (err) {
                                    failure(err);
                                } else {
                                    success({results: res});
                                }
                            });
                        },
                        delay: 500
                    }
                });
            });
        }
    });
});

Template.contactEditForm.helpers(contactHelpers);
Template.contactEditForm.helpers({
    isCountrySelected() {
        let template = Template.instance();
        if (template.data) {
            return template.data.profile.countryId === this._id;
        }
    },
    isEmailTemplateSelected() {
        let template = Template.instance();
        return template.data.profile.emailTemplateId === this._id;
    },
    addingActivityLog() {
        return Template.instance().addingActivityLog.get();
    },
    getActivityLog() {
        return ActivityLogs.find({
            contactId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getFileContext() {
        return {
            itemId: this._id,
            target: 'contacts'
        };
    }
});

Template.contactEditForm.events(Vatfree.templateHelpers.events());
Template.contactEditForm.events({
    'change select[name="shopId"]'(e, template) {
        let shopId = $(e.currentTarget).val();
        template.shopId.set(shopId);
    },
    'change select[name="companyId"]'(e, template) {
        let companyId = $(e.currentTarget).val();
        template.companyId.set(companyId);
    },
    'click .add-activity-log'(e, template) {
        e.preventDefault();
        template.addingActivityLog.set(true);
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/contacts');
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template, $(e.currentTarget));

        let setData = {
            $set: formData
        };

        if (!formData.shopId) {
            if (!setData['$unset']) setData['$unset'] = {};
            setData['$unset'].shopId = 1;
        }
        if (!formData.companyId) {
            if (!setData['$unset']) setData['$unset'] = {};
            setData['$unset'].companyId = 1;
        }
        if (!formData.affiliateId) {
            if (!setData['$unset']) setData['$unset'] = {};
            setData['$unset'].affiliateId = 1;
        }

        Meteor.call('contacts-update', this._id, setData, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Contact saved!')
            }
        });
    }
});

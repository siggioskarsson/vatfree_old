Template.contactsImport.events(Vatfree.templateHelpers.events());
Template.contactsImport.onCreated(function() {
    this.handleFileUpload = function(file) {
        Vatfree.templateHelpers.handleFileUpload(file, {
            target: 'imports',
            subType: 'contacts',
            importInfo: {
                running: false,
                progress: 0,
                progressMessage: ''
            }
        });
    }
});

Template.contactsImport.onRendered(function() {
    this.autorun(() => {
        this.subscribe('imports', 'contacts');
    });
});

Template.contactsImport.helpers({
    getImports() {
        return Files.find({
            target: 'imports',
            subType: 'contacts'
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getStatus() {
        if (this.importInfo.progress === 100) {
            return "Done";
        } else if (this.importInfo.running) {
            return "Running";
        } else {
            return "-";
        }
    },
    getError() {
        if (_.isObject(this.error)) {
            return this.error.code;
        } else {
            return this.error;
        }
    }
});

Template.contactsImport.events({
    'click .import-file'(e, template) {
        e.preventDefault();
        Meteor.call('import-contacts', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.info("Import started");
            }
        });
    }
});

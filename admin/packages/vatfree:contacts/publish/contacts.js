Meteor.publishComposite('contacts', function(searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};
    selector['roles.__global_roles__'] = 'contact';

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        collectionName: "contacts",
        find: function () {
            return Contacts.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                find: function (item) {
                    return Files.find({
                        itemId: item._id,
                        target: 'contacts',
                        category: 'logo'
                    });
                }
            }
        ]
    }
});

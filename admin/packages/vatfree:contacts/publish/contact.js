Meteor.publishComposite('contacts_item', function (itemId) {
    check(itemId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        collectionName: "contacts",
        find: function () {
            return Meteor.users.find({
                _id: itemId,
                'roles.__global_roles__': 'contact'
            });
        },
        children: [
            {
                find: function (item) {
                    return Shops.find({
                        _id: item.profile.shopId
                    });
                }
            },
            {
                find: function (item) {
                    return Companies.find({
                        _id: item.profile.companyId
                    });
                }
            },
            {
                collectionName: "affiliates",
                find: function (item) {
                    return Affiliates.find({
                        _id: item.profile.affiliateId
                    });
                }
            },
            {
                find: function (item) {
                    return Files.find({
                        itemId: item._id,
                        target: 'contacts'
                    });
                }
            }
        ]
    }
});

import { Meteor } from "meteor/meteor";

Meteor.methods({
    'get-contact'(contactId) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Contacts.findOne({_id: contactId});
    },
    'search-contacts'(searchTerm, limit, offset) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {};
        selector['roles.__global_roles__'] = 'contact';
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let contacts = [];
        Contacts.find(selector, {
            sort: {
                'profile.name': 1
            },
            limit: limit,
            offset: offset
        }).forEach((contact) => {
            let entity = "";
            if (contact.profile.shopId) {
                entity = Shops.findOne({_id: contact.profile.shopId});
            } else if (contact.profile.companyId) {
                entity = Companies.findOne({_id: contact.profile.companyId});
            }
            contacts.push({
                text: contact.profile.name + (entity ? ' - ' + entity.name : '') + ' (' + (contact.profile.email ? contact.profile.email + ', ' : "") + (contact.profile.tel ? contact.profile.tel + ', ' : "") + ')',
                id: contact._id
            });
        });

        return contacts;
    },
    'get-contacts-stats'(selector) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        let count = Contacts.find(selector).count();
        return [
            {
                _id: "contacts",
                value: {
                    count: count
                }
            }
        ];
    },
    'contacts-insert'(profile) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let privateData = {};
        if (profile.notes) {
            privateData.notes = profile.notes;
            delete profile.notes;
        }
        return Contacts.insert({
            profile: profile,
            private: privateData,
            roles: {
                __global_roles__: ['contact']
            }
        });
    },
    'contacts-update'(contactId, profileData) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let setData = {};
        _.each(profileData['$set'], (value, key) => {
            setData['profile.' + key] = value;
        });
        if (setData['profile.notes']) {
            setData['private.notes'] = setData['profile.notes'];
            delete setData['profile.notes'];
        }

        let unsetData = {};
        _.each(profileData['$unset'], (value, key) => {
            unsetData['profile.' + key] = value;
        });

        return Contacts.update({
            _id: contactId
        }, {
            $set: setData,
            $unset: unsetData
        });
    },
    'contact-email-export'(selector, includeShopsAndCompanies) {
        if (!Vatfree.userIsInRole(this.userId, 'admin')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const Baby = require('babyparse');
        selector["roles.__global_roles__"] = "contact";
        selector["profile.email"] = {
            $exists: true
        };

        let data = [];
        Meteor.users.find(selector, {
            sort: {
                createdAt: 1
            },
            fields: {
                'profile.name': true,
                'profile.email': true
            }
        }).forEach((contact) => {
            if (!contact.profile.email) return;
            const csvContact = {
                name: contact.profile.name,
                email: contact.profile.email
            };
            data.push(csvContact);
        });

        if (includeShopsAndCompanies) {
            const existsSelectors = [];
            _.each(selector, (value, attr) => {
                const profileAttr = attr.split('.');
                if (profileAttr.length === 2 && profileAttr[0] === 'profile') {
                    existsSelectors.push(profileAttr[1]);
                }
            });


            let shopSelector = {
                email: {
                    $exists: true
                }
            };
            if (selector.textSearch) {
                shopSelector.textSearch = selector.textSearch;
            }

            if (_.contains(existsSelectors, 'shopId') || existsSelectors.length === 0) {
                Shops.find(shopSelector).forEach((shop) => {
                    if (!shop.email) return;
                    const shopContact = {
                        name: shop.name,
                        email: shop.email
                    };
                    data.push(shopContact);
                });
            }

            if (_.contains(existsSelectors, 'companyId') || existsSelectors.length === 0) {
                Companies.find(shopSelector).forEach((company) => {
                    if (!company.email) return;
                    const companyContact = {
                        name: company.name,
                        email: company.email
                    };
                    data.push(companyContact);
                });
            }
        }

        let csv = Baby.unparse(data);

        return csv;
    },
    'delete-shop-contact'(shopId) {
        if (!Vatfree.userIsInRole(this.userId, 'shops-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.users.update({
            _id: shopId
        },{
            $unset: {
                'profile.shopId': 1
            }
        });
    },
    'delete-company-contact'(companyId) {
        if (!Vatfree.userIsInRole(this.userId, 'companies-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.users.update({
            _id: companyId
        },{
            $unset: {
                'profile.companyId': 1
            }
        });
    }
});

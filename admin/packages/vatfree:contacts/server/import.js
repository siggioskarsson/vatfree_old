const csv = require('csvtojson');
import moment from 'moment';

Meteor.methods({
    'import-contacts'(fileId) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (!_.isUndefined(importJobs)) {
            // Create a job:
            let job = new Job(importJobs, 'imports',
                {
                    importFunction: "importContacts",
                    fileId: fileId
                }
            );
            // Set some properties of the job and then submit it
            job.priority('normal')
                .retry({
                    retries: 2,
                    wait: 5 * 60 * 1000
                })
                .save();
        }
    }
});

Vatfree.importContacts = function (contactFile, fileObject, callback) {
    throw new Meteor.Error(404, 'Deprecated function');

    Vatfree.setImportFileProgress(10, 'Populating lookup data', fileObject);

    let countries = {};
    Countries.find().forEach(function (country) {
        countries[country.code] = country;
    });

    let totalLines = Vatfree.importFileLines(contactFile);
    Vatfree.setImportFileProgress(11, 'Initializing csv', fileObject);

    let rowIndex = 0;
    csv({
        delimiter: ';',
        noheader: false,
        trim: true,
        flatKeys: true,
        constructResult: false
    }).fromFile(contactFile)
        .on('json', Meteor.bindEnvironment((row) => {
            if (rowIndex % 100 === 0) {
                console.log('progress', 11 + Math.round(89 * (rowIndex/totalLines)), 'Importing ' + rowIndex + ' of ' + totalLines);
                Vatfree.setImportFileProgress(11 + Math.round(89 * (rowIndex/totalLines)), 'Importing ' + rowIndex + ' of ' + totalLines, fileObject);
            }

            if (row &&  _.size(row) > 2) {
                let updateData = Vatfree.import.processColumns(colMapping, convert, row);

                updateData['name'] = updateData['firstName'] + ' ' + updateData['lastName'];
                let country = updateData['country'] || "NL";
                let userCountry = countries[country.substr(0, 2)];
                if (userCountry) {
                    updateData['countryId'] = userCountry._id;
                }

                //console.log('add contact', rowIndex, updateData['name']);
                saveContact(updateData);
            }
            rowIndex++;
        }))
        .on('done', Meteor.bindEnvironment((error) => {
            Vatfree.setImportFileProgress(100, 'Done', fileObject);
            if (callback) {
                callback();
            }
        }));
};

var header = `
"Contact ID";"Account ID";"Last Modified Date";"Salutation";"First Name";"Last Name";"Title";"language";"Phone";"Home Phone";"Mobile";"Comments";"Email Admin";"SG.contact";"Auth. Contact"
"003570000229Jby";"0015700001pDuDI";"4-1-2017";"";"";"P. Sterk";"";"";"020 618 1727";"";"";"";"1";"0";"0"
"00357000022Ak4p";"0015700001mKO8n";"17-11-2016";"";"";"Finanzverwaltung";"";"";"+49 5101 5859738";"";"";"";"1";"0";"0"
`;

var colMapping = {
    "Contact ID": "contactId",
    "Account ID": "accountId",
    "Last Modified Date": false,
    "Salutation": "salutation",
    "First Name": "firstName",
    "Last Name": "lastName",
    "Mailing Street": "addressFirst",
    "Mailing City": "city",
    "Mailing Zip/Postal Code": "postalCode",
    "Mailing Country": "country",
    "Title": "jobTitle",
    "language": "language",
    "Phone": "tel",
    "Home Phone": "tel2",
    "Mobile": "mobile",
    "Comments": "notes",
    "Email": "email",
    "Email Admin": "emailAdmin",
    "SG.contact": "shoppingGuideContact",
    "Auth. Contact": "authorisationContact"
};

var convert = {
    "shoppingGuideContact": "bool",
    "authorisationContact": "bool"
};

var saveContact = function (updateData) {
    let contact = Contacts.findOne({
        'contactId': updateData['contactId']
    });

    // link account - id is sales force id
    if (updateData['accountId']) {
        let company = Companies.findOne({id: updateData['accountId']},{fields: {_id: 1}});
        if (company) {
            updateData['companyId'] = company._id;
        } else {
            let shop = Shops.findOne({id: updateData['accountId']},{fields: {_id: 1}});
            if (shop) {
                updateData['shopId'] = shop._id;
            }
        }
    }

    // set all languages to English if not set
    updateData['language'] = (updateData['language'] || "en").toLowerCase();
    let contactId;
    let emailAdmin = updateData['emailAdmin'] === "1";
    if (contact) {
        contactId = contact._id;
        Contacts.update({
            _id: contact._id
        }, {
            $set: updateData
        });
    } else {
        contactId = Contacts.insert(updateData);
    }

    if (emailAdmin) {
        // link this user to the billing for the company/shop
        if (updateData['companyId']) {
            //console.log('update company billing contact', contactId);
            Companies.update({
                _id: updateData['companyId']
            },{
                $addToSet: {
                    billingContacts: contactId
                }
            });
        } else if (updateData['shopId']) {
            //console.log('update shop billing contact', contactId);
            Shops.update({
                _id: updateData['shopId']
            },{
                $addToSet: {
                    billingContacts: contactId
                }
            });
        }
    }
};

Template.directUrlImage.onCreated(function() {
    this.url = new ReactiveVar();
    this.showBackup = new ReactiveVar(false);
});

Template.directUrlImage.onRendered(function() {
    var template = this;
    this.autorun(() => {
        var data = Template.currentData();
        Meteor.call('getDirectUrl', data.fileObj._id, [data.store], function(err, res) {
            if (err) {
                console.log(err);
                template.showBackup.set(true);
            } else {
                template.url.set(res[data.store]);
            }
        });
    });
});

Template.directUrlImage.helpers({
    showBackup: function() {
        return Template.instance().showBackup.get();
    },
    directUrl: function() {
        return Template.instance().url.get();
    }
});

Template.directUrlFile.onCreated(function() {
    this.url = new ReactiveVar();
    this.thumbUrl = new ReactiveVar();
    this.showBackup = new ReactiveVar(false);
});

Template.directUrlFile.onRendered(function() {
    let template = this;
    this.autorun(() => {
        let data = Template.currentData();
        if (data.fileObj.isUploaded()) {
            let fileStores = ['vatfree'];
            if (data.fileObj.isImage()) {
                fileStores = ['vatfree', 'vatfree-thumbs'];
            }
            Meteor.call('getDirectUrl', data.fileObj._id, fileStores, function(err, res) {
                if (err) {
                    template.showBackup.set(true);
                } else {
                    template.url.set(res['vatfree']);
                    template.thumbUrl.set(res['vatfree-thumbs']);
                }
            });
        } else {
            template.showBackup.set(true);
        }
    });
});

Template.directUrlFile.helpers({
    showBackup: function() {
        return Template.instance().showBackup.get();
    },
    directUrl: function() {
        return Template.instance().url.get();
    },
    directThumbnailUrl: function() {
        return Template.instance().thumbUrl.get();
    }
});

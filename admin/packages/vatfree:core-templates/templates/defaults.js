Vatfree.defaults = {};
if (Meteor.isClient) {
    Meteor.startup(() => {
        Tracker.autorun(() => {
            if (Meteor.user()) {
                Meteor.call('get-defaults', (err, defaults) => {
                    if (defaults) {
                        Vatfree.defaults = defaults;
                    }
                });
            }
        });
    });
}

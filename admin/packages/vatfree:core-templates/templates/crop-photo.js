Template.crop_photo.onCreated(function() {
    this.isSaving = new ReactiveVar();
});

Template.crop_photo.onRendered(function() {
    let template = this;

    this.autorun(() => {
        let isSaving = this.isSaving.get();
        if (isSaving) {
            $('#darkroom-image').cropper('destroy');
        } else {
            Tracker.afterFlush(() => {
                $('#darkroom-image').cropper({
                    autoCrop: false,
                    checkCrossOrigin: false,
                    ready: function (e) {
                    }
                });
            });
        }
    });
});

Template.crop_photo.onDestroyed(function() {
    $('#darkroom-image').cropper('destroy');
});

Template.crop_photo.helpers({
    isSaving() {
        return Template.instance().isSaving.get() || !this.photo.isUploaded();
    }
});

Template.crop_photo.events({
    'click .rotateLeft'(e) {
        $('#darkroom-image').cropper('rotate', -90);
    },
    'click .rotateRight'(e) {
        $('#darkroom-image').cropper('rotate', 90);
    },
    'click .rotateSmallLeft'(e) {
        $('#darkroom-image').cropper('rotate', -1);
    },
    'click .rotateSmallRight'(e) {
        $('#darkroom-image').cropper('rotate', 1);
    },
    'click .reset'(e) {
        $('#darkroom-image').cropper('reset');
    },
    'click .save-image'(e, template) {
        e.preventDefault();
        $('.cropper-container').css('display','none');
        template.$('button.save-image').attr('disabled', true);

        template.isSaving.set(true);
        let result = $('#darkroom-image').cropper("getCroppedCanvas");
        Tracker.afterFlush(() => {
            $('#darkroom-image').cropper('destroy');
            let base64uri = result.toDataURL('image/jpeg', 0.6);
            let originalFile = template.data.photo;
            if (originalFile) {
                let newFile = {};
                newFile.name = originalFile.name();
                newFile.itemId = template.data.photo.itemId;
                newFile.target = originalFile.target;
                if (originalFile.subType) newFile.subType = originalFile.subType;
                newFile.createdAt = originalFile.createdAt;
                newFile.updatedAt = new Date();
                newFile.uploadedBy = Meteor.userId();
                newFile.originalFile = originalFile._id;

                // contexts
                _.each(['userId', 'travellerId', 'receiptId'], (context) => {
                    if (originalFile[context]) newFile[context] = originalFile[context];
                });

                newFile.fileContent = base64uri;
                Meteor.call('save-file', newFile, (err, fileId) => {
                    if (err) {
                        console.log(err);
                        toastr.error(err.reason || err.message);
                    } else {
                        let newTarget = originalFile.target.replace('_internal', '') + "_internal";
                        if (newTarget === "travellers_internal") {
                            // masked traveller docs will be trashed
                            newTarget = "travellers_trash";
                        }
                        Files.update({
                            _id: originalFile._id
                        }, {
                            $set: {
                                target: newTarget,
                                updatedAt: new Date,
                                supercededBy: fileId
                            },
                            $unset: {
                                subType: 1
                            }
                        });

                        if (_.isFunction(template.data.callback)) {
                            template.data.callback(fileId);
                        }
                        template.isSaving.set();
                    }
                });
            }
        });
    }
});

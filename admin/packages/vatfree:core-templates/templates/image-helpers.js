Vatfree.imageHelpers = {};
Vatfree.imageHelpers.resetOrientation = function(srcBase64, callback) {
    try {
        if (srcBase64.indexOf(',') > 0) {
            srcBase64 = srcBase64.substr(srcBase64.indexOf(',') + 1);
        }

        let srcOrientation = Vatfree.imageHelpers.getOrientation(srcBase64);
        let img = new Image();

        img.onload = function() {
            let width = img.width,
                height = img.height,
                canvas = document.createElement('canvas'),
                ctx = canvas.getContext("2d");

            // set proper canvas dimensions before transform & export
            if ([5,6,7,8].indexOf(srcOrientation) > -1) {
                canvas.width = height;
                canvas.height = width;
            } else {
                canvas.width = width;
                canvas.height = height;
            }

            // transform context before drawing image
            switch (srcOrientation) {
                case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
                case 3: ctx.transform(-1, 0, 0, -1, width, height ); break;
                case 4: ctx.transform(1, 0, 0, -1, 0, height ); break;
                case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
                case 6: ctx.transform(0, 1, -1, 0, height , 0); break;
                case 7: ctx.transform(0, -1, -1, 0, height , width); break;
                case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
                default: ctx.transform(1, 0, 0, 1, 0, 0);
            }

            // draw image
            ctx.drawImage(img, 0, 0);

            // export base64
            callback(null, canvas.toDataURL('image/jpeg', 0.4));
        };

        img.src = 'data:image/jpg;base64,' + srcBase64;
    } catch (e) {
        callback(e);
    }
};

Vatfree.imageHelpers.getOrientation = function(srcBase64) {
    if (srcBase64.indexOf(',') > 0) {
        srcBase64 = srcBase64.substr(srcBase64.indexOf(',') + 1);
    }

    let view = new DataView(_base64ToArrayBuffer(srcBase64));
    if (view.getUint16(0, false) != 0xFFD8) return -2;
    let length = view.byteLength, offset = 2;
    while (offset < length) {
        let marker = view.getUint16(offset, false);
        offset += 2;
        if (marker == 0xFFE1) {
            if (view.getUint32(offset += 2, false) != 0x45786966) return -1;
            let little = view.getUint16(offset += 6, false) == 0x4949;
            offset += view.getUint32(offset + 4, little);
            let tags = view.getUint16(offset, little);
            offset += 2;

            for (let i = 0; i < tags; i++) {
                if (view.getUint16(offset + (i * 12), little) == 0x0112) {
                    return view.getUint16(offset + (i * 12) + 8, little);
                }
            }
        }
        else if ((marker & 0xFF00) != 0xFF00) break;
        else offset += view.getUint16(offset, false);
    }
};

var _base64ToArrayBuffer = function(base64) {
    let binary_string =  window.atob(base64);
    let len = binary_string.length;
    let bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
};

const resizeDataUriImageJS = function (dataUri, maxDimension, callback) {
    let img = new Image;
    img.onload = function () {
        // create an off-screen canvas
        let canvas = document.createElement('canvas'),
            ctx = canvas.getContext('2d');

        let width = this.width;
        let height = this.height;

        if (width < maxDimension && height < maxDimension) {
            callback(null, dataUri);
            return false;
        }

        if (this.height > this.width && this.height > maxDimension) {
            height = maxDimension;
            width = Math.round((this.width / this.height) * maxDimension);
        } else if (this.width > this.height && this.width > maxDimension) {
            width = maxDimension;
            height = Math.round((this.height / this.width) * maxDimension);
        }

        // set its dimension to target size
        canvas.width = width;
        canvas.height = height;

        // draw source image into the off-screen canvas:
        ctx.drawImage(this, 0, 0, width, height);

        // encode image to data-uri with base64 version of compressed image
        callback(null, canvas.toDataURL('image/jpeg', 0.4));
    };
    img.src = dataUri;
};

const resizeDataUriImageNative = function(dataUri, maxDimension, callback) {
    const options = {
        uri: dataUri,
        folderName: "Vatfree",
        quality: 70,
        width: maxDimension,
        height: maxDimension,
        base64: true,
        fit: false
    };

    window.ImageResizer.resize(options,
        (imageUri) => {
            callback(null, imageUri);
        },
        (err) => {
            callback(err);
        });
};

Vatfree.imageHelpers.resizeDataUriImage = function(dataUri, maxDimension, callback) {
    if (_.has(window, 'ImageResizer')) {
        console.log('using native image resizer');
        resizeDataUriImageNative(dataUri, maxDimension, callback);
    } else {
        resizeDataUriImageJS(dataUri, maxDimension, callback);
    }
};

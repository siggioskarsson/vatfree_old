export const editFormOnCreated = function () {
    return function () {
        this.tabLoading = new ReactiveDict();
    }
};

export const editFormHelpers = function() {
    return {
        tabLoading(tab) {
            return Template.instance().tabLoading.get(tab) === 'loading';
        },
        tabLoaded(tab) {
            return Template.instance().tabLoading.get(tab);
        }
    }
};

export const editFormEvents = function() {
    return {
        'click .nav-tabs > li > a'(e, template) {
            let tabId = $(e.currentTarget)[0].hash.replace('#', '');
            if (!template.tabLoading.get(tabId)) {
                template.tabLoading.set(tabId, 'loading');
            }
        }
    }
};

export const setSubListSorting = function (e, template, sortVarable) {
    let data = $(e.currentTarget).data();
    if (data && data.id) {
        let currentSort = template[sortVarable].get();
        let sortOrder = -1;
        if (currentSort && currentSort[data.id] === -1) {
            sortOrder = 1;
        }
        template[sortVarable].set({
            [data.id]: sortOrder
        });
    }
};

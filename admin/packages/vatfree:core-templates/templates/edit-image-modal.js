Template.editImageModal.onCreated(function() {
    this.parentTemplateVariable = this.data.parentTemplateVariable || "editImageFile";
    this.imageMode = new ReactiveVar('crop');

    let template = this;
    this.parentTemplate = Template.instance().parentTemplate(1);
    this.hideModal = () => {
        $('#modal-edit-image').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-edit-image').on('hidden.bs.modal', function () {
            if (template.parentTemplateVariable && template.parentTemplate[template.parentTemplateVariable]) {
                template.parentTemplate[template.parentTemplateVariable].set(false);
            }
        });
        $('#modal-edit-image').on('shown.bs.modal', function () {
        });
        $('#modal-edit-image').modal('show');
    });
});

Template.editImageModal.onDestroyed(function() {
    this.hideModal();

    if (this.parentTemplateVariable && this.parentTemplate[this.parentTemplateVariable]) {
        this.parentTemplate[this.parentTemplateVariable].set(false);
    }

    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.editImageModal.helpers({
    isImageMode(mode) {
        let template = Template.instance();
        return template.imageMode.get() === mode;
    },
    getImage() {
        return Template.instance().data.file;
    },
    maskPhotoCallback() {
        let template = Template.instance();
        return function(newFileId) {
            template.hideModal();
        }
    },
    getCropPhotoCallback() {
        let template = Template.instance();
        return function(newFileId) {
            template.hideModal();
        }
    }
});

Template.editImageModal.events({
    'click .close-modal'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'click .image-mode-crop'(e, template) {
        e.preventDefault();
        template.imageMode.set('crop');
    },
    'click .image-mode-mask'(e, template) {
        e.preventDefault();
        template.imageMode.set('mask');
    }
});

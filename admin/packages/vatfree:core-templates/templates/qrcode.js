import QRCode from 'qrcode';

export const generateQRCode = function (uploadContext, qrcodeCanvas) {
    QRCode.toCanvas(qrcodeCanvas, 'vatfree://image-upload/?context=' + JSON.stringify(uploadContext), {
        margin: 2,
        errorCorrectionLevel: 'Q'
    }, (err) => {
        if (err) {
            toastr.error(err);
        }
    });
};

export const mapStyles = [{
    "featureType": "poi",
    "stylers": [{"visibility": "off"}]
}];

Meteor.startup(() => {
    Meteor.call('get-maps-key', (err, key) => {
        if (err) {
            console.log(err);
        } else {
            if (!_.isUndefined(GoogleMaps)) {
                GoogleMaps.load({
                    key: key,
                    libraries: 'places'
                });
            }
        }
    });
});

Template.forms_map.onCreated(function () {
    this.map = {};
    this.marker = false;
});

Template.forms_map.onRendered(function() {
    var self = this;

    Tracker.afterFlush(() => {
        if (_.isUndefined(GoogleMaps)) return;

        GoogleMaps.ready('_map', function (map) {
            self.map = map;
            // Fix for google maps when initialized hidden
            google.maps.event.trigger(map, 'resize');
            let addressElement = self.$('[id="_map_addressbar"]');
            if (!addressElement.length) {
                // we're inside a velocity animation or something else went wrong
                return false;
            }
            let autocomplete = new google.maps.places.Autocomplete(addressElement[0]);

            if (self.data.location && self.data.location.coordinates) {
                self.marker = new google.maps.Marker({
                    map: map.instance,
                    position: {
                        lat: self.data.location.coordinates[1],
                        lng: self.data.location.coordinates[0]
                    }
                });
            }

            map.instance.addListener('click', function(event) {
                if (self.marker) self.marker.setMap(null);
                self.marker = new google.maps.Marker({
                    position: event.latLng,
                    map: map.instance
                });

                self.$('.map-container').closest('form').find('input[name="' + self.data.name + '"]').val(JSON.stringify({
                    type: "Point",
                    coordinates: [event.latLng.lng(), event.latLng.lat()]
                }));
            });

            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    if (!Meteor.isCordova) {
                        import swal from 'sweetalert';
                        swal({
                            title: TAPi18n.__("Maps lookup failed"),
                            text: TAPi18n.__("Please change the address and try again"),
                            type: "warning",
                            confirmButtonText: TAPi18n.__('OK'),
                            closeOnConfirm: true,
                            html: false
                        });
                    } else {
                        alert(TAPi18n.__("Maps lookup failed") + " - " + TAPi18n.__("Please change the address and try again"));
                    }
                    return;
                }
                if (!self.marker) {
                    self.marker = new google.maps.Marker({
                        map: map.instance,
                        position: place.geometry.location
                    });
                }

                self.marker.setVisible(false);

                map.instance.setCenter(place.geometry.location);
                map.instance.setZoom(15);

                self.marker.setPosition(place.geometry.location);
                self.marker.setVisible(true);

                self.$('.map-container').closest('form').find('input[name="' + self.data.name + '"]').val(JSON.stringify({
                    type: "Point",
                    coordinates: [
                        place.geometry.location.lng(), place.geometry.location.lat()
                    ]
                }));

                if (_.isFunction(self.data.onChangeCallback)) {
                    self.data.onChangeCallback(place);
                }
            });
        });
    });
});

Template.forms_map.helpers({
    getLocationValue: function () {
        if (_.isObject(this.location)) {
            return JSON.stringify(this.location);
        } else {
            return this.location;
        }
    },
    getMapOptions: function () {
        // Make sure the maps API has loaded
        if (_.isUndefined(GoogleMaps)) return;
        if (GoogleMaps.loaded()) {
            let position = {
                lat: 52.379641,
                lng: 4.900417
            }, zoom = 6;

            if (this.location && this.location.coordinates) {
                position = {
                    lat: this.location.coordinates[1],
                    lng: this.location.coordinates[0]
                };
                zoom = 15;
            }

            // Map initialization options
            var mapOptions = {
                center: position,
                zoom: zoom,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                styles: mapStyles
            };

            mapOptions.clickableIcons = false;
            if (this.readOnly) {
                mapOptions.disableDoubleClickZoom = false;
                mapOptions.draggable = false;
                mapOptions.fullscreenControl = false;
                mapOptions.keyboardShortcuts = false;
                mapOptions.panControl = false;
                mapOptions.rotateControl = false;
                mapOptions.scaleControl = false;
                mapOptions.scrollwheel = false;
                mapOptions.signInControl = false;
                mapOptions.streetViewControl = false;
                mapOptions.zoomControl = false;
            }

            return mapOptions;
        }
    }
});

Template.forms_map.events({
    'keydown #_map_addressbar'(e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            e.stopPropagation();
        }
    },
    'keyup #_map_addressbar'(e) {
        Meteor.setTimeout(() => {
            $('.pac-container .pac-item').each(function() {
                $(this).addClass('no-fastclick');
            });
        }, 200);
    }
});

Template.forms_map.onDestroyed(function() {
    $('.pac-container').remove();
});

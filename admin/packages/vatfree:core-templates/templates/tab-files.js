import { generateQRCode } from 'meteor/vatfree:core-templates/templates/qrcode';

Template.tab_files.onRendered(function() {
    Tracker.afterFlush(() => {
        generateQRCode(this.data.fileContext,  this.$('.qrcode-upload > canvas')[0]);
    });
});

Template.tab_files.helpers({
    getFiles() {
        let data = Template.currentData();
        return Files.find(_.clone(data.fileContext), {
            sort: {
                createdAt: 1
            }
        });
    }
});

Template.tab_files.events({
    'click .tab-files .dropzone .add-file': function (e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.$('input[name="dropzone-file"]').trigger('click');
    },
    'change .tab-files input[name="dropzone-file"]': function(e, template) {
        e.preventDefault();
        e.stopPropagation();
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, template.data.fileContext, template.data.disallowMultiple);
        });
    },
    'dropped .tab-files .dropzone-layout.file': function (e, template) {
        e.preventDefault();
        e.stopPropagation();
        $('.dropzone').removeClass('visible');
        FS.Utility.eachFile(e, function (file) {
            Vatfree.templateHelpers.handleFileUpload(file, template.data.fileContext, template.data.disallowMultiple);
        });
    }
});

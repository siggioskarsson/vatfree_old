let initImagePlugins = function () {
    // skip all the jQuery stuff
    _.each(document.querySelectorAll('.zoom-photo'), (el) => {
        el.dispatchEvent(new CustomEvent('wheelzoom.destroy'));
        wheelzoom(el);
    });
};

Template.zoom_photo.onCreated(function() {
    this.directUrl = new ReactiveVar();
    this.showBackup = new ReactiveVar();
});

Template.zoom_photo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let photo = Files.findOne({_id: data.photo._id});
        if (photo && photo.isUploaded() && photo.hasStored('vatfree') && photo.url()) {
            // make any changes to the photo reactive in this context
            Tracker.afterFlush(() => {
                initImagePlugins();
            });
        }
    });

    $(window).resize(() => {
        initImagePlugins();
    });
});

Template.zoom_photo.helpers({
    isStored() {
        return this.hasStored('vatfree');
    }
});
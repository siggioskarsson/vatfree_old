Template.importsList.events(Vatfree.templateHelpers.events());
Template.importsList.onCreated(function() {
    let template = this;
    this.handleFileUpload = function(file) {
        let newFile = new FS.File(file);
        newFile.name(file.name.replace(/[^a-z0-9-_.]/ig, '_'));
        newFile.target = 'imports';
        newFile.subType = template.data.subType;
        newFile.createdAt = new Date();
        newFile.uploadedBy = Meteor.userId();
        newFile.importInfo = {
            running: false,
            progress: 0,
            progressMessage: ''
        };
        Files.insert(newFile, function (err) {
            if (err) {
                toastr.error(err);
            }
        });
    }
});

Template.importsList.onRendered(function() {
    let template = this;
    this.autorun(() => {
        this.subscribe('imports', template.data.subType);
    });
});

Template.importsList.helpers({
    getImports() {
        let template = Template.instance();
        return Files.find({
            target: 'imports',
            subType: template.data.subType
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getStatus() {
        if (this.importInfo.progress === 100) {
            return "Done";
        } else if (this.importInfo.running) {
            return "Running";
        } else {
            return "-";
        }
    },
    getError() {
        if (_.isObject(this.importInfo.error)) {
            return '' + this.importInfo.error.code + ' - ' + (this.importInfo.error.reason || this.importInfo.error.message);
        } else {
            return this.importInfo.error;
        }
    }
});

Template.importsList.events({
    'click .import-file'(e, template) {
        e.preventDefault();
        Meteor.call('import', this._id, template.data.subType, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.info("Import started");
            }
        });
    }
});

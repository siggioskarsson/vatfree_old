Template.modal.onCreated(function() {
    let template = this;
    this.parentTemplateVariable = this.data.parentTemplateVariable;
    this.parentTemplate = Template.instance().parentTemplate(2);

    this.hideModal = () => {
        if (!this.view.isDestroyed) {
            const modal = template.$('.modal');
            if (modal && modal.length) {
                modal.modal('hide');
            }
        }
    };

    Tracker.afterFlush(() => {
        template.$('.modal').on('hidden.bs.modal', function () {
            if (template.parentTemplateVariable && template.parentTemplate && template.parentTemplate[template.parentTemplateVariable]) {
                template.parentTemplate[template.parentTemplateVariable].set(false);
            }
            if (_.isFunction(template.data.onHide)) {
                template.data.onHide.call(template);
            }
        });
        template.$('.modal').on('shown.bs.modal', function () {
            if (_.isFunction(template.data.onShow)) {
                template.data.onShow.call(template);
            }
        });
        console.log('showing modal');
        template.$('.modal').modal('show');
    });
});

Template.modal.onDestroyed(function() {
    this.hideModal();

    if (this.parentTemplateVariable && this.parentTemplate[this.parentTemplateVariable]) {
        this.parentTemplate[this.parentTemplateVariable].set(false);
    }

    $('body').removeClass('modal-open');
    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.modal.helpers({
    fullscreen() {
        return this.fullscreen ? 'fullscreen' : false;
    }
});

Template.modal.events({
    'click .close-modal'(e, template) {
        e.preventDefault();
        template.hideModal();
    }
});

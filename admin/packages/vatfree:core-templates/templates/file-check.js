Template.file_check.onCreated(function() {
    this.updateTarget = new ReactiveVar('travellers_internal');
    this.bulkTarget = new ReactiveVar('travellers_trash');
});

Template.file_check.onRendered(function() {
    this.autorun(() => {
        this.subscribe('files-to-check', this.updateTarget.get(), 400);
    });
});

Template.file_check.helpers({
    updateTarget() {
        return Template.instance().updateTarget.get();
    },
    bulkTarget() {
        return Template.instance().bulkTarget.get();
    },
    getFiles() {
        return Files.find({
            check: true
        },{
            sort: {
                createdAt: 1
            }
        });
    }
});

Template.file_check.events({
    'click .files-to-check .files .file .update-file'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        Files.update({
            _id: this._id
        },{
            $unset: {
                check: 1
            }
        });
    },
    'click .files-to-check .set-bulk'(e, template) {
        e.preventDefault();
        // first we get all the Id's as the subscription will re-trigger when we start editing files
        if (confirm("Update all files with Bulk Target?")) {
            let fileIds = _.map(Files.find({check: true}).fetch(), (file) => {return file._id});
            let bulkTarget = template.bulkTarget.get();
            _.each(fileIds, (fileId) => {
                Files.update({
                    _id: fileId
                },{
                    $set: {
                        target: bulkTarget
                    },
                    $unset: {
                        check: 1
                    }
                });
            });
        }
    },
    'click .files-to-check .set-delete'(e, template) {
        e.preventDefault();
        // first we get all the Id's as the subscription will re-trigger when we start editing files
        if (confirm("Delete all visible files?")) {
            let fileIds = _.map(Files.find({check: true}).fetch(), (file) => {return file._id});
            _.each(fileIds, (fileId) => {
                Files.update({
                    _id: fileId
                },{
                    $set: {
                        deleted: true
                    }
                });
            });
        }
    },
    'change input[name="updateTarget"]'(e, template) {
        e.preventDefault();
        template.updateTarget.set($(e.currentTarget).val());
    },
    'change input[name="bulkTarget"]'(e, template) {
        e.preventDefault();
        template.bulkTarget.set($(e.currentTarget).val());
    }
});

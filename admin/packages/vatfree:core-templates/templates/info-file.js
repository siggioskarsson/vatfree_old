Template.infoFile.onRendered(function() {
    Tracker.afterFlush(() => {
        Meteor.setTimeout(() => {
            // skip all the jQuery stuff
            document.querySelector('.file-zoom').dispatchEvent(new CustomEvent('wheelzoom.destroy'));
            wheelzoom(document.querySelector('.file-zoom'));
        }, 600);
    });
});

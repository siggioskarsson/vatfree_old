Template.billing_form.onRendered(function() {
    this.autorun(() => {
        this.subscribe('email-texts-list');
    });

    Tracker.afterFlush(() => {
        Vatfree.templateHelpers.initDatepicker.call(this, false, false, true);
    });
});

Template.billing_form.helpers({
    isDisabled() {
        return !!this.deleted;
    },
    isShop() {
        // check shop specific fields. One of these should be set
        return !!this.geo || !!this.inShoppingGuide || !!this.partnerShipStatus;
    },
    billingEntityDoc() {
        return Vatfree.billing.getBillingEntityForCompany(this.companyId);
    },
    isEmailTemplateSelected() {
        return this._id === Template.instance().data.billingEmailTemplateId;
    },
    isEmailTextSelected() {
        return this._id === Template.instance().data.billingEmailTextId;
    },
    isEmailReminderTextSelected() {
        return this._id === Template.instance().data.billingReminderEmailTextId;
    },
    isEmailReminder2TextSelected() {
        return this._id === Template.instance().data.billingReminder2EmailTextId;
    },
    getBillingAddressPlaceHolder() {
        let country = Countries.findOne({_id: this.countryId}) || {};
        return this.addressFirst + "\n" + (this.postCode || this.postalCode || "") + " " + (this.city || "") + "\n" + (country.name || "");
    },
    getDefaultBillingTemplate() {
        let defaultBillingTemplateId = Vatfree.defaults.billingEmailTemplateId;
        let emailTemplate = EmailTemplates.findOne({_id: defaultBillingTemplateId}) || "";
        return emailTemplate.name || "";
    },
    getDefaultBillingEmailText() {
        let defaultBillingEmailTextId = Vatfree.defaults.billingEmailTextId;
        let emailText = EmailTexts.findOne({_id: defaultBillingEmailTextId}) || "";
        return emailText.name || "";
    },
    getDefaultBillingReminderEmailText() {
        let defaultBillingReminderEmailTextId = Vatfree.defaults.billingReminderEmailTextId;
        let emailText = EmailTexts.findOne({_id: defaultBillingReminderEmailTextId}) || "";
        return emailText.name || "";
    },
    getDefaultBillingReminder2EmailText() {
        let defaultBillingReminder2EmailTextId = Vatfree.defaults.billingReminder2EmailTextId;
        let emailText = EmailTexts.findOne({_id: defaultBillingReminder2EmailTextId}) || "";
        return emailText.name || "";
    },
    getContacts() {
        return Contacts.find({
            _id: {
                $in: this.billingContacts || []
            }
        });
    },
    getNonBillingContacts() {
        return Contacts.find({
            _id: {
                $nin: this.billingContacts || []
            }
        });
    },
    getContactActions() {
        let template = Template.instance();
        return [
            {
                icon: 'brokenlink',
                action: function(contactId) {
                    // remove contact from billingContacts
                    let entity = Shops.findOne({_id: template.data._id});
                    if (entity) {
                        Shops.update({
                            _id: template.data._id
                        },{
                            $pull: {
                                billingContacts: contactId
                            }
                        });
                    } else {
                        entity = Companies.findOne({_id: template.data._id});
                        if (entity) {
                            Companies.update({
                                _id: template.data._id
                            },{
                                $pull: {
                                    billingContacts: contactId
                                }
                            });
                        }
                    }

                }
            }
        ]
    }
});

Template.billing_form.events({
    'change select[name="addBillingContactId"]'(e, template) {
        e.preventDefault();
        let contactId = $(e.currentTarget).val();
        let entity = Shops.findOne({_id: this._id});
        if (entity) {
            Shops.update({
                _id: this._id
            },{
                $addToSet: {
                    billingContacts: contactId
                }
            });
        } else {
            entity = Companies.findOne({_id: this._id});
            if (entity) {
                Companies.update({
                    _id: this._id
                },{
                    $addToSet: {
                        billingContacts: contactId
                    }
                });
            }
        }
    }
});

Template.translations.helpers({
    getTranslateLanguages(type) {
        if (type === 'traveller') {
            return _.filter(Vatfree.languages.getTravellerLanguages(), (lang) => { return lang.code !== 'en'; });
        } else {
            return _.filter(Vatfree.languages.getShopLanguages(), (lang) => { return lang.code !== 'en'; });
        }
    },
    getTranslatedValue(key, language) {
        const template = Template.instance();
        let data = template.data.values;

        return data.i18n && data.i18n[language] ? data.i18n[language][key] : '';
    }
});

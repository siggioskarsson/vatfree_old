import KeyCode from 'keycode-js';
import moment from 'moment';

Vatfree.templateHelpers = {};
var processFormItem = function (item, obj) {
    if (!item.name) {
        return false;
    }
    if (item.name.indexOf('.') > 0) {
        var el = item.name.split('.');
        if (el.length === 2) {
            if (!obj[el[0]]) {
                obj[el[0]] = {};
            }
            obj[el[0]][el[1]] = item.value;
        } else if (el.length === 3) {
            if (!obj[el[0]]) {
                obj[el[0]] = {};
            }
            if (!obj[el[0]][el[1]]) {
                obj[el[0]][el[1]] = {};
            }
            obj[el[0]][el[1]][el[2]] = item.value;
        }
    } else {
        obj[item.name] = item.value;
    }
};
Vatfree.templateHelpers.getFormData = function (template, formElement) {
    formElement = formElement || 'form';
    let $form = _.isString(formElement) ? $(template.find(formElement)) : formElement;

    let formData = $form.serializeArray().reduce(function (obj, item) {
        if (item.value || !item.name.match(/^i18n\./)) {
            if ($(template.find('select[name="' + item.name + '"]')).attr('multiple') === 'multiple') {
                // multiple select item
                if (!obj[item.name]) {
                    obj[item.name] = [];
                }
                obj[item.name].push(item.value);
            } else {
                processFormItem(item, obj);
            }
        }
        return obj;
    }, {});

    // check for unchecked checkboxes
    $form.find('input:checkbox').each(function () {
        let checkbox = {
            name: this.name,
            value: this.checked ? this.value : false
        };
        if (!formData[checkbox.name]) {
            processFormItem(checkbox, formData);
        }
    });

    // add empty select boxes, sometimes not added properly
    $form.find('select').each(function () {
        if (this.name.indexOf('.') > 0) {
            let el = this.name.split('.');
            if (!formData[el[0]][el[1]] && !this.disabled) {
                if ($(this).attr('multiple') === 'multiple') {
                    formData[el[0]][el[1]] = $(this).val() || [];
                } else {
                    formData[el[0]][el[1]] = this.value || "";
                }
            }
        } else {
            if (!formData[this.name] && !this.disabled) {
                if ($(this).attr('multiple') === 'multiple') {
                    formData[this.name] = $(this).val() || [];
                } else {
                    formData[this.name] = this.value || "";
                }
            }
        }
    });

    return formData;
};
Vatfree.templateHelpers.downloadFile = function (fileNameToSaveAs, textFileAsBlob) {
    let downloadLink = document.createElement('a');
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = 'Download File';
    if (window.webkitURL != null) {
        // Chrome allows the link to be clicked
        // without actually adding it to the DOM.
        downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
    }
    else {
        // Firefox requires the link to be added to the DOM
        // before it can be clicked.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = function (event) {
            document.body.removeChild(event.target);
        };
        downloadLink.style.display = 'none';
        document.body.appendChild(downloadLink);
    }

    downloadLink.click();
};


var isScrolledIntoView = function($elem) {
    let docViewTop = $(window).scrollTop();
    let docViewBottom = docViewTop + $(window).height();

    let elemTop = $elem.offset().top;
    let elemBottom = elemTop + $elem.height();

    return (elemBottom <= docViewBottom);
};

var showMoreResults = function (template) {
    if (template.moreResults && template.moreResults.get() && template.subscriptionsReady()) {
        let $scrollElement = template.$('.infinite-scrolling');
        if ($scrollElement && $scrollElement.length > 0) {
            let target = $(".show-more");
            if (isScrolledIntoView($scrollElement)) {
                if (target.length) {
                    target.data("visible", true);
                    target[0].innerHTML = TAPi18n.__('Getting more results_') + Blaze.toHTML(Template.loading);
                }
                template.limit.set(template.limit.get() + template.itemsIncrement);
            } else {
                if (target.length && target.data("visible")) {
                    target.data("visible", false);
                }
            }
        }
    }
};

Vatfree.templateHelpers.initDatepicker = function (startDate, endDate, showClear) {
    if (!this.view || this.view.isDestroyed) return;

    let dateOptions = {
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        clearBtn: showClear || false,
        format: TAPi18n.__('_date_format').toLowerCase()
    };

    if (startDate) {
        dateOptions.startDate = startDate;
    } else {
        dateOptions.startDate = -Infinity;
    }

    if (endDate) {
        dateOptions.endDate = endDate;
    } else {
        dateOptions.endDate = Infinity;
    }

    const $dateEl = this.$('.input-group.date');
    if ($dateEl && $dateEl.length > 0) {
        $dateEl.datepicker(dateOptions);
        $dateEl.on('change', (e) => {
            let $element = $(e.currentTarget).find('input');
            let date = ($element.val() || "").toString();
            if (date && date.length === 6 && date.indexOf('-') < 0) {
                // transform DDMMYY to official date
                let mDate = moment(date, 'DDMMYY');
                if (mDate.isValid()) {
                    $element.val(mDate.format(TAPi18n.__('_date_format')));
                }
            }
        });
        $dateEl.on('changeDate', (e) => {
            let $element = $(e.currentTarget).find('input');
            $element.trigger('change');
        });
    }
};

Vatfree.templateHelpers.onCreated = function (collection, listUrl, detailUrl) {
    return function () {
        this.collection = collection;
        this.subscription = collection ? (collection.name || collection._collection.name) : false;
        this.subscriptionItem = collection ? ((collection.name || collection._collection.name) + '_item') : false;
        this.listUrl = listUrl;
        this.detailUrl = detailUrl;
        this.itemsIncrement = 25;

        // if "search" is set we are being redirected from another page
        let searchTerm = FlowRouter.getQueryParam('search');

        this.searchTerm = new ReactiveVar(searchTerm ? searchTerm : (Session.get(this.view.name + '.searchTerm') || ''));
        this.limit = new ReactiveVar(this.itemsIncrement);
        this.subLimit = 0;
        this.offset = new ReactiveVar(0);
        this.sort = new ReactiveVar(Session.get(this.view.name + '.sort') || {createdAt: 1});
        this.activeItem = new ReactiveVar(Session.get(this.view.name + '.activeItem'));
        this.initialLoadDone = new ReactiveVar(false);
        this.moreResults = new ReactiveVar(true);
        this.showListFilters = new ReactiveVar(Session.get(this.view.name + '.showListFilters') || false);
        this.listFilters = new ReactiveVar(searchTerm ? {} : (Session.get(this.view.name + '.listFilters') || {}));
        this.showManagementFunctions = new ReactiveVar(false);

        this.statusFilter = new ReactiveArray();
        this.statusAttribute = 'status';
        if (!searchTerm) {
            _.each(Session.get(this.view.name + '.statusFilter') || [], (status) => {
                this.statusFilter.push(status);
            });
        }

        this.addingItem = new ReactiveVar();
        this.showImports = new ReactiveVar();
        this.runningExport = new ReactiveVar();
    }
};

let initSelect2Search = function (filterName, placeholder, searchFunction) {
    this.$('select[name="' + filterName + '"]').select2({
        minimumInputLength: 1,
        multiple: false,
        allowClear: true,
        placeholder: {
            id: "",
            placeholder: placeholder
        },
        ajax: {
            transport: function (params, success, failure) {
                let limit = 20;
                let offset = 0;
                Meteor.call(searchFunction, params.data.q || "", limit, offset, (err, res) => {
                    if (err) {
                        failure(err);
                    } else {
                        success({results: res});
                    }
                });
            },
            delay: 500
        }
    });
};
Vatfree.templateHelpers.onRendered = function () {
    return function () {
        let template = this;

        this.autorun(() => {
            let selector = {};
            if (this.statusFilter) {
                Vatfree.search.addStatusFilter.call(this, selector);
            }
            let listFilters = this.listFilters.get();
            _.each(listFilters, (listFilterValue, listFilterId) => {
                selector[listFilterId] = listFilterValue;
            });
            if (_.isFunction(this.selectorFunction)) {
                this.selectorFunction(selector);
            }

            // reset the limit when the selector changes
            this.limit.set(this.itemsIncrement);
        });

        this.autorun(() => {
            if (this.subscription) {
                this.subLimit = this.limit.get();

                let selector = {};
                if (this.statusFilter) {
                    Vatfree.search.addStatusFilter.call(this, selector);
                }
                let listFilters = this.listFilters.get();
                _.each(listFilters, (listFilterValue, listFilterId) => {
                    selector[listFilterId] = listFilterValue;
                });
                if (_.isFunction(this.selectorFunction)) {
                    this.selectorFunction(selector);
                }

                // reset the more results variable when re-subscribing
                this.moreResults.set(true);
                this.subscribe(this.subscription, this.searchTerm.get(), selector, this.sort.get(), this.subLimit, this.offset.get());
            }
        });

        this.autorun(() => {
            if (this.subscriptionItem && this.activeItem.get()) {
                this.subscribe(this.subscriptionItem, this.activeItem.get());
            }
        });

        this.autorun(() => {
            let searchTerm = this.searchTerm.get();
            // reset search stuff on changing searchTerm
            this.limit.set(this.itemsIncrement);
            this.offset.set(0);
        });

        this.autorun((comp) => {
            if (this.subscriptionsReady()) {
                this.initialLoadDone.set(true);
                comp.stop(); // stop current autorun computation
            }
        });

        this.autorun(() => {
            if (this.subscriptionsReady()) {
                let limit = this.limit.get();
                if (this.subLimit === limit) {
                    Tracker.nonreactive(() => {
                        // check whether we have more items than before
                        var numberOfItems = this.collection.find({}, {
                            reactive: false
                        }).count();
                        if (limit > numberOfItems) {
                            this.moreResults.set(false);
                        }
                    });
                }
            }
        });

        this.autorun(() => {
            Session.setPersistent(this.view.name + '.searchTerm', this.searchTerm.get());
            Session.setPersistent(this.view.name + '.activeItem', this.activeItem.get());
            Session.setPersistent(this.view.name + '.statusFilter', this.statusFilter.array());
            Session.setPersistent(this.view.name + '.sort', this.sort.get());
            Session.setPersistent(this.view.name + '.showListFilters', this.showListFilters.get());
            Session.setPersistent(this.view.name + '.listFilters', this.listFilters.get());
        });

        let infinitScrolling = Vatfree.templateHelpers.onRenderedInfiniteScrolling.call(this);
        infinitScrolling.call(this);


        let initFormHelpers = function () {
            if (!this.view || this.view.isDestroyed === true) {
                return;
            }

            if (this.$('select[name="listFilter.countryId"]').length > 0) {
                this.$('select[name="listFilter.countryId"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.customsCountryId"]').length > 0) {
                this.$('select[name="listFilter.customsCountryId"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.profile.countryId"]').length > 0) {
                this.$('select[name="listFilter.profile.countryId"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.travellerCountryId"]').length > 0) {
                this.$('select[name="listFilter.travellerCountryId"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.partnershipStatus"]').length > 0) {
                this.$('select[name="listFilter.partnershipStatus"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.source"]').length > 0) {
                this.$('select[name="listFilter.source"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.receiptRejectionIds"]').length > 0) {
                this.$('select[name="listFilter.receiptRejectionIds"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.affiliatedWith"]').length > 0) {
                this.$('select[name="listFilter.affiliatedWith"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.receiptTypeExtraFields.type"]').length > 0) {
                this.$('select[name="listFilter.receiptTypeExtraFields.type"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.services"]').length > 0) {
                this.$('select[name="listFilter.services"]').select2({
                    triggerChange: true
                });
            }
            if (this.$('select[name="listFilter.shopId"]').length > 0) {
                initSelect2Search.call(this, "listFilter.shopId", "Select a shop ...", "search-shops");
            }
            if (this.$('select[name="listFilter.affiliateId"]').length > 0) {
                initSelect2Search.call(this, "listFilter.affiliateId", "Select an affiliate ...", "search-affiliates");
            }
            if (this.$('select[name="listFilter.companyId"]').length > 0) {
                initSelect2Search.call(this, "listFilter.companyId", "Select a company ...", "search-companies");
            }
            if (this.$('select[name="listFilter.billingCompanyId"]').length > 0) {
                initSelect2Search.call(this, "listFilter.billingCompanyId", "Select a company ...", "search-companies");
            }
            if (this.$('select[name="listFilter.createdBy"]').length > 0) {
                initSelect2Search.call(this, "listFilter.createdBy", "Select a user ...", "search-users");
            }
            if (this.$('select[name="listFilter.checkedBy"]').length > 0) {
                initSelect2Search.call(this, "listFilter.checkedBy", "Select a user ...", "search-users");
            }
            if (this.$('select[name="listFilter.updatedBy"]').length > 0) {
                initSelect2Search.call(this, "listFilter.updatedBy", "Select a user ...", "search-users");
            }
            if (this.$('select[name="listFilter.travellerId"]').length > 0) {
                initSelect2Search.call(this, "listFilter.travellerId", "Select a user ...", "search-travellers");
            }
            Vatfree.templateHelpers.initDatepicker.call(this, null, null, true);
        };
        this.autorun(() => {
            if (this.subscriptionsReady() && this.showListFilters.get()) {
                Tracker.afterFlush(() => {
                    initFormHelpers.call(this);
                });
            }
        });
        template.resizeWindow = function() {
            initFormHelpers.call(template);
        };
        $(window).resize(template.resizeWindow);

        this.keyHandler = (e) => {
            // general key strokes are ignored when modals are open
            let activeElement = $(document.activeElement);
            if (activeElement.length && activeElement[0].nodeName === 'INPUT') {
                return false;
            }

            if ($('.featherlight-content').length > 0) {
                if (e.keyCode === KeyCode.KEY_ESCAPE) {
                    var current = $.featherlight.current();
                    current.close();
                }
            } else if ($('.modal').length === 0) {
                let selectedLine = $('tr.item-row.active').attr('id');
                if (selectedLine) {
                    if (e.keyCode === KeyCode.KEY_UP) {
                        // up
                        e.stopPropagation();
                        let prev = $('#' + selectedLine).prev();
                        if (prev.length > 0) {
                            this.activeItem.set(prev.attr('id').replace('item-', ''));
                        }
                    } else if (e.keyCode === KeyCode.KEY_DOWN) {
                        // down
                        e.stopPropagation();
                        let next = $('#' + selectedLine).next();
                        if (next.length > 0) {
                            this.activeItem.set(next.attr('id').replace('item-', ''));
                        }
                    }
                } else if (e.keyCode === KeyCode.KEY_UP || e.keyCode === KeyCode.KEY_DOWN) {
                    // select the first element in the list
                    let rows = $('tr.item-row');
                    if (rows.length > 0) {
                        let id = rows[0].id || "";
                        this.activeItem.set(id.replace('item-', ''));
                    }
                }

                if (e.keyCode === KeyCode.KEY_E || e.keyCode === KeyCode.KEY_RETURN) {
                    // edit key
                    $('tr.item-row.active').find('.edit-item').trigger('click');
                } else if (e.keyCode === KeyCode.KEY_T) {
                    // task key
                    $('tr.item-row.active').find('.task-item').trigger('click');
                } else if (e.keyCode === KeyCode.KEY_ESCAPE) {
                    $('.btn.cancel').trigger('click');
                } else {
                    //console.log(e.keyCode);
                }
            }
        };
        Tracker.afterFlush(() => {
            $(document).off('keyup', null, this.keyHandler);
            $(document).on('keyup', null, this.keyHandler);
        });
    }
};

Vatfree.templateHelpers.onDestroyed = function () {
    return function () {
        $(document).off('keyup', null, this.keyHandler);

        if (this.moreResults) {
            $(window).unbind('scroll');
        }

        if (this.view.isDestroyed !== true) {
            if (template.resizeWindow) {
                $(window).off("resize", template.resizeWindow);
            }
        }
    };
};

Vatfree.templateHelpers.onRenderedInfiniteScrolling = function () {
    return function () {
        if (this.view.isDestroyed) {
            return false;
        }
        this.autorun((comp) => {
            if (this.subscriptionsReady()) {
                Tracker.afterFlush(() => {
                    if (this.moreResults && this.moreResults.get) {
                        $(window).scroll(() => {
                            showMoreResults(this);
                        });
                    }
                    comp.stop(); // stop current autorun computation
                });
            }
        });
    }
};

Vatfree.templateHelpers.select2Search = function (searchName, fieldName, multiple = false, allowClear = false) {
    this.$('select[name="' + fieldName + '"]').select2({
        minimumInputLength: 1,
        multiple: multiple,
        allowClear: allowClear,
        placeholder: {
            id: "",
            placeholder: "Select ..."
        },
        ajax: {
            transport: function (params, success, failure) {
                let limit = 20;
                let offset = 0;
                Meteor.call(searchName, params.data.q || "", limit, offset, (err, res) => {
                    if (err) {
                        failure(err);
                    } else {
                        success({results: res});
                    }
                });
            },
            delay: 500
        }
    });
};


Vatfree.templateHelpers.helpers = {
    initialLoadDone() {
        return Template.instance().initialLoadDone.get();
    },
    showImports() {
        return Template.instance().showImports.get();
    },
    getSearchTerm() {
        return Template.instance().searchTerm.get();
    },
    showListFilters() {
        return Template.instance().showListFilters.get();
    },
    showManagementFunctions() {
        return Template.instance().showManagementFunctions.get();
    },
    activeListfilters() {
        let listFilter = Template.instance().listFilters.get();
        return _.size(listFilter) > 0;
    },
    getListFilterKeyExists() {
        return {$exists: true};
    },
    getListFilterSelected(itemId, value) {
        let listFilter = Template.instance().listFilters.get();
        if (_.has(listFilter, '$or')) {
            let foundInOr = undefined;
            _.each(listFilter['$or'], (listFilterOr) => {
                if (itemId.indexOf('.') > 0) {
                    let splitItemId = itemId.split('.');
                    if (_.has(listFilterOr, splitItemId[0]) && _.has(listFilterOr[splitItemId[0]], [splitItemId[1]])) {
                        foundInOr = listFilterOr[splitItemId[0]][splitItemId[1]];
                    }
                } else {
                    const itemIdPath = itemId.replace(/\//g, '.'); // replace / with .
                    if (_.has(listFilterOr, itemIdPath)) {
                        foundInOr = listFilterOr[itemIdPath];
                    }
                }
            });
            if (foundInOr !== undefined) {
                if (_.isObject(foundInOr)) {
                    return _.isEqual(foundInOr, value);
                } else {
                    return foundInOr === value;
                }
            }
        }
        if (itemId.indexOf('.')) {
            let splitItemId = itemId.split('.');
            // we cannot use . notation in keys, but sometimes have to :-(
            // workaround is now to use profile/countryId.$in in the keys in getListFilterSelected function
            let itemKey = splitItemId[0].replace(/\//g, '.');
            let itemValue = listFilter[itemKey] ? listFilter[itemKey][splitItemId[1]] : undefined;
            if (_.isArray(itemValue)) {
                return _.contains(itemValue, value);
            } else {
                return itemValue === value;
            }
        } else {
            // we cannot use . notation in keys, but sometimes have to :-(
            // workaround is now to use profile/countryId.$in in the keys in getListFilterSelected function
            itemId = itemId.replace(/\//g, '.');
            if (listFilter[itemId]) {
                if (_.isArray(listFilter[itemId])) {
                    return _.contains(listFilter[itemId], value);
                } else {
                    return listFilter[itemId] === value;
                }
            }
        }

        return false;
    },
    getListFilterValue(itemId, type, multiplier) {
        let listFilter = Template.instance().listFilters.get();
        let value = undefined;
        if (itemId.indexOf('.') > 0) {
            let splitItemId = itemId.split('.');
            if (_.has(listFilter, splitItemId[0] + '.' + splitItemId[1] )) {
                if (splitItemId[2]) {
                    value = listFilter[ splitItemId[0] + '.' + splitItemId[1] ][splitItemId[2]];
                } else {
                    value = listFilter[ splitItemId[0] + '.' + splitItemId[1] ];
                }
            } else {
                value = listFilter[splitItemId[0]] ? listFilter[splitItemId[0]][splitItemId[1]] : undefined;
            }
        } else {
            value = listFilter[itemId];
        }

        switch(type) {
            case "number":
                value = Number(value);
                if (multiplier) {
                    value = Number(value) / multiplier;
                }
                break;
            case "date":
                value = value ? moment(value).format(TAPi18n.__('_date_format')) : "";
                break;
        }

        return value;
    },
    isActive(name) {
        let template = Template.instance();
        if (template[name] && template[name].get) {
            return Template.instance()[name].get() ? 'active' : false;
        }
    },
    isActiveStatusFilter() {
        let statusFilter = Template.instance().statusFilter.array() || [];
        return _.contains(statusFilter, this.toString()) ? 'active' : '';
    },
    getActiveItem() {
        let template = Template.instance();
        let itemId = template.activeItem.get();
        if (itemId) {
            let item = template.collection.findOne({
                _id: itemId
            });
            return item
        }
    },
    isActiveItem() {
        const template = Template.instance();
        let activeItem = template.activeItem;
        if (!activeItem) {
            let parentTemplate = _.isFunction(template.parentTemplate) ? template.parentTemplate() : false;
            activeItem = parentTemplate ? parentTemplate.activeItem : null;
        }
        if (!activeItem) {
            activeItem = template.data.activeItem;
        }

        if (activeItem && activeItem.get) {
            return activeItem.get() === this._id ? 'active' : '';
        } else if (_.isString(activeItem)) {
            return activeItem === this._id ? 'active' : '';
        }
    },
    moreResults() {
        return Template.instance().moreResults.get();
    },
    addingItem() {
        let template = Template.instance();
        return template.addingItem ? template.addingItem.get() : false;
    },
    getSorting() {
        return Template.instance().sort.get();
    },
    isHeaderSorting(colId) {
        if (this.sorting && this.sorting[colId]) {
            return this.sorting[colId] === -1 ? 'sorting-desc' : 'sorting-asc';
        }
    },
    runningExport() {
        const template = Template.instance();
        return _.isObject(template.runningExport) ? template.runningExport.get() : false;
    },
    getTranslations() {
        const template = Template.instance();
        return template.translations;
    }
};

let saveFile = function(file, context) {
    let newFile = new FS.File(file);
    _saveFile(newFile, context);
};
let saveDataUrl = function(file, context) {
    let newFile = new FS.File();
    newFile.name(file.name.replace(/[^a-z0-9-_.]/ig, '_'));
    newFile.attachData(file.base64Url);

    _saveFile(newFile, context);
};

let _saveFile = function (newFile, context) {
    _.each(context, (contextValue, contextKey) => {
        newFile[contextKey] = contextValue;
    });

    newFile.createdAt = new Date();
    newFile.uploadedBy = Meteor.userId();
    Files.insert(newFile, function (err) {
        if (err) {
            toastr.error(err);
        }
    });
};

Vatfree.templateHelpers.handleFileUpload = function(file, context, disallowMultiple) {
    if (disallowMultiple) {
        Files.find(_.clone(context)).forEach((file) => {
            Files.update({_id: file._id}, {$set: {deleted: true}});
        });
    }

    if (file.type === "application/pdf") {
        import swal from "sweetalert2";
        swal({
            title: "Convert PDF to images?",
            text: "Every page of this PDF will be turned into a JPEG image. The original PDF will be saved in the internal documents.",
            type: "warning",
            showCloseButton: true,
            showCancelButton: true,
            cancelButtonText: "No, thank you",
            confirmButtonText: "Yes, convert it!",
            confirmButtonColor: '#ed5565',
            reverseButtons: true,
            closeOnConfirm: false
        }).then(function () {
            Vatfree.handlePDFUpload(file, context, saveDataUrl);

            let internalContext = JSON.parse(JSON.stringify(context));
            internalContext.target += "_internal";
            saveFile(file, internalContext);

        }, function (dismiss) {
            saveFile(file, context);
        });
    } else {
        saveFile(file, context);
    }
};

Vatfree.convertFileFromPDF = function(fileId) {
    let file = Files.findOne({_id: fileId});
    if (!file) return false;

    let context = Vatfree.getFileContext(file);

    fetch(file.url())
        .then(res => res.blob())
        .then(blob => {
            blob.name = file.name();
            Vatfree.handlePDFUpload(blob, context, saveDataUrl);
        });
};

Vatfree.getFileContext = function(file) {
    let fileContext = {
        target: file.target,
        itemId: file.itemId
    };

    if (file.receiptId) fileContext.receiptId = file.receiptId;
    if (file.userId) fileContext.userId = file.userId;

    return fileContext;
};

Vatfree.handlePDFUpload = function(file, context, fileSaver) {
    const fileReader = new FileReader();
    fileReader.onload = function() {
        pdfjsLib.getDocument(this.result).then(function (pdf) {
            console.log('pages', pdf.numPages);
            for (var i = 1; i <= pdf.numPages; i++) {
                let pageNumber = i;
                pdf.getPage(pageNumber).then(function (page) {
                    let scale = 1.5;
                    let viewport = page.getViewport(scale);

                    let canvas = document.createElement('canvas');
                    let canvasContext = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;

                    page.render({canvasContext: canvasContext, viewport: viewport}).then(function() {
                        // save new file
                        let base64Url = canvas.toDataURL('image/png', 1.0);
                        let newFile = {
                            name: file.name.replace(/[^a-z0-9-_.]/ig, '_').replace('.pdf', '') + '_' + pageNumber + '.jpg',
                            type: 'image/jpeg',
                            base64Url: base64Url
                        };
                        if (pageNumber === 1 && file.subType) {
                            // set the sub type on the first page
                            newFile.subType = file.subType;
                        }
                        fileSaver(newFile, context);
                    });
                });
            }
        });
    };
    fileReader.readAsArrayBuffer(file);
};

Vatfree.templateHelpers.events = function () {
    return {
        'click .toggle-imports'(e, template) {
            e.preventDefault();
            if (template.showImports) {
                template.showImports.set(!template.showImports.get());
            }
        },
        'click .show-management-functions'(e, template) {
            e.preventDefault();
            template.showManagementFunctions.set(!template.showManagementFunctions.get());
        },
        'keyup input[name="item-search"]': _.debounce(function (e, template) {
            e.preventDefault();
            let searchTerm = template.$('input[name="item-search"]').val();
            template.searchTerm.set(searchTerm);
        }, 500),
        'change input[name="item-search"]'(e, template) {
            e.preventDefault();
            let searchTerm = template.$('input[name="item-search"]').val();
            template.searchTerm.set(searchTerm);
        },
        'click .filter-icon.status-filter-icon'(e, template) {
            e.preventDefault();
            e.stopPropagation();
            let status = this.toString();
            let statusFilter = template.statusFilter.array();

            if (_.contains(statusFilter, status)) {
                template.statusFilter.clear();
                _.each(statusFilter, (_status) => {
                    if (status !== _status) {
                        template.statusFilter.push(_status);
                    }
                });
            } else {
                template.statusFilter.push(status);
            }
        },
        'click .toggle-list-filters'(e, template) {
            e.preventDefault();
            if (e.offsetX > 41) {
                // X
                template.listFilters.set({});
            } else {
                template.showListFilters.set(!template.showListFilters.get());
            }
        },
        'click .todos-filter'(e, template) {
            e.preventDefault();
            if (template.todoStatus) {
                let currentStatusFilter = template.statusFilter.array().sort();
                if (currentStatusFilter.length === template.todoStatus.length && _.difference(currentStatusFilter, template.todoStatus).length === 0) {
                    template.statusFilter.clear();
                } else {
                    template.statusFilter.clear();
                    _.each(template.todoStatus, (status) => {
                        template.statusFilter.push(status);
                    });
                }
            }
        },
        'click .input-group-btn'(e, template) {
            if (e.offsetX < 0 && template.searchTerm) {
                template.searchTerm.set();
            }
        },
        'change input[name^="listFilter."], change select[name^="listFilter."], change textarea[name^="listFilter."]'(e, template) {
            e.preventDefault();
            let listFilters = {};

            const processNumber = function (value, multiplier) {
                value = Number(value);
                if (multiplier) {
                    value = value * multiplier;
                }
                return value;
            };

            const processBoolean = function (value) {
                return value.toString().length > 0 ? value === "true" : null;
            };

            template.$('[name^="listFilter."]').each(function() {
                let $el = $(this);
                let itemName = $el.attr('name').replace('listFilter.', '');
                let values = $el.val();
                if (values) {
                    let compare = $el.data('compare');
                    let type = $el.data('type');
                    if (!compare) {
                        if (_.isArray(values)) {
                            compare = '$in';
                        } else {
                            compare = '$eq';
                        }
                    }

                    if (!listFilters[itemName]) {
                        listFilters[itemName] = {};
                    }

                    switch (type) {
                        case "number":
                            let multiplier = $el.data('multiplier');
                            if (_.isArray(values)) {
                                let newValues = [];
                                _.each(values, (value) => {
                                    newValues.push(processNumber(value, multiplier));
                                });
                                values = newValues;
                            } else {
                                values = processNumber(values, multiplier);
                            }
                            break;
                        case "boolean":
                            values = processBoolean(values);
                            break;
                        case "date":
                            let mDate = moment(values, TAPi18n.__('_date_format'));
                            if (compare) {
                                if (compare === '$gt' || compare === '$gte') {
                                    mDate.startOf('day');
                                } else if (compare === '$lt' || compare === '$lte') {
                                    mDate.endOf('day');
                                }
                            }
                            values = mDate.toDate();
                            break;
                    }

                    listFilters[itemName][compare] = values;

                    if (compare === '$exists') {
                        if (values === true) {
                            listFilters[itemName]['$ne'] = "";
                        } else {
                            listFilters['$or'] = [
                                {
                                    [itemName]: {
                                        $exists: false
                                    }
                                },
                                {
                                    [itemName]: {
                                        $eq: ""
                                    }
                                }
                            ];
                            delete listFilters[itemName];
                        }
                    } else if (compare === '$keyExists') {
                        let filterItems = JSON.parse(JSON.stringify(listFilters[itemName]['$keyExists']));
                        if (!_.isArray(filterItems)) filterItems = [filterItems];

                        if (!_.has(listFilters, '$or')) listFilters['$or'] = [];
                        _.each(filterItems, (filterItem) => {
                            listFilters['$or'].push({
                                [itemName + '.' + filterItem]: {
                                    $exists: true
                                }
                            });
                        });
                        delete listFilters[itemName];
                    }

                    if (compare === '$regex') {
                        listFilters[itemName]['$options'] = 'i';
                    }
                }
            });

            template.listFilters.set(listFilters);
        },
        'click .item-list table th.sort-header'(e, template) {
            let data = $(e.currentTarget).data();
            if (data && data.id && template.sort) {
                let currentSort = template.sort.get();
                let sortOrder = -1;
                if (currentSort && currentSort[data.id] === -1) {
                    sortOrder = 1;
                }
                template.sort.set({
                    [data.id]: sortOrder
                });
            }
        },
        'click .item-row'(e, template) {
            if (template.data.disableClicks !== true) {
                if (template.activeItem) {
                    if (template.activeItem.get() === this._id) {
                        template.activeItem.set();
                    } else {
                        template.activeItem.set(this._id);
                    }
                }
            }
        },
        'dblclick .item-row'(e, template) {
            if (template.data.disableClicks !== true && Vatfree.userIsInRole(Meteor.userId(), 'admin')) {
                e.preventDefault();
                if (template.activeItem) {
                    template.activeItem.set(this._id);
                }
                if (template.detailUrl) {
                    e.stopPropagation();
                    FlowRouter.go(template.detailUrl, {itemId: this._id});
                }
            }
        },
        'click .edit-item'(e, template) {
            e.preventDefault();
            if (template.activeItem) {
                template.activeItem.set(this._id);
            }
            if (template.detailUrl) {
                e.stopPropagation();
                FlowRouter.go(template.detailUrl, {itemId: this._id});
            }
        },
        'click .dropzone .add-file': function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('input[name="dropzone-file"]').trigger('click');
        },
        'change input[name="dropzone-file"]': function (e, template) {
            e.preventDefault();
            e.stopPropagation();
            let context = this;
            FS.Utility.eachFile(e, function (file) {
                template.handleFileUpload.call(context, file);
            });
            $(e.currentTarget).val('');
        },
        'dropped .dropzone': function (e, template) {
            e.preventDefault();
            e.stopPropagation();
            $('.dropzone').removeClass('visible');
            let context = this;
            FS.Utility.eachFile(e, function (file) {
                template.handleFileUpload.call(context, file);
            });
        },
        'click .delete-file'(e) {
            e.preventDefault();
            const fileId = this._id;
            let trashBin = $(e.currentTarget).data('trash');
            if (trashBin) {
                Files.update({
                    _id: fileId
                },{
                    $set: {
                        target: trashBin
                    }
                });
            } else {
                import swal from 'sweetalert';
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: true
                }, function () {
                    Files.update({
                        _id: fileId
                    },{
                        $set: {
                            deleted: true
                        }
                    });
                    toastr.success("File has been deleted.");
                });
            }
        },
        'click .edit-file'(e, template) {
            e.preventDefault();
            if (template.editImageFile) {
                template.editImageFile.set(this);
            }
        },
        'click .add-item'(e, template) {
            e.preventDefault();
            template.addingItem.set(true);
        },
        'click .trash-file'(e) {
            e.preventDefault();
            e.stopPropagation();

            let newTarget = this.target.indexOf('traveller') === 0 ? 'travellers_trash' : 'receipts_trash';
            Files.update({
                _id: this._id
            }, {
                $set: {
                    target: newTarget
                }
            });
        },
        'click .move-file'(e) {
            e.preventDefault();
            e.stopPropagation();

            let newTarget = this.target.indexOf('traveller') === 0 ? 'travellers_internal' : 'receipts_internal';
            if (this.target.indexOf('_internal') > 0) {
                newTarget = newTarget.replace('_internal', '');
            }

            Files.update({
                _id: this._id
            }, {
                $set: {
                    target: newTarget
                }
            });
        },
        'click .create-images-from-pdf'(e) {
            e.preventDefault();
            e.stopPropagation();
            const fileId = this._id;
            import swal from 'sweetalert';
            swal({
                title: 'Create images from PDF?',
                text: 'All images in the PDF will be converted to images',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Create images!',
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    Vatfree.convertFileFromPDF(fileId);
                }
            });
        },
        'click .make-receipt-from-file'(e) {
            e.preventDefault();
            e.stopPropagation();

            let travellerId = this.userId || FlowRouter.getParam('travellerId');
            let imageId = this._id;
            import swal from 'sweetalert';
            import 'swal-forms';
            swal({
                title: 'Create new receipt?',
                text: 'This image will be added as a new receipt to this traveller',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Create receipt!',
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    let country = Countries.findOne({_id: Vatfree.defaults.countryId});
                    Receipts.insert({
                        userId: travellerId,
                        amount: 0,
                        totalVat: 0,
                        status: 'userPending',
                        source: 'office',
                        currencyId: country.currencyId,
                        countryId: Vatfree.defaults.countryId
                    }, (err, receiptId) => {
                        if (err) {
                            toastr.error(err.reason || err.message);
                        } else {
                            Files.update({
                                _id: imageId
                            }, {
                                $set: {
                                    receiptId: receiptId,
                                    itemId: receiptId,
                                    target: 'receipts'
                                },
                                $unset: {
                                    userId: 1
                                }
                            });
                        }
                    });
                }
            });
        }
    };
};

Vatfree.templateHelpers.openModal = function(templateName, context) {
    let modalContainer;
    const destroyModal = function(template) {
        Blaze.remove(modalContainer);
    };
    const originalOnHide = context.onHide;
    context.onHide = function(template) {
        if (originalOnHide) {
            originalOnHide(template);
        }
        destroyModal(template);
    };
    modalContainer = Blaze.renderWithData(Template[templateName], context, document.body);
};

Template.taskModal.onCreated(function() {
    this.taskLockId = "task_lock_" + Meteor.userId();
    this.taskLock = false;
    this.lockId = "";
    this.lock = false;
    this.debug = true;
    this.options = this.data.options || {};
    this.parentTemplateVariable = this.options.parentTemplateVariable || "doingTask";

    this.taskLock = new Lock(this.taskLockId, Meteor.userId());

    this.autorun(() => {
        let data = Template.currentData();
        if (data.options.activeItem) {
            this.lockId = data.options.lockId + data.options.activeItem;
            Tracker.nonreactive(() => {
                if (this.lock) {
                    if (this.debug) console.log('releasing lock');
                    this.lock.release();
                    this.lock = null;
                }
                if (this.debug) console.log('creating lock', this.lockId);
                this.lock = new Lock(this.lockId, Meteor.userId());
            });
        } else {
            if (this.lockId && this.lock) {
                this.lock.release();
            }
        }
    });

    let template = this;
    this.taskTemplate = Template.instance().parentTemplate(1);
    this.parentTemplate = Template.instance().parentTemplate(2);
    this.hideModal = () => {
        if (!this.view.isDestroyed) {
            const modal = template.$('#modal-task');
            if (modal && modal.length) {
                modal.modal('hide');
            }
        }
    };

    Tracker.afterFlush(() => {
        $('#modal-task').on('hidden.bs.modal', function () {
            if (template.parentTemplateVariable && template.parentTemplate && template.parentTemplate[template.parentTemplateVariable]) {
                template.parentTemplate[template.parentTemplateVariable].set(false);
            }
            if (_.isFunction(template.options.onHide)) {
                template.options.onHide.call(template);
            }
            if (template.lockId && template.lock) {
                template.lock.release();
            }
            if (template.taskLockId && template.taskLock) {
                template.taskLock.release();
            }
        });
        $('#modal-task').on('shown.bs.modal', function () {
            if (_.isFunction(template.options.onShow)) {
                template.options.onShow.call(template);
            }
        });
        console.log('showing task modal');
        $('#modal-task').modal('show');
    });
});

Template.taskModal.onDestroyed(function() {
    this.hideModal();

    if (this.lockId && this.lock) {
        this.lock.release();
    }
    if (this.taskLockId && this.taskLock) {
        this.taskLock.release();
    }
    if (this.parentTemplateVariable && this.parentTemplate && this.parentTemplate[this.parentTemplateVariable]) {
        this.parentTemplate[this.parentTemplateVariable].set(false);
    }

    $('body').removeClass('modal-open');
    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.taskModal.helpers({
    isLocked() {
        let template = Template.instance();
        return template.lock && template.lock.isLocked() && !template.lock.isMine(Meteor.userId());
    },
    lockUser() {
        let template = Template.instance();
        return !template.lock || !template.lock.user();
    },
    getSidebarTemplate() {
        if (this.options.sidebarTemplate) {
            return this.options.sidebarTemplate;
        }
    },
    getSidebarSize() {
        return this.options.sidebarSize || 'normal';
    },
    getLeftSidebarTemplate() {
        if (this.options.leftSidebarTemplate) {
            return this.options.leftSidebarTemplate;
        }
    },
    getLeftSidebarSize() {
        return this.leftSidebarSize || 'normal';
    }
});

Template.taskModal.events({
    'click .close-tasks'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'click .task-left-sidebar .file'(e, template) {
        let parentTemplate = (template.taskTemplate && template.taskTemplate.selectedFile) ? template.taskTemplate : template.parentTemplate;
        if (parentTemplate.selectedFile) {
            e.preventDefault();
            parentTemplate.selectedFile.set();

            Tracker.afterFlush(() => {
                Meteor.call('getDirectUrl', this._id, ['vatfree', 'vatfree-thumbs'], (err, directUrls) => {
                    let file = _.clone(this);
                    file.directUrls = directUrls;
                    parentTemplate.selectedFile.set(file);
                    Tracker.afterFlush(() => {
                        if (parentTemplate.imageMode && parentTemplate.imageMode.get() === 'zoom') {
                            // skip all the jQuery stuff
                            Meteor.setTimeout(() => {
                                if (document.querySelector('.task-zoom')) {
                                    document.querySelector('.task-zoom').dispatchEvent(new CustomEvent('wheelzoom.destroy'));
                                    wheelzoom(document.querySelector('.task-zoom'));
                                }
                            }, 600);
                        }
                    });
                });
            });
        }
    }
});

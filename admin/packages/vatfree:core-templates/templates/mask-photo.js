Template.mask_photo.onCreated(function() {
    this.canvas = null;
    this.ratio = 0;
    this.scale = 0;

    this.isSaving = new ReactiveVar();
});

let initFabricCanvas = function (template, url, fabric) {
    let isDown, origX, origY;
    template.canvas = new fabric.Canvas('mask-photo-canvas', {selection: true, uniScaleTransform: true});
    fabric.Image.fromURL(url, function (img) {
        template.ratio = img.height / img.width;
        let width = template.$('.mask-photo-div').width();
        let maxHeight = template.$('.mask-photo-div').height();

        let height = width * template.ratio;
        if (height > maxHeight) {
            height = maxHeight;
            width = height / template.ratio;
        }

        template.scale = img.width / width;

        template.canvas.setWidth(width);
        template.canvas.setHeight(height);
        template.canvas.setBackgroundImage(img, template.canvas.renderAll.bind(template.canvas), {
            backgroundImageOpacity: 1,
            backgroundImageStretch: true,
            scaleY: height / img.height,
            scaleX: width / img.width,
        });
        template.canvas.renderAll();
        template.canvas.calcOffset();
        template.canvas.toDataURL('image/jpeg', 0.6);
    });

    fabric.util.addListener(document.body, 'keydown', function (options) {
        if (!template.canvas || options.repeat) {
            return;
        }
        let key = options.which || options.keyCode; // key detection
        let activeObject = template.canvas.getActiveObject();
        if (activeObject && (key === 8 || key === 46)) {
            template.canvas.remove(activeObject);
        }
    });

    template.canvas.on('mouse:down', function (o) {
        if (o.target) {
            o.target.set({
                opacity: 0.4
            });
            return;
        }

        isDown = true;
        let pointer = template.canvas.getPointer(o.e);
        origX = pointer.x;
        origY = pointer.y;
    });

    template.canvas.on('mouse:up', function (o) {
        if (isDown) {
            isDown = false;
            let pointer = template.canvas.getPointer(o.e);
            let rect = new fabric.Rect({
                left: origX > pointer.x ? Math.abs(pointer.x) : origX,
                top: origY > pointer.y ? Math.abs(pointer.y) : origY,
                originX: 'left',
                originY: 'top',
                width: Math.abs(origX - pointer.x),
                height: Math.abs(origY - pointer.y),
                fill: 'black',
                selectable: true
            });
            template.canvas.add(rect);
            template.canvas.renderAll();
        } else {
            let obj = template.canvas.getActiveObject();
            obj.set({
                opacity: 1
            });
            template.canvas.renderAll();
        }
    });
};

Template.mask_photo.onRendered(function() {
    let template = this;
    this.autorun(() => {
        if (this.data && this.data.photo && this.data.photo && this.data.photo.url() && this.data.photo.isUploaded()) {
            let url = this.data.photo.url();
            Tracker.afterFlush(() => {
                Tracker.nonreactive(() => {
                    import("fabric")
                        .then((imported) => {
                            initFabricCanvas(template, url, imported.fabric);
                        });
                });
            });
        }
    });
});

Template.mask_photo.onDestroyed(function() {
    if (this.canvas) {
        this.canvas.dispose();
    }
});

Template.mask_photo.helpers({
    isSaving() {
        return Template.instance().isSaving.get();
    }
});

Template.mask_photo.events({
    'click #generate-output'(e, template) {
        e.preventDefault();

        let objects = template.canvas.getObjects();

        template.isSaving.set(true);
        Tracker.afterFlush(() => {
            //template.$(e.currentTarget).disabled(true);
            let canvas = document.createElement("canvas");
            let url = template.data.photo.url();
            import("fabric")
                .then((imported) => {
                    const fabric = imported.fabric;
                    fabric.Image.fromURL(url, function (img) {
                        canvas.height = img.height;
                        canvas.width = img.width;
                        let outputCanvas = new fabric.Canvas(canvas);
                        outputCanvas.setBackgroundImage(img, outputCanvas.renderAll.bind(outputCanvas), {
                            backgroundImageOpacity: 1,
                            backgroundImageStretch: true
                        });

                        _.each(objects, (object) => {
                            let rect = new fabric.Rect({
                                left: object.left * template.scale,
                                top: object.top * template.scale,
                                originX: object.originX,
                                originY: object.originY,
                                angle: object.angle,
                                width: object.width * template.scale,
                                height: object.height * template.scale,
                                fill: object.fill
                            });
                            outputCanvas.add(rect);
                        });

                        let newFile = {};
                        newFile.name = template.data.photo.name();
                        newFile.itemId = template.data.photo.itemId;
                        newFile.target = template.data.photo.target;
                        if (template.data.photo.subType) newFile.subType = template.data.photo.subType;
                        newFile.createdAt = template.data.photo.createdAt;
                        newFile.updatedAt = new Date();
                        newFile.uploadedBy = Meteor.userId();
                        newFile.originalFile = template.data.photo._id;

                        // contexts
                        _.each(['userId', 'travellerId', 'receiptId'], (context) => {
                            if (template.data.photo[context]) newFile[context] = template.data.photo[context];
                        });

                        newFile.fileContent = outputCanvas.toDataURL({
                            format: 'jpeg',
                            quality: 0.4
                        });
                        Meteor.call('save-file', newFile, (err, fileId) => {
                            if (err) {
                                console.log(err);
                                toastr.error(err.reason || err.message);
                            } else {
                                let newTarget = template.data.photo.target.replace('_internal', '') + "_internal";
                                if (newTarget === "travellers_internal") {
                                    // masked traveller docs will be trashed
                                    newTarget = "travellers_trash";
                                }
                                Files.update({
                                    _id: template.data.photo._id
                                },{
                                    $set: {
                                        target: newTarget,
                                        updatedAt: new Date,
                                        supercededBy: newFile._id
                                    },
                                    $unset: {
                                        subType: 1
                                    }
                                });

                                if (_.isFunction(template.data.callback)) {
                                    template.data.callback(fileId);
                                }
                                template.isSaving.set();
                            }
                        });
                    });
                });

        });
    }
});

FlowRouter.notFound = {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "notFound"});
    }
};

FlowRouter.route('/fileCheck', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "file_check"});
    }
});

Package.describe({
    name: 'vatfree:core-templates',
    summary: 'Vatfree core templates package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'dburles:google-maps@1.1.5',
        'flemay:less-autoprefixer@1.2.0',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'vatfree:core'
    ]);

    // client files
    api.addFiles([
        'router.js',
        'css/cropper.less',
        'css/edit-image-modal.less',
        'css/files-to-check.less',
        'css/info-file.less',
        'css/mask.less',
        'css/modal.less',
        'css/tasks.less',
        'templates/helpers.js',
        'templates/autosize.js',
        'templates/image-helpers.js',
        'templates/ui-helpers.js',
        'templates/billing-form.html',
        'templates/billing-form.js',
        'templates/crop-photo.html',
        'templates/crop-photo.js',
        'templates/defaults.js',
        'templates/edit-image-modal.html',
        'templates/edit-image-modal.js',
        'templates/file-check.html',
        'templates/file-check.js',
        'templates/edit-form-helpers.js',
        'templates/direct-url-image.html',
        'templates/direct-url-image.js',
        'templates/mask-photo.html',
        'templates/mask-photo.js',
        'templates/modal.html',
        'templates/modal.js',
        'templates/activity-timeline-item.html',
        'templates/activity-timeline-item.js',
        'templates/not-found.html',
        'templates/qrcode.js',
        'templates/tab-files.html',
        'templates/tab-files.js',
        'templates/info-file.html',
        'templates/info-file.js',
        'templates/imports.html',
        'templates/imports.js',
        'templates/imports-list.html',
        'templates/imports-list.js',
        'templates/task-modal.html',
        'templates/task-modal.js',
        'templates/translations.html',
        'templates/translations.js',
        'templates/forms/map.html',
        'templates/forms/map.js',
        'templates/zoom-photo.html',
        'templates/zoom-photo.js'
    ], 'client');

    api.export([
    ]);
});

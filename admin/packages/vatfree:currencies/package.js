Package.describe({
    name: 'vatfree:currencies',
    summary: 'Vatfree currencies package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'hermanitos:activity-stream@0.0.1',
        'tap:i18n',
        'tap:i18n-db',
        'vatfree:core'
    ]);

    // shared files
    api.addFiles([
        'currencies.js',
        'lib/currencies.js'
    ]);

    // server files
    api.addFiles([
        'server/currencies.js',
        'publish/currencies.js',
        'publish/currencies-i18n.js',
        'publish/currency.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/currencies.html',
        'templates/currencies.js'
    ], 'client');

    api.export([
        'Currencies'
    ]);
});

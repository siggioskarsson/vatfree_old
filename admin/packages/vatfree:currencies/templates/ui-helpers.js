UI.registerHelper('getCurrencyName', function(currencyId) {
    return Vatfree.currencies.getCurrencyName.call(this, currencyId);
});

UI.registerHelper('getCurrencyCode', function(currencyId) {
    return Vatfree.currencies.getCurrencyCode.call(this, currencyId);
});

UI.registerHelper('getCurrencySymbol', function(currencyId) {
    return Vatfree.currencies.getCurrencySymbol.call(this, currencyId);
});

UI.registerHelper('getCurrencies', function() {
    return Currencies.find({}, {
        sort: {
            name: 1
        }
    });
});

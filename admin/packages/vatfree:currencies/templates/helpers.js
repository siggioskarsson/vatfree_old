/* global currencyHelpers: true */

currencyHelpers = {
    getCurrencies() {
        let template = Template.instance();
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
            let searchTerms = searchTerm.split(' ');

            selector['$and'] = [];
            _.each(searchTerms, (s) => {
                selector['$and'].push({textSearch: new RegExp(s)});
            });
        }

        return Currencies.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get(),
            transform: false
        });
    },
    getVatRates() {
        let rates = [];
        _.each(this.vatRates, (rate) => {
            rates.push(rate.rate + '%');
        });

        return rates.join(', ');
    }
};

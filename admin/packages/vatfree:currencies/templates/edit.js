Template.currencyEdit.onCreated(function () {

});

Template.currencyEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('currencies_item', FlowRouter.getParam('itemId'));
    });
});

Template.currencyEdit.helpers({
    itemDoc() {
        return Currencies.findOne({
            _id: FlowRouter.getParam('itemId')
        }, {
            transform: false
        });
    },
    getBreadcrumbTitle() {
        return this.name + ' (' + this.code + ')';
    }
});

Template.currencyEditForm.onCreated(function() {
    this.translations = [
        {
            name: "Name",
            key: "name"
        }
    ];
});

Template.currencyEditForm.onRendered(function() {
});

Template.currencyEditForm.helpers(Vatfree.templateHelpers.helpers);
Template.currencyEditForm.helpers({
});

Template.currencyEditForm.events({
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/currencies');
    },
    'submit form[name="item-edit-form"], submit form[name="item-edit-translations-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template, $(e.currentTarget));

        Currencies.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Currency saved!')
            }
        });
    }
});

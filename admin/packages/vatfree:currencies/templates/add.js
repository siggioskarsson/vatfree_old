Template.addCurrencyModal.onCreated(function() {
    let template = this;
    let parentVariable = this.data.parentVariable || 'addingItem';

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-currency').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-currency')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-add-currency').on('shown.bs.modal', function () {
            //
        });
        $('#modal-add-currency').modal('show');
    });
});

Template.addCurrencyModal.events({
    'click .cancel-add-currency'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-currency-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        Currencies.insert(formData);
        template.hideModal();
    }
});

Template.currenciesList.helpers(Vatfree.templateHelpers.helpers);
Template.currenciesList.helpers(currencyHelpers);
Template.currenciesList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/currency/:itemId', {itemId: this._id});
    }
});

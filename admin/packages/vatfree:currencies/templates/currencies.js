Template.currencies.onCreated(Vatfree.templateHelpers.onCreated(Currencies, '/currencies', '/currency/:itemId'));
Template.currencies.onRendered(Vatfree.templateHelpers.onRendered());
Template.currencies.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.currencies.helpers(Vatfree.templateHelpers.helpers);
Template.currencies.helpers(currencyHelpers);
Template.currencies.events(Vatfree.templateHelpers.events());

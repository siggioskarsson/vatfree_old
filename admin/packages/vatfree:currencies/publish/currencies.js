Meteor.publish('currencies', function(searchTerm, selector, sort, limit, offset) {
    this.unblock();
    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return Currencies.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset
    });
});

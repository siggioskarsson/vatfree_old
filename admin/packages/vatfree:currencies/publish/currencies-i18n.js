TAPi18n.publish('currencies-i18n', function(searchTerm, selector, sort, limit, offset) {
    this.unblock();
    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return Currencies.i18nFind(selector, {
        sort: sort,
        limit: limit,
        offset: offset
    });
});

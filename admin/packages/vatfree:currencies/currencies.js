Currencies = new TAPi18n.Collection('currencies');
Currencies.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Name of the currency",
        optional: false
    },
    code: {
        type: String,
        label: "2 letter currency code",
        optional: false
    },
    symbol: {
        type: String,
        label: "Symbol for the currency",
        optional: false
    },
    notes: {
        type: String,
        label: "Notes on this currency",
        optional: true
    },
    textSearch: {
        type: String,
        autoValue: function() {
            if (this.field("name").value) {
                return ((this.field("name").value || "") + ' ' + (this.field("code").value || "")).toLowerCase().latinize();
            } else {
                this.unset();
            }
        },
        optional: true
    }
}));
Currencies.attachSchema(CreatedUpdatedSchema);

Currencies.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

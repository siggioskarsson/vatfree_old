FlowRouter.route('/currencies', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "currencies"});
    }
});

FlowRouter.route('/currency/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "currencyEdit"});
    }
});

Vatfree.currencies = {};

Vatfree.currencies.getCurrencyName = function(currencyId) {
    if (!currencyId) currencyId = this.currencyId;

    let currency = Currencies.findOne({_id: currencyId});
    if (currency) {
        return currency.name;
    }

    return "";
};

Vatfree.currencies.getCurrencyCode = function(currencyId) {
    if (!currencyId) currencyId = this.currencyId;

    let currency = Currencies.findOne({_id: currencyId});
    if (currency) {
        return currency.code;
    }

    return "";
};

Vatfree.currencies.getCurrencySymbol = function(currencyId) {
    if (!currencyId) currencyId = this.currencyId;

    let currency = Currencies.findOne({_id: currencyId});
    if (currency) {
        return currency.symbol;
    }

    return "";
};

Package.describe({
    name: 'vatfree:api',
    summary: 'Vatfree api package',
    version: '0.0.2',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'underscore',
        'accounts-base@1.2.16',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'dburles:google-maps@1.1.5',
        'flemay:less-autoprefixer@1.2.0',
        'cfs:standard-packages@0.5.9',
        'cfs:ui@0.1.3',
        'cfs:s3@0.1.3',
        'cfs:graphicsmagick@0.0.18',
        'khamoud:slack-api@0.0.2',
        'nimble:restivus',
        'vatfree:core'
    ]);

    // server files
    api.addFiles([
        'server/restivus.js',
        'server/status-codes.js',
        'server/postmark.js',
        'server/api.js',
        'server/api/is-logged-in.js',
        'server/api/receipt.js',
        'server/api/receipts.js'
    ], 'server');

    api.export([
    ]);
});

/*
Tests are not working yet :-(

Package.on_test(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'underscore',
        'matb33:collection-hooks',
        'xolvio:cleaner',
        'meteortesting:mocha',
        'practicalmeteor:sinon',
        'practicalmeteor:chai'
    ]);

    api.use([
        'vatfree:api'
    ]);

    api.addFiles([
        'tests/api.js'
    ], ['server']);
});
*/

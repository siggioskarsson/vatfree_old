Api.addRoute('receipt/:receiptId', {
    authRequired: true
}, {
    get: {
        action: function () {
            if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
                return statusCodes.noAccess;
            }
            let receipts = Vatfree.api.getReceipts(this.userId, {_id: this.urlParams.receiptId});
            if (receipts && receipts.length === 1) {
                let receipt = receipts[0];
                receipt.files = Vatfree.api.getFiles('receipts', {itemId: this.urlParams.receiptId});
                return Vatfree.api.returnData(receipt);
            } else {
                return statusCodes.noData;
            }
        }
    }
});

Api.addRoute('receipt', {
    authRequired: true
}, {
    post: {
        action: function () {
            if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
                return statusCodes.noAccess;
            }
            console.log(this);
        }
    }
});

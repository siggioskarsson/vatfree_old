Api.addRoute('receipts', {
    authRequired: true
}, {
    get: {
        action: function () {
            if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
                return statusCodes.noAccess;
            }
            return Vatfree.api.returnData(Vatfree.api.getReceipts(this.userId));
        }
    }
});

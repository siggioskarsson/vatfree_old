Api.addRoute('is-logged-in', {
    authRequired: true
}, {
    error: function() {
        return Vatfree.api.returnData({
            loggedIn: false
        });
    },
    get: {
        action: function () {
            return Vatfree.api.returnData({
                loggedIn: !!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)
            });
        }
    }
});

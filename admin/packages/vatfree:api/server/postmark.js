Api.addRoute('postmark-email-bounce', {
    authRequired: false
}, {
    post: {
        authRequired: false,
        action: function () {
            try {
                let params = _.extend(this.urlParams, this.queryParams, this.bodyParams);

                let softBounce = (params.Description.indexOf('temporarily') > 0 || params.Description.indexOf('Automatic') > 0);
                let activity = {
                    type: 'task',
                    status: softBounce ? 'done' : 'new',
                    subject: 'Email bounced: ' + params.Email + ' - ' + params.Description,
                    notes: JSON.stringify(params, null, 2),
                    source: JSON.stringify(params)
                };

                let user = Meteor.users.findOne({
                    $or: [
                        {
                            "profile.email": params.Email.toLowerCase()
                        },
                        {
                            "emails.address": params.Email.toLowerCase()
                        }
                    ]
                });
                if (user && user._id && user.roles && user.roles.__global_roles__) {
                    if (_.contains(user.roles.__global_roles__, 'traveller')) {
                        activity.travellerId = user._id;
                    } else if (_.contains(user.roles.__global_roles__, 'contact')) {
                        activity.contactId = user._id;
                    } else if (_.contains(user.roles.__global_roles__, 'affiliate')) {
                        activity.affiliateId = user._id;
                    }
                }

                ActivityLogs.insert(activity);

                return {
                    statusCode: 200,
                    body: {
                        status: "ok",
                        message: "ok"
                    }
                };
            } catch (e) {
                console.log(e);
                return statusCodes.processingError;

            }
        }
    }
});

Api.addRoute('postmark-email-read', {
    authRequired: false
}, {
    post: {
        authRequired: false,
        action: function () {
            try {
                let params = _.extend(this.urlParams, this.queryParams, this.bodyParams);

                ActivityLogs.update({
                    _id: params.MessageID,
                    readOn: {
                        $exists: false
                    }
                },{
                    $set: {
                        readOn: new Date()
                    }
                });

                return {
                    statusCode: 200,
                    body: {
                        status: "ok",
                        message: "ok"
                    }
                };
            } catch (e) {
                console.log(e);
                return statusCodes.processingError;

            }
        }
    }
});

Vatfree.api = {};

Vatfree.api.returnData = function(data) {
    let returnValue = _.clone(statusCodes.ok);
    returnValue.body.data = data;

    return returnValue;
};

Vatfree.api.getReceipts = function (userId, selector) {
    if (!selector) selector = {};
    selector.userId = userId;

    let receipts = [];
    Receipts.find(selector, {
        fields: {
            receiptNr: 1,
            status: 1,
            amount: 1,
            totalVat: 1,
            vat: 1,
            refund: 1,
            serviceFee: 1,
            currencyId: 1,
            shopId: 1,
            customsDate: 1,
            customsCountryId: 1,
            purchaseDate: 1,
            createdAt: 1,
            updatedAt: 1,
            receiptRejectionIds: 1
        }
    }).forEach((receipt) => {
        enrichReceipt(receipt);
        receipts.push(receipt);
    });

    return receipts;
};

Vatfree.api.getFiles = function(target, selector) {
    let files = [];
    Files.find(selector).forEach((file) => {
        files.push({
            name: file.original.name,
            size: file.original.size,
            type: file.original.type,
            url: file.getDirectUrl('vatfree'),
            thumb: file.getDirectUrl('vatfree-thumbs')
        });
    });

    return files;
};

var enrichReceipt = function(receipt) {
    receipt.shop = Vatfree.shops.getObject(receipt.shopId);
    if (receipt.customsCountryId) receipt.customsCountry = getCountry(receipt.customsCountryId);
    receipt.currency = getCurrency(receipt.currencyId);
    if (receipt.receiptRejectionIds) receipt.receiptRejections = getReceiptRejections(receipt.receiptRejectionIds);
};

var getCurrency = function(currencyId) {
    return Currencies.findOne({
        _id: currencyId
    },{
        fields: {
            name: 1,
            code: 1,
            symbol: 1
        }
    });
};

var getCountry = function(countryId) {
    return Countries.findOne({
        _id: countryId
    },{
        fields: {
            name: 1
        }
    });
};

var getReceiptRejections = function(receiptRejectionIds) {
    return ReceiptRejections.find({
        _id: {
            $in: receiptRejectionIds
        }
    },{
        fields: {
            name: 1,
            description: 1
        }
    }).fetch();
};

// curl -X POST http://localhost:6001/api/v1/login -d "email=siggi.oskarsson@gmail.com&password=test123"
// curl -X POST -H "X-Auth-Token: 0TrL2FL_YKWnuvVF69moOd4KDo9NVT6m-V04HQCg0eZ" -H "X-User-Id: d6csdcGkKEQzMAMQP" http://localhost:6001/api/v1/logout
/*
{
  "status": "success",
  "data": {
    "authToken": "0TrL2FL_YKWnuvVF69moOd4KDo9NVT6m-V04HQCg0eZ",
    "userId": "d6csdcGkKEQzMAMQP"
  }
}

curl -H "X-Auth-Token: 0TrL2FL_YKWnuvVF69moOd4KDo9NVT6m-V04HQCg0eZ" -H "X-User-Id: d6csdcGkKEQzMAMQP" http://localhost:6001/api/v1/receipts
curl -H "X-Auth-Token: 0TrL2FL_YKWnuvVF69moOd4KDo9NVT6m-V04HQCg0eZ" -H "X-User-Id: d6csdcGkKEQzMAMQP" http://localhost:6001/api/v1/is-logged-in
 */
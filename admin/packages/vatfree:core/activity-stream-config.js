Meteor.startup(function() {
    ActivityStream.config.securityContext = [
        'shopId',
        'userId',
        'receiptId'
    ];

    // ignore updates for fields in collections
    // the key is the name of the collection (collection._name)
    ActivityStream.config.ignoreUpdatesFor = {
        affiliates: [
           'textSearch'
        ],
        shops: [
           'textSearch'
        ],
        travellers: [
            'textSearch'
        ],
        receipts: [
            'textSearch'
        ],
        countries: [
            'textSearch'
        ],
        invoices: [
            'textSearch'
        ],
        contacts: [
            'textSearch'
        ],
        'email-templates': [
            'textSearch'
        ],
        'email-texts': [
            'textSearch'
        ],
        'activity-logs': [
            'textSearch'
        ],
        users: [
            'services',
            'textSearch',
            'status'
        ]
    };

    /**
     * This function is called every time a activity stream item is added for the application to check whether a
     * notification needs to be set for a user
     *
     * @param userId
     * @param newDoc
     * @param docDiff
     //* /
    ActivityStream.config.notifyUsers = function(userId, newDoc, docDiff, docDiffOld) {
    };
    */

    ActivityStream.config.securityContextCheck = function(userId, context) {
        // check whether the user has access to the given nest
        if (!userId || !context) {
            return false;
        }

        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    };

    ActivityStream.config.options = {
        userName: 'profile.email'
    };
});

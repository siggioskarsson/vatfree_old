TranslatableSchema = new SimpleSchema({
    i18n: {
        type: Object,
        optional: true,
        blackbox: true
    }
});

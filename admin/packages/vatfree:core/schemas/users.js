var userProfile = new SimpleSchema({
    email:{
        type: String,
        label: "Email that is used to communicate. This is used if there are no emails defined in user.emails",
        optional: true
    },
    contactId: {
        type: [String],
        label: "Legacy contact ID from Salesforce",
        optional: true
    },
    wechatId: {
        type: String,
        label: "WeChat ID",
        optional: true
    },
    asperionNr: {
        type: String,
        label: "Legacy asperion Nr from Salesforce",
        optional: true
    },
    salutation: {
        type: String,
        optional: true
    },
    name: {
        type: String,
        optional: true
    },
    birthDate: {
        type: Date,
        optional: true
    },
    gender: {
        type: String,
        allowedValues: [
            'male',
            'female',
            'happy'
        ],
        optional: true
    },
    countryId: {
        type: String,
        optional: true
    },
    passportNumber: {
        type: String,
        optional: true
    },
    passportExpirationDate: {
        type: Date,
        optional: true
    },
    visaNumber: {
        type: String,
        optional: true
    },
    visaExpirationDate: {
        type: Date,
        optional: true
    },
    visaCountryId: {
        type: String,
        optional: true
    },
    addressFirst: {
        type: String,
        optional: true
    },
    city: {
        type: String,
        optional: true
    },
    state: {
        type: String,
        optional: true
    },
    postalCode: {
        type: String,
        optional: true
    },
    addressCountryId: {
        type: String,
        optional: true
    },
    phone: {
        type: String,
        autoValue: function () {
            if (this.isSet) {
                return this.value.replace(/^(\+)/, '00').replace(/[^0-9]/g, '');
            }
        },
        optional: true
    },
    phone2: {
        type: String,
        autoValue: function () {
            if (this.isSet) {
                return this.value.replace(/^(\+)/, '00').replace(/[^0-9]/g, '');
            }
        },
        optional: true
    },
    mobile: {
        type: String,
        autoValue: function () {
            if (this.isSet) {
                return this.value.replace(/[^0-9]/g, '');
            }
        },
        optional: true
    },
    language: {
        type: String,
        optional: true
    },
    terms: {
        type: Date,
        optional: true
    },
    qrUploadContext: {
        type: Object,
        optional: true,
        blackbox: true
    },
    receiptAddData: {
        type: Object,
        optional: true,
        blackbox: true
    },
    extraDocumentation: {
        type: Boolean,
        label: "Whether the user is allowed to upload extra documentation",
        autoValue: function () {
            if (this.field('profile.extraDocumentation').isSet) {
                return !!this.field('profile.extraDocumentation').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    tel: {
        type: String,
        label: "General telephone number",
        optional: true
    },
    tel2: {
        type: String,
        label: "Second telephone number",
        optional: true
    },
    companyId: {
        type: String,
        label: "Company of contact",
        optional: true
    },
    companyName: {
        type: String,
        label: "Company of contact",
        optional: true
    },
    shopId: {
        type: String,
        label: "Contact of a shop",
        optional: true
    },
    affiliateId: {
        type: String,
        label: "Contact of an affiliate",
        optional: true
    },
    jobTitle: {
        type: String,
        label: "Job title of contact",
        optional: true
    },
    shoppingGuideContact: {
        type: Boolean,
        label: "de contactpersoon voor shopping guide gerelateerde zaken, bijvoorbeeld m.b.t. de beschrijving van de winkel",
        optional: true
    },
    authorisationContact: {
        type: Boolean,
        label: "Degene die bijvoorbeeld contract mag tekenen, en uiteindelijk de beslisser is.",
        optional: true
    },
    emailTemplateId: {
        type: String,
        label: "Preferred email template",
        optional: true
    },
    slackId: {
        type: String,
        label: "Slack Id used for internal communication",
        optional: true
    }
});

var userPrivate = new SimpleSchema({
    travellerTypeId: {
        type: String,
        optional: true
    },
    numberOfRequests: {
        type: Number,
        optional: true
    },
    totalRefunds: {
        type: Number,
        optional: true
    },
    totalServiceFee: {
        type: Number,
        optional: true
    },
    lastRequestDate: {
        type: Date,
        optional: true
    },
    affiliateId: {
        type: String,
        label: "Affiliate Id of this user",
        optional: true
    },
    status: {
        type: String,
        allowedValues: [
            'userUnverified',
            'waitingForDocuments',
            'documentationIncomplete',
            'documentationExpired',
            'notEligibleForRefund',
            'userVerified'
        ],
        optional: true
    },
    checkedAt: {
        type: String,
        label: "When the last check of this user was done",
        optional: true
    },
    rejectionIds: {
        type: [String],
        label: "The id's of the rejection reasons given",
        optional: true
    },
    lastReminderSent: {
        type: Date,
        label: "When the last reminder was sent",
        optional: true
    },
    reminders: {
        type: [ RemindersSchema ],
        label: "Reminders sent to this traveller",
        optional: true
    },
    notes: {
        type: String,
        label: "Notes about this traveller",
        optional: true
    },
    pushStatus: {
        type: Object,
        blackbox: true,
        label: "Status of push subscription across multiple devices",
        optional: true
    }
});

const userAffiliate = new SimpleSchema({
    companyName: {
        type: String,
        label: "Company name of the affiliate (temporary)",
        optional: true
    },
    code: {
        type: String,
        label: "Code of the affiliate",
        optional: true
    },
    status: {
        type: String,
        allowedValues: [
            'new',
            'active',
            'silver',
            'gold'
        ],
        optional: true
    },
    serviceFee: {
        type: Number,
        label: "Percentage (as float) of the service fee to give to the affiliate",
        optional: true
    },
    travellerDiscount: {
        type: Number,
        label: "Percentage (as float) of the service fee to give as a discount to the traveller",
        optional: true
    },
    nrTravellers: {
        type: Number,
        label: "Expected number of travellers",
        optional: true
    },
    avgSpend: {
        type: Number,
        label: "Expected average spend per traveller",
        optional: true
    },
    stats: {
        type: Object,
        label: "Stats about this affiliate",
        optional: true,
        blackbox: true
    }
});

const userPartners = new SimpleSchema({
    companyName: {
        type: String,
        label: "Company name of the partner (temporary)",
        optional: true
    },
    status: {
        type: String,
        allowedValues: [
            'new',
            'active',
            'silver',
            'gold'
        ],
        optional: true
    },
    stats: {
        type: Object,
        label: "Stats about this partner",
        optional: true,
        blackbox: true
    }
});

var userSchema = new SimpleSchema({
    emails: {
        type: Array,
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    taskedAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: new Date()
                };
            }
        }
    },
    profile: {
        type: userProfile,
        optional: false
    },
    private: {
        type: userPrivate,
        optional: false
    },
    affiliate: {
        type: userAffiliate,
        optional: true
    },
    partner: {
        type: userPartners,
        optional: true
    },
    // Make sure this services field is in your schema if you're using any of the accounts packages
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    // Used by settlin:accounts-phone to manage your phone numbers
    phones: {
        type: [Object],
        optional: true,
        blackbox: true
    },
    roles: {
        type: Object,
        optional: true,
        blackbox: true
    },
    status: {
        type: Object,
        optional: true,
        blackbox: true
    },
    passwordSet: {
        type: Date,
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    },
    // In order to avoid an 'Exception in setInterval callback' from Meteor
    heartbeat: {
        type: Date,
        optional: true
    }
});
Meteor.users.attachSchema(userSchema);
Meteor.users.attachSchema(CreatedUpdatedSchema);

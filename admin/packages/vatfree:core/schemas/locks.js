Locks = new Meteor.Collection('locks');
Locks.attachSchema(new SimpleSchema({
    lockId: {
        type: String,
        optional: false
    },
    userId: {
        type: String,
        optional: false
    },
    updatedAt: {
        type: Date,
        optional: false
    }
}));

Locks.allow({
    insert: function (userId, doc) {
        return (Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP)) && doc.userId === userId;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return (Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP)) && doc.userId === userId;
    },
    remove: function (userId, doc) {
        return (Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP)) && doc.userId === userId;
    }
});

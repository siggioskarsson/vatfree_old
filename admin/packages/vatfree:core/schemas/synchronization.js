Vatfree.synchronizationSchema = new SimpleSchema({
    serviceId: {
        type: String,
        label: "Identifier of service we are synching with",
        optional: false
    },
    serviceResult: {
        type: Object,
        label: "Result if applicable",
        optional: true,
        blackbox: true
    },
    serviceError: {
        type: Object,
        label: "Error if applicable",
        optional: true,
        blackbox: true
    },
    serviceLinkId: {
        type: String,
        label: "Identifier in linked system",
        optional: false
    },
    serviceParameters: {
        type: Object,
        label: "Optional service parameters or whatever else needs to be saved",
        optional: true,
        blackbox: true
    },
    updatedAt: {
        type: Date,
        label: "When this service link was updated",
        optional: false
    }
});
Vatfree.synchronizationSchemaIndex = function(collection) {
    collection._ensureIndex({
        'synchronization.serviceId': 1,
        'synchronization.serviceLinkId': 1
    });
    collection._ensureIndex({
        _id: 1,
        'synchronization.serviceId': 1
    }, {unique: true});
};

RemindersSchema = new SimpleSchema({
    sentAt: {
        type: Date,
        label: "The date/time the reminder was sent",
        optional: false
    },
    type: {
        type: String,
        label: "Type of reminder sent",
        allowedValues: [
            'email',
            'push',
            'uncooperative',
            'no-email'
        ],
        optional: false
    }
});

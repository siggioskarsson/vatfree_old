OfficeLocations = new Meteor.Collection('office-locations');
OfficeLocations.attachSchema(new SimpleSchema({
    locationName: {
        type: String,
        optional: false
    },
    locationCode: {
        type: String,
        optional: false
    }
}));
OfficeLocations.attachSchema(CreatedUpdatedSchema);

OfficeLocations.allow({
    insert: function (userId, doc) {
        return false;
        },
    update: function (userId, doc, fieldNames, modifier) {
        return false;
    },
    remove: function (userId, doc) {
        return false;
    }
});

if (Meteor.isServer) {
    OfficeLocations._ensureIndex({locationCode: 1}, {unique: true});
}

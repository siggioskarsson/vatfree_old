var cfsOptions = {
	region: "eu-central-1",
	ACL: 'authenticated-read',
	ServerSideEncryption: 'AES256',
	maxTries: 3
};
var cfsThumbOptions = _.clone(cfsOptions);

Files = new FS.Collection("vatfree-files", {
	stores: [
		new FS.Store.S3("vatfree", cfsOptions),
		new FS.Store.S3("vatfree-thumbs", cfsThumbOptions)
	],
	chunkSize: 1024*1024
});
Files.allow({
	insert: function (userId, doc) {
		return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP) || doc.uploadedBy === userId;
	},
	update: function (userId, doc) {
		return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP);
	},
	remove: function (userId, doc) {
		return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP);
	},
	download:function(userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) ||
            Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP) ||
            doc.uploadedBy === userId ||
            (doc.target === "travellers" && doc.userId === userId) ||
            (doc.target === "receipts" && (Receipts.findOne({_id: doc.itemId}, {fields: {userId: 1}}) || {}).userId === userId) ||
            (doc.target === "shops" && doc.category === 'logo') ||
            (doc.target === "companies" && doc.category === 'logo')
            ;
	}
});

const addFindSelector = function(userId, selector, options) {
	if (!selector) selector = {};
	selector.deleted = {$ne: true};
};
Files.files.before.findOne(addFindSelector);
Files.files.before.find(addFindSelector);

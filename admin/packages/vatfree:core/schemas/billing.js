/* global BillingSchema: true */

BillingSchema = new SimpleSchema({
    serviceFee: {
        type: Number,
        label: "The service fee this entity charges",
        optional: true
    },
    minFee: {
        type: Number,
        label: "The service fee this entity charges",
        optional: true
    },
    maxFee: {
        type: Number,
        label: "The service fee this entity charges",
        optional: true
    },
    billingEntity: {
        type: Boolean,
        label: "Whether this shop / company should be billed",
        autoValue: function () {
            if (this.field('billingEntity').isSet) {
                return !!this.field('billingEntity').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    billingCompanyId: {
        type: String,
        label: "The Id of a parent company that is billed, if applicable",
        optional: true
    },
    invoiceServiceFee: {
        type: Boolean,
        label: "Whether the service fee should be billed seperately to this shop / company",
        autoValue: function () {
            if (this.field('invoiceServiceFee').isSet) {
                return !!this.field('invoiceServiceFee').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    checkInvoiceBeforeSending: {
        type: Boolean,
        label: "Whether the invoices of this entity should always be checked before sending",
        autoValue: function () {
            if (this.field('checkInvoiceBeforeSending').isSet) {
                return !!this.field('checkInvoiceBeforeSending').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    invoicePerReceipt: {
        type: Boolean,
        label: "Whether to send 1 invoice per receipt",
        autoValue: function () {
            if (this.field('invoicePerReceipt').isSet) {
                return !!this.field('invoicePerReceipt').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    sendBy: {
        type: String,
        label: "Send by which method",
        allowedValues: [
            'email',
            'post'
        ],
        defaultValue: "email",
        optional: true
    },
    includeImagesPDF: {
        type: Boolean,
        label: "Whether the email should include an images PDF",
        autoValue: function () {
            if (this.field('includeImagesPDF').isSet) {
                return !!this.field('includeImagesPDF').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    billingName: {
        type: String,
        label: "Billing name, if different from shop/company name",
        optional: true
    },
    billingRecipientName: {
        type: String,
        label: "Billing recipient name - the person that gets the bill",
        optional: true
    },
    billingAddress: {
        type: String,
        label: "Billing postal address",
        optional: true
    },
    billingEmail: {
        type: String,
        label: "Billing email address",
        optional: true
    },
    billingEmailCC: {
        type: String,
        label: "Billing email CC addresses",
        optional: true
    },
    billingContacts: {
        type: [String],
        label: "Billing contacts",
        optional: true
    },
    billingEmailSubjectPrefix: {
        type: String,
        label: "Billing email subject prefix",
        optional: true
    },
    billingEmailTemplateId: {
        type: String,
        label: "Billing email template",
        optional: true
    },
    billingEmailTextId: {
        type: String,
        label: "Billing email text template",
        optional: true
    },
    billingReminderEmail: {
        type: String,
        label: "Billing reminder email address",
        optional: true
    },
    billingReminderEmailCC: {
        type: String,
        label: "Billing reminder email CC addresses",
        optional: true
    },
    billingReminderEmailTextId: {
        type: String,
        label: "Billing email reminder text template",
        optional: true
    },
    billingReminder2EmailTextId: {
        type: String,
        label: "Billing email reminder 2 text template",
        optional: true
    },
    poNumber: {
        type: String,
        label: "Purchase order number",
        optional: true
    },
    billingVat: {
        type: String,
        label: "VAT number of entity that is being billed",
        optional: true
    },
    paymentTerms: {
        type: Number,
        label: "Number of days before the invoice should be paid",
        defaultValue: 14,
        optional: true
    },
    debtCollectionAccount: {
        type: String,
        label: "The account for the debt collection",
        optional: true
    },
    debtCollectionMandate: {
        type: String,
        label: "The mandate id from the bank for the debt collection",
        optional: true
    },
    debtCollectionDate: {
        type: Date,
        label: "The date the mandate was signed",
        optional: true
    },
    partnerDashboardFrom: {
        type: Date,
        label: "From which date to show the partner dashboard",
        optional: true
    },
    billingNotes: {
        type: String,
        label: "Shop notes",
        optional: true
    },
    receiptNotes: {
        type: String,
        label: "Receipt checking notes",
        optional: true
    }
});

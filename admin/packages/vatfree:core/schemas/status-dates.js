/* global StatusDates: true */

StatusDates = {};
StatusDates.schema = new SimpleSchema({
    statusDates: {
        type: Object,
        label: "All the last dates of the status",
        optional: true
    },
});

StatusDates.attachToSchema = function(collection) {
    if (_.isFunction(collection.attachSchema)) {
        collection.attachSchema(StatusDates.schema);

        collection.before.update(function(userId, doc, fieldNames, modifier, options) {
            if (_.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status) {
                modifier.$set['statusDates.' + modifier.$set.status] = new Date();
            }
        });
    }
};

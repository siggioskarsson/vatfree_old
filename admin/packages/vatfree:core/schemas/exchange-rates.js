ExchangeRates = new Meteor.Collection('exchange-rates');
ExchangeRates.attachSchema(new SimpleSchema({
    date: {
        type: String,
        optional: false
    },
    base: {
        type: String,
        optional: false
    },
    rates: {
        type: Object,
        optional: false,
        blackbox: true
    }
}));
ExchangeRates.attachSchema(CreatedUpdatedSchema);

ExchangeRates.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return false;
    },
    remove: function (userId, doc) {
        return false;
    }
});

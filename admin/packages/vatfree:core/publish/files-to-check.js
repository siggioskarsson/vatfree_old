Meteor.publish('files-to-check', function(target, limit) {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return Files.find({
        target: target,
        check: true
    },{
        sort: {
            createdAt: 1
        },
        limit: limit
    });
});

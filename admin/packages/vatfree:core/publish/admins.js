Meteor.publish('admins', function (searchTerm, sort, limit, offset) {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    let selector = {
        'roles.__global_roles__' : {$in: ['admin', 'vatfree']}
    };

    return Meteor.users.find(selector, {
        fields: {
            profile: 1,
            status: 1,
            "services.twitter.profile_image_url_https": 1,
            "services.facebook.id": 1,
            "services.google.picture": 1,
            "services.github.username": 1,
            "services.instagram.profile_picture": 1,
            "services.linkedin.pictureUrl": 1,
            textSearch: 1
        }
    });
});

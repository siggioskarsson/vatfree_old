Meteor.publish('imports', function(subType) {
    check(subType, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }
    this.unblock();

    return Files.find({
        target: 'imports',
        subType: subType
    })
});

Package.describe({
    name: 'vatfree:core',
    summary: 'Vatfree core package',
    version: '0.0.2',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'email',
        'underscore',
        'accounts-base@1.2.16',
        'settlin:accounts-phone@1.2.3',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'dburles:google-maps@1.1.5',
        'flemay:less-autoprefixer@1.2.0',
        'froatsnook:valid-email@1.0.0',
        'percolate:synced-cron@1.3.2',
        'matb33:collection-hooks@0.8.4',
        'cfs:standard-packages@0.5.9',
        'cfs:filesystem@0.1.2',
        'cfs:tempstore@0.1.5',
        'cfs:ui@0.1.3',
        'cfs:s3@0.1.3',
        'cfs:graphicsmagick@0.0.18',
        'simonsimcity:job-collection',
        'khamoud:slack-api@0.0.2',
        'meteorhacks:inject-initial@1.0.4',
        'hermanitos:activity-stream'
    ]);

    // client files
    api.addFiles([
        'client/init.js'
    ], 'client');

    // shared files
    api.addFiles([
        'namespace.js',
        'lib/activity-stream.js',
        'lib/billing.js',
        'lib/collections.js',
        'lib/competitors.js',
        'lib/customs.js',
        'lib/dates.js',
        'lib/exchange-rates.js',
        'lib/files.js',
        'lib/languages.js',
        'lib/latinize.js',
        'lib/lock.js',
        'lib/match-all.js',
        'lib/number.js',
        'lib/payments.js',
        'lib/prune.js',
        'lib/roles.js',
        'lib/string.js',
        'lib/search.js',
        'lib/vat.js',
        'schemas/reminders.js',
        'schemas/counters.js',
        'schemas/billing.js',
        'schemas/createdUpdated.js',
        'schemas/exchange-rates.js',
        'schemas/locks.js',
        'schemas/office-locations.js',
        'schemas/status-dates.js',
        'schemas/synchronization.js',
        'schemas/translatable.js',
        'schemas/users.js'
    ]);

    api.addAssets([
        'server/loader.html'
    ], 'server');

    // server files
    api.addFiles([
        'publish/locks.js',
        'publish/office-locations.js',
        'publish/admins.js',
        'publish/files-to-check.js',
        'publish/imports.js',
        'server/schemas/activity-stream.js',
        'server/schemas/collectionfs.js',
        'server/schemas/exchange-rates.js',
        'server/schemas/import-files.js',
        'server/schemas/import-jobs.js',
        'server/schemas/locks.js',
        'server/schemas/synchs.js',
        'server/schemas/users.js',
        'activity-stream-config.js',
        'server/cron.js',
        'server/methods.js',
        'server/notifications.js',
        'server/inject-loader.js',
        'server/import-file.js',
        'server/mail-config.js',
        'server/accounts-passwordless.js',
        'server/accounts-phone.js',
        'server/on-create-user.js',
        'server/form-methods.js',
        'server/pdf.js',
        'server/synch.js'
    ], 'server');

    // client files
    api.addFiles([
        'schemas/collectionfs.js'
    ], 'client');

    api.export([
        'RemindersSchema',
        'FS',
        'Vatfree',
        'Files',
        'BillingSchema',
        'CreatedUpdatedSchema',
        'StatusDates',
        'Counters',
        'Lock',
        'OfficeLocations',
        'Locks',
        'GoogleMaps',
        'importJobs',
        'importFiles',
        'TranslatableSchema',
        'ExchangeRates',
        'Synchs'
    ]);
});

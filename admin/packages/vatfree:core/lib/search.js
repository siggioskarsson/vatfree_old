/* global Vatfree: true */

Vatfree.search = {};

Vatfree.search.addSearchTermSelector = function (searchTerm, selector, searchAttribute = 'textSearch', allowTextIndex = false) {
    if (false && allowTextIndex) {
        let keywords = searchTerm.split(/\s+/).map(kw => `"${kw}"`).join(' ');
        selector['$text'] = {
            '$search': keywords,
            $caseSensitive: false
        };
    } else {
        searchTerm = searchTerm.replace(/[\/\\^$*+?.()|[\]{}]/g, '\\$&'); // escape all regex unsafe characters
        let searchTerms = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize().match(/[\w-&@.\\]+|"[^"]*"/g);

        selector['$and'] = [];
        _.each(searchTerms, (s) => {
            let matches;
            if (matches = s.match(/^"([^"]+)"/)) {
                selector['$and'].push({[searchAttribute]: new RegExp("(^| |:)" + matches[1] + "( |$)", 'i')});
            } else {
                selector['$and'].push({[searchAttribute]: new RegExp(s, 'i')});
            }
        });
    }
};

Vatfree.search.addStatusFilter = function (selector) {
    let statusFilter = this.statusFilter.array() || [];
    if (statusFilter.length > 0) {
        selector[this.statusAttribute] = {
            $in: statusFilter
        };
    }
};

Vatfree.search.addListFilter = function (selector) {
    let listFilters = this.listFilters.get();
    _.each(listFilters, (listFilterValue, listFilterId) => {
        selector[listFilterId] = listFilterValue;
    });
};

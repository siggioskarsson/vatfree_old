/* global Vatfree: true */

Vatfree.ActivityStreamItem = (function() {
    'use strict';
    return function activityStreamItem(setItem) {
        this.constructor = activityStreamItem;
        this.constructor.prototype = new Object();
        this.constructor.prototype.constructor.apply(this, arguments);
        item = setItem;

        /**
         * Get the textual representation of the activity stream item
         *    ACTOR -> VERB -> OBJECT -> TARGET
         */
        this.getText = function (hideUser) {
            var target = false; //getTarget();

            // TODO: initialize viewer and show
            return TAPi18n.__('activity_stream_' + item.verb + (target ? '_target': ''), {actor: hideUser ? '' : getActor(), object: getObjectDescription(), target: target});
        };

        this.getDetails = function() {
            let changes = [];
            _.each(item.objectOld, (value, key) => {
                if (_.isArray(value)) {
                    changes.push({
                        label: key,
                        oldValue: (value || []).join(', '),
                        newValue: (item.object[key] || []).join(', ')
                    });
                } else if (_.isObject(value)) {
                    _.each(value, (value2, key2) => {
                        // truthy check ...
                        if (value2 || item.object[key][key2]) {
                            changes.push({
                                label: key + '.' + key2,
                                oldValue: value2,
                                newValue: item.object[key][key2]
                            });
                        }
                    });
                } else {
                    // truthy check ...
                    if (value || item.object[key]) {
                        changes.push({
                            label: key,
                            oldValue: value,
                            newValue: item.object[key]
                        });
                    }
                }
            });

            return changes;
        };

        /**
         * Get the description text of the actor
         *
         * @returns string
         */
        var getActor = function () {
            var actor = item.actor.split(':');

            return actor[2] || actor[1] || "vatfree.com";
        };

        /**
         * Get the description of the object being acted upon
         *
         * @returns {string}
         */
        var getObjectDescription = function () {
            var objectDescription = '';

            if (_.isString(item.object)) {
                var object = item.object.split(':');
                if (object[0].indexOf('_') === 0) {
                    // selector object
                    switch (object[0]) {
                        case '_attr':
                        default:
                            var translationKey = 'activity_stream_' + object[0] + '_' + object[1];
                            var translation = TAPi18n.__(translationKey);
                            // return the value itself if the text was not translated
                            objectDescription = (translation === translationKey ? object[1] : translation);
                            break;

                    }
                } else {
                    // database object
                    // we do not support linking yet
                    objectDescription = object[2];
                }
            } else if (_.isObject(item.object)) {
                if (item.target.target === 'files' && item.object.original && item.object.original.name) {
                    objectDescription = item.object.original.name;
                } else if (item.target.target === 'receipts') {
                    objectDescription = TAPi18n.__("receipt __receipt_number__", {receipt_number: item.object.receiptNr || ""});
                } else if (item.target.target === 'invoices') {
                    objectDescription = TAPi18n.__("invoice __invoice_number__", {invoice_number: item.object.invoiceNr || ""});
                } else if (item.target.target === 'users') {
                    objectDescription = TAPi18n.__("traveller __user_name__", {user_name: item.target.name || ""});
                } else if (item.target.target === 'activity-logs') {
                    if (item.object.type === "email") {
                        objectDescription = "Email: " + item.object.subject;
                    }
                } else {
                    objectDescription = item.target.name || TAPi18n.__("an item");
                }
            }

            return s(objectDescription).stripTags().value();
        };

        /**
         * Get the description text of the target object
         *
         * @returns string
         */
        var getTarget = function () {
            var targetDescription = '';

            if (_.isString(item.target)) {
                var object = item.object.split(':');
                var target = (item.target || '').split(':');
                if (object[0].indexOf('_') === 0) {
                    // selector object
                    switch (object[0]) {
                        case '_attr':
                        default:
                            var translationKey = 'activity_stream_' + object[1] + '_' + object[2];
                            var translation = TAPi18n.__(translationKey);
                            // return the value itself if the text was not translated
                            targetDescription = (translation === translationKey ? object[2] : translation);
                            break;

                    }
                } else if (target) {
                    // database object
                    // we do not support linking yet
                    targetDescription = target[2];
                }
            } else if (_.isObject(item.target)) {
                targetDescription = item.target.target;
            }

            return targetDescription;
        };

        // local variables
        var self = this,
            item;
    }
}());

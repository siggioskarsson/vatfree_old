import moment from 'moment';

Vatfree.exchangeRates = {};

Vatfree.exchangeRates.getRateById = function(date, currencyId) {
    if (currencyId === Meteor.settings.defaults.currencyId) {
        return 1;
    }

    const currency = Currencies.findOne({_id: currencyId});
    if (currency) {
        return Vatfree.exchangeRates.getRateByCode(date, currency.code);
    }

    return false;
};
Vatfree.exchangeRates.getRateByCode = function(date, currencyCode) {
    if (currencyCode === 'EUR') {
        return 1;
    }

    const rateDate = moment(new Date(date)).format('YYYY-MM-DD');
    const rateObj = ExchangeRates.findOne({ date:  rateDate});
    if (rateObj && rateObj.rates) {
        return rateObj.rates[currencyCode];
    } else if (Meteor.isServer) {
        const rateRequest = HTTP.get(`https://api.exchangeratesapi.io/${rateDate}`);
        if (rateRequest && rateRequest.data && rateRequest.data.rates) {
            return rateRequest.data.rates[currencyCode];
        }

    }

    return false;
};

Vatfree.exchangeRates.getEuroValueById = function(date, value, currencyId) {
    if (currencyId === Meteor.settings.defaults.currencyId) {
        return value;
    }

    const rate = Vatfree.exchangeRates.getRateById(date, currencyId);
    if (rate && value) {
        return value / rate;
    }

    return false;
};

Vatfree.exchangeRates.getEuroValueByCode = function(date, value, currencyCode) {
    if (currencyCode === "EUR") {
        return value;
    }

    const rate = Vatfree.exchangeRates.getRateByCode(date, currencyCode);
    if (rate && value) {
        return value / rate;
    }

    return false;
};

Vatfree.exchangeRates.getEuroValuesById = function(date, values, currencyId) {
    if (currencyId === Meteor.settings.defaults.currencyId) {
        return values;
    }

    const rate = Vatfree.exchangeRates.getRateById(date, currencyId);
    if (rate) {
        const exchangedValues = {};
        _.each(values, (value, key) => {
            if (value) exchangedValues[key] = value / rate;
        });
        return exchangedValues;
    }

    return false;
};

Vatfree.exchangeRates.getEuroValuesByCode = function(date, values, currencyCode) {
    if (currencyCode === "EUR") {
        return values;
    }

    const rate = Vatfree.exchangeRates.getRateByCode(date, currencyCode);
    if (rate) {
        const exchangedValues = {};
        _.each(values, (value, key) => {
            if (value) exchangedValues[key] = value / rate;
        });
        return exchangedValues;
    }

    return false;
};

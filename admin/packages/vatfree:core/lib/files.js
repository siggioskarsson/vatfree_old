Vatfree.files = {};
Vatfree.files.setTravellerSubType = function (travellerId, fileId, subType) {
    Files.find({
        itemId: travellerId,
        target: /^travellers/,
        subType: subType
    }).forEach((file) => {
        Files.update({
            _id: file._id
        }, {
            $unset: {
                subType: 1
            }
        });
    });

    Files.update({
        _id: fileId
    }, {
        $set: {
            subType: subType,
            target: 'travellers'
        }
    });
};

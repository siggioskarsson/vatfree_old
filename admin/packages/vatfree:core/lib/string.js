String.prototype.splitRemainder = function(separator, limit) {
    let str = this.split(separator);
    if(str.length > limit - 1) {
        let ret = str.splice(0, limit - 1);
        ret.push(str.join(separator));
        return ret;
    }
    return str;
};

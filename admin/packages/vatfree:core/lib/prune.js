export const pruneEmpty = function (obj) {
    var rObj = {};
    _.each(obj, (v, k) => {
        if (v) {
            rObj[k] = v;
        }
    });
    return rObj;
};

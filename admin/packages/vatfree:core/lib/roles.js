Vatfree.userIsInRole = function(userId, roles, group = null) {
    if (!_.isArray(roles)) roles = roles.split(',');

    roles = expandRoles(roles);
    if (_.isString(group)) {
        return Roles.userIsInRole(userId, roles, group);
    }

    return Roles.userIsInRole(userId, "admin", Roles.GLOBAL_GROUP) ||
        (
            Roles.userIsInRole(userId, "vatfree", Roles.GLOBAL_GROUP) &&
            Roles.userIsInRole(userId, roles, 'vatfree-backend')
        );
};

/*
    Expand the roles list to include other roles that have that right
 */
var expandRoles = function(roles) {
    let expandActions = {
        create: ["create", "admin"],
        read: ["create", "read", "update", "delete", "tasks", "admin"], // if any rights, all roles can read
        update: ["update", "admin"],
        delete: ["delete", "admin"],
        tasks: ["tasks", "admin"],
        admin: ["admin"]
    };

    let newRoles = [];
    _.each(roles, (role) => {
        let roleSplit = role.split('-');
        if (roleSplit.length === 2) {
            let roleGroup = roleSplit[0];
            let roleAction = roleSplit[1];
            if (_.has(expandActions, roleAction)) {
                _.each(expandActions[roleAction], (action) => {
                    newRoles.push(roleGroup + '-' + action);
                });
            } else {
                newRoles.push(role);
            }
        } else {
            newRoles.push(role);
        }
    });

    return _.uniq(newRoles);
};

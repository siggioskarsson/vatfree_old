import moment from 'moment';

Vatfree.dates = {
    formatDate: function (date, format, timezone) {
        format = _.isString(format) ? format : TAPi18n.__('_date_format');
        if (!date) {
            date = this.toString();
        }

        let mDate = moment(new Date(date));
        if (_.isString(timezone)) {
            import momentTZ from 'moment-timezone';
            mDate = momentTZ.tz(new Date(date), timezone);
        }
        return mDate.isValid() ? mDate.format(format) : '';
    },
    formatDateShort: function (date, format, timezone) {
        format = _.isString(format) ? format : TAPi18n.__('_date_format_short');
        if (!date) {
            date = this.toString();
        }
        let mDate = moment(new Date(date));
        if (_.isString(timezone)) {
            import momentTZ from 'moment-timezone';
            mDate = momentTZ.tz(new Date(date), timezone);
        }
        return mDate.isValid() ? mDate.format(format) : '';
    },
    formatDateTime: function (date, format, timezone) {
        format = _.isString(format) ? format : TAPi18n.__('_date_time_format');
        if (!date) {
            date = this.toString();
        }
        let mDate = moment(new Date(date));
        if (_.isString(timezone)) {
            import momentTZ from 'moment-timezone';
            mDate = momentTZ.tz(new Date(date), timezone);
        }
        return mDate.isValid() ? mDate.format(format) : '';
    }
};

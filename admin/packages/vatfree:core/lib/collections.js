export const getCollectionDescription = function(Collection, id, cacheObject, includeId) {
    if (!_.has(cacheObject, id)) {
        let item = getCollectionItem(Collection, id);

        if (item) {
            if (Collection._name === 'users') {
                cacheObject[id] = (item.profile.name || item.profile.email) + (includeId ? ' (' + item._id + ')' : '');
            } else if (Collection._name === 'invoices') {
                cacheObject[id] = item.invoiceNr + (includeId ? ' (' + item._id + ')' : '');
            } else if (Collection._name === 'receipts') {
                cacheObject[id] = item.receiptNr + (includeId ? ' (' + item._id + ')' : '');
            } else if (Collection._name === 'payments') {
                cacheObject[id] = item.paymentNr + (includeId ? ' (' + item._id + ')' : '');
            } else {
                cacheObject[id] = item.name + (includeId ? ' (' + item._id + ')' : '');
            }
        }
    }

    return cacheObject[id] || "";
};

export const getCollectionItem = function (Collection, id, cacheObject) {
    if (cacheObject && _.has(cacheObject, id)) {
        return cacheObject[id];
    }

    let item = Collection.findOne({
        _id: id
    });

    if (item && cacheObject) {
        cacheObject[id] = item;
    }

    return item;
};

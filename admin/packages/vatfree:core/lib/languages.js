Vatfree.languages = {};

Vatfree.languages.traveller = [
    {
        code: 'en',
        en: 'English',
        name: 'English'
    },
    {
        code: 'nl',
        en: 'Dutch',
        name: 'Nederlands'
    },
    {
        code: 'zh-CN',
        en: 'Chinese',
        name: '中文'
    }
];

Vatfree.languages.getTravellerLanguages = function() {
    return Vatfree.languages.traveller;
};

Vatfree.languages.shop = [
    {
        code: 'nl',
        en: 'Dutch',
        name: 'Nederlands'
    },
    {
        code: 'en',
        en: 'English',
        name: 'English'
    }
    ,
    {
        code: 'fr',
        en: 'French',
        name: 'Français'
    },
    {
        code: 'de',
        en: 'German',
        name: 'Deutsch'
    },
    {
        code: 'es',
        en: 'Spanish',
        name: 'Español'
    }
];

Vatfree.languages.getShopLanguages = function() {
    return Vatfree.languages.shop;
};


Vatfree.languages.getValidTravellerLanguage = function(language) {
    language = (language || "en").substr(0, 2);
    if (language === 'zh') language = 'zh-CN';

    if (_.find(Vatfree.languages.getTravellerLanguages(), (languageDoc) => { return languageDoc.code === language; })) {
        return language;
    }

    return 'en'
};

Vatfree.setLanguage = function(language) {
    return TAPi18n.setLanguage(language);
};

Vatfree.setTravellerLanguage = function(language) {
    language = language.substr(0, 2);
    if (language === 'zh') language = 'zh-CN';

    if (_.find(Vatfree.languages.getTravellerLanguages(), (languageDoc) => { return languageDoc.code === language; })) {
        return Vatfree.setLanguage(language);
    }
};

Vatfree.setShopLanguage = function(language) {
    if (_.find(Vatfree.languages.getShopLanguages(), (languageDoc) => { return languageDoc.code === language; })) {
        return Vatfree.setLanguage(language);
    }
};

Vatfree.getLanguage = function() {
    return TAPi18n.getLanguage() || 'en';
};

Vatfree.translate = function(key, options) {
    options = options || {};
    options.defaultValue = key;
    return TAPi18n.__(key, options);
};

const uiTranslateHelper = function(key, ...args) {
    let options = (args.pop() || {}).hash;
    if (!_.isEmpty(args)) {
        options.sprintf = args;
    }

    return TAPi18n.__(key, options);
};
if (Meteor.isClient && !_.isUndefined(UI)) {
    UI.registerHelper('t_', uiTranslateHelper);
}

Number.prototype.padLeft = function (n, str) {
    return Array(n - String(this).length + 1).join(str || '0') + this;
};

import accounting from 'accounting';

Vatfree.numbers = {
    formatCurrency(value, decimals, currencySymbol) {
        if (!currencySymbol) currencySymbol = "€";
        if (!_.isNumber(decimals)) decimals = 2;
        // all currency values are *100 to avoid decimals in database
        value = Number(value) / 100;
        if (value < 0.01 && value > 0) {
            // http://stackoverflow.com/a/31002148
            let numberOfZerosAfterDecimal = (-Math.floor( Math.log(value) / Math.log(10) + 1));
            return accounting.formatMoney(value, currencySymbol + " ", numberOfZerosAfterDecimal + 1, TAPi18n.__('_thousand_separator'), TAPi18n.__('_decimal_separator'));
        }
        return accounting.formatMoney(value, currencySymbol + " ", decimals, TAPi18n.__('_thousand_separator'), TAPi18n.__('_decimal_separator'));
    },
    // all numbers
    formatAmount(value, decimals) {
        if (!_.isUndefined(value) && value.toString().length) {
            if (_.isNumber(decimals)) {
                return (Number(value) / 100).toFixed(decimals);
            } else {
                return Number(value) / 100;
            }
        }
    },
    unformatAmount(value) {
        return accounting.unformat(value, TAPi18n.__('_decimal_separator'));
    },
    unformatCurrency(value) {
        return accounting.unformat(value, TAPi18n.__('_decimal_separator'));
    }
};

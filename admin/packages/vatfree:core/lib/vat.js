/* global Vatfree: true */

Vatfree.vat = {};

Vatfree.vat.calculateRefund = function(vatAmount, serviceFee, minFee, maxFee) {
    let fee = Vatfree.vat.calculateFee(vatAmount, serviceFee, minFee, maxFee);

    return vatAmount - fee;
};

Vatfree.vat.calculateFee = function(vatAmount, serviceFee, minFee, maxFee) {
    let fee = Math.round(vatAmount * (serviceFee / 100));
    if (fee < minFee) {
        fee = minFee;
    } else if (fee > maxFee) {
        fee = maxFee;
    }

    return fee;
};

Vatfree.vat.calculateVat = function(amount, vat) {
    vat = vat || 21;
    return Math.round((vat * amount) / (100 + vat));
};

Vatfree.vat.minimumTotalAmount = 50;

/**
 * This function returns the default vat rate for the country (as string !)
 * @param vatRates
 * @returns string
 */
Vatfree.vat.getDefaultRate = function(vatRates) {
    let defaultRate = false;
    let highestRate = 0;
    _.each(vatRates, (vatRate) => {
        if (vatRate.default === true) {
            defaultRate = vatRate.rate;
        }
        if (!highestRate || Number(highestRate) < Number(vatRate.rate)) {
            highestRate = vatRate.rate;
        }
    });
    if (!defaultRate) {
        defaultRate = highestRate;
    }

    return defaultRate;
};

Vatfree.payments = {};

Vatfree.payments.methods = [
    'creditcard',
    'paypal',
    'sepa',
    'wechat',
    'alipay'
];

Vatfree.payments.parseMt940Description = function (mt940description) {
    let data = mt940description.split('/');
    data.shift();

    let parsedData = {};
    for (var i = 0; i <= data.length; i += 2) {
        parsedData[data[i]] = data[i + 1];
    }
    return parsedData;
};

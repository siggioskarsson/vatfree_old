Lock = class Lock {
    constructor(id, userId) {
        this.id = id;
        this.userId = userId;
        this.dict = new ReactiveDict('lock_' + id + '_' + Random.id());
        this.debug = false;

        let lockInstance = this;
        this.getLock((err, lockId) => {
            // wait for lock release by other user, if applicable
            if (!lockId) {
                lockInstance.observer = Locks.find({lockId: lockInstance.id}).observeChanges({
                    removed: function (id) {
                        if (id === lockInstance.id) {
                            lockInstance.getLock();
                        }
                    }
                });
            }

            // re-affirm the lock every 60 seconds
            this.dict.set('intervalId', Meteor.setInterval(() => {
                if (this.debug) console.log('reaffirm lock interval', this.dict.get('released'), this);
                if (this.dict.get('released') === true) {
                    Meteor.clearInterval(this.dict.get('intervalId'));
                }
                if (this.dict.get('lockId')) {
                    this.reAffirm();
                } else {
                    this.getLock();
                }
            }, 60*1000));
        });
    }

    getLockId() {
        return this.lockId;
    }

    isLocked() {
        let currentLock = Locks.findOne({
            lockId: this.id
        });

        return currentLock && this.dict.set('released') !== true;
    }

    /**
     * Check whether the lock is Mine
     *
     * UserId is passed to this function to be able to use it on the server
     *
     * @param userId
     * @returns {boolean}
     */
    isMine(userId) {
        let currentLock = Locks.findOne({
            lockId: this.id
        });

        return !!this.dict.get('lockId') && currentLock && currentLock.userId === userId;
    }

    getUserName() {
        let user = Meteor.users.findOne({_id: this.userId});
        if (user && user.profile) {
            return user.profile.name;
        }
    }

    getCurrentLockUserName() {
        let currentLock = Locks.findOne({
            lockId: this.id
        });

        if (currentLock) {
            let user = Meteor.users.findOne({_id: currentLock.userId});
            if (user && user.profile) {
                return user.profile.name;
            } else {
                return currentLock.userId;
            }
        } else {
            return "Unknown";
        }
    }

    getLock(callback) {
        let lockInstance = this;
        try {
            // try to get lock
            let currentLock = Locks.findOne({
                lockId: this.id
            });
            if (currentLock) {
                if (currentLock.userId === this.userId) {
                    this.dict.set('lockId', currentLock._id);
                    this.reAffirm();
                } else {
                    if (callback) callback("locked");
                }
            } else {
                Locks.insert({
                    lockId: this.id,
                    updatedAt: new Date(),
                    userId: this.userId
                }, (err, lockId) => {
                    if (!err) {
                        lockInstance.dict.set('lockId', lockId);
                    }
                    if (callback) callback(err, lockId);
                });
            }
        } catch(e) {
            // nothing
        }
    }

    reAffirm() {
        Locks.update({
            _id: this.dict.get('lockId')
        },{
            $set: {
                updatedAt: new Date()
            }
        });
    }

    release() {
        if (this.observer) this.observer.stop();
        Locks.remove({
            _id: this.dict.get('lockId')
        });
        Meteor.clearInterval(this.dict.get('intervalId'));
        this.dict.set('released', true);
    }
};

Vatfree.billing = {
    getBillingEntityForCompany: function(companyId) {
        let company = Companies.findOne({_id: companyId});
        if (company) {
            if (company.billingEntity) {
                return company;
            } else {
                return Companies.findOne({
                    _id: {
                        $in: company.ancestors || []
                    },
                    billingEntity: true
                },{
                    sort: {
                        ancestorsLength: -1
                    }
                });
            }
        }
    }
};

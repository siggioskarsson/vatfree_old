Accounts.passwordless = {};
Accounts.passwordLessCodes = new Meteor.Collection('meteor_accounts_passwordless');

if (Meteor.isServer) {
    try {
        Accounts.passwordLessCodes._ensureIndex({email: 1}, {unique: true});
    } catch (e) {
        console.error(e);
    }
}

Accounts.passwordless.emailTemplates = {
    from: 'Meteor Accounts <no-reply@meteor.com>',
    siteName: Meteor.absoluteUrl().replace(/^https?:\/\//, '').replace(/\/$/, ''),

    sendVerificationCode: {
        subject: function (code) {
            return 'Your verification code is ' + code + ' for ' + Accounts.passwordless.emailTemplates.siteName;
        },
        text: function (user, code) {
            var greeting = (user && user.username) ?
                ('Hello ' + user.username + ',') : 'Hello,';
            return greeting + '\n'
                + '\n'
                + 'Your verification code is ' + code + '.\n'
                + '\n'
                + 'Thanks.\n';
        }
    }
};

Meteor.methods({
    'accounts-passwordless.sendVerificationCode': function (selector, options) {
        check(selector, String);
        check(options, Match.Optional(Match.Any));
        Accounts.passwordless.sendVerificationCode(selector, options);
    },
    'accounts-passwordless.setUsername': function (username) {
        check(username, String);
        if (!this.userId) throw new Meteor.Error('You must be logged to change your username');
        if (username.length < 3) throw new Meteor.Error('Your username must have at least 3 characters');
        var existingUser = Meteor.users.findOne({username: username});
        if (existingUser) throw new Meteor.Error('This username already exists');
        Meteor.users.update(this.userId, {$set: {username: username}});
    }
});

// Handler to login with passwordless
Accounts.registerLoginHandler('passwordless', function (options) {
    if (options.code === undefined) return undefined; // don't handle

    check(options, {
        selector: String,
        code: String
    });

    if (!options.selector) throw new Meteor.Error('No selector setuped');

    return Accounts.passwordless.verifyCode(options.selector, options.code);
});

Accounts.passwordless.getOrCreateVerificationCode = function (emailAddress) {
    const passwordLess = Accounts.passwordLessCodes.findOne({email: emailAddress});
    if (!passwordLess) {
        try {
            return Accounts.passwordless.createVerificationCode(emailAddress);
        } catch (e) {
            console.error(e);
            const passwordLess = Accounts.passwordLessCodes.findOne({email: emailAddress});
            return passwordLess.code;
        }
    }

    return passwordLess.code;
};

Accounts.passwordless.createVerificationCode = function (emailAddress) {
    const user = Meteor.users.findOne({'emails.address': emailAddress});
    // If the user doesn't exists, we'll create it when the user will verify his email
    const email = emailAddress;

    /*
    var code = Math.floor(Random.fraction() * 10000) + '';
    // force pin to 4 digits
    code = ('0000' + code).slice(-4);
    */
    // We use a more difficult code, since we are using magic links and not typed codes
    const code = Random.secret();

    // Generate a new code
    Accounts.passwordLessCodes.insert({email: email, code: code});

    return {email: email, user: user, code: code};
};

/**
 * Send a 4 digit verification code by email
 * @param selector The email or username of the user
 */
Accounts.passwordless.sendVerificationCode = function (selector, options) {
    var email;
    var user;
    var __ret = this.createVerificationCode(selector);
    user = __ret.user;
    email = __ret.email;
    var code = __ret.code;

    Email.send({
        to: email,
        from: Accounts.passwordless.emailTemplates.from,
        subject: Accounts.passwordless.emailTemplates.sendVerificationCode.subject(code),
        text: Accounts.passwordless.emailTemplates.sendVerificationCode.text(user, code, selector, options)
    });
};

// from accounts-password code
var createUser = function (options) {
    // Unknown keys allowed, because a onCreateUserHook can take arbitrary
    // options.
    check(options, Match.ObjectIncluding({
        username: Match.Optional(String),
        email: Match.Optional(String),
    }));

    var username = options.username;
    var email = options.email;
    if (!username && !email) {
        throw new Meteor.Error(400, 'Need to set a username or email');
    }

    var user = {services: {}};
    if (options.password) {
        var hashed = hashPassword(options.password);
        user.services.password = {bcrypt: hashed};
    }

    if (username)
        user.username = username;
    if (email)
        user.emails = [{address: email, verified: false}];

    return Accounts.insertUserDoc(options, user);
};

/**
 * Verify if the code is valid
 * @param selector The email or username of the user
 * @param code The code the user entered
 */
Accounts.passwordless.verifyCode = function (selector, code) {
    var user = Meteor.users.findOne({'emails.address': selector});
    var email = selector;

    var validCode = Accounts.passwordLessCodes.findOne({email: email});
    if (!validCode) {
        throw new Meteor.Error('Unknown login code');
    }

    var now = new Date().getTime() / 1000;
    var timeToWait;

    if (validCode.lastTry) {
        timeToWait = validCode.lastTry.getTime() / 1000 + Math.pow(validCode.nbTry, 2);

        if (timeToWait > now)
            throw new Meteor.Error('You must wait ' + Math.ceil(timeToWait - now) + ' seconds');
    }

    if (validCode.code !== code) {
        Accounts.passwordLessCodes.update({email: email}, {$set: {lastTry: new Date()}, $inc: {nbTry: 1}});
        throw new Meteor.Error('Invalid verification code');
    }
    // Clear the verification code after a succesfull login.
    Accounts.passwordLessCodes.remove({email: email});

    var uid;
    if (user) {
        uid = user._id;
    } else {
        uid = createUser({email: email});
        user = Meteor.users.findOne(uid);
        console.log('created user', uid, user);
    }

    if (user) {
        // Set the email as verified since he validated the code with this email
        var ve = _.find(user.emails, function (e) { return e.address === email; });
        if (ve && !ve.verified) {
            // By including the address in the query, we can use 'emails.$' in the
            // modifier to get a reference to the specific object in the emails
            // array. See
            // http://www.mongodb.org/display/DOCS/Updating/#Updating-The%24positionaloperator)
            // http://www.mongodb.org/display/DOCS/Updating#Updating-%24pull
            Meteor.users.update({_id: uid, 'emails.address': email}, {$set: {'emails.$.verified': true}});
        }
    }
    return {userId: uid};
};

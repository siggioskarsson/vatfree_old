import moment from 'moment/moment';

var _importFileLines = function(filename, callback) {
    var exec = require('child_process').exec;
    exec('wc -l < ' + filename.replace(/ /g, '\\ '), Meteor.bindEnvironment((error, totalLines) => {
        totalLines = Number(totalLines);
        callback(error, totalLines);
    }));
};
Vatfree.importFileLines = Meteor.wrapAsync(_importFileLines);


Vatfree.importFile = function (fileId, importFunction, callback) {
    let file = Files.findOne({
        _id: fileId
    });
    if (file) {
        // get the file contents and save in tmp dir
        let url = file.getDirectUrl();
        let fileName = '/tmp/' + file.original.name;

        let https = require('https');
        let fs = require('fs');

        Files.update({
            _id: file._id
        }, {
            $set: {
                'importInfo.running': true,
                'importInfo.progress': 0,
                'importInfo.progressMessage': 'Getting csv from S3'

            },
            $unset: {
                'importInfo.error': true
            }
        });

        let fileStream = fs.createWriteStream(fileName);
        let request = https.get(url, Meteor.bindEnvironment(function (response) {
            // check if response is success
            if (response.statusCode !== 200) {
                Files.update({
                    _id: file._id
                }, {
                    $set: {
                        'importInfo.running': false,
                        'importInfo.progress': 0,
                        'importInfo.progressMessage': '',
                        'importInfo.error': response.statusCode
                    }
                });
            }

            response.pipe(fileStream);

            fileStream.on('finish', Meteor.bindEnvironment(function () {
                fileStream.close(Meteor.bindEnvironment(function () {
                    importFunction(fileName, file, function (error) {
                        fs.unlink(fileName, function(){});
                        Files.update({
                            _id: file._id
                        }, {
                            $set: {
                                'importInfo.running': false,
                                'importInfo.progress': 100,
                                'importInfo.progressMessage': 'Done'
                            }
                        });
                        if (callback) {
                            callback(null, 100);
                        }
                    });
                }));
            }));
        }));

        request.on('error', Meteor.bindEnvironment(function (err) {
            Files.update({
                _id: file._id
            }, {
                $set: {
                    'importInfo.running': false,
                    'importInfo.progress': 0,
                    'importInfo.progressMessage': '',
                    'importInfo.error': err
                }
            });
            if (callback) {
                callback(err);
            }
        }));

        fileStream.on('error', Meteor.bindEnvironment(function (err) {
            fs.unlink(fileName, function(){});
            Files.update({
                _id: file._id
            }, {
                $set: {
                    'importInfo.running': false,
                    'importInfo.progress': 0,
                    'importInfo.progressMessage': '',
                    'importInfo.error': err
                }
            });
            if (callback) {
                callback(err);
            }
        }));
    } else {
        Files.update({
            _id: file._id
        }, {
            $set: {
                'importInfo.running': false,
                'importInfo.progress': 0,
                'importInfo.progressMessage': '',
                'importInfo.error': 'Could not find file'
            }
        });
        if (callback) {
            callback("Could not find file");
        }
    }
};

var _csv2json = function(fileName, callback) {
    let csvData = [];

    try {
        const csv = require('csvtojson');
        csv({
            delimiter: ';',
            noheader: false,
            trim: true,
            flatKeys: true,
            constructResult: false
        }).fromFile(fileName) //'/Users/oskarsson/Downloads/travellers.csv')
            .on('json', function(row, rowIndex) {
                csvData.push(row);
            })
            .on('error', function(error) {
                callback(error);
            })
            .on('done', function(error) {
                callback(error, csvData);
            });
    } catch(e) {
        callback(e);
    }
};
Vatfree.csv2json = Meteor.wrapAsync(_csv2json);

Vatfree.setImportFileProgress = function(progress, progressMessage, fileObject) {
    if (fileObject) {
        Files.update({
            _id: fileObject._id
        },{
            $set: {
                'importInfo.running': progress !== 100,
                'importInfo.progress': progress,
                'importInfo.progressMessage': progressMessage
            }
        });
    }
};

Vatfree.import = {};
Vatfree.import.processColumns = function (colMapping, convert, row, countries) {
    let updateData = {};
    _.each(_.keys(colMapping), (colName) => {
        let newColName = colMapping[colName];
        if (newColName) {
            switch (convert[newColName]) {
                case "date":
                    if (row[colName]) {
                        if (row[colName].length > 10) {
                            updateData[newColName] = row[colName] ? moment(row[colName]).toDate() : null;
                        } else if (row[colName].match("-[0-9]{4}$")) {
                            updateData[newColName] = row[colName] ? moment(row[colName], "DD-MM-YYYY").toDate() : null;
                        } else if (row[colName].match("^[0-9]{4}-")) {
                            updateData[newColName] = row[colName] ? moment(row[colName], "YYYY-MM-DD").toDate() : null;
                        } else {
                            updateData[newColName] = row[colName] ? moment(row[colName], "DD/MM/YYYY").toDate() : null;
                        }
                    } else {
                        updateData[newColName] = null;
                    }
                    break;
                case "bool":
                    updateData[newColName] = row[colName] == "1";
                    break;
                case "number":
                    row[colName] = row[colName] ? row[colName].replace(',', '.') : 0;
                    updateData[newColName] = Number(row[colName]);
                    break;
                case "currency":
                    row[colName] = row[colName] ? row[colName].replace(',', '.') : 0;
                    updateData[newColName] = Math.round(Number(row[colName]) * 100);
                    break;
                case "country":
                    if (countries) {
                        let country = updateData[colName] || "NL";
                        let userCountry = countries[country.substr(0, 2)];
                        if (!userCountry) {
                            userCountry = countries['NL'];
                        }
                        updateData[newColName] = userCountry._id;
                    } else {
                        updateData[newColName] = Meteor.settings.defaults.countryId;
                    }
                    break;
                case "lowerCase":
                    updateData[newColName] = (row[colName] || "").toLowerCase();
                    break;
                case "geo":
                    if (row[colName]) {
                        let geo = row[colName].split(',');
                        if (geo && geo[0] && geo[1]) {
                            updateData[newColName] = {
                                type: "Point",
                                coordinates: [
                                    Number(geo[1]),
                                    Number(geo[0])
                                ],
                            };
                        } else {
                            delete updateData[newColName];
                        }
                    }
                    break;
                default:
                    updateData[newColName] = row[colName] || "";
                    break;
            }
        }
    });

    return updateData;
};

Vatfree.defaultPhantomJSOptions = {
    'format': 'A4',
    'orientation': 'portrait',
    'header': {
        'height': '25mm'
    },
    'footer': {
        'height': '35mm'
    },
    'phantomArgs': [
        '--ignore-ssl-errors=yes'
    ],
    'quality': '75',
    'type': 'pdf',
    'timeout': 30000
};

Vatfree.creatPdfAsync = function (renderedHtml, newFile, options, callback) {
    import pdf from 'html-pdf';
    pdf.create(renderedHtml, options).toBuffer(Meteor.bindEnvironment((err, buffer) => {
        if (err) {
            console.log('PDF Failed', err);
            callback(err);
            return;
        }

        newFile.createdAt = new Date();
        newFile.attachData(buffer, {
            type: 'application/pdf'
        }, function (error) {
            if (error) {
                callback(error);
            } else {
                Files.insert(newFile, function(err, fileObj) {
                    callback(err, fileObj._id);
                });
            }
        });
    }));
};
Vatfree.createPdf = Meteor.wrapAsync(Vatfree.creatPdfAsync);

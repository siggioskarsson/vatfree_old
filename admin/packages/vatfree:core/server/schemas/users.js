try {
    Meteor.users._ensureIndex({"textSearch": "text"}, {"language_override": "_text_language"});
    Meteor.users._ensureIndex({"private.status" : 1});
    Meteor.users._ensureIndex({"profile.affiliateId" : 1});
    Meteor.users._ensureIndex({"createdAt" : -1});
} catch (e) {
    console.error(e);
}

Meteor.users.after.insert(function (userId, doc) {
    Meteor.users.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: Meteor.users.getTextSearch(doc)
        }
    });
});

Meteor.users.after.update(function (userId, doc, fieldNames, modifier, options) {
    Meteor.users.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: Meteor.users.getTextSearch(doc)
        }
    });
});


Meteor.users.getTextSearch = function(doc) {
    const extraEmails = [];
    if (doc.emails && doc.emails.length > 1) {
        _.each(doc.emails, (email) => {
            if (email.address !== doc.profile.email) {
                extraEmails.push(email.address);
            }
        });
    }
    let textSearch = (doc.profile.contactId || "") + ' ' +
        (doc.profile.name || "") + ' ' +
        (doc.profile.email || "") + ' ' +
        (doc.profile.passportNumber || "") + ' ' +
        (doc.profile.visaNumber || "") + ' ' +
        (doc.profile.phone || "") + ' ' +
        (doc.profile.phone2 || "") + ' ' +
        (doc.profile.mobile || "") + ' ' +
        (doc.profile.country || "") + ' ' +
        (doc.profile.city || "") + ' ' +
        (doc.profile.addressFirst || "") + ' ' +
        (doc.profile.state || "") + ' ' +
        (doc.private.notes || "") +
        (extraEmails.length ? ' ' + extraEmails.join(' ') : '')
    ;

    if (doc.affiliate) {
        if (doc.affiliate.code) textSearch+= doc.affiliate.code;
    }

    return textSearch.toLowerCase().latinize();
};

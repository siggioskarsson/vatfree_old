import moment from 'moment';

Locks._ensureIndex({"lockId": 1}, {"unique": 1});
Locks._ensureIndex({"updatedAt": -1});

Meteor.setInterval(() => {
    try {
        Locks.remove({
            updatedAt: {
                $lte: moment().subtract(2, 'minutes').toDate()
            }
        });
    } catch (e) {
        console.error(e);
    }
}, 5000);

import sharp from 'sharp';

let cfsOptions = {
	ACL: 'authenticated-read',
	ServerSideEncryption: 'AES256',
	maxTries: 3,
    region: Meteor.settings.AWS.region,
    bucket: Meteor.settings.AWS.bucket || 'nullbucket',
    accessKeyId: Meteor.settings.AWS.accessKeyId,
    secretAccessKey: Meteor.settings.AWS.secretAccessKey,
    serverNode: ((Meteor.settings.nodeId ? Meteor.settings.nodeId : "") + (process.env.DYNO ? '_' + process.env.DYNO : "")).replace(/[\.-]/g, '_')
};
let cfsThumbOptions = _.clone(cfsOptions);
let cfsOriginalOptions = _.clone(cfsOptions);

cfsOptions.fileKeyMaker = function (fileObj) {
    // Lookup the copy
    let store = fileObj && fileObj._getInfo('vatfree');
    // If the store and key is found return the key
    if (store && store.key) return store.key;

    let filename = fileObj.name();
    let filenameInStore = fileObj.name({store: 'vatfree'});

    return fileObj.collectionName + '/' + fileObj.target + '/' + fileObj._id + '-' + (filenameInStore || filename).replace(/[^a-z0-9-.]/gi, '_');
};

cfsThumbOptions.fileKeyMaker = function (fileObj) {
    // Lookup the copy
    let store = fileObj && fileObj._getInfo('vatfree-thumbs');
    // If the store and key is found return the key
    if (store && store.key) return store.key;

    let filename = fileObj.name();
    let filenameInStore = fileObj.name({store: 'vatfree-thumbs'});

    // If no store key found we resolve / generate a key
    return fileObj.collectionName + '/' + fileObj.target + '/thumbs/' + fileObj._id + '-' + (filenameInStore || filename).replace(/[^a-z0-9-.]/gi, '_') + '.png';
};
cfsThumbOptions.transformWrite = function (fileObj, readStream, writeStream) {
    try {
        if (fileObj.original.type.match('image/*')) {
            readStream
                .pipe(sharp().resize({ width: 192, height: 192, fit: "outside" }).jpeg())
                .on('error', function(e){
                    console.log(e);
                })
                .pipe(writeStream)
                .on('error', function(e){
                    console.log(e);
                })
            ;
        }
    } catch(e) {
        console.log(e);
        readStream.pipe(writeStream);
    }
    /*
    else if (fileObj.original.type.match('application/pdf')) {
        let meteorConsole = console;
        let image = gm(readStream);
        image.identify({bufferStream: true}, function (err, data) {
            meteorConsole.log('transformWrite thumbnail', fileObj._id, fileObj.name());
            if (err) {
                // do nothing for now, this is actually not fatal
                meteorConsole.error('transformWrite thumbnail error', err);
            } else {
                try {
                    image.selectFrame(0).resize(null, 192, '^').stream('JPG', function (err, stdout, stderr) {
                        if (err) {
                            meteorConsole.error('transformWrite thumbnail error', err);
                        } else {
                            stdout.pipe(writeStream);
                        }
                    });
                } catch(e) {
                    meteorConsole.error('transformWrite thumbnail error', e);
                }
            }
        });
    }
    */
};

cfsThumbOptions.filter ={
    allow: {
        contentTypes: ['image/*'] //allow only images in this FS.Collection
    }
};

cfsOriginalOptions.fileKeyMaker = function (fileObj) {
    // Lookup the copy
    let store = fileObj && fileObj._getInfo('vatfree-original');
    // If the store and key is found return the key
    if (store && store.key) return store.key;

    let filename = fileObj.name();
    let filenameInStore = fileObj.name({store: 'vatfree-original'});

    return fileObj.collectionName + '/' + fileObj.target + '/original/' + fileObj._id + '-' + (filenameInStore || filename).replace(/[^a-z0-9-.]/gi, '_') + '.png';
};

FS.TempStore.Storage = new FS.Store.FileSystem("_tempstore" + (cfsOptions.serverNode ? '_' + cfsOptions.serverNode : ''), {
    internal :  true,
    path : '/tmp',
});
Files = new FS.Collection("vatfree-files", {
    stores: [
        new FS.Store.S3("vatfree", cfsOptions),
        new FS.Store.S3("vatfree-thumbs", cfsThumbOptions)
    ],
    chunkSize: 1024*1024,
    serverNode: cfsOptions.serverNode
});
Files.files._ensureIndex({target: 1, itemId: 1, subType: 1});
Files.files._ensureIndex({'copies.vatfree.createdAt': 1});
Files.files._ensureIndex({createdAt: -1});
Files.files._ensureIndex({deleted: 1});
Files.files._ensureIndex({updatedAt: -1});
Files.files._ensureIndex({salesforceId: 1});
Files.files._ensureIndex({'textSearch': "text"}, {"language_override": "_text_language"});

if (Meteor.isServer) {
    // TODO: move this into the cfs library, based on settings
    Files.files.before.insert(function(userId, doc) {
        doc.node = ((Meteor.settings.nodeId ? Meteor.settings.nodeId : "") + (process.env.DYNO ? '_' + process.env.DYNO : "")).replace(/[\.-]/g, '_');
    });
}

Files.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP) || doc.uploadedBy === userId;
    },
    update: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP);
    },
    download:function(userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) ||
            Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP) ||
            doc.uploadedBy === userId ||
            (doc.target === "travellers" && doc.itemId === userId) ||
            (doc.target === "receipts" && (Receipts.findOne({_id: doc.itemId}, {fields: {userId: 1}}) || {}).userId === userId) ||
            (doc.target === "shops" && doc.category === 'logo') ||
            (doc.target === "companies" && doc.category === 'logo')
            ;
    }
});

Files.downloadAllowed = function (doc, userId) {
    if ((doc.receiptId || doc.itemId) && doc.target === 'receipts') {
        // allow user to see his/her own receipt files
        let receipt = Receipts.findOne({_id: doc.itemId}) || {};
        if (receipt.userId === userId) {
            return true;
        }
        if (Vatfree.userIsInRole(userId, 'receipts-read')) {
            return true;
        }
    }
    if ((doc.userId || doc.itemId) && doc.target === 'travellers') {
        // allow user to see his/her own traveller files
        if ((doc.userId || doc.itemId) === userId) {
            return true;
        }
        if (Vatfree.userIsInRole(userId, 'travellers-read')) {
            return true;
        }
    }
    if (!userId) return true; // TEMP TEMP TEMP !!! need to find a fix for the invoice download, without a login :-S
    return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || (doc.uploadedBy && doc.uploadedBy === userId);
};

Files.files.after.insert(function (userId, doc) {
    // update activity stream
    const fileDoc = _.clone(doc);
    Files.updateReceiptTextSearch(fileDoc);
    ActivityStream.add.upload(userId, fileDoc, 'files');

    /* Should receipts be pushed through the flow before originals are in?
    if (doc.target === 'receipts' && !doc.subType) {
        // check whether we are uploading a photo for a receipt and the shop is a partner
        // We can then push the receipts through the flow before the original is in
        let receipt = Receipts.findOne({_id: doc.itemId});
        if (receipt && _.contains(['userPending', 'waitingForOriginal'], receipt.status)) {
            let user = Meteor.users.findOne({_id: receipt.userId});
            let shop = Shops.findOne({_id: receipt.shopId});
            let partnerFlow = user && shop && shop.partnershipStatus === 'partner' && Vatfree.travellers.ncpFilledIn(user);
            if (partnerFlow) {
                Receipts.update({
                    _id: receipt._id
                }, {
                    $set: {
                        status: 'visualValidationPending'
                    }
                });
            }
        }
    }
    */
});

Files.files.after.update(function (userId, doc) {
    // update activity stream
    const fileDoc = _.clone(doc);
    Files.updateReceiptTextSearch(fileDoc);
    ActivityStream.add.update(userId, this.previous, fileDoc, 'files');
});

Files.files.after.remove(function (userId, doc) {
    // update activity stream
    const fileDoc = _.clone(doc);
    ActivityStream.add.remove(userId, fileDoc, 'files');
});


Files.updateReceiptTextSearch = function(doc) {
    let textSearch = doc.original.name;
    textSearch+= ' ' + (doc.original.type || "");
    textSearch+= ' ' + (doc.itemId || "");
    textSearch+= ' ' + (doc.target || "");
    textSearch+= ' ' + (doc.subType || "");
    textSearch+= ' ' + (doc.category || "");
    textSearch+= ' ' + (doc.node || "");

    Files.files.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch.toLowerCase().latinize()
        }
    });
};

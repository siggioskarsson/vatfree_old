importFiles = new JobCollection('import-files');
importFiles.allow({
    // Grant full permission to any authenticated user
    admin: function (userId, method, params) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    }
});

Meteor.publish('import-files', function () {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return importFiles.find({
        status: {
            $ne: 'completed'
        }
    });
});

if (!Meteor.settings.disableJobs) {
    if (Meteor.settings.jobs && Meteor.settings.jobs.importFiles) {
        Meteor.startup(function () {
            // Start the myJobs queue running
            importFiles.startJobServer();

            console.log('JOB activated: importFiles');
            importFiles.processJobs('importFiles', Meteor.settings.jobs.importFiles, (job, cb) => {

            });

            console.log('JOB activated: receiptImportDownloads');
            importFiles.processJobs('receiptImportDownloads', Meteor.settings.jobs.importFiles, (job, cb) => {
                import { getReceiptImages } from 'meteor/vatfree:receipts/server/lib/import-downloads';
                console.log('JOB getReceiptImages', job.data._id, job.data['imageUrl']);
                getReceiptImages(job.data, (err, fileId) => {
                    if (err) {
                        job.fail(err, {
                            fatal: false
                        });
                    } else {
                        job.log('File uploaded: ' + fileId);
                        job.progress(100, 100);
                        job.done();
                    }
                    cb(null);
                });
            });

            console.log('JOB activated: s3ImportDownloads');
            importFiles.processJobs('s3ImportDownloads', Meteor.settings.jobs.importFiles, (job, cb) => {
                import { getS3FileAsync } from 'meteor/vatfree:files/server/import';
                const updateData = job.data;
                console.log('JOB getS3FileAsync', updateData['salesforceId'], updateData['target']);
                getS3FileAsync(updateData['salesforceId'], updateData['name'], updateData['contextIds'], updateData['target'], updateData['type'], (err, fileId) => {
                    if (err) {
                        job.fail(err, {
                            fatal: false
                        });
                    } else {
                        job.log('File uploaded: ' + fileId);
                        job.progress(100, 100);
                        job.done();
                    }
                    cb(null);
                });
            });
        });
    }
}

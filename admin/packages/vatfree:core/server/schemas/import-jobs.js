importJobs = new JobCollection('imports');
importJobs.allow({
    // Grant full permission to any authenticated user
    admin: function (userId, method, params) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    }
});

Meteor.publish('import-jobs', function () {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return importJobs.find({
        status: {
            $ne: 'completed'
        }
    });
});

if (!Meteor.settings.disableJobs) {
    if (Meteor.settings.jobs && Meteor.settings.jobs.imports) {
        Meteor.startup(function () {
            // Start the myJobs queue running
            importJobs.startJobServer();

            importJobs.processJobs('imports', Meteor.settings.jobs.imports, (job, cb) => {
                let data = job.data;
                let importFunction = Vatfree[data.importFunction];
                if (_.isFunction(importFunction)) {
                    Vatfree.importFile(data.fileId, importFunction, function (err, progress) {
                        if (err) {
                            job.fail(err);
                        } else {
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                } else {
                    job.fail("importFunction is not a function");
                    cb(null);
                }
            });
        });
    }
}

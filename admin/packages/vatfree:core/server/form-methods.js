Meteor.methods({
    'get-maps-key'() {
        if (Meteor.settings.google && Meteor.settings.google.mapsKey) {
            return Meteor.settings.google.mapsKey;
        } else {
            throw new Meteor.Error('Maps key is not set in settings');
        }
    }
});

const plugins = {};

Vatfree.synch = {
    addPlugin(pluginName, pluginDefinition) {
        plugins[pluginName] = pluginDefinition;
    },
    to(pluginName, callback) {
        if (_.has(plugins, pluginName)) {
            plugins[pluginName].to(callback);
        } else {
            throw new Meteor.Error(404, 'Could not find plugin: ' + pluginName);
        }
    },
    from(pluginName,  callback) {
        console.log('synching', pluginName, ...arguments);
        if (_.has(plugins, pluginName)) {
            plugins[pluginName].from(callback);
        } else {
            throw new Meteor.Error(404, 'Could not find plugin: ' + pluginName);
        }
    },
    upateSynchStatus(collection, collectionId, serviceId, serviceLinkId, serviceResult) {
        const doc = collection.findOne({_id: collectionId});

        const syncDoc = _.find(doc.synchronization, (syncSubDoc) => {
            return syncSubDoc.serviceId === serviceId;
        });
        if (syncDoc) {
            // update status of synch
            collection.update({
                _id: collectionId,
                'synchronization.serviceId': serviceId
            }, {
                $set: {
                    'synchronization.$.serviceLinkId': serviceLinkId,
                    'synchronization.$.serviceResult': serviceResult,
                    'synchronization.$.updatedAt': new Date()
                }
            });
        } else {
            // insert new synch doc
            collection.update({
                _id: collectionId
            }, {
                $addToSet: {
                    synchronization: {
                        serviceId: serviceId,
                        serviceLinkId: serviceLinkId,
                        serviceResult: serviceResult,
                        updatedAt: new Date()
                    }
                }
            });
        }
    }
};

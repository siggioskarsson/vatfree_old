import { Meteor } from "meteor/meteor";

Meteor.methods({
    'search-users'(searchTerm, limit, offset) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {
            'roles.__global_roles__': {
                $in: ['admin', 'vatfree']
            }
        };
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let users = [];
        Meteor.users.find(selector, {
            sort: {
                'profile.name': 1
            },
            limit: limit,
            offset: offset
        }).forEach((user) => {
            users.push({
                text: user.profile.name + ' (' + user.profile.email + ')',
                id: user._id
            });
        });

        return users;
    },
    'get-defaults'() {
        return Meteor.settings.defaults;
    },
    'get-version-info'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Meteor.settings.versionInfo;
    },
    'save-file'(file) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let newFile = new FS.File();
        newFile.name(file.name.replace(/[^a-z0-9-_.]/ig, '_'));
        _.each(file, (value, key) => {
            if (key !== 'name' && key !== 'fileContent') {
                newFile[key] = value;
            }
        });
        newFile.attachData(file.fileContent);
        let fileObj = Files.insert(newFile);

        return fileObj._id;
    },
    getDirectUrl: function(fileId, storeName) {
        this.unblock();
        check(fileId, String);
        if (!storeName) {
            storeName = ['vatfree'];
        }

        let fileObj = Files.findOne({
            _id: fileId
        });

        if (fileObj) {
            if (!Files.downloadAllowed(fileObj, this.userId)) {
                throw new Meteor.Error(404, 'not allowed');
            }

            let urls = {};
            _.each(storeName, (store) => {
                if (fileObj.copies && fileObj.copies[store]) {
                    urls[store] = fileObj.getDirectUrl(store);
                } else {
                    throw new Meteor.Error(500, 'File is not uploaded yet');
                }
            });

            return urls;
        } else {
            return false;
        }
    },
    getDirectUrls: function(fileIds, storeName = 'vatfree') {
        this.unblock();
        check(fileIds, Array);
        check(storeName, String);

        let urls = [];
        _.each(fileIds, (fileId) => {
            let fileObj = Files.findOne({
                _id: fileId
            });

            if (fileObj) {
                if (!Files.downloadAllowed(fileObj, this.userId)) {
                    throw new Meteor.Error(404, 'not allowed');
                }

                let directUrl = fileObj.getDirectUrl(storeName);
                urls.push({
                    fileId: fileId,
                    url: directUrl
                });
            } else {
                urls.push({
                    fileId: fileId,
                    url: false
                });
            }
        });

        return urls;
    },
    'restoreFileFromOriginal'(fileId) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let fileObj = Files.findOne({_id: fileId});
        return fileObj.copyData("vatfree-original", "vatfree");
    },
    getCurrentVersion: function() {
        return {
            createdAt: process.env['HEROKU_RELEASE_CREATED_AT'],
            version: process.env['HEROKU_RELEASE_VERSION']
        };
    },
    'set-qr-upload-context'(context) {
        check(context, Object);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.users.update({
            _id: this.userId
        },{
            $set: {
                'profile.qrUploadContext': context
            }
        });
    },
    'unset-qr-upload-context'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.users.update({
            _id: this.userId
        },{
            $unset: {
                'profile.qrUploadContext': true
            }
        });
    },
    'import'(fileId, subType) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let importFunction = Vatfree['import-' + subType];
        if (_.isFunction(importFunction)) {
            // Create a job:
            let job = new Job(importJobs, 'imports',
                {
                    importFunction: 'import-' + subType,
                    fileId: fileId
                }
            );
            // Set some properties of the job and then submit it
            job.priority('normal')
                .retry({
                    retries: 0
                })
                .save();
        } else {
            throw new Meteor.Error(404, 'Could not find import function: import-' + subType);
        }
    },
    'send-mail'(emailOptions, activityIds) {
        check(emailOptions, Object);
        check(emailOptions.emailAddresses, Array);
        check(emailOptions.emailSubject, String);
        check(emailOptions.emailText, String);
        check(emailOptions.emailTemplateId, String);

        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let user = Meteor.users.findOne({_id: this.userId});
        let emailSubject = emailOptions.emailSubject;
        let emailText = emailOptions.emailText;
        if (user && user.profile && user.profile.email && emailSubject && emailText) {
            let emailBodyText = Vatfree.notify.stripHtmlTags(emailText);

            let template = EmailTemplates.findOne({_id: emailOptions.emailTemplateId});
            if (!template) {
                throw new Meteor.Error(404, 'Could not find email template');
            }

            import handlebars from 'handlebars';
            Vatfree.notify.registerHandlebarsHelpers(handlebars);

            emailSubject = handlebars.compile(emailSubject)(emailOptions);
            emailText = handlebars.compile(emailText)(emailOptions);

            let renderedEmailHtml = handlebars.compile(template.templateHTML)({text: emailText, vatty: user.profile});
            let renderedEmailText = handlebars.compile(template.templateText)({text: emailText, vatty: user.profile});

            let attachments = [];
            if (emailOptions.attachmentIds) {
                _.each(emailOptions.attachmentIds, (attachmentId) => {
                    let file = Files.findOne({_id: attachmentId});
                    attachments.push({
                        filename: file.original.name,
                        path: file.getDirectUrl()
                    });
                });
            }

            let activityLog = {
                type: 'email',
                subject: emailSubject,
                address: emailOptions.emailAddresses.join(','),
                status: 'done',
                notes: renderedEmailText,
                html: renderedEmailText
            };
            if (emailOptions.attachmentIds.length > 0) {
                activityLog.fileIds = emailOptions.attachmentIds;
            }
            _.each(activityIds, (value, key) => {
                activityLog[key] = value;
            });
            ActivityLogs.insert(activityLog);

            Email.send({
                to: emailOptions.emailAddresses,
                from: emailOptions.from || user.profile.email,
                subject: emailSubject,
                text: renderedEmailText,
                html: renderedEmailHtml,
                attachments: attachments
            });
        } else {
            throw new Meteor.Error(404, "No email text found");
        }
    },
    'synch-to'(pluginName) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Meteor.wrapAsync(Vatfree.synch.to)(pluginName);
    }
});

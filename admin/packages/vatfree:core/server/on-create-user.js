Accounts.onCreateUser(function(options, user) {
    //console.log(options, user);
    user.createdAt = new Date();
    user.createdBy = this.userId;
    user.roles = {};
    user.profile = options.profile || {};
    user.private = options.private || {};

    if (user.services && user.services.google && user.services.google.email.match(/@vatfree\.com$/)) {
        user.roles["__global_roles__"] = ["vatfree"];
        user.profile.email = user.services.google.email;
        user.emails = [{
            address: user.services.google.email,
            verified: true
        }];

        return user;
    } else {
        if (!user.emails) user.emails = [];
        if (user.services && user.services.google) {
            user.profile.email = user.services.google.email;
            user.emails.push({
                address: user.services.google.email,
                verified: true
            });
        } else if (user.services && user.services.facebook) {
            user.profile.email = user.services.facebook.email;
            user.emails.push({
                address: user.services.facebook.email,
                verified: true
            });
        } else if (user.services && user.services.linkedin) {
            user.profile.email = user.services.linkedin.emailAddress;
            user.emails.push({
                address: user.services.linkedin.emailAddress,
                verified: true
            });
        } else if (user.services && user.services.wechat) {
            user.profile.email = '';
            user.profile.name = user.services.wechat.nickname;

            const safeNickname = user.services.wechat.nickname.replace(/\s/g, '_');
            let username = `${safeNickname}@wechat`;
            let usernameExists = true;
            let iterator = 1;
            while (usernameExists && iterator <= 10) {
                usernameExists = Meteor.users.findOne({
                    username,
                });
                if (usernameExists) {
                    username = `${safeNickname}_${iterator}@wechat`;
                }
                iterator += 1;
            }
            if (usernameExists) {
                // fallback to full union id
                username = `${user.services.wechat.unionId}@wechat`;
            }
            user.username = username;
        } else if (user.emails && user.emails[0] && user.emails[0].address) {
            // email registration
            user.profile.email = user.emails[0].address;
            user.username = user.profile.email;
        }

        if (user.services && user.services.phone && user.services.phone.number) {
            user.profile.mobile = user.services.phone.number;
        }

        let existingUser = false;
        if (user.profile.email) {
            existingUser = Meteor.users.findOne({'emails.address': user.profile.email});
            if (!existingUser) {
                // check profile
                existingUser = Meteor.users.findOne({'profile.email': user.profile.email});
            }
        }

        if (existingUser) {
            if (!existingUser.services) {
                existingUser.services = {
                    resume: {
                        loginTokens: []
                    }
                };
            }
            if (!existingUser.services.resume) {
                existingUser.services.resume = {
                    loginTokens: []
                };
            }

            let service = _.keys(user.services)[0];
            // copy across new service info
            existingUser.services[service] = user.services[service];
            if (user.services.resume && user.services.resume.loginTokens) {
                existingUser.services.resume.loginTokens.push(
                    user.services.resume.loginTokens[0]
                );
            }

            // Check wether the login attempt is OK
            if (_.isFunction(Accounts.isValidNewUser) && !Accounts.isValidNewUser(existingUser)) {
                throw new Meteor.Error(404, 'Invalid login attempt');
            }

            // bad bad hackery
            // http://www.meteorpedia.com/read/Merging_OAuth_accounts
            Meteor.users.remove({_id: existingUser._id});
            delete existingUser.updatedAt;
            delete existingUser.updatedBy;
            return existingUser;
        } else {
            user.roles["__global_roles__"] = ["traveller"];
            if (Meteor.settings.defaults.TravellerTypeId) {
                user.private['travellerTypeId'] = Meteor.settings.defaults.TravellerTypeId;
            }
            user.private['status'] = 'waitingForDocuments';

            if (user.services && user.services.google) {
                user.profile.name = user.services.google.name;
            } else if (user.services && user.services.facebook) {
                user.profile.name = user.services.facebook.name;
            } else if (user.services && user.services.linkedin) {
                user.profile.name = user.services.linkedin.firstName + ' ' + user.services.linkedin.lastName;
            }

            return user;
        }
    }
});

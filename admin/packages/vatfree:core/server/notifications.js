Vatfree.adminNotify = function(message) {
    Vatfree.adminNotifySlack(message);
};

Vatfree.adminNotifySlack = function(message, channel) {
    let slackToken = Meteor.settings.notify && Meteor.settings.notify.slack ? Meteor.settings.notify.slack.token : false;
    if (slackToken) {
        if (!channel) {
            channel = Meteor.settings.notify.slack.channel
        }
        channel = channel || "notifications"; // notifications is default channel
        SlackAPI.chat.postMessage(slackToken, channel, message, {
            username: 'Phoenix',
            as_user: false,
            icon_url: 'https://admin.vatfree.com/apple-icon-180x180.png'
        });
    }
};

Vatfree.adminActivityLogNotifySlack = function(message, channel) {
    let slackToken = Meteor.settings.notify && Meteor.settings.notify.slack ? Meteor.settings.notify.slack.token : false;
    if (slackToken) {
        SlackAPI.chat.postMessage(slackToken, channel, message, {
            username: 'Phoenix',
            as_user: false,
            icon_url: 'https://admin.vatfree.com/apple-icon-180x180.png'
        });
    }
};

Meteor.methods({
    sendOtpForLogin(phoneNumber) {
        check(phoneNumber, String);

        const sanitized = Accounts.sanitizePhone(phoneNumber);
        if (!sanitized) {
            throw new Meteor.Error(500, 'Invalid phone number');
        }

        const otp = '' + Math.round(Random.fraction() * 100000);
        Accounts.setPhoneOtp(sanitized, otp);

        console.log('Verification code for ' + sanitized + ': ' + otp);
        // send otp as sms
        if (Meteor.settings.twilio && Meteor.settings.twilio.accountSid && Meteor.settings.twilio.authToken && Meteor.settings.twilio.from) {
            const client = require('twilio')(Meteor.settings.twilio.accountSid, Meteor.settings.twilio.authToken);
            client.messages.create({
                body: 'vatfree login code: ' + otp,
                to: sanitized,
                from: Meteor.settings.twilio.from
            });
        }

        return true;
    },
});

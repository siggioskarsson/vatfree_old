/* global emailTextTemplateData: true */
/*
import CodeMirror from 'CodeMirror';
import 'CodeMirror/mode/htmlmixed/htmlmixed.js';
import 'CodeMirror/mode/handlebars/handlebars.js';
*/
import autosize from 'meteor/vatfree:core-templates/templates/autosize';

Template.emailTextEdit.onCreated(function () {

});

Template.emailTextEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('email-texts_item', FlowRouter.getParam('itemId') || "");
        this.subscribe('receipt-rejections-list', '');
        this.subscribe('traveller-rejections', '');
        this.subscribe('web-texts');
    });

 /*
    this.autorun(() => {
        if (this.subscriptionsReady()) {
            Tracker.afterFlush(() => {
                $('textarea[name^="text."]').each(function() {
                    var myCodeMirror = CodeMirror.fromTextArea(this, {
                        lineNumbers: true,
                        lineWrapping: true,
                        mode: {name: "handlebars", base: "text/html"}
                    });
                });
            });
        }
    });
*/
});

Template.emailTextEdit.helpers({
    itemDoc() {
        return EmailTexts.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    getBreadcrumbTitle() {
        return this.name + ' (' + this.code + ')';
    }
});

Template.emailTextEditForm.onCreated(function() {
    this.vatRates = new ReactiveArray();
});

Template.emailTextEditForm.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.vatRates.clear();
        if (data.vatRates) {
            let key = 0;
            _.each(data.vatRates, (rate) => {
                this.vatRates.push({
                    key: key++,
                    rate: rate.rate,
                    description: rate.description
                });
            });
        }
    });

    Tracker.afterFlush(() => {
        autosize(this.$('textarea'));
    });
});

Template.emailTextEditForm.helpers({
    getTextPreview() {
        let data = Template.instance().data;
        if (data.text && data.text[this.code]) {
            import handlebars from 'handlebars';
            Vatfree.notify.registerHandlebarsHelpers(handlebars);

            let templateData = _.clone(emailTextTemplateData);
            templateData.language = this.code;
            templateData.receipt.receiptRejectionIds = _.map(ReceiptRejections.find({}, {fields: {_id: 1}, limit: 3}).fetch(), (rejection) => { return rejection._id; });
            templateData.traveller.rejectionIds = _.map(TravellerRejections.find({}, {fields: {_id: 1}, limit: 3}).fetch(), (rejection) => { return rejection._id; });

            return Vatfree.notify.stripHtmlTags(handlebars.compile(data.text[this.code])(templateData));
        }
    },
    getHTMLPreview() {
        let data = Template.instance().data;
        if (data.text && data.text[this.code]) {
            import handlebars from 'handlebars';
            Vatfree.notify.registerHandlebarsHelpers(handlebars);

            let templateData = _.clone(emailTextTemplateData);
            templateData.language = this.code;
            templateData.receipt.receiptRejectionIds = _.map(ReceiptRejections.find({}, {fields: {_id: 1}, limit: 3}).fetch(), (rejection) => { return rejection._id; });
            templateData.traveller.rejectionIds = _.map(TravellerRejections.find({}, {fields: {_id: 1}, limit: 3}).fetch(), (rejection) => { return rejection._id; });

            return handlebars.compile(data.text[this.code])(templateData);
        }
    },
    getTextForLanguage() {
        let data = Template.instance().data;
        if (data.text && data.text[this.code]) {
            return data.text[this.code];
        }
    },
    getSubjectForLanguage() {
        let data = Template.instance().data;
        if (data.subject && data.subject[this.code]) {
            return data.subject[this.code];
        }
    },
    getEmailTextLanguages() {
        return this.group === 'travellers' ? Vatfree.languages.getTravellerLanguages() : Vatfree.languages.getShopLanguages();
    }
});

Template.emailTextEditForm.events({
    'click .add-vat-rate'(e, template) {
        e.preventDefault();
        template.vatRates.push({
            key: template.vatRates.length,
            rate: "",
            description: ""
        });
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/email-texts');
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        import handlebars from 'handlebars';
        Vatfree.notify.registerHandlebarsHelpers(handlebars);
        let error = false;
        _.each(formData.text, (emailText, language) => {
            try {
                handlebars.compile(emailText)({})
            } catch(err) {
                error = err;
            }
        });
        if (error) {
            toastr.error(error.message);
            return false;
        }

        EmailTexts.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Email Text saved!')
            }
        });
    }
});

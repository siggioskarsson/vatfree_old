Template.emailTexts.onCreated(Vatfree.templateHelpers.onCreated(EmailTexts, '/email-texts/', '/email-text/:itemId'));
Template.emailTexts.onRendered(Vatfree.templateHelpers.onRendered());
Template.emailTexts.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.emailTexts.helpers(Vatfree.templateHelpers.helpers);
Template.emailTexts.helpers(emailTextHelpers);
Template.emailTexts.events(Vatfree.templateHelpers.events());

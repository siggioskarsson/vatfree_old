Template.emailTextsList.helpers(Vatfree.templateHelpers.helpers);
Template.emailTextsList.helpers(emailTextHelpers);
Template.emailTextsList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/email-text/:itemId', {itemId: this._id});
    }
});

Template.emailTextsList.helpers({
    getMissingTranslations() {
        let emailText = this;
        let missingTranslations = [];
        let languages = this.group === 'travellers' ? Vatfree.languages.getTravellerLanguages() : Vatfree.languages.getShopLanguages();
        _.each(languages, (language) => {
            if (!emailText.text || !emailText.text[language.code]) {
                missingTranslations.push(language.code);
            }
        });

        return missingTranslations.join(', ');
    }
});

/* EXPORT

var out = ''; var language = 'zh-CN';
EmailTexts.find({}, {sort: {name: 1}}).forEach((w) => {
  out += "----------------------------------------------------------------------------\n";
  out += `--- ${w._id} (${language})\n`;
  out += `--- ${w.name}\n`;
  if (w.description) out += `--- ${w.description}\n`;
  out += "----------------------------------------------------------------------------\n";
  out += (_.has(w, 'text') && _.has(w.text, language) ? w.text[language] : '') + "\n";
  out += "----------------------------------------------------------------------------\n\n\n\n\n\n";
});

*/

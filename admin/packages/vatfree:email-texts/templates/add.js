Template.addEmailTextModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-email-text').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-email-text')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-email-text').on('shown.bs.modal', function () {
            //
        });
        $('#modal-add-email-text').modal('show');
    });
});

Template.addEmailTextModal.onDestroyed(function() {
    $('#modal-add-email-text').modal('hide');
});

Template.addEmailTextModal.helpers({
});

Template.addEmailTextModal.events({
    'click .cancel-add-email-text'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-email-text-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        EmailTexts.insert(formData);
        template.hideModal();
    }
});

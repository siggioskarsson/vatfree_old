UI.registerHelper('getEmailTextName', function(emailTextId) {
    if (!emailTextId) emailTextId = this.emailTextId;

    let emailText = EmailTexts.findOne({_id: emailTextId});
    if (emailText) {
        return emailText.name;
    }

    return "";
});

UI.registerHelper('getEmailTexts', function(group) {
    let selector = {};
    if (group) {
        selector.group = group;
    }
    return EmailTexts.find(selector, {
        sort: {
            name: 1
        }
    });
});

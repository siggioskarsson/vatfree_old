ActivityStream.attachHooks(EmailTexts);

EmailTexts.after.insert(function(userId, doc) {
    EmailTexts.updateTextSearch(doc);
});

EmailTexts.after.update(function(userId, doc) {
    EmailTexts.updateTextSearch(doc);
});

EmailTexts.updateTextSearch = function(doc) {
    let textSearch =  (
        (doc.name || "") + ' ' +
        (doc.group || "") + ' ' +
        (doc.description || "")
    );

    _.each(doc.subject, (text) => {
        textSearch += ' ' + text;
    });
    _.each(doc.text, (text) => {
        textSearch += ' ' + text;
    });

    EmailTexts.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: Vatfree.notify.stripHtmlTags(textSearch).toLowerCase().latinize()
        }
    });
};

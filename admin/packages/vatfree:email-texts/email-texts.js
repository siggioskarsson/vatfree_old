EmailTexts = new Meteor.Collection('email-texts');
EmailTexts.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Name of the email template",
        optional: false
    },
    description: {
        type: String,
        label: "Description of this email template",
        optional: true
    },
    group: {
        type: String,
        label: "The group this email text is a part of",
        allowedValues: [
            'shops',
            'travellers',
            'affiliates'
        ],
        defaultValue: 'shops'
    },
    subject: {
        type: Object,
        label: "All the translations for this subject",
        optional: true,
        blackbox: true
    },
    text: {
        type: Object,
        label: "All the translations for this text",
        optional: true,
        blackbox: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
EmailTexts.attachSchema(CreatedUpdatedSchema);

EmailTexts.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

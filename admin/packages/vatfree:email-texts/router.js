FlowRouter.route('/email-texts', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "emailTexts"});
    }
});

FlowRouter.route('/email-text/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "emailTextEdit"});
    }
});

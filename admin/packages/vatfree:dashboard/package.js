Package.describe({
    name: 'vatfree:dashboard',
    summary: 'Vatfree dashboard package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core'
    ]);

    // shared files
    api.addFiles([
    ]);

    // server files
    api.addFiles([
        'server/methods/invoices.js',
        'server/methods.js',
        'server/popr.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/dashboard.less',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/dashboard.html',
        'templates/dashboard.js',
        'templates/invoice-stats.html',
        'templates/invoice-stats.js',
        'templates/popr.html',
        'templates/popr.js',
        'templates/tables.html',
        'templates/tables.js',
        'templates/team-stats.html',
        'templates/team-stats.js',
        'templates/charts/count-receipts-last-12-weeks.html',
        'templates/charts/count-receipts-last-12-weeks.js',
        'templates/charts/duration-last-months.html',
        'templates/charts/duration-last-months.js',
        'templates/charts/invoices-chart-per-month.html',
        'templates/charts/invoices-chart-per-month.js',
        'templates/charts/money-last-12-weeks.html',
        'templates/charts/money-last-12-weeks.js',
        'templates/charts/partner-last-12-weeks.html',
        'templates/charts/partner-last-12-weeks.js',
        'templates/charts/receipts-chart-per-week.html',
        'templates/charts/receipts-chart-per-week.js',
        'templates/charts/receipts-chart-per-month.html',
        'templates/charts/receipts-chart-per-month.js',
        'templates/financials/financials.html',
        'templates/financials/financials.js',
        'templates/popr/table.html',
        'templates/popr/table.js',
        'templates/tables/count-receipts-last-12-weeks.html',
        'templates/tables/count-receipts-last-12-weeks.js',
        'templates/tables/invoices-last-x-months.html',
        'templates/tables/invoices-last-x-months.js',
        'templates/tables/receipt-channel-last-x-months.html',
        'templates/tables/receipt-channel-last-x-months.js',
        'templates/tables/receipt-channel-last-x-weeks.html',
        'templates/tables/receipt-channel-last-x-weeks.js',
        'templates/tables/team-stats.html',
        'templates/tables/team-stats.js',
        'templates/top30/shops.html',
        'templates/top30/shops.js',
        'templates/top30/top30.html',
        'templates/top30/top30.js'
    ], 'client');

    api.export([
    ]);
});

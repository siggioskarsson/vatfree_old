Template.dashboard_popr_table.onCreated(function() {
    this.poprData = new ReactiveVar();
    this.phnxData = new ReactiveVar();
    this.loading = new ReactiveVar(true);
    this.loadingPhnx = new ReactiveVar(true);

    this.shopsLoaded = new ReactiveVar(false);
});

Template.dashboard_popr_table.onRendered(function() {
    const template = this;

    this.autorun(() => {
        let data = Template.currentData();
        let fromDate = data.fromDate;
        let tillDate = data.tillDate;
        let retailerId = data.retailerId;

        template.poprData.set();
        this.loading.set(true);
        Meteor.call('popr-shop-stats', fromDate, tillDate, retailerId, (err, poprData) => {
            if (err) {
                toastr.error("Something went wrong. Please contact IT support.");
                console.log(err);
            } else {
                template.poprData.set(poprData);
            }
            template.loading.set(false);
        });

        this.loadingPhnx.set(true);
        Meteor.call('popr-shop-local-stats', fromDate, tillDate, retailerId, (err, phnxData) => {
            if (err) {
                toastr.error("Something went wrong. Please contact IT support.");
                console.log(err);
            } else {
                template.phnxData.set(phnxData);
            }
            template.loadingPhnx.set(false);
        });
    });

    this.autorun(() => {
        template.shopsLoaded.set(false);

        let poprData = template.poprData.get();

        let poprIds = [];
        _.each(poprData, (p) => {
            poprIds.push(p._id);
        });

        let selector = {
            poprId: {
                $in: poprIds
            }
        };
        this.subscribe('shops', "", selector, {}, 1000, 0, {
            onReady() {
                template.shopsLoaded.set(true);
            }
        });
    });
});

Template.dashboard_popr_table.helpers({
    loadingData() {
        const template = Template.instance();
        return template.loading.get() && template.loadingPhnx.get() && !template.shopsLoaded.get();
    },
    getRetailerName() {
        if (this.retailerId) {
            let retailer = _.find(this.retailers, (r) => { return r._id === this.retailerId; });
            if (retailer) {
                return retailer.name;
            }
        }
    },
    getPoprData() {
        let poprData = Template.instance().poprData.get() || [];

        let shopData = {};
        _.each(poprData, (poprD => {
            const shop = Shops.findOne({poprId: poprD._id});
            if (shop) {
                if (_.has(shopData, shop._id)) {
                    // merge with existing shop ...
                    shopData[shop._id].a += poprD.a;
                    shopData[shop._id].c += poprD.c;
                } else {
                    poprD.shopId = shop._id;
                    poprD.shop = shop;
                    shopData[shop._id] = poprD;
                }
            } else {
                shopData[poprD._id] = poprD;
            }
        }));

        shopData = _.values(shopData);
        shopData.sort((a, b) => {
            if (a.c === b.c) {
                return a.a > b.a ? -1 : 1;
            } else {
                return a.c > b.c ? -1 : 1;
            }
        });

        return shopData;
    },
    getShopValue(field = 'c') {
        const poprData = this;
        if (poprData.shop) {
            const phnxData = Template.instance().phnxData.get() || [];
            if (phnxData) {
                const shopData = _.find(phnxData, (d) => { return d._id === poprData.shop._id; });
                return shopData ? shopData[field] : 0;
            }
        }

        return 0;
    },
    getPercentage(a, b) {
        return Math.round(100 * a/b);
    }
});

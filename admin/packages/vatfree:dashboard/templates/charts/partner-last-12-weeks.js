import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);

let initChart = function (ctx, lineData) {
    import("chart.js")
        .then((imported) => {
            const Chart = imported.default;
            new Chart(ctx, {
                type: 'line',
                data: lineData,
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    scales: {
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                callback: function (value, index, values) {
                                    return value;
                                }
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index',
                        callbacks: {
                            label: function (tooltipItem, data) {
                                const series = data.datasets[tooltipItem.datasetIndex];
                                return (series ? series.label : tooltipItem.yLabel) + ': ' + tooltipItem.yLabel;
                            },
                            footer: function (tooltipItems, data) {
                                var sum = 0;
                                tooltipItems.forEach(function (tooltipItem) {
                                    sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                });
                                return 'Total: ' + sum;
                            },
                        },
                        footerFontStyle: 'normal'
                    }
                }
            });
        });
};

Template.dashboard_partner_receipts_last_weeks.onCreated(function() {
    this.moneyOfReceipts = new ReactiveVar();
});

Template.dashboard_partner_receipts_last_weeks.onRendered(function() {
    const template = this;

    const attribute = template.data.attribute;
    const lineColors = ['e6007e', '1c84c6', '1c84c6', 'f3f3f4', '5E5E5E'];
    Meteor.call('dashboard-partner-receipts-last-weeks', 12, (err, data) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            var weeks = [];
            let mRange = moment.range(moment().startOf('isoWeek').subtract(12, 'weeks'), moment());
            for (let mWeek of mRange.by('weeks')) {
                weeks.push(mWeek.format('GGGG[W]WW'));
            }
            let datasets = [];
            let statii = [
                'partner',
                'pledger',
                'affiliate',
                'new',
                'uncooperative'
            ];

            let n = 0;
            _.each(statii, (statusDescription) => {
                const partnershipStatus = statusDescription;
                let dataSeries = [];
                _.each(weeks, (week) => {
                    let dataWeek = Number(week.replace('W', ''));
                    let dataPoint = _.find(data, (dataP) => {
                            return dataP._id.createdWeek === dataWeek && dataP._id.partnershipStatus === partnershipStatus;
                        }) || {};
                    let dataValue = dataPoint && dataPoint[attribute] ? dataPoint[attribute] : 0;
                    if (attribute !== 'count') {
                        dataValue /= 100;
                    }
                    dataSeries.push(dataValue);
                });

                datasets.push({
                    label: statusDescription,
                    backgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                    borderColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 0.7),
                    pointBackgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                    pointBorderColor: "#fff",
                    data: dataSeries
                });
                n++;
            });

            var lineData = {
                labels: weeks,
                datasets: datasets
            };

            const lineChart = document.createElement('canvas');
            lineChart.id = 'lineChart';
            lineChart.height = '344px';
            template.$('.lineChartDiv').append(lineChart); // adds the canvas to #someBox

            var ctx = lineChart.getContext("2d");
            initChart(ctx, lineData);
        }
    });
});

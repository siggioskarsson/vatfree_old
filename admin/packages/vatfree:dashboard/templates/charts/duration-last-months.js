import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);
import './duration-last-months.html';
import { initChart } from '../helpers';

Template.dashboard_duration_last_months.onCreated(function() {
    this.moneyOfReceipts = new ReactiveVar();
});

Template.dashboard_duration_last_months.onRendered(function() {
    const template = this;
    const nrOfMonths = template.data.months || 12;
    const partners = template.data.partners || false;

    const lineColors = ['4D4D4D', '5DA5DA', 'FAA43A', '60BD68', 'F17CB0', 'B2912F', 'B276B2', 'DECF3F', 'F15854'];
    Meteor.call('dashboard-receipts-duration-last-months', nrOfMonths, partners, (err, data) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            var months = [];
            let mRange = moment.range(moment().startOf('month').subtract(nrOfMonths, 'months'), moment());
            for (let mMonth of mRange.by('months')) {
                months.push(mMonth.format('YYYY[M]MM'));
            }
            let datasets = [];

            let n = 0;
            let dataSeriesPaid = [];
            let dataSeriesInvoiced = [];
            let dataSeriesChecked = [];
            let dataSeriesPost = [];
            let errorObject = {};
            _.each(months, (month) => {
                let dataMonth = month.toString().split('M');
                let dataPoint = _.find(data, (dataP) => {
                    return dataP._id.paidMonth === Number(dataMonth[1]) && dataP._id.paidYear === Number(dataMonth[0]);
                }) || {};

                let dataValuePaid = dataPoint && dataPoint.durationPaid ? dataPoint.durationPaid : 0;
                dataValuePaid /= 1000*60*60*24;
                dataSeriesPaid.push(Math.round(dataValuePaid));

                let dataValueInvoiced = dataPoint && dataPoint.durationInvoiced ? dataPoint.durationInvoiced : 0;
                dataValueInvoiced /= 1000*60*60*24;
                dataSeriesInvoiced.push(Math.round(dataValueInvoiced));

                let dataValueChecked = dataPoint && dataPoint.durationChecked ? dataPoint.durationChecked : 0;
                dataValueChecked /= 1000*60*60*24;
                dataSeriesChecked.push(Math.round(dataValueChecked));

                let dataValuePost = dataPoint && dataPoint.durationPost ? dataPoint.durationPost : 0;
                dataValuePost /= 1000*60*60*24;
                dataSeriesPost.push(Math.round(dataValuePost));

                let stdDev = (dataPoint && dataPoint.stdDev ? dataPoint.stdDev : 0);
                stdDev /= 2*1000*60*60*24;
                errorObject[month] = {
                    plus: stdDev,
                    minus: -stdDev
                };
            });

            datasets.push({
                label: 'Duration until post',
                //backgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                borderColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 0.7),
                pointBackgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                pointBorderColor: "#fff",
                data: dataSeriesPost
            });
            n++;
            datasets.push({
                label: 'Duration until checked',
                //backgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                borderColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 0.7),
                pointBackgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                pointBorderColor: "#fff",
                data: dataSeriesChecked
            });
            n++;
            datasets.push({
                label: 'Duration until invoiced',
                //backgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                borderColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 0.7),
                pointBackgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                pointBorderColor: "#fff",
                data: dataSeriesInvoiced
            });
            n++;
            datasets.push({
                label: 'Duration until paid',
                //backgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                borderColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 0.7),
                pointBackgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                pointBorderColor: "#fff",
                data: dataSeriesPaid
            });
            //errorBars: errorObject

            var lineData = {
                labels: months,
                datasets: datasets
            };

            const lineChart = document.createElement('canvas');
            lineChart.id = 'lineChart';
            lineChart.height = '344px';
            template.$('.lineChartDiv').append(lineChart); // adds the canvas to #someBox

            var ctx = lineChart.getContext("2d");
            initChart(ctx, lineData);
        }
    });
});

import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);
import './receipts-chart-per-week.html';
import { initChart } from '../helpers';

Template.dashboard_table_receipts_chart_per_week.onRendered(function() {
    const template = this;
    const nrOfWeeks = template.data.weeks || moment().diff(moment().subtract(2, 'years').startOf('year'), 'weeks');
    const attr = template.data.attr;

    const lineColors = ['4D4D4D', '5DA5DA', 'FAA43A', '60BD68', 'F17CB0', 'B2912F', 'B276B2', 'DECF3F', 'F15854'];
    Meteor.call('dashboard-money-receipts-last-weeks', nrOfWeeks, false, (err, data) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            let weeks = [];
            let mRange = moment.range(moment().startOf('week').subtract(nrOfWeeks, 'weeks'), moment());
            for (let mMonth of mRange.by('weeks')) {
                weeks.push(mMonth.format('WW'));
            }
            weeks = _.uniq(weeks);
            weeks.sort();

            let datasets = [];
            let yearData = {};
            //agregate yearly data
            _.each(data, (d) => {
                const year = d._id.createdWeek.toString().substr(0, 4);
                const week = d._id.createdWeek.toString().substr(4, 2);
                if (!yearData[year]) yearData[year] = {};
                yearData[year][week] = d;
            });

            console.log(yearData);

            let n = 0;
            _.each(yearData, (weekData, year) => {
                if (year <= moment().format('YYYY')) {
                    const dataSeries = [];
                    _.each(weeks, (week) => {
                        dataSeries.push(weekData[week] ? (weekData[week][attr] || 0) : 0);
                    });
                    datasets.push({
                        label: year,
                        borderColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 0.7),
                        pointBackgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                        pointBorderColor: "#fff",
                        data: dataSeries
                    });
                    n++;
                }
            });

            var lineData = {
                labels: weeks,
                datasets: datasets
            };

            let formatValues = (v) => v;
            if (attr === 'fee') {
                formatValues = (v) => {
                    return Vatfree.numbers.formatCurrency(v, 0);
                }
            }

            const lineChart = document.createElement('canvas');
            lineChart.id = 'lineChart';
            lineChart.height = '344px';
            template.$('.lineChartDiv').append(lineChart); // adds the canvas to #someBox

            var ctx = lineChart.getContext("2d");
            initChart(ctx, lineData, false, formatValues);
        }
    });
});

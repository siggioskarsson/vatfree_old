import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);

let initChart = function (ctx, lineData) {
    import("chart.js")
        .then((imported) => {
            const Chart = imported.default;
            new Chart(ctx, {
                type: 'line',
                data: lineData,
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    scales: {
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                callback: function (value, index, values) {
                                    return Vatfree.numbers.formatCurrency(value);
                                }
                            }
                        }]
                    },
                    tooltips: {
                        mode: 'index',
                        callbacks: {
                            label: function (tooltipItem, data) {
                                const series = data.datasets[tooltipItem.datasetIndex];
                                return (series ? series.label : tooltipItem.yLabel) + ': ' + Vatfree.numbers.formatCurrency(tooltipItem.yLabel);
                            },
                            footer: function (tooltipItems, data) {
                                var sum = 0;
                                tooltipItems.forEach(function (tooltipItem) {
                                    sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                });
                                return 'Total: ' + Vatfree.numbers.formatCurrency(sum);
                            },
                        },
                        footerFontStyle: 'normal'
                    }
                }
            });
        });
};

Template.dashboard_money_receipts_last_weeks.onCreated(function() {
    this.moneyOfReceipts = new ReactiveVar();
});

Template.dashboard_money_receipts_last_weeks.onRendered(function() {
    const template = this;

    Meteor.call('dashboard-money-receipts-last-weeks', 12, (err, data) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            var weeks = [];
            let mRange = moment.range(moment().startOf('isoWeek').subtract(12, 'weeks'), moment());
            for (let mWeek of mRange.by('weeks')) {
                weeks.push(mWeek.format('GGGG[W]WW'));
            }
            let datasets = [];
            let statii = {
                'affiliateFee': "Affiliate service fee",
                'fee': "Service fee",
                'refund': "Refund amount",
                'exVat': "Amount ex VAT",
            };

            let n = 0;
            let total = {
                exVat: 0,
                fee: 0,
                refund: 0
            };
            _.each(statii, (statusDescription, status) => {
                let dataSeries = [];
                _.each(weeks, (week) => {
                    let dataWeek = week.replace('W', '');
                    let dataPoint = _.find(data, (dataP) => {
                            return dataP._id.createdWeek == dataWeek;
                        }) || {};
                    let dataValue = dataPoint[status] ? dataPoint[status] : 0;
                    total[status] += dataValue;
                    dataSeries.push(dataValue);
                });

                datasets.push({
                    label: statusDescription,
                    backgroundColor: Vatfree.dashboard.rgb2rgba(Vatfree.dashboard.lineColors[n], 0.5),
                    borderColor: Vatfree.dashboard.rgb2rgba(Vatfree.dashboard.lineColors[n], 0.7),
                    pointBackgroundColor: Vatfree.dashboard.rgb2rgba(Vatfree.dashboard.lineColors[n], 1),
                    pointBorderColor: "#fff",
                    data: dataSeries
                });
                n++;
            });
            this.moneyOfReceipts.set(total);

            var lineData = {
                labels: weeks,
                datasets: datasets
            };

            const lineChart = document.createElement('canvas');
            lineChart.id = 'lineChart';
            lineChart.height = '344px';
            template.$('.lineChartDiv').append(lineChart); // adds the canvas to #someBox

            var ctx = lineChart.getContext("2d");
            initChart(ctx, lineData);
        }
    });
});

Template.dashboard_money_receipts_last_weeks.helpers({
    totalRevenue() {
        let money = Template.instance().moneyOfReceipts.get();
        if (money && money['exVat']) {
            return money['exVat'] + money['fee'] + money['refund'];
        }
    },
    totalVat() {
        let money = Template.instance().moneyOfReceipts.get();
        if (money && money['exVat']) {
            return money['fee'] + money['refund'];
        }
    },
    totalRefund() {
        let money = Template.instance().moneyOfReceipts.get();
        if (money && money['exVat']) {
            return money['refund'];
        }
    },
    totalFee() {
        let money = Template.instance().moneyOfReceipts.get();
        if (money && money['exVat']) {
            return money['fee'];
        }
    },
});

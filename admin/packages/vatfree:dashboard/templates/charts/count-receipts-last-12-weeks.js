import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);

let initChart = function (ctx, lineData) {
    import("chart.js")
        .then((imported) => {
            const Chart = imported.default;
            new Chart(ctx, {
                type: 'line',
                data: lineData,
                options: {
                    height: 344,
                    maintainAspectRatio: false,
                    responsive: true,
                    scales: {
                        yAxes: [{
                            stacked: true
                        }]
                    },
                    tooltips: {
                        mode: 'nearest',
                        callbacks: {
                            footer: function (tooltipItems, data) {
                                var total = 0;
                                for (var i = 0; i < data.datasets.length; i++)
                                    total += data.datasets[i].data[tooltipItems[0].index];

                                return 'Total: ' + total;
                            },
                        },
                        footerFontStyle: 'normal'
                    }
                }
            });
        });
};

Template.dashboard_count_receipts_last_weeks.onCreated(function() {
    this.numberOfReceipts = new ReactiveVar(0);
});

Template.dashboard_count_receipts_last_weeks.onRendered(function() {
    const template = this;

    Meteor.call('dashboard-count-receipts-last-weeks', 12, (err, data) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            var weeks = [];
            let mRange = moment.range(moment().startOf('isoWeek').subtract(12, 'weeks'), moment());
            for (let mWeek of mRange.by('weeks')) {
                weeks.push(mWeek.format('GGGG[W]WW'));
            }
            let datasets = [];
            let statii = _.clone(Receipts.simpleSchema()._schema.status.allowedValues);
            let n = 0;
            let totalReceiptCount = 0;
            _.each(statii, (status) => {
                let dataSeries = [];
                _.each(weeks, (week) => {
                    let dataWeek = Number(week.replace('W', ''));
                    let dataPoint = _.find(data, (dataP) => {
                            return dataP._id.createdWeek === dataWeek && dataP._id.status === status;
                        }) || {};
                    totalReceiptCount += dataPoint.count || 0;
                    dataSeries.push(dataPoint.count || 0);
                });

                datasets.push({
                    label: TAPi18n.__(status),
                    backgroundColor: Vatfree.dashboard.statusColors[status], //Vatfree.dashboard.rgb2rgba(Vatfree.dashboard.lineColors[n], 0.5),
                    borderColor: Vatfree.dashboard.statusColors[status], //Vatfree.dashboard.rgb2rgba(Vatfree.dashboard.lineColors[n], 0.7),
                    pointBackgroundColor: Vatfree.dashboard.statusColors[status], //Vatfree.dashboard.rgb2rgba(Vatfree.dashboard.lineColors[n], 1),
                    pointBorderColor: "#fff",
                    data: dataSeries
                });
                n++;
            });
            this.numberOfReceipts.set(totalReceiptCount);

            var lineData = {
                labels: weeks,
                datasets: datasets
            };

            const lineChart = document.createElement('canvas');
            lineChart.id = 'lineChart';
            lineChart.height = '344px';
            template.$('.lineChartDiv').append(lineChart); // adds the canvas to #someBox

            var ctx = lineChart.getContext("2d");
            initChart(ctx, lineData);
        }
    });
});

Template.dashboard_count_receipts_last_weeks.helpers({
    numberOfReceipts() {
        return Template.instance().numberOfReceipts.get();
    }
});

import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);
import './receipts-chart-per-month.html';
import { initChart } from '../helpers';

Template.dashboard_table_invoices_chart_per_month.onRendered(function() {
    const template = this;
    const nrOfMonths = template.data.months || moment().diff(moment().subtract(2, 'years').startOf('year'), 'months');
    const attr = template.data.attr;

    const lineColors = ['4D4D4D', '5DA5DA', 'FAA43A', '60BD68', 'F17CB0', 'B2912F', 'B276B2', 'DECF3F', 'F15854'];
    Meteor.call('dashboard-money-invoices-last-months', nrOfMonths, false, (err, data) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            let months = [];
            let mRange = moment.range(moment().startOf('month').subtract(nrOfMonths, 'months'), moment());
            for (let mMonth of mRange.by('months')) {
                months.push(mMonth.format('MM'));
            }
            months = _.uniq(months);
            months.sort();

            let datasets = [];
            let yearData = {};
            //agregate yearly data
            _.each(data, (d) => {
                const year = d._id.createdYear;
                const month = d._id.createdMonth;
                if (!yearData[year]) yearData[year] = {};
                yearData[year][month] = d;
            });

            let n = 0;
            _.each(yearData, (monthData, year) => {
                year = Number(year);
                if (year <= moment().format('YYYY')) {
                    const dataSeries = [];
                    _.each(months, (month) => {
                        month = Number(month);
                        dataSeries.push(monthData[month] ? (monthData[month][attr] || 0) : 0);
                    });
                    datasets.push({
                        label: year,
                        borderColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 0.7),
                        pointBackgroundColor: Vatfree.dashboard.rgb2rgba(lineColors[n], 1),
                        pointBorderColor: "#fff",
                        data: dataSeries
                    });
                    n++;
                }
            });

            var lineData = {
                labels: months,
                datasets: datasets
            };

            let formatValues = (v) => v;
            if (attr === 'fee') {
                formatValues = (v) => {
                    return Vatfree.numbers.formatCurrency(v, 0);
                }
            }

            const lineChart = document.createElement('canvas');
            lineChart.id = 'lineChart';
            lineChart.height = '344px';
            template.$('.lineChartDiv').append(lineChart); // adds the canvas to #someBox

            var ctx = lineChart.getContext("2d");
            initChart(ctx, lineData, false, formatValues);
        }
    });
});

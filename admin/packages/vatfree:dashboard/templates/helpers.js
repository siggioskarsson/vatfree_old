Vatfree.dashboard = {};
Vatfree.dashboard.lineColors = ['4D4D4D', '5DA5DA', 'FAA43A', '60BD68', 'F17CB0', 'B2912F', 'B276B2', 'DECF3F', 'F15854'];
Vatfree.dashboard.rgb2rgba = function(rgb, opacity) {
    var hexToDec = function(hex) {
        var result = 0, digitValue;
        hex = hex.toLowerCase();
        for (var i = 0; i < hex.length; i++) {
            digitValue = '0123456789abcdefgh'.indexOf(hex[i]);
            result = result * 16 + digitValue;
        }
        return result;
    };

    rgb = (rgb || "000000").replace('#' ,'');
    opacity = opacity || 1;

    return 'rgba(' + hexToDec(rgb.substr(0,2)) + ', ' + hexToDec(rgb.substr(2,2)) + ', ' + hexToDec(rgb.substr(4,2)) + ', ' + opacity + ')';
};

Vatfree.dashboard.statusColors = {
    'userPending': '#706f6f',
    'shopPending': '#f77c00',
    'waitingForOriginal': '#706f6f',
    'visualValidationPending': '#f77c00',
    'waitingForDoubleCheck': '#f77c00',
    'readyForInvoicing': '#f77c00',
    'waitingForPayment': '#ffff0b',
    'paidByShop': '#0069b4',
    'userPendingForPayment': '#706f6f',
    'requestPayout': '#9cc2e0',
    'paid': '#95c11f',
    'rejected': '#e1faf5'
};

export const initChart = function (ctx, lineData, stacked = true, formatValues) {
    if (!formatValues) {
        formatValues = (v) => v;
    }
    import("chart.js")
        .then((imported) => {
            const Chart = imported.default;
            import("chartjs-plugin-error-bars")
                .then((imported2) => {
                    new Chart(ctx, {
                        type: 'line',
                        data: lineData,
                        options: {
                            maintainAspectRatio: false,
                            responsive: true,
                            scales: {
                                yAxes: [{
                                    stacked: stacked,
                                    ticks: {
                                        callback: function (value, index, values) {
                                            return formatValues(value);
                                        }
                                    }
                                }]
                            },
                            tooltips: {
                                mode: 'index',
                                callbacks: {
                                    label: function (tooltipItem, data) {
                                        const series = data.datasets[tooltipItem.datasetIndex];
                                        return (series ? series.label : tooltipItem.yLabel) + ': ' + formatValues(tooltipItem.yLabel);
                                    },
                                    footer: function (tooltipItems, data) {
                                        var sum = 0;
                                        tooltipItems.forEach(function (tooltipItem) {
                                            sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                        });
                                        return 'Total: ' + sum;
                                    },
                                },
                                footerFontStyle: 'normal'
                            },
                            plugins: {
                                chartJsPluginErrorBars: {
                                    color: '#666',
                                    width: 10 | '10px' | '60%',
                                    lineWidth: 2 | '2px',
                                    absoluteValues: false
                                }
                            }
                        }
                    });
                });
        });
};

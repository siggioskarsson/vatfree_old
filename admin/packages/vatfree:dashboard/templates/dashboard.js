Template.dashboard.onRendered(function() {
    this.autorun(() => {
        this.subscribe('menu-stats');
    });
});

Template.dashboard.helpers({
    noRoles() {
        let user = Meteor.user();
        if (user) {
            if (!user.roles && !user.roles.__global_roles__) {
                return true;
            }

            if (_.contains(user.roles.__global_roles__, 'vatfree')) {
                return !_.has(user.roles, 'vatfree-backend') || user.roles['vatfree-backend'].length === 0;
            }
        }

        return false;
    }
});

Template.dashboard.events({
    'click .receipt-tasks'(e) {
        e.preventDefault();
        FlowRouter.go('/receipts?tasks=1')
    },
    'click .traveller-tasks'(e) {
        e.preventDefault();
        FlowRouter.go('/travellers?tasks=1')
    },
    'click .shop-tasks'(e) {
        e.preventDefault();
        FlowRouter.go('/shops?tasks=1')
    },
    'click .my-tasks'(e) {
        e.preventDefault();
        FlowRouter.go('/activity-logs?tasks=1')
    }
});

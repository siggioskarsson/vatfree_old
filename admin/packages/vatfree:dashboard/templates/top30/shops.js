Template.dashboard_top30_shops.onCreated(function () {
    this.top30Data = new ReactiveVar();
    this.loading = new ReactiveVar(true);
});

Template.dashboard_top30_shops.onRendered(function () {
    const template = this;

    this.autorun(() => {
        let data = Template.currentData();
        let aggregate = data.aggregate;
        let fromDate = data.fromDate;
        let tillDate = data.tillDate;
        let searchTerm = data.searchTerm;
        let partnershipStatus = data.partnershipStatus;

        if (aggregate && fromDate && tillDate) {
            template.top30Data.set();
            this.loading.set(true);
            Meteor.call('dashboard-top-shops', aggregate, 30, fromDate, tillDate, searchTerm, partnershipStatus, (err, top30Data) => {
                if (err) {
                    toastr.error('Something went wrong. Please contact IT support.');
                    console.log(err);
                } else {
                    template.top30Data.set(top30Data);
                }
                this.loading.set(false);
            });
        }
    });

    this.autorun(() => {
        let top30Data = this.top30Data.get();
        let shopIds = [];
        _.each(top30Data, (data) => {
            shopIds.push(data._id.shopId);
        });

        this.subscribe('shops', "", {_id: {$in: shopIds}});
    });
});

Template.dashboard_top30_shops.helpers({
    getTopData () {
        let top30Data = Template.instance().top30Data.get();
        if (top30Data) {
            top30Data = JSON.parse(JSON.stringify(top30Data));
            const returnData = [];
            _.each(top30Data, (d) => {
                d._id = d._id.shopId;
                returnData.push(d);
            });
            return returnData;
        }

        return false;
    },
    getLoading () {
        return Template.instance().loading.get();
    },
    formatDataValue () {
        let data = Template.instance().data;
        if (data.aggregate === 'count') {
            return this.count;
        } else {
            const shop = Shops.findOne({_id: this._id }) || {};
            const currency = Currencies.findOne({_id: shop.currencyId}) || {};
            const currencySymbol = currency.symbol || "€";
            return Vatfree.numbers.formatCurrency(this[data.aggregate], 0, currencySymbol);
        }
    }
});

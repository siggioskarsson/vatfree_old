import moment from 'moment/moment';

Template.dashboard_top30.onCreated(function() {
    this.dateFrom = new ReactiveVar(Session.get(this.view.name + '.dateFrom') || moment().startOf('week').toDate());
    this.dateTill = new ReactiveVar(Session.get(this.view.name + '.dateTill') || moment().endOf('week').toDate());
    this.searchTerm = new ReactiveVar(Session.get(this.view.name + '.searchTerm') || "");
    this.partnershipStatus = new ReactiveArray();
    _.each(Session.get(this.view.name + '.partnershipStatus') || [], (status) => {
        this.partnershipStatus.push(status);
    });
});

Template.dashboard_top30.onRendered(function() {
    this.autorun(() => {
        let dateFrom = this.dateFrom.get();
        let dateTill = this.dateTill.get();
        let searchTerm = this.searchTerm.get();
        let partnershipStatus = this.partnershipStatus.array();

        Session.setPersistent(this.view.name + '.dateFrom', dateFrom);
        Session.setPersistent(this.view.name + '.dateTill', dateTill);
        Session.setPersistent(this.view.name + '.searchTerm', searchTerm);
        Session.setPersistent(this.view.name + '.partnershipStatus', partnershipStatus);
    });

    Tracker.afterFlush(() => {
        Vatfree.templateHelpers.initDatepicker.call(this, null, moment().format(TAPi18n.__('_date_format')));
    });
});

Template.dashboard_top30.helpers({
    getFromDate() {
        return Template.instance().dateFrom.get();
    },
    getTillDate() {
        return Template.instance().dateTill.get();
    },
    getSearchTerm() {
        return Template.instance().searchTerm.get();
    },
    getPartnershipStatus() {
        return Template.instance().partnershipStatus.array();
    },
});

Template.dashboard_top30.events({
    'change input[name="dateFrom"]'(e, template) {
        template.dateFrom.set(moment($(e.currentTarget).val(), TAPi18n.__('_date_format')).toDate());
    },
    'change input[name="dateTill"]'(e, template) {
        template.dateTill.set(moment($(e.currentTarget).val(), TAPi18n.__('_date_format')).toDate());
    },
    'change input[name="item-search"]'(e, template) {
        e.preventDefault();
        let searchTerm = template.$('input[name="item-search"]').val();
        template.searchTerm.set(searchTerm);
    },
    'click .filter-icon.partnership-status-filter-icon'(e, template) {
        e.preventDefault();
        e.stopPropagation();

        let status = this.toString();
        let partnershipStatusFilter = template.partnershipStatus.array();

        if (_.contains(partnershipStatusFilter, status)) {
            template.partnershipStatus.clear();
            _.each(partnershipStatusFilter, (_status) => {
                if (status !== _status) {
                    template.partnershipStatus.push(_status);
                }
            });
        } else {
            template.partnershipStatus.push(status);
        }
    },
});

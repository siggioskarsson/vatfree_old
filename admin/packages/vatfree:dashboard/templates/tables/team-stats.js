let receiptAggregations = {
    "->rejected": "Created",
    "->userPending": "Created",
    "->visualValidationPending": "Created",
    "->waitingForOriginal": "Created",
    "visualValidationPending->readyForInvoicing": "Receipt checked",
    "visualValidationPending->rejected": "Receipt checked",
    "visualValidationPending->shopPending": "Receipt checked",
    "visualValidationPending->waitingForDoubleCheck": "Receipt checked",
    "visualValidationPending->userPending": "User changed",
    "waitingForDoubleCheck->readyForInvoicing": "Receipt double checked",
    "waitingForDoubleCheck->rejected": "Receipt double checked",
    "waitingForDoubleCheck->shopPending": "Receipt double checked",
    "waitingForDoubleCheck->visualValidationPending": "Re-check",
    "waitingForOriginal->visualValidationPending": "Original processed",
    "paid->paidByShop": "Payout reverted",
    "paidByShop->requestPayout": "Payout requested",
    "paidByShop->waitingForPayment": "Payment reversed",
    "readyForInvoicing->waitingForPayment": "Invoiced",
    "readyForInvoicing->userPending": "Re-check",
    "readyForInvoicing->visualValidationPending": "Re-check",
    "readyForInvoicing->rejected": "Rejected",
    "rejected->userPending": "Re-opened",
    "rejected->visualValidationPending": "Re-opened",
    "rejected->shopPending": "Re-opened",
    "requestPayout->paid": "Paid to traveller",
    "requestPayout->paidByShop": "Payout canceled",
    "shopPending->readyForInvoicing": "Shop activated",
    "shopPending->visualValidationPending": "Shop activated",
    "userPending->visualValidationPending": "Traveller verified",
    "userPending->waitingForOriginal": "Traveller verified",
    "userPendingForPayment->paidByShop": "Traveller verified",
    "userPendingForPayment->waitingForPayment": "Traveller verified",
    "waitingForPayment->notCollectable": "Not collectable",
    "waitingForPayment->paidByShop": "Paid by shop",
    "waitingForPayment->readyForInvoicing": "Invoice canceled",
    "waitingForPayment->userPendingForPayment": "Paid by shop"
};

let teamStatsHelpers = {
    isLoading() {
        return Template.instance().loading.get();
    },
    getVatties() {
        let data = Template.instance().dataCounts.get();
        let users = [];
        _.each(data, (typeCounts, userId) => {
            users.push(userId);
        });

        return Meteor.users.find({
            _id: {
                $in: users
            }
        }, {
            sort: {
                'profile.name': 1
            }
        });
    },
    getShortName() {
        return this.profile.name.split(' ')[0];
    },
    getTaskTypes() {
        let data = Template.instance().dataCounts.get();
        let types = [];
        _.each(data, (typeCounts, userId) => {
            _.each(typeCounts, (count, type) => {
                types.push(type);
            });
        });
        types = _.uniq(types);
        types.sort();

        return types;
    },
    getVattyTaskCount(type) {
        let data = Template.instance().dataCounts.get();
        return data && data[this._id] && _.has(data[this._id], type) ? data[this._id][type] : "";
    },
    getTravellerTaskCount(type) {
        let data = Template.instance().dataCounts.get();
        return data && data["traveller"] && _.has(data["traveller"], type) ? data["traveller"][type] : "";
    },
    getVattyTaskTotal() {
        let data = Template.instance().dataCounts.get();
        let total = 0;
        _.each(data[this._id], (count, type) => {
            total+= count;
        });

        return total;
    },
    getTravellerTaskTotal() {
        let data = Template.instance().dataCounts.get();
        let total = 0;
        _.each(data["traveller"], (count, type) => {
            total+= count;
        });

        return total;
    }
};

Template.dashboard_table_team_receipt_stats.onCreated(function() {
    this.loading = new ReactiveVar();
    this.dataCounts = new ReactiveVar({});
});
Template.dashboard_table_team_receipt_stats.onRendered(function() {
    this.autorun(() => {
        let templateData = Template.currentData();
        this.dataCounts.set({});
        this.loading.set(true);
        Meteor.call('dashboard-team-receipt-task-stats', templateData.fromDate, templateData.tillDate, (err, data) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                if (templateData.aggregate) {
                    let aggregatedData = {};
                    _.each(data, (userData, userId) => {
                        if (!_.has(aggregatedData, userId)) {
                            aggregatedData[userId] = {};
                        }
                        _.each(userData, (number, type) => {
                            if (_.has(receiptAggregations, type)) {
                                type = receiptAggregations[type];
                            }
                            if (!_.has(aggregatedData[userId], type)) {
                                aggregatedData[userId][type] = 0;
                            }
                            aggregatedData[userId][type] += number
                        });
                    });
                    this.dataCounts.set(aggregatedData);
                } else {
                    this.dataCounts.set(data);
                }
            }
            this.loading.set(false);
        });
    });
});

Template.dashboard_table_team_receipt_stats.helpers(teamStatsHelpers);


let travellerAggregations = {
    "->notEligibleForRefund": "Created",
    "->documentationIncomplete": "Created",
    "->userUnverified": "Created",
    "->userVerified": "Created",
    "->waitingForDocuments": "Created",
    "documentationExpired->userUnverified": "User update",
    "documentationExpired->userVerified": "User update",
    "documentationExpired->waitingForDocuments": "User update",
    "documentationIncomplete->userUnverified": "User update",
    "documentationIncomplete->waitingForDocuments": "User update",
    "documentationIncomplete->userVerified": "User verified",
    "notEligibleForRefund->userUnverified": "User update",
    "notEligibleForRefund->waitingForDocuments": "User update",
    "userUnverified->documentationIncomplete": "Task manager",
    "userUnverified->userVerified": "Task manager",
    "userUnverified->waitingForDocuments": "Task manager",
    "userVerified->documentationExpired": "User unverify",
    "userVerified->userUnverified": "User unverify",
    "userVerified->waitingForDocuments": "User unverify",
    "userVerified->notEligibleForRefund": "User unverify",
    "waitingForDocuments->userUnverified": "User update",
    "waitingForDocuments->userVerified": "User update"
};

Template.dashboard_table_team_traveller_stats.onCreated(function() {
    this.loading = new ReactiveVar();
    this.dataCounts = new ReactiveVar({});
});
Template.dashboard_table_team_traveller_stats.onRendered(function() {
    this.autorun(() => {
        let templateData = Template.currentData();
        this.dataCounts.set({});
        this.loading.set(true);
        Meteor.call('dashboard-team-traveller-task-stats', templateData.fromDate, templateData.tillDate, (err, data) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                if (templateData.aggregate) {
                    let aggregatedData = {};
                    _.each(data, (userData, userId) => {
                        if (!_.has(aggregatedData, userId)) {
                            aggregatedData[userId] = {};
                        }
                        _.each(userData, (number, type) => {
                            if (_.has(travellerAggregations, type)) {
                                type = travellerAggregations[type];
                            }
                            if (!_.has(aggregatedData[userId], type)) {
                                aggregatedData[userId][type] = 0;
                            }
                            aggregatedData[userId][type] += number
                        });
                    });
                    this.dataCounts.set(aggregatedData);
                } else {
                    this.dataCounts.set(data);
                }
            }
            this.loading.set(false);
        });
    });
});
Template.dashboard_table_team_traveller_stats.helpers(teamStatsHelpers);

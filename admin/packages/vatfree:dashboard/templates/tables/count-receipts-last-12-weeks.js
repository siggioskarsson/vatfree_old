Template.dashboard_table_count_receipts_last_weeks.onCreated(function() {
    this.numberOfReceipts = new ReactiveVar();

    let template = this;
    Meteor.call('dashboard-data-receipts-last-weeks', 120000, (err, data) => {
        if (err) {
            alert(err);
        } else {
            let statii = {};
            _.each(data, (dataPoint) => {
                let status = dataPoint._id.status;
                if (!_.has(statii, status)) {
                    statii[status] = {
                        amount: 0,
                        exVat: 0,
                        vat: 0,
                        fee: 0,
                        refund: 0,
                        count: 0
                    };
                }
                statii[status].amount += dataPoint.amount;
                statii[status].exVat += dataPoint.exVat;
                statii[status].vat += dataPoint.vat;
                statii[status].fee += dataPoint.fee;
                statii[status].refund += dataPoint.refund;
                statii[status].count += dataPoint.count;
            });

            template.numberOfReceipts.set(statii);
        }
    });
});

Template.dashboard_table_count_receipts_last_weeks.helpers({
    getStatusColors() {
        let colors = [];
        _.each(Vatfree.dashboard.statusColors, (color, status) => {
            colors.push({
                color: color,
                status: status
            });
        });

        return colors;
    },
    getStatusValue(attr = 'count') {
        let requests = Template.instance().numberOfReceipts.get() || {};
        return (requests[this.status] ? requests[this.status][attr] : 0) || 0;
    },
    getStatusVat(attr = 'vat') {
        let requests = Template.instance().numberOfReceipts.get() || {};
        return (requests[this.status] ? requests[this.status][attr] : 0) || 0;
    }
});

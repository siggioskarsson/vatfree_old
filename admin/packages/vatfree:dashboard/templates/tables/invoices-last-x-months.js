import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);

const showWarning = function(period, type, count, percentage) {
    let month = period.toString();
    if (type && type.id && month < moment().subtract(1, 'months').format('YYYYMM')) {
        if (type.id === 'readyToSend' && count > 0) {
            return true;
        } else if (type.id === 'partner' && count > 0 && percentage < 95) {
            return true;
        } else if (type.id === 'pledger' && count > 0 && percentage < 85) {
            return true;
        }
    }

    return false;
};

const showSuccess = function(period, type, count, percentage) {
    let month = period.toString();
    if (type && type.id && month < moment().subtract(1, 'months').format('YYYYMM')) {
        if (count > 0) {
            if (type.id === 'partner') {
                if(percentage >= 95) {
                    return true;
                }
            } else if (type.id === 'pledger') {
                if (percentage >= 85) {
                    return true;
                }
            } else if (percentage >= 79) {
                return true;
            }
        }
    }

    return false;
};

Template.dashboard_table_invoices_last_x_months.onCreated(function() {
    this.dataCounts = new ReactiveVar({});
    this.dataSums = new ReactiveVar({});
    this.dataTypes = new ReactiveVar([]);
    this.poprTotals = new ReactiveVar({});
    this.cashIn = new ReactiveVar({});
    this.cashOut = new ReactiveVar({});

    this.numberOfMonths = new ReactiveVar(this.data.months || 8);
    this.dataAggr = new ReactiveVar(this.data.aggr || 'partnershipStatus');
});

Template.dashboard_table_invoices_last_x_months.onRendered(function() {
    let aggr = this.dataAggr.get();
    Meteor.call('dashboard-count-invoices-by-aggr-last-months', this.numberOfMonths.get(), aggr, (err, rawData) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            const data = [];
            _.each(rawData, (r) => {
                const month = '' + (r._id.createdMonth < 10 ? '0' + r._id.createdMonth : r._id.createdMonth);
                r._id = r._id.createdYear + month + ':' + r._id[aggr];
                data.push(r);
            });

            this.dataCounts.set(data);
            let dataTypes = {};
            _.each(data, (dataCount) => {
                let _id = dataCount._id.split(':');
                let id = _id[1] + (_id[2] ? ':' + _id[2] : '');
                let title = _id[1] + (_id[2] ? ' ' + _id[2] : '');
                dataTypes[id] = {
                    id: id,
                    title: title
                };
            });
            dataTypes = _.values(dataTypes);
            if (aggr === 'status') {
                let status = _.clone(Invoices.simpleSchema()._schema.status.allowedValues);
                dataTypes.sort(function(a,b) {
                    let aIndex = status.indexOf(a.id);
                    if (aIndex === -1) aIndex = 999;
                    let bIndex = status.indexOf(b.id);
                    if (bIndex === -1) bIndex = 999;
                    return aIndex > bIndex ? 1 : -1;
                });
            } else if (aggr === 'partnershipStatus') {
                let status = _.clone(Shops.simpleSchema()._schema.partnershipStatus.allowedValues);
                dataTypes.sort(function(a,b) {
                    let aIndex = status.indexOf(a.id);
                    if (aIndex === -1) aIndex = 999;
                    let bIndex = status.indexOf(b.id);
                    if (bIndex === -1) bIndex = 999;
                    return aIndex > bIndex ? 1 : -1;
                });
            } else {
                dataTypes.sort(function(a,b) {
                    return a.title > b.title ? 1 : -1;
                });
            }
            this.dataTypes.set(dataTypes);
        }
    });
    Meteor.call('dashboard-money-invoices-last-months', this.numberOfMonths.get(), true, (err, data) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            this.dataSums.set(data);
        }
    });

    Meteor.call('get-popr-totals', 'm', 'A', (err, poprData) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            this.poprTotals.set(poprData);
        }
    });

    Meteor.call('dashboard-get-cash-in', 'M', this.numberOfMonths.get(), (err, cashData) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            this.cashIn.set(cashData);
        }
    });

    Meteor.call('dashboard-get-cash-out', 'M', this.numberOfMonths.get(), (err, cashData) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            this.cashOut.set(cashData);
        }
    });
});

Template.dashboard_table_invoices_last_x_months.helpers({
    getDataAggr() {
        const template = Template.instance();
        return template.dataAggr.get();
    },
    getPeriods() {
        let template = Template.instance();
        let months = [];
        let mRange = moment.range(moment().startOf('month').subtract(template.numberOfMonths.get(), 'months'), moment());
        for (let mMonth of mRange.by('months')) {
            months.push(mMonth.format('YYYYMM'));
        }

        return months.reverse();
    },
    getTypes() {
        return Template.instance().dataTypes.get();
    },
    getTotalCount() {
        let month = this.toString();
        let dataCounts = Template.instance().dataCounts.get();
        let total = 0;
        _.each(dataCounts, (dataCount) => {
            if (dataCount._id.match(month)) {
                total += dataCount.count;
            }
        });
        return total;
    },
    getTotalPaid() {
        const template = Template.instance();
        if (template.dataAggr.get() === 'status') return false;

        let month = this.toString();
        let dataCounts = template.dataCounts.get();
        let total = 0;
        let paid = 0;
        _.each(dataCounts, (dataCount) => {
            if (dataCount._id.match(month)) {
                total += dataCount.count;
                paid += dataCount.paid;
            }
        });
        return total ? Math.round(100 * paid / total) : false;
    },
    getPoprCount() {
        const year = Number(this.toString().substr(0,4));
        const month = Number(this.toString().substr(4,2));
        let dataSums = Template.instance().dataSums.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdYear === year && dataSum._id.createdMonth === month) {
                total += dataSum.popr;
            }
        });
        return total;
    },
    getPoprTotalCount() {
        let month = Number(this.toString());
        let poprTotals = Template.instance().poprTotals.get();

        if (poprTotals) {
            let poprData = _.find(poprTotals, (poprTotal) => {
                return poprTotal.pv === month;
            });
            if (poprData) {
                return poprData.c;
            }
        }

        return 0;
    },
    getTypeCount(type) {
        let month = Number(this.toString());
        let dataCounts = Template.instance().dataCounts.get();
        let id = month + ':' + type.id;
        let data = _.find(dataCounts, (dataCount) => {
            return dataCount._id === id;
        });
        return data ? data.count : 0;
    },
    getPercentagePaid(type) {
        const template = Template.instance();
        if (template.dataAggr.get() === 'status') return false;

        let month = Number(this.toString());
        let dataCounts = template.dataCounts.get();
        let id = month + ':' + type.id;
        let data = _.find(dataCounts, (dataCount) => {
            return dataCount._id === id;
        });
        return data ? Math.round(100 * data.paid / data.count) : false;
    },
    getTotalAmount() {
        const year = Number(this.toString().substr(0,4));
        const month = Number(this.toString().substr(4,2));
        let dataSums = Template.instance().dataSums.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdYear === year && dataSum._id.createdMonth === month) {
                total += dataSum.amount;
            }
        });
        return total;
    },
    getTotalServiceFee() {
        const year = Number(this.toString().substr(0,4));
        const month = Number(this.toString().substr(4,2));
        let dataSums = Template.instance().dataSums.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdYear === year && dataSum._id.createdMonth === month) {
                total += dataSum.fee;
            }
        });
        return total;
    },
    getTotalRealisedServiceFee() {
        const year = Number(this.toString().substr(0,4));
        const month = Number(this.toString().substr(4,2));
        let dataSums = Template.instance().dataSums.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdYear === year && dataSum._id.createdMonth === month) {
                total += dataSum.rFee;
            }
        });
        return total;
    },
    getTotalPercentRealised() {
        const year = Number(this.toString().substr(0,4));
        const month = Number(this.toString().substr(4,2));
        let dataSums = Template.instance().dataSums.get();
        let feeTotal = 0;
        let rFeeTotal = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdYear === year && dataSum._id.createdMonth === month) {
                feeTotal += dataSum.fee;
                rFeeTotal += dataSum.rFee;
            }
        });
        return feeTotal ? 100 * 100 * (rFeeTotal / feeTotal) : 0;
    },
    getAverageInvoice() {
        const year = Number(this.toString().substr(0,4));
        const month = Number(this.toString().substr(4,2));
        let dataSums = Template.instance().dataSums.get();
        let fee = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdYear === year && dataSum._id.createdMonth === month) {
                fee += dataSum.fee;
            }
        });
        let dataCounts = Template.instance().dataCounts.get();
        let total = 0;
        _.each(dataCounts, (dataCount) => {
            const dataYearMonth = '' + year + (month.toString().length === 1 ? '0' : '') + month;
            const _id = dataCount._id.split(':');
            if (dataYearMonth === _id[0]) {
                total += dataCount.count;
            }
        });

        return total ? fee / total : 0;
    },
    getTotalVat() {
        const year = Number(this.toString().substr(0,4));
        const month = Number(this.toString().substr(4,2));
        let dataSums = Template.instance().dataSums.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdYear === year && dataSum._id.createdMonth === month) {
                total += dataSum.vat;
            }
        });
        return total;
    },
    getCashIn() {
        let month = this.toString();
        let dataSums = Template.instance().cashIn.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            const dataYear = dataSum._id.valueYear.toString();
            const dataMonth = dataSum._id.valueMonth.toString();
            const dataYearMonth = '' + dataYear + (dataMonth.length === 1 ? '0' : '') + dataMonth;
            if (dataYearMonth === month) {
                total += dataSum.amount;
            }
        });
        return total;
    },
    getCashOut() {
        let month = this.toString();
        let dataSums = Template.instance().cashOut.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            const dataYear = dataSum._id.createdYear.toString();
            const dataMonth = dataSum._id.createdMonth.toString();
            const dataYearMonth = '' + dataYear + (dataMonth.length === 1 ? '0' : '') + dataMonth;
            if (dataYearMonth === month) {
                total += dataSum.amount;
            }
        });
        return total;
    },
    isLastPeriod() {
        let month = this.toString();
        return month === moment().subtract(1, 'months').format('YYYYMM');
    },
    showWarning(period, type, count, percentage) {
        return showWarning(period, type, count, percentage) ? "error" : "";
    },
    showSuccess(period, type, count, percentage) {
        return showSuccess(period, type, count, percentage) ? "all-ok" : "";
    }
});

import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);

Template.dashboard_table_receipts_channel_last_x_weeks.onCreated(function() {
    this.dataCounts = new ReactiveVar({});
    this.dataSums = new ReactiveVar({});
    this.dataTypes = new ReactiveVar([]);
    this.poprTotals = new ReactiveVar({});
    this.cashIn = new ReactiveVar({});
    this.cashOut = new ReactiveVar({});

    this.numberOfWeeks = new ReactiveVar(this.data.weeks || 8);
    this.dataAggr = new ReactiveVar(this.data.aggr || 'source');
});

Template.dashboard_table_receipts_channel_last_x_weeks.onRendered(function() {
    let aggr = this.dataAggr.get();
    Meteor.call('dashboard-count-receipts-by-type-last-weeks', this.numberOfWeeks.get(), aggr, (err, rawData) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            const data = [];
            _.each(rawData, (r) => {
                if (aggr === 'status') {
                    r._id = r._id.createdWeek + ':' + r._id.status;
                } else {
                    r._id = r._id.createdWeek + ':' + r._id.source;
                }
                data.push(r);
            });

            this.dataCounts.set(data);
            let dataTypes = {};
            _.each(data, (dataCount) => {
                let _id = dataCount._id.split(':');
                let id = _id[1] + (_id[2] ? ':' + _id[2] : '');
                let title = _id[1] + (_id[2] ? ' ' + _id[2] : '');
                dataTypes[id] = {
                    id: id,
                    title: title
                };
            });
            dataTypes = _.values(dataTypes);
            if (aggr === 'status') {
                let status = _.clone(Receipts.simpleSchema()._schema.status.allowedValues);
                dataTypes.sort(function(a,b) {
                    let aIndex = status.indexOf(a.id);
                    let bIndex = status.indexOf(b.id);
                    return aIndex > bIndex ? 1 : -1;
                });
            } else {
                dataTypes.sort(function(a,b) {
                    return a.title > b.title ? 1 : -1;
                });
            }
            this.dataTypes.set(dataTypes);
        }
    });
    Meteor.call('dashboard-money-receipts-last-weeks', this.numberOfWeeks.get(), true, (err, data) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            this.dataSums.set(data);
        }
    });

    Meteor.call('get-popr-totals', 'w', 'A', (err, poprData) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            this.poprTotals.set(poprData);
        }
    });

    Meteor.call('dashboard-get-cash-in', 'W', this.numberOfWeeks.get(), (err, cashData) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            this.cashIn.set(cashData);
        }
    });

    Meteor.call('dashboard-get-cash-out', 'W', this.numberOfWeeks.get(), (err, cashData) => {
        if (err) {
            toastr.error(err.reason);
        } else {
            this.cashOut.set(cashData);
        }
    });
});

Template.dashboard_table_receipts_channel_last_x_weeks.helpers({
    numberOfReceipts() {
        return Template.instance().numberOfReceipts.get();
    },
    getWeeks() {
        let template = Template.instance();
        let weeks = [];
        let mRange = moment.range(moment().startOf('isoWeek').subtract(template.numberOfWeeks.get(), 'weeks'), moment());
        for (let mWeek of mRange.by('weeks')) {
            weeks.push(Number(mWeek.format('GGGGWW')));
        }

        return weeks.reverse();
    },
    getTypes() {
        return Template.instance().dataTypes.get();
    },
    getTotalCount() {
        let week = Number(this);
        let dataCounts = Template.instance().dataCounts.get();
        let total = 0;
        _.each(dataCounts, (dataCount) => {
            if (dataCount._id.match(week)) {
                total += dataCount.count;
            }
        });
        return total;
    },
    getPoprCount() {
        let week = Number(this);
        let dataSums = Template.instance().dataSums.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdWeek === week) {
                total += dataSum.popr;
            }
        });
        return total;
    },
    getPoprTotalCount() {
        let week = Number(this.toString());
        let poprTotals = Template.instance().poprTotals.get();

        if (poprTotals) {
            let poprData = _.find(poprTotals, (poprTotal) => {
                return poprTotal.pv === week;
            });
            if (poprData) {
                return poprData.c;
            }
        }

        return 0;
    },
    getTypeCount(type) {
        let week = this.toString();
        let dataCounts = Template.instance().dataCounts.get();
        let id = week + ':' + type.id;
        let data = _.find(dataCounts, (dataCount) => {
            return dataCount._id === id;
        });
        return data ? data.count : 0;
    },
    getTotalServiceFee() {
        let week = Number(this);
        let dataSums = Template.instance().dataSums.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdWeek === week) {
                total += dataSum.fee;
            }
        });
        return total;
    },
    getTotalRealisedServiceFee() {
        let week = Number(this);
        let dataSums = Template.instance().dataSums.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdWeek === week) {
                total += dataSum.rFee;
            }
        });
        return total;
    },
    getTotalPercentRealised() {
        let week = Number(this);
        let dataSums = Template.instance().dataSums.get();
        let feeTotal = 0;
        let rFeeTotal = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdWeek === week) {
                feeTotal += dataSum.fee;
                rFeeTotal += dataSum.rFee;
            }
        });
        return feeTotal ? 100 * 100 * (rFeeTotal / feeTotal) : 0;
    },
    getAverageServiceFee() {
        let week = Number(this);
        let dataSums = Template.instance().dataSums.get();
        let fee = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdWeek === week) {
                fee += dataSum.fee;
            }
        });
        let dataCounts = Template.instance().dataCounts.get();
        let total = 0;
        _.each(dataCounts, (dataCount) => {
            if (dataCount._id.match(week)) {
                total += dataCount.count;
            }
        });

        return fee / total;
    },
    getTotalVat() {
        let week = Number(this);
        let dataSums = Template.instance().dataSums.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdWeek === week) {
                total += dataSum.vat;
            }
        });
        return total;
    },
    getCashIn() {
        let week = Number(this);
        let dataSums = Template.instance().cashIn.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.valueWeek === week) {
                total += dataSum.amount;
            }
        });
        return total;
    },
    getCashOut() {
        let week = Number(this);
        let dataSums = Template.instance().cashOut.get();
        let total = 0;
        _.each(dataSums, (dataSum) => {
            if (dataSum._id.createdWeek === week) {
                total += dataSum.amount;
            }
        });
        return total;
    },
    isLastWeek() {
        let week = this.toString();
        return week === moment().subtract(1, 'weeks').format('GGGGWW');
    }
});

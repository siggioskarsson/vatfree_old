import moment from "moment/moment";

Template.dashboard_team_stats.onCreated(function() {
    this.dateFrom = new ReactiveVar(localStorage.getItem(this.view + '.dateFrom') || moment().startOf('week').toDate());
    this.dateTill = new ReactiveVar(localStorage.getItem(this.view + '.dateTill') || moment().endOf('week').toDate());

    let aggregate = localStorage.getItem(this.view + '.aggregate');
    this.aggregate = new ReactiveVar(_.isUndefined(aggregate) ? true : aggregate);
});

Template.dashboard_team_stats.onRendered(function() {
    Vatfree.templateHelpers.initDatepicker.call(this, null, moment().format(TAPi18n.__('_date_format')));


    this.autorun(() => {
        let dateFrom = this.dateFrom.get();
        let dateTill = this.dateTill.get();
        let aggregate = this.aggregate.get();

        localStorage.setItem(this.view + '.dateFrom', dateFrom);
        localStorage.setItem(this.view + '.dateTill', dateTill);
        localStorage.setItem(this.view + '.aggregate', aggregate);
    });
});

Template.dashboard_team_stats.helpers({
    getFromDate() {
        return Template.instance().dateFrom.get();
    },
    getTillDate() {
        return Template.instance().dateTill.get();
    },
    getAggregate() {
        return Template.instance().aggregate.get();
    }
});

Template.dashboard_team_stats.events({
    'change input[name="dateFrom"]'(e, template) {
        template.dateFrom.set(moment($(e.currentTarget).val(), TAPi18n.__('_date_format')).toDate());
    },
    'change input[name="dateTill"]'(e, template) {
        template.dateTill.set(moment($(e.currentTarget).val(), TAPi18n.__('_date_format')).toDate());
    },
    'change input[name="aggregate"]'(e, template) {
        template.aggregate.set($(e.currentTarget).prop('checked'));
    }
});
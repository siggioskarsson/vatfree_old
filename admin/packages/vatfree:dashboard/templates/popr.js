import moment from "moment/moment";

Template.dashboard_popr.onCreated(function() {
    this.dateFrom = new ReactiveVar(localStorage.getItem(this.view + '.dateFrom') || moment().startOf('week').toDate());
    this.dateTill = new ReactiveVar(localStorage.getItem(this.view + '.dateTill') || moment().endOf('week').toDate());
    this.retailerId = new ReactiveVar(localStorage.getItem(this.view + '.retailerId') || "");

    this.retailers = new ReactiveArray();
});

Template.dashboard_popr.onRendered(function() {
    Vatfree.templateHelpers.initDatepicker.call(this, null, moment().format(TAPi18n.__('_date_format')));

    Meteor.call('popr-retailers', (err, retailers) => {
        if (err) {
            toastr.error(err.reason || err.message);
        } else {
            this.retailers.clear();
            _.each(retailers, (retailer) => {
                this.retailers.push(retailer);
            });
        }
    });

    this.autorun(() => {
        let dateFrom = this.dateFrom.get();
        let dateTill = this.dateTill.get();
        let retailerId = this.retailerId.get();

        localStorage.setItem(this.view + '.dateFrom', dateFrom);
        localStorage.setItem(this.view + '.dateTill', dateTill);
        localStorage.setItem(this.view + '.retailerId', retailerId);
    });
});

Template.dashboard_popr.helpers({
    getFromDate() {
        return Template.instance().dateFrom.get();
    },
    getTillDate() {
        return Template.instance().dateTill.get();
    },
    getRetailerId() {
        return Template.instance().retailerId.get();
    },
    getRetailers() {
        return Template.instance().retailers.array();
    }
});

Template.dashboard_popr.events({
    'change input[name="dateFrom"]'(e, template) {
        template.dateFrom.set(moment($(e.currentTarget).val(), TAPi18n.__('_date_format')).toDate());
    },
    'change input[name="dateTill"]'(e, template) {
        template.dateTill.set(moment($(e.currentTarget).val(), TAPi18n.__('_date_format')).toDate());
    },
    'change select[name="retailerId"]'(e, template) {
        template.retailerId.set($(e.currentTarget).val());
    }
});

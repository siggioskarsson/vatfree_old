Template.dashboard_financials.onCreated(function() {
    this.years = new ReactiveVar([2017]);
    this.financials = new ReactiveDict();
});

Template.dashboard_financials.onRendered(function() {
    _.each(this.years.get(), (year) => {
        Meteor.call('dashboard-financials', year, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                this.financials.set(year, result);
            }
        });
    });
});

Template.dashboard_financials.helpers({
    getYears() {
        return Template.instance().years.get();
    },
    yearPlus1(year) {
        return year + 1;
    },
    getFinancials(year) {
        return Template.instance().financials.get(year);
    }
});

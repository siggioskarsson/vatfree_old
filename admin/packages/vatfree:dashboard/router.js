FlowRouter.route('/tables', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "tables"});
    }
});

FlowRouter.route('/team-stats', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "dashboard_team_stats"});
    }
});

FlowRouter.route('/financials', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "dashboard_financials"});
    }
});

FlowRouter.route('/top30', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "dashboard_top30"});
    }
});

FlowRouter.route('/popr', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "dashboard_popr"});
    }
});

FlowRouter.route('/stats/invoices', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "dashboard_invoice_stats"});
    }
});

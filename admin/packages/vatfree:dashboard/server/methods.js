import moment from 'moment';

export const dashboardMethodsAllowed = function(userId) {
    if (!Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP)) {
        if (!Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }
    }
};

const paidStatus = ['paidByShop', 'requestPayout', 'paid','userPendingForPayment', 'depreciated' ];
export const moneyGroup = function(aggregationKey) {
    return {
        _id: aggregationKey,
        count: {$sum: 1},
        amount: {$sum: {$ifNull: ['$euroAmounts.amount', 0]}},
        exVat: {$sum: {$subtract: ['$euroAmounts.amount', '$euroAmounts.totalVat']}},
        vat: {$sum: {$ifNull: ['$euroAmounts.totalVat', 0]}},
        fee: {$sum: {$add: [{$ifNull: ['$euroAmounts.serviceFee', 0]}, {$ifNull: ['$euroAmounts.deferredServiceFee', 0]}]}},
        affiliateFee: {$sum: {$ifNull: ['$euroAmounts.affiliateServiceFee', 0]}},
        refund: {$sum: {$ifNull: ['$euroAmounts.refund', 0]}},
        rFee: {$sum: {$cond: {if: {$in: ['$status', paidStatus]}, then: {$add: [{$ifNull: ['$euroAmounts.serviceFee', 0]}, {$ifNull: ['$euroAmounts.deferredServiceFee', 0]}]}, else: 0}}},
        popr: {$sum: {$cond: {if: {$eq: [{$type: '$poprId'}, 'string']}, then: 1, else: 0}}}
    };
};

Meteor.methods({
    'dashboard-count-receipts-last-weeks'(weeks) {
        dashboardMethodsAllowed(this.userId);

        const aggregationKey = { createdWeek: "$createdWeek", status: "$status" };
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment().startOf('isoWeek').subtract(weeks, 'weeks').toDate()
                    }
                },
            },
            {
                $group: {
                    _id : aggregationKey,
                    count: { $sum: 1 }
                }
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-data-receipts-last-weeks'(weeks) {
        dashboardMethodsAllowed(this.userId);

        const aggregationKey = { status: "$status" };
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment().startOf('isoWeek').subtract(weeks, 'weeks').toDate()
                    }
                },
            },
            {
                $group: moneyGroup(aggregationKey)
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-count-receipts-by-type-last-weeks'(weeks, aggr) {
        dashboardMethodsAllowed(this.userId);

        let aggregationKey = { createdWeek: "$createdWeek", source: "$source" };
        if (aggr === 'status') {
            aggregationKey = { createdWeek: "$createdWeek", status: "$status" };
        }
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment().startOf('isoWeek').subtract(weeks, 'weeks').toDate()
                    },
                    status: {
                        $nin: ['rejected', 'deleted', 'notCollectable']
                    }
                },
            },
            {
                $group: {
                    _id : aggregationKey,
                    count: { $sum: 1 }
                }
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-count-receipts-by-type-last-months'(months, aggr) {
        dashboardMethodsAllowed(this.userId);

        let aggregationKey = { createdYear: "$createdYear", createdMonth: "$createdMonth", source: "$source" };
        if (aggr === 'status') {
            aggregationKey = { createdYear: "$createdYear", createdMonth: "$createdMonth", status: "$status" };
        }
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment().startOf('month').subtract(months, 'months').toDate()
                    },
                    status: {
                        $nin: ['rejected', 'deleted', 'notCollectable']
                    }
                },
            },
            {
                $group: {
                    _id : aggregationKey,
                    count: { $sum: 1 }
                }
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-money-receipts-last-weeks'(weeks, bySource = false) {
        dashboardMethodsAllowed(this.userId);

        let aggregationKey = { createdWeek: "$createdWeek" };
        if (bySource) {
            aggregationKey.source = "$source";
        }
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment().startOf('isoWeek').subtract(weeks, 'weeks').toDate(),
                        $lt: moment().toDate()
                    },
                    status: {
                        $nin: ['rejected', 'deleted', 'notCollectable']
                    }
                },
            },
            {
                $group: moneyGroup(aggregationKey)
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-money-receipts-weekday'(fromDate, tillDate) {
        dashboardMethodsAllowed(this.userId);

        let aggregationKey = { dayOfWeek: { $dayOfWeek: "$createdAt" } };
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment(fromDate).toDate(),
                        $lt: moment(tillDate).toDate()
                    },
                    status: {
                        $nin: ['rejected', 'deleted', 'notCollectable']
                    }
                },
            },
            {
                $group: moneyGroup(aggregationKey)
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-partner-receipts-last-weeks'(weeks) {
        dashboardMethodsAllowed(this.userId);

        const aggregationKey = { createdWeek: "$createdWeek", partnershipStatus: "$partnershipStatus" };
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment().startOf('isoWeek').subtract(weeks, 'weeks').toDate()
                    },
                    status: {
                        $nin: ['rejected', 'deleted', 'notCollectable']
                    }
                },
            },
            {
                $group: {
                    _id : aggregationKey,
                    count: { $sum: 1 },
                    fee: { $sum: {$add: [ { $ifNull: ['$euroAmounts.serviceFee', 0] }, { $ifNull: ['$euroAmounts.deferredServiceFee', 0] } ] } }
                }
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-money-receipts-last-months'(months, bySource = false) {
        dashboardMethodsAllowed(this.userId);

        let aggregationKey = { createdYear: "$createdYear", createdMonth: "$createdMonth" };
        if (bySource) {
            aggregationKey.source = "$source";
        }
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment().startOf('month').subtract(months, 'months').toDate()
                    },
                    status: {
                        $nin: ['rejected', 'deleted', 'notCollectable']
                    }
                },
            },
            {
                $group: moneyGroup(aggregationKey)
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-financials'(year) {
        dashboardMethodsAllowed(this.userId);

        let financials = {
            invoicesSent: 0,
            invoicesPaid: 0,
            serviceFeeReceived: 0,
            refundPaid: 0,
            invoicesOpen: 0,
            refundTotal: 0,
            refundOpen: 0
        };

        Invoices.find({
            createdYear: year,
            status: {
                $ne: 'canceled'
            },
            deleted: {
                $ne: true
            }
        },{
            fields: {
                amount: 1,
                paidAt: 1,
                serviceFee: 1,
                refund: 1
            }
        }).forEach((invoice) => {
            financials.invoicesSent+= invoice.amount;
            if (invoice.paidAt && moment(invoice.paidAt).isBefore(moment('' + year + '-12-31', 'YYYY-MM-DD').endOf('day'))) {
                financials.invoicesPaid+= invoice.amount;
                financials.serviceFeeReceived+= invoice.serviceFee;
                financials.refundTotal+= invoice.refund;
            }
            financials.invoicesOpen = financials.invoicesSent - financials.invoicesPaid;
        });

        Payouts.find({
            status: "paid",
            paidAt: {
                $gte: moment('' + year + '-01-01', 'YYYY-MM-DD').startOf('day').toDate(),
                $lte: moment('' + year + '-12-31', 'YYYY-MM-DD').endOf('day').toDate()
            }
        },{
            fields: {
                amount: 1,
                paidAt: 1
            }
        }).forEach((payout) => {
            financials.refundPaid+= payout.amount;
        });
        financials.refundOpen = financials.refundTotal - financials.refundPaid;

        return financials;
    },
    'dashboard-team-receipt-task-stats'(fromDate, tillDate) {
        if (!fromDate || !tillDate) return {};
        dashboardMethodsAllowed(this.userId);

        let stats = {};
        let userIds = _.map(Meteor.users.find({'roles.__global_roles__': {$in: ['admin', 'vatfree']}}).fetch(), (user) => { return user._id; });
        ActivityStream.collection.find({
            date: {
                $gte: moment(new Date(fromDate)).startOf('day').toDate(),
                $lte: moment(new Date(tillDate)).endOf('day').toDate()
            },
            "activity.target.target": 'receipts',
            "activity.object.status": {
                $exists: true
            }
        },{
            fields: {
                activity: 1,
                userId: 1
            }
        }).forEach((activityStream) => {
            let userId = _.contains(userIds, activityStream.userId) ? activityStream.userId : 'traveller';

            if (!stats[userId]) {
                stats[userId] = {};
            }
            let statusChange = (activityStream.activity.objectOld ? activityStream.activity.objectOld.status || "" : "") + '->' + (activityStream.activity.object.status || "")
            if (!stats[userId][statusChange]) {
                stats[userId][statusChange] = 0;
            }
            stats[userId][statusChange]++;
        });

        return stats;
    },
    'dashboard-team-traveller-task-stats'(fromDate, tillDate) {
        if (!fromDate || !tillDate) return {};
        dashboardMethodsAllowed(this.userId);

        let stats = {};
        let userIds = _.map(Meteor.users.find({'roles.__global_roles__': {$in: ['admin', 'vatfree']}}).fetch(), (user) => { return user._id; });
        ActivityStream.collection.find({
            date: {
                $gte: moment(new Date(fromDate)).startOf('day').toDate(),
                $lte: moment(new Date(tillDate)).endOf('day').toDate()
            },
            "activity.target.target": 'users',
            "activity.object.private.status": {
                $exists: true
            }
        },{
            fields: {
                activity: 1,
                userId: 1
            }
        }).forEach((activityStream) => {
            let userId = _.contains(userIds, activityStream.userId) ? activityStream.userId : 'traveller';

            if (!stats[userId]) {
                stats[userId] = {};
            }
            let statusChange = (activityStream.activity.objectOld && activityStream.activity.objectOld.private ? activityStream.activity.objectOld.private.status || "" : "") + '->' + (activityStream.activity.object.private.status || "")
            if (!stats[userId][statusChange]) {
                stats[userId][statusChange] = 0;
            }
            stats[userId][statusChange]++;
        });

        return stats;
    },
    'dashboard-top-shops'(aggregate, numberOf, fromDate, tillDate, searchTerm, partnershipStatus) {
        dashboardMethodsAllowed(this.userId);
        if (!aggregate || !numberOf || !fromDate || !tillDate) return {};

        let selector = {
            createdAt:{
                $gte: moment(new Date(fromDate)).startOf('day').toDate(),
                $lte: moment(new Date(tillDate)).endOf('day').toDate()
            },
            status: {
                $nin: ['rejected', 'deleted', 'notCollectable']
            }
        };

        if (searchTerm) {
            check(searchTerm, String);
            let shopSelector = {
                deleted: {
                    $ne: true
                }
            };
            Vatfree.search.addSearchTermSelector(searchTerm, shopSelector);
            let shopIds = _.map(Shops.find(shopSelector, {fields: {_id: 1}}).fetch(), (shop) => {
                return shop._id;
            });
            selector.shopId = {
                $in: shopIds
            };
        }

        if (partnershipStatus && partnershipStatus.length > 0) {
            selector.partnershipStatus = {
                $in: partnershipStatus
            }
        }

        const aggregationKey = { shopId: "$shopId" };
        const pipeline = [
            {
                $match: selector
            },
            {
                $group: {
                    _id : aggregationKey,
                    count: { $sum: 1 },
                    amount: { $sum: { $ifNull: ['$euroAmounts.amount', 0] } },
                    serviceFee: { $sum: {$add: [ { $ifNull: ['$euroAmounts.serviceFee', 0] }, { $ifNull: ['$euroAmounts.deferredServiceFee', 0] } ] } },
                    refund: { $sum: { $ifNull: ['$euroAmounts.refund', 0] } }
                }
            },
            {
                $sort: {
                    [aggregate]: -1
                }
            },
            {
                $limit: 30
            }
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();


        let map = function() {
            var id = this.shopId || "unknown";
            emit(id, aggregate === 'count' ? 1 : (this[aggregate] || 0));
        };
        let reduce = function(key, counts) {
            return Array.sum(counts);
        };

        let mapReduceQuery = {
            createdAt:{
                $gte: moment(new Date(fromDate)).startOf('day').toDate(),
                $lte: moment(new Date(tillDate)).endOf('day').toDate()
            },
            status: {
                $nin: ['rejected', 'deleted', 'notCollectable']
            }
        };

        if (searchTerm) {
            check(searchTerm, String);
            let selector = {
                deleted: {
                    $ne: true
                }
            };
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
            let shopIds = _.map(Shops.find(selector, {fields: {_id: 1}}).fetch(), (shop) => {
                return shop._id;
            });
            mapReduceQuery.shopId = {$in: shopIds};
        }

        const rawReceipts = Receipts.rawCollection();
        const syncMapReduce = Meteor.wrapAsync(rawReceipts.mapReduce, rawReceipts);
        let data = syncMapReduce(map, reduce, {
            query: mapReduceQuery,
            out: {
                inline: 1
            },
            scope: {
                aggregate: aggregate
            }
        });

        return getTopByValue(numberOf, data);
    },
    'dashboard-get-cash-in'(periodType, periods) {
        dashboardMethodsAllowed(this.userId);

        let aggregationKey = { valueWeek: "$valueWeek" };
        if (periodType === 'M') {
            aggregationKey = {
                valueYear: "$valueYear",
                valueMonth: "$valueMonth"
            };
        }

        let fromDate = moment().startOf('isoWeek').subtract(periods, 'weeks').toDate();
        if (aggregationKey === 'M') {
            fromDate = moment().startOf('month').subtract(periods, 'months').toDate();
        }

        const pipeline = [
            {
                $match: {
                    valueDate: {
                        $gte: fromDate
                    },
                    accountNr: {
                        $nin: Meteor.settings.vatfreeBankAccounts || []
                    },
                    amount: {
                        $gt: 0
                    }
                },
            },
            {
                $group: {
                    _id: aggregationKey,
                    count: { $sum: 1 },
                    amount: { $sum: { $ifNull: ['$amount', 0] } }
                }
            },
        ];
        return Payments.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-get-cash-out'(periodType, periods) {
        dashboardMethodsAllowed(this.userId);

        let aggregationKey = { createdWeek: "$createdWeek" };
        if (periodType === 'M') {
            aggregationKey = {
                createdYear: "$createdYear",
                createdMonth: "$createdMonth"
            };
        }

        let fromDate = moment().startOf('isoWeek').subtract(periods, 'weeks').toDate();
        if (aggregationKey === 'M') {
            fromDate = moment().startOf('month').subtract(periods, 'months').toDate();
        }

        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: fromDate
                    }
                },
            },
            {
                $group: {
                    _id: aggregationKey,
                    count: { $sum: 1 },
                    amount: { $sum: { $ifNull: ['$amount', 0] } }
                }
            },
        ];
        return Payouts.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-receipts-duration-last-months'(months, partner) {
        check(months, Number);
        dashboardMethodsAllowed(this.userId);

        const aggregationKey = {
            paidMonth: {
                $month: "$statusDates.paidByShop"
            },
            paidYear: {
                $year: "$statusDates.paidByShop"
            }
        };
        let partnershipStatus = 'partner';
        if (!partner) {
            partnershipStatus = {
                $ne: 'partner'
            };
        }
        const pipeline = [
            {
                $match: {
                    salesforceId: {
                        $exists: false
                    },
                    createdAt: {
                        $gte: moment().subtract(months + 3, 'months').startOf('month').toDate(),
                    },
                    'statusDates.paidByShop': {
                        $gte: moment().startOf('month').subtract(months, 'months').toDate()
                    },
                    'statusDates.userPending': {
                        $exists: false
                    },
                    'statusDates.notCollectable': {
                        $exists: false
                    },
                    partnershipStatus: partnershipStatus
                },
            },
            {
                $group: {
                    _id : aggregationKey,
                    count: { $sum: 1 },
                    durationPost: { $avg: {$subtract: [ { $ifNull: ['$originalsCheckedAt', null] }, { $ifNull: ['$createdAt', null] } ] } },
                    durationChecked: { $avg: {$subtract: [ { $ifNull: ['$statusDates.readyForInvoicing', null ] }, { $ifNull: ['$originalsCheckedAt', null] } ] } },
                    durationInvoiced: { $avg: {$subtract: [ { $ifNull: ['$statusDates.waitingForPayment', null ] }, { $ifNull: ['$statusDates.readyForInvoicing', null] } ] } },
                    durationPaid: { $avg: {$subtract: [ { $ifNull: ['$statusDates.userPendingForPayment', '$statusDates.paidByShop' ] }, { $ifNull: ['$statusDates.waitingForPayment', null] } ] } },
                    stdDev: { $stdDevSamp: {$subtract: [ { $ifNull: ['$statusDates.userPendingForPayment', '$statusDates.paidByShop'] }, { $ifNull: ['$originalsCheckedAt', null] } ] } }
                }
            },
        ];
        return Receipts.rawCollection().aggregate(pipeline).toArray();
    },
    'test-avg'() {
        let vals = [];
        Receipts.find({
            salesforceId: {
                $exists: false
            },
            createdAt: {
                $gte: moment().subtract(15, 'months').startOf('month').toDate(),
            },
            'statusDates.userPending': {
                $exists: false
            },
            'statusDates.notCollectable': {
                $exists: false
            },
            'statusDates.paidByShop': {
                $gte: moment().month(1).year(2019).startOf('month').toDate(),
                $lt: moment().month(1).year(2019).endOf('month').toDate()
            }
        }, {
            fields: {
                receiptNr: 1,
                createdAt: 1,
                originalsCheckedAt: 1,
                'statusDates.userPendingForPayment': 1,
                'statusDates.paidByShop': 1
            }
        }).forEach((r) => {
            const paidDate = r.statusDates.userPendingForPayment || r.statusDates.paidByShop;
            const duration = moment(paidDate).diff(moment(r.originalsCheckedAt), 'days');
            console.log(r.receiptNr, duration);
            vals.push(duration);
        });
        import stats from 'stats-lite';
        console.log(stats);

        console.log('avg', vals.reduce((a, b) => a + b, 0)/vals.length, stats.stdev(vals));
    }
});

var getTopByValue = function(numberOf, data) {
    data.sort(function(a,b) {
        return a.value > b.value ? -1 : 1;
    });

    return data.slice(0, numberOf);
};

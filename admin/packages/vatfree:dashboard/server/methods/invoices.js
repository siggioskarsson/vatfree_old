import moment from 'moment';
import { dashboardMethodsAllowed, moneyGroup, paidStatus } from '../methods';

Meteor.methods({
    'dashboard-count-invoices-by-aggr-last-months'(months, aggr) {
        dashboardMethodsAllowed(this.userId);

        let aggregationKey = { createdYear: "$createdYear", createdMonth: "$createdMonth", [aggr]: `$${aggr}` };
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment().startOf('month').subtract(months, 'months').toDate()
                    },
                    deleted: {
                        $ne: true
                    }
                },
            },
            {
                $group: {
                    _id : aggregationKey,
                    count: { $sum: 1 },
                    paid: { $sum: {$cond: {if: {$in: ['$status', ['paid']]}, then: 1, else: 0} } },
                }
            },
        ];
        return Invoices.rawCollection().aggregate(pipeline).toArray();
    },
    'dashboard-money-invoices-last-months'(months) {
        dashboardMethodsAllowed(this.userId);

        let aggregationKey = { createdYear: "$createdYear", createdMonth: "$createdMonth" };
        const pipeline = [
            {
                $match: {
                    createdAt: {
                        $gte: moment().startOf('month').subtract(months, 'months').toDate()
                    },
                    deleted: {
                        $ne: true
                    }
                },
            },
            {
                $group: moneyGroup(aggregationKey)
            },
        ];
        return Invoices.rawCollection().aggregate(pipeline).toArray();
    }
});

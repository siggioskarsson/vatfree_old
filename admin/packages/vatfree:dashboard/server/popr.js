import moment from 'moment';
import { dashboardMethodsAllowed } from './methods';

let getPoprData = function (url) {
    try {
        let result = HTTP.post(url, {
            data: {
                secret: Meteor.settings.poprSecret
            }
        });

        if (!result || result.statusCode !== 200) {
            throw new Meteor.Error(500, 'Could not get popr data');
        }

        return result.data;
    } catch(e) {
        console.error(e);
        return {};
    }
};

Meteor.methods({
    'popr-retailers'() {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let url = Vatfree.receipts.getPoprUrl() + 'retailers';
        return getPoprData(url);
    },
    'get-popr-totals'(p, r) {
        this.unblock();
        dashboardMethodsAllowed(this.userId);
        if (!p || !r) return {};

        let url = Vatfree.receipts.getPoprUrl() + 'stats/' + p + '/' + r;

        return getPoprData(url);
    },
    'popr-shop-stats'(fromDate, tillDate, retailerId) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (fromDate) {
            fromDate = moment(new Date(fromDate)).startOf('day').unix();
        } else {
            fromDate = 0;
        }

        if (tillDate) {
            tillDate = moment(new Date(tillDate)).endOf('day').unix();
        } else {
            tillDate = 0;
        }

        if (!retailerId) {
            retailerId = 0;
        }

        let url = Vatfree.receipts.getPoprUrl() + `stats/shops/${fromDate}/${tillDate}/${retailerId}`;

        return getPoprData(url);
    },
    'popr-shop-local-stats'(fromDate, tillDate, retailerId) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (fromDate) {
            fromDate = moment(new Date(fromDate)).startOf('day');
        } else {
            fromDate = moment().subtract(1, 'year');
        }

        if (tillDate) {
            tillDate = moment(new Date(tillDate)).endOf('day');
        } else {
            tillDate = moment();
        }

        if (!retailerId) {
            retailerId = 0;
        }

        let selector = {
            poprId: {
                $exists: true
            },
            status: {
                $nin: ['rejected', 'deleted', 'notCollectable']
            }
        };
        if (retailerId && retailerId !== '0') {
            //selector.retailerId = retailerId;
        }

        selector.purchaseDate = {
            $gte: fromDate.toDate(),
            $lte: tillDate.toDate()
        };

        const pipeline = [
            {
                "$match": selector
            },
            {
                "$group": {
                    _id: "$shopId",
                    c: {
                        "$sum": 1
                    },
                    a: {
                        "$sum": "$amount"
                    },
                    f: {
                        "$sum": "$serviceFee"
                    },
                    v: {
                        "$sum": "$totalVat"
                    }
                }
            }
        ];

        return Receipts.rawCollection().aggregate(pipeline).toArray();
    }
});

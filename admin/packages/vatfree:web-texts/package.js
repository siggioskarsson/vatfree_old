Package.describe({
    name: 'vatfree:web-texts',
    summary: 'Vatfree web text package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core'
    ]);

    // shared files
    api.addFiles([
        'web-texts.js'
    ]);

    // server files
    api.addFiles([
        'server/methods.js',
        'server/web-texts.js',
        'publish/web-texts.js',
        'publish/web-texts-list.js',
        'publish/web-text.js',
        'publish/web-texts-site.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/preview.less',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/web-texts.html',
        'templates/web-texts.js'
    ], 'client');

    api.export([
        'WebTexts'
    ]);
});

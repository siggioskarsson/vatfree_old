Meteor.publish('web-texts-site', function(siteGroup, language) {
    check(siteGroup, String);
    check(language, String);

    if (_.contains(['partner', 'travellers', 'app'], siteGroup)) {
        // do nothing, open for now
    } else if (_.contains(['app'], siteGroup)) {
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }
    } else {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }
    }

    const fields = {
        _id: 1,
        code: 1,
        ["text." + language]: 1
    };

    if (language !== 'en') {
        fields["text.en"] = 1;
    }

    return WebTexts.find({
        group: siteGroup
    },{
        fields: fields
    });
});

FlowRouter.route('/web-texts', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "webTexts"});
    }
});

FlowRouter.route('/web-text/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "webTextEdit"});
    }
});

Template.addWebTextModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-web-text').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-web-text')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-web-text').on('shown.bs.modal', function () {
            //
        });
        $('#modal-add-web-text').modal('show');
    });
});

Template.addWebTextModal.onDestroyed(function() {
    $('#modal-add-web-text').modal('hide');
});

Template.addWebTextModal.helpers({
});

Template.addWebTextModal.events({
    'click .cancel-add-web-text'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-web-text-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        WebTexts.insert(formData);
        template.hideModal();
    }
});

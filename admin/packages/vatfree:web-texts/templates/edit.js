/* global webTextTemplateData: true */
import stripTags from 'underscore.string/stripTags';
import autosize from 'meteor/vatfree:core-templates/templates/autosize';

Template.webTextEdit.onCreated(function () {

});

Template.webTextEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('web-texts_item', FlowRouter.getParam('itemId'));
        this.subscribe('receipt-rejections-list', '');
        this.subscribe('traveller-rejections', '');
    });
});

Template.webTextEdit.helpers({
    itemDoc() {
        return WebTexts.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    getBreadcrumbTitle() {
        return this.name + ' (' + this.code + ')';
    }
});

Template.webTextEditForm.onCreated(function() {
    this.vatRates = new ReactiveArray();
});

Template.webTextEditForm.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.vatRates.clear();
        if (data.vatRates) {
            let key = 0;
            _.each(data.vatRates, (rate) => {
                this.vatRates.push({
                    key: key++,
                    rate: rate.rate,
                    description: rate.description
                });
            });
        }
    });

    Tracker.afterFlush(() => {
        autosize(this.$('textarea'));
    });
});

Template.webTextEditForm.helpers({
    getTextPreview() {
        let data = Template.instance().data;
        if (data.text && data.text[this.code]) {
            import handlebars from 'handlebars';
            Vatfree.notify.registerHandlebarsHelpers(handlebars);

            let templateData = _.clone(webTextTemplateData);
            templateData.receipt.receiptRejectionIds = _.map(ReceiptRejections.find({}, {fields: {_id: 1}, limit: 3}).fetch(), (rejection) => { return rejection._id; });
            templateData.traveller.rejectionIds = _.map(TravellerRejections.find({}, {fields: {_id: 1}, limit: 3}).fetch(), (rejection) => { return rejection._id; });

            return Vatfree.notify.stripHtmlTags(handlebars.compile(data.text[this.code])(templateData));
        }
    },
    getHTMLPreview() {
        let data = Template.instance().data;
        if (data.text && data.text[this.code]) {
            import handlebars from 'handlebars';
            Vatfree.notify.registerHandlebarsHelpers(handlebars);

            let templateData = _.clone(webTextTemplateData);
            templateData.receipt.receiptRejectionIds = _.map(ReceiptRejections.find({}, {fields: {_id: 1}, limit: 3}).fetch(), (rejection) => { return rejection._id; });
            templateData.traveller.rejectionIds = _.map(TravellerRejections.find({}, {fields: {_id: 1}, limit: 3}).fetch(), (rejection) => { return rejection._id; });

            return handlebars.compile(data.text[this.code])(templateData);
        }
    },
    getTextForLanguage() {
        let data = Template.instance().data;
        if (data.text && data.text[this.code]) {
            return data.text[this.code];
        }
    },
    getWebTextLanguages() {
        return _.contains(['travellers', 'app'], this.group) ? Vatfree.languages.getTravellerLanguages() : Vatfree.languages.getShopLanguages();
    }
});

Template.webTextEditForm.events({
    'click .add-vat-rate'(e, template) {
        e.preventDefault();
        template.vatRates.push({
            key: template.vatRates.length,
            rate: "",
            description: ""
        });
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/web-texts');
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        import handlebars from 'handlebars';
        Vatfree.notify.registerHandlebarsHelpers(handlebars);
        let error = false;
        _.each(formData.text, (webText, language) => {
            try {
                handlebars.compile(webText)({})
            } catch(err) {
                error = err;
            }
        });
        if (error) {
            toastr.error(error.message);
            return false;
        }

        WebTexts.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Web Text saved!')
            }
        });
    }
});

Template.webTextsList.helpers(Vatfree.templateHelpers.helpers);
Template.webTextsList.helpers(webTextHelpers);
Template.webTextsList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/web-text/:itemId', {itemId: this._id});
    }
});

Template.webTextsList.helpers({
    getMissingTranslations() {
        let webText = this;
        let missingTranslations = [];
        let languages = _.contains(['travellers', 'app'], this.group) ? Vatfree.languages.getTravellerLanguages() : Vatfree.languages.getShopLanguages();
        _.each(languages, (language) => {
            if (!webText.text || !webText.text[language.code]) {
                missingTranslations.push(language.code);
            }
        });

        return missingTranslations.join(', ');
    }
});

/* global webTextHelpers: true, webTextTemplateData: true */
import moment from 'moment';

webTextHelpers = {
    getWebTexts() {
        let template = Template.instance();
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
            let searchTerms = searchTerm.split(' ');

            selector['$and'] = [];
            _.each(searchTerms, (s) => {
                selector['$and'].push({textSearch: new RegExp(s)});
            });
        }

        return WebTexts.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    getTextGroupIcon() {
        let icon = "";
        switch (this.group) {
            case "travellers":
                icon = "icon icon-traveller";
                break;
            case "admin":
                icon = "icon icon-fenix";
                break;
            case "app":
                icon = "icon icon-moblie";
                break;
            case "mdwapp":
                icon = "icon-actorvatty";
                break;
            case "partner":
                icon = "icon icon-shop";
                break;
        }

        return icon;
    }
};

webTextTemplateData ={
    language: 'en',
    receipt: {
        receiptNr: 2017001337,
        purchaseDate: moment().subtract(12, 'days').toDate(),
        customsDate: moment().subtract(1, 'days').toDate(),
        currencyId: "NYG8kkBuAzp8ph6Kn",
        currency: {
            name: "Euro",
            code: "EUR",
            symbol: "€"
        },
        receiptRejectionIds: []
    },
    invoice: {
        _id: "invoiceId",
        invoiceNr: 170000125,
        invoiceDate: moment().subtract(1, 'days').toDate(),
        amount: 121000,
        totalVat: 21000,
        secret: "geheim"
    },
    traveller: {
        name: "Joni Smeenk",
        email: "joni@vatfree.com",
        salutation: "Mrs.",
        birthDate: moment('1975-01-01', 'YYYY-MM-DD').toDate(),
        gender: 'female',
        passportNumber: 'A1234567890',
        passportExpirationDate: moment().add(543, 'days').toDate(),
        addressFirst: "Laantje 22",
        postalCode: "1000 AA",
        city: "Loetjebroek",
        state: "Noord Holland",
        country: {
            name: "The Netherlands",
            code: "NL"
        },
        rejectionIds: []
    },
    shop: {
        name: "Daan's wondere winkel van wonderen",
        email: "daan@wonderen.nl",
        addressFirst: "Laantje 22",
        postCode: "1000 AA",
        city: "Loetjebroek",
        state: "Noord Holland",
        country: {
            name: "The Netherlands",
            code: "NL"
        }
    },
    company: {
        name: "Daan's wondere bedrijf van wonderen",
        email: "daan@wonderen-bedrijf.nl",
        addressFirst: "Laantje 23",
        postCode: "1000 AB",
        city: "Loetjebroek",
        state: "Noord Holland",
        country: {
            name: "The Netherlands",
            code: "NL"
        }
    },
    affiliate: {
        name: "Daan the affiliate",
        email: "daan@affiliate.nl",
        addressFirst: "Laantje 42",
        postCode: "1000 AF",
        city: "Loetjebroek",
        state: "Noord Holland",
        country: {
            name: "The Netherlands",
            code: "NL"
        }
    },
    payout: {
        travellerId: "HhnnjCyBzkWZQ6RYF",
        receiptIds: [
            "rBk2TLnzd67iFsfQw",
            "uyoLDrps765WHarh3"
        ],
        amount: 16133,
        currencyId: "NYG8kkBuAzp8ph6Kn",
        paymentMethod: "sepa",
        paymentNr: "NL19INGB0656678232",
        payoutNr: "P180000303",
        status: "new",
        createdAt: new Date(1517233027958),
        createdWeek: 201805,
        createdMonth: 1,
        createdYear: 2018,
        createdBy: "HhnnjCyBzkWZQ6RYF",
        textSearch: "P180000303",
        paidAt: new Date(1517263027958)
    },
    receipts: [
        {
            receiptNr: 2017001337,
            purchaseDate: moment().subtract(12, 'days').toDate(),
            customsDate: moment().subtract(1, 'days').toDate(),
            currencyId: "NYG8kkBuAzp8ph6Kn",
            currency: {
                name: "Euro",
                code: "EUR",
                symbol: "€"
            },
            receiptRejectionIds: []
        },
        {
            receiptNr: 2017001338,
            purchaseDate: moment().subtract(12, 'days').toDate(),
            customsDate: moment().subtract(1, 'days').toDate(),
            currencyId: "NYG8kkBuAzp8ph6Kn",
            currency: {
                name: "Euro",
                code: "EUR",
                symbol: "€"
            },
            receiptRejectionIds: []
        }
    ]
};

UI.registerHelper('getWebTextName', function(webTextId) {
    if (!webTextId) webTextId = this.webTextId;

    let webText = WebTexts.findOne({_id: webTextId});
    if (webText) {
        return webText.name;
    }

    return "";
});

UI.registerHelper('getWebTexts', function(group) {
    let selector = {};
    if (group) {
        selector.group = group;
    }
    return WebTexts.find(selector, {
        sort: {
            name: 1
        }
    });
});

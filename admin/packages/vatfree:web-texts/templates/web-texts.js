Template.webTexts.onCreated(Vatfree.templateHelpers.onCreated(WebTexts, '/web-texts/', '/web-text/:itemId'));
Template.webTexts.onRendered(Vatfree.templateHelpers.onRendered());
Template.webTexts.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.webTexts.helpers(Vatfree.templateHelpers.helpers);
Template.webTexts.helpers(webTextHelpers);
Template.webTexts.events(Vatfree.templateHelpers.events());

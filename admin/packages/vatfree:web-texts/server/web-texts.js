ActivityStream.attachHooks(WebTexts);

WebTexts.after.insert(function(userId, doc) {
    WebTexts.updateTextSearch(doc);
});

WebTexts.after.update(function(userId, doc) {
    WebTexts.updateTextSearch(doc);
});

WebTexts.updateTextSearch = function(doc) {
    let textSearch =  (
        (doc.name || "") + ' ' +
        (doc.code || "") + ' ' +
        (doc.group || "") + ' ' +
        (doc.description || "")
    );

    _.each(doc.text, (text) => {
        textSearch += ' ' + text;
    });

    WebTexts.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: Vatfree.notify.stripHtmlTags(textSearch).toLowerCase().latinize()
        }
    });
};

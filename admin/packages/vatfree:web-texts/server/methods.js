Meteor.methods({
    getWebText(code, language = 'en') {
        check(code, String);

        let webText = WebTexts.findOne({code: code});
        if (webText && webText.text) {
            return webText.text[language] || webText.text['en'] || "";
        } else {
            return "";
        }
    }
});

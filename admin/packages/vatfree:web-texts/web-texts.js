WebTexts = new Meteor.Collection('web-texts');
WebTexts.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Name of the web text",
        optional: false
    },
    description: {
        type: String,
        label: "Description of this web text",
        optional: true
    },
    group: {
        type: String,
        label: "The group this web text is a part of",
        allowedValues: [
            'admin',
            'travellers',
            'app',
            'mdwapp',
            'partner'
        ],
        defaultValue: 'travellers'
    },
    code: {
        type: String,
        label: "The reference code in the app",
        optional: false
    },
    text: {
        type: Object,
        label: "All the translations for this text",
        optional: true,
        blackbox: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
WebTexts.attachSchema(CreatedUpdatedSchema);

WebTexts.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

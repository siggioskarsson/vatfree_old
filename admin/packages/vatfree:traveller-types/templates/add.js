Template.addTravellerTypeModal.onCreated(function() {
    let template = this;
    let parentVariable = this.data.parentVariable || 'addingItem';

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-traveller-type').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-traveller-type')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-add-traveller-type').on('shown.bs.modal', function () {
            //
        });
        $('#modal-add-traveller-type').modal('show');
    });
});

Template.addTravellerTypeModal.events({
    'click .cancel-add-item'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-traveller-type-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        TravellerTypes.insert(formData);
        template.hideModal();
    }
});

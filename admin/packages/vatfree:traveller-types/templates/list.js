Template.travellerTypesList.helpers(Vatfree.templateHelpers.helpers);
Template.travellerTypesList.helpers(travellerTypeHelpers);
Template.travellerTypesList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/traveller-type/:itemId', {itemId: this._id});
    }
});

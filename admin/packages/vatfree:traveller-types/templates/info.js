Template.travellerTypeInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.travellerTypeInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.travellerTypeInfo.helpers(travellerTypeHelpers);
Template.travellerTypeInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

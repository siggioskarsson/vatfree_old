UI.registerHelper('getTravellerTypeOptions', function() {
    return TravellerTypes.find({},{
        sort: {
            name: 1
        }
    });
});

UI.registerHelper('getTravellerTypeIcon', function(travellerTypeId) {
    travellerTypeId = travellerTypeId || (this.private ? this.private.travellerTypeId : false);
    if (travellerTypeId === 'nato') {
        return "icon icon-knobnato";
    } else if (travellerTypeId) {
        let travellerType = TravellerTypes.findOne({
            _id: travellerTypeId
        });
        if (travellerType) {
            return "icon " + (travellerType.icon || "icon-traveller");
        }
    }

    return "icon icon-traveller";
});

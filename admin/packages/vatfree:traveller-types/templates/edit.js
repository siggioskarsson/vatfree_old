Template.travellerTypeEdit.onCreated(function () {

});

Template.travellerTypeEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('traveller-types_item', FlowRouter.getParam('itemId'));
    });
});

Template.travellerTypeEdit.helpers({
    itemDoc() {
        return TravellerTypes.findOne({
            _id: FlowRouter.getParam('itemId')
        }, {
            transform: false
        });
    },
    getBreadcrumbTitle() {
        return this.name;
    }
});

Template.travellerTypeEditForm.onCreated(function() {
    this.translations = [
        {
            name: "Name",
            key: 'name'
        },
        {
            name: "Certificate name",
            key: "certificateName"
        },
        {
            name: "Requirements text",
            key: "receiptRequirementsText"
        },
        {
            name: "Register receipt text",
            key: "registerReceiptText"
        }
    ];
});

Template.travellerTypeEditForm.onRendered(function() {
    this.autorun(() => {
        this.subscribe('receipt-types');
    });
    Tracker.afterFlush(() => {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square icheckbox_square-blue',
            radioClass: 'iradio_square iradio_square-blue'
        });
    });
});

Template.travellerTypeEditForm.helpers(Vatfree.templateHelpers.helpers);
Template.travellerTypeEditForm.helpers({
});

Template.travellerTypeEditForm.events({
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/traveller-types');
    },
    'submit form[name="item-edit-form"], submit form[name="item-edit-translations-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template, $(e.currentTarget));

        TravellerTypes.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Traveller Type saved!')
            }
        });
    }
});

Template.travellerTypes.onCreated(Vatfree.templateHelpers.onCreated(TravellerTypes, '/traveller-types', '/traveller-type/:itemId'));
Template.travellerTypes.onRendered(Vatfree.templateHelpers.onRendered());
Template.travellerTypes.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.travellerTypes.helpers(Vatfree.templateHelpers.helpers);
Template.travellerTypes.helpers(travellerTypeHelpers);
Template.travellerTypes.events(Vatfree.templateHelpers.events());

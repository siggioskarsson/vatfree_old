Meteor.publish('traveller-types', function(searchTerm, selector, sort, limit, offset) {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, ['admin', 'vatfree', 'traveller'], Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    sort = sort || {_id: 1};
    limit = limit || 100;
    offset = offset || 0;

    selector = selector || {};

    if (searchTerm) {
        searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
        let searchTerms = searchTerm.split(' ');

        selector['$and'] = [];
        _.each(searchTerms, (s) => {
            selector['$and'].push({textSearch: new RegExp(s)});
        });
    }

    return TravellerTypes.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset
    });
});

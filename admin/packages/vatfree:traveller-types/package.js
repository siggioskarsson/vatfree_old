Package.describe({
    name: 'vatfree:traveller-types',
    summary: 'Vatfree traveller-types package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'hermanitos:activity-stream@0.0.1',
        'tap:i18n',
        'tap:i18n-db',
        'vatfree:core'
    ]);

    // shared files
    api.addFiles([
        'traveller-types.js',
        'lib/traveller-types.js'
    ]);

    // server files
    api.addFiles([
        'server/traveller-types.js',
        'publish/traveller-types.js',
        'publish/traveller-types-i18n.js',
        'publish/traveller-type.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/traveller-types.html',
        'templates/traveller-types.js'
    ], 'client');

    api.export([
        'TravellerTypes'
    ]);
});

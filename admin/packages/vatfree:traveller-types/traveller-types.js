TravellerTypes = new TAPi18n.Collection('traveller-types');
TravellerTypes.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Name of the traveller type",
        optional: false
    },
    documentsNeeded: {
        type: Object,
        label: "A description of the documents needed for this TravellerType",
        optional: true,
        blackbox: true
    },
    isNato: {
        type: Boolean,
        label: "Special type for NATO clients",
        autoValue: function () {
            if (this.field('isNato').isSet) {
                return !!this.field('isNato').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    subType: {
        type: "String",
        label: "The special type of traveller",
        allowedValues: [
            'nato',
            'cargo'
        ],
        optional: true
    },
    hidden: {
        type: Boolean,
        label: "Whether this type is hidden on the traveller site, but visible for admins to select for the traveller",
        autoValue: function () {
            if (this.field('hidden').isSet) {
                return !!this.field('hidden').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    forceReceiptTypeId: {
        type: String,
        label: "Which receipt type belongs to this traveller type",
        optional: true
    },
    certificateName: {
        type: String,
        label: "The name of the certificate need together with the receipts",
        optional: true
    },
    receiptRequirementsText: {
        type: String,
        label: "A short explanation about what we need from the traveller for processing receipts",
        optional: true
    },
    registerReceiptText: {
        type: String,
        label: "A short explanation about what we need for processing receipts",
        optional: true
    },
    icon: {
        type: String,
        label: "Icon to use for traveller type",
        optional: true
    },
    notes: {
        type: String,
        label: "Notes on this traveller type",
        optional: true
    },
    textSearch: {
        type: String,
        autoValue: function() {
            if (this.field("name").value) {
                return ((this.field("name").value || "") + ' ' + (this.field("code").value || "")).toLowerCase().latinize();
            } else {
                this.unset();
            }
        },
        optional: true
    }
}));
TravellerTypes.attachSchema(CreatedUpdatedSchema);
TravellerTypes.attachSchema(TranslatableSchema);

TravellerTypes.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

if (Meteor.isServer) {
    Meteor.startup(() => {
        if (TravellerTypes.find().count() === 0) {
            TravellerTypes.insert({
                name: "Traveller",
                documentsNeeded: {
                    passport: {
                        name: "Passport",
                        required: true
                    },
                    visum: {
                        name: "Visum",
                        required: false
                    }
                }
            });
            TravellerTypes.insert({
                name: "NATO Brunsum",
                documentsNeeded: {
                    passport: {
                        name: "Passport",
                        required: true
                    }
                }
            });
            TravellerTypes.insert({
                name: "NATO Geilenkirchen",
                documentsNeeded: {
                    passport: {
                        name: "Passport",
                        required: true
                    }
                }
            });
        }
    });
}

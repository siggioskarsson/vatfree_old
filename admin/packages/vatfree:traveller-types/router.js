FlowRouter.route('/traveller-types', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "travellerTypes"});
    }
});

FlowRouter.route('/traveller-type/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "travellerTypeEdit"});
    }
});

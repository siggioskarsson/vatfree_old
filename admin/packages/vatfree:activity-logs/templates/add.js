import moment from 'moment';

Template.addActivityLogModal.onCreated(function() {
    let template = this;
    let parentVariable = this.data.parentVariable || 'addingItem';

    this.ignoreShop = new ReactiveVar();
    this.ignoreCompany = new ReactiveVar();
    this.ignoreUser = new ReactiveVar();

    this.autorun(() => {
        let data = Template.currentData();
        this.ignoreShop.set(data.shopId === false);
        this.ignoreCompany.set(data.companyId === false);
        this.ignoreUser.set(data.userId === false);
    });

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-activity-log').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-activity-log')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-add-activity-log').on('shown.bs.modal', function () {
            template.$('select[name="countryId"]').select2();
            template.$('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: TAPi18n.__('_date_format').toLowerCase()
            });
            Vatfree.templateHelpers.select2Search.call(template, 'search-users', 'userId');
            Vatfree.templateHelpers.select2Search.call(template, 'search-contacts', 'contactId');
            Vatfree.templateHelpers.select2Search.call(template, 'search-shops', 'shopId');
            Vatfree.templateHelpers.select2Search.call(template, 'search-companies', 'companyId');
        });
        $('#modal-add-activity-log').modal('show');
    });
});

Template.addActivityLogModal.helpers({
    ignoreShop() {
        return Template.instance().ignoreShop.get();
    },
    ignoreCompany() {
        return Template.instance().ignoreCompany.get();
    },
    ignoreUser() {
        return Template.instance().ignoreUser.get();
    }
});

Template.addActivityLogModal.events({
    'click .cancel-add-activity-log'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-activity-log-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (_.has(formData, 'due')) {
            if (formData.due) {
                formData.due = moment(formData.due, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.due = null;
            }
        }

        ActivityLogs.insert(formData);
        template.hideModal();
    }
});

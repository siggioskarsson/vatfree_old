Template.activityLogTab.onCreated(function() {
    this.addingActivityLog = new ReactiveVar();
    this.allowAdd = new ReactiveVar();
});

Template.activityLogTab.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        if (_.isUndefined(data.allowAdd)) {
            this.allowAdd.set(true);
        } else {
            this.allowAdd.set(data.allowAdd);
        }
    });
});

Template.activityLogTab.helpers({
    allowAdd() {
        return Template.instance().allowAdd.get();
    },
    addingActivityLog() {
        return Template.instance().addingActivityLog.get();
    }
});

Template.activityLogTab.events({
    'click .add-activity-log'(e, template) {
        e.preventDefault();
        template.addingActivityLog.set(true);
    }
});

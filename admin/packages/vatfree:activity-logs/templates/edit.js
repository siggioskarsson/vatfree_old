import moment from 'moment';

Template.ActivityLogEdit.onCreated(function () {

});

Template.ActivityLogEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('activity-logs_item', FlowRouter.getParam('itemId'));
    });
});

Template.ActivityLogEdit.helpers({
    itemDoc() {
        return ActivityLogs.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    showBreadcrumb() {
        return this.ancestors && this.ancestors.length > 1;
    }
});

Template.activityLogEditForm.onCreated(function() {
    this.shopId = new ReactiveVar(this.data.shopId);
    this.companyId = new ReactiveVar(this.data.companyId);
    this.vatRates = new ReactiveArray();
    this.handleFileUpload = function (file) {
        Vatfree.templateHelpers.handleFileUpload(file, {
            itemId: FlowRouter.getParam('itemId'),
            target: 'activity-logs'
        });
    };
});

Template.activityLogEditForm.onRendered(function() {
    this.autorun(() => {
        this.subscribe('shops_item', this.shopId.get() || "");
    });
    this.autorun(() => {
        this.subscribe('companies_item', this.companyId.get() || "");
    });

    this.autorun(() => {
        let data = Template.currentData();
        let shopId = this.shopId.get();
        let companyId = this.companyId.get();
        if (this.subscriptionsReady()) {
            Tracker.afterFlush(() => {
                this.$('select[name="countryId"]').select2();
                Vatfree.templateHelpers.initDatepicker.call(this);
                Vatfree.templateHelpers.select2Search.call(this, 'search-users', 'userId', false, true);
                Vatfree.templateHelpers.select2Search.call(this, 'search-contacts', 'contactId', false, true);
                Vatfree.templateHelpers.select2Search.call(this, 'search-shops', 'shopId', false, true);
                Vatfree.templateHelpers.select2Search.call(this, 'search-companies', 'companyId', false, true);
            });
        }
    });
});

Template.activityLogEditForm.helpers(activityLogHelpers);
Template.activityLogEditForm.helpers({
    isDisabled() {
        return this.status === 'done';
    },
    printError() {
        return JSON.stringify(this.error, null, 2);
    },
    getCleanHtml() {
        let match = this.html ? this.html.match(/<body[^>]*>([\w|\W]*)<\/body>/im) : false;
        if (match && match[1]) {
            let html = match[1];
            html = html.replace(/(<style>[\w|\W]*<\/style>)/im, '');
            return html;
        }

        return "";
    },
    getFiles() {
        let cursor = Files.find({
            _id: {
                $in: this.fileIds || []
            }
        });

        return cursor.count() ? cursor : false;
    }
});

Template.activityLogEditForm.events(Vatfree.templateHelpers.events());
Template.activityLogEditForm.events({
    'change select[name="shopId"]'(e, template) {
        let shopId = $(e.currentTarget).val();
        template.shopId.set(shopId);
    },
    'change select[name="companyId"]'(e, template) {
        let companyId = $(e.currentTarget).val();
        template.companyId.set(companyId);
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/activity-logs');
    },
    'click .reopen-activity'(e) {
        e.preventDefault();
        ActivityLogs.update({
            _id: this._id
        },{
            $set: {
                status: 'doing'
            }
        });
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template, $(e.currentTarget));

        if (_.has(formData, 'due')) {
            if (formData.due) {
                formData.due = moment(formData.due, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.due = null;
            }
        }

        let setData = {
            $set: formData
        };

        if (!formData.shopId) {
            if (!setData['$unset']) setData['$unset'] = {};
            setData['$unset'].shopId = 1;
        }
        if (!formData.companyId) {
            if (!setData['$unset']) setData['$unset'] = {};
            setData['$unset'].companyId = 1;
        }

        console.log(setData);
        ActivityLogs.update({
            _id: this._id
        }, setData, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('ActivityLog saved!')
            }
        });
    }
});

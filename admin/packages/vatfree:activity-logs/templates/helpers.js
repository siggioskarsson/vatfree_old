/* global activityLogHelpers: true */
import moment from 'moment';

activityLogHelpers = {
    getActivityLogsSelector(template, useTextIndex = false) {
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector, 'textSearch', useTextIndex);
        }
        Vatfree.search.addStatusFilter.call(template, selector);
        Vatfree.search.addListFilter.call(template, selector);
        template.selectorFunction(selector);

        return selector;
    },
    getActivityLogs() {
        let template = Template.instance();
        let selector = activityLogHelpers.getActivityLogsSelector(template);

        return ActivityLogs.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    isOverdue() {
        if (this.due) {
            return moment(this.due).isBefore(moment());
        }
    },
    getDataArray() {
        const data = [];
        _.each(this.data, (value, key) => {
            data.push({
                key,
                value
            });
        });
        return data;
    },
    getDataValue(value) {
        if (_.isBoolean(value)) {
            return value ? 'Yes': 'No'
        } else if (_.isObject(value)) {
            return JSON.stringify(value);
        } else if (_.isArray(value)) {
            return value.join(', ');
        }

        return value;
    }
};

Template.activityLogInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.activityLogInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('shops_item', data.shopId || "");
        this.subscribe('companies_item', data.companyId || "");
    });
});

Template.activityLogInfo.helpers(activityLogHelpers);
Template.activityLogInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    },
    getTraveller() {
        return Travellers.findOne({_id: this.travellerId});
    },
    getShop() {
        return Shops.findOne({_id: this.shopId});
    },
    getFiles() {
        let cursor = Files.find({
            _id: {
                $in: this.fileIds || []
            }
        });

        return cursor.count() ? cursor : false;
    },
    getHierarchyCompanyId() {
        if (this.companyId) {
            return this.companyId;
        } else if (this.shopId) {
            const shop = Shops.findOne({_id: this.shopId});
            if (shop) {
                return shop.companyId || shop.billingCompanyId;
            }
        }

        return false;
    }
});

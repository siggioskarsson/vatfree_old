UI.registerHelper('getActivityLogDescription', function(activityLogId) {
    if (!activityLogId) activityLogId = this.activityLogId;

    let activityLog = ActivityLogs.findOne({_id: activityLogId});
    if (activityLog) {
        let country = Countries.findOne({_id: activityLog.countryId}) || {};
        return activityLog.name + ' - ' + activityLog.addressFirst + ', ' + activityLog.postalCode + ' ' + activityLog.city + ', ' + country.name;
    }

    return "";
});

UI.registerHelper('getActivityLogSubject', function(activityLogId) {
    if (!activityLogId) activityLogId = this.activityLogId;

    let activityLog = ActivityLogs.findOne({_id: activityLogId});
    if (activityLog) {
        return activityLog.subject;
    }

    return "";
});

UI.registerHelper('getActivityLogTypeOptions', function() {
    return _.clone(ActivityLogs.simpleSchema()._schema.type.allowedValues);
});

UI.registerHelper('getActivityLogStatusOptions', function() {
    return _.clone(ActivityLogs.simpleSchema()._schema.status.allowedValues);
});

UI.registerHelper('getActivityLogStatusIcon', function(icon) {
    let icons = {
        'new': 'icon icon-tasknew',
        'doing': 'icon icon-tasksdoing',
        'done': 'icon icon-tasksdone'
    };

    return icons[icon];
});

UI.registerHelper('getActivityLogTypeIcon', function(type) {
    if (!type) type = this.type;
    switch (type) {
        case 'in-call':
            return "icon icon-phoneIn";
        case 'out-call':
            return "icon icon-phoneOut";
        case 'email':
            return "icon icon-email";
        case 'push':
            return "icon icon-push";
        case 'in-person':
            return "icon icon-chinperson";
        case 'task':
            return "icon icon-task";
        case 'burny':
            return "icon icon-actorbot";
        case 'event':
            return "icon icon-event";
        case 'post-in':
            return "icon icon-chmail";
        case 'shop-visit':
            return "icon icon-refshop";
        default:
            return "icon icon-unknown";
    }
});

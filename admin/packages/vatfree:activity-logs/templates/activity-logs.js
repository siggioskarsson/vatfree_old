Template.ActivityLogs.onCreated(Vatfree.templateHelpers.onCreated(ActivityLogs, '/activity-logs/', '/activity-log/:itemId'));
Template.ActivityLogs.onRendered(Vatfree.templateHelpers.onRendered());
Template.ActivityLogs.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.ActivityLogs.helpers(Vatfree.templateHelpers.helpers);
Template.ActivityLogs.helpers(activityLogHelpers);
Template.ActivityLogs.events(Vatfree.templateHelpers.events());

Template.ActivityLogs.onCreated(function() {
    let template = this;
    this.myTasksFilter = new ReactiveVar(Session.get(this.view.name + '.myTasksFilter'));

    if (!this.sort.get()) {
        this.sort.set({createdAt: -1});
    }
    this.todoStatus = [
        'new',
        'doing'
    ];

    this.typeFilter = new ReactiveArray();
    _.each(Session.get(this.view.name + '.typeFilter') || [], (type) => {
        this.typeFilter.push(type);
    });
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.typeFilter', this.typeFilter.array());
    });

    this.entityFilter = new ReactiveArray();
    _.each(Session.get(this.view.name + '.entityFilter') || [], (entity) => {
        this.entityFilter.push(entity);
    });
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.entityFilter', this.entityFilter.array());
    });

    this.selectorFunction = function(selector) {
        const myTasksFilter = this.myTasksFilter.get();
        if (myTasksFilter === 'active') {
            selector.userId = Meteor.userId()
        }
        if (myTasksFilter === 'inactive') {
            selector.userId = {
                $exists: false
            }
        }

        let typeFilter = this.typeFilter.array() || [];
        if (typeFilter.length > 0) {
            selector.type = {
                $in: typeFilter
            };
        }
        let entityFilter = this.entityFilter.array() || [];
        if (entityFilter.length > 0) {
            _.each(entityFilter, (entityId) => {
                selector[entityId] = {
                    $exists: true
                };
            });
        }
    };

    this.autorun(() => {
        Session.setPersistent(this.view.name + '.myTasksFilter', this.myTasksFilter.get());
    });
});

Template.ActivityLogs.onRendered(function() {
    this.autorun(() => {
        if (FlowRouter.getQueryParam('tasks')) {
            Tracker.afterFlush(() => {
                this.statusFilter.clear();
                _.each(this.todoStatus, (status) => {
                    this.statusFilter.push(status);
                });
            });
        }
    });
});

Template.ActivityLogs.helpers({
    isActiveLogTypeFilter() {
        let typeFilter = Template.instance().typeFilter.array() || [];
        return _.contains(typeFilter, this.toString()) ? 'active' : '';
    },
    isActiveMyTasksFilter() {
        return Template.instance().myTasksFilter.get() || '';
    },
    isActiveLogEntityFilter(entity) {
        let entityFilter = Template.instance().entityFilter.array() || [];
        return _.contains(entityFilter, entity) ? 'active' : '';
    },
    getActivityLogsStats() {
        let template = Template.instance();
        let selector = activityLogHelpers.getActivityLogsSelector(template, true);

        let stats = ReactiveMethod.call('get-activity-logs-stats', selector);
        return stats && stats[0] ? stats[0].value : false;
    }
});

Template.ActivityLogs.events({
    'click .my-tasks-filter-icon'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        const myTasksFilter = template.myTasksFilter.get();
        if (myTasksFilter === 'active') {
            template.myTasksFilter.set('inactive');
        } else if (myTasksFilter === 'inactive') {
            template.myTasksFilter.set(false);
        } else {
            template.myTasksFilter.set('active');
        }
    },
    'click .filter-icon.type-filter-icon'(e, template) {
        e.preventDefault();
        let type = this.toString();
        let typeFilter = template.typeFilter.array();

        if (_.contains(typeFilter, type)) {
            template.typeFilter.clear();
            _.each(typeFilter, (_type) => {
                if (type !== _type) {
                    template.typeFilter.push(_type);
                }
            });
        } else {
            template.typeFilter.push(type);
        }
    },
    'click .filter-icon.entity-filter-icon'(e, template) {
        e.preventDefault();
        let entity = $(e.currentTarget).attr('id');
        let entityFilter = template.entityFilter.array();

        if (_.contains(entityFilter, entity)) {
            template.entityFilter.clear();
            _.each(entityFilter, (_type) => {
                if (entity !== _type) {
                    template.entityFilter.push(_type);
                }
            });
        } else {
            template.entityFilter.push(entity);
        }
    }
});

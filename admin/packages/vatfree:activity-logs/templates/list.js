Template.activityLogsList.helpers(Vatfree.templateHelpers.helpers);
Template.activityLogsList.helpers(activityLogHelpers);
Template.activityLogsList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/activity-log/:itemId', {itemId: this._id});
    }
});

Template.activityLogsList.helpers({
    allowResendEmail() {
        return this.type === 'email' && this.travellerId && Vatfree.userIsInRole(Meteor.userId(), 'travellers-update');
    },
    isTask() {
        return this.type === 'task';
    },
    assignedToMe() {
        return this.userId === Meteor.userId();
    },
    unassigned() {
        return !this.userId;
    },
    notDone() {
        return this.status !== "done";
    }
});

Template.activityLogsList.events({
    'click .resend-email'(e, template) {
        e.preventDefault();
        let self = this;
        let traveller = Travellers.findOne({
            _id: this.travellerId
        });
        import swal from 'sweetalert';
        swal({
            title: "Resend email to traveller?",
            text: "Email: " + traveller.profile.email + "\n\n" + self.subject,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, resend!",
            closeOnConfirm: false
        }, function () {
            Meteor.call('resend-activity-log-email', self._id, self.travellerId, (err) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                }
            });
            swal("Sent!", "Email has been sent to " + traveller.profile.email, "success");
        });
    },
    'click .mark-done'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        ActivityLogs.update({
            _id: this._id
        },{
            $set: {
                status: "done"
            }
        });
    },
    'click .assign-self'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        ActivityLogs.update({
            _id: this._id
        },{
            $set: {
                userId: Meteor.userId(),
                status: 'doing'
            }
        });
    }
});

Template.activityLogPreviewHtml.onRendered(function () {
    this.autorun(() => {
        console.log('sub', FlowRouter.getParam('itemId'));
        this.subscribe('activity-logs_item', FlowRouter.getParam('itemId'));
    });
});

Template.activityLogPreviewHtml.helpers({
    getHTMLPreview() {
        let activityLog = ActivityLogs.findOne({
            _id: FlowRouter.getParam('itemId')
        }) || {};

        return activityLog.html;
    }
});

Meteor.publishComposite('activity-logs_item', function (itemId) {
    check(itemId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    let activityLogItem = ActivityLogs.findOne({
        _id: itemId
    });

    let children = [
        {
            find: function (item) {
                return Files.find({
                    itemId: item._id,
                    target: 'activity-logs'
                });
            }
        }
    ];

    if (activityLogItem.shopId) {
        children.push({
            find: function (item) {
                return Shops.find({
                    _id: item.shopId
                });
            }
        });
    }

    if (activityLogItem.companyId) {
        children.push({
            find: function (item) {
                return Companies.find({
                    _id: item.companyId
                });
            }
        });
    }

    if (activityLogItem.contactId) {
        children.push({
            collectionName: 'contacts',
            find: function (item) {
                return Contacts.find({
                    _id: item.contactId
                });
            }
        });
    }

    if (activityLogItem.affiliateId) {
        children.push({
            collectionName: "affiliates",
            find: function (item) {
                return Affiliates.find({
                    _id: item.affiliateId
                });
            }
        });
    }

    if (activityLogItem.invoiceId) {
        children.push({
            find: function (item) {
                return Invoices.find({
                    _id: item.invoiceId
                });
            }
        });
    }

    if (activityLogItem.paymentId) {
        children.push({
            find: function (item) {
                return Payments.find({
                    _id: item.paymentId
                });
            }
        });
    }

    if (activityLogItem.receiptId) {
        children.push({
            find: function (item) {
                return Receipts.find({
                    _id: item.receiptId
                });
            }
        });
    }

    if (activityLogItem.travellerId) {
        children.push({
            collectionName: 'travellers',
            find: function (item) {
                return Meteor.users.find({
                    _id: item.travellerId
                });
            }
        });
    }

    if (activityLogItem.payoutId) {
        children.push({
            find: function (item) {
                return Payouts.find({
                    _id: item.payoutId
                });
            }
        });
    }

    if (activityLogItem.fileIds) {
        children.push({
            find: function (item) {
                return Files.find({
                    _id: {
                        $in: item.fileIds || []
                    }
                });
            }
        });
    }

    return {
        find: function () {
            return ActivityLogs.find({
                _id: itemId
            });
        },
        children: children
    }
});

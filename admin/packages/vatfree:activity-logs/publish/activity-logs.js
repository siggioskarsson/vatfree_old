Meteor.publish('activity-logs', function(searchTerm, selector, sort, limit, offset, includeNotes = false) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
            selector.travellerId = {$exists: false};
        }
        if (!Vatfree.userIsInRole(this.userId, 'shops-read')) {
            selector.shopId = {$exists: false};
        }
        if (!Vatfree.userIsInRole(this.userId, 'receipts-read')) {
            selector.receiptId = {$exists: false};
        }
        if (!Vatfree.userIsInRole(this.userId, 'payments-read')) {
            selector.paymentId = {$exists: false};
        }
        if (!Vatfree.userIsInRole(this.userId, 'payouts-read')) {
            selector.payoutId = {$exists: false};
        }
    }

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector, 'textSearch', true);
    }

    let fields = {
        notes: false,
        html: false
    };
    if (includeNotes) {
        delete fields.notes;
    }

    return ActivityLogs.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset,
        fields: fields
    });
});

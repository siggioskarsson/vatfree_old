FlowRouter.route('/activity-logs', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "ActivityLogs"});
    }
});

FlowRouter.route('/activity-log/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "ActivityLogEdit"});
    }
});

FlowRouter.route('/activity-log/:itemId/preview-html', {
    action: function() {
        BlazeLayout.render("blankLayout", {content: "activityLogPreviewHtml"});
    }
});

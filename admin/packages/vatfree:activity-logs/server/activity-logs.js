try { ActivityLogs._dropIndex('textSearch_text'); } catch(e) {};
try {
    ActivityLogs._ensureIndex({'createdAt': -1});
    //ActivityLogs._ensureIndex({'textSearch': 1});
    //ActivityLogs._ensureIndex({'textSearch': "text"}, {"language_override": "_text_language"});
} catch(e) {
    console.error(e);
}

ActivityStream.attachHooks(ActivityLogs);

const sendSlackUpdate = function(activityLog) {
    const user = Meteor.users.findOne({_id: activityLog.userId});
    if (user && user.profile && user.profile.slackId) {
        const text = `An activity has been assigned to you: "${activityLog.subject}"\nhttps://admin.vatfree.com/activity-log/${activityLog._id}`;
        Vatfree.adminActivityLogNotifySlack(text, user.profile.slackId);
    }
};

ActivityLogs.before.insert(function(userId, doc) {
    if (doc.type === 'post-in') {
        doc.status = 'done';
    }
});

ActivityLogs.after.insert(function(userId, doc) {
    Meteor.defer(() => {
        ActivityLogs.updateTextIndex(doc);
        if (doc.userId && doc.createdBy !== doc.userId) {
            sendSlackUpdate(doc);
        }
    });
});

ActivityLogs.after.update(function(userId, doc) {
    Meteor.defer(() => {
        ActivityLogs.updateTextIndex(doc);
        if (doc.userId && this.previous.userId !== doc.userId && doc.updatedBy !== doc.userId) {
            sendSlackUpdate(doc);
        }
    });
});

ActivityLogs.updateTextIndex = function(doc) {
    let t = ((doc.subject || "") + ' ' + (doc.notes || "")).toLowerCase().latinize();
    t = _.uniq(t.replace(/\s+/g, ' ').replace(/(https?:\/\/[^ "]+)/g, '').split(' ')).join(' ').substr(0, 500);

    ActivityLogs.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: t
        }
    });
};

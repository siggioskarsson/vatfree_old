/* global Vatfree: true */
const csv = require('csvtojson');
const DEBUGGING = true;

const colMapping = {
    'ID': 'salesforceId',
    'WHOID': 'contactId',
    'WHATID': 'linkId',
    'SUBJECT': 'subject',
    'ACTIVITYDATE': 'date',
    'STATUS': 'status',
    'OWNERID': 'userId',
    'DESCRIPTION': 'notes',
    'TYPE': 'subtype',
    'ISDELETED': 'deleted',
    'ACCOUNTID': 'shopOrCompany',
    'ISCLOSED': 'closed',
    'CREATEDDATE': 'createdAt',
    'CREATEDBYID': 'createdBy',
    'LASTMODIFIEDDATE': 'updatedAt',
    'LASTMODIFIEDBYID': 'updatedBy',
    'SYSTEMMODSTAMP': false,
    'ISARCHIVED': false,
    'REMINDERDATETIME': 'due',
    'TASKSUBTYPE': 'type'
};

const convert = {
    'date': 'date',
    'createdAt': 'date',
    'updatedAt': 'date',
    'due': 'date'
};

const cleanupUpdateData = function (updateData, links) {
    let link = links[updateData['linkId'].substr(0, 15)];
    if (!link) {
        console.error('Could not find parent item for', updateData['salesforceId'], updateData['linkId']);
        return false;
    }

    switch(updateData['status']) {
        case 'Not Started':
            updateData['status'] = 'todo';
            break;
        case 'Noted':
            updateData['status'] = 'done';
            break;
        case 'Delegated':
            updateData['status'] = 'doing';
            break;
        case 'Trying to reach':
            updateData['status'] = 'doing';
            break;
        case 'Waiting on someone else':
            updateData['status'] = 'doing';
            break;
        case 'Completed':
            updateData['status'] = 'done';
            break;
        case 'Overruled':
            updateData['status'] = 'done';
            break;
        case 'Failed':
            updateData['status'] = 'done';
            break;
    }
    if (updateData['closed'] === "true") {
        updateData['status'] = 'done';
        delete updateData['closed'];
    }

    delete updateData['subtype'];

    switch(updateData['type']) {
        case 'Task':
            updateData['type'] = 'task';
            break;
        case 'Call':
            updateData['type'] = 'out-call';
            break;
        case 'Email':
            updateData['type'] = 'email';
            break;
    }

    switch(link.type) {
        case 'travellers':
            updateData['travellerId'] = link._id;
            break;
        case 'receipts':
            updateData['receiptId'] = link._id;
            break;
        case 'invoices':
            updateData['invoiceId'] = link._id;
            break;
        case 'shops':
            updateData['shopId'] = link._id;
            break;
        case 'companies':
            updateData['companyId'] = link._id;
            break;
        case 'contacts':
            updateData['contactId'] = link._id;
            break;
    }

    updateData['userId'] = (links[updateData['userId']] || {})._id;
    updateData['createdBy'] = (links[updateData['createdBy']] || {})._id;
    updateData['updatedBy'] = (links[updateData['updatedBy']] || {})._id;

    if (updateData['deleted'] === "true") {
        updateData['deleted'] = true;
    } else {
        delete updateData['deleted'];
    }

    if (updateData['contactId']) updateData['contactId'] = (links[updateData['contactId']] || {})._id;

    if (updateData['shopOrCompany']) {
        let element = links[updateData['shopOrCompany']];
        if (element) {
            if (element.type === 'shops') {
                updateData['shopId'] = element._id
            } else if (element.type === 'companies') {
                updateData['companyId'] = element._id
            }
        }
        delete updateData['shopOrCompany'];
    }

    return updateData;
};
const saveActivity = function (updateData) {
    let activity = ActivityLogs.findOne({
        'salesforceId': updateData['salesforceId']
    });

    if (activity) {
        // do nothing, already imported
        return activity._id;
    } else {
       return ActivityLogs.direct.insert(updateData);
    }
};

Vatfree['import-activity-logs'] = function (importFile, fileObject, callback) {
    Vatfree.setImportFileProgress(10, 'Populating lookup data', fileObject);

    // traveller / receipt / shop / company / invoice / contacts ?
    const links = {};
    Meteor.users.find({'profile.contactId': {$exists: true}},{fields: {'profile.contactId': 1}}).forEach((item) => {
        links[item.profile.contactId] = {
            _id: item._id,
            type: 'travellers'
        };
    });
    Receipts.find({salesforceId: {$exists: true}},{fields: {salesforceId: 1}}).forEach((item) => {
        links[item.salesforceId] = {
            _id: item._id,
            type: 'receipts'
        };
    });
    Invoices.find({salesforceId: {$exists: true}},{fields: {salesforceId: 1}}).forEach((item) => {
        links[item.salesforceId] = {
            _id: item._id,
            type: 'invoices'
        };
    });
    Shops.find({id: {$exists: true}},{fields: {id: 1}}).forEach((item) => {
        links[item.id] = {
            _id: item._id,
            type: 'shops'
        };
    });
    Companies.find({id: {$exists: true}},{fields: {id: 1}}).forEach((item) => {
        links[item.id] = {
            _id: item._id,
            type: 'companies'
        };
    });
    Contacts.find({'profile.contactId': {$exists: true}},{fields: {'profile.contactId': 1}}).forEach((item) => {
        links[item.profile.contactId] = {
            _id: item._id,
            type: 'contacts'
        };
    });

    let totalLines = Vatfree.importFileLines(importFile);
    let lines = 0;
    Vatfree.setImportFileProgress(12, 'Initializing csv', fileObject);
    if (DEBUGGING) console.log('Starting csv import for', importFile);
    csv({
        delimiter: ',',
        noheader: false,
        trim: true,
        flatKeys: true,
        constructResult: false
    }).fromFile(importFile)
        .on('json', Meteor.bindEnvironment((row) => {
            lines++;
            if (row && _.size(row) > 2) {
                if (lines % 100 === 0) {
                    console.log('progress', 12 + Math.round(89 * (lines / totalLines)), 'Importing ' + lines + ' of ' + totalLines);
                    Vatfree.setImportFileProgress(12 + Math.round(89 * (lines / totalLines)), 'Importing ' + lines + ' of ' + totalLines, fileObject);
                }

                let updateData = Vatfree.import.processColumns(colMapping, convert, row);
                updateData = cleanupUpdateData(updateData, links);
                saveActivity(updateData);
            }
        }))
        .on('done', Meteor.bindEnvironment((error) => {
            if (DEBUGGING) console.log('CSV import done', importFile);
            Vatfree.setImportFileProgress(100, 'Done', fileObject);
            if (callback) {
                callback();
            }
        }));
};

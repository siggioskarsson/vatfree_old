import { Meteor } from "meteor/meteor";

Meteor.methods({
    'get-activity-log'(activityLogId) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return ActivityLogs.findOne({_id: activityLogId});
    },
    'search-activity-logs'(searchTerm, limit, offset) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {};
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector, 'textSearch', true);
        }

        let activityLogs = [];
        ActivityLogs.find(selector, {
            sort: {
                name: 1
            },
            limit: limit,
            offset: offset
        }).forEach((activityLog) => {
            activityLogs.push({
                text: activityLog.name + ' - ' + activityLog.addressFirst + ', ' + activityLog.postalCode + ' ' + activityLog.city,
                id: activityLog._id
            });
        });

        return activityLogs;
    },
    'get-activity-logs-stats'(selector) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        let count = ActivityLogs.find(selector).count();
        return [
            {
                _id: "activity-logs",
                value: {
                    count: count
                }
            }
        ];
    },
    'resend-activity-log-email'(activityLogId, travellerId) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let traveller = Meteor.users.findOne({
            _id: travellerId
        });
        if (!traveller) {
            throw new Meteor.Error(500, 'Could not find traveller');
        }

        let activityLog = ActivityLogs.findOne({
            _id: activityLogId
        });
        if (!activityLog) {
            throw new Meteor.Error(500, 'Could not find activity log');
        }

        let activityLogItem = {
            type: 'email',
            travellerId: travellerId,
            subject: activityLog.subject,
            address: traveller.profile.email,
            status: 'done',
            notes: activityLog.notes,
            html: activityLog.html
        };
        ActivityLogs.insert(activityLogItem);

        Email.send({
            to: traveller.profile.email,
            from: 'support@vatfree.com',
            subject: activityLog.subject,
            text: activityLog.notes,
            html: activityLog.html
        });
    },
    'activity-log-insert'(activityLogItem) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        return ActivityLogs.insert(activityLogItem);
    }
});

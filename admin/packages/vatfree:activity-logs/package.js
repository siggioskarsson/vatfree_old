Package.describe({
    name: 'vatfree:activity-logs',
    summary: 'Vatfree activity-logs package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'email',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core',
        'vatfree:countries',
        'vatfree:email-templates'
    ]);

    api.addAssets([
    ], 'server');

    // shared files
    api.addFiles([
        'activity-logs.js'
    ]);

    // server files
    api.addFiles([
        'server/methods.js',
        'server/import.js',
        'server/activity-logs.js',
        'publish/activity-logs.js',
        'publish/activity-log.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/activity-log.less',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/preview-html.html',
        'templates/preview-html.js',
        'templates/tab.html',
        'templates/tab.js',
        'templates/activity-logs.html',
        'templates/activity-logs.js'
    ], 'client');

    api.export([
        'ActivityLogs'
    ]);
});

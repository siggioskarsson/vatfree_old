ActivityLogs = new Meteor.Collection('activity-logs');
ActivityLogs.attachSchema(new SimpleSchema({
    salesforceId: {
        type: String,
        optional: true
    },
    type: {
        type: String,
        label: "Type of this activity log",
        defaultValue: 'call',
        allowedValues: [
            'in-call',
            'out-call',
            'email',
            'push',
            'in-person',
            'task',
            'burny', // burny the job bot
            'event',
            'post-in',
            'shop-visit'
        ],
        optional: false
    },
    subject: {
        type: String,
        label: "Subject of this activity log item",
        optional: false
    },
    address: {
        type: String,
        label: "Email address if type is email",
        optional: true
    },
    userId: {
        type: String,
        label: "Name of user that needs to do something",
        index: 1,
        optional: true
    },
    status: {
        type: String,
        label: "Status of this activity log",
        defaultValue: 'new',
        allowedValues: [
            'new',
            'doing',
            'done'
        ],
        optional: false
    },
    due: {
        type: Date,
        label: "Due date of this activity log item",
        optional: true,
        index: -1
    },
    companyId: {
        type: String,
        label: "Company of activity log",
        index: 1,
        optional: true
    },
    contactId: {
        type: String,
        label: "Contact of activity log",
        index: 1,
        optional: true
    },
    shopId: {
        type: String,
        label: "Shop of activity-log",
        index: 1,
        optional: true
    },
    affiliateId: {
        type: String,
        label: "Affiliate of activity-log",
        index: 1,
        optional: true
    },
    invoiceId: {
        type: String,
        label: "Invoice of activity-log",
        index: 1,
        optional: true
    },
    receiptId: {
        type: String,
        label: "Receipt of activity-log",
        index: 1,
        optional: true
    },
    travellerId: {
        type: String,
        label: "Traveller of activity-log",
        index: 1,
        optional: true
    },
    paymentId: {
        type: String,
        label: "Payment of activity-log",
        index: 1,
        optional: true
    },
    payoutId: {
        type: String,
        label: "Payout of activity-log",
        index: 1,
        optional: true
    },
    fileIds: {
        type: [String],
        label: "Files attached to this activity",
        optional: true
    },
    notes: {
        type: String,
        label: "Notes on this ActivityLog",
        optional: true
    },
    html: {
        type: String,
        label: "HTML part of this ActivityLog",
        optional: true
    },
    source: {
        type: String,
        label: "Source part of this ActivityLog - could be for instance from raw email",
        optional: true
    },
    readOn: {
        type: Date,
        label: "The date time the message was read - if applicable",
        optional: true
    },
    hideInMessageBox: {
        type: Boolean,
        label: "Whether this message should not be shown in the traveller message box",
        optional: true
    },
    data: {
        type: Object,
        label: "Extra data saved in this activity log",
        optional: true,
        blackbox: true
    },
    error: {
        type: Object,
        optional: true,
        blackbox: true
    },
    deleted: {
        type: Boolean,
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
ActivityLogs.attachSchema(CreatedUpdatedSchema);
StatusDates.attachToSchema(ActivityLogs);

ActivityLogs.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP) || Roles.userIsInRole(userId, 'vatfree', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

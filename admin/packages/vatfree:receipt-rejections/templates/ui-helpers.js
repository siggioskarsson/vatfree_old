UI.registerHelper('getReceiptRejectionName', function(receiptRejectionId) {
    if (!receiptRejectionId) receiptRejectionId = this.receiptRejectionId;

    let receiptRejection = ReceiptRejections.findOne({_id: receiptRejectionId});
    if (receiptRejection) {
        return receiptRejection.name;
    }

    return "";
});

UI.registerHelper('getReceiptRejections', function(type) {
    let selector = {
        deleted: {
            $ne: true
        }
    };
    if (type) {
        selector.type = type;
    }
    return ReceiptRejections.find(selector, {
        sort: {
            name: 1
        }
    });
});

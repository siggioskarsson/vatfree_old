import './receipt-rejection-list.html';

Template.receiptRejectionList.onCreated(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('receipt-rejections-list');
    })
});

Template.receiptRejectionList.helpers({
    getReceiptRejections() {
        let selector = {
            deleted: {
                $ne: true
            }
        };
        if (_.has(this, 'receiptRejectionIds')) {
            selector['_id'] = {
                $in: this.receiptRejectionIds || []
            }
        }
        return ReceiptRejections.find(selector, {
            sort: {
                name: 1
            }
        });
    }
});

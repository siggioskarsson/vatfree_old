import './list.html';

Template.receiptRejectionsList.helpers(Vatfree.templateHelpers.helpers);
Template.receiptRejectionsList.helpers(receiptRejectionHelpers);
Template.receiptRejectionsList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/rejection-receipt/:itemId', {itemId: this._id});
    }
});

Template.receiptRejectionsList.helpers({
    getTypeIcon() {
        return this.type === "invoice" ? "vtb" : 'receiptsmore';
    }
});

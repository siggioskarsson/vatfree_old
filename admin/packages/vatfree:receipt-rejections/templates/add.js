Template.addReceiptRejectionModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-receipt-rejection').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-receipt-rejection')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-receipt-rejection').on('shown.bs.modal', function () {
            //
            $('input[name="name"]').focus();
        });
        $('#modal-add-receipt-rejection').modal('show');
    });
});

Template.addReceiptRejectionModal.events({
    'click .cancel-add-receipt-rejection'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-receipt-rejection-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        ReceiptRejections.insert(formData);
        template.hideModal();
    }
});

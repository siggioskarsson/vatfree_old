import autosize from 'meteor/vatfree:core-templates/templates/autosize';

Template.receiptRejectionEdit.onCreated(function () {

});

Template.receiptRejectionEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('receipt-rejections_item', FlowRouter.getParam('itemId'));
    });
});

Template.receiptRejectionEdit.helpers({
    itemDoc() {
        return ReceiptRejections.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    getBreadcrumbTitle() {
        return this.name;
    }
});

Template.receiptRejectionEditForm.onCreated(function() {
    this.type = new ReactiveVar(this.data.type);
});

Template.receiptRejectionEditForm.onRendered(function() {
    Tracker.afterFlush(() => {
        autosize(this.$('textarea'));
    });

    this.autorun(() => {
        if (this.type.get()) {
            Tracker.afterFlush(() => {
                this.$('select[name="receiptTypeIds"]').select2();
            });
        }
    });
});

Template.receiptRejectionEditForm.helpers({
    getType() {
        return Template.instance().type.get();
    },
    getTextForLanguage() {
        let data = Template.instance().data;
        if (data.text && data.text[this.code]) {
            return data.text[this.code];
        }
    }
});

Template.receiptRejectionEditForm.events({
    'change select[name="type"]'(e, template) {
        template.type.set($(e.currentTarget).val());
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/rejections-receipt');
    },
    'click .delete-receipt-rejection'(e) {
        e.preventDefault();
        let receiptRejectionId = this._id;
        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "This receipt rejection will not visible in any list anymore!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            ReceiptRejections.update({
                _id: receiptRejectionId
            },{
                $set: {
                    deleted: true
                }
            });
            swal("Deleted!", "Receipt rejection has been deleted.", "success");
            FlowRouter.go('/rejections-receipt');
        });
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        ReceiptRejections.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('ReceiptRejection saved!')
            }
        });
    }
});

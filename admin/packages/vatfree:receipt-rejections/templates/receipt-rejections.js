import './receipt-rejections.html';

Template.receiptRejections.onCreated(Vatfree.templateHelpers.onCreated(ReceiptRejections, '/rejection-receipts', '/rejection-receipt/:itemId'));
Template.receiptRejections.onRendered(Vatfree.templateHelpers.onRendered());
Template.receiptRejections.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.receiptRejections.helpers(Vatfree.templateHelpers.helpers);
Template.receiptRejections.helpers(receiptRejectionHelpers);
Template.receiptRejections.events(Vatfree.templateHelpers.events());

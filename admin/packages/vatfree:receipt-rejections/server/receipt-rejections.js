ReceiptRejections._ensureIndex({'textSearch': "text"}, {"language_override": "_text_language"});

ActivityStream.attachHooks(ReceiptRejections);

Meteor.methods({
    'import-receipt-rejections'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let data = JSON.parse(Assets.getText('receipt-rejections.json'));
        _.each(data, (receiptRejection) => {
            ReceiptRejections.insert({
                id: receiptRejection.id,
                name: receiptRejection.name,
                code: receiptRejection.code
            });
        });
    }
});


ReceiptRejections.after.insert(function(userId, doc) {
    ReceiptRejections.updateReceiptTextSearch(doc);
});


ReceiptRejections.after.update(function(userId, doc, fieldNames, modifier, options) {
    ReceiptRejections.updateReceiptTextSearch(doc);
});

ReceiptRejections.updateReceiptTextSearch = function(doc) {
    let textSearch = doc.name;
    textSearch += doc.description;

    textSearch += ' type:' + doc.type;

    _.each(doc.text, (t) => {
        textSearch+= ' ' + t;
    });

    ReceiptRejections.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch.toLowerCase().latinize()
        }
    });
};

Meteor.publish('receipt-rejections_item', function(itemId) {
    check(itemId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }
    }

    return ReceiptRejections.find({
       _id: itemId
    });
});

ReceiptRejections = new Meteor.Collection('receipt-rejections');
ReceiptRejections.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "The internal id ised to identify the rejection reason",
        optional: false
    },
    description: {
        type: String,
        label: "Descriptive text of the rejection",
        optional: true
    },
    allowReopen: {
        type: Boolean,
        label: "Whether the receipt is allowed to be reopened with this reason",
        autoValue: function () {
            if (this.field('allowReopen').isSet) {
                return !!this.field('allowReopen').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    type: {
        type: String,
        label: "What type of rejection this reason is for (receipt, invoice)",
        allowedValues: [
            'receipt',
            'invoice'
        ],
        optional: true
    },
    receiptTypeIds: {
        type: [String],
        label: "For which receipt types this rejection is used",
        optional: true
    },
    qrRejection: {
        type: Boolean,
        label: "Whether to use this rejection when a receipt was added via QR code",
        autoValue: function () {
            if (this.field('qrRejection').isSet) {
                return !!this.field('qrRejection').value;
            } else {
                this.unset();
            }
        },
        optional: true
    },
    deleted: {
        type: Boolean,
        label: "When this receipt rejection was deleted",
        optional: true
    },
    text: {
        type: Object,
        label: "All the translations for this reject reason",
        optional: true,
        blackbox: true
    },
    textSearch: {
        type: String,
        optional: true,
    }
}));
ReceiptRejections.attachSchema(CreatedUpdatedSchema);


ReceiptRejections.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    }
});

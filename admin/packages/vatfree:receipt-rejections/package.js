Package.describe({
    name: 'vatfree:receipt-rejections',
    summary: 'Vatfree receipt-rejections package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core'
    ]);

    // shared files
    api.addFiles([
        'receipt-rejections.js'
    ]);

    // server files
    api.addFiles([
        'server/receipt-rejections.js',
        'publish/receipt-rejections.js',
        'publish/receipt-rejections-list.js',
        'publish/receipt-rejection.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/receipt-rejections.html',
        'templates/receipt-rejections.js',
        'templates/components/receipt-rejection-list.html',
        'templates/components/receipt-rejection-list.js'
    ], 'client');

    api.export([
        'ReceiptRejections'
    ]);
});

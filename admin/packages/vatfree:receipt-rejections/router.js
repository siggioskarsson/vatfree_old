FlowRouter.route('/rejections-receipt', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "receiptRejections"});
    }
});

FlowRouter.route('/rejection-receipt/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "receiptRejectionEdit"});
    }
});

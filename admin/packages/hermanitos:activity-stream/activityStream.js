ActivityStream.add.insert = function(userId, doc, target) {
    ActivityStream.add._insert(userId, 'create', {}, doc, target);
};
ActivityStream.add.update = function(userId, oldDoc, newDoc, target) {
    ActivityStream.add._insert(userId, 'change', oldDoc, newDoc, target);
};
ActivityStream.add.remove = function(userId, doc, target) {
    ActivityStream.add._insert(userId, 'delete', {}, doc, target);
};
ActivityStream.add.download = function(userId, doc, target) {
    console.log('ActivityStream.add.download', userId, doc, target);
    ActivityStream.add._insert(userId, 'download', {}, doc, target);
};
ActivityStream.add.upload = function(userId, doc, target) {
    ActivityStream.add._insert(userId, 'upload', {}, doc, target);
};

// verb: create, update, delete, move, upload, download
ActivityStream.add._insert = function(userId, verb, oldDoc, newDoc, target) {
    check(verb, String);
    check(oldDoc, Object);
    check(newDoc, Object);
    check(target, String);

    var options = ActivityStream.config.options || {};
    var ignoreFields = (ActivityStream.config.ignoreUpdatesFor ? (ActivityStream.config.ignoreUpdatesFor[target] || []) : []);

    try {
        // We do a plain doc diff here, removing any "random" fields
        // If this needs to be viewed, create a ActivityStream.viewer[target] function that returns the view
        if (oldDoc) {
            var docDiff = difference(oldDoc, newDoc, ignoreFields);
            delete docDiff.createdAt;
            delete docDiff.createdBy;
            delete docDiff.updatedAt;
            delete docDiff.updatedBy;
        } else {
            docDiff = newDoc;
        }

        if (docDiff !== false) {
            // set security context
            var securityContext = {};
            _.each(ActivityStream.config.securityContext, function(context) {
                securityContext[context] = newDoc[context];
            });

            var itemId = newDoc._id;

            var activity = {};

            var userName = '';
            if (userId) {
                var user = Meteor.users.findOne({
                    _id: userId
                });
                if (user) {
                    if (options.userName) {
                        userName = _.deepPickValue(user, options.userName);
                    } else {
                        if (user.profile && user.profile.name) {
                            userName = user.profile.name;
                        }
                    }
                }
            }
            activity.actor = "users:" + (userId || "") + ":" + (userName || "");

            activity.verb = verb;

            activity.object = docDiff;

            if (verb === 'change') {
                var docDiffOld = difference(newDoc, oldDoc, ignoreFields);
                delete docDiffOld.createdAt;
                delete docDiffOld.createdBy;
                delete docDiffOld.updatedAt;
                delete docDiffOld.updatedBy;

                activity.objectOld = docDiffOld;
            }

            activity.target = {
                target: target,
                itemId: itemId,
                name: options.itemName ? newDoc[options.itemName] : (newDoc.name || (newDoc.profile ? newDoc.profile.name : null))
            };

            var activityItem = {
                userId: userId,
                securityContext: securityContext,
                activity: activity
            };

            if (ActivityStream.config.notifyUsers) {
                var notifyUsers = ActivityStream.config.notifyUsers(userId, newDoc, docDiff, docDiffOld);
                if (notifyUsers && notifyUsers.length) {
                    activityItem.notifications = [];
                    _.each(notifyUsers, (user) => {
                        if (user) {
                            activityItem.notifications.push({
                                userId: user
                            });
                        }
                    });
                }
            }

            if (!activityItem || !activityItem.activity || !activityItem.activity.object || (activityItem.activity.object === {} && activityItem.activity.objectOld === {})) {
                return false;
            }
            ActivityStream.collection.insert(activityItem);
        }
    } catch(e) {
        console.log(e);
    }
};

/*
 http://codereview.stackexchange.com/a/11580
 */
var difference = function(o1, o2, ignoreKeys) {
    if (!ignoreKeys) ignoreKeys = [];
    var key, kDiff,
        diff = {};
    for (key in o1) {
        if (!_.contains(ignoreKeys, key)) {
            if (!o1.hasOwnProperty(key)) {
            } else if (_.isArray(o1[key])) {
                // array comparison
                if (!_.isEqual(o1[key], o2[key])) {
                    diff[key] = o2[key];
                }
            } else if (typeof o1[key] != 'object' || typeof o2[key] != 'object') {
                // object comparison
                if (!(key in o2) || o1[key] !== o2[key]) {
                    diff[key] = o2[key];
                }
            } else if (kDiff = difference(o1[key], o2[key], [])) {
                // other comparison
                diff[key] = kDiff;
            }
        }
    }
    for (key in o2) {
        if (!_.contains(ignoreKeys, key)) {
            if (o2.hasOwnProperty(key) && !(key in o1)) {
                diff[key] = o2[key];
            }
        }
    }
    for (key in diff) {
        if (!_.contains(ignoreKeys, key)) {
            if (diff.hasOwnProperty(key)) {
                return diff;
            }
        }
    }
    return false;
};

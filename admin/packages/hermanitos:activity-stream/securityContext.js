ActivityStream.checkSecurityContext = function(userId, doc) {
	// check for user specific security context
	if (ActivityStream.config.securityContext) {
		// check that all the security context items are filled in
		_.each(ActivityStream.config.securityContext, function(context) {
			if (doc.securityContext && doc.securityContext[context] === undefined) {
				return false;
			}
		});

		// user defined function to check security context
		if (_.isFunction(ActivityStream.config.securityContextCheck)) {
			return ActivityStream.config.securityContextCheck(userId, doc);
		}

		return true;
	} else {
		return true;
	}
};

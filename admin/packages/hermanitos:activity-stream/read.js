ActivityStream.setRead = function(id, userId) {
    check(id, String);
    check(userId, String);

    if (Meteor.isClient) {
        Meteor.call('activityStreamSetRead', id);
    } else {
        ActivityStream.collection.update({
            _id: id,
            'notifications.userId': userId
        },{
            $set: {
                'notifications.$.readOn': new Date()
            }
        });
    }
};

ActivityStream.setUnread = function(id, userId) {
    check(id, String);
    check(userId, String);

    if (Meteor.isClient) {
        Meteor.call('activityStreamSetUnread', id);
    } else {
        ActivityStream.collection.update({
            _id: id,
            'notifications.userId': userId
        }, {
            $unset: {
                'notifications.$.readOn': 1
            }
        });
    }
};

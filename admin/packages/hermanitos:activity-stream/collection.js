ActivityStream.collection = new Mongo.Collection("activity-stream");
var NotificationsSchema = new SimpleSchema({
    userId: {
        type: String,
        optional: false
    },
    readOn: {
        type: Date,
        optional: true
    }
});

var activityStreamSchema = new SimpleSchema({
	securityContext: {
		type: Object,
		label: "Security context object. This needs to be configured before usage on the server",
		optional: false,
		blackbox: true
	},
	userId: {
		type: String,
		label: "Id of the user initiating the activity",
		optional: true
	},
	date: {
		type: Date,
		label: "Date/time of activity",
		autoValue: function() {
			if (this.isInsert) {
				return new Date();
			} else if (this.isUpsert) {
				return {
					$setOnInsert: new Date()
				};
			} else {
				this.unset();
			}
		}
	},
	/**
	 * Activity object has the following parameters:
	 * - actor
	 * - verb (created, updated, deleted, moved, uploaded)
     * - object
     * - target
	 */
	activity: {
		type: Object,
		label: "Activity information. Is a blackbox object, because it can be encrypted",
		optional: false,
		blackbox: true
	},
    /**
     * Notifications sub-document is of the form
     *
     * notifications: [
     *    {
     *       userId: <userId>,
     *       read: false
     *    },
     *    {
     *       userId: <userId>,
     *       read: false
     *    }
     * ]
     */
    notifications: {
        type: [NotificationsSchema],
        optional: true,
        blackbox: true
    }
});
ActivityStream.collection.attachSchema(activityStreamSchema);

ActivityStream.collection.allow({
    // turn off all client side updates
	insert: function () {
        return false;
	},
	update: function () {
		return false;
	},
	remove: function () {
		return false;
	}
});

if (Meteor.isServer) {
    ActivityStream.collection._ensureIndex({'notifications.userId': 1, 'notifications.readOn': 1});
    ActivityStream.collection._ensureIndex({'notifications.userId': 1, 'notifications.readOn': 1, 'date': -1});
    ActivityStream.collection._ensureIndex({'date': -1});
    ActivityStream.collection._ensureIndex({'activity.target.itemId': 1});
}

Meteor.publish('activity-stream', function(context, itemId, limit, offset) {
    this.unblock();

    check(context, Object);
    if (!_.isUndefined(itemId) && itemId !== null) {
        check(itemId, String)
    }
    if (!_.isUndefined(limit)) {
        check(limit, Number)
    }
    if (!_.isUndefined(offset)) {
        check(offset, Number)
    }

    if (ActivityStream.config.securityContext && !ActivityStream.checkSecurityContext(this.userId, context)) {
        return [];
    }

    limit = limit || 100;
    offset = offset || 0;

    var selector = {};
    _.each(context, function(contextValue, contextKey) {
        selector['securityContext.' + contextKey] = contextValue;
    });

    if (itemId) {
        selector['activity.target.itemId'] = itemId;
    }

    return ActivityStream.collection.find(selector, {
        limit: limit,
        offset: offset,
        sort: {
            date: -1
        },
        fields: {
            'activity.object.notes': false,
            'activity.object.html': false
        }
    });
});

Meteor.publish('activity-notifications', function(limit, offset) {
    this.unblock();

    limit = Number(limit) || 100;
    offset = Number(offset) || 0;

    var selector = {
        'notifications.userId': this.userId,
        'notifications.readOn': {
            $exists: false
        }
    };

    return ActivityStream.collection.find(selector, {
        limit: limit,
        offset: offset,
        sort: {
            date: -1
        }
    })
});

describe('namespace', function() {
    it('should have the ActivityStream namespace', function( ){
        expect(ActivityStream).to.not.be.undefined;
        expect(ActivityStream.config).to.not.be.undefined;
    })
});

ActivityStream.attachHooks = function(collection) {
    var target = collection._name;

    collection.after.insert(function (userId, doc) {
        // update activity stream
        ActivityStream.add.insert(userId, doc, target);
    });

    collection.after.update(function (userId, doc, fieldNames, modifier, options) {
        // update activity stream
        if (ActivityStream.config && ActivityStream.config.ignoreUpdatesFor && ActivityStream.config.ignoreUpdatesFor[target]) {
            // check whether we need to ignore the update
            var fieldUpdates = _.difference(fieldNames, ActivityStream.config.ignoreUpdatesFor[target]);
            if (!fieldUpdates.length) {
                return false;
            }
        }
        ActivityStream.add.update(userId, this.previous, doc, target);
    });

    collection.after.remove(function(userId, doc) {
        // update activity stream
        ActivityStream.add.remove(userId, doc, target);
    });
};

Package.describe({
	summary: 'Hermanitos generic activity stream package.',
	version: '0.0.1',
	name: 'hermanitos:activity-stream',
	git: ''
});

Package.on_use(function (api) {
	api.versionsFrom("1.2.0.2");

	api.use([
		'mongo',
        'check',
		'templating',
		'ecmascript',
		'flemay:less-autoprefixer',
		'aldeed:collection2@2.10.0',
		'aldeed:simple-schema@1.5.3',
        'gfk:underscore-deep@1.0.0'
	]);

	//shared
	api.addFiles([
		'globals.js',
		'collection.js',
        'securityContext.js',
        'item.js',
        'activityStream.js',
        'hooks.js',
        'read.js'
	]);

	//client
	api.addFiles([
	], 'client');

	//server
	api.addFiles([
		'publish.js',
        'server/read.js'
	], 'server');

	api.addAssets([
		'nl.i18n.json'
	], ["client", "server"]);

	api.export("ActivityStream");
});

Package.on_test(function (api) {
	api.use([
		'practicalmeteor:mocha',
		'practicalmeteor:sinon',
		'practicalmeteor:chai'
	]);

	api.use([
		'hermanitos:activity-stream'
	]);

	api.addFiles([
		'tests/activity-stream.js'
	], ['client', 'server']);
});

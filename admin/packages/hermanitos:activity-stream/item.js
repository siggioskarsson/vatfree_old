/**
 * Naive wrapper for the activity stream object
 * The plan is to extend this later, but for now it's very simple
 */
ActivityStream.item = (function() {

    'use strict';
    return function activityStreamItem(setItem) {
        this.constructor = activityStreamItem;
        this.constructor.prototype = new Object();
        this.constructor.prototype.constructor.apply(this, arguments);
        item = setItem;

        /**
         * Get the textual representation of the activity stream item
         *    ACTOR -> VERB -> OBJECT -> TARGET
         */
        this.getText = function () {
            var target = getTarget();
            return TAPi18n.__('activity_stream_' + item.verb + (target ? '_target': ''), {actor: getActor(), object: getObjectDescription(), target: target});
        };

        /**
         * Get the html representation of the activity stream item, including links
         */
        this.getHtml = function () {
            return TAPi18n.__('activity_stream_' + item.verb, getActor(), getObjectDescription(), getTarget());
        };

        //
        // "privates"
        //

        /**
         * Get the description text of the actor
         *
         * @returns string
         */
        var getActor = function () {
            var actor = item.actor.split(':');

            return actor[2] || actor[1];
        };

        /**
         * Get the description of the object being acted upon
         *
         * @returns {string}
         */
        var getObjectDescription = function () {
            var objectDescription = '';

            if (_.isString(item.object)) {
                var object = item.object.split(':');
                if (object[0].indexOf('_') === 0) {
                    // selector object
                    switch (object[0]) {
                        case '_attr':
                        default:
                            var translationKey = 'activity_stream_' + object[0] + '_' + object[1];
                            var translation = TAPi18n.__(translationKey);
                            // return the value itself if the text was not translated
                            objectDescription = (translation === translationKey ? object[1] : translation);
                            break;

                    }
                } else {
                    // database object
                    // we do not support linking yet
                    objectDescription = object[2];
                }
            } else if (_.isObject(item.object)) {
                // TODO: initialize viewer and show
                objectDescription = item.target.name || TAPi18n.__("an item");
            }

            return objectDescription;
        };

        /**
         * Get the description text of the target object
         *
         * @returns string
         */
        var getTarget = function () {
            var targetDescription = '';

            if (_.isString(item.target)) {
                var object = item.object.split(':');
                var target = (item.target || '').split(':');
                if (object[0].indexOf('_') === 0) {
                    // selector object
                    switch (object[0]) {
                        case '_attr':
                        default:
                            var translationKey = 'activity_stream_' + object[1] + '_' + object[2];
                            var translation = TAPi18n.__(translationKey);
                            // return the value itself if the text was not translated
                            targetDescription = (translation === translationKey ? object[2] : translation);
                            break;

                    }
                } else if (target) {
                    // database object
                    // we do not support linking yet
                    targetDescription = target[2];
                }
            } else if (_.isObject(item.target)) {
                targetDescription = item.target.target;
            }

            return targetDescription;
        };

        // local variables
        var self = this,
            item;
    }
}());

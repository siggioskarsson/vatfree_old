Meteor.methods({
    activityStreamSetRead: function(id) {
        ActivityStream.setRead(id, this.userId);
    },
    activityStreamSetUnread: function(id) {
        ActivityStream.setUnread(id, this.userId);
    }
});

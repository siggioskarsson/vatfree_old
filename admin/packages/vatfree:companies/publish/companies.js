Meteor.publishComposite('companies', function(searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};
    if (!_.has(selector, 'deleted')) {
        selector.deleted = {
            $exists: false
        };
    }

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        find: function () {
            return Companies.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                find: function (item) {
                    return Files.find({
                        itemId: item._id,
                        target: 'companies',
                        category: 'logo'
                    });
                }
            }
        ]
    }
});

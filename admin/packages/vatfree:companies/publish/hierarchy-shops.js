Meteor.publish('company-hierarchy-shops', function(companyId) {
    check(companyId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    let company = Companies.findOne({_id: companyId}) || {};
    let rootCompany = Companies.findOne({
        _id: {
            $in: company.ancestors || []
        },
        $or: [
            {
                parentId: {
                    $exists: false
                }
            },
            {
                parentId: null
            }
        ]
    });

    const companyIds = Companies.find({
        ancestors: rootCompany._id,
        deleted: {
            $ne: true
        }
    },{
        fields: {
            _id: 1
        }
    }).map((c) => { return c._id; }) || [];

    return Shops.find({
        companyId: {
            $in: companyIds
        },
        deleted: {
            $ne: true
        }
    })
});

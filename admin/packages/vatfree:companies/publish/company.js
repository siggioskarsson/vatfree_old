Meteor.publishComposite('companies_item', function (itemId, subscribeToDetails = false) {
    check(itemId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    let children = [];
    if (subscribeToDetails) {
        children = [
            {
                find: function (item) {
                    return Companies.find({
                        _id: {
                            $in: item.ancestors || []
                        },
                        deleted: {
                            $ne: true
                        }
                    });
                }
            },
            {
                find: function (item) {
                    return Files.find({
                        itemId: item._id,
                        target: 'companies'
                    });
                }
            },
            {
                collectionName: 'contacts',
                find: function (item) {
                    return Contacts.find({
                        'profile.companyId': item._id
                    });
                }
            }
        ];
    }

    return {
        find: function () {
            return Companies.find({
                _id: itemId
            });
        },
        children: children
    }
});

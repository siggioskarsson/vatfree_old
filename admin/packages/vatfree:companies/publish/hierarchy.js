Meteor.publish('company-hierarchy', function(companyId) {
    check(companyId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    let company = Companies.findOne({_id: companyId}) || {};
    let rootCompany = Companies.findOne({
        _id: {
            $in: company.ancestors || []
        },
        $or: [
            {
                parentId: {
                    $exists: false
                }
            },
            {
                parentId: null
            }
        ]
    });

    return Companies.find({
        ancestors: rootCompany._id,
        deleted: {
            $ne: true
        }
    },{
        fields: {
            name: 1,
            addressFirst: 1,
            addressFirstNumber: 1,
            city: 1,
            countryId: 1,
            parentId: 1,
            deleted: 1
        }
    });
});

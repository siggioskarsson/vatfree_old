Companies = new Meteor.Collection('companies');
Companies.attachSchema(new SimpleSchema({
    id: {
        type: String,
        label: "Internal company id",
        optional: true
    },
    name: {
        type: String,
        label: "Name of the Company",
        optional: false
    },
    status: {
        type: String,
        label: "The status of this company",
        allowedValues: [
            'new',
            'incomplete',
            'toCall',
            'toMerge',
            'bankrupt',
            'moved',
            'dissolved',
            'takenOver',
            'active',
            'inactive'
        ],
        optional: false,
        defaultValue: 'active'
    },
    email: {
        type: String,
        label: "General email address",
        optional: true
    },
    tel: {
        type: String,
        label: "General telephone number",
        optional: true
    },
    website: {
        type: String,
        label: "General website",
        optional: true
    },
    addressFirst: {
        type: String,
        label: "Address of the Company",
        optional: true
    },
    postalCode: {
        type: String,
        label: "Postal code of the Company",
        optional: true
    },
    city: {
        type: String,
        label: "City of the Company",
        optional: true
    },
    countryId: {
        type: String,
        label: "Country Id of the Company",
        optional: true
    },
    chamberOfCommerce: {
        type: String,
        label: "Chamber of commerce identifier",
        optional: true
    },
    vatNumber: {
        type: String,
        label: "VAT number",
        optional: true
    },
    parentId: {
        type: String,
        label: "Parent of this company",
        optional: true
    },
    ancestors: {
        type: [String],
        label: "Ancestors",
        optional: true,
        index: true
    },
    ancestorsLength: {
        type: Number,
        label: "Number of Ancestors",
        optional: true
    },
    partnershipStatus: {
        type: String,
        label: "Partnership status",
        allowedValues: [
            'new',
            'partner',
            'pledger',
            'affiliate',
            'uncooperative'
        ],
        optional: true,
        defaultValue: 'new'
    },
    subscription: {
        type: String,
        label: "The subscription of this shop",
        allowedValues: [
            'none',
            'free',
            'basic',
            'blend',
            'boost',
            'boutique',
            'premium'
        ],
        optional: false,
        defaultValue: 'none'
    },
    affiliatedWith: {
        type: [String],
        label: "Affiliated with",
        optional: true
    },
    "affiliatedWith.$": {
        type: String,
        label: "Affiliated with",
        allowedValues: Vatfree.competitors
    },
    partneredBy: {
        type: String,
        label: "The user that partnered this shop",
        optional: true
    },
    partneredAt: {
        type: Date,
        label: "The date that this shop was partnered",
        optional: true
    },
    language: {
        type: String,
        label: "Language code",
        optional: true
    },
    emailTemplateId: {
        type: String,
        label: "Preferred email template",
        optional: true
    },
    stats: {
        type: Object,
        label: "Stats about this Company",
        optional: true,
        blackbox: true
    },
    synchronization: {
        type: [Vatfree.synchronizationSchema],
        label: "Synchronizations of the item to/from other systems",
        optional: true
    },
    notes: {
        type: String,
        label: "Notes on this Company",
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    },
    deleted: {
        type: Date,
        optional: true
    }
}));
Companies.attachSchema(CreatedUpdatedSchema);
Companies.attachSchema(BillingSchema);

Companies.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

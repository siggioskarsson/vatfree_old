try {
    Companies._ensureIndex({'id': 1});
    Companies._ensureIndex({'textSearch': "text"}, {"language_override": "_text_language"});
} catch (e) {
    console.error(e);
}

ActivityStream.attachHooks(Companies);

Meteor.methods({
    'import-companies'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let data = JSON.parse(Assets.getText('companies.json'));
        _.each(data, (company) => {
            Companies.insert({
                id: company.id,
                name: company.name,
                code: company.code
            });
        });
    }
});

var updateTextSearch = function (doc) {
    let textSearch = (
        (doc.name || "") + ' ' +
        (doc.id || "") + ' ' +
        (doc.email || "") + ' ' +
        (doc.tel || "") + ' ' +
        (doc.addressFirst || "") + ' ' +
        (doc.postalCode || "") + ' ' +
        (doc.city || "") + ' ' +
        (doc.notes || "") + ' ' +
        (doc.code || "")
    ).toLowerCase().latinize();

    Companies.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch
        }
    });
};

var getAncestors = function (company) {
    var parent = Companies.findOne({_id: company.parentId});
    if (!parent) parent = {}; // for top level companies
    if (!parent.ancestors) parent.ancestors = [];
    parent.ancestors.unshift(company._id);
    return parent.ancestors;
};

var getAncestorsById = function (itemId) {
    var ancestors = [itemId];
    var item = Companies.direct.findOne({_id: itemId});
    var parentId = item.parentId;
    while (item && item.parentId && (item = Companies.direct.findOne({_id: item.parentId}))) {
        if (item._id === itemId) {
            // bad stuff, the item should not be a parent, there is a circular reference :-(
            break;
        }
        ancestors.push(item._id);
        parentId = item.parentId;
    }
    return ancestors;
};

Companies.updateAncestors = function(itemId) {
    var updateAncestorsHelper = function (itemId) {
        var ancestors = getAncestorsById(itemId);
        Companies.direct.update({_id: itemId}, {
            $set: {
                ancestors: ancestors,
                ancestorsLength: ancestors.length
            }
        });
        updateAncestorsOfChildren(itemId);
    };
    var updateAncestorsOfChildren = function (itemId) {
        Companies.direct.find({parentId: itemId}).forEach(function (doc) {
            updateAncestorsHelper(doc._id);
        });
    };

    updateAncestorsHelper(itemId);
};

Companies.before.insert((userId, doc) => {
    doc.ancestors = getAncestors(doc);
});
Companies.after.insert((userId, doc) => {
    updateTextSearch(doc);

    if (!_.has(doc, 'ancestors')) {
        Companies.updateAncestors(doc._id);
    }
});

Companies.before.update(function (userId, doc, fieldNames, modifier, options) {
    if (modifier.$set && _.has(modifier.$set, 'partnershipStatus') && doc.partnershipStatus !== modifier.$set.partnershipStatus) {
        if (modifier.$set.partnershipStatus === 'uncooperative' && Meteor.settings.defaults.uncooperativeBillingEmailTextId) {
            modifier.$set.billingEmailTextId = Meteor.settings.defaults.uncooperativeBillingEmailTextId;
        }
    }
});

Companies.after.update(function (userId, doc, fieldNames, modifier, options) {
    updateTextSearch(doc);

    if (_.has(modifier, '$set') && _.has(modifier['$set'], 'parentId')) {
        Companies.updateAncestors(doc._id);
    }
    if (_.has(modifier, '$unset') && _.has(modifier['$unset'], 'parentId')) {
        Companies.direct.update({
            _id: doc._id
        },{
            $set: {
                ancestors: [doc._id],
                ancestorsLength: 1
            }
        });
    }

    // Update invoice stats that point back here
    if (
        _.contains(fieldNames, 'partnershipStatus') && modifier['$set'] && modifier.$set.partnershipStatus && modifier.$set.partnershipStatus !== this.previous.partnershipStatus
        ||
        _.contains(fieldNames, 'countryId') && modifier['$set'] && modifier.$set.countryId && modifier.$set.countryId !== this.previous.countryId
    ) {
        // update saved partnershipStatus in invoices
        Invoices.update({
            companyId: doc._id
        },{
            $set: {
                countryId: (modifier.$set.countryId || this.previous.countryId),
                partnershipStatus: (modifier.$set.partnershipStatus || this.previous.partnershipStatus)
            }
        },{
            multi: true
        });
    }

    if (_.contains(fieldNames, 'partnershipStatus') && modifier['$set'] && modifier.$set.partnershipStatus && modifier.$set.partnershipStatus !== this.previous.partnershipStatus) {
        Shops.update({
            billingCompanyId: doc._id
        },{
            $set: {
                partnershipStatus: modifier.$set.partnershipStatus
            }
        },{
            multi: true
        });
        Companies.update({
            billingCompanyId: doc._id
        },{
            $set: {
                partnershipStatus: modifier.$set.partnershipStatus
            }
        },{
            multi: true
        });
    }

    if (_.contains(fieldNames, 'status') && modifier['$set'] && modifier.$set.status && modifier.$set.status !== this.previous.status) {
        Shops.update({
            billingCompanyId: doc._id
        },{
            $set: {
                status: modifier.$set.status
            }
        },{
            multi: true
        });
        Companies.update({
            billingCompanyId: doc._id
        },{
            $set: {
                status: modifier.$set.status
            }
        },{
            multi: true
        });
    }

    if (_.has(modifier, '$set') && _.has(modifier['$set'], 'billingEntity')) {
        updateBillingCompanyId(doc._id);
    }
    if (_.has(modifier, '$unset') && _.has(modifier['$unset'], 'billingEntity')) {
        updateBillingCompanyId(doc._id);
    }
});

var updateBillingCompanyId  = function(companyId) {
    Companies.find({
        ancestors: companyId
    }).forEach((childCompany) => {
        let billingEntityDoc = Vatfree.billing.getBillingEntityForCompany(childCompany._id);
        let setData = {
            $unset: {
                billingCompanyId: 1
            }
        };
        if (billingEntityDoc) {
            setData = {
                $set: {
                    billingCompanyId: billingEntityDoc._id
                }
            };
        }
        Companies.update({
            _id: childCompany._id
        }, setData);
        Shops.update({
            companyId: childCompany._id
        }, setData, {
            multi: true
        });
    });
};

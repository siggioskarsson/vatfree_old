/* global Vatfree: true */

const csv = require('csvtojson');
const DEBUGGING = false;

const colMapping = {
    "Account ID": "id",
    "Parent Account ID": "parentAccountId",
    "Parent Account": false,
    "SG_keydata_complete": false,
    "SG Shop": "sgShop",
    "Account Name": "accountName",
    "Billing name": "billingName",
    "T.A.V.": "billingRecipientName",
    "Communication in": "language",
    "Description": "description",
    "General Phone": "tel",
    "Phone": "tel2",
    "Fax": "fax",
    "Website": "website",
    "TAX district": false,
    "KVK": "chamberOfCommerce",
    "Registered VAT no.": "vatNumber",
    "Shop no.": false,
    "Remarks": "notes",
    "Email": "email",
    "Affiliated since": "partneredAt",
    "Affiliated by": false,
    "AC Communication TEmpate": "communicationTemplate",
    "AC VTB processing remark": "billingNotes",
    "S-debtcollection": false,
    "Bank account": "debtCollectionAccount",
    "Bank name": false,
    "IncassoMandaatNummer": false,
    "Incaso Mandaat ID": "debtCollectionMandate",
    "D_Starterspakket": false,
    "send by post": false,
    "send by fax": false,
    "+ in .pdf": false,
    "+ no html": false,
    "+ authorization": false,
    "+ original receipts": false,
    "do not remind": false,
    "Accounting": "billingEntity",
    "Shop": "name",
    "street address": "addressFirst",
    "number": "addressFirstNumber",
    "adr_toevoeging": "addressFirstAddition",
    "zip code": "postCode",
    "city": "city",
    "country": "country",
    "SG_Geocode": "geo",
    "Billing Street": "billingAddress",
    "Billing Zip/Postal Code": "billingPostal",
    "Billing City": "billingCity",
    "Billing Country": "billingCountry",
    "AC VTB profile": "partnershipStatus",
    "Affiliated with": "affiliatedWith",
    "SGI lookup": false,
    "SGI ID": false,
    "SGI_Shopname": "shopName",
    "Last Modified Date": false,
    "Checked For Sync": false,
    "Sync to APP": "syncToApp",
    "API Vatfree Id": false,
    "Last time synced": false,
    "Kassasysteem": false,
    "Subscription": "accountStatus"
};

const convert = {
    "language": "lowerCase",
    "partneredAt": "date",
    "geo": "geo"
};

const partnerShipStatusConvert = {
    'Meewerkend': 'pledger',
    'Partner': 'partner',
    'Niet meewerkend 1': 'uncooperative',
    'Affiliate': 'affiliate',
    'Niet meewerkend definitief': 'uncooperative',
    'Niet meewerkend 2': 'uncooperative'
};

const affiliateMapping = {
    "Global Blue": "Global Blue",
    "direct refund": "direct refund",
    "Detaxe": "Detaxe",
    "eValidation Austria": "eValidation Austria",
    "Easy Taxfree": "Easy Tax Free",
    "Eurorefund": "Eurorefund",
    "EU Taxfree": "EU Taxfree",
    "Fexco Tax Free": "Fexco Tax Free",
    "GB Taxfree": "GB Taxfree",
    "Innova Taxfree Group": "Innova Tax Free",
    "Premier Taxfree": "Premier Tax Free",
    "Red All": "Red All",
    "Safety Tax Free": "Safety Tax Free",
    "Star Tax Free": "Star Tax Free",
    "Synergy Tax Free": "Synergy Tax Free",
    "Taxfree Worldwide": "Tax Free Worldwide",
    "Tax Refund Italy": "Tax Refund Italy",
    "Tax Free Easy": "Tax Free Easy",
    "Tax Free Germany": "Tax Free Germany"
};

const statusMapping = {
    'Free': 'active',
    'Boutique': 'active',
    'Basic': 'active',
    'Blend': 'active',
    'Boost': 'active',
    'Premium': 'active',
    'Other': 'active',
    'Competitor': 'active',
    'Investor': 'active',
    'Inner Circle': 'active',
    'Outer Circle': 'active',
    'Press': 'active',
    'Reseller': 'active',
    'Bankrupt': 'bankrupt'
};

const subscriptionMapping = {
    'Free': 'free',
    'Boutique': 'boutique',
    'Basic': 'basic',
    'Blend': 'blend',
    'Boost': 'boost',
    'Premium': 'premium',
    'Other': 'none',
    'Competitor': 'none',
    'Investor': 'none',
    'Inner Circle': 'none',
    'Outer Circle': 'none',
    'Press': 'none',
    'Reseller': 'none',
    'Bankrupt': 'none'
};

const cleanupUpdateData = function (updateData, countries) {
    updateData['tel'] = updateData['tel'] || updateData['tel2'];
    delete updateData['tel2'];

    if (updateData['country'].match(/^UK/)) {
        updateData['country'] = updateData['country'].replace('UK', 'GB');
    }

    let country = updateData['country'] || "NL";
    let userCountry = countries[country.substr(0, 2)];
    if (!userCountry) {
        userCountry = countries['NL'];
    }

    updateData['countryId'] = userCountry._id;
    updateData['currencyId'] = userCountry.currencyId;

    updateData['addressFirst'] = updateData['addressFirst'] + (updateData['addressFirstNumber'] ? ' ' + updateData['addressFirstNumber']: '') + (updateData['addressFirstAddition'] ? ' ' + updateData['addressFirstAddition'] : '');
    updateData['postalCode'] = updateData['postCode']; // Yeah, really! :-S

    let billingCountryCode = updateData['billingCountry'] || "NL";
    let billingCountry = countries[billingCountryCode.substr(0, 2)];
    updateData['billingAddress'] = updateData['billingAddress'] + "\n" + updateData['billingPostal'] + " " + updateData['billingCity'] + "\n" + (billingCountry ? billingCountry.name : "");

    if (updateData['language'] === 'du') {
        updateData['language'] = 'de';
    }

    if (updateData['affiliatedWith']) {
        let affiliatedWith = [];
        let affiliates = updateData['affiliatedWith'].split('; ');
        _.each(affiliates, (affiliate) => {
            if (affiliateMapping[affiliate]) {
                affiliatedWith.push(affiliateMapping[affiliate]);
            }
        });
        updateData['affiliatedWith'] = affiliatedWith;
    } else {
        updateData['affiliatedWith'] = [];
    }

    if (updateData['billingEntity'] === "1") {
        updateData['billingEntity'] = true;
        if (updateData['vatNumber']) {
            updateData['billingVat'] = updateData['vatNumber'];
        }
    } else {
        updateData['billingEntity'] = false;
    }
};

const changeChildCompanyId = function (id, parentCompany) {
    if (DEBUGGING) console.log('changeChildCompanyId', id, parentCompany);
    if (parentCompany.children && parentCompany.children.length > 0) {
        Shops.find({
            id: {
                $in: parentCompany.children
            }
        }, {
            fields: {
                _id: 1
            }
        }).forEach((shop) => {
            if (DEBUGGING) console.log('updating shop', shop._id, "parent companyId", parentCompany.companyId);
            Shops.update({_id: shop._id}, {
                $set: {
                    companyId: parentCompany.companyId
                }
            });
        });

        Companies.find({
            id: {
                $in: parentCompany.children
            }
        }, {
            fields: {
                _id: 1
            }
        }).forEach((company) => {
            Companies.update({
                _id: company._id
            }, {
                $set: {
                    parentId: parentCompany.companyId
                }
            });
        });
    }

    if (parentCompany.billingEntity) {
        // if the company is a billing entity and we also created a shop from the company
        // we need to make sure the child shop is not a billing entity
        Shops.update({
            id: id
        },{
            $set: {
                billingEntity: false,
                companyId: parentCompany.companyId
            }
        });
    } else {
        // update the shop with the hierarchy in case it was also created as a company
        // we need to link the shop to the new company
        Shops.update({
            id: id
        },{
            $set: {
                companyId: parentCompany.companyId
            }
        });
    }
};

const saveCompanies = function(csv, importFile, parentCompanies, countries, fileObject, callback) {
    console.log('saveCompanies importFile', importFile);

    let lines = 0;
    csv({
        delimiter: ';',
        noheader: false,
        trim: true,
        flatKeys: true,
        constructResult: false
    }).fromFile(importFile)
        .on('json', Meteor.bindEnvironment((row) => {
            if (row) {
                let updateData = Vatfree.import.processColumns(colMapping, convert, row);
                let id = updateData['id'];
                if (parentCompanies[id]) {
                    lines++;
                    updateData['name'] = updateData['billingName'] || updateData['accountName'] || updateData['name'];
                    cleanupUpdateData(updateData, countries);

                    if (updateData['billingEntity']) parentCompanies[id].billingEntity = true;

                    if (!updateData['name']) {
                        console.error('ERROR: No name for company', id);
                    } else {
                        updateData['partnershipStatus'] = partnerShipStatusConvert[updateData['partnershipStatus']];
                        let companyId = saveCompany(updateData);
                        parentCompanies[id].companyId = companyId;
                        changeChildCompanyId(id, parentCompanies[id]);
                    }
                }
            }
        }))
        .on('done', Meteor.bindEnvironment((error) => {
            Vatfree.setImportFileProgress(70, 'Companies done - doing hierarchy', fileObject);

            if (callback) {
                callback();
            }
        }));
};

const saveCompany = function (updateData) {
    let company = Companies.findOne({
        'id': updateData['id']
    });

    updateData.textSearch =  (
        (updateData.name || "") + ' ' +
        (updateData.addressFirst || "") + ' ' +
        (updateData.postCode || "") + ' ' +
        (updateData.city || "")
    ).toLowerCase().latinize();

    // set all languages to English
    if (company) {
        Companies.update({
            _id: company._id
        }, {
            $set: updateData
        });
        if (DEBUGGING) console.log('saving company', company._id);
        return company._id;
    } else {
        updateData['createdAt'] = new Date();
        if (DEBUGGING) console.log('saving new company', updateData['id']);
        return Companies.insert(updateData);
    }
};

Vatfree['import-companies'] = function (importFile, fileObject, callback) {
    Vatfree.setImportFileProgress(10, 'Populating lookup data', fileObject);

    let countries = {};
    Countries.find().forEach(function (country) {
        countries[country.code] = country;
    });

    let lines = 0;
    let parentCompanies = {};

    Vatfree.setImportFileProgress(12, 'Initializing csv', fileObject);
    csv({
        delimiter: ';',
        noheader: false,
        trim: true,
        flatKeys: true,
        constructResult: false
    }).fromFile(importFile)
        .on('json', Meteor.bindEnvironment((row) => {
            lines++;
            if (row &&  _.size(row) > 2) {
                // first process parent account if applicable
                let accountId = row["Account ID"];
                let parentAccountId = row["Parent Account ID"];

                if (!parentCompanies[accountId]) {
                    parentCompanies[accountId] = {
                        children: []
                    }
                }

                if (parentAccountId) {
                    if (!parentCompanies[parentAccountId]) {
                        parentCompanies[parentAccountId] = {
                            children: []
                        }
                    }
                    parentCompanies[parentAccountId].children.push(accountId);
                }
            }
        }))
        .on('done', Meteor.bindEnvironment((error) => {
            Vatfree.setImportFileProgress(20, 'Parsing data done - importing companies', fileObject);

            Meteor.setTimeout(() => {
                saveCompanies(csv, importFile, parentCompanies, countries, fileObject, function() {
                    console.log('end', error);
                    Vatfree.setImportFileProgress(100, 'Done', fileObject);
                    if (callback) {
                        callback();
                    }
                });
            }, 5000);
        }));
};

export {
    colMapping,
    convert,
    partnerShipStatusConvert,
    affiliateMapping,
    statusMapping,
    subscriptionMapping,
    cleanupUpdateData,
    saveCompanies,
    saveCompany
};

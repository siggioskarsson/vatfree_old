import { Meteor } from "meteor/meteor";

export const getEntityExportSelector = function (searchTerm, status, listFilters, partnershipStatusFilter, billingEntityFilter) {
    let selector = {};
    if (searchTerm) {
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    if (status && status.length > 0) {
        selector['status'] = {
            $in: status
        };
    }

    _.each(listFilters, (listFilterValue, listFilterId) => {
        selector[listFilterId] = listFilterValue;
    });

    if (partnershipStatusFilter.length > 0) {
        selector.partnershipStatus = {
            $in: partnershipStatusFilter
        };
    }

    if (billingEntityFilter) {
        selector['$and'] = [
            {
                billingEntity: {
                    $ne: true
                }
            },
            {
                billingCompanyId: {
                    $exists: false
                }
            }
        ];
    }
    return selector;
};

Meteor.methods({
    'get-company'(companyId) {
        this.unblock();
        return Companies.findOne({_id: companyId});
    },
    'search-companies'(searchTerm, limit, offset) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {
            deleted: {
                $exists: false
            }
        };
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let companies = [];
        Companies.find(selector, {
            sort: {
                name: 1
            },
            limit: limit,
            offset: offset
        }).forEach((company) => {
            companies.push({
                text: company.name + ' - ' + (company.addressFirst || "") + ', ' + (company.postalCode || "") + ' ' + (company.city || ""),
                id: company._id
            });
        });

        return companies;
    },
    'search-companies-excluding-children'(companyId, searchTerm, limit, offset) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {
            ancestors: {
                $ne: companyId
            }
        };
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let companies = [];
        Companies.find(selector, {
            sort: {
                name: 1
            },
            limit: limit,
            offset: offset
        }).forEach((company) => {
            companies.push({
                text: company.name + ' - ' + (company.addressFirst || "") + ', ' + (company.postalCode || "") + ' ' + (company.city || ""),
                id: company._id
            });
        });

        return companies;
    },
    'fix-ancestors'() {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Companies.find({}, {
            fields: {
                _id: 1
            }
        }).forEach((company) => {
            Companies.updateAncestors(company._id);
        });
    },
    'company-get-linked-elements'(companyId) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let contacts = Contacts.find({'profile.companyId': companyId}, {fields: {_id: 1}}).count();
        let shops = Shops.find({companyId: companyId}, {fields: {_id: 1}}).count();
        let companies = Companies.find({parentId: companyId}, {fields: {_id: 1}}).count();
        let invoices = Invoices.find({companyId: companyId}, {fields: {_id: 1}}).count();
        let activityLogs = ActivityLogs.find({companyId: companyId}, {fields: {_id: 1}}).count();

        return {
            contacts: contacts,
            companies: companies,
            shops: shops,
            invoices: invoices,
            activityLogs: activityLogs
        };
    },
    'merge-company'(companyId, mergeWithCompanyId) {
        check(companyId, String);
        check(mergeWithCompanyId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let newCompany = Companies.findOne({_id: mergeWithCompanyId});
        if (!newCompany) {
            throw new Meteor.Error(404, 'Could not find shop to merge into');
        }

        Shops.find({
            companyId: companyId
        }).forEach((shop) => {
            Shops.update({
                _id: shop._id
            },{
                $set: {
                    companyId: mergeWithCompanyId
                }
            });
        });

        Companies.find({
            parentId: companyId
        }).forEach((company) => {
            Companies.update({
                _id: company._id
            },{
                $set: {
                    parentId: mergeWithCompanyId
                }
            });
        });

        Invoices.find({
            companyId: companyId
        }).forEach((invoice) => {
            Invoices.update({
                _id: invoice._id
            },{
                $set: {
                    companyId: mergeWithCompanyId
                }
            });
        });

        Contacts.find({
            'profile.companyId': companyId
        }).forEach((contact) => {
            Contacts.update({
                _id: contact._id
            },{
                $set: {
                    'profile.companyId': mergeWithCompanyId
                }
            });
        });

        ActivityLogs.find({
            companyId: companyId
        }).forEach((activityLog) => {
            ActivityLogs.update({
                _id: activityLog._id
            },{
                $set: {
                    companyId: mergeWithCompanyId
                }
            });
        });

        return true;
    },
    'get-company-stats'(selector) {
        if (!Vatfree.userIsInRole(this.userId, 'companies-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const pipeline = [
            {
                $match: selector,
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 }
                }
            },
        ];
        return Companies.rawCollection().aggregate(pipeline).toArray();
    },
    'companies-export'(searchTerm, status, listFilters, partnershipStatusFilter, billingEntityFilter) {
        if (!Vatfree.userIsInRole(this.userId, 'companies-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const Baby = require('babyparse');
        let selector = getEntityExportSelector(searchTerm, status, listFilters, partnershipStatusFilter, billingEntityFilter);

        let data = [];
        Companies.find(selector, {
            sort: {
                createdAt: 1
            },
            fields: {
                textSearch: false
            }
        }).forEach((company) => {
            company.stats = JSON.stringify(company.stats);
            data.push(company);
        });
        let csv = Baby.unparse(data);

        return csv;
    },
    'companies-debt-collection-export'(searchTerm, status, listFilters, partnershipStatusFilter, billingEntityFilter) {
        if (!Vatfree.userIsInRole(this.userId, 'companies-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const Baby = require('babyparse');
        let selector = getEntityExportSelector(searchTerm, status, listFilters, partnershipStatusFilter, billingEntityFilter);

        let data = [];
        Companies.find(selector, {
            sort: {
                createdAt: 1
            },
            fields: {
                textSearch: false
            }
        }).forEach((company) => {
            if (!company.stats) company.stats = {};
            let billingEmail = company.billingEmail;
            if (!billingEmail) {
                const contactEmails = [];
                Contacts.find({
                    'roles.__global_roles__': 'contact',
                    'profile.companyId': company._id
                }).forEach((contact) => {
                    if (contact.profile.email) {
                        contactEmails.push(contact.profile.email);
                    }
                });
                if (contactEmails.length > 0) {
                    billingEmail = _.uniq(contactEmails).join(', ');
                }
            }
            if (!billingEmail) {
                billingEmail = company.email;
            }
            const companyExport = {
                name: company.name,
                address: company.addressFirst + ', ' + company.city,
                email: billingEmail,
                phone: company.tel,
                partnershipStatus: company.partnershipStatus,
                nrInvoices: company.stats.numberOfInvoices || 0,
                refunds: company.stats.totalInvoicedRefunds ? Vatfree.numbers.formatAmount(company.stats.totalInvoicedRefunds) : '',
                fees: company.stats.totalInvoicedServiceFee ? Vatfree.numbers.formatAmount(company.stats.totalInvoicedServiceFee) : ''
            };
            data.push(companyExport);
        });
        let csv = Baby.unparse(data);

        return csv;
    }
});

Template.addCompanyModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-company').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-company')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-company').on('shown.bs.modal', function () {
            template.$('select[name="countryId"]').select2();
            template.$('select[name="parentId"]').select2({
                minimumInputLength: 1,
                multiple: false,
                allowClear: true,
                placeholder: {
                    id: "",
                    placeholder: "Select company ..."
                },
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-companies', params.data.q || "", limit, offset, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                console.log('get search companies', res);
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });
        });
        $('#modal-add-company').modal('show');
    });
});

Template.addCompanyModal.helpers({
});

Template.addCompanyModal.events({
    'click .cancel-add-company'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-company-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        Companies.insert(formData);
        template.hideModal();
    }
});

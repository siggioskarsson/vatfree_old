import './move-shops.html';

Template.companyMoveShops.helpers({
    getShopCount() {
        return Template.instance().data.shopIds.length;
    },
    onModalShow() {
        const template = Template.instance();
        Meteor.setTimeout(() => {
            Vatfree.templateHelpers.select2Search.call(template, 'search-companies', 'companyId', false, true);
        }, 600);
    }
});

Template.companyMoveShops.events({
    'click .company-move-shops-form-submit'(e, template) {
        e.preventDefault();
        const shopIds = template.data.shopIds;
        const newCompanyId = template.$('select[name="companyId"]').val();

        _.each(shopIds, (shopId) => {
            Shops.update({
                _id: shopId
            },{
                $set: {
                    companyId: newCompanyId
                }
            });
        });

        toastr.success("Shops moved to new parent company");
        $('input[name^="shops."]:checkbox:checked').each(function() {
            $(this).attr('checked', false);
        });
        $('.close-modal').trigger('click');
    }
});

import './shop-status.html';

Template.companySetShopStatus.helpers({
    getShopCount() {
        return Template.instance().data.shopIds.length;
    }
});

Template.companySetShopStatus.events({
    'click .company-move-shops-form-submit'(e, template) {
        e.preventDefault();
        const shopIds = template.data.shopIds;
        const newStatus = template.$('select[name="status"]').val();
        const newPartnershipStatus = template.$('select[name="partnershipStatus"]').val();

        const setData = {};
        if (newStatus) {
            setData.status = newStatus;
        }
        if (newPartnershipStatus) {
            setData.partnershipStatus = newPartnershipStatus;
        }

        if (_.size(setData)) {
            _.each(shopIds, (shopId) => {
                Shops.update({
                    _id: shopId
                },{
                    $set: setData
                });
            });
        }

        toastr.success("Changed status of shops");
        $('input[name^="shops."]:checkbox:checked').each(function() {
            $(this).attr('checked', false);
        });
        $('.close-modal').trigger('click');
    }
});

Template.companyHierarchy.onCreated(function() {
    this.showShops = new ReactiveVar();
    this.loadingShops = new ReactiveVar();
});

Template.companyHierarchy.onRendered(function() {
    const template = this;
    this.subscribe('company-hierarchy', this.data.companyId);
    this.autorun(() => {
        if (this.showShops.get()) {
            template.loadingShops.set(true);
            this.subscribe('company-hierarchy-shops', this.data.companyId, {
                onReady() {
                    template.loadingShops.set();
                }
            });
        }
    });
});

Template.companyHierarchy.events({
    'change input[name="showShops"]'(e, template) {
        e.preventDefault();
        template.showShops.set($(e.currentTarget).prop('checked'));
    }
});

Template.companyHierarchy.helpers({
    showShops() {
        return Template.instance().showShops.get();
    },
    loadingShops() {
        return Template.instance().loadingShops.get();
    },
    getTopLevelCompany() {
        let company = Companies.findOne({_id: this.companyId}) || {};
        return Companies.findOne({
            _id: {
                $in: company.ancestors || []
            },
            $or: [
                {
                    parentId: {
                        $exists: false
                    }
                },
                {
                    parentId: null
                }
            ]
        });
    }
});

Template.companyHierarchyCompany.helpers({
    isCurrentCompany () {
        const template = Template.instance();
        return template.data.currentCompanyId === this.company._id;
    },
    getCompanyShops() {
        return Shops.find({
            companyId: this.company._id
        });
    }
});

Template.companyHierarchyChildren.helpers({
    getChildren() {
        return Companies.find({
            parentId: this.companyId
        },{
            sort: {
                name: 1
            }
        });
    }
});

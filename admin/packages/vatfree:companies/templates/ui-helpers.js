UI.registerHelper('getCompany', function(companyId) {
    companyId = companyId || this.companyId || this._id;
    return Companies.findOne({_id: companyId});
});

UI.registerHelper('getCompanyDescription', function(companyId) {
    if (!companyId) companyId = this.companyId;

    let company = Companies.findOne({_id: companyId});
    if (company) {
        let country = Countries.findOne({_id: company.countryId}) || {};
        return company.name + ' - ' + (company.addressFirst || "") + ', ' + (company.postalCode || "") + ' ' + (company.city || "") + ', ' + (country.name || "");
    }

    return "";
});

UI.registerHelper('getCompanyName', function(companyId) {
    if (!companyId) companyId = this.companyId;

    let company = Companies.findOne({_id: companyId});
    if (company) {
        return company.name;
    }

    return "";
});

UI.registerHelper('getCompanyLogoFile', function(companyId) {
    if (!companyId) companyId = this.companyId || this._id;

    return Files.findOne({
        itemId: companyId,
        target: 'companies',
        category: 'logo'
    },{
        sort: {
            createdAt: 1
        }
    });
});

var FileSaver = require('file-saver');
import moment from 'moment';

Template.Companies.onCreated(Vatfree.templateHelpers.onCreated(Companies, '/companies/', '/company/:itemId'));
Template.Companies.onRendered(Vatfree.templateHelpers.onRendered());
Template.Companies.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.Companies.onCreated(function() {
    this.partnershipStatusFilter = new ReactiveArray();
    this.billingEntityFilter = new ReactiveVar(Session.get(this.view.name + '.billingEntityFilter'));
    _.each(Session.get(this.view.name + '.partnershipStatusFilter') || [], (status) => {
        this.partnershipStatusFilter.push(status);
    });
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.partnershipStatusFilter', this.partnershipStatusFilter.array());
        Session.setPersistent(this.view.name + '.billingEntityFilter', this.billingEntityFilter.get());
    });

    this.selectorFunction = (selector) => {
        let partnershipStatusFilter = this.partnershipStatusFilter.array() || [];
        if (partnershipStatusFilter.length > 0) {
            selector.partnershipStatus = {
                $in: partnershipStatusFilter
            };
        }

        let billingEntityFilter = this.billingEntityFilter.get() || false;
        if (billingEntityFilter) {
            selector['billingEntity'] = true;
        }
    };
});

Template.Companies.helpers(Vatfree.templateHelpers.helpers);
Template.Companies.helpers(companyHelpers);
Template.Companies.helpers({
    getCompanyStats() {
        let template = Template.instance();
        let selector = companyHelpers.getSelector(template);

        let stats = ReactiveMethod.call('get-company-stats', selector);
        return stats && stats[0] ? stats[0] : false;
    }
});

Template.Companies.events(Vatfree.templateHelpers.events());
Template.Companies.events({
    'click .filter-icon.partnership-status-filter-icon'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        let status = this.toString();
        let partnershipStatusFilter = template.partnershipStatusFilter.array();

        if (_.contains(partnershipStatusFilter, status)) {
            template.partnershipStatusFilter.clear();
            _.each(partnershipStatusFilter, (_status) => {
                if (status !== _status) {
                    template.partnershipStatusFilter.push(_status);
                }
            });
        } else {
            template.partnershipStatusFilter.push(status);
        }
    },
    'click .filter-icon.billing-entity-filter-icon'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        let billingEntityFilter = template.billingEntityFilter.get();
        template.billingEntityFilter.set(!billingEntityFilter);
    },
    'click .companies-export'(e, template) {
        e.preventDefault();
        template.runningExport.set(true);

        Meteor.call('companies-export', template.searchTerm.get(), template.statusFilter.array(), template.listFilters.get(), template.partnershipStatusFilter.array(), template.billingEntityFilter.get(), (err, csv) => {
            var blob = new Blob([csv], {
                type: "text/plain;charset=utf-8"
            });
            FileSaver.saveAs(blob, "companies_export_" + moment().format(TAPi18n.__('_date_time_format')) + ".csv");
            template.runningExport.set(false);
        });
    },
    'click .companies-debt-collection-export'(e, template) {
        e.preventDefault();
        template.runningExport.set(true);

        Meteor.call('companies-debt-collection-export', template.searchTerm.get(), template.statusFilter.array(), template.listFilters.get(), template.partnershipStatusFilter.array(), template.billingEntityFilter.get(), (err, csv) => {
            var blob = new Blob([csv], {
                type: "text/plain;charset=utf-8"
            });
            FileSaver.saveAs(blob, "companies_export_" + moment().format(TAPi18n.__('_date_time_format')) + ".csv");
            template.runningExport.set(false);
        });
    }
});

Template.company_merge.onCreated(function() {
    let template = this;

    this.isLoaded = new ReactiveVar();
    this.isMerging = new ReactiveVar();
    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-companies-merge').modal('hide');
    };

    this.companyId = new ReactiveVar();
    this.counts = new ReactiveVar();

    Tracker.afterFlush(() => {
        $('#modal-companies-merge').on('hidden.bs.modal', function () {
            if (template.parentTemplate && template.parentTemplate.mergeCompany) template.parentTemplate.mergeCompany.set(false);
        });
        $('#modal-companies-merge').on('shown.bs.modal', function () {
            template.isLoaded.set(true);

            template.$('select[name="companyId"]').select2({
                minimumInputLength: 1,
                multiple: false,
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-companies', params.data.q || "", limit, offset, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });
        });
        $('#modal-companies-merge').modal('show');
    });
});

Template.company_merge.onRendered(function() {
    let template = this;

    this.autorun(() => {
        let companyId = this.data.companyId;
        if (companyId) {
            Meteor.call('company-get-linked-elements', companyId, (err, counts) => {
                template.counts.set(counts);
            });
        }
    });

    this.autorun(() => {
        let companyId = this.companyId.get();
        if (companyId) {
            this.subscribe('companies_item', companyId);
        }
    });
});

Template.company_merge.onDestroyed(function() {
    this.hideModal();

    if (this.parentTemplateVariable && this.parentTemplate[this.parentTemplateVariable]) {
        this.parentTemplate[this.parentTemplateVariable].set(false);
    }

    $('body').removeClass('modal-open');
    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.company_merge.helpers({
    isMerging() {
        return Template.instance().isMerging.get();
    },
    getCompany() {
        return Companies.findOne({_id: Template.instance().companyId.get() });
    },
    getCounts() {
        return Template.instance().counts.get();
    }
});

Template.company_merge.events({
    'change select[name="companyId"]'(e, template) {
        template.companyId.set($(e.currentTarget).val());
    },
    'click .cancel-merge-company'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.hideModal();
    },
    'submit form[name="send-merge-company-form"]'(e, template) {
        e.preventDefault();
        let companyId = this.companyId;
        let mergeWithCompanyId = template.$('select[name="companyId"]').val();

        if (companyId === mergeWithCompanyId) {
            alert('Cannot merge company with self');
            return false;
        }

        if (!confirm('Merge companies?')) {
            return false;
        }

        template.isMerging.set(true);
        Meteor.call('merge-company', companyId, mergeWithCompanyId, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                FlowRouter.go('/company/' + mergeWithCompanyId);
                Companies.update({
                    _id: companyId
                },{
                    $set: {
                        deleted: new Date()
                    }
                });
                template.hideModal();
            }
            template.isMerging.set(false);
        });
    }
});

/* global companyHelpers: true */

companyHelpers = {
    getSelector: function (template) {
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
            let searchTerms = searchTerm.split(' ');

            selector['$and'] = [];
            _.each(searchTerms, (s) => {
                selector['$and'].push({textSearch: new RegExp(s)});
            });
        }

        template.selectorFunction(selector);
        Vatfree.search.addStatusFilter.call(template, selector);
        Vatfree.search.addListFilter.call(template, selector);

        return selector;
    },
    getCompanies() {
        let template = Template.instance();
        let selector = companyHelpers.getSelector(template);

        return Companies.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    isActivePartnershipStatusFilter() {
        let partnershipStatusFilter = Template.instance().partnershipStatusFilter.array() || [];
        return _.contains(partnershipStatusFilter, this.toString()) ? 'active' : '';
    },
    isActiveBillingEntityFilter() {
        return Template.instance().billingEntityFilter.get() ? 'active' : '';
    }
};

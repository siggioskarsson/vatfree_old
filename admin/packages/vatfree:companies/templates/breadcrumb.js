Template.companyBreadcrumb.helpers({
    getAncestors() {
        let ancestors = _.clone(this.ancestors) || [];

        return ancestors.reverse();
    }
});

import moment from 'moment';

Template.CompanyInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.CompanyInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.CompanyInfo.helpers(companyHelpers);
Template.CompanyInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

Template.companyCard.events({
    'click .request-material'(e) {
        e.preventDefault();
        e.stopPropagation();

        const company = this;
        const country = Countries.findOne({_id: company.countryId});
        import swal from "sweetalert2";
        swal({
            title: "Create envelop activity?",
            showCloseButton: true,
            showCancelButton: true,
            cancelButtonText: "Cancel",
            confirmButtonText: "Yes, create it",
            confirmButtonColor: '#ed5565',
            reverseButtons: true,
            closeOnConfirm: true
        }).then(function () {
            const activityLogId = ActivityLogs.insert({
                type: 'task',
                status: 'new',
                subject: `ENVELOPPEN ${company.name}, ${company.addressFirst}, ${company.city} FORMS`,
                shopId: company._id,
                userId: "mMXRAJdMjxvod8hdt", // Michiel
                due: moment().add(1, 'day').toDate(),
                notes: `${company.name}\n${company.addressFirst}\n${company.postalCode} ${company.city}\n${country.name}\n\n`
            });

            FlowRouter.go(`/activity-log/${activityLogId}`);
        }, function (dismiss) {
        });
    }
});

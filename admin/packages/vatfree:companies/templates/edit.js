import moment from "moment/moment";
import { editFormOnCreated, editFormHelpers, editFormEvents, setSubListSorting } from 'meteor/vatfree:core-templates/templates/edit-form-helpers';
import './edit.html';
import 'meteor/vatfree:invoices/templates/filtered-list';
import 'meteor/vatfree:invoices/templates/add';

Template.CompanyEdit.onCreated(function () {
    this.activeShopId = new ReactiveVar();
    this.activeInvoiceId = new ReactiveVar();
});

Template.CompanyEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('companies_item', FlowRouter.getParam('itemId') || "", true);
        this.subscribe('email-templates-list', "", {}, {_id: 1}, 1000, 0); // get all email templates
    });
});

Template.CompanyEdit.helpers({
    itemDoc() {
        return Companies.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    activeInvoiceId() {
        return Template.instance().activeInvoiceId.get();
    },
    getSelectedInvoice() {
        return Invoices.findOne({
            _id: Template.instance().activeInvoiceId.get()
        });
    },
    activeShopId() {
        return Template.instance().activeShopId.get();
    },
    getSelectedShop() {
        return Shops.findOne({
            _id: Template.instance().activeShopId.get()
        });
    },
    showBreadcrumb() {
        return this.ancestors && this.ancestors.length > 1;
    },
    addActiveInvoiceId() {
        this.activeInvoiceId = Template.instance().activeInvoiceId.get();
        return this;
    },
    addActiveShopId() {
        this.activeShopId = Template.instance().activeShopId.get();
        return this;
    }
});

Template.CompanyEdit.events({
    'click table.invoice-list .item-row'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeInvoiceId) {
            template.activeShopId.set();
            if (template.activeInvoiceId.get() === this._id) {
                template.activeInvoiceId.set();
            } else {
                template.activeInvoiceId.set(this._id);
            }
        }
    },
    'click table.shop-list .item-row'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeShopId) {
            template.activeInvoiceId.set();
            if (template.activeShopId.get() === this._id) {
                template.activeShopId.set();
            } else {
                template.activeShopId.set(this._id);
            }
        }
    }
});

Template.CompanyEditForm.onCreated(editFormOnCreated());
Template.CompanyEditForm.onCreated(function() {
    this.vatRates = new ReactiveArray();
    this.addingContact = new ReactiveVar();
    this.addingActivityLog = new ReactiveVar();
    this.addingInvoice = new ReactiveVar();
    this.activeInvoiceId = new ReactiveVar();
    this.activeShopId = new ReactiveVar();

    this.invoiceSort = new ReactiveVar({
        invoiceNr: -1
    });
    this.shopSort = new ReactiveVar({
        createdAt: -1
    });
    this.contactSort = new ReactiveVar({
        createdAt: -1
    });
    this.sendOpenInvoiceMail = new ReactiveVar();

    this.mergeCompany = new ReactiveVar();
    this.movingShops = new ReactiveVar();
    this.changeShopStatus = new ReactiveVar();

    this.autorun(() => {
        let data = Template.currentData() || {};
        this.activeInvoiceId.set(data.activeInvoiceId);
    });

    this.autorun(() => {
        let data = Template.currentData() || {};
        this.activeShopId.set(data.activeShopId);
    });
});

Template.CompanyEditForm.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('activity-logs', "", {companyId: data._id});
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.vatRates.clear();
        if (data.vatRates) {
            let key = 0;
            _.each(data.vatRates, (rate) => {
                this.vatRates.push({
                    key: key++,
                    rate: rate.rate,
                    description: rate.description
                });
            });
        }
    });

    let formatPartnershipStatus = function (status) {
        let button = '';
        switch (status.id) {
            case 'new':
                button = 'VFCircleUnkown.svg';
                break;
            case 'pledger':
                button = 'VFCircleBlue.svg';
                break;
            case 'affiliate':
                button = 'vfcircleaffiliate.svg';
                break;
            case 'partner':
                button = 'VFCircleShade.svg';
                break;
            case 'uncooperative':
            default:
                button = 'VFCircleNonrefund.svg';
                break;
        }

        if (button) {
            return $(
                '<span class="select2-item"><img src="/Logos/' + button + '" class="img-responsive" /> ' + status.text + '</span>'
            );
        } else {
            return status.text;
        }
    };
    Tracker.afterFlush(() => {
        this.$('select[name="affiliatedWith"]').select2();
        this.$('select[name="partnershipStatus"]').select2({
            templateSelection: formatPartnershipStatus,
            templateResult: formatPartnershipStatus
        });

        let endDate = moment().format(TAPi18n.__('_date_format'));
        Vatfree.templateHelpers.initDatepicker.call(this, null, endDate);
        Vatfree.templateHelpers.select2Search.call(this, 'search-companies', 'companyId', false, true);
    });

    this.autorun(() => {
        let data = Template.currentData();
        Tracker.afterFlush(() => {
            this.$('select[name="countryId"]').select2();

            this.$('select[name="parentId"]').select2({
                minimumInputLength: 1,
                multiple: false,
                allowClear: true,
                placeholder: {
                    id: "",
                    placeholder: "Select company ..."
                },
                ajax: {
                    transport: function (params, success, failure) {
                        let limit = 20;
                        let offset = 0;
                        Meteor.call('search-companies-excluding-children', data._id, params.data.q || "", limit, offset, (err, res) => {
                            if (err) {
                                failure(err);
                            } else {
                                console.log('get search companies', res);
                                success({results: res});
                            }
                        });
                    },
                    delay: 500
                }
            });
        });
    });

    this.autorun(() => {
        if (this.tabLoading.get('tab-2')) {
            let data = Template.currentData();
            this.subscribe('company-shops', data._id, (ready) => {
                this.tabLoading.set('tab-2', true);
            });
        }
    });

    this.autorun(() => {
        if (this.tabLoading.get('tab-3')) {
            let data = Template.currentData();
            this.subscribe('invoices', '', {companyId: data._id}, (ready) => {
                this.tabLoading.set('tab-3', true);
            });
        }
    });
});

Template.CompanyEditForm.helpers(editFormHelpers());
Template.CompanyEditForm.helpers(companyHelpers);
Template.CompanyEditForm.helpers({
    mergeCompany() {
        return Template.instance().mergeCompany.get();
    },
    movingShops() {
        return Template.instance().movingShops.get();
    },
    changeShopStatus() {
        return Template.instance().changeShopStatus.get();
    },
    sendOpenInvoiceMail() {
        return Template.instance().sendOpenInvoiceMail.get();
    },
    isCountrySelected() {
        let template = Template.instance();
        if (template.data) {
            return template.data.countryId === this._id;
        }
    },
    getShops() {
        let template = Template.instance();
        let sort = template.shopSort.get();
        return Shops.find({
            companyId: this._id
        },{
            sort: sort
        });
    },
    getShopSorting() {
        return Template.instance().shopSort.get();
    },
    getUsers() {
        return Meteor.users.find({});
    },
    getInvoices() {
        let template = Template.instance();
        let sort = template.invoiceSort.get();
        return Invoices.find({
            companyId: this._id
        }, {
            sort: sort
        });
    },
    getInvoicesSelector() {
        return {
            companyId: this._id
        };
    },
    getInvoiceSorting() {
        return Template.instance().invoiceSort.get();
    },
    isDisabled() {
        return this.deleted;
    },
    isEmailTemplateSelected() {
        let template = Template.instance();
        return template.data.emailTemplateId === this._id;
    },
    addingContact() {
        return Template.instance().addingContact.get();
    },
    addingActivityLog() {
        return Template.instance().addingActivityLog.get();
    },
    addingInvoice() {
        return Template.instance().addingInvoice.get();
    },
    getContacts() {
        return Contacts.find({
            'profile.companyId': this._id
        },{
            sort: Template.instance().contactSort.get()
        });
    },
    getContactSorting() {
        return Template.instance().contactSort.get();
    },
    getActivityLog() {
        return ActivityLogs.find({
            companyId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getFileContext() {
        return {
            itemId: this._id,
            target: 'companies',
            category: null
        };
    },
    getLogoFileContext() {
        return {
            itemId: this._id,
            target: 'companies',
            category: 'logo'
        };
    },
    getContactActions() {
        return [
            {
                icon: 'icon icon-trash',
                action: function(id) {
                    import swal from 'sweetalert2';
                    swal({
                        title: 'Are you sure?',
                        text: "Remove this contact from this company?",
                        showCloseButton: true,
                        showCancelButton: true,
                        cancelButtonText: 'Cancel',
                        confirmButtonText: 'Yes, delete contact',
                        confirmButtonColor: '#ed5565',
                        reverseButtons: true,
                        closeOnConfirm: true
                    }).then(function () {
                        Meteor.call('delete-company-contact', id, (err) => {
                            if (err) {
                                toastr.error(err.reason || err.message);
                            }
                        });
                    }, function (dismiss) {
                    });
                }
            }
        ]
    }
});

Template.CompanyEditForm.events(editFormEvents());
Template.CompanyEditForm.events(Vatfree.templateHelpers.events());
Template.CompanyEditForm.events({
    'click .load-shops'(e, template) {
        e.preventDefault();
        if (!template.tabLoading.get()) {
            template.tabLoading.set('tab-2', 'loading');
        }
    },
    'click .add-contact'(e, template) {
        e.preventDefault();
        template.addingContact.set(true);
    },
    'click .add-activity-log'(e, template) {
        e.preventDefault();
        template.addingActivityLog.set(true);
    },
    'click .add-invoice'(e, template) {
        e.preventDefault();
        template.addingInvoice.set(true);
    },
    'click table.invoice-list th.sort-header'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        setSubListSorting(e, template, 'invoiceSort');
    },
    'click table.shop-list th.sort-header'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        setSubListSorting(e, template, 'shopSort');
    },
    'click table.contact-list th.sort-header' (e, template) {
        e.preventDefault();
        e.stopPropagation();
        setSubListSorting(e, template, 'contactSort');
    },
    'click .send-open-invoice-mail'(e, template) {
        e.preventDefault();
        template.sendOpenInvoiceMail.set(true);
    },
    'click .company-move-shops'(e, template) {
        e.preventDefault();
        template.movingShops.set();
        const shopIds = [];
        template.$('input[name^="shops."]:checkbox:checked').each(function() {
            let shop = Blaze.getData(this);
            shopIds.push(shop._id);
        });
        template.movingShops.set(shopIds);
    },
    'click .company-change-status-shops'(e, template) {
        e.preventDefault();
        template.changeShopStatus.set();
        const shopIds = [];
        template.$('input[name^="shops."]:checkbox:checked').each(function() {
            let shop = Blaze.getData(this);
            shopIds.push(shop._id);
        });
        template.changeShopStatus.set(shopIds);
    },
    'click .company-delete-shops'(e, template) {
        e.preventDefault();
        import swal from 'sweetalert2';
        swal({
            title: 'Are you sure?',
            text: "Delete all selected shops?",
            showCloseButton: true,
            showCancelButton: true,
            cancelButtonText: 'Cancel',
            confirmButtonText: 'Yes, delete shops',
            confirmButtonColor: '#ed5565',
            reverseButtons: true,
            closeOnConfirm: true
        }).then(function () {
            let shopsMarked = 0;
            template.$('input[name^="shops."]:checkbox:checked').each(function() {
                let shop = Blaze.getData(this);
                Shops.update({
                    _id: shop._id
                },{
                    $set: {
                        deleted: new Date()
                    }
                });
                shopsMarked++;
                $(this).attr('checked', false);
            });

            toastr.success('Moved ' + shopsMarked + ' shops');
        }, function (dismiss) {
        });
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/companies');
    },
    'submit form[name="item-edit-form"], submit form[name="billing-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template, $(e.currentTarget));

        if (_.has(formData, 'partneredAt')) {
            if (formData.partneredAt) {
                formData.partneredAt = moment(formData.partneredAt, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.partneredAt = null;
            }
        }

        if (_.has(formData, 'minFee')) {
            formData.minFee = formData.minFee > 0 ? formData.minFee * 100 : null;
        }
        if (_.has(formData, 'maxFee')) {
            formData.maxFee = formData.maxFee > 0 ? formData.maxFee * 100 : null;
        }

        if (_.has(formData, 'debtCollectionDate')) {
            if (formData.debtCollectionDate) {
                formData.debtCollectionDate = moment(formData.debtCollectionDate, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.debtCollectionDate = null;
            }
        }
        if (_.has(formData, 'partnerDashboardFrom')) {
            if (formData.partnerDashboardFrom) {
                formData.partnerDashboardFrom = moment(formData.partnerDashboardFrom, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.partnerDashboardFrom = null;
            }
        }

        // Check whether we have all debt collection fields, if we have 1 of the fields
        if (formData['debtCollectionAccount'] || formData['debtCollectionMandate'] || formData['debtCollectionDate']) {
            if (!formData['debtCollectionAccount'] || !formData['debtCollectionMandate'] || !formData['debtCollectionDate']) {
                toastr.error('Please fill in all the debt collection fields');
                return false;
            }
        }

        let setData = {
            $set: formData
        };
        if (_.has(formData, 'parentId') && !formData.parentId) {
            setData['$unset'] = {
                parentId: 1
            }
        }
        Companies.update({
            _id: this._id
        }, setData, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Company saved!')
            }
        });
    },
    'click .merge-company'(e, template) {
        e.preventDefault();
        template.mergeCompany.set(true);
    }
});

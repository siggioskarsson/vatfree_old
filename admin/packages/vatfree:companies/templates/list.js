Template.companiesList.helpers(Vatfree.templateHelpers.helpers);
Template.companiesList.helpers(companyHelpers);
Template.companiesList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/company/:itemId', {itemId: this._id});
    }
});

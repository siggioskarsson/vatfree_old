Package.describe({
    name: 'vatfree:companies',
    summary: 'Vatfree companies package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'manuel:reactivearray@1.0.5',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core',
        'vatfree:countries',
        'vatfree:email-templates',
        'vatfree:invoices'
    ]);

    api.addAssets([
    ], 'server');

    // shared files
    api.addFiles([
        'companies.js'
    ]);

    // server files
    api.addFiles([
        'server/methods.js',
        'server/companies.js',
        'server/import.js',
        'publish/companies.js',
        'publish/company.js',
        'publish/hierarchy.js',
        'publish/hierarchy-shops.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/breadcrumb.less',
        'css/company.less',
        'css/hierarchy.less',
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/breadcrumb.html',
        'templates/breadcrumb.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/hierarchy.html',
        'templates/hierarchy.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/merge.html',
        'templates/merge.js',
        'templates/companies.html',
        'templates/companies.js',
        'templates/modals/move-shops.html',
        'templates/modals/move-shops.js',
        'templates/modals/shop-status.html',
        'templates/modals/shop-status.js'
    ], 'client');

    api.export([
        'Companies'
    ]);
});

FlowRouter.route('/companies', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "Companies"});
    }
});

FlowRouter.route('/company/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "CompanyEdit"});
    }
});

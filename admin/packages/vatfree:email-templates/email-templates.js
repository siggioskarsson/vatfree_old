EmailTemplates = new Meteor.Collection('email-templates');
EmailTemplates.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Name of the email template",
        optional: false
    },
    description: {
        type: String,
        label: "Description of this email template",
        optional: true
    },
    templateText: {
        type: String,
        label: "Text template",
        optional: true
    },
    templateHTML: {
        type: String,
        label: "HTML template",
        optional: true
    },
    textSearch: {
        type: String,
        autoValue: function() {
            if (this.field("name").value) {
                return ((this.field("name").value || "") + ' ' + (this.field("description").value || "")).toLowerCase().latinize();
            } else {
                this.unset();
            }
        },
        optional: true
    }
}));
EmailTemplates.attachSchema(CreatedUpdatedSchema);

EmailTemplates.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

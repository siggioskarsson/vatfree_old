Meteor.publish('email-templates-list', function(searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
        let searchTerms = searchTerm.split(' ');

        selector['$and'] = [];
        _.each(searchTerms, (s) => {
            selector['$and'].push({textSearch: new RegExp(s)});
        });
    }

    return EmailTemplates.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset,
        fields: {
            name: 1,
            description: 1
        }
    });
});

import handlebars from 'handlebars';

Template.emailTemplatePreview.onRendered(function () {
    this.autorun(() => {
        this.subscribe('email-templates_item', FlowRouter.getParam('itemId'));
        this.subscribe('web-texts');
    });
});

Template.emailTemplatePreview.helpers({
    getHTMLPreview() {
        let template = EmailTemplates.findOne({
            _id: FlowRouter.getParam('itemId')
        });
        if (template) {
            let text = template.templateHTML || "";
            let templateData = {
                name: "Mr. Siggi Oskarsson",
                text: `Thank you for choosing our service to help you reclaim VAT.

Before we can start the process, we will need
a) the original stamped purchase receipt;
b) a signed authorization (see attachment);
c) a copy of your passport stating you live in a country outside the EU.*

<b>Please sign the document in the attachment</b> to confirm your registration and e-mail it to support@vatfree.com. To digitally sign this document, use the free app SignEasy. It's a cool app that lets you sign documents directly on your mobile device in seconds. Download the App at http://getsigneasy.com. Use the vatfree.com referral code LMgopCft to sign 5 documents for free.

*If you have not made use of our service before, please e-mail us a scan (or photo) of your passport. Make sure to scramble your photo and social security number for safety reasons. Note Was your passport issued in the EU? Then also send us a document confirming your country of residence (visa, work permit etc.).

As soon as your claim is complete we will start contacting stores to request a VAT reimbursement.

If you have any questions, visit the <a href="http://vatfreecom.desk.com/customer/en/portal/articles">FAQ</a> of our website
or call us +31 (0) 88 828 37 33.

Help us to improve our service! :)
Please submit your rating for our service at the desk <a href="https://supportvatfreecom.typeform.com/to/dbSfCL">Click here</a>.
Have a safe flight!`
            };

            Vatfree.notify.registerHandlebarsHelpers(handlebars);

            return handlebars.compile(text)(templateData);
        }
    }
});

Template.addEmailTemplateModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-email-template').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-email-template')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-email-template').on('shown.bs.modal', function () {
            //
        });
        $('#modal-add-email-template').modal('show');
    });
});

Template.addEmailTemplateModal.onDestroyed(function() {
    $('#modal-add-email-template').modal('hide');
});

Template.addEmailTemplateModal.helpers({
});

Template.addEmailTemplateModal.events({
    'click .cancel-add-email-template'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-email-template-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        EmailTemplates.insert(formData);
        template.hideModal();
    }
});

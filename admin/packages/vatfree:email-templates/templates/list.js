Template.emailTemplatesList.helpers(Vatfree.templateHelpers.helpers);
Template.emailTemplatesList.helpers(emailTemplateHelpers);
Template.emailTemplatesList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/email-template/:itemId', {itemId: this._id});
    }
});

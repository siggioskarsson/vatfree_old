Template.emailTemplates.onCreated(Vatfree.templateHelpers.onCreated(EmailTemplates, '/email-templates/', '/email-template/:itemId'));
Template.emailTemplates.onRendered(Vatfree.templateHelpers.onRendered());
Template.emailTemplates.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.emailTemplates.helpers(Vatfree.templateHelpers.helpers);
Template.emailTemplates.helpers(emailTemplateHelpers);
Template.emailTemplates.events(Vatfree.templateHelpers.events());

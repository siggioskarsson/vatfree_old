UI.registerHelper('getEmailTemplateName', function(emailTemplateId) {
    if (!emailTemplateId) emailTemplateId = this.emailTemplateId;

    let emailTemplate = EmailTemplates.findOne({_id: emailTemplateId});
    if (emailTemplate) {
        return emailTemplate.name;
    }

    return "";
});

UI.registerHelper('getEmailTemplates', function() {
    return EmailTemplates.find({}, {
        sort: {
            name: 1
        }
    });
});

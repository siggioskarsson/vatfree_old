import handlebars from 'handlebars';
import autosize from 'meteor/vatfree:core-templates/templates/autosize';

Template.emailTemplateEdit.onCreated(function () {

});

Template.emailTemplateEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('email-templates_item', FlowRouter.getParam('itemId'));
        this.subscribe('web-texts');
    });
});

Template.emailTemplateEdit.helpers({
    itemDoc() {
        return EmailTemplates.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    getBreadcrumbTitle() {
        return this.name + ' (' + this.code + ')';
    }
});

Template.emailTemplateEditForm.onCreated(function() {
    this.vatRates = new ReactiveArray();
});

Template.emailTemplateEditForm.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.vatRates.clear();
        if (data.vatRates) {
            let key = 0;
            _.each(data.vatRates, (rate) => {
                this.vatRates.push({
                    key: key++,
                    rate: rate.rate,
                    description: rate.description
                });
            });
        }
    });

    Tracker.afterFlush(() => {
        autosize(this.$('textarea'));
    });
});

Template.emailTemplateEditForm.helpers({
    getTextPreview() {
        let text = this.templateText || "";
        let templateData = {
            name: "Mr. Siggi Oskarsson",
            text: `Thank you for choosing our service to help you reclaim VAT.

Before we can start the process, we will need
a) the original stamped purchase receipt;
b) a signed authorization (see attachment);
c) a copy of your passport stating you live in a country outside the EU.*

Please sign the document in the attachment to confirm your registration and e-mail it to support@vatfree.com. To digitally sign this document, use the free app SignEasy. It's a cool app that lets you sign documents directly on your mobile device in seconds. Download the App at http://getsigneasy.com. Use the vatfree.com referral code LMgopCft to sign 5 documents for free.

*If you have not made use of our service before, please e-mail us a scan (or photo) of your passport. Make sure to scramble your photo and social security number for safety reasons. Note Was your passport issued in the EU? Then also send us a document confirming your country of residence (visa, work permit etc.).

As soon as your claim is complete we will start contacting stores to request a VAT reimbursement.

If you have any questions, visit the FAQ of our website
or call us +31 (0) 88 828 37 33.

Help us to improve our service! :)
Please submit your rating for our service at the desk Click here.
Have a safe flight!`
        };

        Vatfree.notify.registerHandlebarsHelpers(handlebars);

        return Vatfree.notify.stripHtmlTags(handlebars.compile(text)(templateData));
    }
});

Template.emailTemplateEditForm.events({
    'click .add-vat-rate'(e, template) {
        e.preventDefault();
        template.vatRates.push({
            key: template.vatRates.length,
            rate: "",
            description: ""
        });
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/email-templates');
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        let rates = [];
        _.each(formData.rate, (rate, key) => {
            rates.push({
                rate: rate,
                description: formData.rateDescription[key]
            });
        });
        formData.vatRates = rates;
        delete formData.rate;
        delete formData.rateDescription;

        EmailTemplates.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Email Template saved!')
            }
        });
    }
});

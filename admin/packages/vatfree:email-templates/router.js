FlowRouter.route('/email-templates', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "emailTemplates"});
    }
});

FlowRouter.route('/email-template/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "emailTemplateEdit"});
    }
});

FlowRouter.route('/email-template/:itemId/preview', {
    action: function() {
        BlazeLayout.render("blankLayout", {content: "emailTemplatePreview"});
    }
});

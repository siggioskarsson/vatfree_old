UI.registerHelper('getNotificationTriggerName', function(notificationTriggerId) {
    if (!notificationTriggerId) notificationTriggerId = this.notificationTriggerId;

    let notificationTrigger = NotificationTriggers.findOne({_id: notificationTriggerId});
    if (notificationTrigger) {
        return notificationTrigger.name;
    }

    return "";
});

UI.registerHelper('getNotificationTriggerCode', function(notificationTriggerId) {
    if (!notificationTriggerId) notificationTriggerId = this.notificationTriggerId;

    let notificationTrigger = NotificationTriggers.findOne({_id: notificationTriggerId});
    if (notificationTrigger) {
        return notificationTrigger.code;
    }

    return "";
});

UI.registerHelper('getNotificationTriggerSymbol', function(notificationTriggerId) {
    if (!notificationTriggerId) notificationTriggerId = this.notificationTriggerId;

    let notificationTrigger = NotificationTriggers.findOne({_id: notificationTriggerId});
    if (notificationTrigger) {
        return notificationTrigger.symbol;
    }

    return "";
});

UI.registerHelper('getNotificationTriggers', function() {
    return NotificationTriggers.find({}, {
        sort: {
            name: 1
        }
    });
});

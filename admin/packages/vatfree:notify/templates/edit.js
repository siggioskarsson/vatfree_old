import swal from 'sweetalert';

Template.notificationTriggerEdit.onCreated(function () {
});

Template.notificationTriggerEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('notification-triggers_item', FlowRouter.getParam('itemId'));
    });
});

Template.notificationTriggerEdit.helpers({
    itemDoc() {
        return NotificationTriggers.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    getBreadcrumbTitle() {
        return this.name;
    }
});

Template.notificationTriggerEditForm.onCreated(function() {
    this.selectedCollection = new ReactiveVar(this.data.collection);

    this.subscribe('email-templates-list', '');
    this.subscribe('email-texts-list', '');
});

Template.notificationTriggerEditForm.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();

    });
});

Template.notificationTriggerEditForm.helpers(notificationTriggerHelpers);
Template.notificationTriggerEditForm.helpers({

});

Template.notificationTriggerEditForm.events({
    'change select[name="collection"]'(e, template) {
        template.selectedCollection.set($(e.currentTarget).val());
    },
    'click .delete-trigger'(e) {
        e.preventDefault();
        const trigger = this;

        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "Deleting this trigger will disabled notification based on it.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function () {
            NotificationTriggers.remove({
                _id: trigger._id
            });
            toastr.success('Notification trigger deleted!');
            FlowRouter.go('/notification-triggers');
        });
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/notification-triggers');
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        NotificationTriggers.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Notification trigger saved!')
            }
        });
    }
});

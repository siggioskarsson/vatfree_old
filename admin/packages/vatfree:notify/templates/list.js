Template.notificationTriggersList.helpers(Vatfree.templateHelpers.helpers);
Template.notificationTriggersList.helpers(notificationTriggerHelpers);
Template.notificationTriggersList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/notification-trigger/:itemId', {itemId: this._id});
    }
});

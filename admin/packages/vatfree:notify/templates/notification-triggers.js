Template.notificationTriggers.onCreated(Vatfree.templateHelpers.onCreated(NotificationTriggers, '/notificationTriggers/', '/notificationTrigger/:itemId'));
Template.notificationTriggers.onRendered(Vatfree.templateHelpers.onRendered());
Template.notificationTriggers.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.notificationTriggers.helpers(Vatfree.templateHelpers.helpers);
Template.notificationTriggers.helpers(notificationTriggerHelpers);
Template.notificationTriggers.events(Vatfree.templateHelpers.events());

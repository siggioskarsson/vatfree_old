/* global notificationTriggerHelpers: true, notificationTriggerCollections: true */

notificationTriggerHelpers = {
    getNotificationTriggers() {
        let template = Template.instance();
        let selector = {};

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
            let searchTerms = searchTerm.split(' ');

            selector['$and'] = [];
            _.each(searchTerms, (s) => {
                selector['$and'].push({textSearch: new RegExp(s)});
            });
        }

        return NotificationTriggers.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    getStatusOptions() {
        let template = Template.instance();
        let collection = template.selectedCollection.get();
        let collections = notificationTriggerCollections.get();
        if (collection && collections && _.has(collections, collection)) {
            if (collection === 'travellers') {
                // Yay exceptions !
                return _.clone(Meteor.users.simpleSchema()._schema['private.status'].allowedValues);
            } else if (collection === 'affiliates') {
                // Yay exceptions !
                return _.clone(Meteor.users.simpleSchema()._schema['affiliate.status'].allowedValues);
            } else {
                return _.clone(collections[collection].simpleSchema()._schema.status.allowedValues);
            }
        }
    },
    getPreviousStatus() {
        const previousStatus = this.previousStatus || "";
        if (previousStatus) {
            return Vatfree.translate(previousStatus);
        }

        return "*";
    }
};

notificationTriggerCollections = new ReactiveVar();
Meteor.startup(() => {
    notificationTriggerCollections.set({
        'receipts': Receipts,
        'shops': Shops,
        'invoices': Invoices,
        'travellers': Travellers,
        'payouts': Payouts,
        'affiliates': Affiliates
    });
});

Template.addNotificationTriggerModal.onCreated(function() {
    let template = this;
    let parentVariable = this.data.parentVariable || 'addingItem';

    this.selectedCollection = new ReactiveVar();

    this.subscribe('email-templates', '');
    this.subscribe('email-texts-list', '');

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-notificationTrigger').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-notificationTrigger')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-add-notificationTrigger').on('shown.bs.modal', function () {
            //
        });
        $('#modal-add-notificationTrigger').modal('show');
    });
});

Template.addNotificationTriggerModal.helpers(notificationTriggerHelpers);
Template.addNotificationTriggerModal.helpers({
    isEmailTemplateSelected() {
        let template = Template.instance();
        return template.data.emailTemplateId === this._id;
    }
});

Template.addNotificationTriggerModal.events({
    'change select[name="collection"]'(e, template) {
        template.selectedCollection.set($(e.currentTarget).val());
    },
    'click .cancel-add-notificationTrigger'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-notificationTrigger-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        NotificationTriggers.insert(formData);
        template.hideModal();
    }
});

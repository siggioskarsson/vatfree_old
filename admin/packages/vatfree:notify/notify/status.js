Vatfree.notify.checkStatusChange = function (collection, userId, previousDoc, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status && previousDoc.status !== modifier.$set.status) {
        this.processNotificationTriggers(userId, collection, modifier.$set.status, previousDoc.status, doc._id, options);
    }
};

Vatfree.notify.processNotificationTriggers = function(userId, collection, status, previousStatus, docId, options) {
    options = options || {};
    previousStatus = previousStatus || { $exists: false };
    if (_.has(this.sendStatusNotification, collection)) {
        // find the first notification trigger that matches with the new status
        // any definition with a previous status will be preferred over empty previous status
        NotificationTriggers.find({
            collection: collection,
            status: status,
            $or: [
                {
                    previousStatus: previousStatus
                },
                {
                    previousStatus: null

                }
            ]
        },{
            limit: 1,
            sort: {
                previousStatus: -1
            }
        }).forEach((notificationTrigger) => {
            this.sendStatusNotification[collection](userId, docId, notificationTrigger, options);
        });
    } else {
        throw new Meteor.Error(500, 'Unknown collection or no handler available for status change notification');
    }
};

Vatfree.notify.sendStatusNotification = {};

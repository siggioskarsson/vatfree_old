import handlebars from 'handlebars';
import moment from 'moment';

Vatfree.notify.registerHandlebarsHelpers(handlebars);

Vatfree.notify.createMagicLink = function (emailText, toAddress, templateData) {
    if (emailText.match('{{createMagicLink}}')) {
        let code = Accounts.passwordLessCodes.findOne({email: toAddress});
        if (code) {
            // code already exists and has not been used, re-use
            templateData.magicLink = code;
        } else {
            templateData.magicLink = Accounts.passwordless.getOrCreateVerificationCode(toAddress);
        }
        let siteUrl = Meteor.absoluteUrl();
        if (Meteor.settings.travellerSiteUrl) {
            siteUrl = Meteor.settings.travellerSiteUrl;
        }
        templateData.magicLink.url = siteUrl + "login/" + encodeURIComponent(encodeURIComponent(toAddress) + "/" + templateData.magicLink.code);
    }
};

Vatfree.notify.createPasswordResetCode = function (emailText, toAddress, templateData) {
    if (emailText.match('{{passwordResetCode}}')) {
        let dbCode = Accounts.passwordLessCodes.findOne({email: toAddress + '_passwordReset'});
        if (dbCode) {
            // code already exists and has not been used, re-use
            templateData.passwordResetCode = dbCode.code;
        } else {
            let code = Math.round(Random.fraction() * 1000000);
            Accounts.passwordLessCodes.upsert({email: toAddress + '_passwordReset'}, {$set: {code: code}});
            templateData.passwordResetCode = code;
        }
    }
};

Vatfree.notify.sendNotification = function (travellerStatusNotify, templateData, toAddress, fromAddress, activityIds, language, traveller = false, options = {}) {
    let templateText = EmailTexts.findOne({_id: travellerStatusNotify.emailTextId});
    if (!templateText || !templateText.text || (!templateText.text[language] && !templateText.text['en'])) {
        throw new Meteor.Error(500, 'Could not find email text to send to traveller: ' + travellerStatusNotify.emailTextId);
    }
    if (!templateText || !templateText.subject || (!templateText.subject[language] && !templateText.subject['en'])) {
        throw new Meteor.Error(500, 'Could not find email subject to send to traveller:' + travellerStatusNotify.emailTextId);
    }

    let emailSubject = templateText.subject[language] || templateText.subject['en'];
    let renderedSubject = handlebars.compile(emailSubject)(templateData);

    let emailText = templateText.text[language] || templateText.text['en'];

    // Add magic link if applicable
    this.createMagicLink(emailText, toAddress, templateData);
    this.createPasswordResetCode(emailText, toAddress, templateData);

    templateData.text = handlebars.compile(emailText)(templateData);

    let template = EmailTemplates.findOne({_id: travellerStatusNotify.emailTemplateId});
    let renderedEmailHtml = handlebars.compile(template.templateHTML || "")(templateData);

    templateData.text = Vatfree.notify.stripHtmlTags(templateData.text);
    let renderedEmailText = Vatfree.notify.stripHtmlTags(handlebars.compile(template.templateText || "")(templateData));

    // TODO abstract this to a function
    const activityLog = {
        type: 'email',
        subject: renderedSubject,
        address: toAddress,
        status: 'done',
        notes: renderedEmailText,
        html: renderedEmailHtml
    };
    _.each(activityIds, (id, idKey) => {
        activityLog[idKey] = id;
    });
    if (options['hideInMessageBox']) {
        activityLog.hideInMessageBox = true;
    }
    let messageId = ActivityLogs.insert(activityLog);

    // email can be missing, but we still want the above activity log entry
    if (toAddress) {
        Email.send({
            to: toAddress,
            from: fromAddress,
            subject: renderedSubject,
            text: renderedEmailText,
            html: renderedEmailHtml,
            messageId: messageId
        });
    }

    if (traveller && !options['suppressMobileNotification']) {
        Vatfree.notify.sendPushNotification(travellerStatusNotify, templateData, activityIds, traveller, messageId);
    }

    return true;
};

Vatfree.notify.sendPushNotification = function (travellerStatusNotify, templateData, activityIds, traveller, messageId) {
    const templateText = EmailTexts.findOne({_id: travellerStatusNotify.emailTextId});
    if (!templateText || !templateText.text || !templateText.text['en']) {
        throw new Meteor.Error(500, 'Could not find push notification text to send to traveller: ' + travellerStatusNotify.emailTextId);
    }
    if (!templateText || !templateText.subject || !templateText.subject['en']) {
        throw new Meteor.Error(500, 'Could not find push notification text to send to traveller: ' + travellerStatusNotify.emailTextId);
    }

    if (!Meteor.settings.oneSignalApiKey) {
        console.error('oneSignalApiKey not set');
        return false;
    }

    const devices = _.filter(traveller.private.pushStatus || {}, (pushStatus) => {
        const daysSinceUpdate = moment().diff(pushStatus.updatedAt, 'days');
        return daysSinceUpdate <= 100; // only people that have opened the app in the last 100 days
    });
    if (!devices.length) {
        // No valid devices found for userId
        return false;
    }

    const userId = traveller._id;
    const contents = {};
    _.each(templateText.subject, (subject, language) => {
        if (_.isString(subject) && subject.length) {
            const renderedSubject = handlebars.compile(subject)(templateData);
            const pushLanguage = language === 'zh-CN' ? 'zh-Hans' : language;
            contents[pushLanguage] = Vatfree.notify.stripHtmlTags(renderedSubject);
        }
    });

    const addToActivityLog = function (error = {}, result = {}) {
        const activityLog = {
            type: 'push',
            subject: contents.en,
            status: 'done',
            notes: JSON.stringify(contents),
        };
        _.each(activityIds, (id, idKey) => {
            activityLog[idKey] = id;
        });
        if (error) {
            activityLog.error = error;
        } else {
            activityLog.address = JSON.stringify(result.data);
        }
        ActivityLogs.insert(activityLog);
    };

    try {
        // console.log('Push notification', userId, contents);
        const apiUrl = 'https://onesignal.com/api/v1/notifications';
        const apiKey = Meteor.settings.oneSignalApiKey;
        HTTP.call('POST', apiUrl, {
            data: {
                app_id: Meteor.settings.public.oneSignalAppId,
                filters: [{"field": "tag", "key": "user_id", "relation": "=", "value": userId}],
                contents: contents,
                data: {
                    travellerId: traveller._id,
                    messageId: messageId
                }
            },
            headers: {
                Authorization: `Basic ${apiKey}`
            }
        }, addToActivityLog);
    } catch (e) {
        console.error('Push notification failed', e);
    }

    return true;
};

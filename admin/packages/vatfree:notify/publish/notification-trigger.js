Meteor.publish('notification-triggers_item', function(itemId) {
    check(itemId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return NotificationTriggers.find({
       _id: itemId
    });
});

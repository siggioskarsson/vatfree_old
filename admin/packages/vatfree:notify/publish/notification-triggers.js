Meteor.publishComposite('notification-triggers', function(searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        find: function () {
            return NotificationTriggers.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                find: function (notificationTrigger) {
                    return EmailTemplates.find({
                        _id: notificationTrigger.emailTemplateId
                    });
                }
            },
            {
                find: function (notificationTrigger) {
                    return EmailTexts.find({
                        _id: notificationTrigger.emailTextId
                    });
                }
            }
        ]
    };
});

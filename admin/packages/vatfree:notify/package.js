Package.describe({
    name: 'vatfree:notify',
    summary: 'Vatfree notify package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'email',
        'http',
        'underscore',
        'reactive-var@1.0.11',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core',
        'vatfree:email-templates'
    ]);

    // shared files
    api.addFiles([
        'namespace.js',
        'router.js',
        'notification-triggers.js',
        'lib/handlebars.js',
        'lib/strip-html-tags.js'
    ]);

    // server files
    api.addFiles([
        'server/notification-triggers.js',
        'notify/status.js',
        'notify/send-notification.js',
        'notify/collection/shops.js',
        'publish/notification-triggers.js',
        'publish/notification-trigger.js'
    ], 'server');

    // client files
    api.addFiles([
        'templates/helpers.js',
        'templates/ui-helpers.js',
        'templates/add.html',
        'templates/add.js',
        'templates/edit.html',
        'templates/edit.js',
        'templates/info.html',
        'templates/info.js',
        'templates/list.html',
        'templates/list.js',
        'templates/notification-triggers.html',
        'templates/notification-triggers.js'
    ], 'client');

    api.export([
        'NotificationTriggers'
    ]);
});

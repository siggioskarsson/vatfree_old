NotificationTriggers = new Meteor.Collection('notification-triggers');
NotificationTriggers.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "The name of this trigger",
        optional: false
    },
    description: {
        type: String,
        label: "A descriptive text of this trigger",
        optional: true
    },
    collection: {
        type: String,
        label: "The name of the collection (in mongo) to match",
        optional: false
    },
    status: {
        type: String,
        label: "The status of the above collection to match",
        optional: false
    },
    previousStatus: {
        type: String,
        label: "The previous status of the above collection to match",
        optional: true
    },
    active: {
        type: Boolean,
        label: "Whether this trigger is active",
        optional: false,
        defaultValue: true
    },
    emailTemplateId: {
        type: String,
        label: "The email template to us for this trigger",
        optional: false
    },
    emailTextId: {
        type: String,
        label: "The text to use for this trigger",
        optional: false
    },
    textSearch: {
        type: String,
        label: "Text search cache",
        optional: true,
        autoValue: function() {
            if (this.field("name").value) {
                let emailTemplateName = (EmailTemplates.findOne({_id: this.field("emailTemplateId").value}) || {}).name;
                let emailTextName = (EmailTexts.findOne({_id: this.field("emailTextId").value}) || {}).name;

                return (
                    (this.field("name").value || "") + ' ' +
                    (this.field("description").value || "") + ' ' +
                    (this.field("collection").value || "") + ' ' +
                    (this.field("status").value || "") + ' ' +
                    (emailTemplateName || "") + ' ' +
                    (emailTextName || "")
                ).toLowerCase().latinize();
            } else {
                this.unset();
            }
        }
    }
}));
NotificationTriggers.attachSchema(CreatedUpdatedSchema);

NotificationTriggers.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    }
});

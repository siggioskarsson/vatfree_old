FlowRouter.route('/notification-triggers', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "notificationTriggers"});
    }
});

FlowRouter.route('/notification-trigger/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "notificationTriggerEdit"});
    }
});

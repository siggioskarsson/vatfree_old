import stripTags from 'underscore.string/stripTags';
const Entities = require('html-entities').AllHtmlEntities;

Vatfree.notify.stripHtmlTags = function(htmlText) {
    // change links into raw form
    let text = htmlText.replace(/(<a href="([^"]+)"[^>]*>([^<]+)<\/a>)/g, "$3: $2");

    const entities = new Entities();
    text = entities.decode(text);

    // remove all remaining tags
    return stripTags(text);
};

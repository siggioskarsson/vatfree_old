let getWebText = function (language, code) {
    if (!_.isString(language)) {
        language = this.language || 'en';
    }
    let webText = WebTexts.findOne({code: code});
    if (webText && webText.text) {
        return webText.text[language] || webText.text['en'];
    }

    return '';
};

Vatfree.notify.registerHandlebarsHelpers = function (handlebars) {
    handlebars.registerHelper('formatDate', Vatfree.dates.formatDate);
    handlebars.registerHelper('formatAmount', Vatfree.numbers.formatAmount);
    handlebars.registerHelper('formatCurrency', function (value, decimals, currencyId) {
        currencyId = currencyId || this.currencyId;
        let currency = Currencies.findOne({_id: currencyId}) || {};
        return Vatfree.numbers.formatCurrency(value, decimals, currency.symbol);
    });
    handlebars.registerHelper('getReceiptRejectionText', function (language, contextData) {
        let receiptRejectionId = this.toString();
        let receiptRejection = ReceiptRejections.findOne({_id: receiptRejectionId});
        let text = receiptRejection.text ? receiptRejection.text[language] || receiptRejection.text['en'] : receiptRejection.name;

        if (contextData) {
            text = handlebars.compile(text)(contextData);
        }

        return text;
    });
    handlebars.registerHelper('getTravellerRejectionText', function (language, contextData) {
        let travellerRejectionId = this.toString();
        let travellerRejection = TravellerRejections.findOne({_id: travellerRejectionId});
        let text = travellerRejection.text ? travellerRejection.text[language] || travellerRejection.text['en'] : travellerRejection.name;

        if (contextData) {
            text = handlebars.compile(text)(contextData);
        }

        return text;
    });
    handlebars.registerHelper('createMagicLink', function () {
        let email = this.traveller ? this.traveller.email : this.toString();
        if (Meteor.isClient) {
            this.magicLink = {
                email: email,
                code: 'test',
                url: Meteor.absoluteUrl() + "login/" + encodeURIComponent(email) + "/test"
            };
        }

        return "";
    });
    handlebars.registerHelper('statusDescription', function (language) {
        return this.receipt ? TAPi18n.__(this.receipt.status, {}, language) : "";
    });
    handlebars.registerHelper('isStatus', function (status) {
        return this.receipt ? this.receipt.status === status : false;
    });
    handlebars.registerHelper('isReferral', function (referral) {
        return this.receipt ? this.receipt.referral === referral : false;
    });
    handlebars.registerHelper('isChannel', function (channel) {
        return this.receipt ? this.receipt.channel === channel : false;
    });
    handlebars.registerHelper('isSource', function (source) {
        return this.receipt ? this.receipt.source === source : false;
    });
    handlebars.registerHelper('isTravellerStatus', function (status) {
        return this.traveller ? this.traveller.status === status : false;
    });
    handlebars.registerHelper('$eq', function (a, b) {
        return a === b;
    });
    handlebars.registerHelper('$neq', function (a, b) {
        return a !== b;
    });
    handlebars.registerHelper('$gt', function (a, b) {
        return a > b;
    });
    handlebars.registerHelper('$gte', function (a, b) {
        return a >= b;
    });
    handlebars.registerHelper('$lt', function (a, b) {
        return a < b;
    });
    handlebars.registerHelper('$lte', function (a, b) {
        return a <= b;
    });
    handlebars.registerHelper('$or', function (a, b) {
        return a || b;
    });
    handlebars.registerHelper('$and', function (a, b) {
        return a && b;
    });
    handlebars.registerHelper('insertWebText', function (code, language) {
        return getWebText.call(this, language, code);
    });
    handlebars.registerHelper('insertWebTextAsText', function (code, language) {
        const webText = getWebText.call(this, language, code);
        return Vatfree.notify.stripHtmlTags(webText);
    });
};

Package.describe({
    name: 'vatfree:receipt-types',
    summary: 'Vatfree receipt types package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'underscore',
        'random',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'tap:i18n',
        'tap:i18n-db',
        'hermanitos:activity-stream@0.0.1',
        'vatfree:core'
    ]);

    // shared files
    api.addFiles([
        'receipt-types.js'
    ]);

    // server files
    api.addFiles([
        'publish/receipt-types.js',
        'publish/receipt-types-i18n.js'
    ], 'server');

    // client files
    api.addFiles([
        'templates/ui-helpers.js'
    ], 'client');

    api.export([
        'ReceiptTypes'
    ]);
});

ReceiptTypes = new TAPi18n.Collection('receipt-types');
ReceiptTypes.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Name of the receipt type",
        optional: false
    },
    extraFields: {
        type: Object,
        label: "The extra fields that need to be filled in",
        optional: true,
        blackbox: true
    },
    icon: {
        type: String,
        label: "Icon to use for this type",
        optional: true
    },
    originalsNotNeeded: {
        type: Boolean,
        label: "Whether the originals are not needed when submitting a receipt",
        optional: true
    },
    notes: {
        type: String,
        label: "Notes on this receipt type",
        optional: true
    },
    textSearch: {
        type: String,
        autoValue: function() {
            if (this.field("name").value) {
                return ((this.field("name").value || "") + ' ' + (this.field("notes").value || "")).toLowerCase().latinize();
            } else {
                this.unset();
            }
        },
        optional: true
    }
}));
ReceiptTypes.attachSchema(CreatedUpdatedSchema);
ReceiptTypes.attachSchema(TranslatableSchema);

ReceiptTypes.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

if (Meteor.isServer) {
    Meteor.startup(() => {
        if (ReceiptTypes.find().count() === 0) {
            ReceiptTypes.insert({
                name: "Cheque",
                extraFields: {
                    type: {
                        name: "Cheque Type",
                        type: "select",
                        options: Vatfree.competitors,
                        unique: true,
                        required: true
                    },
                    amount: {
                        name: "Cheque Amount",
                        type: "number",
                        required: true
                    },
                    id: {
                        name: "Cheque id",
                        type: "text",
                        unique: true,
                        required: false
                    }
                }
            });
            ReceiptTypes.insert({
                name: "NATO",
                extraFields: {
                    formReceived: {
                        name: "Form received",
                        type: "boolean",
                        required: true
                    },
                    id: {
                        name: "Form id",
                        type: "text",
                        unique: true,
                        required: false
                    }
                }
            });
            ReceiptTypes.insert({
                name: "Cargo",
                extraFields: {
                    importDocsComplete: {
                        name: "Import docs complete",
                        type: "boolean",
                        required: false
                    },
                    exportDocsComplete: {
                        name: "Import docs complete",
                        type: "boolean",
                        required: true
                    }
                }
            });
        }
    });
}

UI.registerHelper('getReceiptTypeName', function(receiptTypeId) {
    if (!receiptTypeId) receiptTypeId = this.receiptTypeId;

    let receiptType = ReceiptTypes.findOne({_id: receiptTypeId});
    if (receiptType) {
        return receiptType.name;
    }

    return "Normal";
});

UI.registerHelper('getReceiptTypeIcon', function(receiptTypeId) {
    if (!receiptTypeId) receiptTypeId = this.receiptTypeId;

    let receiptType = ReceiptTypes.findOne({_id: receiptTypeId});
    if (receiptType) {
        return receiptType.icon;
    }

    return "icon icon-receipt";
});

UI.registerHelper('getReceiptTypes', function() {
    return ReceiptTypes.find().fetch();
});

import moment from 'moment';

DebtCollection = new Meteor.Collection('debt-collection');
DebtCollection.attachSchema(new SimpleSchema({
    debtCollectionNumber: {
        type: String,
        label: "Unique name of this debt collection batch",
        autoValue: function() {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return getNextDebtCollectionNumber();
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: getNextDebtCollectionNumber()
                    };
                } else {
                    this.unset();
                }
            } else {
                this.unset();
            }
        },
        optional: true
    },
    amount: {
        type: String,
        label: "Total debt collection amount",
        optional: false
    },
    currencyId: {
        type: String,
        label: "Debt collection currency",
        optional: false
    },
    invoiceIds: {
        type: [String],
        label: "All invoice Ids in this debt collection",
        optional: false
    },
    debtCollectionLineIds: {
        type: [String],
        label: "All debt collection line ids in this debt collection",
        optional: false
    }
}));
DebtCollection.attachSchema(CreatedUpdatedSchema);

var getNextDebtCollectionNumber = function() {
    let year = moment().format('YYYY').toString();
    let counter = Number(incrementCounter(Counters, 'debtCollectionNumber_' + year)).padLeft(5).toString();

    let nextDebtCollectionNumber = year + counter;

    return "INC" + nextDebtCollectionNumber;
};

/*
    "Iban Geincasseerde","Mandaat ID",NLVAT_total,"VTB: VTB no.",Shoplookup,"INCA: INCA Name","shop name","TO (T.A.V.)",date
    NL30ABNA0626135699,109389-00001,41.62,855032017,"Invito Beheer Headquarters",INCA0084,"Invito Beheer B.V.",,08/06/2017
*/
DebtCollectionLines = new Meteor.Collection('debt-collection-lines');
DebtCollectionLines.attachSchema(new SimpleSchema({
    debtCollectionId: {
        type: String,
        label: "Id of debt collection",
        optional: true
    },
    invoiceId: {
        type: String,
        label: "Invoice id",
        optional: false
    },
    shopName: {
        type: String,
        label: "Shop name",
        optional: false
    },
    billingName: {
        type: String,
        label: "Billing name",
        optional: false
    },
    recipientName: {
        type: String,
        label: "Recipient name",
        optional: true
    },
    debtCollectionAccount: {
        type: String,
        label: "Debt collection account",
        optional: false
    },
    debtCollectionMandate: {
        type: String,
        label: "Debt collection mandate",
        optional: false
    },
    debtCollectionDate: {
        type: Date,
        label: "Debt collection signing date",
        optional: false
    },
    amount: {
        type: String,
        label: "Debt collection amount",
        optional: false
    },
    currencyId: {
        type: String,
        label: "Currency of this debt collection line",
        optional: false
    }
}));
DebtCollectionLines.attachSchema(CreatedUpdatedSchema);
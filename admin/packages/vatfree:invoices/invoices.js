import moment from 'moment';


const EuroAmountsSchema = new SimpleSchema({
    amount: {
        type: Number,
        decimal: true,
        optional: true
    },
    totalVat: {
        type: Number,
        decimal: true,
        optional: true
    },
    refund: {
        type: Number,
        decimal: true,
        optional: true
    },
    serviceFee: {
        type: Number,
        decimal: true,
        optional: true
    },
    affiliateServiceFee: {
        type: Number,
        decimal: true,
        optional: true
    },
    deferredServiceFee: {
        type: Number,
        decimal: true,
        optional: true
    }
});

Invoices = new Meteor.Collection('invoices');
Invoices.attachSchema(new SimpleSchema({
    salesforceId: {
        type: String,
        label: "Internal salesforce id",
        optional: true
    },
    invoiceNr: {
        type: Number,
        label: "Internally generated unique invoice number",
        autoValue: function() {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return getNextInvoiceNumber(this.field("type").value, this.field("referenceInvoiceId").value);
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: getNextInvoiceNumber(this.valueOf('type'), this.valueOf('referenceInvoiceId'))
                    };
                } else {
                    this.unset();
                }
            } else {
                this.unset();
            }
        },
        optional: false
    },
    secret: {
        type: String,
        label: "Secret access key to view invoice without a login",
        autoValue: function() {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return Random.id(64);
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: Random.id(64)
                    };
                } else {
                    this.unset();
                }
            } else {
                this.unset();
            }
        },
        optional: false
    },
    shopId: {
        type: String,
        label: "Shop this invoice was issued",
        optional: true
    },
    companyId: {
        type: String,
        label: "Company this invoice was issued",
        optional: true
    },
    countryId: {
        type: String,
        label: "Country this invoice was sent to",
        optional: false
    },
    partnershipStatus: {
        type: String,
        label: "Partnership status of the entity this invoice was sent to",
        optional: false
    },
    invoiceDate: {
        type: Date,
        label: "Invoice date of this invoice",
        optional: false
    },
    dueDate: {
        type: Date,
        label: "due date of this invoice",
        optional: true
    },
    currencyId: {
        type: String,
        label: "Currency of this invoice",
        optional: false
    },
    amount: {
        type: Number,
        label: "Total amount of invoice",
        optional: false
    },
    euroAmount: {
        type: Number,
        label: "Total amount of invoice in euro (if currency not the same)",
        optional: true
    },
    totalVat: {
        type: Number,
        label: "Total of VAT on the invoice",
        optional: false
    },
    serviceFee: {
        type: Number,
        label: "Total service fee",
        optional: false
    },
    deferredServiceFee: {
        type: Number,
        label: "Total deferred service fee",
        optional: true
    },
    affiliateServiceFee: {
        type: Number,
        label: "Total affiliate service fee",
        optional: true
    },
    refund: {
        type: Number,
        label: "Total refund",
        optional: false
    },
    euroAmounts: {
        type: EuroAmountsSchema,
        label: 'Euro amounts for all values for statistics',
        optional: true
    },
    receiptIds: {
        type: [String],
        label: "A list of receipts included on this invoice",
        optional: false
    },
    language: {
        type: String,
        label: "Language code",
        defaultValue: "nl",
        optional: true
    },
    status: {
        type: String,
        label: "Status of the invoice",
        optional: false,
        allowedValues: [
            'new',
            'toCheck',
            'readyToSend',
            'sentToRetailer',
            'promisedToPay',
            'partiallyPaid',
            'paymentDifference',
            'paid',
            'notCollectable',
            'credited'
        ]
    },
    type: {
        type: String,
        label: "Type of invoice, determines template to use to generate the PDF invoice",
        allowedValues: [
            'serviceFee',
            'credit'
        ],
        optional: true
    },
    referenceInvoiceId: {
        type: String,
        label: "Id of the invoice this invoice refers to",
        optional: true
    },
    invoiceSent: {
        type: Date,
        label: "Date the invoice was sent to the retailer",
        optional: true
    },
    promisedAt: {
        type: Date,
        label: "Date the invoice was promised to pay by the retailer",
        optional: true
    },
    sendBy: {
        type: String,
        label: "The method that was used to send this invoice",
        defaultValue: 'email',
        allowedValues: [
            'email',
            'post'
        ],
        optional: false
    },
    reminderSent: {
        type: Date,
        label: "Date the invoice reminder was sent to the retailer",
        optional: true
    },
    reminder2Sent: {
        type: Date,
        label: "Date the invoice second reminder was sent to the retailer",
        optional: true
    },
    remindersStop: {
        type: Date,
        label: "Date the invoice second reminder was sent to the retailer",
        optional: true
    },
    paymentTerms: {
        type: Number,
        label: "Number of days before the invoice should be paid",
        defaultValue: 14,
        optional: true
    },
    statusDates: {
        type: Object,
        label: "All the last dates of the status",
        optional: true
    },
    paymentIds: {
        type: [String],
        label: "Payments that were made to this invoice",
        optional: true
    },
    salesforcePaymentIds: {
        type: [String],
        label: "Payments that were made to this invoice in salesforce",
        optional: true
    },
    debtCollectionId: {
        type: String,
        label: "Debt collection Id if applicable",
        optional: true
    },
    paybackId: {
        type: String,
        label: "Payback Id if applicable",
        optional: true
    },
    paybackLineId: {
        type: String,
        label: "Payback line Id if applicable",
        optional: true
    },
    paidAt: {
        type: Date,
        label: "When the invoice was marked as paid",
        optional: true
    },
    paymentDifference: {
        type: Number,
        label: "Payment difference for this invoice",
        optional: true
    },
    receiptRejectionIds: {
        type: [String],
        label: "The id's of the receipt-rejections of this receipt",
        optional: true
    },
    synchronization: {
        type: [Vatfree.synchronizationSchema],
        label: "Synchronizations of the item to/from other systems",
        optional: true
    },
    notes: {
        type: String,
        label: "Invoice notes",
        optional: true
    },
    deleted: {
        type: Boolean,
        label: "Whether the invoice was deleted",
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
Invoices.attachSchema(CreatedUpdatedSchema);
StatusDates.attachToSchema(Invoices);

var getNextInvoiceNumber = function(type, referenceInvoiceId) {
    let year = moment().format('YY').toString();
    let counter = Number(incrementCounter(Counters, 'invoiceNr_' + year)).padLeft(7).toString();
    let nextInvoiceNumber = year + counter;

    return Math.round(Number(nextInvoiceNumber));
};

Invoices.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return false;
    }
});

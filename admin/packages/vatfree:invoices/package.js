Package.describe({
    name: 'vatfree:invoices',
    summary: 'Vatfree invoices package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'check',
        'email',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'lamhieu:unblock@1.0.0',
        'kadira:flow-router@2.12.1',
        'konecty:mongo-counter@0.0.5_3',
        'reywood:publish-composite@1.5.1',
        'flemay:less-autoprefixer@1.2.0',
        'hermanitos:activity-stream@0.0.1',
        'simonsimcity:job-collection',
        'percolate:synced-cron@1.3.2',
        'vatfree:core',
        'vatfree:countries',
        'vatfree:notify'
    ]);

    // shared files
    api.addFiles([
        'debt-collection.js',
        'invoices.js',
        'jobs.js',
        'payback.js',
        'lib/invoices.js'
    ]);

    // server files
    api.addFiles([
        'server/lib/reminders.js',
        'server/lib/send.js',
        'server/methods.js',
        'server/jobs.js',
        'server/cron.js',
        'server/debt-collection.js',
        'server/import.js',
        'server/invoices.js',
        'server/notify.js',
        'server/payback.js',
        'server/pdf.js',
        'server/synch/exact.js',
        'publish/debt-collection.js',
        'publish/debt-collection-lines.js',
        'publish/files.js',
        'publish/invoices.js',
        'publish/invoice.js',
        'publish/payback.js',
        'publish/payback-lines.js',
        'publish/receipts-for-invoicing.js',
        'publish/shop-invoices.js',
        'publish/stats.js'
    ], 'server');

    // client files
    api.addFiles([
        'router.js',
        'css/invoices.less',
        'css/tasks.less',
        'templates/ui-helpers.js'
    ], 'client');

    // Invoice templates
    api.addAssets([
        'templates/pdf/style.html',
        'templates/pdf/invoice.de.html',
        'templates/pdf/invoice.en.html',
        'templates/pdf/invoice.es.html',
        'templates/pdf/invoice.fr.html',
        'templates/pdf/invoice.nl.html',
        'templates/pdf/invoice-credit.de.html',
        'templates/pdf/invoice-credit.en.html',
        'templates/pdf/invoice-credit.es.html',
        'templates/pdf/invoice-credit.fr.html',
        'templates/pdf/invoice-credit.nl.html',
        'templates/pdf/invoice-images.html',
        'templates/pdf/invoice-serviceFee.en.html',
        'templates/pdf/invoice-serviceFee.nl.html'
    ], 'server');

    api.addAssets([
        'templates/debt-collection.xml',
        'templates/sepa-payback.xml'
    ], 'server');

    api.export([
        'Invoices',
        'DebtCollection',
        'DebtCollectionLines',
        'Payback',
        'PaybackLines',
        'invoiceJobs'
    ]);
});

import moment from 'moment';

Payback = new Meteor.Collection('payback');
Payback.attachSchema(new SimpleSchema({
    paybackNumber: {
        type: String,
        label: "Unique name of this payback collection batch",
        autoValue: function() {
            if (Meteor.isServer) {
                if (this.isInsert) {
                    return getNextPaybackNumber();
                } else if (this.isUpsert) {
                    return {
                        $setOnInsert: getNextPaybackNumber()
                    };
                } else {
                    this.unset();
                }
            } else {
                this.unset();
            }
        },
        optional: true
    },
    amount: {
        type: String,
        label: "Total payback collection amount",
        optional: false
    },
    invoiceIds: {
        type: [String],
        label: "All invoice Ids in this payback collection",
        optional: false
    },
    paybackLineIds: {
        type: [String],
        label: "All payback collection line ids in this payback collection",
        optional: false
    }
}));
Payback.attachSchema(CreatedUpdatedSchema);

var getNextPaybackNumber = function() {
    let year = moment().format('YYYY').toString();
    let counter = Number(incrementCounter(Counters, 'paybackNumber_' + year)).padLeft(5).toString();

    let nextPaybackNumber = year + counter;

    return "PB" + nextPaybackNumber;
};

PaybackLines = new Meteor.Collection('payback-lines');
PaybackLines.attachSchema(new SimpleSchema({
    paybackId: {
        type: String,
        label: "Id of payback collection",
        optional: true
    },
    invoiceId: {
        type: String,
        label: "Invoice id",
        optional: false
    },
    shopName: {
        type: String,
        label: "Shop name",
        optional: false
    },
    billingName: {
        type: String,
        label: "Billing name",
        optional: false
    },
    recipientName: {
        type: String,
        label: "Recipient name",
        optional: true
    },
    paybackAccount: {
        type: String,
        label: "Payback collection account",
        optional: false
    },
    amount: {
        type: String,
        label: "Payback collection amount",
        optional: false
    }
}));
PaybackLines.attachSchema(CreatedUpdatedSchema);
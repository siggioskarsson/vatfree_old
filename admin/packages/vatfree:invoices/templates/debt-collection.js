import './debt-collection.html';

Template.debtCollection_details.onCreated(function() {
    this.subscribe('debt-collection', FlowRouter.getParam('debtCollectionId'));
});

Template.debtCollection_details.helpers({
    debtCollection() {
        return DebtCollection.findOne({
            _id: FlowRouter.getParam('debtCollectionId')
        });
    },
    debtCollectionLines() {
        return DebtCollectionLines.find({
            debtCollectionId: this._id
        });
    },
    getInvoiceNr() {
        let invoice = Invoices.findOne({
            _id: this.invoiceId
        });
        if (invoice) {
            return invoice.invoiceNr;
        }
    }
});

Template.debtCollection_details.events({
    'click .save-debt-collection'(e, template) {
        e.preventDefault();

        let textToWrite = template.$('textarea').val();
        let textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
        let fileNameToSaveAs = 'debt-collection-' + this.debtCollectionNumber + '.csv';

        let downloadLink = document.createElement("a");
        downloadLink.download = fileNameToSaveAs;
        downloadLink.innerHTML = "Download File";
        if (window.webkitURL != null)
        {
            // Chrome allows the link to be clicked
            // without actually adding it to the DOM.
            downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
        }
        else
        {
            // Firefox requires the link to be added to the DOM
            // before it can be clicked.
            downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
            downloadLink.onclick = function(event) {
                document.body.removeChild(event.target);
            };
            downloadLink.style.display = "none";
            document.body.appendChild(downloadLink);
        }

        downloadLink.click();
    },
    'click .save-debt-collection-xml'(e, template) {
        e.preventDefault();
        import handlebars from 'handlebars';
        handlebars.registerHelper('formatDate', Vatfree.dates.formatDate);
        handlebars.registerHelper('formatAmount', Vatfree.numbers.formatAmount);
        handlebars.registerHelper('toUpperCase', function(string) {
            return (string || "").toString().replace(/\s/g, '').toUpperCase();
        });

        Meteor.call('get-debt-collection-xml-template', (err, xmlTemplate) => {
            let debtCollection = _.clone(this);

            let index = 1;
            debtCollection.debtCollectionLines = [];
            debtCollection.sumOfDebtCollections = 0;
            DebtCollectionLines.find({
                debtCollectionId: debtCollection._id
            }).forEach((debtCollectionLine) => {
                debtCollectionLine.index = index++;
                debtCollectionLine.invoice = Invoices.findOne({_id: debtCollectionLine.invoiceId});
                debtCollection.sumOfDebtCollections += Number(debtCollectionLine.amount);
                debtCollection.debtCollectionLines.push(debtCollectionLine);
            });
            debtCollection.numberOfDebtCollections = debtCollection.debtCollectionLines.length;

            const textToWrite = handlebars.compile(xmlTemplate)(debtCollection);

            let textFileAsBlob = new Blob([textToWrite], {type:'application/xml'});
            let fileNameToSaveAs = debtCollection.debtCollectionNumber + '.xml';

            Vatfree.templateHelpers.downloadFile(fileNameToSaveAs, textFileAsBlob);
        });
    }
});

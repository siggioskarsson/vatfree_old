/* global invoiceHelpers: true */
import moment from 'moment';

export const invoiceHelpers = {
    getInvoicesSelector(template) {
        let selector = {
            deleted: {
                $ne: true
            }
        };

        let searchTerm = template.searchTerm.get();
        if (searchTerm) {
            searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
            let searchTerms = searchTerm.split(' ');

            selector['$and'] = [];
            _.each(searchTerms, (s) => {
                selector['$and'].push({textSearch: new RegExp(s)});
            });
        }

        let statusFilter = template.statusFilter.array() || [];
        if (statusFilter.length > 0) {
            selector.status = {
                $in: statusFilter

            };
        }
        Vatfree.search.addListFilter.call(template, selector);

        if (_.isFunction(template.selectorFunction)) {
            template.selectorFunction(selector);
        }

        return selector;
    },
    getInvoices(template) {
        template = template || Template.instance();
        let selector = invoiceHelpers.getInvoicesSelector(template);

        return Invoices.find(selector, {
            sort: template.sort.get(),
            limit: template.limit.get(),
            offset: template.offset.get()
        });
    },
    getContactType() {
        switch (this.contactType) {
            case 'tel':
                return 'phone';
            case 'email':
            default:
                return 'envelope';

        }
    },
    getContactDetails() {
        return "-";
    },
    getPartnershipStatusLogo() {
        switch (this.partnershipStatus) {
            case 'pledger':
                return 'VFCircleBlue.svg';
            case 'partner':
                return 'VFCircleShade.svg';
            case 'uncooperative':
                return 'VFCircleNonrefund.svg';
            default:
                return 'VFCircleUnkown.svg';
        }
    },
    getTraveller() {
        // Travellers is not available, not known how to publishComposite into that collection
        return Meteor.users.findOne({_id: this.userId});
    },
    getTravellerName() {
        let traveller = Meteor.users.findOne({_id: this.userId});
        if (traveller && traveller.profile) {
            return traveller.profile.name;
        }
    },
    getShop() {
        return Shops.findOne({_id: this.shopId});
    },
    getCompany() {
        return Companies.findOne({_id: this.companyId});
    },
    daysOpen() {
        let mOpen = this.paidAt ? moment(this.paidAt) : moment();
        return moment(mOpen).diff(this.invoiceDate, 'days');
    }
};

invoiceEditFormAfterFlush = function() {
    $('.input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: TAPi18n.__('_date_format').toLowerCase()
    });
    $('select[name="userId"]').select2({
        minimumInputLength: 1,
        multiple: false,
        ajax: {
            transport: function (params, success, failure) {
                let limit = 20;
                let offset = 0;
                Meteor.call('search-travellers', params.data.q || "", limit, offset, (err, res) => {
                    if (err) {
                        failure(err);
                    } else {
                        success({results: res});
                    }
                });
            },
            delay: 500
        }
    });
    $('select[name="shopId"]').select2({
        minimumInputLength: 1,
        multiple: false,
        ajax: {
            transport: function (params, success, failure) {
                let limit = 20;
                let offset = 0;
                Meteor.call('search-shops', params.data.q || "", limit, offset, (err, res) => {
                    if (err) {
                        failure(err);
                    } else {
                        success({results: res});
                    }
                });
            },
            delay: 500
        }
    });
};

import './filtered-list.html';
import './list';

Template.invoicesFilteredList.onCreated(Vatfree.templateHelpers.onCreated(Invoices, '/invoices/', '/invoice/:itemId'));
Template.invoicesFilteredList.onRendered(Vatfree.templateHelpers.onRendered());
Template.invoicesFilteredList.onDestroyed(Vatfree.templateHelpers.onDestroyed());
Template.invoicesFilteredList.helpers(Vatfree.templateHelpers.helpers);
Template.invoicesFilteredList.events(Vatfree.templateHelpers.events());

Template.invoicesFilteredList.onCreated(function() {
    this.salesforceFilter = new ReactiveVar(Session.get(this.view.name + '.salesforceFilter'));
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.salesforceFilter', this.salesforceFilter.get());
    });
});

Template.invoicesFilteredList.helpers({
    getInvoices() {
        const template = Template.instance();
        const selector = JSON.parse(JSON.stringify(this.invoicesSelector));

        Vatfree.search.addStatusFilter.call(template, selector);
        let salesforceFilter = template.salesforceFilter.get();
        if (salesforceFilter) {
            if (salesforceFilter === 'active') {
                selector.salesforceId = {
                    $exists: true
                }
            }
            if (salesforceFilter === 'inactive') {
                selector.salesforceId = {
                    $exists: false
                }
            }
        }

        return Invoices.find(selector, {
            sort: this.sorting
        });
    },
    isActiveSalesforceFilter() {
        return Template.instance().salesforceFilter.get() === 'active';
    },
    isInactiveSalesforceFilter() {
        return Template.instance().salesforceFilter.get() === 'inactive';
    }
});

Template.invoicesFilteredList.events({
    'click .filter-icon.salesforce-filter-icon' (e, template) {
        e.preventDefault();
        const salesforceFilter = template.salesforceFilter.get();
        if (salesforceFilter === 'active') {
            template.salesforceFilter.set('inactive');
        } else if (salesforceFilter === 'inactive') {
            template.salesforceFilter.set(false);
        } else {
            template.salesforceFilter.set('active');
        }
    },
});

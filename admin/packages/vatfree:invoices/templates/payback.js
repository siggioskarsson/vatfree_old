import './payback.html';

Template.payback_details.onCreated(function() {
    this.subscribe('payback', FlowRouter.getParam('paybackId'));
});

Template.payback_details.helpers({
    payback() {
        return Payback.findOne({
            _id: FlowRouter.getParam('paybackId')
        });
    },
    paybackLines() {
        return PaybackLines.find({
            paybackId: this._id
        });
    },
    getInvoiceNr() {
        let invoice = Invoices.findOne({
            _id: this.invoiceId
        });
        if (invoice) {
            return invoice.invoiceNr;
        }
    }
});

Template.payback_details.events({
    'click .save-payback'(e, template) {
        e.preventDefault();

        let textToWrite = template.$('textarea').val();
        let textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
        let fileNameToSaveAs = 'payback-' + this.paybackNumber + '.csv';

        let downloadLink = document.createElement("a");
        downloadLink.download = fileNameToSaveAs;
        downloadLink.innerHTML = "Download File";
        if (window.webkitURL != null)
        {
            // Chrome allows the link to be clicked
            // without actually adding it to the DOM.
            downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
        }
        else
        {
            // Firefox requires the link to be added to the DOM
            // before it can be clicked.
            downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
            downloadLink.onclick = function(event) {
                document.body.removeChild(event.target);
            };
            downloadLink.style.display = "none";
            document.body.appendChild(downloadLink);
        }

        downloadLink.click();
    },
    'click .save-payback-sepa-xml'(e, template) {
        e.preventDefault();
        let templateData = JSON.parse(JSON.stringify(this));

        import handlebars from 'handlebars';
        handlebars.registerHelper('formatDate', Vatfree.dates.formatDate);
        handlebars.registerHelper('formatAmount', Vatfree.numbers.formatAmount);
        handlebars.registerHelper('toUpperCase', function(string) {
            return (string || "").toString().toUpperCase();
        });
        handlebars.registerHelper('getInvoiceNr', function() {
            let invoice = Invoices.findOne({_id: this.invoiceId});
            return invoice ? invoice.invoiceNr : "";
        });

        Meteor.call('get-xml-template', 'sepa-payback', (err, xmlTemplate) => {
            templateData.getPaybacks = [];
            let index = 0;
            let sumOfPaybacks = 0;
            PaybackLines.find({
                paybackId: templateData._id
            },{
                sort: {
                    createdAt: 1
                }
            }).forEach((payback) => {
                payback.index = index++;
                sumOfPaybacks += Number(payback.amount);
                templateData.getPaybacks.push(payback);
            });
            templateData.numberOfPaybacks = templateData.getPaybacks.length;
            templateData.sumOfPaybacks = sumOfPaybacks;

            const textToWrite = handlebars.compile(xmlTemplate)(templateData);

            let textFileAsBlob = new Blob([textToWrite], {type:'application/xml'});
            let fileNameToSaveAs = templateData.paybackNumber + '-sepa.xml';

            Vatfree.templateHelpers.downloadFile(fileNameToSaveAs, textFileAsBlob);
        });
    }
});

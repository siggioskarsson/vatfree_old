import { invoiceHelpers } from "./helpers";
import './credit-control.html';

Template.invoice_credit_control.onCreated(function() {
    this.parentTemplate = this.parentTemplate();
});

Template.invoice_credit_control.onRendered(function() {
    this.autorun(() => {
        let selector = invoiceHelpers.getInvoicesSelector(this.parentTemplate);
        this.subscribe('invoices', this.parentTemplate.searchTerm.get(), selector, {}, 10000, 0);
    });
});

Template.invoice_credit_control.helpers({
    getGroups() {
        let groups = {};
        let template = Template.instance();
        let selector = invoiceHelpers.getInvoicesSelector(template.parentTemplate);
        Invoices.find(selector).forEach((invoice) => {
            let entityId = invoice.shopId || invoice.companyId;

            if (!_.has(groups, entityId)) {
                let type = invoice.shopId ? 'shop' : 'company';
                let entity = {};
                if (type === 'shop') {
                    entity = Shops.findOne({_id: entityId});
                } else {
                    entity = Companies.findOne({_id: entityId});
                }

                groups[entityId] = {
                    id: entityId,
                    type: type,
                    name: entity.name,
                    total: invoice.amount,
                    partnershipStatus: entity.partnershipStatus
                };
            } else {
                groups[entityId].total += invoice.amount;
            }
        });

        groups = _.values(groups);

        groups.sort(function(a,b) {
            return a.total > b.total ? -1 : 1;
        });

        return groups;
    },
    groupName() {
        let entity = {};
        if (this.type === 'shop') {
            entity = Shops.findOne({_id: this.id});
        } else {
            entity = Companies.findOne({_id: this.id});
        }

        return `${entity.name} - ${entity.addressFirst}, ${entity.city}`;
    },
    getInvoices() {
        let template = Template.instance();
        let selector = invoiceHelpers.getInvoicesSelector(template.parentTemplate);

        if (this.type === 'shop') {
            selector.shopId = this.id;
        } else {
            selector.companyId = this.id;
        }

        return Invoices.find(selector, {
            sort: {
                invoiceNr: -1
            }
        });
    }
});

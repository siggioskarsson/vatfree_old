UI.registerHelper('getInvoiceIcon', function(status) {
    status = status || this.status;

    let icon = Vatfree.invoices.icons();
    return _.has(icon, status) ? icon[status] : "circle-o";
});

UI.registerHelper('getInvoiceStatusOptions', function() {
    return _.clone(Invoices.simpleSchema()._schema.status.allowedValues);
});

UI.registerHelper('getInvoiceTypes', function() {
    return _.clone(Invoices.simpleSchema()._schema.type.allowedValues);
});

UI.registerHelper('getInvoiceName', function(invoiceId) {
    if (!invoiceId) invoiceId = this.invoiceId;

    let invoice = Invoices.findOne({_id: invoiceId});
    if (invoice) {
        return invoice.invoiceNr;
    }

    return "";
});

UI.registerHelper('getInvoice', function(invoiceId) {
    if (!invoiceId) invoiceId = this.invoiceId;

    return Invoices.findOne({_id: invoiceId});
});

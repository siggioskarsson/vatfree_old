import './set-not-collectable.html';

Template.invoiceSetNotCollectable.events({
   'click button.invoice-set-not-collectable-do'(e, template) {
       e.preventDefault();
       let parentTemplate = template.parentTemplate();
       let formData = Vatfree.templateHelpers.getFormData(template);

       let receiptRejectionIds = [];
       _.each(formData.receiptRejectionIds, (includeReceiptRejection, receiptRejectionId) => {
           if (includeReceiptRejection === 'on') {
               receiptRejectionIds.push(receiptRejectionId);
           }
       });
       if (receiptRejectionIds.length === 0) {
           toastr.error('Select 1 or more reasons for setting this invoice to not collectable');
           return false;
       }

       Invoices.update({
           _id: this.invoiceId
       },{
           $set: {
               status: 'notCollectable',
               receiptRejectionIds: receiptRejectionIds
           }
       });

       toastr.info('Invoice marked as not collectable.');
       parentTemplate.settingNotCollectable.set();
   }
});

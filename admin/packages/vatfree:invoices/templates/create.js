import './create.html';

Template.createInvoicesModal.onCreated(function() {
    this.gettingReceipts = new ReactiveVar(true);
    this.receipts = new ReactiveVar({});
    this.receiptIds = new ReactiveVar([]);
    this.creatingInvoices = new ReactiveVar();

    this.activeItem = new ReactiveVar();

    this.getReceipts = function() {
        // we get the receipts we are invoicing once, so reactivity doesn't mess up while we are busy
        Meteor.call('get-receipts-for-invoicing', (err, receipts) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                let receiptIds = [];
                let groupedReceipts = {};
                // group receipts by shop or company
                _.each(receipts, (receipt) => {
                    let id = "shopId:" + receipt.shopId + ':' + receipt.currencyId + '::' + receipt.billingCompanyName;
                    if (receipt.billingCompanyId) {
                        id = "companyId:" + receipt.billingCompanyId + ':' + receipt.currencyId + ':' + receipt.companyId + ':' + receipt.billingCompanyName;
                    } else if (!receipt.billingEntity) {
                        receipt.error = "Cannot bill receipt. No billing entity found.";
                    }
                    if (!_.has(groupedReceipts, id)) {
                        groupedReceipts[id] = [];
                    }
                    groupedReceipts[id].push(receipt);
                    receiptIds.push(receipt._id);

                });
                this.receipts.set(groupedReceipts);
                this.receiptIds.set(receiptIds);
                this.gettingReceipts.set(false);
            }
        });
    };
});

Template.createInvoicesModal.onRendered(function() {
    this.subscribe('receipts-for-invoicing', 10000, 0);

    this.autorun((comp) => {
        if (this.subscriptionsReady()) {
            this.getReceipts.call(this);
            comp.stop(); // stop current autorun computation
        }
    });
});

Template.createInvoicesModal.helpers(createInvoiceHelpers);
Template.createInvoicesModal.helpers({
    getOptions() {
        let template = Template.instance();
        return {
            lockId: "invoices_create",
            lockMessage: "Invoice creation is locked by another user",
            sidebarTemplate: "createInvoicesModal_sidebar",
            parentTemplateVariable: "creatingInvoices",
            activeItem: 'invoice_create',
            activeReceipt: template.activeItem,
            receipts: template.receipts,
            receiptIds: template.receiptIds,
            gettingReceipts: template.gettingReceipts
        }
    },
    creatingInvoices() {
        return Template.instance().creatingInvoices.get();
    },
    gettingReceipts() {
        return Template.instance().gettingReceipts.get();
    }
});

Template.createInvoicesModal.events({
    'click .ibox-content .item-row input[type="checkbox"]'(e, template) {
        e.stopPropagation();
        e.stopImmediatePropagation();
    },
    'click .ibox-content .item-row'(e, template) {
        e.preventDefault();
        e.stopImmediatePropagation();
        if (template.activeItem.get() === this._id) {
            template.activeItem.set();
        } else {
            template.activeItem.set(this._id);
        }
    },
    'click .file-box .file-name'(e, template) {
        let receiptId = this.receiptId;
        template.activeItem.set(receiptId);
        Tracker.afterFlush(() => {
            $('.task-main').scrollTo($('#receipt-' + receiptId), {offset: {top: -100}, duration: 200});
        });
    },
    'dblclick .ibox-content .item-row'(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
    },
    'click .select-all-global'(e, template) {
        e.preventDefault();
        template.$('input[type="checkbox"]').each(function() { $(this).prop('checked', true) } )
    },
    'click .select-none-global'(e, template) {
        e.preventDefault();
        template.$('input[type="checkbox"]').each(function() { $(this).prop('checked', false) } )
    },
    'click .select-all'(e, template) {
        let receipts = template.receipts.get();
        _.each(receipts[this.group], (receipt) => {
            template.$('input[name="receipts.' + receipt._id + '"]').prop('checked', true);
        });
    },
    'click .select-none'(e, template) {
        let receipts = template.receipts.get();
        _.each(receipts[this.group], (receipt) => {
            template.$('input[name="receipts.' + receipt._id + '"]').prop('checked', false);
        });
    },
    'submit form[name="tasks-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        let receiptIds = [];
        _.each(formData.receipts, (on, receiptId) => {
            if (on !== false) {
                receiptIds.push(receiptId);
            }
        });

        template.creatingInvoices.set(true);
        Meteor.setTimeout(() => {
            Meteor.call('create-invoices', receiptIds, (err, result) => {
                if (err) {
                    toastr.error(err.reason);
                } else {
                    toastr.info('Invoice creation started in the background');
                    template.getReceipts.call(template);
                    template.creatingInvoices.set(false);
                    $('#modal-task').find('.close-tasks').trigger('click');
                }
            });
        }, 1);
    }
});

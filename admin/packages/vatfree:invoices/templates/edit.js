import moment from 'moment';
import './edit.html';

Template.invoiceEdit.onCreated(function () {
    this.activeReceiptId = new ReactiveVar();
});

Template.invoiceEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('invoices_item', FlowRouter.getParam('invoiceId') || '');
    });
});

Template.invoiceEdit.helpers({
    invoiceDoc() {
        return Invoices.findOne({
            _id: FlowRouter.getParam('invoiceId')
        });
    },
    getBreadcrumbTitle() {
        let shop = Shops.findOne({_id: this.shopId});
        if (shop) {
            return this.invoiceNr + ' - ' + shop.name + ', ' + shop.city;
        }
    },
    activeReceiptId() {
        return Template.instance().activeReceiptId.get();
    },
    addActiveReceiptId() {
        this.activeReceiptId = Template.instance().activeReceiptId.get();
        return this;
    },
    getSelectedReceipt() {
        let receiptId = Template.instance().activeReceiptId.get();
        if (receiptId) {
            return Receipts.findOne({_id: receiptId});
        }
    }
});

Template.invoiceEdit.events({
    'click table.table.receipt-list .item-row'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeReceiptId) {
            if (template.activeReceiptId.get() === this._id) {
                template.activeReceiptId.set();
            } else {
                template.activeReceiptId.set(this._id);
            }
        }
    }
});

Template.invoiceEditForm.onCreated(function () {
    this.creatingPDF = new ReactiveVar();
    this.creatingImagesPDF = new ReactiveVar();
    this.disableButtons = new ReactiveVar();
    this.selectedReceiptIds = new ReactiveVar();
    this.activeReceiptId = new ReactiveVar();

    this.settingNotCollectable = new ReactiveVar();

    this.receiptSort = new ReactiveVar({
        receiptNr: -1
    });
});

Template.invoiceEditForm.onRendered(function () {
    Tracker.afterFlush(() => {
        invoiceEditFormAfterFlush.call(this);
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('invoice-receipts', data._id);
        this.subscribe('activity-logs', "", {invoiceId: data._id});
    });

    this.autorun(() => {
        let data = Template.currentData();
        this.activeReceiptId.set(data.activeReceiptId);
    });

    this.autorun(() => {
        let data = Template.currentData();
        let shop = Shops.findOne({_id: data.shopId });
        if (shop && shop.companyId) {
            this.subscribe('companies_item', shop.companyId || "");
        }
    });
});

Template.invoiceEditForm.helpers({
    activeReceiptId() {
        return Template.instance().activeReceiptId.get();
    },
    isStatus(status) {
        let template = Template.instance();
        return template.data.status === status;
    },
    isDisabled() {
        return this.status !== 'new';
    },
    canDelete() {
        let template = Template.instance();
        return template.data.type !== 'credit' && (template.data.status === 'new' || template.data.status === 'readyToSend' || template.data.status === 'sentToRetailer' || template.data.status === 'promisedToPay');
    },
    getReceipts() {
        let template = Template.instance();
        let sort = template.receiptSort.get();
        return Receipts.find({
            invoiceId: this._id
        }, {
            sort: sort
        });
    },
    getReceiptSorting() {
        return Template.instance().receiptSort.get();
    },
    settingNotCollectable() {
        return Template.instance().settingNotCollectable.get();
    },
    getFiles() {
        return Files.find({
            itemId: this._id,
            target: "invoices"
        }, {
            sort: {
                createdAt: 1
            }
        });
    },
    getActivityLog() {
        return ActivityLogs.find({
            invoiceId: this._id
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    creatingPDF() {
        return Template.instance().creatingPDF.get();
    },
    disableButtons() {
        return Template.instance().disableButtons.get();
    },
    creatingImagesPDF() {
        return Template.instance().creatingImagesPDF.get();
    },
    getPayments() {
        return Payments.find({
            invoiceIds: this._id
        });
    },
    getOnlineLink() {
        return 'https://partner.vatfree.com/invoice/' + this._id + '/' + this.secret;
    },
    imagesPDFExists() {
        return Files.find({
            itemId: this._id,
            target: "invoices",
            subType: "images"
        }).count() > 0;
    },
    showCreateDebtCollection() {
        let entity = {};
        if (this.companyId) {
            entity = Companies.findOne({_id: this.companyId}) || {};
        } else {
            entity = Shops.findOne({_id: this.shopId}) || {};
        }

        return this.status !== 'paid' && entity.debtCollectionAccount && entity.debtCollectionMandate;
    },
    billingEntityDoc() {
        return this.billingEntity ? false : Vatfree.billing.getBillingEntityForCompany(this.companyId || this._id);
    },
    allowCreateCredit() {
        return this.type !== 'credit' && this.status !== 'credited';
    },
    showPromisedToPay() {
        return _.contains(['sentToRetailer', 'promisedToPay'], this.status);
    }
});

Template.invoiceEditForm.events({
    'click .invoice-check-is-ok'(e, template) {
        e.preventDefault();

        let invoice = this;
        swal({
            title: "Mark invoice as OK?",
            text: "The invoice will be marked as OK and ready to send!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!"
        }, function () {
            Invoices.update({
                _id: invoice._id
            },{
                $set: {
                    status: 'readyToSend'
                }
            });
        });
    },
    'click .invoice-mark-collectable'(e, template) {
        e.preventDefault();

        let invoice = this;
        import swal from 'sweetalert';
        swal({
            title: "Mark invoice as collectable?",
            text: "All receipts in this invoice will also be marked as collectable!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!"
        }, function () {
            let status = "readyToSend";
            if (invoice.invoiceSent) {
                status = "sentToRetailer";
            }

            Invoices.update({
                _id: invoice._id
            },{
                $set: {
                    status: status
                }
            });
        });
    },
    'click .invoice-already-sent'(e, template) {
        e.preventDefault();
        template.disableButtons.set(true);

        let updateDueDate = false;
        if (confirm("Update the due date of the invoice?")) {
            updateDueDate = true;
        }

        Meteor.call('invoice-already-sent', this._id, updateDueDate, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.info('Invoice sent');
            }
            template.disableButtons.set();
        });
    },
    'click table.receipt-list th.sort-header'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        let data = $(e.currentTarget).data();
        if (data && data.id) {
            let currentSort = template.receiptSort.get();
            let sortOrder = -1;
            if (currentSort && currentSort[data.id] === -1) {
                sortOrder = 1;
            }
            template.receiptSort.set({
                [data.id]: sortOrder
            });
        }
    },
    'click .update-to-billing-entity'(e ,template) {
        e.preventDefault();
        let invoice = template.data;
        let newBillingEntity = this;

        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "Change this invoice to " + UI._globalHelpers.getCompanyDescription(newBillingEntity._id),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, change it!",
            closeOnConfirm: false
        }, function () {
            console.log(template.data);
            Invoices.update({
                _id: invoice._id
            },{
                $set: {
                    companyId: newBillingEntity._id
                },
                $unset: {
                    shopId: 1
                }
            }, (err, result) => {
                if (err) {
                    alert(err);
                } else {
                    Files.find({
                        target: {
                            $ne: false
                        },
                        itemId: invoice._id
                    }).forEach((invoiceFile) => {
                        Files.update({
                            _id: invoiceFile._id
                        },{
                            $set: {
                                deleted: true
                            }
                        });
                    });

                    Meteor.call('create-invoice-pdf', invoice._id, console.log);

                    swal("Invoice changed!", "PDF invoice are being regenerated. Please manually send invoice if applicable.", "success");
                }
            });
        });
    },
    'click .delete-file'(e) {
        e.preventDefault();
        let fileId = this._id;
        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            Files.update({
                _id: fileId
            },{
                $set: {
                    deleted: true
                }
            });
            swal("Deleted!", "File has been deleted.", "success");
        });
    },
    'click .invoice-send'(e, template) {
        e.preventDefault();
        template.disableButtons.set(true);

        let updateDueDate = false;
        if (confirm("Update the due date of the invoice?")) {
            updateDueDate = true;
        }

        Meteor.call('invoice-send', this._id, updateDueDate, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.info('Invoice sent');
            }
            template.disableButtons.set();
        });
    },
    'click .invoice-resend'(e, template) {
        e.preventDefault();
        template.disableButtons.set(true);

        let updateDueDate = false;
        if (confirm("Update the due date of the invoice?")) {
            updateDueDate = true;
        }

        Meteor.call('invoice-resend', this._id, updateDueDate, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.info('Invoice sent');
            }
            template.disableButtons.set();
        });
    },
    'click .invoice-send-reminder'(e, template) {
        e.preventDefault();
        template.disableButtons.set(true);
        Meteor.call('invoice-send-reminder', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.info('Invoice sent');
            }
            template.disableButtons.set();
        });
    },
    'click .invoice-promise-to-pay'(e, template) {
        e.preventDefault();
        const invoice = this;

        import swal from 'sweetalert';
        import 'swal-forms';
        swal.withForm({
            title: 'Promised to pay?',
            text: 'By which date did the client promise to pay?',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Promised to pay!',
            closeOnConfirm: true,
            formFields: [
                { id: 'promisedAt', placeholder: 'Promise', type: 'date' }
            ]
        }, function (isConfirm) {
            if (isConfirm) {
                Invoices.update({
                    _id: invoice._id
                }, {
                    $set: {
                        status: 'promisedToPay',
                        promisedAt: moment(this.swalForm.promisedAt, 'YYYY-MM-DD').set({hour: 8}).toDate()
                    }
                });
            }
        });
    },
    'click .invoice-create-pdf'(e, template) {
        e.preventDefault();
        template.creatingPDF.set(true);
        Meteor.call('create-invoice-pdf', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            }
            toastr.info('Invoice creation started in the background');
            template.creatingPDF.set();
        });
    },
    'click .invoice-create-pdf-images'(e, template) {
        e.preventDefault();
        template.creatingImagesPDF.set(true);
        Meteor.call('create-invoice-pdf-images', this._id, (err, result) => {
            if (err) {
                toastr.error(err.reason);
            }
            toastr.info('Images PDF creation started in the background');
            template.creatingImagesPDF.set();
        });
    },
    'click .invoice-not-collectable'(e, template) {
        e.preventDefault();
        template.settingNotCollectable.set(true);
    },
    'click .credit-invoice'(e, template) {
        e.preventDefault();
        let invoice = this;
        import swal from 'sweetalert';
        swal({
            title: "Create credit invoice?",
            text: "A invoice will be created and a payback initiated (if invoice is paid)!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, create credit!",
            closeOnConfirm: false
        }, function () {
            Meteor.call('invoice-create-credit-invoice', invoice._id, (err, result) => {
                if (err) {
                    toastr.error(err.message || err.reason);
                } else {
                    swal("Created!", "The credit invoice has been created and linked to this invoice. Paybacks have also been created if a payment was made on this invoice.", "success");
                }
            });
        });
    },
    'click .create-debt-collection'(e, template) {
        e.preventDefault();
        let invoice = this;
        let currency = Currencies.findOne({_id: invoice.currencyId});
        import swal from 'sweetalert';
        swal({
            title: "Create debt collection line?",
            text: "A debt collection line will be create for " + Vatfree.numbers.formatCurrency(invoice.amount, 2, currency.symbol) + "!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, create it!",
            closeOnConfirm: false
        }, function () {
            let createDebtCollection = function () {
                Meteor.call('invoice-create-debt-collection-line', invoice._id, (err, result) => {
                    if (err) {
                        toastr.error(err.message || err.reason);
                    } else {
                        swal("Created!", "The debt collection line has been created and will be included in the next export.", "success");
                    }
                });
            };

            let debtCollection = DebtCollectionLines.findOne({invoiceId: invoice._id});
            if (debtCollection) {
                swal({
                    title: "Debt collection line exists",
                    text: "A debt collection line already exists. Create another?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, create it!",
                    closeOnConfirm: false
                }, function () {
                    createDebtCollection();
                });
            } else {
                createDebtCollection();
            }
        });
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/invoices');
    },
    'click .invoice-ready-to-send'(e, template) {
        e.preventDefault();
        let invoiceId = this._id;

        if (this.status === 'new') {
            import swal from 'sweetalert';
            swal({
                title: "Are you sure?",
                text: "The invoice will be marked for sending to shop!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, it's all good!",
                closeOnConfirm: true
            }, function () {
                template.$('form[name="edit-invoice-form"]').trigger('submit');

                Invoices.update({
                    _id: invoiceId
                },{
                    $set: {
                        status: 'readyToSend'
                    }
                });
            });
        } else {
            toastr.error('Invoice can only be ready for send when new');
        }
    },
    'click .invoice-reopen'(e) {
        e.preventDefault();
        let invoiceId = this._id;

        if (this.status === 'readyToSend') {
            import swal from 'sweetalert';
            swal({
                title: "Are you sure?",
                text: "The invoice will be re-opened for checking!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, it's all good!",
                closeOnConfirm: true
            }, function () {
                Invoices.update({
                    _id: invoiceId
                },{
                    $set: {
                        status: 'new'
                    }
                });
            });
        } else {
            toastr.error('Invoice can only be re-opened when ready to send');
        }
    },
    'click .invoice-mark-paid-difference'(e, template) {
        e.preventDefault();
        let invoiceId = this._id;

        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "This invoice will marked as paid and the payment difference recorded! All receipts will be marked as paid!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function () {
            Meteor.call('invoice-mark-paid-difference', invoiceId, (err, result) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                }
            });
        });
    },
    'click .invoice-mark-paid-payback'(e, template) {
        e.preventDefault();
        let invoiceId = this._id;

        import swal from 'sweetalert';
        swal({
            title: "Are you sure?",
            text: "This invoice will marked as paid and the difference will be paid back!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, do it!",
            closeOnConfirm: true
        }, function () {
            Meteor.call('invoice-mark-paid-payback', invoiceId, (err, result) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                }
            });
        });
    },
    'click .invoice-delete'(e) {
        e.preventDefault();
        let invoiceId = this._id;

        if (this.status === 'new' || this.status === 'toCheck' || this.status === 'readyToSend' || this.status === 'sentToRetailer' || this.status === 'promisedToPay') {
            import swal from 'sweetalert';
            swal({
                title: "Are you sure?",
                text: "All receipts in this invoice will be put back to the 'Ready for invoicing' status!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true
            }, function () {
                Meteor.call('invoice-delete', invoiceId, (err, result) => {
                    if (err) {
                        toastr.error(err.reason);
                    } else {
                        FlowRouter.go('/invoices');
                    }
                });
            });
        } else {
            toastr.error('Invoice can only be deleted when ready for sending');
        }
    },
    'click .invoice-mark-receipts-paid'(e, template) {
        e.preventDefault();
        let receiptsMarked = 0;
        template.$('input[name^="receipts."]:checkbox:checked').each(function() {
            let receipt = Blaze.getData(this);
            if (receipt.status === "waitingForPayment") {
                Receipts.update({
                    _id: receipt._id
                }, {
                    $set: {
                        status: "paidByShop"
                    }
                });
                receiptsMarked++;
            } else {
                toastr.warning("Cannot mark receipt " + receipt.receiptNr + " as paid. It is not 'waitingForPayment'.");
            }
            $(this).attr('checked', false);
        });

        toastr.success('Marked ' + receiptsMarked + ' receipts as paidByShop');
    },
    'click .invoice-mark-receipts-unpaid'(e, template) {
        e.preventDefault();
        let receiptsMarked = 0;
        template.$('input[name^="receipts."]:checkbox:checked').each(function() {
            let receipt = Blaze.getData(this);
            if (receipt.status === "paidByShop") {
                Receipts.update({
                    _id: receipt._id
                }, {
                    $set: {
                        status: "waitingForPayment"
                    }
                });
                receiptsMarked++;
            } else {
                toastr.warning("Cannot mark receipt " + receipt.receiptNr + " as paid. It is not 'paidByShop'.");
            }
            $(this).attr('checked', false);
        });

        toastr.success('Marked ' + receiptsMarked + ' receipts as waitingForPayment');
    },
    'submit form[name="edit-invoice-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        // fix dates
        if (_.has(formData, 'invoiceDate')) {
            if (formData.invoiceDate) {
                formData.invoiceDate = moment(formData.invoiceDate, TAPi18n.__('_date_format')).toDate();
            } else {
                formData.invoiceDate = null;
            }
        }

        // fix numbers
        if (_.has(formData, 'amount')) formData.amount = Math.round(Number(formData.amount)*100);
        if (_.has(formData, 'totalVat')) formData.totalVat = Math.round(Number(formData.totalVat)*100);
        if (_.has(formData, 'serviceFee')) formData.serviceFee = Math.round(Number(formData.serviceFee)*100);
        if (_.has(formData, 'refund')) formData.refund = Math.round(Number(formData.refund)*100);

        Invoices.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Invoice saved!')
            }
        });
    }
});

import { invoiceHelpers } from "./helpers";
import './info.html';

Template.invoiceInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.invoiceInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData() || {};
        let limit = 20;
        let offset = 0;
        this.subscribe('invoices_item', data.invoice._id || '');
        this.subscribe('activity-stream', {invoiceId: data.invoice._id}, null, limit, offset);
        this.subscribe('activity-stream', {}, data.invoice._id, limit, offset);
        this.subscribe('companies_item', data.invoice.companyId || "");
        this.subscribe('debt-collection-lines', data.invoice._id);
        this.subscribe('payback-lines', data.invoice._id);
    });
});

Template.invoiceInfo.helpers(invoiceHelpers);
Template.invoiceInfo.helpers({
    getReferenceInvoiceNr() {
        let invoice = Invoices.findOne({_id: this.referenceInvoiceId});
        if (invoice) {
            return invoice.invoiceNr;
        }
    },
    getDebtCollection() {
        let debtCollections = DebtCollection.find({invoiceIds: this._id});
        return debtCollections.count() > 0 ? debtCollections : false;
    },
    getDebtCollectionLines() {
        let lines = DebtCollectionLines.find({invoiceId: this._id});
        return lines.count() > 0 ? lines : false;
    },
    getPaybackLines() {
        let lines = PaybackLines.find({invoiceId: this._id});
        return lines.count() > 0 ? lines : false;
    },
    getPayback() {
        return Payback.findOne({invoiceIds: this._id});
    },
    getOpenInvoiceAmount() {
        let invoice = this;
        let amountPaid = 0;

        Payments.find({
            invoiceIds: invoice._id
        }).forEach((payment) => {
            let invoiceAmount = _.find(payment.invoiceAmounts, (amount) => {
                if (amount.invoiceId === invoice._id) {
                    return amount;
                }
            });

            if (invoiceAmount) {
                amountPaid += invoiceAmount.amount;
            }
        });

        return amountPaid - invoice.amount;
    },
    getFiles() {
        let invoiceId = this._id;
        return Files.find({
            itemId: invoiceId,
            target: 'invoices'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getActivity() {
        return ActivityStream.collection.find({
            $or: [
                {
                    'securityContext.invoiceId': this._id
                },
                {
                    'activity.target.itemId': this._id
                }
            ]
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    },
    getInvoiceReceiptsFilter() {
        return {
            _id: {
                $in: this.receiptIds
            }
        }
    }
});

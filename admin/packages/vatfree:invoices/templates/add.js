import './add.html';

Template.addInvoiceModal.onCreated(function() {
    let template = this;
    let parentVariable = this.data.parentVariable || 'addingItem';

    this.hideModal = () => {
        $('#modal-add-invoice').modal('hide');
    };

    this.autorun(() => {
        let data = Template.currentData();
        if (data.companyId) {
            this.subscribe('company-receipts', data.companyId, "", ['readyForInvoicing']);
        } else {
            this.subscribe('shop-receipts', data.shopId, "", ['readyForInvoicing']);
        }
    });

    this.parentTemplate = Template.instance().parentTemplate();

    this.autorun(() => {
        if (this.subscriptionsReady()) {
            Tracker.afterFlush(() => {
                $('input[type="checkbox"]').each(function() {
                    $(this).attr('checked', true);
                })
            });
        }
    });

    Tracker.afterFlush(() => {
        $('#modal-add-invoice').on('hidden.bs.modal', function () {
            if (template.parentTemplate[parentVariable]) template.parentTemplate[parentVariable].set(false);
        });
        $('#modal-add-invoice').modal('show');
    });
});

Template.addInvoiceModal.helpers({
    getReceipts() {
        let selector = {
            status: 'readyForInvoicing'
        };

        if (this.companyId) {
            selector.billingCompanyId = this.companyId
        } else {
            selector.shopId = this.shopId;
        }

        return Receipts.find(selector, {
            sort: {
                createdAt: 1
            }
        });
    }
});

Template.addInvoiceModal.events({
    'click .cancel-add-invoice'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-invoice-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        let receipts = {};
        _.each(formData.receipts, (includeReceipt, receiptId) => {
            if (includeReceipt === 'on') {
                let receipt = Receipts.findOne({_id: receiptId});
                if (!_.has(receipts, receipt.currencyId)) {
                    receipts[receipt.currencyId] = [];
                }
                receipts[receipt.currencyId].push(receiptId);
            }
        });

        _.each(receipts, (receiptIds, currencyId) => {
            Meteor.call('create-invoice', formData.shopId, formData.companyId, receiptIds, currencyId, function(err, result) {
                if (err) {
                    toastr.error(err.reason);
                } else {
                    template.hideModal();
                }
            });
        });
    }
});

/* global createInvoiceHelpers: true */

createInvoiceHelpers = {
    getReceiptGroups() {
        let groups = _.map(Template.instance().receipts.get(), (receipts, group) => {
            return {
                group: group,
                receipts: receipts
            };
        });

        groups.sort(function(a, b) {
            let aName = a.group.split(':')[4];
            let bName = b.group.split(':')[4];
            return aName > bName ? 1 : -1;
        });
        return groups;
    },
    isShop() {
        let field = this.group.toString().split(':')[0];
        return field === "shopId";
    },
    isCompany() {
        let field = this.group.toString().split(':')[0];
        return field === "companyId";
    },
    getShop() {
        let value = this.group.toString().split(':')[1];
        return Shops.findOne({_id: value});
    },
    getCompany() {
        let value = this.group.toString().split(':')[1];
        return Companies.findOne({_id: value});
    },
    getReceipts() {
        let field = this.group.toString().split(':')[0];
        let value = this.group.toString().split(':')[1];
        if (field === 'companyId') {
            value = this.group.toString().split(':')[3];
        }
        let currencyId = this.group.toString().split(':')[2];
        return Receipts.find({
            currencyId: currencyId,
            _id: {
                $in: _.map(this.receipts, (receipt) => { return receipt._id; }) || []
            }
        },{
            sort: {
                receiptNr: 1
            }
        });
    }
};

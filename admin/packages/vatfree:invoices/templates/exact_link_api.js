import './exact_link_api.html';

Template.invoices_exact_link_api.onRendered(function() {
    const code = FlowRouter.getQueryParam('code');
    if (code) {
        if (Meteor.user() && !Meteor.loggingIn()) {
            Meteor.call('register-exact-api-link-code', code, (err) => {
                if (err) {
                    toastr.error(err.reason);
                } else {
                    toastr.success("API code registered");
                }
                FlowRouter.go('/invoices');
            });
        }
    } else {
        FlowRouter.go('/invoices');
    }
});

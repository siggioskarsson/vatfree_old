import './job-list.html';

Template.invoiceJobList.onRendered(function() {
    this.autorun(() => {
        this.subscribe('invoice-jobs');
    });
});

Template.invoiceJobList.helpers({
    getJobs() {
        return invoiceJobs.find({}, {
            sort: {
                createdAt: 1
            }
        });
    },
    statusFailed() {
        return this.status === 'failed';
    }
});

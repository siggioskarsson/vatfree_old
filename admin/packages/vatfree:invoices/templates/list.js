import { invoiceHelpers } from "./helpers";
import './list.html';

Template.invoicesList.onCreated(function() {
    this.detailUrl = '/invoice/:itemId';
});
Template.invoicesList.onRendered(function() {
    Tracker.afterFlush(() => {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square icheckbox_square-blue',
            radioClass: 'iradio_square iradio_square-blue'
        });
    });
});

Template.invoicesList.helpers(Vatfree.templateHelpers.helpers);
Template.invoicesList.helpers(invoiceHelpers);
Template.invoicesList.events(Vatfree.templateHelpers.events());

Template.invoicesList.helpers({
    isInvoiceChecked() {
        let data = Template.instance().data;
        if (data.allSelected) {
            return true;
        } else if (_.isArray(data.selected)) {
            return _.contains(data.selected, this._id);
        }
    },
    getTotal() {
        let total = {
            currencies: {},
            amount: [],
            number: 0
        };

        let invoices = this.invoices;
        if (this.invoices && this.invoices.fetch) {
            invoices = this.invoices.fetch();
        }
        _.each(invoices, (invoice) => {
            total.number++;
            if (!_.has(total.currencies, invoice.currencyId)) {
                total.currencies[invoice.currencyId] = {
                    currencyId: invoice.currencyId,
                    amount: 0,
                    serviceFee: 0,
                    paymentDifference: 0
                };
            }

            total.currencies[invoice.currencyId].amount += invoice.amount;
            total.currencies[invoice.currencyId].serviceFee += invoice.serviceFee - (invoice.deferredServiceFee || 0);
            total.currencies[invoice.currencyId].paymentDifference += invoice.paymentDifference;
        });

        _.each(total.currencies, (amount) => {
            total.amount.push(amount);
        });

        return total;
    }
});

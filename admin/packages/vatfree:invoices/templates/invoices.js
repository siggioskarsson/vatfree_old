import { invoiceHelpers } from "./helpers";
import moment from "moment/moment";
var FileSaver = require('file-saver');
import './invoices.html';

Template.invoices.onCreated(Vatfree.templateHelpers.onCreated(Invoices, '/invoices/', '/invoice/:itemId'));
Template.invoices.onRendered(Vatfree.templateHelpers.onRendered());
Template.invoices.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.invoices.helpers(Vatfree.templateHelpers.helpers);
Template.invoices.helpers(invoiceHelpers);
Template.invoices.events(Vatfree.templateHelpers.events());

Template.invoices.onCreated(function() {
    this.creatingInvoices = new ReactiveVar();
    this.invoiceCreditControl = new ReactiveVar();
    this.showJobs = new ReactiveVar();
    this.runningExport = new ReactiveVar();

    this.salesforceFilter = new ReactiveVar(Session.get(this.view.name + '.salesforceFilter'));
    this.autorun(() => {
        Session.setPersistent(this.view.name + '.salesforceFilter', this.salesforceFilter.get());
    });

    this.todoStatus = [
        'new'
    ];
    if (!this.sort.get()) {
        this.sort.set({
            invoiceNr: -1
        });
    }

    this.subscribe('debt-collection-lines', "");
    this.subscribe('payback-lines', "");

    this.selectorFunction = (selector) => {
        let salesforceFilter = this.salesforceFilter.get();
        if (salesforceFilter) {
            if (salesforceFilter === 'active') {
                selector.salesforceId = {
                    $exists: true
                }
            }
            if (salesforceFilter === 'inactive') {
                selector.salesforceId = {
                    $exists: false
                }
            }
        }
    };
});

Template.invoices.onRendered(function() {
    this.autorun(() => {
        this.subscribe('invoice-stats');
    });
});

Template.invoices.helpers({
    runningExport() {
        return Template.instance().runningExport.get();
    },
    creatingInvoices() {
        return Template.instance().creatingInvoices.get();
    },
    invoiceCreditControl() {
        return Template.instance().invoiceCreditControl.get();
    },
    showJobs() {
        return Template.instance().showJobs.get();
    },
    getDebtCollectionLines() {
        return DebtCollectionLines.find({
            debtCollectionId: {
                $exists: false
            }
        });
    },
    getPaybackLines() {
        return PaybackLines.find({
            paybackId: {
                $exists: false
            }
        });
    },
    getInvoiceStats() {
        let template = Template.instance();
        let selector = invoiceHelpers.getInvoicesSelector(template);

        let stats = ReactiveMethod.call('get-invoice-stats', selector);
        return stats && stats[0] ? stats[0] : false;
    },
    isActiveSalesforceFilter() {
        return Template.instance().salesforceFilter.get() === 'active';
    },
    isInactiveSalesforceFilter() {
        return Template.instance().salesforceFilter.get() === 'inactive';
    }
});

Template.invoices.events({
    'click .filter-icon.salesforce-filter-icon'(e, template) {
        e.preventDefault();
        const salesforceFilter = template.salesforceFilter.get();
        if (salesforceFilter === 'active') {
            template.salesforceFilter.set('inactive');
        } else if (salesforceFilter === 'inactive') {
            template.salesforceFilter.set(false);
        } else {
            template.salesforceFilter.set('active');
        }
    },
    'click .invoices-export'(e, template) {
        e.preventDefault();
        template.runningExport.set(true);
        Meteor.call('invoices-export', template.searchTerm.get(), template.statusFilter.array(), template.listFilters.get(), template.salesforceFilter.get(), (err, csv) => {
            var blob = new Blob([csv], {
                type: "text/plain;charset=utf-8"
            });
            FileSaver.saveAs(blob, "invoices_export_" + moment().format(TAPi18n.__('_date_time_format')) + ".csv");
            template.runningExport.set(false);
        });
    },
    'click .invoice-jobs'(e, template) {
        e.preventDefault();
        template.showJobs.set(!template.showJobs.get());
    },
    'click .exact-link-api'(e ,template) {
        e.preventDefault();
        Meteor.call('get-exact-auth-url', (err, authUrl) => {
            if (err) {
                toastr.error(err.reason);
            } else {
                document.location.href = authUrl;
            }
        });
    },
    'click .invoice-create-new'(e, template) {
        e.preventDefault();
        template.creatingInvoices.set(true);
    },
    'click .invoice-credit-control'(e, template) {
        e.preventDefault();
        template.invoiceCreditControl.set(!template.invoiceCreditControl.get());
    },
    'click .debt-collection-new'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).attr('disabled') === 'disabled') {
            return false;
        }

        if (confirm('Do you want to create an export file and tag the lines as completed?')) {
            Meteor.call('create-debt-collection-batch', (err, debtCollectionId) => {
                if (err) {
                    alert(err.reason || err.message);
                } else {
                    FlowRouter.go('/invoice/debt-collection/' + debtCollectionId);
                }
            });
        }
    },
    'click .payback-new'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).attr('disabled') === 'disabled') {
            return false;
        }

        if (confirm('Do you want to create an export file and tag the lines as completed?')) {
            Meteor.call('create-payback-batch', (err, paybackId) => {
                if (err) {
                    alert(err.reason || err.message);
                } else {
                    FlowRouter.go('/invoice/payback/' + paybackId);
                }
            });
        }
    },
    'click .invoice-send-all'(e, template) {
        e.preventDefault();
        Meteor.call('count-invoices-to-send', (err, count) => {
            if (err) {
                alert(err.reason || err.message);
            } else {
                if (confirm('Do you want to send ' + count + ' invoices?')) {
                    Meteor.call('send-all-invoices', (err, result) => {
                        if (err) {
                            alert(err.reason || err.message);
                        } else {
                            import swal from 'sweetalert2';
                            swal("Sending", "Invoice sending has been started in the background.", "success");
                        }
                    });
                }
            }
        });
    },
    'click .invoice-send-reminders'(e, template) {
        e.preventDefault();
        if (confirm('Do you want to send all invoice reminders?')) {
            Meteor.call('send-invoice-reminders', (err, count) => {
                if (err) {
                    alert(err.reason || err.message);
                } else {
                    import swal from 'sweetalert2';
                    swal("Sending reminders", "Invoice reminder sending has been started in the background.", "success");
                }
            });
        }
    }
});

import './create-sidebar.html';

Template.createInvoicesModal_sidebar.onCreated(function() {
    this.activeItem = new ReactiveVar();

    this.receipts = this.data.options.receipts;
    this.receiptIds = this.data.options.receiptIds;

    this.autorun(() => {
        let data = Template.currentData();
        this.activeItem.set(data.options.activeReceipt.get());
    });
});

Template.createInvoicesModal_sidebar.helpers(createInvoiceHelpers);
Template.createInvoicesModal_sidebar.helpers({
    getReceipt() {
        let template = Template.instance();
        return Receipts.findOne({
            _id: template.activeItem.get()
        });
    },
    gettingReceipts() {
        return Template.instance().data.options.gettingReceipts.get();
    },
    getReceiptFiles() {
        return false;
        return Files.find({
            itemId: this._id,
            target: 'receipts'
        },{
            limit: 10
        });
    }
});

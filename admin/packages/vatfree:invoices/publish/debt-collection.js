Meteor.publishComposite('debt-collection', function(debtCollectionId) {
    check(debtCollectionId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return DebtCollection.find({
                _id: debtCollectionId
            });
        },
        children: [
            {
                find: function (debtCollection) {
                    return DebtCollectionLines.find({
                        debtCollectionId: debtCollection._id
                    });
                }
            },
            {
                find: function (debtCollection) {
                    return Invoices.find({
                        _id: {
                            $in: debtCollection.invoiceIds
                        }
                    });
                }
            }
        ]
    }
});

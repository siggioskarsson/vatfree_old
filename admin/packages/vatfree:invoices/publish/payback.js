Meteor.publishComposite('payback', function(paybackId) {
    check(paybackId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Payback.find({
                _id: paybackId
            });
        },
        children: [
            {
                find: function (payback) {
                    return PaybackLines.find({
                        paybackId: payback._id
                    });
                }
            },
            {
                find: function (payback) {
                    return Invoices.find({
                        _id: {
                            $in: payback.invoiceIds
                        }
                    });
                }
            }
        ]
    }
});

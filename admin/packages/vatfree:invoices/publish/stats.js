import moment from "moment/moment";

Meteor.publish("invoice-stats", function () {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    let self = this;
    let initializing = true;

    var countInvoicesToSend = 0;
    var handleInvoicesToSend = Invoices.find({
        status: 'readyToSend',
        deleted: {
            $ne: true
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countInvoicesToSend++;
            if (!initializing)
                self.changed("stats", 'invoicesToSend', {count: countInvoicesToSend});
        },
        removed: function() {
            countInvoicesToSend--;
            if (!initializing)
                self.changed("stats", 'invoicesToSend', {count: countInvoicesToSend});
        }
    });

    var countInvoicesToSendReminders = 0;
    var handleInvoicesToSendReminders = Invoices.find({
        status: 'sentToRetailer',
        partnershipStatus: {
            $nin: ['uncooperative']
        },
        debtCollectionId: {
            $exists: false
        },
        remindersStop: {
            $exists: false
        },
        $or: [
            {
                reminderSent: {
                    $exists: false
                },
                dueDate: {
                    $lt: moment().subtract(Meteor.settings.defaults.reminderInvoiceDays || 7, 'days').toDate()
                }
            },
            {
                reminder2Sent: {
                    $exists: false
                },
                reminderSent: {
                    $lt: moment().subtract(Meteor.settings.defaults.reminder2InvoiceDays || 7, 'days').toDate()
                }
            }
        ],
        deleted: {
            $ne: true
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countInvoicesToSendReminders++;
            if (!initializing)
                self.changed("stats", 'invoicesToSendReminders', {count: countInvoicesToSendReminders});
        },
        removed: function() {
            countInvoicesToSendReminders--;
            if (!initializing)
                self.changed("stats", 'invoicesToSendReminders', {count: countInvoicesToSendReminders});
        }
    });

    // Observe only returns after the initial added callbacks have
    // run.  Now return an initial value and mark the subscription
    // as ready.
    initializing = false;
    self.added("stats", 'invoicesToSend', {count: countInvoicesToSend});
    self.added("stats", 'invoicesToSendReminders', {count: countInvoicesToSendReminders});
    self.ready();

    // Stop observing the cursor when client unsubs.
    // Stopping a subscription automatically takes
    // care of sending the client any removed messages.
    self.onStop(function () {
        handleInvoicesToSend.stop();
        handleInvoicesToSendReminders.stop();
    });
});

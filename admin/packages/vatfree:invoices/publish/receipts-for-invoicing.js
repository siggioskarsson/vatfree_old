Meteor.publishComposite('receipts-for-invoicing', function(limit, offset) {
    if (!Vatfree.userIsInRole(this.userId, 'invoices-create')) {
        throw new Meteor.Error(404, 'access denied');
    }

    const selector = {
        status: 'readyForInvoicing'
    };

    const sort = {
        receiptNr: 1
    };

    const childFields = {
        name: 1,
        addressFirst: 1,
        postCode: 1,
        postalCode: 1,
        city: 1,
        countryId: 1,
        partnershipStatus: 1
    };

    const children = [
        {
            find: function (receipt) {
                return Shops.find({
                    _id: receipt.shopId
                }, {
                    fields: childFields
                });
            }
        },
        {
            find: function (receipt) {
                return Companies.find({
                    _id: receipt.companyId
                }, {
                    fields: childFields
                });
            }
        }
    ];

    return {
        find: function () {
            return Receipts.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset,
                fields: {
                    reminders: false,
                    textSearch: false
                }
            });
        },
        children: children
    };
});

Meteor.publishComposite('invoices_item', function(invoiceId) {
    check(invoiceId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Invoices.find({
                _id: invoiceId
            });
        },
        children: [
            {
                find: function (invoice) {
                    return Meteor.users.find({
                        _id: invoice.userId
                    });
                }
            },
            {
                find: function (invoice) {
                    return Shops.find({
                        _id: invoice.shopId
                    });
                }
            },
            {
                find: function (invoice) {
                    return Receipts.find({
                        _id: {
                            $in: invoice.receiptIds || []
                        }
                    });
                }
            },
            {
                find: function (invoice) {
                    return Companies.find({
                        _id: invoice.companyId
                    });
                }
            },
            {
                find: function (invoice) {
                    return Invoices.find({
                        _id: invoice.referenceInvoiceId
                    });
                }
            },
            {
                find: function (invoice) {
                    return DebtCollection.find({
                        invoiceIds: invoice._id
                    });
                }
            },
            {
                find: function (invoice) {
                    return DebtCollectionLines.find({
                        invoiceId: invoice._id
                    });
                }
            },
            {
                find: function (invoice) {
                    return Payback.find({
                        invoiceIds: invoice._id
                    });
                }
            },
            {
                find: function (invoice) {
                    return PaybackLines.find({
                        invoiceId: invoice._id
                    });
                }
            },
            {
                find: function (invoice) {
                    return Payments.find({
                        _id: {
                            $in: invoice.paymentIds || []
                        }
                    });
                }
            },
            {
                find: function (invoice) {
                    return Files.find({
                        itemId: invoice._id,
                        target: 'invoices'
                    });
                }
            }
        ]
    };
});

Meteor.publish('debt-collection-lines', function(debtCollectionId) {
    check(debtCollectionId, String);
    let selector = {
        debtCollectionId: debtCollectionId
    };
    if (!debtCollectionId) {
        selector = {
            debtCollectionId: {
                $exists: false
            }
        }
    }

    return DebtCollectionLines.find(selector);
});

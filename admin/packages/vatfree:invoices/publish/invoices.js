Meteor.publishComposite('invoices', function (searchTerm, selector, sort, limit, offset) {
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};
    selector.deleted = {
        $ne: true
    };

    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    const childFields = {
        name: 1,
        addressFirst: 1,
        postCode: 1,
        postalCode: 1,
        city: 1,
        countryId: 1,
        partnershipStatus: 1
    };

    return {
        find: function () {
            return Invoices.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset,
                fields: {
                    mailsSent: false,
                    notes: false,
                    secret: false
                }
            });
        },
        children: [
            {
                find: function (invoice) {
                    return Shops.find({
                        _id: invoice.shopId
                    },{
                        fields: childFields
                    });
                }
            },
            {
                find: function (invoice) {
                    return Companies.find({
                        _id: invoice.companyId
                    },{
                        fields: childFields
                    });
                }
            }
        ]
    };
});

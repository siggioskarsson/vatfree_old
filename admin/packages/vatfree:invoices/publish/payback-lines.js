Meteor.publish('payback-lines', function(paybackId) {
    check(paybackId, String);
    let selector = {
        paybackId: paybackId
    };
    if (!paybackId) {
        selector = {
            paybackId: {
                $exists: false
            }
        }
    }

    return PaybackLines.find(selector);
});

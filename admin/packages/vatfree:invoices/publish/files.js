Meteor.publish('invoice-files', function (invoiceId) {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return Files.find({
        itemId: invoiceId,
        target: 'invoices'
    });
});

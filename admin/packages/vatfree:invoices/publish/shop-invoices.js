Meteor.publishComposite('shop-invoices', function(shopId) {
    check(shopId, String);
    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return {
        find: function () {
            return Invoices.find({
                shopId: shopId
            });
        },
        children: [
            {
                find: function (invoice) {
                    return Meteor.users.find({
                        _id: invoice.userId
                    });
                }
            }
        ]
    }
});

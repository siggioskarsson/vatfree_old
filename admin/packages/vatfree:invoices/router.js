FlowRouter.route('/invoices', {
    action: function() {
        import("meteor/vatfree:invoices/templates")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "invoices"});
            });
    }
});

FlowRouter.route('/invoice/:invoiceId', {
    action: function() {
        import("meteor/vatfree:invoices/templates")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "invoiceEdit"});
            });
    }
});

FlowRouter.route('/invoice/debt-collection/:debtCollectionId', {
    action: function() {
        import("meteor/vatfree:invoices/templates/debt-collection")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "debtCollection"});
            });
    }
});

FlowRouter.route('/invoice/payback/:paybackId', {
    action: function() {
        import("meteor/vatfree:invoices/templates/payback")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "payback"});
            });
    }
});

FlowRouter.route('/invoice/exact/link-api', {
    action: function() {
        import("meteor/vatfree:invoices/templates/exact_link_api")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "invoices_exact_link_api"});
            });
    }
});

DebtCollection.after.insert(function(userId, doc) {
    // update all debt collectionIds
    DebtCollectionLines.update({
        _id: {
            $in: doc.debtCollectionLineIds
        }
    },{
        $set: {
            debtCollectionId: doc._id
        }
    },{
        multi: 1
    });

    // update all invoices with the debt collection link
    Invoices.update({
        _id: {
            $in: doc.invoiceIds
        }
    },{
        $set: {
            debtCollectionId: doc._id
        }
    },{
        multi: 1
    });

    DebtCollection.updateTextSearch(doc);
});

DebtCollection.updateTextSearch = function(doc) {
    let textSearch = doc.debtCollectionNumber.toString();

    _.each(doc.invoiceIds, (invoiceId) => {
        let invoice = Invoices.findOne({_id: invoiceId}, {fields: {textSearch: 1}});
        if (invoice) {
            textSearch += ' ' + invoice.textSearch;
        }
    });

    DebtCollection.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch.toLowerCase().normalize()
        }
    });
};

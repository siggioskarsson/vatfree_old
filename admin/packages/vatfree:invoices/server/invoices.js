import moment from 'moment';
import { pruneEmpty } from 'meteor/vatfree:core/lib/prune';

try {
    Invoices._ensureIndex({'invoiceNr': 1}, {unique: 1});
    Invoices._ensureIndex({'userId': 1});
    Invoices._ensureIndex({'shopId': 1});
    Invoices._ensureIndex({'createdAt': 1});
    Invoices._ensureIndex({'geo': "2dsphere"});
    Invoices._ensureIndex({'textSearch': "text"}, {"language_override": "_text_language"});
    Vatfree.synchronizationSchemaIndex(Invoices);
} catch(e) {
    console.error(e);
}

ActivityStream.attachHooks(Invoices);
// Add index to activity stream collection for invoices
ActivityStream.collection._ensureIndex({'securityContext.invoiceId': 1});

let createInvoicePDF = function (doc) {
// Create a job:
    let job = new Job(invoiceJobs, 'invoicePdf',
        {
            invoiceId: doc._id,
            invoiceNr: doc.invoiceNr
        }
    );
    // Set some properties of the job and then submit it
    job.priority('normal')
        .retry({
            retries: 5,
            wait: 5 * 60 * 1000
        })  // 5 minutes between attempts
        .save();               // Commit it to the server
};

let createInvoiceImagesPDF = function (invoice) {
    let job = new Job(invoiceJobs, 'invoicePdfImages',
        {
            invoiceId: invoice._id,
            invoiceNr: invoice.invoiceNr
        }
    );
    // Set some properties of the job and then submit it
    job.priority('normal')
        .retry({
            retries: 5,
            wait: 5 * 60 * 1000
        })  // 5 minutes between attempts
        .save();               // Commit it to the server
};


Invoices.updateEuroAmounts = function(invoice) {
    if (!invoice || !invoice._id) return;

    const date = moment(invoice.invoiceDate).format('YYYY-MM-DD');
    const values = {
        amount: invoice.amount || null,
        totalVat: invoice.totalVat || null,
        refund: invoice.refund || null,
        serviceFee: invoice.serviceFee || null,
        affiliateServiceFee: invoice.affiliateServiceFee || null,
        deferredServiceFee: invoice.deferredServiceFee || null
    };

    let exchangedValues;
    if (invoice.currencyId !== Meteor.settings.defaults.currencyId) {
        exchangedValues = Vatfree.exchangeRates.getEuroValuesById(date, values, invoice.currencyId);
    } else {
        exchangedValues = values;
    }

    Invoices.direct.update({
        _id: invoice._id
    },{
        $set: {
            euroAmounts: pruneEmpty(exchangedValues)
        }
    });
};

Invoices.before.insert(function(userId, doc) {
    if (doc.invoiceDate && doc.paymentTerms) {
        doc.dueDate = moment(doc.invoiceDate).add(doc.paymentTerms, 'days').toDate();
    }
});

Invoices.after.insert(function(userId, doc) {
    Invoices.updateInvoiceTextSearch(doc);

    // update receipts with invoiceId, except for credit invoices
    if (doc.type !== 'credit') {
        Receipts.update({
            _id: {
                $in: doc.receiptIds || []
            },
            invoiceId: {
                $exists: false
            },
            status: 'readyForInvoicing'
        },{
            $set: {
                invoiceId: doc._id,
                status: 'waitingForPayment'
            }
        },{
            multi: 1
        });
    }

    Meteor.defer(() => {
        Vatfree.notify.processNotificationTriggers(userId, 'invoices', '- insert -', false, doc._id);
        Invoices.updateEuroAmounts(doc);

        // update stats in shops
        Invoices.updateShopInvoiceStatistics(doc.shopId, doc.companyId);

        if (doc.status === 'new') {
            // create PDF docs if this invoice was created as new
            createInvoicePDF(doc);
            let entity = false;
            if (doc.companyId) {
                entity = Companies.findOne({_id: doc.companyId});
            } else {
                entity = Shops.findOne({_id: doc.shopId});
            }
            if (entity && (entity.includeImagesPDF || entity.partnershipStatus === 'new')) {
                createInvoiceImagesPDF(doc);
            }
        } else if (doc.status === 'credit') {
            createInvoicePDF(doc);
        }
    });
});

Invoices.before.update(function(userId, doc, fieldNames, modifier, options) {
    if (_.contains(fieldNames, 'deleted') && modifier.$set && modifier.$set.deleted) {
        if (doc.status === 'new' || doc.status === 'toCheck' || doc.status === 'readyToSend' || doc.status === 'sentToRetailer' || doc.status === 'promisedToPay') {
            Receipts.update({
                _id: {
                    $in: doc.receiptIds || []
                },
                status: 'waitingForPayment'
            },{
                $set: {
                    status: 'readyForInvoicing'
                },
                $unset: {
                    invoiceId: 1
                }
            },{
                multi: true
            });

            DebtCollectionLines.remove({
                invoiceId: doc._id,
                debtCollectionId: {
                    $exists: false
                }
            },{
                multi: true
            });
        } else {
            throw new Meteor.Error(500, 'Not allowed');
        }
    }

    if ((doc.status === 'partiallyPaid' || doc.status === 'paymentDifference') && _.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status && modifier.$set.status === 'paid') {
        if (!_.contains(fieldNames, 'paymentDifference') && modifier.$set && modifier.$set.paymentDifference && modifier.$set.paymentDifference !== 0) {
            if (!_.contains(fieldNames, 'paybackLineId') && modifier.$set && !modifier.$set.paybackLineId) {
                throw new Meteor.Error(500, 'Paid status can only be set if payment difference or payback is being set');
            }
        }
    } else if (_.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status && (modifier.$set.status === 'paid' || modifier.$set.status === 'partiallyPaid' || modifier.$set.status === 'paymentDifference')) {
        // Check whether we attach a paymentId to the invoice when marking it as paid, otherwise not allowed
        if (!_.contains(fieldNames, 'paymentIds') || !_.contains(fieldNames, 'paidAt')) {
            throw new Meteor.Error(500, 'Paid status can only be set when linking payments');
        } else {
            // check payments being set, are these already linked to the invoice and do the numbers add up ?
            if (modifier.$addToSet && modifier.$addToSet.paymentIds) {
                let payment = Payments.findOne({_id: modifier.$addToSet.paymentIds});
                if (payment) {
                    if (!payment.invoiceIds || !_.contains(payment.invoiceIds, doc._id)) {
                        throw new Meteor.Error(500, 'Invoice is not part of payment');
                    }
                } else {
                    throw new Meteor.Error(500, 'Could not find payment');
                }
            } else {
                throw new Meteor.Error(500, 'Could not verify payment');
            }
        }
    }
});

const updatePartnershipStatusOfEntity = function (entity, doc) {
    if (entity && (entity.partnershipStatus === 'uncooperative' || entity.partnershipStatus === 'new')) {
        if (doc.shopId) {
            Shops.update({
                _id: doc.shopId
            }, {
                $set: {
                    partnershipStatus: 'pledger'
                }
            });
        } else {
            Companies.update({
                _id: doc.companyId
            }, {
                $set: {
                    partnershipStatus: 'pledger'
                }
            });
        }
    }
};

const updateEntityToPledger = function (doc) {
    // check parent entity status and set to pledger if uncooperative, since invoice was paid
    let entity = {};
    if (doc.shopId) {
        entity = Shops.findOne({
            _id: doc.shopId
        }, {
            fields: {
                partnershipStatus: 1
            }
        });
    } else if (doc.companyId) {
        entity = Companies.findOne({
            _id: doc.companyId
        }, {
            fields: {
                partnershipStatus: 1
            }
        });
        // update all shops in this invoice, need to go through all the receipts
        Receipts.find({
            _id: {
                $in: doc.receiptIds || []
            }
        }, {
            fields: {
                _id: 1,
                shopId: 1
            }
        }).forEach((receipt) => {
            const shop = Shops.findOne({_id: receipt.shopId}, {fields: { partnershipStatus: 1 }});
            updatePartnershipStatusOfEntity(shop, receipt);
        });
    }
    updatePartnershipStatusOfEntity(entity, doc);
};

Invoices.after.update(function(userId, doc, fieldNames, modifier, options) {
    Invoices.updateInvoiceTextSearch(doc);

    if (_.contains(fieldNames, 'status') && modifier.$set && modifier.$set.status && modifier.$set.status !== this.previous.status) {
        // paymentDifference == too much paid !
        if (modifier.$set.status === 'paid' || modifier.$set.status === 'paymentDifference') {
            let rate = false;
            if (doc.euroAmount) {
                // we need to calculate the euro refund amount for all the invoices
                rate = doc.euroAmount / doc.amount;
            }

            _.each(doc.receiptIds, (receiptId) => {
                let receipt = Receipts.findOne({_id: receiptId});
                let user = Meteor.users.findOne({_id: receipt.userId});
                let userVerified = user && user.private && user.private.status === 'userVerified';

                let setData = {
                    status: userVerified ? 'paidByShop' : 'userPendingForPayment'
                };
                if (rate) {
                    setData.euroRefund = Math.round(receipt.refund * rate);
                    setData.euroServiceFee = Math.round(receipt.serviceFee * rate);
                }

                Receipts.update({
                    _id: receipt._id,
                    status: {
                        $in: ['waitingForPayment', 'notCollectable']
                    }
                },{
                    $set: setData
                });
            });

            if (modifier.$set.status === 'paid') {
                Meteor.defer(() => {
                    updateEntityToPledger(doc);
                });
            }
        } else if (modifier.$set.status === 'notCollectable') {
            // set all receipts linked to this invoice as not collectable
            Receipts.update({
                _id: {
                    $in: doc.receiptIds
                },
                status: 'waitingForPayment'
            },{
                $set: {
                    status: 'notCollectable',
                    receiptRejectionIds: modifier.$set.receiptRejectionIds || []
                }
            },{
                multi: true
            });
        } else if (this.previous.status === 'notCollectable') {
            // invoice was re-opened, we must re-open the receipts as well
            Receipts.update({
                _id: {
                    $in: doc.receiptIds
                },
                status: 'notCollectable'
            },{
                $set: {
                    status: 'waitingForPayment'
                },
                $unset: {
                    receiptRejectionIds: true
                }
            },{
                multi: true
            });

            // remove the activities linked to this invoice that are still open
            let activityUpdateSelector = {
                type: 'task',
                subject: 'Not-collectable invoice - please do something',
                status: 'new',
                invoiceId: doc._id
            };
            ActivityLogs.update(activityUpdateSelector, {
                $set: {
                    subject: 'Not-collectable invoice - paid by shop',
                    status: 'done'
                }
            }, {
                multi: true
            });
        } else if ((modifier.$set.status === 'sentToRetailer' || modifier.$set.status === 'credited') && this.previous.status === 'paid') {
            // invoice charge back, set all receipts back to waiting for payment
            _.each(doc.receiptIds, (receiptId) => {
                let receipt = Receipts.findOne({_id: receiptId});
                let markReceiptAsUnpaid = function (receiptId) {
                    Receipts.update({
                        _id: receiptId
                    }, {
                        $set: {
                            status: 'waitingForPayment'
                        }
                    });
                };
                if (receipt.payoutId) {
                    // check whether we can cancel the payout, we cannot cancel if it has been paid
                    let payout = Payouts.findOne({_id: receipt.payoutId});
                    if (payout && payout.status !== 'paid') {
                        Vatfree.removeReceiptFromPayout(receipt.payoutId, receiptId);
                        markReceiptAsUnpaid(receiptId);
                    } else {
                        // receipt has been paid out - this is not good
                        let activityLog = {
                            type: 'task',
                            status: 'new',
                            subject: "Invoice payment has been canceled, but receipt is already paid",
                            receiptId: receipt._id,
                            invoiceId: doc._id
                        };
                        ActivityLogs.insert(activityLog);
                    }
                } else {
                    markReceiptAsUnpaid(receiptId);
                }
            });
        }
    }

    Meteor.defer(() => {
        Vatfree.notify.checkStatusChange('invoices', userId, this.previous, doc, fieldNames, modifier);
        Invoices.updateEuroAmounts(doc);
    });
});


Invoices.updateInvoiceTextSearch = function(doc) {
    let textSearch = doc.invoiceNr.toString();
    textSearch += ' ' + doc.salesforceId;
    textSearch += ' amount:' + doc.amount;
    textSearch += ' status:' + doc.status;

    let entity = {};
    if (doc.shopId) {
        entity = Shops.findOne({
            _id: doc.shopId
        });
    } else if (doc.companyId) {
        entity = Companies.findOne({
            _id: doc.companyId
        });
    }
    if (entity) {
        textSearch += ' ' + entity.textSearch;
    }

    if (doc.debtCollectionId) {
        let debtCollection = DebtCollection.findOne({_id: doc.debtCollectionId}) || {};
        textSearch += ' ' + debtCollection.debtCollectionNumber;
    }

    if (doc.paybackId) {
        let payback = Payback.findOne({_id: doc.paybackId}) || {};
        textSearch += ' ' + payback.paybackNumber;
    }

    Invoices.direct.update({
        _id: doc._id
    },{
        $set: {
            textSearch: textSearch.toLowerCase().normalize()
        }
    });
};

Invoices.updateShopInvoiceStatistics = function(shopId, companyId) {
    let stats = {
        'stats.numberOfInvoices': 0,
        'stats.totalInvoicedRefunds': 0,
        'stats.totalInvoicedServiceFee': 0,
        'stats.lastInvoicedDate': null
    };

    if (shopId) {
        Invoices.find({
            shopId: shopId
        }).forEach((invoice) => {
            stats['stats.numberOfInvoices']++;
            stats['stats.totalInvoicedRefunds'] += invoice.refund;
            stats['stats.totalInvoicedServiceFee'] += invoice.serviceFee;
            if (invoice.createdAt > stats['stats.lastInvoicedDate']) {
                stats['stats.lastInvoicedDate'] = invoice.createdAt;
            }
        });

        Shops.direct.update({
            _id: shopId
        },{
            $set: stats
        });
    } else if (companyId) {
        Invoices.find({
            companyId: companyId
        }).forEach((invoice) => {
            stats['stats.numberOfInvoices']++;
            stats['stats.totalInvoicedRefunds'] += invoice.refund;
            stats['stats.totalInvoicedServiceFee'] += invoice.serviceFee;
            if (invoice.createdAt > stats['stats.lastInvoicedDate']) {
                stats['stats.lastInvoicedDate'] = invoice.createdAt;
            }
        });

        Companies.direct.update({
            _id: companyId
        },{
            $set: stats
        });
    }
};

Meteor.startup(() => {
    return false;
    let fs = Npm.require('fs');
    fs.readFile('/Users/oskarsson/Downloads/invoices.csv', 'utf8', Meteor.bindEnvironment(function (err, data) {
        if (err) {
            console.log('Error: ' + err);
            return;
        }

        let rows = data.replace("\r", "").split("\n");
        rows.shift();
        _.each(rows, (row) => {
            let line = row.split(",");
            let geo = null;
            if (line[6] && line[7]) {
                geo = {
                    type: "Point",
                    coordinates: [
                        Number(line[6]),
                        Number(line[7])
                    ],
                };
            }
            let invoice = {
                id: line[0],
                name: line[1],
                addressFirst: line[2],
                postCode: line[3],
                city: line[4],
                country: line[5],
                geo: geo,
                partnershipStatus: line[8]
            };
            Invoices.insert(invoice);
        });

        console.log('rows.length', rows.length);
    }));
});

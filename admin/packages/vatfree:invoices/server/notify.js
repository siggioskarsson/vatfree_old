Vatfree.notify.sendStatusNotification['invoices'] = function (userId, invoiceId, travellerStatusNotify) {
    try {
        let invoice = Invoices.findOne({_id: invoiceId});
        let currency = Currencies.findOne({_id: invoice.currencyId});
        let entity = {};
        if (invoice.companyId) {
            entity = Companies.findOne({_id: invoice.companyId});
        } else {
            entity = Shops.findOne({_id: invoice.shopId});
        }
        let emailLanguage = invoice.language || entity.language || "en";

        let referenceInvoice = {};
        if (invoice.referenceInvoiceId) {
            referenceInvoice = Invoices.findOne({_id: invoice.referenceInvoiceId });
        }

        let activityIds = {
            invoiceId: invoiceId
        };

        if (invoice.companyId) {
            activityIds.companyId = invoice.companyId;
        } else {
            activityIds.shopId = invoice.shopId;
        }

        let templateData = {
            language: emailLanguage,
            invoice: invoice,
            company: entity,
            currency: currency,
            referenceInvoice: referenceInvoice
        };

        let toAddress = entity.billingEmail || entity.email;
        let fromAddress = 'Finance vatfree.com <finance@vatfree.com>';

        Vatfree.notify.sendNotification(travellerStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage);
    } catch (e) {
        ActivityLogs.insert({
            type: 'task',
            userId: userId,
            subject: 'Sending of invoice to company failed',
            status: 'new',
            invoiceId: invoiceId,
            notes: "Error: " + e.message
        });
    }
};

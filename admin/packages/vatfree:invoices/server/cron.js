import { syncInvoicesToExact } from './synch/exact';

/*
 # ┌───────────── min (0 - 59)
 # │ ┌────────────── hour (0 - 23)
 # │ │ ┌─────────────── day of month (1 - 31)
 # │ │ │ ┌──────────────── month (1 - 12)
 # │ │ │ │ ┌───────────────── day of week (0 - 6) (0 to 6 are Sunday to Saturday, or use names; 7 is Sunday, the same as 0)
 # │ │ │ │ │
 # │ │ │ │ │
 # * * * * *  command to execute
 */
if (Meteor.settings && Meteor.settings.cron) {
    if (Meteor.settings.cron.invoiceRemindersCron) {
        SyncedCron.add({
            name: 'Send all invoice reminders',
            schedule: function(parser) {
                return parser.cron(Meteor.settings.cron.invoiceRemindersCron);
            },
            job: function() {
                Vatfree.sendAllInvoiceReminders((err, numberSent) => {
                    if (err) {
                        ActivityLogs.insert({
                            type: 'task',
                            subject: 'ERROR: sending invoice reminders',
                            status: 'new',
                            error: err
                        });
                    } else {
                        ActivityLogs.insert({
                            type: 'burny',
                            subject: 'Invoice reminders sent',
                            status: 'done',
                            notes: "Sent " + (numberSent ? numberSent : 0) + " reminders"
                        });
                    }
                });
            }
        });
    }

    if (Meteor.settings.cron.exact && Meteor.settings.cron.exact.invoices) {
        SyncedCron.add({
            name: 'Send new invoices to book keeping',
            schedule: function (parser) {
                // parser is a later.parse object
                // every day at 23:13 am
                // '13 23 * * *'
                return parser.cron(Meteor.settings.cron.exact.invoices);
            },
            job: function (intendedAt) {
                syncInvoicesToExact((err, numberSynched) => {
                    if (err) {
                        ActivityLogs.insert({
                            type: 'task',
                            subject: 'ERROR: synching invoices to Exact',
                            status: 'new',
                            error: err
                        });
                    } else if (numberSynched > 0) {
                        ActivityLogs.insert({
                            type: 'burny',
                            subject: 'Invoices synched to Exact',
                            status: 'done',
                            notes: "Synched " + (numberSynched ? numberSynched : 0) + " invoices"
                        });
                    }
                });
            }
        });
    }
}

Meteor.methods({
    "jobs.syncInvoices": function() {
        if (Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            console.log('jobs.syncInvoices -> syncInvoicesToExact');
            return new Promise(function(resolve, reject) {
                syncInvoicesToExact((err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                });
            });
        }
    }
});

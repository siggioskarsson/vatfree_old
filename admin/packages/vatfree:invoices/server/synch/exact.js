import moment from 'moment';
import { createClient } from '../lib/exact-online';

export const DEBUGGING = Meteor.settings.exact ? Meteor.settings.exact.debug : false;
export const SERVICE_ID = 'nl.exactonline.start';
export const Ledger = {
    '2530': "4e92acd1-e663-47aa-a695-9d06769621c7", // Kruispost VTB
    '2540': "a9c62965-7770-4c00-bbae-5aa2a9333791", // SEPA
    '2545': "8610dbcf-5c60-4178-a043-44e6b1df1d74", // Paypal
    '2550': "67c4f3cf-4096-4119-bade-710816b29abb", // Credit Card
    '2560': "8ed011e0-7618-498e-beb2-57d0b9653c37", // AliPay
    '2570': "1eb2ee6e-848f-48db-b274-64eb0a275051", // WeChat
    '8000': "cd894db0-fa92-4a94-a44e-62c35fe14bb4"
};
const customerId = "26c38dc5-a1fd-4220-aaf6-afeee1bbff56";

let exactClient = null;
export const getExactClient = function() {
    if (!exactClient) {
        exactClient = createClient({
            clientId: Meteor.settings.exact.clientId, // OAuth Client ID
            clientSecret: Meteor.settings.exact.clientSecret, // OAuth Client Secret
            redirectUri: Meteor.settings.exact.redirectUri,
            env: 'production', // Environment (production/development)
            debug: DEBUGGING
        });
    }

    return exactClient;
};

export const getAuthorizedExactClient = function(callback) {
    try {
        const exactClient = getExactClient();
        const synch = Synchs.findOne({_id: SERVICE_ID}) || {};

        exactClient.setRefreshToken(synch.refresh_token);
        exactClient.setDivision(Meteor.settings.exact.division);
        exactClient.refreshToken((err, refreshToken) => {
            if (err) {
                callback(err);
            } else {
                Synchs.upsert({
                    _id: SERVICE_ID
                },{
                    $set: refreshToken
                });
                callback(null, exactClient);
            }
        });
    } catch(e) {
        console.error(e);
        callback(e);
    }
};

Vatfree.synch.addPlugin('exact-invoices', {
    whoami(callback) {
        // Get current user
        getAuthorizedExactClient((err, exactClient) => {
            exactClient.sys.me((err, result) => {
                if (err) {
                    callback(err);
                } else {
                    callback(null, result.d.results || null);
                }
            });
        });
    },
    to(callback) {
        syncInvoicesToExact(callback);
    },
    from(callback) {
        throw new Meteor.Error(500, 'Not implemented');
    }
});


export const syncInvoicesToExact = function(callback) {
    if (!Meteor.settings.exact) {
        callback(new Meteor.Error(500, "Exact settings not defined"));
        return false;
    }

    try {
        if (DEBUGGING) console.log('Synching invoices to Exact', 'starting exact client');
        getAuthorizedExactClient((err, exactClient) => {
            if (err) {
                callback(err);
                return false;
            }

            exactClient.sys.me((err, userResult) => {
                if (err) {
                    callback(err);
                    return false;
                }

                const userData = userResult.d.results;

                if (DEBUGGING) console.log('Synching invoices to Exact', 'getting invoices');
                let nrSynched = 0;
                Invoices.find({
                    'synchronization.serviceId': {
                        $ne: SERVICE_ID
                    },
                    paidAt: {
                        $gte: new Date("2018-01-01 00:00:00")
                    }
                },{
                    limit: Meteor.settings.exact.limit
                }).forEach((invoice) => {
                    // sync invoice to Exact
                    if (DEBUGGING) console.log('Synching invoice', invoice._id, invoice.invoiceNr);

                    let entity;
                    if (invoice.shopId) {
                        entity = Shops.findOne({_id: invoice.shopId});
                    } else {
                        entity = Companies.findOne({_id: invoice.companyId});
                    }
                    if (!entity) {
                        // report this is a problem ...
                        console.error('no entity found for invoice', invoice._id, invoice.invoiceNr);
                        return false;
                    }

                    /*
                    let customerId;
                    const entitySynch = _.find(entity.synchronization, (syncDoc) => {
                        return syncDoc.serviceId === SERVICE_ID;
                    });
                    if (!entitySynch) {
                        // synch entity to book keeping and get link Id ...
                        customerId = syncExactCustomer(exactClient, invoice.shopId ? Shops : Companies, entity);
                    } else {
                        customerId = entitySynch.serviceLinkId;
                    }
                    if (!customerId) {
                        // report this is a problem ...
                        console.error('no customerId found for exact crm account');
                        return false;
                    }
                    */

                    const salesEntrySearch = syncExactSearchSalesEntry(exactClient, "ExternalLinkReference eq '" + invoice._id + "'");
                    if (salesEntrySearch && salesEntrySearch.length > 0) {
                        // Found an old entry, update sync status
                        if (DEBUGGING) console.log('Found an old entry', salesEntrySearch);
                        Vatfree.synch.upateSynchStatus(Invoices, invoice._id, SERVICE_ID, salesEntrySearch[0].EntryID, salesEntrySearch[0]);
                    } else {
                        const currency = Currencies.findOne({_id: invoice.currencyId || Meteor.settings.defaults.currencyId}, {fields: { code: 1 } });
                        const salesEntry = {
                            "Customer" : customerId,
                            "Description" : "Phoenix VTB " + invoice.invoiceNr,
                            "EntryDate" : moment(invoice.paidAt).format('YYYY-MM-DD'),
                            "DueDate" : moment(invoice.paidAt).format('YYYY-MM-DD'),
                            "Journal" : "VTB",
                            "ReportingPeriod" : moment(invoice.paidAt).format('Q'),
                            "ReportingYear" : moment(invoice.paidAt).format('YYYY'),
                            "YourRef" : '' + invoice.invoiceNr,
                            "ExternalLinkReference" : invoice._id,
                            "Currency": currency.code,
                            "Type": 20,
                            "SalesEntryLines": [
                                {
                                    "AmountFC" : Vatfree.numbers.formatAmount(invoice.serviceFee),
                                    "GLAccount" : Ledger['8000'],
                                    "Type": 20,
                                    "VATCode" : "VN"
                                },
                                {
                                    "AmountFC" : Vatfree.numbers.formatAmount(invoice.refund),
                                    "GLAccount" : Ledger['2530'],
                                    "Type": 20
                                }
                            ]
                        };

                        if (DEBUGGING) console.log('salesEntry', JSON.stringify(salesEntry));
                        const salesEntryId = syncExactSalesEntry(exactClient, invoice._id, salesEntry);
                        nrSynched++;
                    }
                });
                callback(null, nrSynched);
            });
        });
    } catch(e) {
        console.error(e);
        callback(e);
    }
};

const syncExactSearchSalesEntryAsync = function(exactClient, searchTerm, callback) {
    try {
        exactClient.salesentry.searchSalesEntry(searchTerm, function(err, result) {
            if (err) {
                callback(err);
            } else {
                callback(null, result.d.results || null);
            }
        });
    } catch(e) {
        callback(e);
    }
};
export const syncExactSearchSalesEntry = Meteor.wrapAsync(syncExactSearchSalesEntryAsync);

const syncExactSalesEntryAsync = function(exactClient, invoiceId, salesEntry, callback) {
    try {
        exactClient.salesentry.createSalesEntry(salesEntry, function (err, result) {
            if (err) {
                callback(err);
            } else {
                if (DEBUGGING) console.log('createSalesEntry', result);
                const salesEntryId = result.d.EntryID;

                Vatfree.synch.upateSynchStatus(Invoices, invoiceId, SERVICE_ID, salesEntryId, result.d);

                callback(null, salesEntryId);
            }
        });
    } catch(e) {
        console.error(e);
        callback(e);
    }
};
export const syncExactSalesEntry = Meteor.wrapAsync(syncExactSalesEntryAsync);

const syncExactCustomerAsync = function(exactClient, collection, doc, callback) {
    try {
        const country = Countries.findOne({_id: doc.countryId}) || {};
        const customerEntry = {
            "Name" : doc.name,
            "AddressLine1": doc.addressFirst,
            "Postcode": (doc.postCode || doc.postalCode),
            "City": doc.city,
            "Country": country.code,
            "Language": doc.language,
            "Type": "A",
            "Remarks": doc._id
        };

        exactClient.crm.searchAccount("Remarks eq '" + doc._id + "'", function (err, result) {
            if (err) {
                callback(err);
            } else {
                if (result.d.results.length > 0) {
                    const customerEntryId = result.d.results[0].ID;
                    Vatfree.synch.upateSynchStatus(collection, doc._id, SERVICE_ID, customerEntryId, result);
                    callback(null, customerEntryId);
                } else {
                    exactClient.crm.createAccount(customerEntry, function (err, result) {
                        if (err) {
                            callback(err);
                        } else {
                            const customerEntryId = result.d.ID;
                            Vatfree.synch.upateSynchStatus(collection, doc._id, SERVICE_ID, customerEntryId, result);
                            callback(null, customerEntryId);
                        }
                    });
                }
            }
        });
    } catch(e) {
        console.error(e);
        callback(e);
    }
};
export const syncExactCustomer = Meteor.wrapAsync(syncExactCustomerAsync);

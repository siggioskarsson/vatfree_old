/* global Vatfree: true */

const csv = require('csvtojson');
const DEBUGGING = false;
import moment from 'moment';

const colMapping = {
    "VTB: ID": "salesforceId",
    "PHNX EXPORT SHOP ID": "entityId",
    "PHNX EXPORT VPS1 ID": "salesforcePaymentIds1",
    "PHNX EXPORT VPS2 ID": "salesforcePaymentIds2",
    "PHNX EXPORT VPS3 ID": "salesforcePaymentIds3",
    "VTB: VTB no.": "invoiceNr",
    "Communication in": "language",
    "S_send": "invoiceDate",
    "VTB: Created Date": "createdAt",
    "S_resend": false,
    "D_sent by post": "sendByMethodPost",
    "reminddate": "dueDate",
    "S_send_1REminder": "reminderSent",
    "S_send_2REminder": "reminder2Sent",
    "S_payment completed": "paidAt",
    "S_not_collectable": "",
    "NLVAT6": "",
    "NLVAT19": "",
    "NLVAT_total": "amount",
    "PHNX EXPORT STATUS": "status"
};

const convert = {
    "amount": "currency",
    "language": "lowerCase",
    "invoiceDate": "date",
    "createdAt": "date",
    "dueDate": "date",
    "reminderSent": "date",
    "reminder2Sent": "date",
    "paidAt": "date"
};

const cleanupUpdateData = function(updateData) {
    let entity = Shops.findOne({id: updateData['entityId']}) || {};
    if (!entity._id) {
        entity = Companies.findOne({id: updateData['entityId']}) || {};
        updateData['companyId'] = entity._id;
    } else {
        updateData['shopId'] = entity._id;
    }
    updateData['countryId'] = entity['countryId'];
    updateData['partnershipStatus'] = entity['partnershipStatus'];

    updateData['currencyId'] = Meteor.settings.defaults.currencyId;
    updateData['paymentTerms'] = moment(updateData['dueDate']).diff(moment(updateData['invoiceDate']), 'days');

    if (updateData['language'] === 'du') {
        updateData['language'] = 'de';
    }

    updateData['sendBy'] = updateData['sendByMethodPost'] ? 'post' : 'email';

    if (!updateData['invoiceDate']) updateData['invoiceDate'] = updateData['createdAt'];

    // add missing data
    updateData['secret'] = Random.secret();
    updateData['euroAmount'] = updateData['amount'];
    updateData['totalVat'] = 0;
    updateData['serviceFee'] = 0;
    updateData['refund'] = 0;
    updateData['receiptIds'] = [];
    updateData['invoiceSent'] = updateData['invoiceDate'];
    updateData['paymentIds'] = ['salesforce'];
    updateData['notes'] = "imported from salesforce";

    return updateData;
};
const saveInvoice = function(updateData) {
    let invoice = Invoices.findOne({
        'salesforceId': updateData['salesforceId']
    });

    // set all languages to English
    if (invoice) {
        Invoices.direct.update({
            _id: invoice._id
        }, {
            $set: updateData
        });
        if (DEBUGGING) console.log('updating invoice', updateData['salesforceId'], invoice._id);
        return invoice._id;
    } else {
        if (DEBUGGING) console.log('saving new invoice', updateData['salesforceId']);
        Invoices.direct.insert(updateData, (err, invoiceId) => {
            if (err) {
                console.log('ERROR inserting invoice', updateData['salesforceId']);
                console.error(err);
            } else {
                updateData._id = invoiceId;
                Invoices.updateInvoiceTextSearch(updateData);
                Invoices.updateShopInvoiceStatistics(updateData.shopId, updateData.companyId);
            }
        });
    }
};

Vatfree['import-invoices'] = function (importFile, fileObject, callback) {
    Vatfree.setImportFileProgress(10, 'Populating lookup data', fileObject);

    let totalLines = Vatfree.importFileLines(importFile);
    let lines = 0;
    Vatfree.setImportFileProgress(12, 'Initializing csv', fileObject);
    csv({
        delimiter: ';',
        noheader: false,
        trim: true,
        flatKeys: true,
        constructResult: false
    }).fromFile(importFile)
        .on('json', Meteor.bindEnvironment((row) => {
            lines++;
            if (row &&  _.size(row) > 2) {
                if (lines % 100 === 0) {
                    console.log('progress', 12 + Math.round(89 * (lines/totalLines)), 'Importing ' + lines + ' of ' + totalLines);
                    Vatfree.setImportFileProgress(12 + Math.round(89 * (lines/totalLines)), 'Importing ' + lines + ' of ' + totalLines, fileObject);
                }

                let updateData = Vatfree.import.processColumns(colMapping, convert, row);
                cleanupUpdateData(updateData);
                //console.log(row, updateData);

                if (!updateData['shopId'] && !updateData['companyId']) {
                    if (DEBUGGING) console.error('Could not find entity for invoice', updateData['salesforceId']);
                } else if (updateData['amount'] > 0) {
                    saveInvoice(updateData);
                }
            }
        }))
        .on('done', Meteor.bindEnvironment((error) => {
            Vatfree.setImportFileProgress(100, 'Done', fileObject);
            if (callback) {
                callback();
            }
        }));
};

import moment from 'moment';
import { Meteor } from "meteor/meteor";
import { getCollectionDescription } from 'meteor/vatfree:core/lib/collections';
import { getExactClient } from './synch/exact';

const getPaymentDifference = function (invoice) {
    let amountPaid = 0;
    Payments.find({
        invoiceIds: invoice._id
    }).forEach((payment) => {
        let invoiceAmount = _.find(payment.invoiceAmounts, (amount) => {
            if (amount.invoiceId === invoice._id) {
                return amount;
            }
        }) || {};

        if (invoiceAmount) {
            amountPaid += invoiceAmount.amount;
        }
    });

    return amountPaid - invoice.amount;
};

Vatfree.createInvoices = function (receiptIds, callback) {
    console.log('Creating invoices for # receipts', receiptIds.length);

    let selector = {
        status: "readyForInvoicing"
    };
    if (receiptIds.length > 0) {
        selector['_id'] = {
            $in: receiptIds || []
        }
    }

    let groupedReceipts = {};
    let shops = {};
    let billingCompany = {};
    console.time('CreateInvoicesJob - Grouping receipts');
    Receipts.find(selector, {
        fields: {
            shopId: 1,
            currencyId: 1
        }
    }).forEach((receipt) => {
        if (!_.has(shops, receipt.shopId)) {
            shops[receipt.shopId] = Shops.findOne({_id: receipt.shopId});
        }
        let shop = shops[receipt.shopId];

        let id = "shopId:" + receipt.shopId + ':' + receipt.currencyId + ':' + (shop.invoicePerReceipt ? receipt._id : '*');
        if (shop.billingEntity) {
            if (!_.has(groupedReceipts, id)) {
                groupedReceipts[id] = [];
            }
            groupedReceipts[id].push(receipt._id);
        } else {
            if (!_.has(billingCompany, shop.companyId)) {
                billingCompany[shop.companyId] = Vatfree.billing.getBillingEntityForCompany(shop.companyId);
            }

            if (billingCompany[shop.companyId]) {
                let billingCompanyId = billingCompany[shop.companyId]._id;
                id = "companyId:" + billingCompanyId + ':' + receipt.currencyId + ':' + (billingCompany[shop.companyId].invoicePerReceipt ? receipt._id : '*');
                if (!_.has(groupedReceipts, id)) {
                    groupedReceipts[id] = [];
                }
                groupedReceipts[id].push(receipt._id);
            }
        }
    });
    console.timeEnd('CreateInvoicesJob - Grouping receipts');

    // create invoices
    let count = {
        success: 0,
        error: 0
    };
    console.time('CreateInvoicesJob - create invoices');
    console.log('Creating invoices', _.size(groupedReceipts));
    _.each(groupedReceipts, (receiptIds, id) => {
        let group = id.split(':');
        let entityField = group[0];
        let entityId = group[1];
        let currencyId = group[2];

        try {
            if (entityField === 'companyId') {
                createInvoiceForCompany(entityId, receiptIds, currencyId);
                count.success++;
            } else if (entityField === 'shopId') {
                createInvoiceForShop(entityId, receiptIds, currencyId);
                count.success++;
            } else {
                console.error('no entitity field found when creating invoice', id);
                count.error++;
            }
        } catch(e) {
            let activityLog = {
                type: 'task',
                status: 'new',
                subject: "Could not create invoice",
                notes: e.message
            };
            if (entityField === 'shopId') {
                activityLog.shopId = entityId;
            } else {
                activityLog.companyId = entityId;
            }
            ActivityLogs.insert(activityLog);

            if (callback) {
                callback(e);
            }
            console.error(e);
        }
    });
    console.timeEnd('CreateInvoicesJob - create invoices');

    if (callback) {
        callback(null, count);
    } else {
        return count;
    }
};

const createInvoice = function (entity, receiptIds, currencyId, shopId, companyId) {
    console.log('createInvoice', entity.name, receiptIds, currencyId, shopId, companyId);
    // run through all the receipts and make sure they exist and are in the readyForInvoicing status
    let status = entity.checkInvoiceBeforeSending ? 'toCheck' : 'new';
    let notes = entity.checkInvoiceBeforeSending ? "Check: generic check setup on Billing tab\n" : '';
    let amount = 0;
    let totalVat = 0;
    let serviceFee = 0;
    let refund = 0;
    let affiliateServiceFee = 0;
    let invoiceReceiptIds = [];
    let invoiceServiceFee = 0;
    let deferredInvoiceServiceFee = 0;
    let invoiceServiceFeeReceiptIds = [];
    _.each(receiptIds, (receiptId) => {
        let receipt = Receipts.findOne({_id: receiptId});
        if (!receipt) {
            throw new Meteor.Error(500, 'Could not find receipt ' + receipt.receiptNr + ' for invoicing');
        }
        if (receipt.invoiceId) {
            throw new Meteor.Error(500, 'Receipt ' + receipt.receiptNr + ' has already been invoiced');
        }
        if (receipt.status !== 'readyForInvoicing') {
            throw new Meteor.Error(500, 'Receipt ' + receipt.receiptNr + ' is not ready for invoicing');
        }
        if (receipt.currencyId !== currencyId) {
            throw new Meteor.Error(500, 'Receipt ' + receipt.receiptNr + ' does not match the currency selected');
        }
        if (shopId) {
            if (receipt.shopId !== shopId) {
                throw new Meteor.Error(500, 'Receipt ' + receipt.receiptNr + ' does not match the shop selected');
            }
        }

        if (receipt.amount <= 0) {
            status = 'toCheck';
            notes+= `Check: receipt ${receipt.receiptNr} amount is zero\n`;
        }
        if (receipt.totalVat <= 0) {
            status = 'toCheck';
            notes+= `Check: receipt ${receipt.receiptNr} vat is zero\n`;
        }
        if (receipt.serviceFee <= 0 && receipt.deferredServiceFee <= 0) {
            status = 'toCheck';
            notes+= `Check: receipt ${receipt.receiptNr} serviceFee is zero\n`;
        }
        if (receipt.refund <= 0) {
            status = 'toCheck';
            notes+= `Check: receipt ${receipt.receiptNr} refund is zero\n`;
        }

        amount += receipt.amount;
        totalVat += receipt.totalVat;
        serviceFee += receipt.serviceFee;
        refund += receipt.refund;

        if (receipt.deferredServiceFee) {
            invoiceServiceFee += receipt.deferredServiceFee;
            invoiceServiceFeeReceiptIds.push(receipt._id);
        }

        if (receipt.affiliateServiceFee) {
            affiliateServiceFee += receipt.affiliateServiceFee;
        }

        invoiceReceiptIds.push(receiptId);
    });

    if (totalVat <= 0) {
        status = 'toCheck';
        notes+= "Check: invoice amount is zero\n";
    }

    let invoiceId = Invoices.insert({
        shopId: shopId,
        companyId: companyId,
        countryId: entity.countryId,
        partnershipStatus: entity.partnershipStatus || "new",
        invoiceDate: new Date(),
        currencyId: currencyId,
        amount: totalVat,
        totalVat: 0,
        serviceFee: serviceFee,
        refund: refund,
        deferredServiceFee: deferredInvoiceServiceFee,
        affiliateServiceFee: affiliateServiceFee,
        receiptIds: invoiceReceiptIds,
        paymentTerms: entity.paymentTerms || 14,
        language: entity.language || "nl",
        sendBy: entity.sendBy || 'email',
        status: status,
        notes: notes
    });

    if (entity.debtCollectionAccount && entity.debtCollectionMandate) {
        // create debt collection item
        DebtCollectionLines.insert({
            invoiceId: invoiceId,
            debtCollectionAccount: entity.debtCollectionAccount,
            debtCollectionMandate: entity.debtCollectionMandate,
            debtCollectionDate: entity.debtCollectionDate,
            currencyId: currencyId,
            amount: totalVat,
            shopName: entity.name,
            billingName: entity.billingName || entity.name,
            recipientName: entity.billingRecipientName
        });
    }

    if (invoiceServiceFee > 0) {
        // we also need to create an invoice for the service fee to the shop
        let vatRate = 0;
        if (Meteor.settings.chargeVatToCountry && Meteor.settings.chargeVatToCountry[entity.countryId]) {
            vatRate = Meteor.settings.chargeVatToCountry[entity.countryId];
        }
        let serviceFeeVat = Math.round((vatRate * invoiceServiceFee) / (100));
        let serviceFeeInvoiceId = Invoices.insert({
            shopId: shopId,
            companyId: companyId,
            countryId: entity.countryId,
            partnershipStatus: entity.partnershipStatus || "new",
            invoiceDate: new Date(),
            currencyId: currencyId,
            amount: invoiceServiceFee + serviceFeeVat,
            totalVat: serviceFeeVat,
            serviceFee: invoiceServiceFee,
            refund: 0,
            receiptIds: invoiceServiceFeeReceiptIds,
            referenceInvoiceId: invoiceId,
            paymentTerms: entity.paymentTerms || 14,
            type: 'serviceFee',
            language: entity.language || "nl",
            status: 'new'
        });

        if (entity.debtCollectionAccount && entity.debtCollectionMandate) {
            // create debt collection item
            DebtCollectionLines.insert({
                invoiceId: serviceFeeInvoiceId,
                debtCollectionAccount: entity.debtCollectionAccount,
                debtCollectionMandate: entity.debtCollectionMandate,
                debtCollectionDate: entity.debtCollectionDate,
                currencyId: currencyId,
                amount: invoiceServiceFee + serviceFeeVat,
                shopName: entity.name,
                billingName: entity.billingName || entity.billingName,
                recipientName: entity.billingRecipientName
            });
        }
    }

    return invoiceId;
};

var createInvoiceForShop = function (shopId, receiptIds, currencyId) {
    let shop = Shops.findOne({_id: shopId});
    if (!shop) {
        throw new Meteor.Error(404, "Could not find shop to create invoice for");
    }

    return createInvoice(shop, receiptIds, currencyId, shopId, null);
};

var createInvoiceForCompany = function (companyId, receiptIds, currencyId) {
    let company = Companies.findOne({_id: companyId});
    if (!company) {
        throw new Meteor.Error(404, "Could not find company to create invoice for");
    }

    return createInvoice(company, receiptIds, currencyId, null, companyId);
};

let createPDFInvoice = function (invoice, reminder) {
    if (invoiceJobs) {
        let job = new Job(invoiceJobs, 'invoicePdf',
            {
                invoiceId: invoice._id,
                invoiceNr: invoice.invoiceNr,
                reminder: reminder
            }
        );
        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 5,
                wait: 5 * 60 * 1000
            })  // 5 minutes between attempts
            .save();               // Commit it to the server
    } else {
        Vatfree.invoicePdf(invoice._id, reminder, (err, fileId) => {
            if (err) {
                console.error(err);
            }
        });
    }
};

let createInvoiceImagesPDF = function (invoice) {
    if (invoiceJobs) {
        let job = new Job(invoiceJobs, 'invoicePdfImages',
            {
                invoiceId: invoice._id,
                invoiceNr: invoice.invoiceNr
            }
        );
        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 5,
                wait: 5 * 60 * 1000
            })  // 5 minutes between attempts
            .save();               // Commit it to the server
    } else {
        Vatfree.invoicePdfImages(invoice._id, (err, fileId) => {
            if (err) {
                console.error(err);
            }
        });
    }
};

let getLastPayment = function (invoice) {
    // get last payment for this invoice
    let payment = Payments.findOne({
        invoiceIds: invoice._id
    }, {
        sort: {
            createdAt: -1
        }
    });
    return payment;
};

var getPaymentAccount = function (invoice) {
    let payment = getLastPayment(invoice);
    if (payment) {
        let description = Vatfree.payments.parseMt940Description(payment.description);
        return _.has(description, 'IBAN') ? description['IBAN'] : '???????????';
    }

    return '???????????';
};

Meteor.methods({
    'create-invoice-pdf-images'(invoiceId) {
        check(invoiceId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice) return false;
        createInvoiceImagesPDF(invoice);
    },
    'get-invoice'(invoiceId) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Invoices.findOne({_id: invoiceId});
    },
    'search-invoices'(searchTerm, limit, offset) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let user = Meteor.users.findOne({_id: this.userId}) || {};
        let userLanguage = user.profile.language || "en";

        let selector = {};
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let invoices = [];
        Invoices.find(selector, {
            sort: {
                name: 1
            },
            limit: limit,
            offset: offset
        }).forEach((invoice) => {
            let entity = {};
            if (invoice.shopId) {
                entity = Shops.findOne({_id: invoice.shopId});
            } else {
                entity = Companies.findOne({_id: invoice.companyId});
            }
            let currency = Currencies.findOne({_id: invoice.currencyId}) || {};
            invoices.push({
                text: invoice.invoiceNr + ' - ' + entity.name + (invoice.deleted ? ' - DELETED!' : '') + ' - ' + moment(invoice.invoiceDate).format(TAPi18n.__('_date_format', {}, userLanguage)) + ' - ' + Vatfree.numbers.formatCurrency(invoice.amount, 2, currency.symbol) + ' (status: ' + invoice.status + ')',
                amount: invoice.amount,
                id: invoice._id
            });
        });

        return invoices;
    },
    'search-debt-collection'(searchTerm, limit, offset) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {};
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        let debtCollections = [];
        DebtCollection.find(selector, {
            sort: {
                name: 1
            },
            limit: limit,
            offset: offset
        }).forEach((debtCollection) => {
            let currency = Currencies.findOne({_id: debtCollection.currencyId}) || {};
            debtCollections.push({
                text: debtCollection.debtCollectionNumber + ' - ' + Vatfree.numbers.formatCurrency(debtCollection.amount, 2, currency.symbol),
                amount: debtCollection.amount,
                id: debtCollection._id
            });
        });

        return debtCollections;
    },
    'create-invoice': function(shopId, companyId, receiptIds, currencyId) {
        check(shopId, String);
        check(receiptIds, Array);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (receiptIds.length === 0) {
            throw new Meteor.Error(500, 'No receipts selected for invoice');
        }

        if (shopId) {
            return createInvoiceForShop(shopId, receiptIds, currencyId);
        } else {
            return createInvoiceForCompany(companyId, receiptIds, currencyId);
        }
    },
    'create-invoice-pdf'(invoiceId, reminder) {
        check(invoiceId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice) return false;
        createPDFInvoice(invoice, reminder);

        let entity = false;
        if (invoice.companyId) {
            entity = Companies.findOne({_id: invoice.companyId});
        } else {
            entity = Shops.findOne({_id: invoice.shopId});
        }
        if (entity && (entity.includeImagesPDF || entity.partnershipStatus === 'new')) {
            createInvoiceImagesPDF(invoice);
        }
    },
    'invoice-delete'(invoiceId) {
        check(invoiceId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice || (invoice.status !== 'new' && invoice.status !== 'toCheck' && invoice.status !== 'readyToSend' && invoice.status !== 'sentToRetailer' && invoice.status !== 'promisedToPay')) {
            throw new Meteor.Error(500, 'Not a valid invoice given for deletion');
        }

        Invoices.update({
            _id: invoiceId
        },{
            $set: {
                deleted: true
            }
        });
    },
    'get-receipts-for-invoicing'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receipts = [];
        let shops = {};
        let billingCompany = {};
        Receipts.find({
            status: 'readyForInvoicing'
        },{
            sort: {
                receiptNr: 1
            },
            fields: {
                _id: 1,
                shopId: 1,
                currencyId: 1
            },
            limit: 10000
        }).forEach((receipt) => {
            if (!_.has(shops, receipt.shopId)) {
                shops[receipt.shopId] = Shops.findOne({_id: receipt.shopId});
            }
            let shop = shops[receipt.shopId];
            if (shop.billingEntity) {
                receipt.billingCompanyName = shop.name;
                receipts.push(receipt);
            } else {
                if (shop.companyId) {
                    if (!_.has(billingCompany, shop.companyId)) {
                        billingCompany[shop.companyId] = Vatfree.billing.getBillingEntityForCompany(shop.companyId);
                    }

                    if (billingCompany[shop.companyId]) {
                        receipt.companyId = shop.companyId;
                        receipt.billingCompanyId = billingCompany[shop.companyId]._id;
                        receipt.billingCompanyName = billingCompany[shop.companyId].name;
                        receipts.push(receipt);
                    }
                }
            }
        });

        return receipts;
    },
    'create-invoices'(receiptIds) {
        check(receiptIds, Array);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let job = new Job(invoiceJobs, 'createInvoices', {receiptIds: receiptIds});
        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 1
            })
            .save();

        return true;
    },
    'invoice-already-sent'(invoiceId, updateDueDate) {
        check(invoiceId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice) {
            throw new Meteor.Error(500, 'Invoice is not in status sent to retailer');
        }

        return Vatfree.invoices.markInvoiceSent(invoice, updateDueDate);
    },
    'invoice-send'(invoiceId, updateDueDate) {
        check(invoiceId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Vatfree.sendInvoice(invoiceId, updateDueDate);
    },
    'invoice-resend'(invoiceId, updateDueDate) {
        check(invoiceId, String);
        check(updateDueDate, Boolean);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice || invoice.status !== 'sentToRetailer' || invoice.status !== 'promisedToPay') {
            throw new Meteor.Error(500, 'Invoice is not in status sent to retailer');
        }

        if (Vatfree.invoices.send(invoiceId)) {
            if (updateDueDate) {
                Invoices.update({
                    _id: invoiceId
                },{
                    $set: {
                        dueDate: moment().add(invoice.paymentTerms, 'days').toDate()
                    }
                });
            }
            return true;
        } else {
            throw new Meteor.Error(500, 'Sending of email failed');
        }
    },
    'invoice-send-reminder'(invoiceId) {
        check(invoiceId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.defer(() => {
            Vatfree.sendInvoiceReminder(invoiceId)
        });

        return true;
    },
    'create-debt-collection-batch'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let currencyId;
        let amount = 0;
        let invoiceIds = [];
        let debtCollectionLineIds = [];
        DebtCollectionLines.find({
            debtCollectionId: {
                $exists: false
            }
        }).forEach((debtCollectionLine) => {
            // TEMP - we only support EURO for debt collections so far
            currencyId = debtCollectionLine.currencyId;
            amount += Number(debtCollectionLine.amount);
            invoiceIds.push(debtCollectionLine.invoiceId);
            debtCollectionLineIds.push(debtCollectionLine._id)
        });

        return DebtCollection.insert({
            currencyId: currencyId || Vatfree.defaults.currencyId,
            amount: amount,
            invoiceIds: invoiceIds,
            debtCollectionLineIds: debtCollectionLineIds
        });
    },
    'create-payback-batch'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let amount = 0;
        let invoiceIds = [];
        let paybackLineIds = [];
        PaybackLines.find({
            paybackId: {
                $exists: false
            }
        }).forEach((paybackLine) => {
            amount += paybackLine.amount;
            invoiceIds.push(paybackLine.invoiceId);
            paybackLineIds.push(paybackLine._id)
        });

        return Payback.insert({
            amount: amount,
            invoiceIds: invoiceIds,
            paybackLineIds: paybackLineIds
        });
    },
    'invoice-create-credit-invoice'(invoiceId) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        const originalInvoice = Invoices.findOne({_id: invoiceId});

        let entity = false;
        if (originalInvoice.companyId) {
            entity = Companies.findOne({_id: originalInvoice.companyId});
        } else {
            entity = Shops.findOne({_id: originalInvoice.shopId});
        }

        if (!entity) {
            throw new Meteor.Error(500, "Could not find entity to pay back");
        }

        const invoice = {};
        const copyFields = ['shopId', 'companyId', 'receiptIds', 'countryId', 'partnershipStatus', 'currencyId', 'paymentTerms', 'language', 'secret', 'sendBy'];
        _.each(copyFields, (copyField) => {
            invoice[copyField] = originalInvoice[copyField];
        });
        invoice.invoiceDate = new Date();
        invoice.referenceInvoiceId = invoiceId;
        invoice.type = 'credit';
        invoice.status = 'new';
        invoice.dueDate = moment(invoice.invoiceDate).add(invoice.paymentTerms).toDate();

        invoice.amount = -1 * originalInvoice.amount;
        invoice.totalVat = -1 * originalInvoice.totalVat;
        invoice.serviceFee = -1 * originalInvoice.serviceFee;
        invoice.refund = -1 * originalInvoice.refund;

        if (originalInvoice.status !== 'paid') {
            // set invoice to credited as it will not be paid
            invoice.status = 'credited';
        }

        const newInvoiceId = Invoices.insert(invoice);

        let createPaybackLine = function (amount) {
            let paybackAccount = getPaymentAccount(originalInvoice);
            if (paybackAccount.indexOf('?') >= 0) {
                paybackAccount = entity.debtCollectionAccount || '???????????';
            }
            PaybackLines.insert({
                invoiceId: newInvoiceId,
                paybackAccount: paybackAccount,
                amount: amount,
                shopName: entity.name,
                billingName: entity.billingName || entity.name,
                recipientName: entity.billingRecipientName
            });
        };
        if (originalInvoice.status === 'paid') {
            createPaybackLine(originalInvoice.amount);
        } else if (_.contains(['partiallyPaid', 'paymentDifference'], originalInvoice.status)) {
            let amountPaid = 0;
            Payments.find({
                invoiceIds: invoiceId
            }).forEach((payment) => {
                let invoiceAmount = _.find(payment.invoiceAmounts, (amount) => {
                    if (amount.invoiceId === invoiceId) {
                        return amount;
                    }
                });

                if (invoiceAmount) {
                    amountPaid += invoiceAmount.amount;
                }
            });
            if (amountPaid > 0) {
                createPaybackLine(amountPaid);
            }
        }

        Invoices.update({
            _id: invoiceId
        }, {
            $set: {
                status: 'credited',
                referenceInvoiceId: newInvoiceId
            }
        });

        return newInvoiceId;
    },
    'invoice-mark-paid-payback'(invoiceId) {
        check(invoiceId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice) {
            throw new Meteor.Error(500, "Could not find invoice");
        }

        let difference = getPaymentDifference(invoice);
        if (difference <= 0) {
            throw new Meteor.Error(500, "Cannot pay back a negative amount");
        }

        let entity = false;
        if (invoice.companyId) {
            entity = Companies.findOne({_id: invoice.companyId});
        } else {
            entity = Shops.findOne({_id: invoice.shopId});
        }

        if (!entity) {
            throw new Meteor.Error(500, "Could not find entity to pay back");
        }

        let paybackAccount = getPaymentAccount(invoice);
        if (paybackAccount.indexOf('?') >= 0) {
            paybackAccount = entity.debtCollectionAccount || '???????????';
        }
        PaybackLines.insert({
            invoiceId: invoiceId,
            paybackAccount: paybackAccount,
            amount: difference,
            shopName: entity.name,
            billingName: entity.billingName || entity.name,
            recipientName: entity.billingRecipientName
        });
    },
    'invoice-mark-paid-difference'(invoiceId) {
        check(invoiceId, String);
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice) {
            throw new Meteor.Error(500, "Could not find invoice");
        }

        let difference = getPaymentDifference(invoice);

        let payment = getLastPayment(invoice);
        if (!payment) {
            throw new Meteor.Error(500, "Could not find payment for this invoice");
        }

        let invoiceAmount = _.find(payment.invoiceAmounts, (amount) => {
            if (amount.invoiceId === invoiceId) {
                return amount;
            }
        });
        if (!invoiceAmount) {
            throw new Meteor.Error(500, "Could not find invoice amount in payment for this invoice");
        }

        Payments.update({
            _id: payment._id,
            'invoiceAmounts.invoiceId': invoiceId
        },{
            $set: {
                'invoiceAmounts.$.amount': invoiceAmount.amount - difference,
                paymentDifference: Number(payment.paymentDifference || 0) + difference
            }
        });
        Invoices.update({
            _id: invoiceId
        },{
            $set: {
                status: 'paid',
                paymentDifference: difference
            }
        });
    },
    'invoice-create-debt-collection-line'(invoiceId) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice || invoice.status !== 'sentToRetailer' || invoice.status !== 'promisedToPay') {
            throw new Meteor.Error(500, 'Invoice is not in status sent to retailer');
        }

        let entity = false;
        if (invoice.companyId) {
            entity = Companies.findOne({_id: invoice.companyId});
        } else {
            entity = Shops.findOne({_id: invoice.shopId});
        }

        if (entity.debtCollectionAccount && entity.debtCollectionMandate) {
            // create debt collection item
            return DebtCollectionLines.insert({
                invoiceId: invoiceId,
                debtCollectionAccount: entity.debtCollectionAccount,
                debtCollectionMandate: entity.debtCollectionMandate,
                debtCollectionDate: entity.debtCollectionDate,
                amount: invoice.amount,
                currencyId: invoice.currencyId,
                shopName: entity.name,
                billingName: entity.billingName || entity.billingName,
                recipientName: entity.billingRecipientName
            });
        } else {
            throw new Meteor.Error(500, 'Entity does not have debt collection defined');
        }

    },
    'count-invoices-to-send'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Invoices.find({
            status: 'readyToSend',
            deleted: {
                $ne: true
            }
        }).count();
    },
    'send-all-invoices'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let job = new Job(invoiceJobs, 'sendAllOpenInvoices', {});

        // Set some properties of the job and then submit it
        job.priority('normal')
            .retry({
                retries: 1
            })
            .save();
    },
    'send-invoice-reminders'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.defer(() => {
            Vatfree.sendAllInvoiceReminders();
        });

        return true;
    },
    'get-invoice-stats'(selector) {
        if (!Vatfree.userIsInRole(this.userId, 'invoices-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const pipeline = [
            {
                $match: selector,
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 },
                    amount: { $sum: { $ifNull: ['$euroAmounts.amount', 0] } },
                    exVat: { $sum: { $subtract: ["$euroAmounts.amount", "$euroAmounts.totalVat"] } },
                    vat: { $sum: { $ifNull: ['$euroAmounts.totalVat', 0] } },
                    fee: { $sum: { $ifNull: ['$euroAmounts.serviceFee', 0] } },
                    refund: { $sum: { $ifNull: ['$euroAmounts.refund', 0] } }
                }
            },
        ];
        return Invoices.rawCollection().aggregate(pipeline).toArray();
    },
    'remove-receipt-from-invoice'(invoiceId, receiptId) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        /*
        console...
        var invoiceId = "zCkgPqWxg4ey5zopm"; var receiptId = "pM4DZQQjDJdTzm6vc"; var receipt = Receipts.findOne({_id: receiptId}); var invoice = Invoices.findOne({_id: invoiceId}); Invoices.update({_id: invoiceId},{$set: {amount: invoice.amount - receipt.totalVat, serviceFee: invoice.serviceFee - receipt.serviceFee, refund: invoice.refund - receipt.refund}, $pull: {receiptIds: receiptId}}); Receipts.update({_id: receiptId},{$set: {status: "readyForInvoicing"}, $unset: {invoiceId: true}});
        */
        const receipt = Receipts.findOne({_id: receiptId});
        const invoice = Invoices.findOne({_id: invoiceId});

        Invoices.update({
            _id: invoiceId
        }, {
            $set: {
                amount: invoice.amount - receipt.totalVat,
                serviceFee: invoice.serviceFee - receipt.serviceFee,
                refund: invoice.refund - receipt.refund
            },
            $pull: {
                receiptIds: receiptId
            }
        });
        Receipts.update({
            _id: receiptId
        },{
            $set: {
                status: 'readyForInvoicing'
            },
            $unset: {
                invoiceId: true
            }
        });
    },
    'invoices-export'(searchTerm, status, listFilters, salesforceFilter) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        var Baby = require('babyparse');
        let selector = {};
        if (searchTerm) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        if (status && status.length > 0) {
            selector['status'] = {
                $in: status
            };
        }

        _.each(listFilters, (listFilterValue, listFilterId) => {
            selector[listFilterId] = listFilterValue;
        });

        if (salesforceFilter) {
            if (salesforceFilter === 'active') {
                selector.salesforceId = {
                    $exists: true
                }
            }
            if (salesforceFilter === 'inactive') {
                selector.salesforceId = {
                    $exists: false
                }
            }
        }

        // cache objects
        let users = {};
        let shops = {};
        let companies = {};
        let countries = {};
        let currencies = {};

        let data = [];
        Invoices.find(selector, {
            sort: {
                createdAt: 1
            },
            fields: {
                textSearch: false
            }
        }).forEach((invoice) => {
            // fix amounts
            let invoiceCsv = {
                _id: invoice._id,
                invoiceNr: invoice.invoiceNr
            };

            if (invoice.shopId) {
                invoiceCsv.entity = getCollectionDescription(Shops, invoice.shopId, shops);
            } else if (invoice.companyId) {
                invoiceCsv.entity = getCollectionDescription(Companies, invoice.companyId, companies);
            }
            invoiceCsv.partnershipStatus = invoice.partnershipStatus;

            invoiceCsv.date = moment(invoice.invoiceDate).format("YYYY-MM-DD");
            invoiceCsv.currencyId = getCollectionDescription(Currencies, invoice.currencyId, currencies);
            invoiceCsv.amount = Vatfree.numbers.formatAmount(invoice.amount);
            invoiceCsv.totalVat = Vatfree.numbers.formatAmount(invoice.totalVat);
            invoiceCsv.serviceFee = Vatfree.numbers.formatAmount(invoice.serviceFee);
            invoiceCsv.refund = Vatfree.numbers.formatAmount(invoice.refund);
            invoiceCsv.deferredServiceFee = invoice.deferredServiceFee ? Vatfree.numbers.formatAmount(invoice.deferredServiceFee) : 0;
            invoiceCsv.affiliateServiceFee = invoice.affiliateServiceFee ? Vatfree.numbers.formatAmount(invoice.affiliateServiceFee) : 0;

            invoiceCsv.paymentTerms = invoice.paymentTerms;
            invoiceCsv.status = invoice.status;

            invoiceCsv.debtCollection = invoice.debtCollectionId ? 'yes': '';

            invoiceCsv.invoiceSent = moment(invoice.invoiceSent).format("YYYY-MM-DD");
            invoiceCsv.dueDate = moment(invoice.dueDate).format("YYYY-MM-DD");
            invoiceCsv.reminderSent = invoice.reminderSent ? moment(invoice.reminderSent).format("YYYY-MM-DD") : '';
            invoiceCsv.paidAt = invoice.paidAt ? moment(invoice.paidAt).format("YYYY-MM-DD") : '';

            invoiceCsv.createdAt = moment(invoice.createdAt).format("YYYY-MM-DD");
            invoiceCsv.createdBy = getCollectionDescription(Meteor.users, invoice.createdBy, users);
            invoiceCsv.updatedAt = invoice.updatedAt ? moment(invoice.updatedAt).format("YYYY-MM-DD") : '';
            invoiceCsv.updatedBy = invoice.updatedBy ? getCollectionDescription(Meteor.users, invoice.createdBy, users) : '';
            invoiceCsv.deleted = _.has(invoice, 'deleted') ? invoice.deleted : '';

            invoiceCsv.countryId = getCollectionDescription(Countries, invoice.countryId, countries);

            data.push(invoiceCsv);
        });
        let csv = Baby.unparse(data);

        return csv;
    },
    'get-debt-collection-xml-template'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Assets.getText('templates/debt-collection.xml');
    },
    'get-xml-template'(templateName) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return Assets.getText('templates/' + templateName + '.xml');
    },
    'get-exact-auth-url'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        const exactClient = getExactClient();
        return exactClient.authUrl(Meteor.settings.exact.redirectUri);
    },
    'register-exact-api-link-code'(code) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        const _getToken = function(code, redirectUri, callback) {
            const exactClient = getExactClient();
            exactClient.token(code, 'authorization_code', redirectUri, callback);
        };
        const getToken = Meteor.wrapAsync(_getToken);

        const tokenData = getToken(code, Meteor.settings.exact.redirectUri) || {};
        Synchs.upsert({
            _id: 'nl.exactonline.start'
        }, {
            $set: tokenData
        });

        if (tokenData.error) {
            throw new Meteor.Error(500, tokenData.error);
        }
    }
});

Vatfree.sendAllOpenInvoices = function (callback) {
    let counts = {
        error: 0,
        success: 0
    };
    Invoices.find({
        status: 'readyToSend',
        deleted: {
            $ne: true
        }
    }).forEach((invoice) => {
        try {
            Vatfree.sendInvoice(invoice._id);
            counts.success++;
        } catch(e) {
            console.log(e);
            counts.error++;
        }
    });

    callback(null, counts);
};

Vatfree.invoices.markInvoiceSent = function (invoice, updateDueDate) {
    let setData = {
        status: 'sentToRetailer',
        invoiceSent: new Date()
    };
    if (updateDueDate) {
        setData['dueDate'] = moment().add(invoice.paymentTerms || 14, 'days').toDate();
    }

    Invoices.update({
        _id: invoice._id
    }, {
        $set: setData
    });
};

Vatfree.sendInvoice = function (invoiceId, updateDueDate) {
    let invoice = Invoices.findOne({_id: invoiceId});
    if (!invoice || invoice.status !== 'readyToSend') {
        throw new Meteor.Error(500, 'Invoice is not in status ready to send');
    }

    if (Vatfree.invoices.send(invoiceId)) {
        Vatfree.invoices.markInvoiceSent(invoice, updateDueDate);
    } else {
        throw new Meteor.Error(500, 'Sending of email failed');
    }
};

import moment from 'moment/moment';

export const sendFirstInvoiceReminders = function () {
    let numberOf = 0;
    Invoices.find({
        status: 'sentToRetailer',
        partnershipStatus: {
            $nin: ['uncooperative']
        },
        debtCollectionId: {
            $exists: false
        },
        reminderSent: {
            $exists: false
        },
        dueDate: {
            $lt: moment().subtract(Meteor.settings.defaults.reminderInvoiceDays || 7, 'days').toDate()
        },
        deleted: {
            $ne: true
        }
    }, {
        fields: {
            _id: 1
        }
    }).forEach((invoice) => {
        if (invoiceJobs) {
            let job = new Job(invoiceJobs, 'sendInvoiceReminder', {
                invoiceId: invoice._id
            });
            // Set some properties of the job and then submit it
            job.priority('normal')
                .retry({
                    retries: 1
                })
                .save();
            numberOf++;
        }
    });

    return numberOf;
};

export const sendSecondInvoiceReminders = function () {
    let numberOf = 0;
    Invoices.find({
        status: 'sentToRetailer',
        partnershipStatus: {
            $nin: ['uncooperative']
        },
        debtCollectionId: {
            $exists: false
        },
        reminder2Sent: {
            $exists: false
        },
        reminderSent: {
            $lt: moment().subtract(Meteor.settings.defaults.reminder2InvoiceDays || 7, 'days').toDate()
        },
        deleted: {
            $ne: true
        }
    }, {
        fields: {
            _id: 1
        }
    }).forEach((invoice) => {
        if (invoiceJobs) {
            let job = new Job(invoiceJobs, 'sendInvoiceReminder', {
                invoiceId: invoice._id,
                reminder2: true
            });
            // Set some properties of the job and then submit it
            job.priority('normal')
                .retry({
                    retries: 1
                })
                .save();
            numberOf++;
        }
    });

    return numberOf;
};

export const createInvoiceReminderTasks = function () {
    let numberOf = 0;
    Invoices.find({
        status: 'sentToRetailer',
        partnershipStatus: {
            $nin: ['new', 'uncooperative']
        },
        debtCollectionId: {
            $exists: false
        },
        reminderSent: {
            $exists: true
        },
        remindersStop: {
            $exists: false
        },
        reminder2Sent: {
            $exists: true,
            $lt: moment().subtract(Meteor.settings.defaults.reminder2InvoiceDays || 7, 'days').toDate()
        },
        deleted: {
            $ne: true
        }
    }, {
        fields: {
            _id: 1,
            shopId: 1,
            companyId: 1
        }
    }).forEach((invoice) => {
        // stop sending reminders and create a task to call the entity
        ActivityLogs.insert({
            type: 'task',
            subject: 'Call shop about invoice - 2 reminders sent',
            status: 'new',
            invoiceId: invoice._id,
            shopId: invoice.shopId,
            companyId: invoice.companyId
        });
        Invoices.update({
            _id: invoice._id
        },{
            $set: {
                remindersStop: new Date()
            }
        });
        numberOf++;
    });

    return numberOf;
};

export const closeNewShopInvoices = function() {
    // select all invoices new with 2 reminders and +7 days
    let numberOf = 0;
    Invoices.find({
        status: 'sentToRetailer',
        partnershipStatus: 'new',
        reminder2Sent: {
            $exists: true,
            $lt: moment().subtract(Meteor.settings.defaults.closeNewInvoiceDays || 7, 'days').toDate()
        },
        deleted: {
            $ne: true
        }
    }, {
        fields: {
            _id: 1,
            shopId: 1,
            companyId: 1
        }
    }).forEach((invoice) => {
        console.log('closeNewShopInvoices', invoice);
        if (invoice.shopId) {
            Shops.update({
                _id: invoice.shopId
            }, {
                $set: {
                    partnershipStatus: 'uncooperative'
                }
            });
        } else if (invoice.companyId) {
            Companies.update({
                _id: invoice.companyId
            }, {
                $set: {
                    partnershipStatus: 'uncooperative'
                }
            });
        }

        Invoices.update({
            _id: invoice._id
        },{
            $set: {
                status: 'notCollectable',
                receiptRejectionIds: ['48WGhCyGxwz3j4dY5']
            }
        });
    });
};

export const closeUncooperativeShopInvoices = function() {
    // select all invoices uncooperative +14 days
    let numberOf = 0;
    Invoices.find({
        status: 'sentToRetailer',
        partnershipStatus: 'uncooperative',
        dueDate: {
            $lt: moment().subtract(Meteor.settings.defaults.closeUncoooperativeInvoiceDays || 14, 'days').toDate()
        },
        deleted: {
            $ne: true
        }
    }, {
        fields: {
            _id: 1,
            shopId: 1,
            companyId: 1
        }
    }).forEach((invoice) => {
        // set op not-collectable indien eerder invoice met not-collectable reden
        let selector = {
            _id: {
                $ne: invoice._id
            },
            status: 'notCollectable'
        };
        if (invoice.shopId) {
            selector.shopId = invoice.shopId;
        } else {
            selector.companyId = invoice.companyId;
        }
        let previousInvoice = Invoices.findOne(selector, {sort: {createdAt: -1}});
        if (previousInvoice) {
            Invoices.update({
                _id: invoice._id
            },{
                $set: {
                    status: 'notCollectable',
                    receiptRejectionIds: previousInvoice.receiptRejectionIds || []
                }
            });
        } else {
            // check op geen contactgevens -> reject met die reden
            let entity = false;
            if (invoice.companyId) {
                entity = Companies.findOne({_id: invoice.companyId});
            } else {
                entity = Shops.findOne({_id: invoice.shopId});
            }
            let contactInfo = Vatfree.getInvoiceRecipients(entity, false);
            if (!contactInfo) {
                Invoices.update({
                    _id: invoice._id
                },{
                    $set: {
                        status: 'notCollectable',
                        receiptRejectionIds: ['NHwEhHR3K5B4FRqr6']
                    }
                });
            } else {
                // anders maak taak aan, als deze nog niet bestaat
                let activitySelector = {
                    type: 'task',
                    subject: 'Not-collectable invoice - please do something',
                    status: 'new',
                    invoiceId: invoice._id,
                    shopId: invoice.shopId,
                    companyId: invoice.companyId
                };
                if (!ActivityLogs.findOne(activitySelector)) {
                    ActivityLogs.insert(activitySelector);
                }
            }
        }
        numberOf++;
    });

    return numberOf;
};

Vatfree.sendAllInvoiceReminders = function (callback) {
    let numberOf = 0;
    try {
        numberOf += sendFirstInvoiceReminders();
        numberOf += sendSecondInvoiceReminders();
        numberOf += createInvoiceReminderTasks();
        // We don't count this is reminders sent
        closeNewShopInvoices();
        closeUncooperativeShopInvoices();
    } catch(e) {
        if (callback) {
            callback(e);
        }
        return;
    }

    if (callback) {
        callback(null, numberOf);
    }
    return numberOf;
};

Vatfree.sendInvoiceReminder = function (invoiceId, reminder2 = false, callback) {
    try {
        let invoice = Invoices.findOne({_id: invoiceId});
        if (!invoice || invoice.status !== 'sentToRetailer') {
            throw new Meteor.Error(500, 'Invoice is not in status sent to retailer');
        }

        let entity = false;
        if (invoice.companyId) {
            entity = Companies.findOne({_id: invoice.companyId});
        } else {
            entity = Shops.findOne({_id: invoice.shopId});
        }

        let billingReminderEmailTextId = entity.billingReminderEmailTextId || Meteor.settings.defaults.billingReminderEmailTextId;
        if (reminder2) {
            if (entity.billingReminder2EmailTextId || Meteor.settings.defaults.billingReminder2EmailTextId) {
                billingReminderEmailTextId = entity.billingReminder2EmailTextId || Meteor.settings.defaults.billingReminder2EmailTextId;
            }
        }
        if (billingReminderEmailTextId) {
            if (Vatfree.invoices.send(invoiceId, billingReminderEmailTextId, true)) {
                Invoices.update({
                    _id: invoiceId
                }, {
                    $set: {
                        [reminder2 ? "reminder2Sent" : "reminderSent"]: new Date()
                    }
                });
            } else {
                if (callback) {
                    callback('Sending of email failed');
                } else {
                    throw new Meteor.Error(500, 'Sending of email failed');
                }
            }
        } else {
            if (callback) {
                callback('No billing text defined for entity');
            } else {
                throw new Meteor.Error(500, 'No billing text defined for entity');
            }
        }

        if (callback) callback(null, 1);
        return true;
    } catch (e) {
        if (callback) {
            callback(JSON.stringify(e, Object.getOwnPropertyNames(e)));
        } else {
            throw e;
        }
    }
};

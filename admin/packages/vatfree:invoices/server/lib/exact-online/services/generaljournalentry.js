export const GeneralJournalEntry = exports.GeneralJournalEntry = function (Client) {
  this.client = Client;

  return this;
};


// ---- GENERAL JOURNAL ENTRY BEGIN ----

/**
 * Get back account
 * @param  {String}   generalJournalEntryId Entry ID
 * @param  {Integer}  division  Division ID
 * @param  {Function} callback  Callback
 */
GeneralJournalEntry.prototype.getGeneralJournalEntry = function(generalJournalEntryId, division, callback) {
  var params = {
    $filter: 'EntryID eq guid\''+ generalJournalEntryId +'\''
  };

  if(typeof division === 'function') {
    callback = division;
    division = this.client.division;
  }

  this.client.sendRequest('/v1/' + division + '/generaljournalentry/GeneralJournalEntries', 'GET', params, null, callback);
};

/**
 * Get back account
 * @param  {String}   searchTerm
 * @param  {Integer}  division  Division ID
 * @param  {Function} callback  Callback
 */
GeneralJournalEntry.prototype.searchGeneralJournalEntry = function(searchTerm, division, callback) {
  var params = {
    $filter: searchTerm,
    $select: '*'
  };

  if(typeof division === 'function') {
    callback = division;
    division = this.client.division;
  }

  this.client.sendRequest('/v1/' + division + '/generaljournalentry/GeneralJournalEntries', 'GET', params, null, callback);
};

/**
 * Get back account
 * @param  {String}   searchTerm
 * @param  {Integer}  division  Division ID
 * @param  {Function} callback  Callback
 */
GeneralJournalEntry.prototype.searchGeneralJournalEntryLines = function(searchTerm, division, callback) {
  var params = {
    $filter: searchTerm,
    $select: '*'
  };

  if(typeof division === 'function') {
    callback = division;
    division = this.client.division;
  }

  this.client.sendRequest('/v1/' + division + '/generaljournalentry/GeneralJournalEntryLines', 'GET', params, null, callback);
};

/**
 * Alter a bank account
 * @param  {String}   generalJournalEntryId  JSON Object with content saved
 * @param  {String}   userData        JSON Object with content saved
 * @param  {Integer}  division        Division ID
 * @param  {Function} callback        Callback
 */
GeneralJournalEntry.prototype.saveGeneralJournalEntry = function(generalJournalEntryId, userData, division, callback) {

  if(typeof division === 'function') {
    callback = division;
    division = this.client.division;
  }

  this.client.sendRequest('/v1/' + division + '/generaljournalentry/GeneralJournalEntries(guid\'' + generalJournalEntryId + '\')', 'PUT', null, userData, callback);
};

/**
 * Create Sales Invoice
 * @param {Object} userData   POST fields
 * @param {Integer} division Division ID
 * @param {Function} callback Gets called after request is complete
 */
GeneralJournalEntry.prototype.createGeneralJournalEntry = function(userData, division, callback) {
  if(typeof division === 'function') {
    callback = division;
    division = this.client.division;
  }

  this.client.sendRequest('/v1/' + division + '/generaljournalentry/GeneralJournalEntries', 'POST', null, userData, callback);
};

// ---- GENERAL JOURNAL ENTRY END ----

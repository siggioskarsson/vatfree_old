export const SalesEntry = exports.SalesEntry = function (Client) {
  this.client = Client;

  return this;
};


// ---- SALES ENTRY BEGIN ----

/**
 * Get back account
 * @param  {String}   salesEntryId Account ID
 * @param  {Integer}  division  Division ID
 * @param  {Function} callback  Callback
 */
SalesEntry.prototype.getSalesEntry = function(salesEntryId, division, callback) {
  var params = {
    $filter: 'EntryID eq guid\''+ salesEntryId +'\''
  };

  if(typeof division === 'function') {
    callback = division;
    division = this.client.division;
  }

  this.client.sendRequest('/v1/' + division + '/salesentry/SalesEntries', 'GET', params, null, callback);
};

/**
 * Get back account
 * @param  {String}   searchTerm Account ID
 * @param  {Integer}  division  Division ID
 * @param  {Function} callback  Callback
 */
SalesEntry.prototype.searchSalesEntry = function(searchTerm, division, callback) {
  var params = {
    $filter: searchTerm
  };

  if(typeof division === 'function') {
    callback = division;
    division = this.client.division;
  }

  this.client.sendRequest('/v1/' + division + '/salesentry/SalesEntries', 'GET', params, null, callback);
};

/**
 * Alter a bank account
 * @param  {String}   salesEntryId  JSON Object with content saved
 * @param  {String}   userData        JSON Object with content saved
 * @param  {Integer}  division        Division ID
 * @param  {Function} callback        Callback
 */
SalesEntry.prototype.saveSalesEntry = function(salesEntryId, userData, division, callback) {

  if(typeof division === 'function') {
    callback = division;
    division = this.client.division;
  }

  this.client.sendRequest('/v1/' + division + '/salesentry/SalesEntries(guid\'' + salesEntryId + '\')', 'PUT', null, userData, callback);
};

/**
 * Create Sales Invoice
 * @param {Object} userData   POST fields
 * @param {Integer} division Division ID
 * @param {Function} callback Gets called after request is complete
 */
SalesEntry.prototype.createSalesEntry = function(userData, division, callback) {
  if(typeof division === 'function') {
    callback = division;
    division = this.client.division;
  }

  this.client.sendRequest('/v1/' + division + '/salesentry/SalesEntries', 'POST', null, userData, callback);
};

// ---- SALES INVOICE END ----

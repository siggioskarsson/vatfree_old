/* global Vatfree:false */

Vatfree.invoices.send = function(invoiceId, emailTextId, reminder) {
    check(invoiceId, String);
    let invoice = Invoices.findOne({_id: invoiceId});
    if (!invoice) {
        throw new Meteor.Error(500, 'Could not find invoice');
    }

    let entity = false;
    if (invoice.companyId) {
        entity = Companies.findOne({_id: invoice.companyId});
    } else {
        entity = Shops.findOne({_id: invoice.shopId});
    }

    let attachments = [];
    let maxAttachmentSize = Meteor.settings.defaults.maxAttachmentSize || 1024*1024*10;
    Files.find({
        itemId: invoiceId,
        target: 'invoices'
    }).forEach((file) => {
        if (file.original.size <= maxAttachmentSize && (invoice.subType !== 'images' || entity.includeImagesPDF || entity.partnershipStatus === 'new')) {
            attachments.push({
                filename: file.original.name,
                path: file.getDirectUrl()
            });
        }
    });
    if (!attachments.length) {
        throw new Meteor.Error(500, "Invoice PDF's not found");
        return;
    }

    let emailTemplateId = entity.billingEmailTemplateId || entity.emailTemplateId || Meteor.settings.defaults.billingEmailTemplateId;
    emailTextId = emailTextId || entity.billingEmailTextId || Meteor.settings.defaults.billingEmailTextId;
    return emailInvoice(invoice, entity, attachments, emailTemplateId, emailTextId, null, null, reminder);
};

Vatfree.getInvoiceRecipients = function (entity, reminder) {
    let recipient = entity.billingEmail || '';
    let recipientCC = entity.billingEmailCC || false;

    if (reminder && entity.billingReminderEmail) {
        recipient = entity.billingReminderEmail;
        recipientCC = entity.billingReminderEmailCC || false;
    } else {
        if (_.isArray(entity.billingContacts) && entity.billingContacts.length > 0) {
            Contacts.find({
                _id: {
                    $in: entity.billingContacts
                }
            }).forEach((contact) => {
                if (contact.profile.email) {
                    recipient += (recipient.length ? ',' : '') + contact.profile.email;
                }
            });
        }

    }
    return {recipient, recipientCC};
};

var emailInvoice = function (invoice, entity, attachments, emailTemplateId, emailTextId, emailSubject, emailText, reminder) {
    let { recipient, recipientCC } = Vatfree.getInvoiceRecipients(entity, reminder);

    if (entity.sendBy === 'post') {
        recipient = Meteor.settings.defaults.postEmail || "finance@vatfree.com";
        recipientCC = false;
    }

    if (!recipient) {
        let activityLog = {
            type: 'task',
            status: 'new',
            subject: "Could not send invoice - no email address defined",
            invoiceId: invoice._id
        };
        if (invoice.shopId) {
            activityLog.shopId = invoice.shopId;
        } else {
            activityLog.companyId = invoice.companyId;
        }
        ActivityLogs.insert(activityLog);

        return false;
    }

    let invoiceLanguage = invoice.language || entity.language || "nl";

    let emailTextDoc = EmailTexts.findOne({_id: emailTextId});
    if (!emailText && emailTextDoc) {
        emailText = emailTextDoc.text[invoiceLanguage] || emailTextDoc.text["nl"];
    }
    if (!emailSubject && emailTextDoc) {
        emailSubject = emailTextDoc.subject[invoiceLanguage] || emailTextDoc.subject["nl"];
    }

    if (!emailSubject || !emailText) {
        throw new Meteor.Error(500, "Email subject and/or text could not be found");
    }

    if (entity.billingEmailSubjectPrefix) {
        emailSubject = entity.billingEmailSubjectPrefix + ' ' + emailSubject;
    }

    if (entity.sendBy === 'post') {
        emailSubject = 'POST: ' + emailSubject;
    }

    let templateData = {
        invoice: invoice,
        entity: entity,
        language: invoiceLanguage
    };

    import handlebars from 'handlebars';
    Vatfree.notify.registerHandlebarsHelpers(handlebars);

    let text = handlebars.compile(emailText)(templateData);
    let subject = handlebars.compile(emailSubject)(templateData);

    let mailOptions = {
        from: "Vatfree finance <finance@vatfree.com>",
        to: recipient,
        cc: recipientCC,
        attachments: attachments
    };

    let activityIds = {
        invoiceId: invoice._id,
    };
    if (invoice.companyId) {
        activityIds.companyId = invoice.companyId;
    } else {
        activityIds.shopId = invoice.shopId;
    }

    return !!Vatfree.sendEmail(mailOptions, emailTemplateId, subject, text, activityIds);
};

Vatfree.sendEmail = function(options, emailTemplateId, subject, text, activityIds) {
    if (!options) {
        throw new Error('No options provided');
    }
    if (!options.from) {
        options.from = "Vatfree.com <support@vatfree.com>";
    }

    let template = EmailTemplates.findOne({
        _id: emailTemplateId
    });
    if (!template) {
        throw new Meteor.Error(500, 'Email template of type ' + emailTemplateId + ' not found');
    }

    import handlebars from 'handlebars';
    Vatfree.notify.registerHandlebarsHelpers(handlebars);

    let renderedEmailHtml = handlebars.compile(template.templateHTML)({text: text});
    let renderedEmailText = handlebars.compile(template.templateText)({text: Vatfree.notify.stripHtmlTags(text)});

    _.extend(options, {
        subject: subject,
        text: renderedEmailText,
        html: renderedEmailHtml
    });

    try {
        Email.send(options);
        if (activityIds) {
            let activityLog = {
                type: 'email',
                address: options.to,
                subject: subject,
                status: 'done',
                notes: renderedEmailText,
                html: renderedEmailHtml
            };
            _.each(activityIds, (id, idKey) => {
                activityLog[idKey] = id;
            });
            return ActivityLogs.insert(activityLog);
        }

        return true;
    } catch(e) {
        console.log(e);

        let activityLog ={
            type: 'task',
            subject: 'Sending of invoice email failed',
            status: 'new',
            notes: "Error: " + e.message
        };
        _.each(activityIds, (id, idKey) => {
            activityLog[idKey] = id;
        });
        ActivityLogs.insert(activityLog);
    }

    return false;
};

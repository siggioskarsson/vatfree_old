PaybackLines._ensureIndex({'invoiceId': 1}, {unique: 1});

Payback.after.insert(function(userId, doc) {
    // update all debt collectionIds
    PaybackLines.update({
        _id: {
            $in: doc.paybackLineIds
        }
    },{
        $set: {
            paybackId: doc._id
        }
    },{
        multi: 1
    });

    // update all invoices with the debt collection link
    Invoices.update({
        _id: {
            $in: doc.invoiceIds
        }
    },{
        $set: {
            paybackId: doc._id
        }
    },{
        multi: 1
    });
});

PaybackLines.after.insert(function(userId, doc) {
    Invoices.update({
        _id: doc.invoiceId
    },{
        $set: {
            paybackLineId: doc._id
        }
    });
});

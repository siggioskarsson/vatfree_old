if (!Meteor.settings.disableJobs) {
    invoiceJobs.allow({
        // Grant full permission to any authenticated user
        admin: function (userId, method, params) {
            return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
        }
    });

    Meteor.publish('invoice-jobs', function () {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return invoiceJobs.find({
            status: {
                $ne: 'completed'
            }
        });
    });

    Files.on('stored', Meteor.bindEnvironment(function (fileObj, store) {
        if (store === 'vatfree' && _.has(fileObj, 'itemId') && fileObj.target === 'invoices' && fileObj.subType === 'invoice') {
            console.log('Updating invoice status for', fileObj._id, 'to readyToSend');
            Invoices.update({
                _id: fileObj.itemId,
                status: 'new'
            }, {
                $set: {
                    status: 'readyToSend'
                }
            });
        }
    }));

    if (Meteor.settings.jobs && Meteor.settings.jobs.invoicePDF) {
        Meteor.startup(function () {
            // Start the myJobs queue running
            invoiceJobs.startJobServer();

            if (Meteor.settings.jobs.invoicePDF) {
                invoiceJobs.processJobs('invoicePdf', Meteor.settings.jobs.invoicePDF, (job, cb) => {
                    // This will only be called if a 'invoicePdf' job is obtained
                    let data = job.data;

                    console.log('Running invoicePdf job', data.invoiceId);
                    Vatfree.invoicePdf(data.invoiceId, data.reminder, (err, fileId) => {
                        if (err) {
                            console.error(err);
                            job.fail(err, {
                                fatal: true
                            });
                        } else {
                            job.log('created invoice pdf for ' + data.invoiceId + ' - created file ' + fileId);
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                });
            }

            if (Meteor.settings.jobs.invoicePdfImages) {
                invoiceJobs.processJobs('invoicePdfImages', Meteor.settings.jobs.invoicePdfImages, (job, cb) => {
                    // This will only be called if a 'invoicePdf' job is obtained
                    let data = job.data;

                    console.log('Running invoicePdfImages job', data.invoiceId);
                    Vatfree.invoicePdfImages(data.invoiceId, (err, fileId) => {
                        if (err) {
                            console.error(err);
                            job.fail(err, {
                                fatal: true
                            });
                        } else {
                            job.log('created invoice images pdf for ' + data.invoiceId + ' - created file ' + fileId);
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                });
            }

            if (Meteor.settings.jobs.sendAllOpenInvoices) {
                invoiceJobs.processJobs('sendAllOpenInvoices', Meteor.settings.jobs.sendAllOpenInvoices, (job, cb) => {
                    console.log('Running sendAllOpenInvoices job');
                    Vatfree.sendAllOpenInvoices((err, count) => {
                        if (err) {
                            console.error(err);
                            job.fail(err, {
                                fatal: true
                            });
                        } else {
                            job.log(count.success + ' invoices sent, ' + count.error + ' errors encountered');
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                });
            }

            if (Meteor.settings.jobs.createInvoices) {
                invoiceJobs.processJobs('createInvoices', Meteor.settings.jobs.createInvoices, (job, cb) => {
                    let data = job.data;

                    console.log('Running create invoices job', data.receiptIds.length);
                    Vatfree.createInvoices(data.receiptIds, (err, count) => {
                        if (err) {
                            console.error(err);
                            job.fail(err, {
                                fatal: true
                            });
                        } else {
                            job.log(count.success + ' invoices created, ' + count.error + ' errors encountered');
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                });
            }

            if (Meteor.settings.jobs.sendInvoiceReminder) {
                invoiceJobs.processJobs('sendInvoiceReminder', Meteor.settings.jobs.sendInvoiceReminder, (job, cb) => {
                    let data = job.data;

                    console.log('Running send invoice reminder job', data.invoiceId);
                    Vatfree.sendInvoiceReminder(data.invoiceId, data.reminder2, (err, count) => {
                        if (err) {
                            console.error(err);
                            job.fail(err, {
                                fatal: true
                            });
                        } else {
                            job.log('Reminder invoice sent for invoice ' + data.invoiceId);
                            job.progress(100, 100);
                            job.done();
                        }
                        cb(null);
                    });
                });
            }
        });
    }
}

/* global Vatfree: true */
import moment from 'moment';

Vatfree.invoicePdfImages = function (invoiceId, callback = false) {
    try {
        let invoice = Invoices.findOne({_id: invoiceId});
        let receipts = [];
        Receipts.find({
            _id: {
                $in: invoice.receiptIds || []
            }
        }).forEach((receipt) => {
            let files = [];
            let fileIndex = 1;
            Files.find({
                itemId: receipt._id,
                target: 'receipts',
                subType: 'main'
            }, {
                sort: {
                    createdAt: 1
                }
            }).forEach((file) => {
                let fileObj = new FS.File(file);
                file.data = getBase64DataSync(fileObj);
                file.fileIndex = fileIndex++;
                files.push(file);
            });
            if (files.length === 0) {
                // try again, but now for all sub types
                Files.find({
                    itemId: receipt._id,
                    target: 'receipts'
                }, {
                    sort: {
                        createdAt: 1
                    }
                }).forEach((file) => {
                    let fileObj = new FS.File(file);
                    //file.data = getBase64DataSync(fileObj);
                    file.url = file.getDirectUrl('vatfree');
                    file.fileIndex = fileIndex++;
                    files.push(file);
                });
            }
            receipt.files = files;

            receipts.push(receipt);
        });

        let templateData = {
            invoice: invoice,
            receipts: receipts
        };

        let invoiceTemplate = Assets.getText('templates/pdf/invoice-images.html');

        import handlebars from 'handlebars';
        let renderedHtml = handlebars.compile(invoiceTemplate)(templateData);

        import pdf from 'html-pdf';
        let options = _.clone(Vatfree.defaultPhantomJSOptions);
        options.quality = '75';
        options.timeout = 15 * 60 * 1000;
        pdf.create(renderedHtml, options).toBuffer(Meteor.bindEnvironment((err, buffer) => {
            if (err) {
                console.log('PDF image file failed', err);
                callback(err.message);
            } else {
                var newFile = new FS.File();
                newFile.name(invoice.invoiceNr + '-images.pdf');
                newFile.itemId = invoice._id;
                newFile.target = 'invoices';
                newFile.subType = 'images';
                newFile.createdAt = new Date();

                newFile.attachData(buffer, {
                    type: 'application/pdf'
                }, function (error) {
                    if (error) {
                        callback(error);
                    } else {
                        let fileObj = Files.insert(newFile);
                        fileObj.on('stored', Meteor.bindEnvironment((store) => {
                            if (store === 'vatfree') {
                                callback(null, fileObj._id);
                            }
                        }));
                    }
                });
            }
        }));
    } catch (e) {
        callback(e.message);
    }
};

var getBase64Data = function (doc, callback) {
    var buffer = Buffer.alloc(0);
    var readStream = doc.createReadStream();
    readStream.on('readable', function () {
        let read = readStream.read();
        if (read) {
            buffer = Buffer.concat([buffer, read]);
        }
    });
    readStream.on('error', function (err) {
        callback(err, null);
    });
    readStream.on('end', function () {
        // done
        callback(null, buffer.toString('base64'));
    });
};
var getBase64DataSync = Meteor.wrapAsync(getBase64Data);

Vatfree.invoicePdf = function (invoiceId, reminder = false, callback = false) {
    import QRCode from 'qrcode';
    console.log('invoicePdf', invoiceId, reminder, !!callback);
    try {
        let invoice = Invoices.findOne({_id: invoiceId});
        import handlebars from 'handlebars';

        const style = Assets.getText('templates/pdf/style.html');
        handlebars.registerHelper('style', function() {
            return style;
        });

        handlebars.registerHelper('formatCurrency', Vatfree.numbers.formatCurrency);
        handlebars.registerHelper('getDeferredServiceFeeVat', function (deferredServiceFee, countryId) {
            let vatRate = 0;
            if (Meteor.settings.chargeVatToCountry && Meteor.settings.chargeVatToCountry[countryId]) {
                vatRate = Meteor.settings.chargeVatToCountry[countryId];
            }
            return Vatfree.numbers.formatCurrency(Vatfree.vat.calculateVat(deferredServiceFee, vatRate));
        });
        handlebars.registerHelper('getDeferredServiceFeeInclVat', function (deferredServiceFee, countryId) {
            let vatRate = 0;
            if (Meteor.settings.chargeVatToCountry && Meteor.settings.chargeVatToCountry[countryId]) {
                vatRate = Meteor.settings.chargeVatToCountry[countryId];
            }
            return Vatfree.numbers.formatCurrency(deferredServiceFee + Vatfree.vat.calculateVat(deferredServiceFee, vatRate));
        });
        handlebars.registerHelper('getInvoiceVatRate', function (vat, currencySymbol) {
            if (vat && vat[this.rate]) {
                return Vatfree.numbers.formatCurrency(vat[this.rate], 2, currencySymbol || '€');
            }
        });
        handlebars.registerHelper('getTotalColumns', function (vat) {
            if (vat && vat.length) {
                return 2 + vat.length;
            } else {
                return 2;
            }
        });

        const _encodeQR = function (qrCode, callback) {
            QRCode.toDataURL(qrCode, function (err, dataUri) {
                if (err) {
                    console.error(err);
                } else {
                    callback(null, dataUri);
                }
            });
        };
        const encodeQr = Meteor.wrapAsync(_encodeQR);

        handlebars.registerHelper('getPoprQrCode', function () {
            if (this.poprId) {
                let poprQR = 'https://app.popr.io/' + this.poprId + '/' + this.poprSecret;
                return encodeQr(poprQR);
            }
        });

        let entity = {};
        if (invoice.shopId) {
            entity = Shops.findOne({
                _id: invoice.shopId
            }) || {};
        } else if (invoice.companyId) {
            entity = Companies.findOne({
                _id: invoice.companyId
            }) || {};
        }

        let invoiceLanguage = invoice.language || entity.language || 'nl';

        let invoiceLines = [];
        let shopTurnover = 0;
        let shopTurnoverExVat = 0;
        Receipts.find({
            _id: {
                $in: invoice.receiptIds || []
            }
        }).forEach((receipt) => {
            shopTurnover += receipt.amount;
            shopTurnoverExVat += receipt.amount - receipt.totalVat;
            invoiceLines.push(receipt);
        });

        let country = Countries.findOne({_id: entity.countryId});

        let shopAddress = (entity.addressFirst || '') + '\n' + (entity.postCode || entity.postalCode || '') + ' ' + (entity.city || '') + '\n' + (country.name || '');

        let referenceInvoice = {};
        if (invoice.referenceInvoiceId) {
            referenceInvoice = Invoices.findOne({_id: invoice.referenceInvoiceId});
        }

        let debtCollection = DebtCollectionLines.findOne({invoiceId: invoice._id}) || {};

        let paymentTerms = entity.paymentTerms || 14;

        let invoiceTemplateName = 'templates/pdf/invoice.' + invoiceLanguage + '.html';
        if (invoice.type) {
            invoiceTemplateName = 'templates/pdf/invoice-' + invoice.type + '.' + invoiceLanguage + '.html';
        } else if (reminder) {
            invoiceTemplateName = 'templates/pdf/invoice-reminder.' + invoiceLanguage + '.html';
        }

        let invoiceTemplate;
        try {
            invoiceTemplate = Assets.getText(invoiceTemplateName);
        } catch (e) {
            invoiceLanguage = 'en';
            invoiceTemplateName = 'templates/pdf/invoice.' + invoiceLanguage + '.html';
            if (invoice.type) {
                invoiceTemplateName = 'templates/pdf/invoice-' + invoice.type + '.' + invoiceLanguage + '.html';
            } else if (reminder) {
                invoiceTemplateName = 'templates/pdf/invoice-reminder.' + invoiceLanguage + '.html';
            }
            invoiceTemplate = Assets.getText(invoiceTemplateName);
        }

        let currency = Currencies.findOne({_id: invoice.currencyId}) || {};

        let templateData = {
            recipient: {
                name: entity.billingName ? entity.billingName : entity.name,
                address: (entity.billingAddress ? entity.billingAddress : shopAddress).replace(/\n/g, '<br />')
            },
            invoiceDate: moment(invoice.invoiceDate).format(TAPi18n.__('_date_format', {}, invoiceLanguage)),
            paymentTerms: paymentTerms,
            expiryDate: moment(invoice.invoiceDate).add(paymentTerms, 'days').format(TAPi18n.__('_date_format', {}, invoiceLanguage)),
            invoiceNr: invoice.invoiceNr,
            billingVat: (entity.billingVat || entity.vatNumber || ''),
            poNumber: entity.poNumber,
            invoiceLines: invoiceLines,
            totalVat: invoice.totalVat,
            amount: invoice.amount,
            serviceFee: invoice.serviceFee,
            refund: invoice.refund,
            amountExVat: invoice.amount - invoice.totalVat,
            shopTurnover: shopTurnover,
            shopTurnoverExVat: shopTurnoverExVat,
            currencyId: invoice.currencyId,
            currencySymbol: currency.symbol || '€',
            totalCredit: -1 * invoice.totalVat,
            referenceInvoice: referenceInvoice,
            debtCollection: debtCollection,
            country: country
        };
        let renderedHtml = handlebars.compile(invoiceTemplate)(templateData);

        let invoiceFile = new FS.File();
        invoiceFile.name(invoice.invoiceNr + (reminder ? '-reminder' : '') + '.pdf');
        invoiceFile.itemId = invoice._id;
        invoiceFile.target = 'invoices';
        invoiceFile.subType = 'invoice';

        if (callback) {
            Vatfree.creatPdfAsync(renderedHtml, invoiceFile, Vatfree.defaultPhantomJSOptions, Meteor.bindEnvironment(function (err, fileId) {
                if (err) {
                    callback(err);
                } else {
                    callback(null, fileId);
                }
            }));
        } else {
            return Vatfree.createPdf(renderedHtml, invoiceFile, Vatfree.defaultPhantomJSOptions);
        }

        // for testing
        // pdf.create(renderedHtml).toFile('/tmp/' + invoice.get('invoiceId') + '.pdf', (err, res) => {
        //     console.log('PDF Done', err, res);
        // });
    } catch (e) {
        if (callback) {
            callback(JSON.stringify(e, Object.getOwnPropertyNames(e)));
        } else {
            return false;
        }
    }
};

/* global Vatfree: true */

Vatfree.invoices = {};

Vatfree.invoices.status = function() {
    return {
        'new': 'default',
        'toCheck': 'default',
        'readyToSend': 'default',
        'sentToRetailer': 'info',
        'promisedToPay': 'info',
        'partiallyPaid': 'warning',
        'paymentDifference': 'warning',
        'paid': 'primary',
        'notCollectable': 'danger',
        'credited': 'danger',
    };
};

Vatfree.invoices.icons = function() {
    return {
        'new': 'icon icon-vtbnew',
        'toCheck': 'icon icon-waiting4admincheck',
        'readyToSend': 'icon icon-vtbsend',
        'sentToRetailer': 'icon icon-vtbwaiting',
        'promisedToPay': 'icon icon-shoppromised',
        'partiallyPaid': 'icon icon-vtbpaidbyshoppartial',
        'paymentDifference': 'icon icon-vtbpaidbyshoppartial',
        'paid': 'icon icon-vtbpaidbyshop',
        'notCollectable': 'icon icon-vtbnoncollectable',
        'credited': 'icon icon-invoice-creditet'
    };
};

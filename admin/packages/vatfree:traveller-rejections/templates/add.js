Template.addTravellerRejectionModal.onCreated(function() {
    let template = this;

    this.parentTemplate = Template.instance().parentTemplate();
    this.hideModal = () => {
        $('#modal-add-traveller-rejection').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-add-traveller-rejection')
            .on('hidden.bs.modal', function () {
                if (template.parentTemplate && template.parentTemplate.addingItem) template.parentTemplate.addingItem.set(false);
        });
        $('#modal-add-traveller-rejection').on('shown.bs.modal', function () {
            //
            $('input[name="name"]').focus();
        });
        $('#modal-add-traveller-rejection').modal('show');
    });
});

Template.addTravellerRejectionModal.events({
    'click .cancel-add-traveller-rejection'(e, template) {
        e.preventDefault();
        template.hideModal();
    },
    'submit form[name="add-traveller-rejection-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        TravellerRejections.insert(formData);
        template.hideModal();
    }
});

Template.travellerRejections.onCreated(Vatfree.templateHelpers.onCreated(TravellerRejections, '/rejections/', '/rejection-traveller/:itemId'));
Template.travellerRejections.onRendered(Vatfree.templateHelpers.onRendered());
Template.travellerRejections.onDestroyed(Vatfree.templateHelpers.onDestroyed());

Template.travellerRejections.helpers(Vatfree.templateHelpers.helpers);
Template.travellerRejections.helpers(travellerRejectionHelpers);
Template.travellerRejections.events(Vatfree.templateHelpers.events());

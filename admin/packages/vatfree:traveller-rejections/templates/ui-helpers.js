UI.registerHelper('getTravellerRejectionName', function(travellerRejectionId) {
    if (!travellerRejectionId) travellerRejectionId = this.travellerRejectionId;

    let travellerRejection = TravellerRejections.findOne({_id: travellerRejectionId});
    if (travellerRejection) {
        return travellerRejection.name;
    }

    return "";
});

UI.registerHelper('getTravellerRejections', function() {
    return TravellerRejections.find({}, {
        sort: {
            name: 1
        }
    });
});

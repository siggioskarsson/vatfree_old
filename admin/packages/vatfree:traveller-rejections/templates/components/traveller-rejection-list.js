Template.travellerRejectionList.onCreated(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('traveller-rejections');
    })
});

Template.travellerRejectionList.helpers({
    getTravellerRejections() {
        let selector = {};
        if (_.has(this, 'travellerRejectionIds')) {
            selector['_id'] = {
                $in: this.travellerRejectionIds || []
            }
        }
        return TravellerRejections.find(selector, {
            sort: {
                name: 1
            }
        });
    }
});

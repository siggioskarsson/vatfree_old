Template.travellerRejectionInfo.onRendered(Vatfree.templateHelpers.onRenderedInfiniteScrolling());
Template.travellerRejectionInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let limit = 20;
        let offset = 0;
        this.subscribe('activity-stream', {}, data._id, limit, offset);
    });
});

Template.travellerRejectionInfo.helpers(travellerRejectionHelpers);
Template.travellerRejectionInfo.helpers({
    getActivity() {
        return ActivityStream.collection.find({
            'activity.target.itemId': this._id
        },{
            sort: {
                date: -1
            },
            limit: 20
        });
    }
});

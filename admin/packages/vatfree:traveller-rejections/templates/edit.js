import autosize from 'meteor/vatfree:core-templates/templates/autosize';

Template.travellerRejectionEdit.onCreated(function () {

});

Template.travellerRejectionEdit.onRendered(function () {
    this.autorun(() => {
        this.subscribe('traveller-rejections_item', FlowRouter.getParam('itemId'));
    });
});

Template.travellerRejectionEdit.helpers({
    itemDoc() {
        return TravellerRejections.findOne({
            _id: FlowRouter.getParam('itemId')
        });
    },
    getBreadcrumbTitle() {
        return this.name;
    }
});

Template.travellerRejectionEditForm.onCreated(function() {
    this.vatRates = new ReactiveArray();
});

Template.travellerRejectionEditForm.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        this.vatRates.clear();
        if (data.vatRates) {
            let key = 0;
            _.each(data.vatRates, (rate) => {
                this.vatRates.push({
                    key: key++,
                    rate: rate.rate,
                    description: rate.description
                });
            });
        }
    });

    Tracker.afterFlush(() => {
        autosize(this.$('textarea'));
    });
});

Template.travellerRejectionEditForm.helpers({
    getVatRates() {
        return Template.instance().vatRates.array();
    },
    getTextForLanguage() {
        let data = Template.instance().data;
        if (data.text && data.text[this.code]) {
            return data.text[this.code];
        }
    }
});

Template.travellerRejectionEditForm.events({
    'click .add-vat-rate'(e, template) {
        e.preventDefault();
        template.vatRates.push({
            key: template.vatRates.length,
            rate: "",
            description: ""
        });
    },
    'click .cancel'(e) {
        e.preventDefault();
        FlowRouter.go('/rejections-traveller');
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);

        TravellerRejections.update({
            _id: this._id
        },{
            $set: formData
        }, function(err, result) {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                toastr.success('Traveller Rejection saved!')
            }
        });
    }
});

Template.travellerRejectionsList.helpers(Vatfree.templateHelpers.helpers);
Template.travellerRejectionsList.helpers(travellerRejectionHelpers);
Template.travellerRejectionsList.events({
    'click .edit-item'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        if (template.activeItem) {
            template.activeItem.set(this._id);
        }
        FlowRouter.go('/rejection-traveller/:itemId', {itemId: this._id});
    }
});

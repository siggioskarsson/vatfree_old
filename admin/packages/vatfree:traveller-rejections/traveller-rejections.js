TravellerRejections = new Meteor.Collection('traveller-rejections');
TravellerRejections.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "The internal id ised to identify the rejection reason",
        optional: false
    },
    description: {
        type: String,
        label: "Descriptive text of the rejection",
        optional: true
    },
    text: {
        type: Object,
        label: "All the translations for this reject reason",
        optional: true,
        blackbox: true
    }
}));
TravellerRejections.attachSchema(CreatedUpdatedSchema);


TravellerRejections.allow({
    insert: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    update: function (userId, doc, fieldNames, modifier) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    },
    remove: function (userId, doc) {
        return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
    }
});

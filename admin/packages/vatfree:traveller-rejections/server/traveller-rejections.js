ActivityStream.attachHooks(TravellerRejections);

Meteor.methods({
    'import-traveller-rejections'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let data = JSON.parse(Assets.getText('traveller-rejections.json'));
        _.each(data, (travellerRejection) => {
            TravellerRejections.insert({
                id: travellerRejection.id,
                name: travellerRejection.name,
                code: travellerRejection.code
            });
        });
    }
});

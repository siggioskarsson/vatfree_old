Meteor.publish('traveller-rejections', function(searchTerm, selector, sort, limit, offset) {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, ['admin', 'vatfree'], Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    selector = selector || {};

    if (searchTerm) {
        searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
        let searchTerms = searchTerm.split(' ');

        selector['$and'] = [];
        _.each(searchTerms, (s) => {
            selector['$and'].push({textSearch: new RegExp(s)});
        });
    }

    return TravellerRejections.find(selector, {
        sort: sort,
        limit: limit,
        offset: offset
    });
});

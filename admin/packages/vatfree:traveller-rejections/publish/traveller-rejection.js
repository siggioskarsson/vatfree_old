Meteor.publish('traveller-rejections_item', function(itemId) {
    check(itemId, String);
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return TravellerRejections.find({
       _id: itemId
    });
});

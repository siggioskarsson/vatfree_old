FlowRouter.route('/rejections-traveller', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "travellerRejections"});
    }
});

FlowRouter.route('/rejection-traveller/:itemId', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "travellerRejectionEdit"});
    }
});

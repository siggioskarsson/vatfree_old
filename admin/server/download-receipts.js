Meteor.methods({
    /*
        Test method for downloading receipts for partner purposes
     */
    'download-receipts'() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Files.find({
            target: 'receipts',
            subType: {
                $ne: 'form'
            }
        },{
            sort: {
                createdAt: -1
            },
            limit: 100,
            skip: 200
        }).forEach((file) => {
            let url = file.getDirectUrl('vatfree');
            let name = "/Users/oskarsson/tmp/receiptImages/" + file._id + '_' + file.name();
            downloadFile(url, name);
        });
    }
});

const downloadFile = function(url, name) {
    const https = require('https');
    var fs = require('fs');

    var file = fs.createWriteStream(name);
    var request = https.get(url, function(response) {
        response.pipe(file);
    });
};

if (Meteor.settings.wechat) {
    ServiceConfiguration.configurations.upsert({
        service: 'wechat'
    }, {
        $set: {
            appId: Meteor.settings.wechat.appId,
            secret: Meteor.settings.wechat.secret,
            mpAppId: Meteor.settings.wechat.mpAppId,
            mpSecret: Meteor.settings.wechat.mpSecret,
            mainId: 'unionId'
        }
    });
}

ServiceConfiguration.configurations.upsert({
    service: "linkedin"
}, {
    $set: {
        clientId: Meteor.settings.linkedin.clientId,
        loginStyle: "popup",
        secret: Meteor.settings.linkedin.secret
    }
});

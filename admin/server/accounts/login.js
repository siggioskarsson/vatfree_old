if (Meteor.settings.versionInfo == 'DEV') {
    Meteor.methods({
        loginFromServer: function(email) {
            let user = Meteor.users.findOne({'profile.email': email});
            if (user && user._id) {
                let stampedLoginToken = Accounts._generateStampedLoginToken();
                Accounts._insertLoginToken(user._id, stampedLoginToken);
                return stampedLoginToken;
            }
        }
    });
}
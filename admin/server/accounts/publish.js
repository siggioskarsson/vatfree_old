Meteor.publish(null, function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "services.twitter.profile_image_url_https": 1,
            "services.facebook.id": 1,
            "services.google.picture": 1,
            "services.github.username": 1,
            "services.instagram.profile_picture": 1,
            "services.linkedin.pictureUrl": 1
        }
    });
});

ServiceConfiguration.configurations.upsert({
    service: "facebook"
}, {
    $set: {
        appId: Meteor.settings.facebook.clientId,
        loginStyle: "popup",
        secret: Meteor.settings.facebook.secret
    }
});

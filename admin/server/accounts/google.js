ServiceConfiguration.configurations.upsert({
    service: 'google'
}, {
    $set: {
        clientId: Meteor.settings.google.clientId,
        loginStyle: 'popup',
        secret: Meteor.settings.google.secret
    }
});

const checkEmailAgainstAllowedDomains = function (email) {
    let allowedDomains = ['vatfree.com'];
    let domain = email.replace(/.*@/, '').toLowerCase();
    return _.contains(allowedDomains, domain);
};

Accounts.isValidNewUser = function (newUser) {
    let newUserEmail = newUser.services.google.email;
    if (!newUserEmail) {
        throw new Meteor.Error(403, 'You need a valid email address to sign up.');
    }
    if (!checkEmailAgainstAllowedDomains(newUserEmail)) {
        if (!_.contains(newUser.roles.__global_roles__, 'vatfree')) {
            throw new Meteor.Error(403, 'You need an accepted organization email address to sign up.');
        }
    }

    return true;
};

Accounts.validateNewUser(function (newUser) {
    return Accounts.isValidNewUser(newUser);
});

Accounts.validateLoginAttempt(function (loginAttempt) {
    if (loginAttempt.error && loginAttempt.error.reason) {
        throw new Meteor.Error(404, loginAttempt.error.reason);
    }

    if (!loginAttempt.user) {
        throw new Meteor.Error(404, "Invalid login attempt");
    }

    let userEmail = loginAttempt.user.services.google.email;
    if (!userEmail) {
        throw new Meteor.Error(403, 'You need a valid email address to sign up.');
    }
    if (!checkEmailAgainstAllowedDomains(userEmail)) {
        if (!_.contains(loginAttempt.user.roles.__global_roles__, 'vatfree')) {
            throw new Meteor.Error(403, 'You need an accepted organization email address to sign up.');
        }
    }

    return true;
});

Meteor.publish("menu-stats", function () {
    this.unblock();
    if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP) && !Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    const self = this;
    let initializing = true;
    let handles = {};

    if (Vatfree.userIsInRole(Meteor.userId(), 'travellers-tasks')) {
        let countTravellers = 0;
        handles.travellers = Meteor.users.find({
            'private.status': 'userUnverified'
        },{
            fields: {
                _id: 1
            }
        }).observeChanges({
            added: function (id, fields) {
                countTravellers++;
                if (!initializing)
                    self.changed("stats", 'travellers', {count: countTravellers});
            },
            removed: function (id, fields) {
                countTravellers--;
                if (!initializing)
                    self.changed("stats", 'travellers', {count: countTravellers});
            }
        });
        self.added("stats", 'travellers', {count: countTravellers});
    }

    var countReceipts = 0;
    var handleReceipts = Receipts.find({
        $or: [
            {
                status: 'visualValidationPending'
            },
            {
                checkedBy: {
                    $ne: this.userId
                },
                status: 'waitingForDoubleCheck'
            }
        ]
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countReceipts++;
            if (!initializing)
                self.changed("stats", 'receipts', {count: countReceipts});
        },
        removed: function() {
            countReceipts--;
            if (!initializing)
                self.changed("stats", 'receipts', {count: countReceipts});
        }
    });

    var countReceiptShops = 0;
    var handleReceiptShops = Receipts.find({
        status: 'shopPending',
        shopId: {
            $exists: false
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countReceiptShops++;
            if (!initializing)
                self.changed("stats", 'receipt_shops', {count: countReceiptShops});
        },
        removed: function() {
            countReceiptShops--;
            if (!initializing)
                self.changed("stats", 'receipt_shops', {count: countReceiptShops});
        }
    });

    var countInvoices = 0;
    var handleInvoices = Invoices.find({
        status: 'new',
        deleted: {
            $ne: true
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countInvoices++;
            if (!initializing)
                self.changed("stats", 'invoices', {count: countInvoices});
        },
        removed: function() {
            countInvoices--;
            if (!initializing)
                self.changed("stats", 'invoices', {count: countInvoices});
        }
    });

    var countShopIncompletes = 0;
    var handleShopIncompletes = Shops.find({
        status: 'incomplete',
        deleted: {
            $exists: false
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countShopIncompletes++;
            if (!initializing)
                self.changed("stats", 'shop_incompletes', {count: countShopIncompletes});
        },
        removed: function() {
            countShopIncompletes--;
            if (!initializing)
                self.changed("stats", 'shop_incompletes', {count: countShopIncompletes});
        }
    });

    var countShopCalls = 0;
    var handleShopCalls = Shops.find({
        status: 'toCall',
        deleted: {
            $exists: false
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countShopCalls++;
            if (!initializing)
                self.changed("stats", 'shop_calls', {count: countShopCalls});
        },
        removed: function() {
            countShopCalls--;
            if (!initializing)
                self.changed("stats", 'shop_calls', {count: countShopCalls});
        }
    });

    var countPayments = 0;
    var handlePayments = Payments.find({
        invoiceIds: {
            $exists: false
        },
        ignorePayment: {
            $ne: true
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countPayments++;
            if (!initializing)
                self.changed("stats", 'payments', {count: countPayments});
        },
        removed: function() {
            countPayments--;
            if (!initializing)
                self.changed("stats", 'payments', {count: countPayments});
        }
    });

    var countPayouts = 0;
    var handlePayouts = Payouts.find({
        status: 'new',
        payoutBatchId: {
            $exists: false
        },
        partnerId: {
            $exists: false
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countPayouts++;
            if (!initializing)
                self.changed("stats", 'payouts', {count: countPayouts});
        },
        removed: function() {
            countPayouts--;
            if (!initializing)
                self.changed("stats", 'payouts', {count: countPayouts});
        }
    });

    var countPayoutsPartner = 0;
    var handlePayoutsPartner = Payouts.find({
        status: 'new',
        payoutBatchId: {
            $exists: false
        },
        partnerId: {
            $exists: true
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countPayoutsPartner++;
            if (!initializing)
                self.changed("stats", 'payouts-partner', {count: countPayoutsPartner});
        },
        removed: function() {
            countPayoutsPartner--;
            if (!initializing)
                self.changed("stats", 'payouts-partner', {count: countPayoutsPartner});
        }
    });

    var countAffiliatePayouts = 0;
    var handleAffiliatePayouts = AffiliatePayouts.find({
        status: 'new',
        affiliatePayoutBatchId: {
            $exists: false
        }
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countAffiliatePayouts++;
            if (!initializing)
                self.changed("stats", 'affiliate-payouts', {count: countAffiliatePayouts});
        },
        removed: function() {
            countAffiliatePayouts--;
            if (!initializing)
                self.changed("stats", 'affiliate-payouts', {count: countAffiliatePayouts});
        }
    });

    var countActivityLogs = 0;
    var handleActivityLogs = ActivityLogs.find({
        $or: [
            {
                status: {
                    $in: ['new', 'doing']
                },
                userId: this.userId
            },
            {
                type: 'task',
                status: {
                    $in: ['new', 'doing']
                },
                userId: {
                    $exists: false
                }

            }
        ]
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countActivityLogs++;
            if (!initializing)
                self.changed("stats", 'activity-logs', {count: countActivityLogs});
        },
        removed: function() {
            countActivityLogs--;
            if (!initializing)
                self.changed("stats", 'activity-logs', {count: countActivityLogs});
        }
    });

    var countAffiliates = 0;
    var handleAffiliates = Affiliates.find({
        'affiliate.status': 'new'
    },{
        fields: {
            _id: 1
        }
    }).observeChanges({
        added: function (id, fields) {
            countAffiliates++;
            if (!initializing)
                self.changed("stats", 'affiliates', {count: countAffiliates});
        },
        removed: function() {
            countAffiliates--;
            if (!initializing)
                self.changed("stats", 'affiliates', {count: countAffiliates});
        }
    });

    // Observe only returns after the initial added callbacks have
    // run.  Now return an initial value and mark the subscription
    // as ready.
    initializing = false;
    self.added("stats", 'receipts', {count: countReceipts});
    self.added("stats", 'receipt_shops', {count: countReceiptShops});
    self.added("stats", 'invoices', {count: countInvoices});
    self.added("stats", 'shop_incompletes', {count: countShopIncompletes});
    self.added("stats", 'shop_calls', {count: countShopCalls});
    self.added("stats", 'payments', {count: countPayments});
    self.added("stats", 'payouts', {count: countPayouts});
    self.added("stats", 'payouts-partner', {count: countPayoutsPartner});
    self.added("stats", 'affiliate-payouts', {count: countAffiliatePayouts});
    self.added("stats", 'activity-logs', {count: countActivityLogs});
    self.added("stats", 'affiliates', {count: countAffiliates});
    self.ready();

    // Stop observing the cursor when client unsubs.
    // Stopping a subscription automatically takes
    // care of sending the client any removed messages.
    self.onStop(function () {
        handleReceipts.stop();
        handleReceiptShops.stop();
        handleInvoices.stop();
        handleShopIncompletes.stop();
        handleShopCalls.stop();
        handlePayments.stop();
        handlePayouts.stop();
        handlePayoutsPartner.stop();
        handleAffiliatePayouts.stop();
        handleActivityLogs.stop();
        handleAffiliates.stop();

        _.each(handles, (handle, handleKey) => {
            handle.stop();
        });
    });
});

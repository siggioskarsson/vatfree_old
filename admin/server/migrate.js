import { SHA256 } from 'meteor/sha';
import { checkTotalPayments } from 'meteor/vatfree:payments/server/lib/payments';
import { getIBAN } from 'meteor/vatfree:payments/server/payments';
import moment from 'moment';
import { pruneEmpty } from 'meteor/vatfree:core/lib/prune';

Migrations.add({
    version: 1,
    name: 'Fix invoices secret',
    up: function () {
        Invoices.find({
            secret: {
                $exists: false
            }
        }).forEach(function (doc) {
           Invoices.direct.update({
               _id: doc._id
           },{
               $set: {
                   secret: Random.id(64)
               }
           });
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 2,
    name: 'Fix user emails',
    up: function () {
        Meteor.users.find({
            emails: {
                $exists: false
            },
            'profile.email': {
                $exists: true
            }
        }).forEach(function (doc) {
            Meteor.users.direct.update({
                _id: doc._id
            }, {
                $set: {
                    emails: [
                        {
                            address: doc.profile.email,
                            verified: true
                        }
                    ]
                }
            }, (err, res) => {
                if (err) {
                    console.log(err, doc);
                }
            });
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 3,
    name: 'Fix shop currencies',
    up: function () {
        Shops.find({
            currencyId: {
                $exists: false
            }
        }).forEach((doc) => {
            let country = Countries.findOne({_id: doc.countryId}) || {};
            Shops.direct.update({
                _id: doc._id
            }, {
                $set: {
                    currencyId: country.currencyId || 'NYG8kkBuAzp8ph6Kn'
                }
            }, (err, res) => {
                if (err) {
                    console.log(err, doc);
                }
            });
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 4,
    name: 'Fix email texts group',
    up: function () {
        EmailTexts.find({
            group: {
                $exists: false
            }
        }).forEach(function (doc) {
            EmailTexts.direct.update({
                _id: doc._id
            }, {
                $set: {
                    group: 'shops'
                }
            });
        });
    },
    down: function () {
    }
});

let euCountries = [
    'AT', 'BE', 'HR', 'BG', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE',
    'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'GB'
];

Migrations.add({
    version: 5,
    name: 'Update visa requiremens',
    up: function () {
        Countries.update({
            code: {
                $in: euCountries
            }
        }, {
            $set: {
                needVisa: true
            }
        }, {
            multi: 1
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 6,
    name: 'Update wrong invoice amounts',
    up: function () {
        Invoices.find({
            type: {
                $exists: false
            },
            totalVat: {
                $gt: 0
            }
        }).forEach((invoice) => {
            Invoices.update({
                _id: invoice._id
            }, {
                $set: {
                    amount: invoice.totalVat,
                    totalVat: 0
                }
            });
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 7,
    name: 'Update payments text search',
    up: function () {
        Payments.update({}, {
            $set: {
                notes: ''
            }
        }, {
            multi: 1
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 8,
    name: 'Remove waitingForVerification status from shops',
    up: function () {
        Shops.update({
            status: 'waitingForVerification'
        }, {
            $set: {
                status: 'new'
            }
        }, {
            multi: 1
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 9,
    name: 'Fix missing partnershipStatus in shops',
    up: function () {
        Shops.update({
            partnershipStatus: {
                $exists: false
            }
        }, {
            $set: {
                partnershipStatus: 'new'
            }
        }, {
            multi: 1
        });
    },
    down: function () {
    }
});

let updateBillingCompanyIds = function () {
    Receipts.find({
        companyId: {
            $exists: true
        },
        billingCompanyId: {
            $exists: false
        }
    }).forEach((receipt) => {
        let billingCompany = Vatfree.billing.getBillingEntityForCompany(receipt.companyId);
        if (billingCompany) {
            Receipts.direct.update({
                _id: receipt._id
            }, {
                $set: {
                    billingCompanyId: billingCompany._id
                }
            });
        }
    });
};
Meteor.methods({
    migrate_updateBillingCompanyIds() {
        return updateBillingCompanyIds();
    }
});

Migrations.add({
    version: 10,
    name: 'Fix billingCompanyId in receipts',
    up: function () {
        Meteor.setTimeout(() => {
            updateBillingCompanyIds();
        }, 1000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 11,
    name: 'Fix pending users for new receipt flow',
    up: function () {
        Receipts.find({
            status: 'userPending'
        }).forEach((receipt) => {
            let shop = Shops.findOne({_id: receipt.shopId});
            if (shop && shop.partnershipStatus === 'partner') {
                Receipts.direct.update({
                    _id: receipt._id
                }, {
                    $set: {
                        status: receipt.originalsCheckedBy ? 'visualValidationPending' : 'waitingForOriginal'
                    }
                });
            }
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 12,
    name: 'Get all countries of invoices',
    up: function () {
        Invoices.find().forEach((invoice) => {
            let entity;
            if (invoice.shopId) {
                entity = Shops.findOne({_id: invoice.shopId}, {fields: {countryId: 1, partnershipStatus: 1}});
            } else {
                entity = Companies.findOne({_id: invoice.companyId}, {fields: {countryId: 1, partnershipStatus: 1}});
            }
            if (entity && entity.countryId) {
                Invoices.update({
                    _id: invoice._id
                }, {
                    $set: {
                        countryId: entity.countryId,
                        partnershipStatus: entity.partnershipStatus || 'new'
                    }
                });
            }
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 13,
    name: 'Update partnerShip status of invoices',
    up: function () {
        Invoices.find({}, {
            fields: {
                _id: 1,
                shopId: 1,
                companyId: 1,
                partnershipStatus: 1
            }
        }).forEach((invoice) => {
            let entity;
            if (invoice.shopId) {
                entity = Shops.findOne({_id: invoice.shopId}, {fields: {partnershipStatus: 1}}) || {};
            } else {
                entity = Companies.findOne({_id: invoice.companyId}, {fields: {partnershipStatus: 1}}) || {};
            }

            if (invoice.partnershipStatus !== entity.partnershipStatus) {
                Invoices.update({
                    _id: invoice._id
                }, {
                    $set: {
                        partnershipStatus: entity.partnershipStatus || 'new'
                    }
                });
            }
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 14,
    name: 'Add taskedAt date to all receipts',
    up: function () {
        Receipts.find({}, {
            fields: {
                createdAt: 1
            }
        }).forEach((receipt) => {
            Receipts.direct.update({
                _id: receipt._id
            }, {
                $set: {
                    taskedAt: receipt.createdAt
                }
            });
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 15,
    name: 'Add taskedAt date to all receipts',
    up: function () {
    },
    down: function () {
    }
});
Migrations.add({
    version: 16,
    name: 'Add taskedAt date to all receipts',
    up: function () {
    },
    down: function () {
    }
});
Migrations.add({
    version: 17,
    name: 'Add taskedAt / textSearch date to all travellers',
    up: function () {
        Meteor.setTimeout(function () {
            Meteor.users.find({}).forEach((user) => {
                Meteor.users.direct.update({
                    _id: user._id
                }, {
                    $set: {
                        taskedAt: user.createdAt,
                        textSearch: Meteor.users.getTextSearch(user)
                    }
                });
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 18,
    name: 'Update debt collection text searches',
    up: function () {
        DebtCollection.find({}).forEach((doc) => {
            DebtCollection.updateTextSearch(doc);

            let amount = 0;
            Invoices.find({
                _id: {
                    $in: doc.invoiceIds
                }
            }, {
                fields: {
                    amount: 1
                }
            }).forEach((invoice) => {
                amount += Number(invoice.amount);
            });
            DebtCollection.direct.update({
                _id: doc._id
            }, {
                $set: {
                    amount: amount
                }
            });
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 19,
    name: 'Update payout text search',
    up: function () {
        Payouts.find({}).forEach((payout) => {
            Payouts.updateTextSearch(payout);
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 20,
    name: 'Update partnerShip status ordering on shops',
    up: function () {
        Meteor.setTimeout(function () {
            _.each(['partner', 'affiliate', 'pledger', 'new', 'uncooperative'], (partnershipStatus, index) => {
                Shops.direct.update({
                    partnershipStatus: partnershipStatus
                }, {
                    $set: {
                        partnershipStatusOrder: index
                    }
                }, {
                    multi: true
                });
            });
            // set for shops without a status
            Shops.direct.update({
                partnershipStatus: {$exists: false}
            }, {
                $set: {
                    partnershipStatusOrder: 5
                }
            }, {
                multi: true
            });
            Shops.direct.update({
                partnershipStatus: null
            }, {
                $set: {
                    partnershipStatusOrder: 5
                }
            }, {
                multi: true
            });
        }, 5000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 21,
    name: 'Add archive code nr',
    up: function () {
        Meteor.setTimeout(function () {
            Receipts.find({
                archiveCode: {
                    $exists: true
                },
                archiveCodeNr: {
                    $exists: false
                }
            }).forEach((receipt) => {
                Receipts.update({
                    _id: receipt._id
                }, {
                    $set: {
                        archiveCode: receipt.archiveCode
                    }
                });
            });
        }, 2000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 22,
    name: 'Update sent by method for invoices',
    up: function () {
        Invoices.update({}, {
            $set: {
                sendBy: 'email'
            }
        }, {
            multi: true
        });
        Invoices.update({
            'mailsSent.options.subject': /^POST/
        }, {
            $set: {
                sendBy: 'post'
            }
        }, {
            multi: true
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 23,
    name: 'Prune mailSent from invoices',
    up: function () {
        Meteor.setTimeout(function () {
            Invoices.direct.update({}, {
                $unset: {
                    mailsSent: 1
                }
            }, {
                multi: true
            });
        }, 2000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 24,
    name: 'Migrate salesforce contactIds to array',
    up: function () {
        Meteor.setTimeout(function () {
            Meteor.users.find({
                'profile.contactId': {
                    $exists: true
                }
            }).forEach((user) => {
                if (!_.isArray(user.profile.contactId)) {
                    Meteor.users.direct.update({
                        _id: user._id
                    }, {
                        $set: {
                            'profile.contactId': [user.profile.contactId]
                        }
                    });
                }
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 25,
    name: 'Migrate receipt rejections types',
    up: function () {
        ReceiptRejections.update({}, {
            $set: {
                type: 'receipt'
            }
        }, {
            multi: true
        });

        if (!ReceiptRejections.findOne({type: 'invoice'})) {
            let newTypes = [
                {
                    name: 'Paid directly to traveller',
                    description: 'The shop paid the refund directly to the traveller.',
                    text: {
                        en: 'The shop has paid the receipt refund directly.'
                    },
                    type: 'invoice',
                    allowReopen: false
                },
                {
                    name: 'Works exclusively with competitor',
                    description: 'The shop works exclusively with a competitor.',
                    text: {
                        en: 'The shop works exclusively with a competitor.'
                    },
                    type: 'invoice',
                    allowReopen: false
                },
                {
                    name: 'Went bankrupt',
                    description: 'The shop has gone bankrupt.',
                    text: {
                        en: 'The shop has gone bankrupt.'
                    },
                    type: 'invoice',
                    allowReopen: false
                },
                {
                    name: 'Decided to stop co-operating',
                    description: 'The shop decided to stop co-operating.',
                    text: {
                        en: 'The shop decided to stop co-operating.'
                    },
                    type: 'invoice',
                    allowReopen: false
                }
            ];

            _.each(newTypes, (newType) => {
                ReceiptRejections.insert(newType);
            });
        }
    },
    down: function () {
    }
});

Migrations.add({
    version: 26,
    name: 'Add shaSum to all receipts',
    up: function () {
        Meteor.setTimeout(function () {
            Receipts.find({}).forEach((receipt) => {
                Receipts.direct.update({
                    _id: receipt._id
                }, {
                    $set: {
                        shaSum: Vatfree.crypto.calculateReceiptHash(receipt)
                    }
                });
            });
        }, 2000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 27,
    name: 'Add tel codes to countries',
    up: function () {
        let telCodes = {
            '2Xg8K35mX9RmHKgk4': ['65'],
            '33fGyXaCxpHxPxQAy': ['973'],
            '34dCtumvJ2ypFpXk3': ['500'],
            '3n9jaH35wrYwBWu4Q': ['61'],
            '3ozbjvjh6WbXrLETv': ['502'],
            '43R53Zjh6z3tptJ4v': ['248'],
            '45sxkxHAtsAfdod4n': ['356'],
            '4XzPbpbJqtfABtQNi': ['359'],
            '4bagEEXW68esr7yLW': ['1-345'],
            '4goBrh2FeWGpA4ZbM': ['47'],
            '4pgDtqJdaAbrRt4vG': ['93'],
            '58ZecARNwtEXXHrw8': ['55'],
            '5AFksGjnYXytd2kDv': ['41'],
            '5FDXcbXkxM4TeFpLx': ['90'],
            '5GGjoZTu8BoqSZsuZ': ['246'],
            '5TxS7AEkMefzwqyvn': ['7'],
            '5omw9mTP84XLj3dCp': ['64'],
            '6BfBYgRb4KrraJ6xd': ['886'],
            '6TPEKcSe3vhsKQKZF': ['597'],
            '6YASWHt4Y6vXEGtrY': ['53'],
            '6dbkEjHPxWiS7Msiy': ['352'],
            '6vas8poua2ab2EpRR': ['92'],
            '7Ph8YEnXbqjuqqrr3': ['880'],
            '7RgnnWqF3Ayd2SypB': ['378'],
            '7ZxJMQRvNNavWDAXj': ['960'],
            '83REPRGGtgxkfyyyP': ['54'],
            '8hJb7mQFyiXaZSsvE': ['681'],
            '8keWFRbNcuXJN72dE': ['84'],
            '8qWH3GLKvjtMLWynC': ['220'],
            '97em2mQufe2bHMtMq': ['1-264'],
            '9q8T6BwGsm4Xd6zfJ': ['591'],
            '9z85a43X8q6dHmoeo': ['370'],
            'ATuY8Q3RPsisNh64p': ['30'],
            'Ajda8QECrR68ZRrzW': ['56'],
            'AsPLm38abLvtYg5wf': ['855'],
            'AxZc7TbEQdRaMMWfk': ['40'],
            'AzfEYatdKGTnaRsLZ': ['261'],
            'B2CyENYwA7tMvho3h': ['971'],
            'BEbkaDDmasapkCzkM': ['250'],
            'BdEzxDajrwcTiQjxc': ['44-1534'],
            'Bqx8SbzmPHLNqCPRr': ['48'],
            'CKuAXMq3EXhHZdiGw': ['508'],
            'CTuj6mERv6rRpg9g3': ['994'],
            'CgqpDgiWLyafmw2R3': ['51'],
            'E4rKCcrGK8WtBQ8pG': ['254'],
            'EDkbyuMCkxqh2hMF4': ['963'],
            'EkhCD2FgduqaWXQXG': ['95'],
            'EoKSuLTDooFP5cLk2': ['599'],
            'EoKfXdfDxFhH4sex8': ['1-664'],
            'EyvGKGJhEnXkBFxSX': ['503'],
            'F7zTvFvP2RsJHviGF': ['1-876'],
            'FL9t533ECCK75JzcM': ['1-869'],
            'Fq4Jrko2kG8tjH8gD': ['1'],
            'FuNhrERLZrdgpbATM': ['387'],
            'FwE2iCr8q84Rsw59s': ['386'],
            'G3x83pXB5ZSL2r4i9': ['506'],
            'GLys8XF9BudMe9phR': ['509'],
            'HHw7WW3GAsBX3iemb': ['269'],
            'HLxXq64D66RnJpfAn': ['297'],
            'HxBLfDamZC7Kv4gRH': ['223'],
            'Jiuc4urdabWYoGHgh': ['291'],
            'JmvF7T7BhLCeregQS': ['677'],
            'JzYLahEumgfCuL8py': ['374'],
            'KRKTKfauk7zxQyXTj': ['962'],
            'Kaw8xYF4TqcKdZaSB': ['262'],
            'KmBigidzCBRAQmeFL': ['266'],
            'L2vuFk73GsTGFXNGL': ['299'],
            'LSH3REaRNfZPHZupZ': ['670'],
            'LSHDpTJKb9wJ9y9ZF': ['423'],
            'LWfH8DzwhYTdg8mXe': ['375'],
            'LfAPWctb5XJkSty6B': ['993'],
            'LgcJypnHiJP7ESzLA': ['218'],
            'Lj3MvnTeBNoP3wth3': ['389'],
            'Lo546qXn87vv3nLHS': ['590'],
            'MQS6YWWA7BThvt2Wp': ['52'],
            'MijdNL3DskM7LR46D': ['505'],
            'MtQAEokuutiro4b88': ['98'],
            'N2dm9gEwirE9uu9vj': ['231'],
            'NBxXey9MoapvyJi7o': ['373'],
            'NN9RfJBHKffCMqxRC': ['673'],
            'NXzuQKJem7TtWuqBi': ['233'],
            'NbZArWWsEvtW57rBf': ['968'],
            'P7rDcqMvka2a5mpEj': ['251'],
            'PBvjd85Grn5Z6QYWK': ['58'],
            'PTTeAm3RLtsYNzg3R': ['290'],
            'PiG9R2Hd7FkFn5NCT': ['501'],
            'PradQZ3uRfnNaxwec': ['592'],
            'Pxjq8Qfs6bf3PswFk': ['1'],
            'QHQfADghSorLPhmbY': ['381'],
            'QJiLkYJa372WhKWXR': ['213'],
            'QWo2xnt8dovJqbdH8': ['964'],
            'QeNi4GoFxDCcr5HCn': ['94'],
            'QiRbW8fGgS8xwD7Yj': ['60'],
            'Qor8LH3mAZ78PNGgg': ['44'],
            'RA6dy6fbeB56LQDh8': ['264'],
            'RFR2MMnGiLDPvnwrv': ['672'],
            'Ru8tkRGrEFAFaZvPB': ['383'],
            'Rvxy5Mfac4mpNyBkG': ['34'],
            'Rw5GCGmsj2xXXeFdh': ['853'],
            'SAB56rn98PHWNG6cy': ['1-441'],
            'SDzhfwttqKhooJuuv': ['212'],
            'SdDWyEKbkj3dvcgbh': ['507'],
            'TJi3vqtKwLL8JMEXn': ['382'],
            'TkXPfP5zmMf5angar': ['421'],
            'TrHs8WQiXrcYM2Ekf': ['1-721'],
            'TrNzLo9wwptr7uJnD': ['1-340'],
            'W5ZrtgHhGgbsCYyA4': ['245'],
            'WF7gHZPbDdDHsuHjr': ['241'],
            'WGDFTzWfYPXYoEKRn': ['961'],
            'WbWp4gC2ggTi4cTZP': ['350'],
            'XHQtNPdJzJS64e9o6': ['675'],
            'XbXhCTP8pmxoj5XEz': ['354'],
            'Xej2i6K7o74WB3Wrz': ['268'],
            'YDnyx82bnfntWaHGm': ['998'],
            'YdSL4YbNH8FppatED': ['244'],
            'YhqKDPwuHvHbmv5Nm': ['229'],
            'YkNJwYe652seMgXqE': ['385'],
            'YpjNzaKnwYoMMpgQX': ['690'],
            'Yqix5jkqj4Lt9Ej2q': ['260'],
            'YtGGate2785DtWYsP': ['298'],
            'Yv3MhPSDoMjGmhc6o': ['61'],
            'Z2i7a68AHHW2pQgRy': ['239'],
            'Z75NsLw8xAFapzWtd': ['852'],
            'ZEFzTvX6WrmJFqWBA': ['86'],
            'ZLr8SgFxLqJu6uYsi': ['688'],
            'ZZSczBYbMph8jJXkt': ['977'],
            'ZZWMonvaAQn9iC3Ak': ['240'],
            'ZZzyG3CZMdPRD8twy': ['1-473'],
            'a2DF3PoHYTGgPQMAC': ['255'],
            'a4dh7Yva3pjsNDxK6': ['27'],
            'a7sAxR4mC5Z79DJBC': ['353'],
            'a8MzHGJxcu3EB4ZKw': ['682'],
            'ai2dmGGWuLZbG5gEP': ['504'],
            'ayNB4PBvKezztPZiD': ['974'],
            'b7d7cAWyo3JbnTRNH': ['351'],
            'bR5C6qxz4cJkmeEPM': ['263'],
            'bWiNrqwkfoTA8utCE': ['689'],
            'bWkGuofFXaWH4f7YQ': ['1-671'],
            'bggYGoxkgJ4Pwyk5x': ['371'],
            'bgifgB4fASead7883': ['595'],
            'bnE2Wky2jGaDycaQS': ['379'],
            'by8qhYdZNybaMYtH2': ['62'],
            'c5hng4S5XJJS6H37y': ['265'],
            'cDAH53Mgkq9xoG9ie': ['49'],
            'cEHJ3Z22XD84LKcD7': ['1-784'],
            'cEwpybSGeavXim7w7': ['82'],
            'cdJNyR3HZ4ATyeCk2': ['91'],
            'd6Tbr9DdAJE4jzmi9': ['692'],
            'd9SamaYBBmsZ7PXzR': ['31'],
            'dXgJCjMDRgrYoECWF': ['36'],
            'e78jKCZpw8nibkHu4': ['687'],
            'e8EZineiT4zFbBFwF': ['1-242'],
            'eb6BNFCzKA7gfKugY': ['222'],
            'edbh9gL32ZWpnDJws': ['679'],
            'em7omw5FCGaZDpd8N': ['45'],
            'eu6axwWf9TYJ5imow': ['678'],
            'fCWa5pJP2o8yncXNg': ['216'],
            'fHaGEw9KwZqGiPYTQ': ['20'],
            'gAmFemgyEEJSPZr6A': ['965'],
            'gGYjPoxWNbCq8JAEQ': ['850'],
            'gQx2oFxbsWtL83Sq5': ['211'],
            'h5suDRwwgvkWteBbS': ['683'],
            'hBBfNkLYFQNYPoutt': ['238'],
            'hJoCoqsBXbEMba8HQ': ['1-649'],
            'hMfB79EssFQhfiGek': ['377'],
            'hPxCHuRnaHL42cCeB': ['44-1481'],
            'haQtqNxCMiQKK6zS3': ['57'],
            'iGoWQBcPNeEsxnwzG': ['995'],
            'izjzHNTuqtqexsEqc': ['262'],
            'jCDpYECkb9bW5qAA4': ['267'],
            'jj9NHmGwaxrjfGP7c': ['357'],
            'k49jHLWboAhWfi34P': ['676'],
            'k5iMSkMmi6fcWs79T': ['1-284'],
            'k74b843La82gSGzeg': ['249'],
            'kDizJ9DAxG4jgctdZ': ['242'],
            'kW9SrSgbQDhYn3k2b': ['972'],
            'ke7ewtAzq2g8JapXT': ['355'],
            'kqLodrprwHSmDKvgh': ['590'],
            'mBuuwcgvEYzhr4wq6': ['680'],
            'mEdBRXFWRaamXykuM': ['1-809', '1-829', '1-849'],
            'mYrk4k6axYsLPBYoG': ['256'],
            'mxyKPP4iNXBNiG4Le': ['46'],
            'myqAPajtcddTrzLRA': ['685'],
            'nDbKgdgu9b2WHZeee': ['376'],
            'nZSf4ixvujmkAP4B5': ['39'],
            'nbzpW5hecRpDrkuD7': ['47'],
            'ne6kCXass3QnGnx8H': ['253'],
            'nmkuHeJfsbcCtpFdn': ['61'],
            'nn4w797hspugWGejs': ['257'],
            'nsDzwXpEcEqgc7xiw': ['228'],
            'nx3hBZwL8QLEJc4KA': ['992'],
            'o5uNN4Lmu9bNznjJh': ['225'],
            'oCSv75wLvT7CMvN7i': ['420'],
            'op2vfFKTosEb7uDpa': ['237'],
            'p7Lw7QNmfCpfaF9Dv': ['258'],
            'p7SyvKmpcb92MbtrS': ['43'],
            'pECienCDvL7fK8YyB': ['674'],
            'pNFuNXdyzm4DHHwLa': ['252'],
            'pPQextwrukQW7DmYj': ['221'],
            'pqQJqbGG3WBYqBiiP': ['1-268'],
            'qdPxiH6KhR3SdGBj8': ['1-868'],
            'qh9ZZRdb9kRtbaEqC': ['593'],
            'qiQt4Smyt4EdFGPzm': ['7'],
            'qzAceELdgQo7CBF2p': ['691'],
            'r7keZzrupAhLTbxMk': ['966'],
            'rJB6wPYLXbZwYYjx8': ['66'],
            'rQKWacEojXiDZ7Dfr': ['63'],
            'rnsYF4Nf9zkjioDoA': ['686'],
            'ruoXo5jSmgdEupJcQ': ['236'],
            'tkWjspKLAwKq32RAw': ['64'],
            'trtNoYQtC8GJsDCCk': ['975'],
            'twgM8QcA9BgoKpNHW': ['1-758'],
            'umsuvCgr67FpY2hQm': ['1-767'],
            'usZmC5oz7KfSq64So': ['33'],
            'vR495JCRQtHjnvCEt': ['235'],
            'vRLLjqbTreddnfMz2': ['243'],
            'vdcDsnCaSMY6RkoRL': ['212'],
            'viLkH7WodJiGDNTsQ': ['32'],
            'vyiAtnTJ35iQaaYc9': ['1-684'],
            'wHxHjSyzLbePQoNfA': ['81'],
            'wWTwEg2XnCC3XN9EA': ['44-1624'],
            'wk5Ft64d7g69pS7y8': ['358'],
            'wx9m8Q6G2pEBdFbgq': ['1-787', '1-939'],
            'xphLGC76aB6A6c5Nz': ['598'],
            'xsmtXF38H35z3f6Wa': ['232'],
            'xvs64ceKM9rXdbxyz': ['234'],
            'yLfsJxsGAe49SvFWa': ['224'],
            'ygqgYMbkkfwggAxFr': ['970'],
            'ynzPNXDx6PXmmTbPy': ['856'],
            'yvQx3TK8BbCTXnNfc': ['230'],
            'z3oebZM3BWrN62sCC': ['967'],
            'zAwT8MGveRexhYGxN': ['1-246'],
            'zDuxv3XRsPCXCuyx6': ['226'],
            'zDwP6QeEizGjKoywq': ['227'],
            'zT3zmSE9M4s2hRYBm': ['976'],
            'zWbxTh7TTsQDZCNte': ['380'],
            'zd4hQWo2oyLvwktFR': ['372'],
            'zfbGLuScYy8kGbi5g': ['996'],
            'zyeMi4hrWeeA7S7x7': ['1-670']
        };
        _.each(telCodes, (telCode, countryId) => {
            Countries.update({
                _id: countryId
            }, {
                $set: {
                    telCodes: telCode
                }
            });
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 28,
    name: 'Add readOn for all old messages that were sent',
    up: function () {
        Meteor.setTimeout(function () {
            ActivityLogs.update({
                type: 'email',
                travellerId: {
                    $exists: true
                }
            }, {
                $set: {
                    readOn: new Date()
                }
            }, {
                multi: true
            });
        }, 2000);
    },
    down: function () {
    }
});

Meteor.methods({
    migrate_updatedReceiptPartnershipStatus () {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return updatedReceiptPartnershipStatus();
    }
});
let updatedReceiptPartnershipStatus = function () {
    Receipts.find({
        partnershipStatus: {
            $exists: false
        }
    }, {
        fields: {
            _id: 1,
            shopId: 1,
            partnershipStatus: 1
        }
    }).forEach((receipt) => {
        let entity = Shops.findOne({_id: receipt.shopId}, {fields: {partnershipStatus: 1}}) || {};

        if (receipt.partnershipStatus !== entity.partnershipStatus) {
            Receipts.update({
                _id: receipt._id
            }, {
                $set: {
                    partnershipStatus: entity.partnershipStatus || 'new'
                }
            });
        }
    });
};
Migrations.add({
    version: 29,
    name: 'Update partnerShip status of invoices',
    up: function () {
        Meteor.setTimeout(function () {
            updatedReceiptPartnershipStatus();
        }, 2000);
    },
    down: function () {
    }

});

Migrations.add({
    version: 30,
    name: 'Update transactionId of payments',
    up: function () {
        Meteor.setTimeout(function () {
            Payments.find({}).forEach((payment) => {
                const transactionId = SHA256(payment.transactionId + payment.description);
                Payments.direct.update({
                    _id: payment._id
                }, {
                    $set: {
                        transactionId: transactionId
                    }
                });
            });
        }, 2000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 31,
    name: 'Migrate contacts to users',
    up: function () {
        Meteor.setTimeout(function () {
            const ContactsCollection = new Meteor.Collection('contacts');
            let bulk = Meteor.users.rawCollection().initializeOrderedBulkOp();
            let count = 0;
            ContactsCollection.find({}).forEach((contact) => {
                let id = contact._id;
                let newUser = Meteor.users.simpleSchema().clean({
                    profile: contact,
                    private: {},
                    roles: {
                        '__global_roles__': [
                            'contact'
                        ]
                    },
                    createdAt: contact.createdAt,
                    createdBy: contact.createdBy,
                    updatedAt: contact.updatedAt,
                    updatedBy: contact.updatedBy
                });
                newUser._id = id;
                newUser.textSearch = Meteor.users.getTextSearch(newUser);
                bulk.insert(newUser);
                if (count % 1000 === 0) {
                    bulk.execute();
                    bulk = Meteor.users.rawCollection().initializeOrderedBulkOp();
                }
                count++;
            });
            bulk.execute(); // for remainder
        }, 2000);
    },
    down: function () {
        Meteor.users.remove({
            'roles.__global_roles__': 'contact'
        });
    }
});

Migrations.add({
    version: 32,
    name: 'Add travellerCountryId to receipts',
    up: function () {
        Meteor.setTimeout(function () {
            Meteor.users.find({
                'profile.countryId': {
                    $exists: true
                }
            }, {
                fields: {
                    'profile.countryId': 1
                }
            }).forEach((user) => {
                if (user.profile.countryId) {
                    Receipts.direct.update({
                        userId: user._id
                    }, {
                        $set: {
                            travellerCountryId: user.profile.countryId
                        }
                    }, {
                        multiple: true
                    });
                }
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 33,
    name: 'Rename all file ids to itemId',
    up: function () {
        Meteor.setTimeout(function () {
            let pipeline = [
                {
                    $addFields: {
                        itemId: {
                            $concat: [
                                {$ifNull: ['$userId', '']},
                                {$ifNull: ['$shopId', '']},
                                {$ifNull: ['$companyId', '']},
                                {$ifNull: ['$receiptId', '']},
                                {$ifNull: ['$invoiceId', '']},
                                {$ifNull: ['$paymentId', '']},
                                {$ifNull: ['$payoutId', '']},
                                {$ifNull: ['$contactId', '']},
                                {$ifNull: ['$affiliateId', '']},
                                {$ifNull: ['$activityId', '']}
                            ]
                        }
                    }
                },
                {
                    $out: 'cfs.vatfree-files.filerecord'
                }
            ];
            Files.files.rawCollection().aggregate(pipeline).toArray();
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 34,
    name: 'Create receipt rejection text search',
    up: function () {
        Meteor.setTimeout(function () {
            ReceiptRejections.find({
                textSearch: {
                    $exists: false
                }
            }).forEach((doc) => {
                ReceiptRejections.updateReceiptTextSearch(doc);
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 35,
    name: 'Remove old locks index',
    up: function () {
        Locks._dropIndex({'updatedAt': -1});
    },
    down: function () {
    }
});

Migrations.add({
    version: 36,
    name: 'Add textSearch to all files',
    up: function () {
        Meteor.setTimeout(function () {
            let pipeline = [
                {
                    $addFields: {
                        textSearch: {
                            $toLower: {
                                $concat: [
                                    {$ifNull: ['$original.name', '']},
                                    ' ',
                                    {$ifNull: ['$original.type', '']},
                                    ' ',
                                    {$ifNull: ['$itemId', '']},
                                    ' ',
                                    {$ifNull: ['$target', '']},
                                    ' ',
                                    {$ifNull: ['$subType', '']},
                                    ' ',
                                    {$ifNull: ['$category', '']},
                                    ' ',
                                    {$ifNull: ['$node', '']},
                                ]
                            },
                        }
                    }
                },
                {
                    $out: 'cfs.vatfree-files.filerecord'
                }
            ];
            Files.files.rawCollection().aggregate(pipeline).toArray();
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 37,
    name: 'Update payment difference of invoices',
    up: function () {
        Meteor.setTimeout(function () {
            Invoices.find({}).forEach((invoice) => {
                if (invoice.status === 'paymentDifference' || invoice.status === 'partiallyPaid') {
                    let payments = checkTotalPayments(invoice);
                    Invoices.update({
                        _id: invoice._id
                    }, {
                        $set: {
                            paymentDifference: (payments.totalPaid - invoice.amount)
                        }
                    });
                }
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 38,
    name: 'Fix billingCompanyId in receipts - after salesforce import',
    up: function () {
        Meteor.setTimeout(() => {
            updateBillingCompanyIds();
        }, 1000);
    },
    down: function () {
    }
});

const updateShopStatistics = function() {
    Shops.find({
        deleted: {
            $exists: false
        }
    }, {
        fields: {
            _id: 1
        }
    }).forEach((shop) => {
        Receipts.updateShopReceiptStatistics(shop._id);
    });
};
Meteor.methods({
    migrate_updateShopStatistics() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return updateShopStatistics();
    }
});
Migrations.add({
    version: 39,
    name: 'Update shop statistics',
    up: function () {
        Meteor.setTimeout(() => {
            updateShopStatistics();
        }, 1000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 40,
    name: 'Update travellerCountryId to receipts',
    up: function () {
        Meteor.setTimeout(function () {
            Meteor.users.find({
            }, {
                fields: {
                    'profile.addressCountryId': 1,
                    'profile.visaCountryId': 1,
                    'profile.countryId': 1
                }
            }).forEach((user) => {
                if (user.profile.countryId) {
                    const travellerCountryId = user.profile.addressCountryId || user.profile.visaCountryId || user.profile.countryId;
                    Receipts.direct.update({
                        userId: user._id
                    }, {
                        $set: {
                            travellerCountryId: travellerCountryId
                        }
                    }, {
                        multi: true
                    });
                }
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 41,
    name: 'Migrate all uncooperative shops/companies to correct template',
    up: function () {
        Meteor.setTimeout(function () {
            const textId = Meteor.settings.defaults.uncooperativeBillingEmailTextId;
            if (textId) {
                Shops.direct.update({
                    partnershipStatus: 'uncooperative'
                }, {
                    $set: {
                        billingEmailTextId: textId
                    }
                }, {
                    multi: true
                });
                Companies.direct.update({
                    partnershipStatus: 'uncooperative'
                }, {
                    $set: {
                        billingEmailTextId: textId
                    }
                }, {
                    multi: true
                });
            }
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 42,
    name: 'Update textSearch of all users with a secondary email address',
    up: function () {
        Meteor.setTimeout(function () {
            Meteor.users.find({
                emails: {
                    $exists: true
                }
            }, {
                fields: {
                    profile: 1,
                    private: 1,
                    emails: 1
                }
            }).forEach((user) => {
                if (user.emails && user.emails.length > 1) {
                    Meteor.users.direct.update({
                        _id: user._id
                    },{
                        $set: {
                            textSearch: Meteor.users.getTextSearch(user)
                        }
                    });
                }
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 43,
    name: 'Upgrade FQAs to use separate collection for groups',
    up: function () {
        const groups = {};
        FAQs.find({}).forEach((faq) => {
            groups[faq.group] = 1;
        });

        _.each(groups, (count, groupName) => {
            groups[groupName] = FAQGroups.insert({
                name: {
                    en: groupName
                }
            });
        });

        _.each(groups, (id, groupName) => {
            FAQs.direct.update({
                group: groupName
            },{
                $set: {
                    groupId: id
                }
            }, {
                multi: 1
            });
        });
    },
    down: function () {
    }
});

Migrations.add({
    version: 44,
    name: 'Migrate affiliates to users',
    up: function () {
        const AffiliatesCollection = new Meteor.Collection('affiliates');
        let bulk = Meteor.users.rawCollection().initializeOrderedBulkOp();
        let count = 0;
        AffiliatesCollection.find({}).forEach((affiliate) => {
            let id = affiliate._id;
            const profileObj = _.clone(affiliate);

            const affiliateObj = {
                code: affiliate.code,
                status: 'new'
            };
            if (affiliate.serviceFee) {
                affiliateObj.serviceFee = affiliate.serviceFee;
            }
            if (affiliate.travellerDiscount) {
                affiliateObj.travellerDiscount = affiliate.travellerDiscount;
            }

            let newUser = Meteor.users.simpleSchema().clean({
                profile: profileObj,
                private: {},
                affiliate: affiliateObj,
                roles: {
                    '__global_roles__': [
                        'affiliate'
                    ]
                },
                createdAt: affiliate.createdAt,
                createdBy: affiliate.createdBy,
                updatedAt: affiliate.updatedAt,
                updatedBy: affiliate.updatedBy
            });
            newUser._id = id;
            newUser.textSearch = Meteor.users.getTextSearch(newUser);
            bulk.insert(newUser);
            if (count % 1000 === 0) {
                bulk.execute();
                bulk = Meteor.users.rawCollection().initializeOrderedBulkOp();
            }
            count++;
        });
        bulk.execute(); // for remainder
    },
    down: function () {
        Meteor.users.remove({
            'roles.__global_roles__': 'affiliate'
        });
    }
});

Migrations.add({
    version: 45,
    name: 'Migrate payments to include accountNr field',
    up: function () {
        Meteor.setTimeout(() => {
            Payments.find({}).forEach((payment) => {
                Payments.update({
                    _id: payment._id
                },{
                    $set: {
                        accountNr: getIBAN(payment.description)
                    }
                });
            });
        }, 1000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 46,
    name: 'Migrate payments to include aggregation dates for value',
    up: function () {
        Meteor.setTimeout(() => {
            Payments.find({}).forEach((payment) => {
                Payments.direct.update({
                    _id: payment._id
                },{
                    $set: {
                        valueWeek: Number(moment(payment.valueDate).format('GGGGWW')),
                        valueMonth: Number(moment(payment.valueDate).format('MM')),
                        valueYear: Number(moment(payment.valueDate).format('YYYY'))
                    }
                });
            });
        }, 1000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 47,
    name: 'Migrate all poprIds in shops to array of strings',
    up: function () {
        Meteor.setTimeout(() => {
            Shops.find({
                poprId: {
                    $exists: true
                }
            }).forEach((shop) => {
                Shops.update({
                    _id: shop._id
                },{
                    $set: {
                        poprId: [shop.poprId]
                    }
                });
            });
        }, 1000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 48,
    name: 'Get historical exchange rates',
    up: function () {
        const url = "https://api.exchangeratesapi.io/history?start_at=2017-01-01&end_at=" + moment().format('YYYY-MM-DD');
        const rateRequest = HTTP.get(url);
        if (rateRequest && rateRequest.data && rateRequest.data.rates) {
            let bulk = ExchangeRates.rawCollection().initializeOrderedBulkOp();
            _.each(rateRequest.data.rates, (rates, date) => {
                const insertRate = {
                    _id: Random.id(),
                    date: date,
                    rates: rates,
                    base: "EUR"
                };
                bulk.insert(insertRate);
            });
            bulk.execute();
        }
    },
    down: function () {
        ExchangeRates.remove({});
    }
});

Migrations.add({
    version: 49,
    name: 'Euro amounts for all receipts for statistics',
    up: function () {
        Meteor.setTimeout(() => {
            Receipts.find({
                currencyId: {
                    $ne: Meteor.settings.defaults.currencyId
                }
            },{
                fields: {
                    _id: 1,
                    purchaseDate: 1,
                    currencyId: 1,
                    amount: 1,
                    totalVat: 1,
                    refund: 1,
                    serviceFee: 1,
                    affiliateServiceFee: 1,
                    deferredServiceFee: 1
                }
            }).forEach((receipt) => {
                const date = moment(receipt.purchaseDate).format('YYYY-MM-DD');
                const values = {
                    amount: receipt.amount,
                    totalVat: receipt.totalVat,
                    refund: receipt.refund,
                    serviceFee: receipt.serviceFee,
                    affiliateServiceFee: receipt.affiliateServiceFee,
                    deferredServiceFee: receipt.deferredServiceFee
                };
                const exchangedValues = Vatfree.exchangeRates.getEuroValuesById(date, values, receipt.currencyId);
                Receipts.direct.update({
                    _id: receipt._id
                },{
                    $set: {
                        euroAmounts: pruneEmpty(exchangedValues)
                    }
                });
            });
        }, 1000);
    },
    down: function () {
        Receipts.direct.update({},{
            $unset: {
                euroAmounts: 1
            }
        },{
            multi: true
        });
    }
});

Migrations.add({
    version: 50,
    name: 'Euro amounts for all receipts for statistics',
    up: function () {
        Meteor.setTimeout(() => {
            Receipts.find({
                euroAmounts: {
                    $exists: false
                }
            },{
                fields: {
                    _id: 1,
                    purchaseDate: 1,
                    currencyId: 1,
                    amount: 1,
                    totalVat: 1,
                    refund: 1,
                    serviceFee: 1,
                    affiliateServiceFee: 1,
                    deferredServiceFee: 1
                }
            }).forEach((receipt) => {
                const values = {
                    amount: receipt.amount || null,
                    totalVat: receipt.totalVat || null,
                    refund: receipt.refund || null,
                    serviceFee: receipt.serviceFee || null,
                    affiliateServiceFee: receipt.affiliateServiceFee || null,
                    deferredServiceFee: receipt.deferredServiceFee || null
                };
                Receipts.direct.update({
                    _id: receipt._id
                },{
                    $set: {
                        euroAmounts: pruneEmpty(values)
                    }
                });
            });
        }, 1000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 51,
    name: 'Euro amounts for all invoices for statistics',
    up: function () {
        Meteor.setTimeout(() => {
            Invoices.find({
                euroAmounts: {
                    $exists: false
                }
            },{
                fields: {
                    _id: 1,
                    invoiceDate: 1,
                    currencyId: 1,
                    amount: 1,
                    totalVat: 1,
                    refund: 1,
                    serviceFee: 1,
                    affiliateServiceFee: 1,
                    deferredServiceFee: 1
                }
            }).forEach((invoice) => {
                let values = {
                    amount: invoice.amount || null,
                    totalVat: invoice.totalVat || null,
                    refund: invoice.refund || null,
                    serviceFee: invoice.serviceFee || null,
                    affiliateServiceFee: invoice.affiliateServiceFee || null,
                    deferredServiceFee: invoice.deferredServiceFee || null
                };
                if (invoice.currencyId !== Meteor.settings.defaults.currencyId) {
                    const date = moment(invoice.invoiceDate).format('YYYY-MM-DD');
                    values = Vatfree.exchangeRates.getEuroValuesById(date, values, invoice.currencyId);
                }
                Invoices.direct.update({
                    _id: invoice._id
                },{
                    $set: {
                        euroAmounts: pruneEmpty(values)
                    }
                });
            });
        }, 1000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 52,
    name: 'Update WebTexts text search index',
    up: function () {
        Meteor.setTimeout(function () {
            WebTexts.find({}).forEach((webText) => {
                WebTexts.updateTextSearch(webText);
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 53,
    name: 'Update EmailTexts text search index',
    up: function () {
        Meteor.setTimeout(function () {
            EmailTexts.find({}).forEach((emailText) => {
                EmailTexts.updateTextSearch(emailText);
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 54,
    name: 'Fix too long activity log text index',
    up: function () {
        Meteor.setTimeout(function () {
            ActivityLogs.find({},{
                fields: {
                    _id: 1,
                    subject: 1,
                    notes: 1,
                    textSearch: 1
                }
            }).forEach((actvityLog) => {
                if (actvityLog.textSearch.length > 1024) {
                    let t = (actvityLog.subject + ' ' + actvityLog.subject.notes).toLowerCase().latinize();
                    t = _.uniq(t.replace(/\s+/g, ' ').replace(/(https?:\/\/[^ "]+)/g, '').split(' ')).join(' ').substr(0, 1024);
                    console.log('Updating activity log', actvityLog._id);
                    ActivityLogs.direct.update({
                        _id: actvityLog._id
                    },{
                        $set: {
                            textSearch: t
                        }
                    });
                }
            });
            try {
                ActivityLogs._ensureIndex({'textSearch': 1});
            } catch(e) {
                console.error(e);
            }
        }, 1000);
    },
    down: function () {
    }
});

Migrations.add({
    version: 55,
    name: 'Update status of Companies',
    up: function () {
        Meteor.setTimeout(function () {
            Companies.direct.update({},{
                $set: {
                    status: 'active'
                }
            },{
                multi: true
            });
        }, 10);
    },
    down: function () {
    }
});

Migrations.add({
    version: 56,
    name: 'Add paidAt and payoutAt dates to non salesforce receipts',
    up: function () {
        Meteor.setTimeout(() => {
            Receipts.find({
                salesforceId: {
                    $exists: false
                }
            }, {
                fields: {
                    status: 1,
                    statusDates: 1
                }
            }).forEach((receipt) => {
                const setData = {};

                if (receipt.statusDates.userPendingForPayment) {
                    setData.paidAt = receipt.statusDates.userPendingForPayment;
                } else if (receipt.statusDates.paidByShop) {
                    setData.paidAt = receipt.statusDates.paidByShop;
                }

                if (receipt.statusDates.paid) {
                    setData.payoutAt = receipt.statusDates.paid;
                }

                if (_.size(setData) > 0) {
                    Receipts.direct.update({
                        _id: receipt._id
                    }, {
                        $set: setData
                    });
                }
            });
        }, 3000);
    },
    down: function () {
    }
});

Meteor.startup(function () {
    if (Meteor.settings.migrate === true) {
        Migrations.migrateTo('56');
    }
});

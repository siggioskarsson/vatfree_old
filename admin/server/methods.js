import { RedisOplog } from 'meteor/cultofcoders:redis-oplog';
import moment from 'moment';

Meteor.methods({
    getRedisStats() {
        // works only server-side
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return RedisOplog.stats()
    },
    restoreDeletedFiles(target) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        if (!target) {
            throw new Meteor.Error(500, 'Target missing');
        }
        Meteor.defer(() => {
            let selector = {
                "activity.verb": "delete",
                "activity.target.target": "files",
                "activity.object.target": target, // receipts / travellers
                date: {
                    $gt: new Date('2018-12-11'),
                    $lt: new Date('2018-12-13')
                }
            };
            ActivityStream.collection.find(selector).forEach((a) => {
                let fileObj = a.activity.object;
                if (!fileObj.deleted && fileObj.copies && !Files.findOne({_id: fileObj._id})) {
                    // restore file
                    fileObj.restored = new Date();
                    Files.files.direct.insert(fileObj);
                }
            });
        });
    },
    checkDuration() {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        return checkDuration();
    }
});

const checkDuration = function() {
    const months = 1;
    Receipts.find({
        salesforceId: {
            $exists: false
        },
        createdAt: {
            $gte: moment().subtract(months + 3, 'months').startOf('month').toDate(),
        },
        'statusDates.paidByShop': {
            $gte: moment().startOf('month').subtract(months, 'months').toDate()
        },
        'statusDates.userPending': {
            $exists: false
        },
        'statusDates.notCollectable': {
            $exists: false
        },
        partnershipStatus: 'partner'
    }).forEach((receipt) => {
        // durationPost: { $avg: {$subtract: [ { $ifNull: ['$originalsCheckedAt', null] }, { $ifNull: ['$createdAt', null] } ] } },
        console.log(receipt.receiptNr, receipt.createdAt, receipt.originalsCheckedAt, moment(receipt.originalsCheckedAt).diff(moment(receipt.createdAt), 'days'));
    });
};

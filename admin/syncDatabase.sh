#!/usr/bin/env bash

echo "Fill in the mlab password, followed by [ENTER]:"
read -s password

COLLECTION=""
if [[ -n $1 ]]; then
    COLLECTION="-c $1"
fi

mongodump -u heroku_7vrc112b -p $password -h ds145542-a0.mlab.com:45542 -d "heroku_7vrc112b" --out=../heroku_7vrc112b/ --excludeCollectionsWithPrefix=activity ${COLLECTION}
mongorestore --drop -h localhost:6002 -d "meteor" ../heroku_7vrc112b/heroku_7vrc112b/
rm -rf ../heroku_7vrc112b/

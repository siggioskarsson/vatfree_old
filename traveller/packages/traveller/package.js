Package.describe({
    name: 'traveller',
    summary: 'Traveller import package',
    version: '0.0.2',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'mongo',
        'ecmascript',
        'templating',
        'underscore',
        'aldeed:collection2@2.10.0',
        'aldeed:simple-schema@1.5.3',
        'dburles:google-maps@1.1.5',
        'flemay:less-autoprefixer@1.2.0',
        'cfs:standard-packages@0.5.9',
        'cfs:gridfs@0.0.33',
        'cfs:ui@0.1.3',
        'cfs:s3@0.1.3',
        'cfs:graphicsmagick@0.0.18',
        'vatfree:core',
        'vatfree:core-templates',
        'vatfree:countries',
        'vatfree:currencies'
    ]);

    api.use([
        'vatfree:receipts',
        'vatfree:receipt-types',
        'vatfree:receipt-rejections',
        'vatfree:invoices',
        'vatfree:shops',
        'vatfree:affiliates',
        'vatfree:companies',
        'vatfree:contacts',
        'vatfree:travellers',
        'vatfree:traveller-types',
        'vatfree:traveller-rejections',
        'vatfree:activity-logs',
        'vatfree:notify',
        'vatfree:email-templates',
        'vatfree:email-texts',
        'vatfree:web-texts',
        'vatfree:payouts',
        'vatfree:faqs'
    ], 'server');

    // shared files
    api.addFiles([
    ]);

    // server files
    api.addFiles([
    ], 'server');

    // client files
    api.addFiles([
    ], 'client');

    api.export([
        'Vatfree',
        'Receipts',
        'ReceiptTypes',
        'ReceiptRejections',
        'Affiliates',
        'Travellers',
        'TravellerTypes',
        'TravellerRejections',
        'Countries',
        'Currencies',
        'Companies',
        'Contacts',
        'EmailTexts',
        'EmailTemplates',
        'WebTexts',
        'ActivityLogs',
        'Payouts',
        'Shops',
        'ShopSearch',
        'FAQs',
        'FAQGroups',
        'FS',
        'Files',
        'GoogleMaps'
    ]);
});

FlowRouter.notFound = {
    action: function() {
        FlowRouter.go('/');
    }
};

FlowRouter.route('/', {
    name: "vatfree.com",
    action: function() {
        BlazeLayout.render("mainLayout", {content: "reroute"});
    }
});

FlowRouter.route('/receipts', {
    name: "Receipts",
    action: function() {
        import("/imports/ui/pages/receipts")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "receipts"});
            });
    }
});

FlowRouter.route('/payouts', {
    name: "Payouts",
    action: function() {
        import("/imports/ui/pages/payouts")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "payouts"});
            });
    }
});

FlowRouter.route('/receipt/add', {
    name: "Add receipt",
    action: function() {
        import("/imports/ui/pages/receipt-add")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "receipt_add"});
            });
    }
});


FlowRouter.route('/receipt/cargo', {
    name: "Add cargo receipt",
    action: function() {
        import("/imports/ui/pages/receipt-add")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "receipt_cargo"});
            });
    }
});

FlowRouter.route('/profile', {
    name: "Profile",
    action: function() {
        import("/imports/ui/pages/profile")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "profile"});
            });
    },
    triggersExit: [
        function() {
            try {
                const blazeView = Blaze.getView($('.container.profile > .row')[0]);
                if (blazeView && blazeView.parentView && blazeView.parentView.templateInstance) {
                    let template = blazeView.parentView.templateInstance();
                    if (template.changesMade.get() && confirm("You have unsaved changes. Save now?")) {
                        let formData = Vatfree.templateHelpers.getFormData(template);
                        Meteor.call('save-my-user-profile', formData.profile, TAPi18n.getLanguage(), (err, result) => {
                            if (err) {
                                toastr.error(err.reason || err.message);
                            } else {
                                template.changesMade.set();
                                toastr.success('Profile saved');
                            }
                        });
                    }
                }
            } catch(e) {
                console.log(e);
            }
        }
    ]
});

FlowRouter.route('/splash', {
    name: "Splash",
    action: function() {
        BlazeLayout.render("publicLayout", {content: "splash"});
    }
});

FlowRouter.route('/how', {
    name: "How does it work",
    action: function() {
        BlazeLayout.render("mainLayout", {content: "how"});
    }
});

FlowRouter.route('/shops', {
    name: "Shopping guide",
    action: function() {
        import("/imports/ui/pages/shops")
            .then(() => {
                BlazeLayout.render("mainLayout", {content: "shops"});
            });
    }
});

FlowRouter.route('/shops/embed', {
    name: "Shopping guide - full screen",
    action: function() {
        import("/imports/ui/pages/shops")
            .then(() => {
                BlazeLayout.render("embedLayout", {content: "shopsEmbed"});
            });
    }
});

FlowRouter.route('/faqs', {
    name: "FAQs",
    action: function() {
        import("/imports/ui/pages/faqs")
            .then(() => {
                BlazeLayout.render("publicLayout", {content: "faqs"});
            });
    }
});

FlowRouter.route('/faq/:_id/:_title', {
    name: "FAQ detail",
    action: function() {
        import("/imports/ui/pages/faqs")
            .then(() => {
                BlazeLayout.render("publicLayout", {content: "faq"});
            });
    }
});

FlowRouter.route('/faq/embed/:_id/:_title', {
    name: "FAQ detail",
    action: function() {
        import("/imports/ui/pages/faqs")
            .then(() => {
                BlazeLayout.render("embedLayoutContainer", {content: "faq"});
            });
    }
});

FlowRouter.route('/faqs/embed', {
    name: "FAQ - full screen",
    action: function() {
        import("/imports/ui/pages/faqs")
            .then(() => {
                BlazeLayout.render("embedLayoutContainer", {content: "faqsEmbed"});
            });
    }
});

FlowRouter.route('/a/:affiliateCode', {
    name: "Affiliate linking",
    action: function() {
        import("/imports/ui/pages/affiliate")
            .then(() => {
                BlazeLayout.render("publicLayout", {content: "affiliate"});
            });
    }
});

FlowRouter.route('/p/:poprId/:poprSecret', {
    name: "POPR receipt scanned",
    action: function() {
        import("/imports/ui/pages/popr")
            .then(() => {
                BlazeLayout.render("publicLayout", {content: "popr"});
            });
    }
});

FlowRouter.route('/login/:selector/:code', {
    action: function(params, queryParams) {
        var options = {
            code: params.code,
            selector: params.selector
        };

        Meteor.logout(function(err, result) {
            Meteor.loginWithPasswordless(options, function (error, result) {
                if (error) {
                    console.log(error);
                    toastr.error(error.reason || error.error);
                } else {
                    if (dataLayer) {
                        dataLayer.push({
                            'event':'loggedin',
                            'UID': Meteor.userId(),
                            'userType':'traveller',
                        });
                    }
                }
                console.log('got to page', '/' + (queryParams && queryParams.page ? queryParams.page : ''));
                FlowRouter.go('/' + (queryParams && queryParams.page ? queryParams.page : ''));
            });
        });
    }
});

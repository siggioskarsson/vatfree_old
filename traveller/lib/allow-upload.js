Vatfree.allowTravellerUpload = function (receipt) {
    let receiptType = ReceiptTypes.findOne({_id: receipt.receiptTypeId}) || {};
    let isNato = receiptType.name === "NATO";
    let isCargo = receiptType.name === "Cargo";

    let allowWebReceiptUpload = (_.contains(['web', 'mobile'], receipt.source) && (_.contains([
        'userPending',
        'shopPending',
        'waitingForOriginal',
        'waitingForCustomsStamp'
    ], receipt.status)));

    let allowPoprReceiptUpload = (receipt.poprId && (_.contains([
        'userPending',
        'shopPending',
        'waitingForOriginal',
        'visualValidationPending',
        'waitingForCustomsStamp'
    ], receipt.status)));

    let allowNatoReceiptUpload = (isNato && (_.contains([
        'userPending',
        'shopPending',
        'waitingForOriginal',
        'visualValidationPending',
        'waitingForCustomsStamp'
    ], receipt.status)));

    let allowCargoReceiptUpload = (isCargo && (_.contains([
        'userPending',
        'shopPending',
        'waitingForOriginal',
        'visualValidationPending',
        'waitingForCustomsStamp'
    ], receipt.status)));

    return allowWebReceiptUpload || allowPoprReceiptUpload || allowNatoReceiptUpload || allowCargoReceiptUpload;
};

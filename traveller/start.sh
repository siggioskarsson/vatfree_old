#!/usr/bin/env bash
meteor npm install --no-optional
MONGO_URL=mongodb://localhost:6002/meteor ROOT_URL=http://local.vatfree.com:6011/ meteor --port 6011 --settings settings.json

const fixUrl = function(url) {
    // check whether we are in the app and change url to traveller site
    if (Meteor.settings.travellerSiteUrl) {
        url = url.replace(/(http?:\/\/[^\/]+)\//, Meteor.settings.travellerSiteUrl);
    }

    return url;
};

Accounts.emailTemplates.from = 'Vatfree.com support <support@vatfree.com>';

Accounts.emailTemplates.verifyEmail = {
    subject(user) {
        let emailText = EmailTexts.findOne({ _id: Meteor.settings.defaults.verifyNewEmailAddressId });
        if (emailText && emailText.subject && emailText.subject[user.profile.language]) {
            return emailText.subject[user.profile.language];
        } else {
            return "Verify your new email address on vatfree.com";
        }
    },
    text(user, url) {
        url = fixUrl(url);
        let language = user.profile.language || "en";
        let emailText = EmailTexts.findOne({ _id: Meteor.settings.defaults.verifyNewEmailAddressId });
        let emailTemplate = EmailTemplates.findOne({ _id: Meteor.settings.defaults.emailTemplateId });

        if (emailText && emailText.subject && emailText.text[language] && emailTemplate && emailTemplate.templateText) {
            import handlebars from 'handlebars';
            const templateData = {
                url: url,
                user: user
            };
            let emailTextRendered = Vatfree.notify.stripHtmlTags(handlebars.compile(emailText.text[language])(templateData));
            console.log('emailTextRendered', emailTextRendered);

            const htmlTemplateData = {
                text: emailTextRendered,
            };
            return handlebars.compile(emailTemplate.templateText)(htmlTemplateData);
        } else {
            return `Hello ${user.profile.name},

To verify your account email, simply click the link below.

${url}

Thanks.`;
        }
    },
    html(user, url) {
        url = fixUrl(url);
        let language = user.profile.language || "en";
        let emailText = EmailTexts.findOne({ _id: Meteor.settings.defaults.verifyNewEmailAddressId });
        let emailTemplate = EmailTemplates.findOne({ _id: Meteor.settings.defaults.emailTemplateId });

        if (emailText && emailText.subject && emailText.text[language] && emailTemplate && emailTemplate.templateHTML) {
            import handlebars from 'handlebars';
            const templateData = {
                url: url,
                user: user
            };
            let emailTextRendered = handlebars.compile(emailText.text[language])(templateData);

            const htmlTemplateData = {
                text: emailTextRendered,
            };
            return handlebars.compile(emailTemplate.templateHTML)(htmlTemplateData);
        } else {
            return false;
        }
    }
};

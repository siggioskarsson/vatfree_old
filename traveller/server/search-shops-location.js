Meteor.methods({
    'search-shops-location'(searchTerm, mapCenter, mapBound, limit, offset) {
        // this.unblock(); // blocked so we force a single search at a time
        check(searchTerm, String);
        check(mapBound, Object);

        let selector = {
            'g.type': 'Point',
            'g.coordinates.0': {
                $ne: 0
            }
        };
        if (mapBound.hasOwnProperty('sw') && mapBound.hasOwnProperty('ne')) {
            selector.g = {
                $geoWithin: {
                    $box: [
                        mapBound.sw,
                        mapBound.ne
                    ]
                }
            }
        }

        if (searchTerm.length) {
            Vatfree.search.addSearchTermSelector(searchTerm, selector, 't');
        }

        if (!searchTerm.length) {
            // If not using a search term, only "active" partnerships
            selector.p = {$in: [ 'partner', 'affiliate', 'pledger' ]};
        }

        let partnershipOrder = [ 'partner', 'affiliate', 'pledger', 'new', 'uncooperative' ];
        const shops = [];
        ShopSearch.find(selector, {
            limit: limit,
            offset: offset,
            sort: {
                r: -1
            }
        }).forEach((shop) => {
            if (shop.p === 'affiliate') {
                shop.p = 'partner';
            }
            shops.push({
                _id: shop._id,
                geo: shop.g,
                n: shop.n,
                a: shop.a,
                p: shop.p,
                po: partnershipOrder.indexOf(shop.p),
                r: shop.r,
            });
        });

        return shops;
    }
});

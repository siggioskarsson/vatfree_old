Meteor.publishComposite('my-receipts', function(searchTerm, sort, limit, offset) {
    let selector = {
        userId: this.userId,
        status: {
            $ne: 'deleted'
        }
    };
    if (searchTerm) {
        check(searchTerm, String);
        Vatfree.search.addSearchTermSelector(searchTerm, selector);
    }

    return {
        find: function () {
            return Receipts.find(selector, {
                sort: sort,
                limit: limit,
                offset: offset
            });
        },
        children: [
            {
                find: function (receipt) {
                    return Shops.find({
                        _id: receipt.shopId
                    });
                }
            },
            {
                find: function (receipt) {
                    return Countries.find({
                        _id: receipt.countryId
                    });
                }
            },            {
                find: function (receipt) {
                    return Currencies.find({
                        _id: receipt.currencyId
                    });
                }
            },
            {
                find: function (receipt) {
                    return ReceiptTypes.find({
                        _id: receipt.receiptTypeId
                    });
                }
            },
            {
                find: function (receipt) {
                    return ReceiptTypes.find({
                        _id: {
                            $in: receipt.receiptRejectionIds || []
                        }
                    });
                }
            },
            {
                find: function (receipt) {
                    return Files.find({
                        itemId: receipt._id,
                        target: 'receipts'
                    });
                }
            }
        ]
    };
});

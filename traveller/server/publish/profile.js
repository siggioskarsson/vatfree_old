Meteor.publishComposite('my-profile', function() {
    return {
        find: function () {
            return Meteor.users.find({
                _id: this.userId
            }, {
                fields: {
                    'private.status': 1,
                    'private.rejectionIds': 1,
                    'private.travellerTypeId': 1,
                    passwordSet: 1
                }
            });
        },
        children: [
            {
                find: function (user) {
                    // publish all countries
                    return Countries.find({});
                }
            },
            {
                find: function (user) {
                    return TravellerRejections.find({
                        _id: {
                            $in: user.private.rejectionIds || []
                        }
                    });
                }
            },
            {
                find: function (user) {
                    return Files.find({
                        itemId: user._id,
                        target: 'travellers'
                    });
                }
            }
        ]
    };
});

Meteor.publish('public-faqs', function(searchTerm, language) {
    let selector = {};

    if (searchTerm) {
        searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase().latinize();
        let searchTerms = searchTerm.split(' ');

        selector['$and'] = [];
        _.each(searchTerms, (s) => {
            selector['$and'].push({textSearch: new RegExp(s)});
        });
    }

    const fields = {
        _id: 1,
        groupId: 1,
        ['subject.' + language]: 1,
        ['text.' + language]: 1,
        createdAt: 1,
        updatedAt: 1
    };

    const groupFields = {
        _id: 1,
        order: 1,
        ['name.' + language]: 1
    };

    if (language !== 'en') {
        fields['subject.en'] = 1;
        fields['text.en'] = 1;

        groupFields['name.en'] = 1;
    }

    return [
        FAQs.find(selector, {
            fields: fields
        }),
        FAQGroups.find({}, {
            fields: groupFields
        })
    ];
});

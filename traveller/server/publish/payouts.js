Meteor.publishComposite('my-payouts', function() {
    return {
        find: function () {
            return Payouts.find({
                travellerId: this.userId
            }, {
                transform: function(doc) {
                    if (doc.paymentMethod === 'creditcard') {
                        doc.paymentNr = doc.paymentNr.replace(/\s/g, '');
                        doc.paymentNr = 'xxxx xxxx xxxx ' + doc.paymentNr.substr(12, 4);
                    }
                    if (doc.status === 'sentToApiProvider') {
                        doc.status = 'sentToPaymentProvider';
                    }
                    return doc;
                }
            });
        },
        children: [
            {
                find: function (payout) {
                    return Receipts.find({
                        _id: {
                            $in: payout.receiptIds || []
                        }
                    });
                },
                children: [
                    {
                        find: function (receipt) {
                            return Shops.find({
                                _id: receipt.shopId
                            });
                        }
                    }
                ]
            }
        ]
    };
});

import algoliasearch from 'algoliasearch';

const client = algoliasearch("X8O58AGGDO", "37f1dd509a6a7387bd032793a127eaa1");
const index = client.initIndex('prod_shops');

Meteor.startup(() => {
    return false;

    console.log('creating shops export file')
    let fs = require('fs');
    let exportFile = '/Users/oskarsson/gitcheckout/vatfree/shops.json';

    fs.truncate(exportFile, 0, function(){console.log('done')})
    fs.appendFile(exportFile, "[\n", function (err) {
        if (err) {
            console.log('Saved!');
        }
    });

    Shops.find({},{
        fields: {
            _id: 1,
            name: 1,
            addressFirst: 1,
            postCode: 1,
            city: 1
        }
    }).forEach((shop) => {
        let shopObj = {_id: shop._id, name: shop.name + ' - ' + (shop.addressFirst || "") + ', ' + (shop.postCode || "") + ' ' + (shop.city || "")};
        fs.appendFile(exportFile, JSON.stringify(shopObj) + ",\n", function (err) {
            if (err) {
                console.log('Saved!');
            }
        });
    });

    fs.appendFile(exportFile, "]\n", function (err) {
        if (err) {
            console.log('Saved!');
        }
    });

});

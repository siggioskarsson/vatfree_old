/* global Vatfree: true */
import moment from 'moment';
import { sendMagicLinkLogin } from 'meteor/vatfree:travellers/server/lib/magic-link';

let areAllDocsUploaded = function (userId, travellerType, countryId) {
    let allDocsUploaded = true;
    let documentsNeeded = Vatfree.travellers.getDocumentsNeeded(travellerType, countryId);
    _.each(documentsNeeded, (documentNeeded) => {
        let docUploaded = !!Files.findOne({
            itemId: userId,
            target: 'travellers',
            subType: documentNeeded.subType
        });
        allDocsUploaded = allDocsUploaded && (docUploaded || !documentNeeded.required);
    });
    return allDocsUploaded;
};

let checkUserStatusChange = function (user) {
    let userStatus = 'userUnverified';
    let travellerType = TravellerTypes.findOne({_id: user.private.travellerTypeId});
    if (travellerType) {
        const allDocsUploaded = areAllDocsUploaded(user._id, travellerType, user.profile.countryId);
        const ncpFilledIn = Vatfree.travellers.ncpFilledIn(user);
        if (allDocsUploaded && ncpFilledIn) {
            userStatus = 'userUnverified';
        } else {
            userStatus = 'waitingForDocuments';
        }
    } else {
        userStatus = 'userUnverified';
    }

    // no errors, update status
    Meteor.users.update({
        _id: user._id
    }, {
        $set: {
            'private.status': userStatus
        }
    });
};
Meteor.methods({
    'setUserLanguage'(language) {
        if (!this.userId) {
            throw new Meteor.Error(404, 'Not logged in');
        }

        language = language.substr(0, 2);
        if (language === 'zh') language = 'zh-CN';
        
        language = Vatfree.languages.getValidTravellerLanguage(language);

        Meteor.users.update({
            _id: this.userId
        },{
            $set: {
                'profile.language': language
            }
        });
    },
    'save-my-user-profile'(profile, language) {
        if (!this.userId) {
            throw new Meteor.Error(404, 'Not logged in');
        }

        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let user = Meteor.users.findOne({_id: this.userId});

        if (profile.passportExpirationDate) {
            if (!_.isDate(profile.passportExpirationDate)) {
                profile.passportExpirationDate = moment(profile.passportExpirationDate, TAPi18n.__('_date_format', {}, language)).toDate();
            }
        }

        if (profile.visaExpirationDate) {
            if (!_.isDate(profile.visaExpirationDate)) {
                profile.visaExpirationDate = moment(profile.visaExpirationDate, TAPi18n.__('_date_format', {}, language)).toDate();
            }
        }

        if (profile.birthDate && !_.isDate(profile.birthDate)) {
            profile.birthDate = moment(profile.birthDate, TAPi18n.__('_date_format', {}, language)).toDate();
        }

        // we curate the fields that are allowed to change
        let updateUser = {
            'profile.gender': profile.gender,
            'profile.name': profile.name,
            'profile.addressFirst': profile.addressFirst,
            'profile.postalCode': profile.postalCode,
            'profile.city': profile.city,
            'profile.addressCountryId': profile.addressCountryId || profile.countryId,
            'profile.countryId': profile.countryId || profile.addressCountryId,
            'profile.birthDate': profile.birthDate,
            'profile.phone': profile.phone,
            'profile.mobile': profile.mobile,
            'profile.passportNumber': profile.passportNumber,
            'profile.passportExpirationDate': profile.passportExpirationDate,
            'profile.visaNumber': profile.visaNumber,
            'profile.visaExpirationDate': profile.visaExpirationDate,
            'profile.visaCountryId': profile.visaCountryId,
            'private.travellerTypeId': profile.travellerTypeId
        };

        if (profile.language) {
            updateUser['profile.language'] = Vatfree.languages.getValidTravellerLanguage(profile.language);
        }

        const ncpChanged = Vatfree.travellers.ncpChanged(user, profile);

        Meteor.users.update({
            _id: this.userId
        },{
            $set: updateUser
        });
        // reload user doc
        user = Meteor.users.findOne({_id: this.userId});

        let travellerType = TravellerTypes.findOne({_id: profile.travellerTypeId});
        let updateUserStatus = '';
        // kick user back to unverifed status to re-verify
        if (ncpChanged) {
            if (travellerType) {
                const allDocsUploaded = areAllDocsUploaded(this.userId, travellerType, profile.countryId);
                const ncpFilledIn = Vatfree.travellers.ncpFilledIn(user);
                if (allDocsUploaded && ncpFilledIn) {
                    updateUserStatus = 'userUnverified';
                } else {
                    updateUserStatus = 'waitingForDocuments';
                }
            } else {
                // no traveller type ???
                updateUserStatus = 'userUnverified';
            }
        }

        if (travellerType) {
            if ((!travellerType.documentsNeeded || _.size(travellerType.documentsNeeded) === 0)) {
                // if no documents needed - we set the user to verified, there is nothing to check
                updateUserStatus = 'userVerified';
            }
        }

        if (updateUserStatus && user.private.status !== updateUserStatus) {
            Meteor.users.update({
                _id: this.userId
            },{
                $set: {
                    'private.status': updateUserStatus
                }
            });
        }
    },
    'savePushStatus'(userId, deviceId, pushStatus) {
        this.unblock();
        check(userId, String);
        check(deviceId, String);
        check(pushStatus, Object);
        if (!this.userId) {
            throw new Meteor.Error(404, 'Not logged in');
        }

        pushStatus.updatedAt = new Date();
        Meteor.users.update({
            _id: userId
        }, {
            $set: {
                [`private.pushStatus.${deviceId}`]: pushStatus
            }
        });
    },
    'unsetPushStatus'(userId, deviceId) {
        this.unblock();
        check(userId, String);
        check(deviceId, String);
        if (!this.userId) {
            throw new Meteor.Error(404, 'Not logged in');
        }

        Meteor.users.update({
            _id: userId
        }, {
            $unset: {
                [`private.pushStatus.${deviceId}`]: true
            }
        });
    },
    'registerWithMagicLink'(email) {
        this.unblock();
        email = email.toLowerCase();
        let user = Meteor.users.findOne({"emails.address": email});
        if (user) {
            throw new Meteor.Error(500, "This email address has already been registered");
        }

        let newUser = {
            createdAt: new Date(),
            createdBy: this.userId,
            roles: {
                '__global_roles__': ['traveller']
            },
            profile:  {
                email: email
            },
            private: {
                status: 'waitingForDocuments'
            }
        };
        if (Meteor.settings.defaults.TravellerTypeId) {
            newUser.private['travellerTypeId'] = Meteor.settings.defaults.TravellerTypeId;
        }

        newUser.emails = [
            {
                address: email,
                verified: false
            }
        ];

        let userId = Meteor.users.insert(newUser);
        Meteor.defer(() => {
            sendMagicLinkLogin(email);
        });

        return userId;
    },
    'registerAsNewUser'(email, password, password2, language = 'en') {
        this.unblock();
        check(email, String);
        check(password, String);
        check(password2, String);

        if (password !== password2) {
            throw new Meteor.Error(500, "Passwords do not match");
        }

        email = email.toLowerCase();
        let user = Meteor.users.findOne({"emails.address": email});
        if (user) {
            throw new Meteor.Error(500, "This email address has already been registered");
        }

        let newUser = {
            createdAt: new Date(),
            createdBy: this.userId,
            roles: {
                '__global_roles__': ['traveller']
            },
            profile:  {
                email: email,
                language: language
            },
            private: {
                status: 'waitingForDocuments'
            }
        };
        if (Meteor.settings.defaults.TravellerTypeId) {
            newUser.private['travellerTypeId'] = Meteor.settings.defaults.TravellerTypeId;
        }

        newUser.emails = [
            {
                address: email,
                verified: false
            }
        ];

        let userId = Meteor.users.insert(newUser);

        Accounts.setPassword(userId, password);

        return userId;
    },
    'sendPasswordReset'(email) {
        this.unblock();
        check(email, String);

        email = email.toLowerCase();
        let user = Meteor.users.findOne({"emails.address": email});
        if (user) {
            sendPasswordResetCode(user, email);
        }

        return true;
    },
    'resetPasswordFromCode'(email, password, password2, code) {
        this.unblock();
        check(email, String);
        check(password, String);
        check(password2, String);

        email = email.toLowerCase();
        let dbCode = Accounts.passwordLessCodes.findOne({ email: email + '_passwordReset', code: Number(code) });
        let user = Meteor.users.findOne({"emails.address": email});
        if (!user || !dbCode) {
            throw new Meteor.Error(500, "Could not find matching email and reset code, please verify your input and try again");
        }

        if (password !== password2) {
            throw new Meteor.Error(500, "Passwords do not match");
        }

        Accounts.setPassword(user._id, password);
        Accounts.passwordLessCodes.remove({ email: email + '_passwordReset' });
        Meteor.users.update({
            _id: user._id
        },{
            $set: {
                passwordSet: new Date()
            }
        });

        return true;
    },
    'submit-verification-documentation'(docs) {
        if (!docs || docs.length === 0) {
            throw new Meteor.Error(500, "No documents submitted");
        }
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let user = Meteor.users.findOne({_id: this.userId});
        if (!user || !user.private || !user.profile) {
            throw new Meteor.Error(500, "User not found");
        }

        if (_.contains(['userUnverified', 'waitingForDocuments', 'documentationExpired', 'documentationIncomplete', 'notEligibleForRefund'], user.private.status)) {
            _.each(docs, (file) => {
                let removeOldSubtype = false;

                let newFile = new FS.File();
                newFile.name(file.file.name.replace(/[^a-z0-9-_.]/ig, '_'));
                newFile.userId = user._id;
                newFile.itemId = user._id;
                newFile.target = 'travellers' + (file.internal ? '_internal' : '');
                if (file.subType) {
                    newFile.subType = file.subType;
                    removeOldSubtype = true;
                }
                newFile.createdAt = new Date();
                newFile.uploadedBy = user._id;
                newFile.attachData(file.fileContent);

                if (removeOldSubtype) {
                    Files.update({
                        itemId: user._id,
                        target: 'travellers',
                        subType: file.subType
                    },{
                        $unset: {
                            subType: 1
                        }
                    },{
                        multi: 1
                    });
                }
                Files.insert(newFile, function (err) {
                    if (err) {
                        console.error(err);
                        throw new Meteor.Error(500, 'Could not process file');
                    }
                });
            });

            checkUserStatusChange(user);
        } else {
            throw new Meteor.Error(500, "Not allowed to upload new documents");
        }
    },
    'set-verification-documentation-subtype'(fileId, subType) {
        check(fileId, String);
        check(subType, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let file = Files.findOne({_id: fileId});
        if (file.target !== "travellers" || file.itemId !== this.userId) {
            throw new Meteor.Error(404, 'access denied');
        }

        let user = Meteor.users.findOne({_id: this.userId});
        if (!user || !user.private || !user.profile) {
            throw new Meteor.Error(500, "User not found");
        }

        if (_.contains(['userUnverified', 'waitingForDocuments', 'documentationExpired', 'documentationIncomplete', 'notEligibleForRefund'], user.private.status)) {
            Files.update({
                itemId: this.userId,
                target: 'travellers',
                subType: subType
            }, {
                $unset: {
                    subType: 1
                }
            }, {
                multi: 1
            });

            Files.update({
                _id: fileId
            }, {
                $set: {
                    subType: subType
                }
            });
            checkUserStatusChange(user);
        } else {
            throw new Meteor.Error(500, "Not allowed to change documents");
        }
    },
    'upload-receipt-image'(receiptId, file) {
        check(receiptId, String);
        check(file, Object);
        if (!file.name || !file.fileContent) {
            throw new Meteor.Error(500, 'Invalid file');
        }

        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let user = Meteor.users.findOne({_id: this.userId});
        if (!user || !user.private || !user.profile) {
            throw new Meteor.Error(500, "User not found");
        }

        let receipt = Receipts.findOne({_id: receiptId});
        if (!receipt) {
            throw new Meteor.Error(500, "Receipt not found");
        }

        if (Vatfree.allowTravellerUpload(receipt)) {
            let newFile = new FS.File();
            newFile.name(file.name.replace(/[^a-z0-9-_.]/ig, '_'));
            newFile.receiptId = receiptId;
            newFile.itemId = receiptId;
            newFile.target = 'receipts' + (file.internal ? '_internal' : '');
            newFile.createdAt = new Date();
            newFile.uploadedBy = this.userId;
            newFile.attachData(file.fileContent);

            Files.insert(newFile, function (err) {
                if (err) {
                    console.error(err);
                    throw new Meteor.Error(500, 'Could not process file');
                }
            });
        } else {
            throw new Meteor.Error(500, "Not allowed to upload new receipt images");
        }
    },
    'delete-receipt-image'(fileId) {
        check(fileId, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let file = Files.findOne({_id: fileId});
        if (!file) {
            throw new Meteor.Error(500, "File not found");
        }

        let receipt = Receipts.findOne({_id: file.itemId});
        if (!receipt) {
            throw new Meteor.Error(500, "Receipt not found");
        }

        if (Vatfree.allowTravellerUpload(receipt) && receipt.userId === this.userId) {
            Files.update({
                _id: fileId
            },{
                $set: {
                    deleted: true
                }
            });
        } else {
            throw new Meteor.Error(500, "Not allowed to remove this receipt image");
        }
    },
    'register-popr-receipt-bare'(poprId, poprSecret) {
        check(poprId, String);
        check(poprSecret, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receiptData = {
            userId: this.userId,
            customsDate: null,
            customsCountryId: null,
            currencyId: Meteor.settings.defaults.currencyId,
            status: 'waitingForCustomsStamp',
            source: 'web',
            channel: 'inperson'
        };

        if (Vatfree.receipts.processPoprReceipt(poprId, poprSecret, receiptData)) {
            const minimumReceiptAmount = Vatfree.receipts.checkMinimumAmount(receiptData);
            if (minimumReceiptAmount !== true) {
                throw new Meteor.Error(407, 'Receipt amount is below minimum required by country (' + Vatfree.numbers.formatCurrency(minimumReceiptAmount) + ')');
            }

            const traveller = Meteor.users.findOne({_id: this.userId}) || {};
            const travellerTypeId = traveller.private.travellerTypeId;
            Vatfree.receipts.processServiceFees(travellerTypeId, receiptData, receiptData.shopId);
            const country = Countries.findOne({_id: receiptData.countryId}) || {};
            Vatfree.receipts.processVatLines(receiptData, country);

            console.log('popr receiptData', receiptData);
            let receiptId;
            try {
                receiptId = Receipts.insert(receiptData);
            } catch (e) {
                console.log(e);
                throw new Meteor.Error(500, 'Could not register receipt');
            }
            return receiptId;
        } else {
            throw new Meteor.Error(500, "Could not register receipt");
        }
    },
    'add-traveller-receipt'(formData, sourceIdentifier, receiptIsStamped = true) {
        check(formData, Object);
        if (sourceIdentifier) {
            check(sourceIdentifier, String);
        }
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let traveller = Meteor.users.findOne({
            _id: this.userId
        });
        if (!traveller) {
            throw new Meteor.Error(404, 'access denied');
        }
        let travellerType = TravellerTypes.findOne({_id: traveller.private.travellerTypeId});

        let receiptData = {};
        receiptData.receiptTypeId = formData.receiptTypeId;
        receiptData.shopId = formData.shopId;
        receiptData.countryId = formData.countryId;
        receiptData.currencyId = formData.currencyId;
        receiptData.customsDate = formData.customsDate;
        receiptData.customsCountryId = formData.customsCountryId;
        receiptData.purchaseDate = formData.purchaseDate;
        receiptData.amount = formData.amount;
        receiptData.totalVat = formData.totalVat;
        //receiptData.vat = formData.vat;

        if (travellerType && travellerType.forceReceiptTypeId) {
            receiptData.receiptTypeId = travellerType.forceReceiptTypeId;
        }

        let receiptType = ReceiptTypes.findOne({_id: receiptData.receiptTypeId});
        if (receiptType && receiptType.extraFields && _.size(receiptType.extraFields) > 0) {
            // check whether any of the fields are supposed to be unique
            let uniqueFields = [];
            _.each(receiptType.extraFields, (extraField, extraFieldKey) => {
                if (extraField.unique === true) {
                    uniqueFields.push(extraFieldKey);
                }
            });
            if (uniqueFields.length > 0) {
                // check whether this has been submitted before
                let allFieldsHaveValue = true;
                let uniqueSelector = {
                    receiptTypeId: receiptType._id,
                };
                _.each(uniqueFields, (uniqueField) => {
                    allFieldsHaveValue = allFieldsHaveValue && !!formData.receiptTypeExtraFields[uniqueField];
                    uniqueSelector['receiptTypeExtraFields.' + uniqueField] = formData.receiptTypeExtraFields[uniqueField];
                });

                if (allFieldsHaveValue && Receipts.findOne(uniqueSelector)) {
                    throw new Meteor.Error(500, "This receipt has already been registered");
                }
            }
        }

        if (!travellerType.isNato && moment(formData.customsDate).isBefore(moment(formData.purchaseDate))) {
            throw new Meteor.Error(500, 'Customs stamp date cannot be before purchase date');
        }

        if (formData.receiptTypeExtraFields) {
            receiptData.receiptTypeExtraFields = {};
            _.each(formData.receiptTypeExtraFields, (value, key) => {
                if (_.isString(key) && _.isString(value)) {
                    receiptData.receiptTypeExtraFields[key] = value;
                }
            });
        }

        if (travellerType.isNato && Shops.isPartner(formData.shopId)) {
            receiptData.serviceFee = 0;
            receiptData.deferredServiceFee = 900;
            receiptData.refund = receiptData.totalVat;
            receiptData.nonStandardVat = true;
        } else {
            let fees = Vatfree.receipts.getFees(formData.shopId);
            if (fees.deferredServiceFee) {
                receiptData.serviceFee = 0;
                receiptData.deferredServiceFee = Vatfree.vat.calculateFee(receiptData.totalVat, fees.deferredServiceFee, fees.deferredMinFee, fees.deferredMaxFee);
                receiptData.refund = receiptData.totalVat;
            } else {
                receiptData.serviceFee = Vatfree.vat.calculateFee(receiptData.totalVat, fees.serviceFee, fees.minFee, fees.maxFee);
                receiptData.refund = Vatfree.vat.calculateRefund(receiptData.totalVat, fees.serviceFee, fees.minFee, fees.maxFee);
            }
        }

        if (receiptType && receiptType.originalsNotNeeded) {
            receiptData.originalsCheckedBy = receiptType.name;
            receiptData.originalsCheckedAt = new Date();
        }

        // fill the split vat rates in
        let country = Countries.findOne({_id: receiptData.countryId}) || {};
        receiptData.vat = {};
        let defaultRate = Vatfree.vat.getDefaultRate(country.vatRates);
        _.each(country.vatRates, (vatRate) => {
            if (vatRate.rate === defaultRate) {
                receiptData.vat[vatRate.rate.toString().replace('.', '_')] = receiptData.totalVat;
            } else {
                receiptData.vat[vatRate.rate.toString().replace('.', '_')] = 0;
            }
        });

        receiptData.userId = this.userId;
        receiptData.status = 'userPending';
        if (!receiptIsStamped) {
            receiptData.status = 'waitingForCustomsStamp';
        }
        receiptData.source = 'web';
        receiptData.sourceIdentifier = sourceIdentifier || (this.connection && this.connection.httpHeaders ? this.connection.httpHeaders['user-agent'] : '');

        return Receipts.insert(receiptData);
    },
    'traveller-get-receipt'(receiptId) {
        check(receiptId, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let receipt = Receipts.findOne({_id: receiptId});
        if (!receipt || receipt.userId !== this.userId) {
            throw new Meteor.Error(500, "Receipt not found");
        }

        return receipt;
    },
    'traveller-add-shop'(place, geo) {
        check(place, Object);
        check(geo, Object);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let name = place.name;
        let address = (_.find(place.address_components, (component) => { return _.contains(component.types, 'route'); }) || {}).long_name;
        let postal_code = (_.find(place.address_components, (component) => { return _.contains(component.types, 'postal_code'); }) || {}).long_name;
        let city = (_.find(place.address_components, (component) => { return _.contains(component.types, 'locality'); }) || {}).long_name;
        let countryCode = (_.find(place.address_components, (component) => { return _.contains(component.types, 'country'); }) || {}).short_name;
        let countryId = Meteor.settings.defaults.countryId;
        let country = Countries.findOne({code: countryCode});
        if (country) {
            countryId = country._id
        } else {
            country = Countries.findOne({_id: countryId});
        }
        if (!country) {
            throw new Meteor.Error('Refunds from the selected country are not supported');
        }

        // try to find shop with the same name and postal code
        let shop = Shops.findOne({
            name: name,
            addressFirst: address,
            postCode: postal_code,
            city: city,
            countryId: countryId
        });
        if (shop) {
            return shop._id;
        }

        let shopData = {
            name: name,
            addressFirst: address,
            postCode: postal_code,
            city: city,
            countryId: countryId,
            currencyId: country.currencyId || Meteor.settings.defaults.currencyId,
            geo: geo
        };

        return Shops.insert(shopData);
    },
    'accept-terms-and-conditions'() {
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Meteor.users.update({
            _id: this.userId,
            'profile.terms': {
                $exists: false
            }
        },{
            $set: {
                'profile.terms': new Date()
            }
        });
    },
    'delete-traveller-file'(fileId) {
        check(fileId, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let file = Files.findOne({_id: fileId});
        if (file.itemId !== this.userId) {
            throw new Meteor.Error(404, 'access denied');
        }
        let traveller = Travellers.findOne({_id: this.userId}) || {};
        if (traveller.private && traveller.private.status) {
            if (_.contains(['userUnverified', 'waitingForDocuments', 'documentationExpired', 'documentationIncomplete', 'notEligibleForRefund'], traveller.private.status)) {
                Files.update({
                    _id: fileId
                },{
                    $set: {
                        deleted: true
                    }
                });

                let user = Meteor.users.findOne({_id: this.userId});
                let travellerType = TravellerTypes.findOne({_id: user.private.travellerTypeId});
                if (travellerType) {
                    let allDocsUploaded = areAllDocsUploaded(user._id, travellerType, user.profile.countryId);
                    if (!allDocsUploaded) {
                        Meteor.users.update({
                            _id: this.userId
                        },{
                            $set: {
                                'private.status': 'waitingForDocuments'
                            }
                        });
                    }
                }
            } else {
                throw new Meteor.Error(404, 'access denied');
            }
        } else {
            throw new Meteor.Error(404, 'access denied');
        }
    },
    'need-to-set-password'() {
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let user = Meteor.users.findOne({_id: this.userId});
        return checkSetPassword(user);
    },
    'set-password'(password) {
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        let user = Meteor.users.findOne({_id: this.userId});
        if (checkSetPassword(user)) {
            Accounts.setPassword(this.userId, password, {logout: false});
            Meteor.users.update({
                _id: this.userId
            },{
                $set: {
                    passwordSet: new Date()
                }
            });
        } else {
            throw new Meteor.Error(404, 'access denied');
        }
    },
    'traveller-add-email'(email) {
        check(email, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        email = email.toLowerCase();

        let existingUser = Meteor.users.findOne({'emails.address': email});
        if (existingUser) {
            throw new Meteor.Error(500, "Email is already registered in our system");
        }

        Accounts.addEmail(this.userId, email, false);
        Accounts.sendVerificationEmail(this.userId, email);
    },
    'traveller-resend-verification-email'(email) {
        check(email, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        email = email.toLowerCase();

        let existingUser = Meteor.users.findOne({_id: this.userId, 'emails.address': email});
        if (!existingUser) {
            throw new Meteor.Error(500, "Email address not found in our system");
        }

        let emailObject = _.find(existingUser.emails, (emailObj) => {
            if (emailObj.address === email) {
                return emailObj;
            }
        });
        if (emailObject && emailObject.verified !== true) {
            Accounts.sendVerificationEmail(this.userId, email);
        } else {
            throw new Meteor.Error(500, "Email address has already been verified");
        }

        return true;
    },
    'traveller-delete-receipt'(receiptId) {
        check(receiptId, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        const receipt = Receipts.findOne({_id: receiptId});
        if (!receipt || receipt.userId !== this.userId || !Vatfree.receipts.travellerAllowedDeleted(receipt)) {
            throw new Meteor.Error(404, 'access denied');
        }

        Receipts.update({
            _id: receiptId
        },{
            $set: {
                status: 'deleted'
            },
            $unset: {
                poprId: true,
                poprSecret: true,
                poprClaimToken: true
            }
        });
    },
    'set-affiliate-code'(affiliateCode) {
        if (this.userId) {
            const affiliate = Affiliates.findOne({'affiliate.code': affiliateCode}, {fields: {_id: 1}});
            if (affiliate) {
                Meteor.users.update({
                    _id: this.userId,
                    'private.affiliateId': {
                        $exists: false
                    }
                },{
                    $set: {
                        'private.affiliateId': affiliate._id
                    }
                });
            }
        }
    },
    'receipt-is-stamped'(receiptId, customsDate) {
        check(receiptId, String);
        if (!Roles.userIsInRole(this.userId, 'traveller', Roles.GLOBAL_GROUP)) {
            throw new Meteor.Error(404, 'access denied');
        }

        const receipt = Receipts.findOne({_id: receiptId});
        if (!receipt || receipt.userId !== this.userId || receipt.status !== 'waitingForCustomsStamp') {
            throw new Meteor.Error(404, 'access denied');
        }

        let newStatus = 'userPending';

        const user = Meteor.users.findOne({_id: this.userId});
        let userVerified = user && user.private && user.private.status === 'userVerified';
        if (userVerified) {
            if (receipt.originalsCheckedBy) {
                newStatus = 'visualValidationPending';
            } else {
                newStatus = 'waitingForOriginal';
            }
        }

        Receipts.update({
            _id: receiptId
        },{
            $set: {
                customsDate: customsDate,
                status: newStatus
            }
        });
    },
    log() {
        console.log(...arguments);
    }
});

let checkSetPassword = function (user) {
    if (user.services) {
        let serviceKeys = _.keys(user.services);
        serviceKeys = _.without(serviceKeys, 'resume');
        if (serviceKeys.length === 0) {
            return true;
        }
    } else {
        return true;
    }

    return false;
};

let sendPasswordResetCode = function (user, email) {
    if (user) {
        let travellerStatusNotify = {
            emailTextId: Meteor.settings.defaults.emailPasswordResetTextId,
            emailTemplateId: Meteor.settings.defaults.emailTemplateId
        };
        let templateData = {
            traveller: user.profile
        };
        let toAddress = email;
        let fromAddress = "support@vatfree.com";
        let activityIds = {
            travellerId: user._id
        };
        let emailLanguage = user.profile.language || 'en';

        // send magic link email - using the notification class
        const notificationOptions = {
            suppressMobileNotification: true,
            hideInMessageBox: true
        };
        return Vatfree.notify.sendNotification(travellerStatusNotify, templateData, toAddress, fromAddress, activityIds, emailLanguage, false, notificationOptions);
    }
};

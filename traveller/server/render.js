import { onPageLoad } from "meteor/server-render";

const getFaqs = function (selector, language) {
    const fields = {
        ['subject.' + language]: 1,
        ['text.' + language]: 1,
    };

    if (language !== 'en') {
        fields['subject.en'] = 1;
        fields['text.en'] = 1;
    }

    let injectBody = '';

    FAQs.find(selector, {
        fields: fields
    }).forEach((faq) => {
        let subject = faq.subject[language];
        let text = faq.text[language];

        if (language !== 'en') {
            if (!subject) {
                subject = faq.subject['en'];
            }
            if (!text) {
                text = faq.text['en'];
            }
        }

        injectBody += `${subject} / ${text}`;
    });

    return injectBody;
};

onPageLoad(function (sink) {
    const path = sink.request.url.pathname;
    if (path && path.length > 0) {
        const pathParts = path.split('/');
        const page = pathParts[1];
        let language = 'en';

        const query = sink.request.url.query;
        if (query && query.lang) {
            language = query.lang;
        }

        let injectBody = '';
        let selector = {};
        switch (page) {
            case 'faq':
                let faqId = pathParts[2];
                if (faqId === 'embed') {
                    faqId = pathParts[3];
                }
                selector = {
                    _id: faqId
                };
                injectBody = getFaqs(selector, language);
                break;
            case 'faqs':
                injectBody = getFaqs(selector, language);
                break;
        }

        if (injectBody) {
            sink.appendToBody("<div class='server-render'>" + injectBody + '</div>');
        }
    }
});

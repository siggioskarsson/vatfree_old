/*
Meteor settings:
{
  "public"
    "analyticsSettings": {
      // Add your analytics tracking ids here (remove this line before running)
      "Google Analytics" : {"trackingId": "Your tracking ID"},
      "Amplitude"        : {"apiKey": "..."},
      "Chartbeat"        : {"uid": "..."},
      "comScore"         : {"c2": "..."},
      "HubSpot"          : {"portalId": "..."},
      "Intercom"         : {"appId": "..."},
      "Keen IO"          : {"projectId": "...", "writeKey": "..."},
      "KISSmetrics"      : {"apiKey": "..."},
      "Mixpanel"         : {"token":  "...", "people": true},
      "Quantcast"        : {"pCode": "..."},
      "Segment.io"       : {"apiKey": "..."}
    },
  }
}
 */
Vatfree.trackEvent = function(event, eventCategory, extraData) {
    let traveller = Meteor.user() || {};
    let userId = traveller._id;
    let status = traveller.private ? traveller.private.status : "unknown";
    let travellerTypeId = traveller.private ? traveller.private.travellerTypeId : "unknown";

    let trackEvent = {
        userId: userId,
        eventCategory: eventCategory,
        status: status,
        travellerType: travellerTypeId
    };

    if (extraData) {
        _.each(extraData, (value, key) => {
            trackEvent[key] = value;
        });
    }

    if (typeof analytics !== "undefined" && analytics && analytics.track && Meteor.settings.public.analyticsSettings) {
        // Web analytics
        analytics.track(event, trackEvent);
    } else if (typeof analytics !== "undefined" && analytics && analytics.trackEvent && Meteor.settings.public.analyticsSettings) {
        // Cordova analytics
        analytics.trackEvent(event, trackEvent);
    }

    if (typeof dataLayer !== "undefined" && dataLayer) {
        dataLayer.push(_.extend({event: event}, trackEvent));
    }
};

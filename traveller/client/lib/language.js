import moment from 'moment';

Meteor.startup(() => {
    const previousLanguage = localStorage.getItem('language');
    if (previousLanguage) {
        Session.set("showLanguageLoadingIndicator", true);
        Vatfree.setTravellerLanguage(previousLanguage)
            .done(function () {
                Session.set("showLanguageLoadingIndicator", false);
            })
            .fail(function (error_message) {
                console.error(error_message);
                Session.set("showLanguageLoadingIndicator", false);
            });
    }

    Tracker.autorun(() => {
        let user = Meteor.user();
        let language = Vatfree.getLanguage();
        if (user && user.profile && user.profile.language && user.profile.language !== language) {
            Vatfree.setTravellerLanguage(user.profile.language);
            localStorage.setItem('language', user.profile.language);
        } else if (!user && !previousLanguage) {
            // http://www.apps4android.org/?p=3695
            let deviceLanguage = navigator.language.substr(0, 2);
            if (deviceLanguage === 'zh') deviceLanguage = 'zh-CN';

            if (deviceLanguage !== language) {
                Vatfree.setTravellerLanguage(deviceLanguage);
                localStorage.setItem('language', Vatfree.getLanguage());
            }
        }
        moment.locale(language.toLowerCase());
        Meteor.subscribe('web-texts-site', 'app', language);
    });

    Tracker.autorun(() => {
        let user = Meteor.user();
        if (!user) {
            let language = Vatfree.getLanguage();
            localStorage.setItem('language', language);
        }
    });
});

import { Accounts } from 'meteor/accounts-base';

// replace the verify-email callback from accounts-ui
delete Accounts._accountsCallbacks['verify-email'];
Accounts.onEmailVerificationLink(function(token, done) {
    Accounts.verifyEmail(token, function(err) {
        if (err) {
            toastr.error(err.message);
        } else {
            toastr.info(TAPi18n.__('Email address has been verified'));
            FlowRouter.go('/');
        }
        done();
    });
});

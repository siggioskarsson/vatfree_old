Accounts.passwordless = {};

/**
 * Request a verification code.
 * @param selector The email or username of the user
 * @param [callback]
 */
Meteor.sendVerificationCode = function (selector, options, callback) {
    if (!callback && typeof options === 'function')
        callback = options;

    // Save the selector in a Session so even if the client reloads, the selector is stored
    Session.set('accounts-passwordless.selector', selector);
    Meteor.call('accounts-passwordless.sendVerificationCode', selector, options, callback);
};

/**
 * Login with the verification code.
 * @param options code The verification code. selector The username or email (optional)
 * @param [callback]
 */
Meteor.loginWithPasswordless = function (options, callback) {
    console.log('lwpl', options);
    Accounts.callLoginMethod({
        methodArguments: [{
            selector: Session.get('accounts-passwordless.selector') || options.selector,
            code: options.code
        }],
        userCallback: callback
    });
};

/**
 * Set username. The user must be logged
 * @param username The name of the user
 * @param [callback]
 */
Meteor.setUsername = function (username, callback) {
    Meteor.call('accounts-passwordless.setUsername', username, callback);
};

const algoliasearch = require("algoliasearch/dist/algoliasearchLite");

export let algoliaIndex = false;
if (Meteor.settings.public.algolia && Meteor.settings.public.algolia.applicationId) {
    let algoliaClient = algoliasearch(Meteor.settings.public.algolia.applicationId, Meteor.settings.public.algolia.apiKey);
    algoliaIndex = algoliaClient.initIndex(Meteor.settings.public.algolia.shopIndex);
}

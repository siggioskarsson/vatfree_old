Meteor.startup(() => {
    Tracker.autorun(() => {
        let user = Meteor.user();
        if (user && user.profile && user.profile.language) {
            Vatfree.setTravellerLanguage(user.profile.language);
        }

        let language = Vatfree.getLanguage() || 'en';
        Meteor.subscribe('web-texts-site', 'travellers', language);
        $.fn.select2.defaults.set('language', language);
    });
});

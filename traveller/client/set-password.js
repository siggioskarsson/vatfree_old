Template.set_password.onCreated(function() {
    this.isSettingPassword = new ReactiveVar();
});

Template.set_password.helpers({
    isSettingPassword() {
        return Template.instance().isSettingPassword.get();
    }
});

Template.set_password.events({
    'submit form[name="password-set-form"]'(e, template) {
        e.preventDefault();
        e.preventDefault();
        template.isSettingPassword.set(true);

        let password = $('input[name="password"]').val();
        let password2 = $('input[name="password2"]').val();

        if (!password || password !== password2) {
            alert("Passwords do not match.");
            return false;
        }

        Meteor.call('set-password', password, (err, result) => {
            if (err) {
                alert(err.reason);
            } else {
                toastr.success('Password has been saved!');
            }
            template.isSettingPassword.set();
        });
    }
});

Template.login.onCreated(function() {
    this.registering = new ReactiveVar();
    this.forgotPassword = new ReactiveVar();
    this.forgotPasswordSet = new ReactiveVar();

    this.isSettingPassword = new ReactiveVar();
    this.isLoggingIn = new ReactiveVar();
    this.isRegistering = new ReactiveVar();
    this.isSendingPasswordRestCode = new ReactiveVar();
    this.isLoggingInWithSocial = new ReactiveVar();
});

Template.login.onRendered(function() {
    if (Session.get('affiliateCode')) {
        FlowRouter.go('/a/' + Session.get('affiliateCode'));
    }
    if (Session.get('poprCode') && FlowRouter.current().route.path !== '/p/:poprId/:poprSecret') {
        FlowRouter.go('/p/' + Session.get('poprCode'));
    }
});

Template.login.helpers({
    registering() {
        return Template.instance().registering.get();
    },
    forgotPassword() {
        return Template.instance().forgotPassword.get();
    },
    forgotPasswordSet() {
        return Template.instance().forgotPasswordSet.get();
    },
    isRegistering() {
        return Template.instance().isRegistering.get();
    },
    isSettingPassword() {
        return Template.instance().isSettingPassword.get();
    },
    isLoggingIn() {
        return Template.instance().isLoggingIn.get();
    },
    isSendingPasswordRestCode() {
        return Template.instance().isSendingPasswordRestCode.get();
    },
    isLoggingInWithSocial(social) {
        if (social) {
            return Template.instance().isLoggingInWithSocial.get() === social;
        } else {
            return !!Template.instance().isLoggingInWithSocial.get();
        }
    },
    loginServiceActive(service) {
        return !!Meteor.settings.public.login[service];
    }
});

Template.login.events({
    'click .forgot-password'(e, template) {
        e.preventDefault();
        template.forgotPassword.set(true);
    },
    'click .cancel-forgot-password'(e, template) {
        e.preventDefault();
        template.forgotPassword.set();
        template.forgotPasswordSet.set();
    },
    'submit form[name="password-forgot-form"]'(e, template) {
        e.preventDefault();
        let email = $('input[name="email"]').val();

        if (!email) {
            alert(Vatfree.translate("Please fill in an email address"));
        }

        template.isSendingPasswordRestCode.set(true);
        Meteor.call('sendPasswordReset', email, (err, result) => {
            if (err) {
                alert(err.reason);
            } else {
                template.forgotPasswordSet.set(email);
                template.isSendingPasswordRestCode.set();
                Tracker.afterFlush(() => {
                    alert(Vatfree.translate('We sent you a reset code by email. Please fill in the code in the field below and reset your password'));
                });
            }
        });
    },
    'submit form[name="password-forgot-set-form"]'(e, template) {
        e.preventDefault();
        e.preventDefault();
        let email = $('input[name="email"]').val();
        let code = $('input[name="code"]').val();

        let password = $('input[name="password"]').val();
        let password2 = $('input[name="password2"]').val();

        if (!email) {
            alert("Please fill in an email address.");
            return false;
        }
        if (!password || password !== password2) {
            alert(Vatfree.translate("Passwords do not match"));
            return false;
        }

        template.isSettingPassword.set(true);
        Meteor.call('resetPasswordFromCode', email, password, password2, code, (err, result) => {
            if (err) {
                alert(err.reason);
                template.isSettingPassword.set();
            } else {
                Meteor.loginWithPassword(email, password, (err, result) => {
                    if (err) {
                        alert(err.reason);
                    } else {
                        template.forgotPassword.set();
                        template.forgotPasswordSet.set();
                    }
                    template.isSettingPassword.set();
                });
            }
        });
    },
    'click .register-account'(e, template) {
        e.preventDefault();
        template.registering.set(true);
    },
    'click .cancel-register-account'(e, template) {
        e.preventDefault();
        template.registering.set();
    },
    'click .google-login'(e, template) {
        e.preventDefault();
        template.isLoggingInWithSocial.set('google');
        Meteor.loginWithGoogle((err) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                if (dataLayer) {
                    dataLayer.push({
                        'event':'loggedin',
                        'UID': Meteor.userId(),
                        'userType':'traveller',
                        'loginService': 'google',
                    });
                }
            }
            template.isLoggingInWithSocial.set();
        });
    },
    'click .linkedin-login'(e, template) {
        e.preventDefault();
        template.isLoggingInWithSocial.set('linkedin');
        Meteor.loginWithLinkedIn((err) => {
            if (err) {
                console.log(err);
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                if (dataLayer) {
                    dataLayer.push({
                        'event':'loggedin',
                        'UID': Meteor.userId(),
                        'userType':'traveller',
                        'loginService': 'linkedin',
                    });
                }
            }
            template.isLoggingInWithSocial.set();
        });
    },
    'click .facebook-login'(e, template) {
        e.preventDefault();
        template.isLoggingInWithSocial.set('facebook');
        Meteor.loginWithFacebook({
            requestPermissions: ['public_profile', 'email']
        }, (err) => {
            if (err) {
                console.log(err);
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                if (dataLayer) {
                    dataLayer.push({
                        'event':'loggedin',
                        'UID': Meteor.userId(),
                        'userType':'traveller',
                        'loginService': 'facebook',
                    });
                }
            }
            template.isLoggingInWithSocial.set();
        });
    },
    'click .wechat-login'(e, template) {
        e.preventDefault();
        template.isLoggingInWithSocial.set('wechat');
        Meteor.loginWithWechat({}, (err) => {
            if (err) {
                console.log(err);
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                if (dataLayer) {
                    dataLayer.push({
                        'event':'loggedin',
                        'UID': Meteor.userId(),
                        'userType':'traveller',
                        'loginService': 'wechat',
                    });
                }
            }
            template.isLoggingInWithSocial.set();
        });
    },
    'submit form[name="register-form"]'(e, template) {
        e.preventDefault();
        template.isRegistering.set(true);
        let email = $('input[name="register_email"]').val();
        Meteor.call('registerWithMagicLink', email, (err, result) => {
            if (err) {
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                alert(Vatfree.translate('An email has been sent to you_ Please click on the link in the email to login_'));
                template.registering.set();
            }
            template.isRegistering.set();
        });
    },
    'submit form[name="password-login"]'(e, template) {
        e.preventDefault();
        let email = $('input[name="email"]').val();
        let password = $('input[name="password"]').val();

        template.isLoggingIn.set(true);
        Meteor.loginWithPassword(email, password, (err, result) => {
            if (err) {
                if (err.reason === "User has no password set") {
                    alert(Vatfree.translate("This account has no password set. Please request a password reset to login."));
                } else {
                    alert(Vatfree.translate("Could nog log on. Please check your email and password and try again."));
                }
            }
            template.isLoggingIn.set();
        });
    }
});

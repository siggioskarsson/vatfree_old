Template.mainLayout.onCreated(function() {
    this.passwordChecked = new ReactiveVar(false);
    this.setPassword = new ReactiveVar(false);
    this.subscribe('my-profile');
});

Template.mainLayout.onRendered(function() {
    this.autorun(() => {
        document.title = FlowRouter.getRouteName();
    });

    this.autorun(() => {
        let user = Meteor.user();
        if (user) {
            Meteor.call('need-to-set-password', (err, result) => {
                if (err) {
                    console.log(err);
                } else {
                    this.setPassword.set(result);
                }
                this.passwordChecked.set(true);
            });
        }
    });

    this.autorun(() => {
        let user = Meteor.user();
        let language = Vatfree.getLanguage();
        TAPi18n.subscribe('countries-i18n');
        TAPi18n.subscribe('currencies-i18n');
        TAPi18n.subscribe('traveller-types-i18n');
        TAPi18n.subscribe('receipt-types-i18n');
    });
});

Template.mainLayout.helpers({
    getBackgroundImage() {
        return "background-image";
    },
    getVersionInfo() {
        return Meteor.settings.public.versionInfo;
    },
    passwordChecked() {
        return Template.instance().passwordChecked.get();
    },
    setPassword() {
        return Template.instance().setPassword.get();
    },
    useMobileWebsite() {
        return Session.get('traveller.mobileWebsite');
    }
});

Template.mainLayout.events({
    'dropped #wrapper'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    },
    'click .mobile-no-thanks'(e, template) {
        e.preventDefault();
        Session.setPersistent('traveller.mobileWebsite', true);
    }
});

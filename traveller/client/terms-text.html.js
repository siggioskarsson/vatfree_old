Template.terms_text.helpers({
    languageNotNL() {
        return Vatfree.getLanguage() !== 'nl';
    }
});

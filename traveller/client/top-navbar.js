Template.topNavbar.events({
    'click .logout'(e) {
        e.preventDefault();
        Meteor.logout();
    },
    'click .nav.navbar-nav > li > a'(e) {
        // close hamburger
        $('button.navbar-toggle').trigger('click');
    }
});

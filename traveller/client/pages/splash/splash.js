Template.splash.onRendered(function() {
    this.autorun(() => {
        if (Meteor.user()) {
            FlowRouter.go('/');
        }
    });
});

Template.splash.events({
    'click .register-receipts'(e) {
        e.preventDefault();
        FlowRouter.go('/');
    }
});

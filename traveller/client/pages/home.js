Template.reroute.onCreated(function() {
    this.autorun(() => {
        let user = Meteor.user();
        if (user && user.private) {
            if (user.private.status === 'userVerified') {
                FlowRouter.go('/receipts');
            } else {
                FlowRouter.go('/profile');
            }
        }
    });
});

Shops = new Meteor.Collection('shops');
Shops.attachSchema(new SimpleSchema({
    id: {
        type: Number,
        label: "Internal shop id",
        optional: true
    },
    name: {
        type: String,
        label: "Name of the shop",
        optional: false
    },
    status: {
        type: String,
        label: "The status of this shop",
        allowedValues: [
            'bankrupt',
            'moved',
            'dissolved',
            'takenOver',
            'waitingForVerification',
            'active'
        ],
        optional: false,
        defaultValue: 'waiting'
    },
    affiliatedWith: {
        type: String,
        label: "Affiliated with",
        optional: true
    },
    partneredBy: {
        type: String,
        label: "The user that partnered this shop",
        optional: true
    },
    partneredAt: {
        type: Date,
        label: "The date that this shop was partnered",
        optional: true
    },
    email: {
        type: String,
        label: "General email address",
        optional: true
    },
    tel: {
        type: String,
        label: "General telephone number",
        optional: true
    },
    addressFirst: {
        type: String,
        label: "Address of the shop",
        optional: false
    },
    postCode: {
        type: String,
        label: "Postal code of the shop",
        optional: true
    },
    city: {
        type: String,
        label: "City of the shop",
        optional: false
    },
    currencyId: {
        type: String,
        label: "Currency of the shop",
        optional: false
    },
    countryId: {
        type: String,
        label: "Country Id of the shop",
        optional: false
    },
    chamberOfCommerce: {
        type: String,
        label: "Chamber of commerce identifier",
        optional: true
    },
    vatNumber: {
        type: String,
        label: "VAT number",
        optional: true
    },
    companyId: {
        type: String,
        label: "Parent Company Id of the shop",
        optional: true
    },
    geo: {
        type: Object,
        label: "Geo information about the loation of the shop",
        blackbox: true,
        optional: true
    },
    partnershipStatus: {
        type: String,
        label: "Partnership status",
        allowedValues: [
            'new',
            'partner',
            'pledger',
            'uncooperative'
        ],
        optional: true
    },
    language: {
        type: String,
        label: "Language code",
        optional: true
    },
    emailTemplateId: {
        type: String,
        label: "Preferred email template",
        optional: true
    },
    stats: {
        type: Object,
        label: "Stats about this shop",
        optional: true,
        blackbox: true
    },
    notes: {
        type: String,
        label: "Shop notes",
        optional: true
    },
    inShoppingGuide: {
        type: Boolean,
        label: "Whether this shop is in the shopping guide",
        defaultValue: true,
        optional: true
    },
    useParentShoppingGuideInfo: {
        type: Boolean,
        label: "Whether to use the parent shopping guide info",
        defaultValue: false,
        optional: true
    },
    shoppingGuideDescription: {
        type: String,
        label: "Shopping guide descriptive text",
        optional: true
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
Shops.attachSchema(CreatedUpdatedSchema);

Shops.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return false;
    },
    remove: function (userId, doc) {
        return false;
    }
});

import moment from 'moment';

ReceiptRejections = new Meteor.Collection('receipt-rejections');

Receipts = new Meteor.Collection('receipts');
Receipts.attachSchema(new SimpleSchema({
    receiptNr: {
        type: Number,
        label: "Internally generated unique receipt number",
        optional: true
    },
    receiptTypeId: {
        type: String,
        label: "The type of the receipt, if not normal",
        optional: true
    },
    receiptTypeExtraFields: {
        type: Object,
        label: "The extra fields the receipt types add to the receipt form",
        optional: true,
        blackbox: true
    },
    source: {
        type: String,
        label: "Source of the receipt",
        optional: false,
        defaultValue: 'desk',
        allowedValues: [
            'mobile',
            'web',
            'desk',
            'deskClient',
            'affiliate',
            'office',
            'officeViaMerchant',
            'officeViaCompetitor'
        ]
    },
    invoiceId: {
        type: String,
        label: "The invoice this receipt was invoiced to the shop",
        optional: true
    },
    userId: {
        type: String,
        label: "User that submitted this receipt",
        optional: false
    },
    shopId: {
        type: String,
        label: "Shop this receipt was issued",
        optional: true
    },
    companyId: {
        type: String,
        label: "Company this receipt will be billed to",
        optional: true
    },
    poprId: {
        type: String,
        label: "POPr Id of this receipt, if receipts is available via POPr",
        optional: true
    },
    affiliateId: {
        type: String,
        label: "Affiliate Id of this receipt",
        optional: true
    },
    purchaseDate: {
        type: Date,
        label: "Purchase date of this receipt",
        optional: true
    },
    customsDate: {
        type: Date,
        label: "Customs stamp date of this receipt",
        optional: true
    },
    stampDate: {
        type: Date,
        label: "Customs stamp date of this receipt",
        optional: true
    },
    amount: {
        type: Number,
        label: "Total amount of receipt",
        optional: false
    },
    countryId: {
        type: String,
        label: "Country the purchase was made",
        optional: true
    },
    currencyId: {
        type: String,
        label: "Currency of this receipt",
        optional: false
    },
    customsCountryId: {
        type: String,
        label: "Customs stamp country",
        optional: true
    },
    vat: {
        type: Object,
        label: "VAT on receipt",
        optional: true,
        blackbox: true
    },
    totalVat: {
        type: Number,
        label: "Total of VAT on the receipt",
        optional: true
    },
    serviceFee: {
        type: Number,
        label: "Total service fee",
        optional: true
    },
    deferredServiceFee: {
        type: Number,
        label: "Total deferred service fee, if servie is supposed to be invoiced to shop/company",
        optional: true
    },
    refund: {
        type: Number,
        label: "Total refund",
        optional: true
    },
    language: {
        type: String,
        label: "Language code",
        optional: true
    },
    status: {
        type: String,
        label: "Status of the receipt",
        optional: false,
        allowedValues: [
            'userPending',
            'shopPending',
            'waitingForOriginal',
            'visualValidationPending',
            'waitingForDoubleCheck',
            'readyForInvoicing',
            'waitingForPayment',
            'paidByShop',
            'requestPayout',
            'paid',
            'rejected'
        ]
    },
    receiptRejectionIds: {
        type: [String],
        label: "The id's of the receipt-rejections of this receipt",
        optional: true
    },
    reasonRejected: {
        type: String,
        label: "Reason the request was rejected",
        optional: true,
        allowedValues: [
            'userNotEligible',
            'receiptNotReadable',
            'customsStampMissing',
            'customsStampExpired',
            'originalReceiptMissing',
            'receiptExpired',
            'shopNotCooperating'
        ]
    },
    textSearch: {
        type: String,
        optional: true
    }
}));
Receipts.attachSchema(CreatedUpdatedSchema);

Receipts.allow({
    insert: function (userId, doc) {
        return false;
    },
    update: function (userId, doc, fieldNames, modifier) {
        return false;
    },
    remove: function (userId, doc) {
        return false;
    }
});

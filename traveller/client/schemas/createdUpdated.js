/* global CreatedUpdatedSchema: true */
import moment from 'moment';

CreatedUpdatedSchema = new SimpleSchema({
    createdAt: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: new Date()
                };
            } else {
                this.unset();
            }
        }
    },
    createdWeek: {
        type: Number,
        label: "Week of creation date, used in stats",
        autoValue: function() {
            let week = Number(moment().format('GGGGWW'));
            if (this.isInsert) {
                return week;
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: week
                };
            } else {
                this.unset();
            }
        }
    },
    createdMonth: {
        type: Number,
        label: "Week of creation date, used in stats",
        autoValue: function() {
            let month = Number(moment().format('MM'));
            if (this.isInsert) {
                return month;
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: month
                };
            } else {
                this.unset();
            }
        }
    },
    createdYear: {
        type: Number,
        label: "Week of creation date, used in stats",
        autoValue: function() {
            let year = Number(moment().format('YYYY'));
            if (this.isInsert) {
                return year;
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: year
                };
            } else {
                this.unset();
            }
        }
    },
    createdBy: {
        type: String,
        autoValue: function() {
            if (this.isInsert) {
                return this.userId;
            } else if (this.isUpsert) {
                return {
                    $setOnInsert: this.userId
                };
            } else {
                this.unset();
            }
        },
        optional: true
    },
    updatedAt: {
        type: Date,
        autoValue: function() {
            if (this.isUpdate) {
                return new Date();
            }
        },
        denyInsert: true,
        optional: true
    },
    updatedBy: {
        type: String,
        autoValue: function() {
            if (this.isUpdate) {
                return this.userId;
            }
        },
        denyInsert: true,
        optional: true
    }
});

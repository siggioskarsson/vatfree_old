import { mapStyles } from 'meteor/vatfree:core-templates/templates/forms/map';
import { shopHelpers } from "./helpers";
import { algoliaIndex } from '/client/lib/algolia';
import './shops.html';

Template.shopsEmbed.onCreated(function() {
    this.searchTerm = new ReactiveVar('');
    this.mapsReady = new ReactiveVar(false);

    this.mapCenterlng = new ReactiveVar(0);
    this.mapCenterlat = new ReactiveVar(0);

    this.mapSWlng = new ReactiveVar(0);
    this.mapSWlat = new ReactiveVar(0);
    this.mapNElng = new ReactiveVar(0);
    this.mapNElat = new ReactiveVar(0);

    this.shopsCollection = new Meteor.Collection(null);
    this.markers = {};
});

Template.shopsEmbed.onRendered(function() {
    const template = this;

    Tracker.afterFlush(() => {
        GoogleMaps.ready('_map', function (map) {
            // Fix for google maps when initialized hidden
            google.maps.event.trigger(map, 'resize');
            template.infoWindow = new google.maps.InfoWindow();

            const setMapBounds = function() {
                const mapCenter = map.instance.getCenter();
                template.mapCenterlng.set(mapCenter.lng());
                template.mapCenterlat.set(mapCenter.lat());

                const bounds = map.instance.getBounds(),
                    sw = bounds.getSouthWest(),
                    ne = bounds.getNorthEast();
                template.mapSWlng.set(sw.lng());
                template.mapSWlat.set(sw.lat());
                template.mapNElng.set(ne.lng());
                template.mapNElat.set(ne.lat());
            };
            map.instance.addListener("idle", function(e) {
                setMapBounds();
            });
            setMapBounds();

            template.map = map;
            template.mapsReady.set(true);
        });
    });

    let previousSearchTerm = '';
    this.autorun(() => {
        if (!this.mapsReady.get()) {
            return false;
        }
        const searchTerm = this.searchTerm.get(),
            limit = 100,
            offset = 0;

        const mapCenter = [ this.mapCenterlng.get(), this.mapCenterlat.get() ];
        let mapBound = {
            sw: [ this.mapSWlng.get(), this.mapSWlat.get() ],
            ne: [ this.mapNElng.get(), this.mapNElat.get() ],
        };
        if (previousSearchTerm !== searchTerm) {
            // For search term changes by the user we ignore the map bounds
            // TODO: there are use cases where this doesn't make sense but otherwise you would not find shops outside of your bounds
            mapBound = {};
        }

        search(searchTerm, mapCenter, mapBound, limit, offset);
    });

    const search = _.debounce(function(searchTerm, mapCenter, mapBound, limit, offset) {
        let processSearchResults = function (res) {
            const shopIds = [];
            res.forEach(shop => {
                shopIds.push(shop._id);
                if (!template.shopsCollection.findOne({
                    _id: shop._id
                })) {
                    template.shopsCollection.insert(shop);
                    if (shop.geo && shop.geo.coordinates) {
                        template.markers[shop._id] = new google.maps.Marker({
                            map: template.map.instance,
                            icon: {
                                url: '/Logos/' + shopHelpers.getPartnershipStatusLogo.call(shop, shop.p),
                                scaledSize: new google.maps.Size(24, 24),
                            },
                            position: {
                                lat: shop.geo.coordinates[1],
                                lng: shop.geo.coordinates[0]
                            }
                        });

                        template.markers[shop._id].addListener('click', function () {
                            template.infoWindow.setContent(Blaze.toHTMLWithData(Template.shopInfo, shop));
                            template.infoWindow.open(template.map.instance, this);
                        });
                    }
                }
            });
            template.shopsCollection.find().forEach(shop => {
                if (shopIds.indexOf(shop._id) === -1) {
                    template.shopsCollection.remove({
                        _id: shop._id
                    });
                    if (template.markers.hasOwnProperty(shop._id)) {
                        template.markers[shop._id].setMap(null);
                        google.maps.event.clearInstanceListeners(template.markers[shop._id]);
                        delete template.markers[shop._id];
                    }
                }
            });
            if (previousSearchTerm !== searchTerm) {
                // only if the user actually changed the search term
                Tracker.afterFlush(() => {
                    setBoundingArea();
                });
            }
            previousSearchTerm = searchTerm;
        };

        if (algoliaIndex) {
            let shops = [];
            let selector = {
                query: searchTerm,
                hitsPerPage: limit
            };
            if (mapBound && mapBound.sw && mapBound.ne) {
                selector.insideBoundingBox = [
                    [
                        mapBound.sw[0],
                        mapBound.sw[1],
                        mapBound.ne[0],
                        mapBound.ne[1]
                    ]
                ];
            }
            algoliaIndex.search(selector, function searchDone(err, res) {
                if (err) {
                    console.error(err);
                } else {
                    if (res && res.hits) {
                        _.each(res.hits, (shop) => {
                            let shopResult = {
                                _id: shop._id,
                                n: shop.n,
                                a: shop.a,
                                p: shop.p,
                                po: shop.po,
                                r: shop.r,
                            };
                            if (shop._geoloc && shop._geoloc.lat) {
                                shopResult.geo = {
                                    coordinates: [
                                        shop._geoloc.lat,
                                        shop._geoloc.lng
                                    ],
                                };
                            }
                            shops.push(shopResult);
                            processSearchResults(shops);
                        });
                    } else {
                        processSearchResults([]);
                    }
                }
            });
        } else {
            Meteor.call('search-shops-location', searchTerm, mapCenter, mapBound, limit, offset, (err, res) => {
                if (err) {
                    toastr.error(Vatfree.translate(err.reason || err.message));
                    return;
                }

                processSearchResults(res);
            });
        }
    }, 200);

    const setBoundingArea = function() {
        if (!_.size(template.markers)) {
            return false;
        }
        const bounds = new google.maps.LatLngBounds();
        _.each(template.markers, marker => {
            bounds.extend(marker.position);
        });
        template.map.instance.fitBounds(bounds);
    };
});

Template.shopsEmbed.helpers({
    shops() {
        const template = Template.instance();
        return template.shopsCollection.find({}, {
            sort: {
                r: -1,
                po: 1
            }
        });
    },
    partnershipLogo() {
        return shopHelpers.getPartnershipStatusLogo.call(this, this.p);
    },
    getAddressWithoutPostalCode() {
        return this.a.replace('<br/>', ', ').replace(/([0-9]{4} [A-Z]{2})/i, '');
    },
    getMapOptions: function () {
        // Make sure the maps API has loaded
        if (GoogleMaps.loaded()) {
            // Default to overview of NL
            const position = {
                lat: 52.18587185973456,
                lng: 5.370345526935807
            }, zoom = 8;

            // Map initialization options
            const mapOptions = {
                center: position,
                zoom: zoom,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                styles: mapStyles,
                clickableIcons: false,
                maxZoom: 17,
            };

            return mapOptions;
        }
    }
});

Template.shopsEmbed.events({
    'keyup input[name="search"]': _.debounce(function (e, template) {
        e.preventDefault();
        template.searchTerm.set(e.currentTarget.value);
    }, 300),
    'click .shop-item'(e, template) {
        e.preventDefault();

        if (template.markers.hasOwnProperty(this._id)) {
            google.maps.event.trigger(template.markers[this._id], 'click');
        }
    }
});

Template.shopInfo.helpers({
    partnershipLogo() {
        return shopHelpers.getPartnershipStatusLogo.call(this, this.p);
    },
    isPartnerShipStatus(partnershipStatus) {
        return partnershipStatus === this.p;
    }
});

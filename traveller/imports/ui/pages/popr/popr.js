/* global Vatfree: true */
import './popr.html';
import swal from 'sweetalert2';
import moment from 'moment-timezone';

const registerPoprReceipt = function(poprId, poprSecret, callback) {
    Meteor.call('register-popr-receipt-bare', poprId, poprSecret, (err, receiptId) => {
        if (err) {
            toastr.error(Vatfree.translate(err.reason || err.message));
            if (Meteor.user()) {
                FlowRouter.go('/receipts');
                Session.set('poprCode');
            }
        } else {
            toastr.info(Vatfree.translate("Receipt added successfully!"));
            FlowRouter.go('/receipts?receiptId=' + receiptId);
        }

        if (callback) callback(err, receiptId);
    });
};

Template.popr.onCreated(function() {
    this.isLoggingIn = new ReactiveVar();
    this.isLoggingInWithSocial = new ReactiveVar();
});

Template.popr.onRendered(function() {
    this.autorun(() => {
        if (!Meteor.loggingIn()) {
            const poprId = FlowRouter.getParam('poprId');
            const poprSecret = FlowRouter.getParam('poprSecret');

            Tracker.nonreactive(() => {
                if (poprId && poprSecret) {
                    if (Meteor.user()) {
                        Meteor.call('get-popr-info', poprId, poprSecret, (err, poprDoc) => {
                            if (err) {
                                toastr.error(Vatfree.translate(err.reason || err.message));
                                FlowRouter.go('/receipts');
                                Session.set('poprCode');
                            } else {
                                console.log(poprDoc);
                                let html = Vatfree.translate('Please check the receipt details before submitting') + '<br/><br/>';
                                html += `<b>${poprDoc.shop.name}</b><br/>${poprDoc.shop.addressFirst}, ${poprDoc.shop.city}<br/><br/>`;
                                html += '<table style=\'width: 80%; margin-left: 10%;\'>';
                                html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Purchase date") + '</td><td style=\'text-align: right\'>' + moment.tz(new Date(poprDoc.time * 1000), 'Europe/Amsterdam').format(TAPi18n.__('_date_format')) + '</td></tr>';
                                html += '<tr><td colspan=\'2\'>&nbsp;</td></tr>';
                                html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Purchase amount") + '</td><td style=\'text-align: right\'>' + Vatfree.numbers.formatCurrency(poprDoc.totalValue, 2, '€') + '</td></tr>';
                                html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("VAT amount") + '</td><td style=\'text-align: right\'>' + Vatfree.numbers.formatCurrency(poprDoc.totalVAT, 2, '€') + '</td></tr>';
                                html += '</table>';

                                swal({
                                    title: Vatfree.translate("Register receipt?"),
                                    html: html,
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonText: Vatfree.translate("Yes"),
                                    cancelButtonText: Vatfree.translate('Cancel'),
                                    confirmButtonColor: '#93c01f',
                                    reverseButtons: true
                                }).then(function () {
                                    registerPoprReceipt(poprId, poprSecret);
                                }).catch(function() {
                                    FlowRouter.go('/receipts');
                                    Session.set('poprCode');
                                });
                            }
                        });
                    } else {
                        Session.set('poprCode', poprId + '/' + poprSecret);
                    }
                }
            });
        }
    });
});

import './payouts.html';

Template.payouts.onRendered(function() {
    this.autorun(() => {
        this.subscribe('my-payouts', Meteor.userId());
    });
});

Template.payouts.helpers({
    getPayouts() {
        return Payouts.find({
            travellerId: Meteor.userId()
        });
    },
    getPayoutStatus() {
        return Vatfree.translate('payout_' + this.status);
    },
    error() {
        return this.status === 'cancelled';
    },
    getTotalPayoutAmount() {
        let total = 0;
        Payouts.find({
            travellerId: Meteor.userId()
        }).forEach((p) => {
            if (p.status !== 'cancelled') {
                total += Number(p.amount);
            }
        });

        return total;
    }
});

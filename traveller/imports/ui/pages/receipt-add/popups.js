import moment from 'moment-timezone';

export const getAlertPopuHtml = function (formData, travellerType, receiptIsStamped = true) {
    let shop = Shops.findOne({_id: formData.shopId});
    let shopName = shop.name + ' - ' + (shop.addressFirst || '') + ', ' + (shop.postCode || '') + ' ' + (shop.city || '');

    let customsCountry = Countries.findOne({_id: formData.customsCountryId}) || {};
    let countryName = customsCountry.name;
    let currency = Currencies.findOne({_id: formData.currencyId}) || {};
    let currencySymbol = currency.symbol || '€';

    let html = Vatfree.translate('Please check the receipt details before submitting') + '<br/><br/>';
    html += '<b>' + shopName + '</b><br/><br/>';
    html += '<table style=\'width: 80%; margin-left: 10%;\'>';
    if (!travellerType.isNato) {
        if (receiptIsStamped) {
            html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Customs country") + '</td><td style=\'text-align: right\'>' + countryName + '</td></tr>';
            html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Customs stamp date") + '</td><td style=\'text-align: right\'>' + moment.tz(formData.customsDate, 'Europe/Amsterdam').format(TAPi18n.__('_date_format')) + '</td></tr>';
        } else {
            html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Departure country") + '</td><td style=\'text-align: right\'>' + countryName + '</td></tr>';
            html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Expected departure date") + '</td><td style=\'text-align: right\'>' + moment.tz(formData.customsDate, 'Europe/Amsterdam').format(TAPi18n.__('_date_format')) + '</td></tr>';
        }
    }
    html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Purchase date") + '</td><td style=\'text-align: right\'>' + moment.tz(formData.purchaseDate, 'Europe/Amsterdam').format(TAPi18n.__('_date_format')) + '</td></tr>';
    html += '<tr><td colspan=\'2\'>&nbsp;</td></tr>';
    html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Purchase amount") + '</td><td style=\'text-align: right\'>' + Vatfree.numbers.formatCurrency(formData.amount, 2, currencySymbol) + '</td></tr>';
    html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("VAT amount") + '</td><td style=\'text-align: right\'>' + Vatfree.numbers.formatCurrency(formData.totalVat, 2, currencySymbol) + '</td></tr>';
    html += '</table>';

    return html;
};

export const getAddReceiptSuccessPopupHtml = function (receipt, travellerType, receiptIsStamped = true, showUploadPhoto = true) {
    let htmlText = '<b>' + Vatfree.translate("The estimated refund is __refund__", {refund: Vatfree.numbers.formatCurrency(receipt.refund, receipt.currencyId)}) + '</b><br/><br/>';
    htmlText += '<div class=\'receipt-submit\'>';
    htmlText += '<div class=\'receipt-submit-icons\'>';
    if (receiptIsStamped) {
        htmlText += '<i class=\'icon icon-numberon\'></i> › ';
        htmlText += '<i class=\'icon icon-photoreceipt\'></i> › ';
        if (travellerType.isNato) {
            htmlText += '<i class=\'icon icon-makepicform\'></i>';
        } else {
            htmlText += '<i class=\'icon icon-sendin\'></i>';
        }
        htmlText += '</div>';
        htmlText += '<div class=\'receipt-submit-text\'><ol>';
        htmlText += '<li>' + Vatfree.translate("Write this number on the receipt __receiptNr__", {receiptNr: receipt.receiptNr}) + '</li>';
        if (showUploadPhoto) {
            htmlText += '<li>' + Vatfree.translate("Upload a photo of the stamped receipt") + '</li>';
        }
        if (travellerType.isNato) {
            htmlText += '<li>' + Vatfree.translate("Upload a photo of the __certificate_name__", {certificate_name: travellerType.certificateName}) + '</li>';
        } else {
            htmlText += '<li>' + Vatfree.translate("Send the original receipt(s) via post") + '</li>';
        }
        htmlText += '</ol></div></div>';
    } else {
        htmlText += '<div>';
        htmlText += '<img src="/img/receipt-get-stamp.svg" />';
        htmlText += '<div class="receipt-submit-text">';
        htmlText += Vatfree.getWebText('receipt_added_get_stamp_instructions');
        htmlText += '</div>';
        htmlText += '</div>';
        htmlText += '</div>';
    }

    return htmlText;
};

export const getAddCargoReceiptSuccessPopupHtml = function (estimatedRefund, currencyId) {
    let htmlText = '<b>' + Vatfree.translate("The estimated refund is __refund__", {refund: Vatfree.numbers.formatCurrency(estimatedRefund, currencyId)}) + '</b><br/><br/>';
    htmlText += '<div class=\'receipt-submit\'>';
    htmlText += '<div class=\'receipt-submit-icons\'>';
    htmlText += '<i class=\'icon icon-numberon\'></i> › ';
    htmlText += '<i class=\'icon icon-photoreceipt\'></i> › ';
    htmlText += '<i class=\'icon icon-makepicform\'></i>';
    htmlText += '</div>';
    htmlText += '<div class=\'receipt-submit-text\'><ol>';
    htmlText += '<li>' + Vatfree.translate("You can still upload photos") + '</li>';
    htmlText += '</ol></div></div>';

    return htmlText;
};

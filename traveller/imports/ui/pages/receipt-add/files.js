import './files.html';

Template.receipt_add_files.helpers({
    getFileTypes() {
        return this.fileTypes.array();
    },
    getFiles() {
        const subType = this.subType;
        return _.filter(Template.instance().data.files.array(), (file) => { return file.subType === subType; });
    }
});

Template.receipt_add_files.events({
    'change input[name="receiptFile"]': function(e, template) {
        e.preventDefault();
        const file = e.currentTarget.files[0];
        file.subType = this.subType;

        const reader = new FileReader();
        reader.onload = function(fileLoadEvent) {
            file.fileContent = reader.result;
            template.data.files.push(file);
            template.$('input[name="receiptFile"]').val('');
        };
        reader.readAsDataURL(file);
    },
    'click .file-thumb:not(.add-file-button)'(e, template) {
        e.preventDefault();
        let file = this;

        import swal from 'sweetalert2';
        swal({
            title: Vatfree.translate("Remove file?"),
            text: Vatfree.translate("Remove this file from the receipt form?"),
            type: "warning",
            showCancelButton: true,
            cancelButtonText: Vatfree.translate('Cancel'),
            confirmButtonText: Vatfree.translate("Remove file"),
            confirmButtonColor: '#dd6b55',
            reverseButtons: true
        }).then(function () {
            let index = template.data.files.indexOf(file);
            template.data.files.splice(index, 1);
        });
    }
});

import { travellerReceiptAddOnCreated, travellerReceiptAddHelpers, travellerReceiptAddEvents } from './add-helpers';
import './nato-options.html';

Template.receipt_add_nato_options.onCreated(travellerReceiptAddOnCreated);
Template.receipt_add_nato_options.helpers(travellerReceiptAddHelpers);
Template.receipt_add_nato_options.events(travellerReceiptAddEvents);

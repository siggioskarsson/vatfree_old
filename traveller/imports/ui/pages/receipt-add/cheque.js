import { travellerReceiptAddOnRendered, travellerReceiptAddHelpers, travellerReceiptAddEvents } from './add-helpers';
import './cheque.html';

Template.receipt_add_cheque.onRendered(travellerReceiptAddOnRendered);
Template.receipt_add_cheque.helpers(travellerReceiptAddHelpers);
Template.receipt_add_cheque.events(travellerReceiptAddEvents);

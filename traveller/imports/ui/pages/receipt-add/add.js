import {
    travellerReceiptAddOnCreated,
    travellerReceiptAddOnRendered,
    travellerReceiptAddOnDestroyed,
    travellerReceiptAddHelpers,
    travellerReceiptAddEvents
} from './add-helpers';
import './add.html';

Template.receipt_add.onCreated(function() {
    travellerReceiptAddOnCreated.call(this);

    this.autorun(() => {
        this.fileTypes = new ReactiveArray();
        this.fileTypes.push({
            name: Vatfree.translate('Receipt photo'),
            description: Vatfree.getWebText('traveller_receipt_photo_explanation'),
            subType: 'receipt',
            required: true
        });
    });
});

Template.receipt_add.onRendered(function() {
    travellerReceiptAddOnRendered.call(this);
});
Template.receipt_add.helpers(travellerReceiptAddHelpers);
Template.receipt_add.events(travellerReceiptAddEvents);
Template.receipt_add.onDestroyed(function() {
    travellerReceiptAddOnDestroyed.call(this);
});

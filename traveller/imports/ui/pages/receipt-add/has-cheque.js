import './has-cheque.html';

Template.receipt_has_cheque.helpers({
    hasCheque(bool) {
        return this.hasCheque === bool;
    }
});

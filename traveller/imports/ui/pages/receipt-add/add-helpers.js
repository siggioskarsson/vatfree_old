import moment from 'moment-timezone';
import { handleFileUpload } from '../receipts/helpers';
import { algoliaIndex } from '/client/lib/algolia';
import { getAlertPopuHtml, getAddReceiptSuccessPopupHtml } from './popups';

export const initShopIdSelect2 = function (template) {
    let formatPartnershipStatus = function (status) {
        let button = '';
        let partnershipStatus = status.partnershipStatus;
        if (!partnershipStatus && status.id) {
            let shop = Shops.findOne({_id: status.id});
            if (shop) {
                partnershipStatus = shop.partnershipStatus;
            }
        }
        switch (partnershipStatus) {
            case 'new':
                button = 'VFCircleUnkown.svg';
                break;
            case 'pledger':
            case 'affiliate':
                button = 'VFCircleBlue.svg';
                break;
            case 'partner':
                button = 'VFCircleShade.svg';
                break;
            case 'uncooperative':
                button = 'VFCircleNonrefund.svg';
                break;
        }

        if (button) {
            return $(
                '<span class="select2-item has-image"><div class="select2-item-image"><img src="/Logos/' + button + '" class="img-responsive" /></div> <div class="select2-item-text">' + status.text + '</div></span>'
            );
        } else {
            return status.text;
        }
    };
    if (template && template.view && template.view.isDestroyed !== true) {
        template.$('select[name="shopId"]').select2({
            minimumInputLength: 1,
            multiple: false,
            ajax: {
                transport: function (params, success, failure) {
                    let limit = 20;
                    let offset = 0;
                    if (params.data.q) {
                        if (algoliaIndex) {
                            algoliaIndex.search(params.data.q, function searchDone(err, res) {
                                if (err) {
                                    failure(err);
                                } else {
                                    //console.log(res);
                                    let searchResults = [];
                                    if (res && res.hits) {
                                        searchResults = _.map(res.hits, (item) => {
                                            let shopName = item.n + ' - ' + item.a.replace('<br/>', ', ');
                                            return {
                                                name: shopName,
                                                text: shopName,
                                                id: item._id,
                                                partnershipStatus: item.p
                                            };
                                        });
                                    }
                                    success({results: searchResults });
                                }
                            });
                        } else {
                            Meteor.call('search-shops', params.data.q || "", limit, offset, (err, res) => {
                                if (err) {
                                    failure(err);
                                } else {
                                    success({results: res});
                                }
                            });
                        }
                    } else {
                        success({result: []});
                    }
                },
                delay: 500
            },
            language: {
                noResults: function () {
                    return "<div class='select2-no-matches'>" + Vatfree.getWebText('traveller_no_shops_found') + "</div>";
                },
                errorLoading: function () {
                    return Vatfree.translate('The results could not be loaded');
                },
                inputTooShort: function (e) {
                    var t = e.minimum - e.input.length;
                    return Vatfree.translate('Please enter __number_of_chars__ or more characters', {number_of_chars: t});
                },
                loadingMore: function () {
                    return Vatfree.translate('Loading more results___');
                },
                searching: function () {
                    return Vatfree.translate('Searching___');
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function(state) {
                return formatPartnershipStatus(state);
            },
            templateSelection: function(state) {
                return formatPartnershipStatus(state);
            }
        });
    }
};

export const travellerReceiptAddHelpers = {
    showTerms() {
        return Template.instance().showTerms.get();
    },
    receiptIsStamped() {
        return Template.instance().receiptIsStamped.get();
    },
    receiptStampedChoice() {
        return !_.isUndefined(Template.instance().receiptIsStamped.get());
    },
    receiptTypeId() {
        return Template.instance().receiptTypeId.get();
    },
    hasCheque() {
        return Template.instance().hasCheque.get();
    },
    hasChequeChoice() {
        return !_.isUndefined(Template.instance().hasCheque.get());
    },
    addingShop() {
        return Template.instance().addingShop.get();
    },
    getShopId() {
        return Template.instance().shopId.get();
    },
    getShopCountryId(shopId) {
        shopId = shopId || this.shopId || Template.instance().shopId.get();
        let shop = Shops.findOne({_id: shopId});
        return shop ? shop.countryId : "";
    },
    getCountryId() {
        return Template.instance().countryId.get();
    },
    getCurrencySymbol() {
        return Template.instance().currencySymbol.get();
    },
    getCustomsDate() {
        return Template.instance().customsDate.get();
    },
    getFileTypes() {
        return Template.instance().fileTypes;
    },
    getFiles() {
        return Template.instance().files;
    },
    hasFiles(type) {
        type = type || "receipt";
        let files = this.files || Template.instance().files;
        return _.filter(files.array(), (file) => { return file.subType === type; }).length > 0;
    },
    moreReceipts() {
        return Template.instance().moreReceipts.array();
    },
    hasMoreReceipts() {
        return Template.instance().moreReceipts.array().length > 0;
    },
    getCountryVatRates() {
        let template = Template.instance();
        let countryId = this.countryId || (template.countryId ? template.countryId.get() : false);
        let country = Countries.findOne({_id: countryId});
        if (country) {
            let rates = [];
            let index = 20;
            _.each(country.vatRates, (rate) => {
                rate = _.clone(rate);
                rate.index = index++;
                rates.push(rate);
            });

            return rates;
        }
    },
    selectMapLocationCallback() {
        let template = Template.instance();
        return function(place) {
            if (place && place.address_components) {
                template.$('input[name="place"]').val(JSON.stringify(place));
            }
        }
    },
    checqueTypes() {
        return [
            "vatfree.com",
            "direct refund",
            "Detaxe",
            "Easy Tax Free",
            "EU Taxfree",
            "Euro Free Shopping",
            "Eurorefund",
            "GB Taxfree",
            "Global Blue",
            "Innova Tax Free",
            "Kapital Taxfree",
            "Premier Tax Free",
            "Red All",
            "Tax Free Germany",
            "Tax Free Worldwide",
            "Tax Refund For Tourists",
            "Tax Refund Italy",
            "Other"
        ];
    },
    getTravellerType() {
        let traveller = Meteor.user();
        if (traveller && traveller.private) {
            let travellerType = TravellerTypes.findOne({
                _id: traveller.private.travellerTypeId
            }) || {};
            return travellerType;
        }
    },
    isNato() {
        let traveller = Meteor.user();
        if (traveller && traveller.private) {
            let travellerType = TravellerTypes.findOne({
                _id: traveller.private.travellerTypeId
            }) || {};
            return travellerType.isNato;
        }
    },
    isUncooperativeShop(shopId) {
        shopId = shopId || this.shopId || Template.instance().shopId.get();
        let shop = Shops.findOne({_id: shopId}) || {};
        return shop.partnershipStatus === "uncooperative";
    },
    shopIsPartner(shopId) {
        shopId = shopId || this.shopId || Template.instance().shopId.get();
        let shop = Shops.findOne({_id: shopId}) || {};
        return shop.partnershipStatus === "partner";
    }
};

export const travellerReceiptAddOnCreated = function () {
    this.shopId = new ReactiveVar(this.data.shopId);
    this.addingShop = new ReactiveVar(false);
    this.countryId = new ReactiveVar();
    this.currencySymbol = new ReactiveVar('€');
    this.customsDate = new ReactiveVar();
    this.showTerms = new ReactiveVar();
    this.hasCheque = new ReactiveVar();
    this.receiptIsStamped = new ReactiveVar();
    this.receiptTypeId = new ReactiveVar();

    this.moreReceipts = new ReactiveArray();

    this.files = new ReactiveArray();
};

export const travellerReceiptAddOnRendered = function () {
    let template = this;

    this.autorun(() => {
        let shopId = this.shopId ? this.shopId.get() : false;
        if (shopId) {
            this.subscribe('shops_item', shopId);
            Meteor.call('get-shop', shopId, (err, shop) => {
                if (shop) {
                    this.countryId.set(shop.countryId);
                    let currency;
                    if (shop.currencyId) {
                        currency = Currencies.findOne({
                            _id: shop.currencyId
                        });
                    } else {
                        let country = Countries.findOne({
                            _id: shop.countryId
                        });
                        currency = Currencies.findOne({
                            _id: country.currencyId
                        });
                    }
                    if (currency) {
                        $('input[name="countryId"]').val(shop.countryId);
                        $('input[name="currencyId"]').val(currency._id);
                        this.currencySymbol.set(currency.symbol);
                    }
                }
            });
        }
    });

    let ua = navigator.userAgent.toLowerCase();
    let isAndroid = ua.indexOf('android') > -1; //&& ua.indexOf("mobile");

    this.autorun(() => {
        let customsDate = this.customsDate ? this.customsDate.get() : false;
        if (this.cargoTypeChoice) {
            let cargoTypeChoice = this.cargoTypeChoice.get();
        }
        if (this.basicFileTypes && _.isFunction(this.hasBasicFiles)) {
            let hasBasicFiles = this.hasBasicFiles.call(this);
        }
        if (this.addingReceipt) {
            let addingReceipt = this.addingReceipt.get();
        }
        Tracker.afterFlush(() => {
            initShopIdSelect2(template);

            // android resizes the window when keyboard appears, this breaks select2 focus
            if (!isAndroid) {
                window.addEventListener('resize', function () {
                    initShopIdSelect2(template);
                });
            }
        });
    });

    this.autorun(() => {
        let traveller = Meteor.user();
        let travellerType = {};
        if (traveller && traveller.private) {
            travellerType = TravellerTypes.findOne({
                _id: traveller.private.travellerTypeId
            }) || {};
        }
        if (this.receiptTypeId) {
            if (travellerType && travellerType.forceReceiptTypeId) {
                this.receiptTypeId.set(travellerType.forceReceiptTypeId);
            } else {
                this.receiptTypeId.set();
            }
        }

        let shopId = this.shopId ? this.shopId.get() : false;
        let customsDate = this.customsDate ? this.customsDate.get() : false;
        let receiptStamped = this.receiptIsStamped ? this.receiptIsStamped.get() : false;
        if (this.cargoTypeChoice) {
            let cargoTypeChoice = this.cargoTypeChoice.get();
        }
        if (this.basicFileTypes && _.isFunction(this.hasBasicFiles)) {
            let hasBasicFiles = this.hasBasicFiles.call(this);
        }
        Tracker.afterFlush(() => {
            template.$('.input-group.date input[name="purchaseDate"]').closest('.input-group.date').datepicker('destroy');
            template.$('.input-group.date input[name="purchaseDate"]').closest('.input-group.date').datepicker({
                todayBtn: 'linked',
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                endDate: (travellerType.isNato ? moment().format(TAPi18n.__('_date_format')) : moment(customsDate, TAPi18n.__('_date_format')).isAfter(moment()) ? moment().format(TAPi18n.__('_date_format')) : customsDate),
                format: TAPi18n.__('_date_format').toLowerCase(),
                language: Vatfree.getLanguage()
            });
            template.$('.input-group.date input[name="customsDate"]').closest('.input-group.date').datepicker('destroy');
            template.$('.input-group.date input[name="customsDate"]').closest('.input-group.date').datepicker({
                todayBtn: 'linked',
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                endDate: receiptStamped ? moment().format(TAPi18n.__('_date_format')) : null,
                format: TAPi18n.__('_date_format').toLowerCase(),
                language: Vatfree.getLanguage()
            });
            template.$('select[name="customsCountryId"]').select2();
        });
    });

    this.autorun(() => {
        let user = Meteor.user();
        if (user && user.profile && !user.profile.terms) {
            if (this.showTerms) this.showTerms.set(true);
        } else {
            if (this.showTerms) this.showTerms.set(false);
            Tracker.afterFlush(() => {
                initShopIdSelect2(template);
            });
        }
    });
};

export const travellerReceiptAddOnDestroyed = function() {
    try {
        $('select').select2('destroy');
    } catch(e) {}
};

export const prepareAndCheckReceiptFiles = function (uploadFiles, fileTypes) {
    let fileErrors = [];
    _.each(fileTypes, (fileType) => {
        if (fileType.required) {
            if (!_.find(uploadFiles, (file) => { return file.subType === fileType.subType; })) {
                fileErrors.push(fileType.name);
            }
        }
    });
    if (fileErrors.length === 1) {
        toastr.error(Vatfree.translate('Please upload a __file_type__', {file_type: fileErrors[0]}));
        return false;
    } else if (fileErrors.length > 1) {
        toastr.error(Vatfree.translate('Please upload the required files: __file_type_list__', {file_type_list: fileErrors.join(', ')}));
        return false;
    }
};

export const prepareAndCheckReceipt = function (template, formData, travellerType) {
    // fix dates
    let mPurchaseDate = moment.tz(formData.purchaseDate, TAPi18n.__('_date_format'), 'Europe/Amsterdam');
    if (!mPurchaseDate.isValid()) {
        toastr.error(Vatfree.translate('Purchase date is not a valid date'));
        return false;
    }
    formData.purchaseDate = mPurchaseDate.toDate();

    let mCustomsDate = moment.tz(formData.customsDate, TAPi18n.__('_date_format'), 'Europe/Amsterdam');
    if (!mCustomsDate.isValid()) {
        toastr.error(Vatfree.translate('Customs date is not a valid date'));
        return false;
    }
    formData.customsDate = mCustomsDate.toDate();

    // fix numbers
    formData.amount = Math.round(Number(formData.amount) * 100);
    formData.totalVat = Math.round(Number(formData.totalVat) * 100);

    let minimumReceiptAmount = Vatfree.receipts.getMinimumAmount(formData);
    if (formData.amount < minimumReceiptAmount) {
        toastr.error(Vatfree.translate('Receipt amount is below minimum required by country (__minimum_amount__)', { minimum_amount: Vatfree.numbers.formatCurrency(minimumReceiptAmount) } ));
        return false;
    }

    // Checks
    if (!travellerType.isNato && template.receiptIsStamped.get()) {
        if (moment(formData.customsDate).isBefore(moment(formData.purchaseDate))) {
            toastr.error(Vatfree.translate('Customs stamp date cannot be before purchase date'));
            return false;
        }
    }

    if (template.hasCheque.get() === true) {
        formData.receiptTypeId = 'm55R7K4SLTKnoJHBj'; // Cheque type
    }

    if (formData.receiptTypeExtraFields) {
        if (_.has(formData.receiptTypeExtraFields, 'formReceived')) {
            formData.receiptTypeExtraFields.formReceived = formData.receiptTypeExtraFields.formReceived === 'true';
        }
    }
};

export const getTravellerType = function () {
    let traveller = Meteor.user();
    let travellerType = {};
    if (traveller && traveller.private) {
        travellerType = TravellerTypes.findOne({
            _id: traveller.private.travellerTypeId
        }) || {};
    }
    return travellerType;
};

export const travellerReceiptAddEvents = {
    'click .cancel-add-receipt'(e, template) {
        e.preventDefault();
        if (template.moreReceipts && template.moreReceipts.array().length) {
            if (template.addingReceipt.get()) {
                template.addingReceipt.set();
            } else {
                // we cannot cancel directly, since we have at least 1 receipt in waiting
                import swal from 'sweetalert2';
                swal({
                    title: Vatfree.translate("Cancel submission?"),
                    text: Vatfree.translate("Are you sure you want to cancel the whole receipt submission? All info entered will be lost."),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: Vatfree.translate("Cancel submission"),
                    cancelButtonText: Vatfree.translate('Cancel'),
                    confirmButtonColor: '#dd6b55',
                    reverseButtons: true
                }).then(function () {
                    Vatfree.trackEvent('cancel-add-receipt', 'receipts');
                    FlowRouter.go('/receipts');
                });
            }
        } else {
            Vatfree.trackEvent('cancel-add-receipt', 'receipts');
            FlowRouter.go('/receipts');
        }
    },
    'change .accept-terms-check'(e) {
        if ($(e.currentTarget).prop('checked')) {
            $('.accept-terms').attr('disabled', false);
        } else {
            $('.accept-terms').attr('disabled', true);
        }
    },
    'click .accept-terms'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('accepted-terms', 'receipts');
        Meteor.call('accept-terms-and-conditions', (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                Tracker.afterFlush(() => {
                    $(window).scrollTo(0);
                });
            }
        });
    },
    'click .receipt-stamped-yes'(e, template) {
        e.preventDefault();
        template.receiptIsStamped.set(true);
    },
    'click .receipt-stamped-no'(e, template) {
        e.preventDefault();
        template.receiptIsStamped.set(false);
    },
    'click .has-cheque-yes'(e, template) {
        e.preventDefault();
        template.hasCheque.set(true);
    },
    'click .has-cheque-no'(e, template) {
        e.preventDefault();
        template.hasCheque.set(false);
    },
    'change input[name="addShop"]'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('add-shop', 'receipts');
        if ($(e.currentTarget).val()) {
            try {
                $('select').select2('destroy');
            } catch(e) {}
            template.addingShop.set(true);
            $(e.currentTarget).val("");
        }
    },
    'click .cancel-add-shop'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('cancel-add-shop', 'receipts');
        if (template.addingShop) {
            template.addingShop.set(false);
        }
        Tracker.afterFlush(() => {
            initShopIdSelect2(template);
        });
    },
    'submit form[name="add-shop-form"]'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('submit-shop', 'receipts');
        let formData = Vatfree.templateHelpers.getFormData(template);
        if (!formData.geo && !formData.place) {
            toastr.error(Vatfree.translate("Please select the shop by searching for it in the input field"));
            return false;
        }
        let geo = JSON.parse(formData.geo);
        let place = JSON.parse(formData.place);
        if (place && place.address_components) {
            Meteor.call('traveller-add-shop', place, geo, (err, shopId) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    if (template.shopId) template.shopId.set(shopId);
                    if (template.addingShop) template.addingShop.set(false);
                    Tracker.afterFlush(() => {
                        Meteor.call('get-shop', shopId, (err, shop) => {
                            if (err) {
                                console.log(err);
                            } else {
                                let name = shop.name + ' - ' + (shop.addressFirst || "") + ', ' + (shop.postCode || "") + ' ' + (shop.city || "");
                                let $shopId = $('select[name="shopId"]');
                                $shopId.append($("<option></option>")
                                    .attr("value", shopId)
                                    .text(name));
                                $shopId.val(shopId);
                                initShopIdSelect2(template);
                            }
                        });
                    });
                }
            });
        } else {
            toastr.error(Vatfree.translate("Please select a shop from the pull down menu"))
        }
    },

    'change select[name="shopId"]'(e, template) {
        template.shopId.set($(e.currentTarget).val());
    },
    'change input[name="customsDate"]'(e, template) {
        template.customsDate.set($(e.currentTarget).val());
    },
    'change select[name="receiptTypeId"]'(e, template) {
        let receiptTypeId = $(e.currentTarget).val();
        template.selectedReceiptTypeId.set(receiptTypeId);
        Tracker.afterFlush(() => {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square icheckbox_square-blue',
                radioClass: 'iradio_square iradio_square-blue'
            });
        });
    },
    'submit form[name="add-receipt-form"]'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);
        Vatfree.trackEvent('submit-receipt', 'receipts', {amount: formData.amount});

        let travellerType = getTravellerType();
        if (prepareAndCheckReceipt(template, formData, travellerType) === false) {
            return false;
        }
        let uploadFiles = template.files.array();
        if (prepareAndCheckReceiptFiles(uploadFiles, template.fileTypes.array()) === false) {
            return false;
        }

        const receiptIsStamped = template.receiptIsStamped.get();

        import swal from 'sweetalert2';
        let html = getAlertPopuHtml(formData, travellerType, receiptIsStamped);
        // store info, customs stamp date, purchase date , amount ,vat amount
        swal({
            title: Vatfree.translate("Final check"),
            html: html,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: Vatfree.translate("Submit receipt"),
            cancelButtonText: Vatfree.translate('Cancel'),
            confirmButtonColor: '#93c01f',
            reverseButtons: true
        }).then(function () {
            let sourceIdentifier = navigator.userAgent || window.ua;
            Meteor.call('add-traveller-receipt', formData, sourceIdentifier, receiptIsStamped, (err, receiptId) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    FlowRouter.go('/receipts');
                    Tracker.afterFlush(() => {
                        Meteor.call('traveller-get-receipt', receiptId, (err, receipt) => {
                            if (err) {
                                toastr.error(err.reason || err.message);
                            } else {
                                if (uploadFiles) {
                                    let context = {
                                        data: {
                                            receipt: receipt
                                        }
                                    };
                                    _.each(uploadFiles, (uploadFile) => {
                                        handleFileUpload.call(context, uploadFile);
                                    });
                                }
                                let htmlText = getAddReceiptSuccessPopupHtml(receipt, travellerType, receiptIsStamped);
                                swal({
                                    title: Vatfree.translate("Receipt added successfully!"),
                                    html: htmlText,
                                    type: "success",
                                    confirmButtonColor: '#93c01f'
                                });
                            }
                        });
                    });
                }
            });
        });
    }
};

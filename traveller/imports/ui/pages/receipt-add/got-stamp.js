import './got-stamp.html';

Template.receipt_got_stamp.helpers({
    receiptIsStamped(bool) {
        return this.receiptIsStamped === bool;
    }
});

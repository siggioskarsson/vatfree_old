import { travellerReceiptAddHelpers, travellerReceiptAddEvents } from './add-helpers';
import './add-shop.html';

Template.receipt_add_shop.helpers(travellerReceiptAddHelpers);
Template.receipt_add_shop.events(travellerReceiptAddEvents);

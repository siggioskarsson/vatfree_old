import { travellerReceiptAddHelpers } from './add-helpers';
import './details.html';

Template.receipt_add_details.helpers(travellerReceiptAddHelpers);

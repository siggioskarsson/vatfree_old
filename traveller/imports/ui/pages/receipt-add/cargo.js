import {
    travellerReceiptAddOnCreated,
    travellerReceiptAddOnRendered,
    travellerReceiptAddOnDestroyed,
    travellerReceiptAddHelpers,
    travellerReceiptAddEvents,
    prepareAndCheckReceipt,
    prepareAndCheckReceiptFiles,
    getTravellerType
} from './add-helpers';
import { getAddCargoReceiptSuccessPopupHtml } from './popups';
import { handleFileUpload } from '../receipts/helpers';
import swal from 'sweetalert2';
import './cargo.html';

const algoliasearch = require("algoliasearch/dist/algoliasearchLite");

let algoliaIndex = false;
if (Meteor.settings.public.algolia && Meteor.settings.public.algolia.applicationId) {
    let algoliaClient = algoliasearch(Meteor.settings.public.algolia.applicationId, Meteor.settings.public.algolia.apiKey);
    algoliaIndex = algoliaClient.initIndex(Meteor.settings.public.algolia.shopIndex);
}

Template.receipt_cargo.onCreated(function() {
    travellerReceiptAddOnCreated.call(this);
    this.cargoTypeChoice = new ReactiveVar();
    this.addingReceipt = new ReactiveVar(true);
    this.submittingReceipts = new ReactiveVar();
    this.fileTypes = new ReactiveArray();
    this.basicFileTypes = new ReactiveArray();

    this.hasBasicFiles = function () {
        const files = this.files;
        let hasFiles = true;
        _.each(this.basicFileTypes.array(), (basicFile) => {
            if (basicFile.required) {
                const foundFiles = _.filter(files.array(), (file) => { return file.subType === basicFile.subType; });
                hasFiles = hasFiles && !!foundFiles.length;
            }
        });

        return hasFiles;
    };

    this.autorun(() => {
        this.fileTypes.clear();
        this.fileTypes.push({
            name: Vatfree.translate('Receipt photo'),
            description: Vatfree.getWebText('traveller_receipt_photo_explanation'),
            subType: 'receipt',
            required: true
        });
    });

    this.autorun(() => {
        this.basicFileTypes.clear();
        this.files.clear();
        if (_.contains(['ship'], this.cargoTypeChoice.get())) {
            this.basicFileTypes.push({
                name: Vatfree.translate("EX-A document"),
                description: Vatfree.getWebText('traveller_receipt_photo_ex-a_document'),
                subType: 'exa',
                required: true
            });
            this.basicFileTypes.push({
                name: Vatfree.translate("Confirmation of Exit"),
                description: Vatfree.getWebText('traveller_receipt_photo_confirmation_of_exit'),
                subType: 'exit'
            });
        } else {
            this.basicFileTypes.push({
                name: Vatfree.translate("Proof of Delivery"),
                description: Vatfree.getWebText('traveller_receipt_photo_proof_of_delivery'),
                subType: 'pod',
                required: true
            });
        }
    });

    this.autorun(() => {
        _.each(this.moreReceipts.array(), (moreReceipt) => {
            this.subscribe('shops_item', moreReceipt.receipt.shopId);
        });
    });
});

Template.receipt_cargo.onRendered(function() {
    travellerReceiptAddOnRendered.call(this);
});
Template.receipt_cargo.helpers(travellerReceiptAddHelpers);
Template.receipt_cargo.helpers({
    receiptTypeId() {
        return Vatfree.defaults.receiptTypeCargoId || "KuzFRkKwRbvR4275n";
    },
    hasCargoTypeChoice() {
        return !_.isUndefined(Template.instance().cargoTypeChoice.get());
    },
    getCargoTypeChoice() {
        return Template.instance().cargoTypeChoice.get();
    },
    addingReceipt() {
        return Template.instance().addingReceipt.get();
    },
    submittingReceipts() {
        return Template.instance().submittingReceipts.get();
    },
    isActiveCargoTypeSelected(type) {
        return Template.instance().cargoTypeChoice.get() === type ? 'active' : false;
    },
    normalFlow() {
        let template = Template.instance();
        return _.contains(['car', 'plane', 'train'], template.cargoTypeChoice.get());
    },
    basicFileTypes() {
        return Template.instance().basicFileTypes;
    },
    hasBasicFiles() {
        const template = Template.instance();
        return template.hasBasicFiles.call(template);
    }
});

Template.receipt_cargo.events(travellerReceiptAddEvents);
Template.receipt_cargo.events({
    'click .pill-type-option'(e, template) {
        e.preventDefault();
        let id = $(e.currentTarget).attr('id');
        template.cargoTypeChoice.set(id);
    },
    'click .add-another-receipt'(e, template) {
        e.preventDefault();
        template.addingReceipt.set(true);
    },
    'click .add-more-receipts'(e, template) {
        e.preventDefault();
        let formData = Vatfree.templateHelpers.getFormData(template);
        let travellerType = getTravellerType();

        prepareAndCheckReceipt(template, formData, travellerType);

        let uploadFiles = template.files.array();
        if (prepareAndCheckReceiptFiles(uploadFiles, template.fileTypes.array()) === false) {
            return false;
        }
        if (uploadFiles === false) {
            return false;
        }

        let files = _.filter(uploadFiles, (file) => { return file.subType === 'receipt'; });
        template.moreReceipts.push({
            receipt: formData,
            files: files
        });

        // reset form
        template.shopId.set();
        template.addingReceipt.set(false);
        template.$('select[name="shopId"]').val('').trigger('change');

        let index = 0;
        _.each(template.files.array(), (file) => {
            if (file.subType === 'receipt') {
                template.files.splice(index, 1);
            } else {
                index++;
            }
        });
    },
    'click .remove-more-receipt'(e, template) {
        e.preventDefault();
        let index = template.moreReceipts.array().indexOf(this);
        import swal from 'sweetalert2';
        swal({
            title: Vatfree.translate("Remove receipt?"),
            text: Vatfree.translate("Remove this receipt from the submission?"),
            type: "warning",
            showCancelButton: true,
            cancelButtonText: Vatfree.translate('Cancel'),
            confirmButtonText: Vatfree.translate("Remove receipt"),
            confirmButtonColor: '#dd6b55',
            reverseButtons: true
        }).then(function () {
            template.moreReceipts.splice(index, 1);
        });
    },
    'submit form[name="add-cargo-receipt-form"]'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('submit-cargo-receipt', 'receipts', {type: 'cargo'});

        template.submittingReceipts.set(true);
        Meteor.setTimeout(() => {
            // submit shit
            let estimatedRefund = 0;
            let currencyId = '';
            let receipts = template.moreReceipts.array();
            const numberOfReceipts = receipts.length;
            let currentReceiptNr = 0;
            _.each(receipts, (receiptSubmission) => {
                let receipt = receiptSubmission.receipt;
                let files = receiptSubmission.files;

                let sourceIdentifier = navigator.userAgent || window.ua;
                let submitErrors = [];
                Meteor.call('add-traveller-receipt', receipt, sourceIdentifier, (err, receiptId) => {
                    if (err) {
                        submitErrors.push(err.reason || err.message);
                        currentReceiptNr++;
                    } else {
                        Tracker.afterFlush(() => {
                            Meteor.call('traveller-get-receipt', receiptId, (err, insertedReceipt) => {
                                if (err) {
                                    submitErrors.push(err.reason || err.message);
                                } else {
                                    estimatedRefund+= insertedReceipt.refund || 0;
                                    currencyId = insertedReceipt.currencyId;
                                    if (files) {
                                        let context = {
                                            data: {
                                                receipt: insertedReceipt
                                            }
                                        };
                                        _.each(files, (uploadFile) => {
                                            handleFileUpload.call(context, uploadFile);
                                        });
                                        let basicFiles = template.files.array();
                                        _.each(basicFiles, (uploadFile) => {
                                            handleFileUpload.call(context, uploadFile);
                                        });
                                    }
                                }
                                currentReceiptNr++;
                                if (currentReceiptNr >= numberOfReceipts) {
                                    let htmlText = getAddCargoReceiptSuccessPopupHtml(estimatedRefund, currencyId);
                                    swal({
                                        title: Vatfree.translate("Receipts have been added successfully!"),
                                        html: htmlText,
                                        type: "success",
                                        confirmButtonColor: '#93c01f'
                                    });
                                    template.submittingReceipts.set(false);
                                    FlowRouter.go('/receipts');
                                }
                            });
                        });
                    }
                });
            });
        }, 1);
    }
});

Template.receipt_cargo.onDestroyed(function() {
    travellerReceiptAddOnDestroyed.call(this);
});

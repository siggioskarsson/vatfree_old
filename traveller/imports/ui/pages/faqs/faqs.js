import './faqs.html';

Template.faqsView.onCreated(function() {
    this.searchTerm = new ReactiveVar('');
});

Template.faqsView.onRendered(function() {
    this.autorun(() => {
        this.subscribe('public-faqs', false, Vatfree.getLanguage());
    });
});

let getSelector = function (template, language) {
    let selector = {};

    if (template.data.selectedId) {
        selector._id = template.data.selectedId;
    }

    let searchTerm = template.searchTerm.get();
    if (searchTerm) {
        let subjectSelector = {};
        Vatfree.search.addSearchTermSelector(searchTerm, subjectSelector, 'subject.' + language);

        let textSelector = {};
        Vatfree.search.addSearchTermSelector(searchTerm, textSelector, 'text.' + language);

        selector['$or'] = [
            subjectSelector,
            textSelector
        ]
    }
    return selector;
};

Template.faqsView.helpers({
    embeddedFaq() {
        return window.location.pathname.match('embed') ? '/embed' : '';
    },
    getGroups() {
        let template = Template.instance();
        let language = Vatfree.getLanguage();
        let selector = getSelector(template, language);

        let groupIds = [];
        FAQS.find(selector).forEach((faq) => {
            if (!_.contains(groupIds, faq.groupId)) {
                groupIds.push(faq.groupId);
            }
        });

        return FAQGroups.find({
            _id: {$in: groupIds}
        }, {
            sort: {
                order: 1
            }
        });
    },
    getFaqs() {
        let template = Template.instance();
        let language = Vatfree.getLanguage();
        let selector = getSelector(template, language);

        selector.groupId = this._id;

        return FAQS.find(selector, {
            sort: {
                ['subject.' + language]: 1
            }
        });
    },
    getGroupName() {
        let language = Vatfree.getLanguage();
        let name = this.name[language];
        if (!name) name = this.name['en'];

        return name;
    },
    getQuestion() {
        let language = Vatfree.getLanguage();
        let subject = this.subject[language];
        if (!subject) subject = this.subject['en'];

        return subject;
    },
    getAnswer() {
        let language = Vatfree.getLanguage();
        let text = this.text[language];
        if (!text) text = this.text['en'];

        return text;
    },
    getQuestionForUrl() {
        let language = Vatfree.getLanguage();
        let subject = this.subject[language];
        if (!subject) subject = this.subject['en'];
        return (subject || "").replace(/[^a-z]/ig, '_').replace(/ /g, '_');
    }
});

Template.faqsView.events({
    'keyup input[name="search"]': _.debounce(function (e, template) {
        e.preventDefault();
        template.searchTerm.set(e.currentTarget.value);
    }, 200)
});

Template.faq.helpers({
    getSelectedId() {
        return FlowRouter.getParam('_id');
    }
});

import moment from 'moment';
import { handleFileUpload, receiptHelpers } from './helpers';
import './info.html';

Template.receiptInfo.onCreated(function () {
    this.isUploadingFile = new ReactiveVar();
    this.showReceiptIsStampedModal = new ReactiveVar();
});
Template.receiptInfo.onRendered(function () {
    this.autorun(() => {
        let data = Template.currentData();
        this.subscribe('receipt-files', data.receipt._id);
        this.subscribe('receipt-rejections');
    });
});

Template.receiptInfo.helpers(receiptHelpers);
Template.receiptInfo.helpers({
    isUploadingFile() {
        return Template.instance().isUploadingFile.get();
    },
    showReceiptIsStampedModal() {
        return Template.instance().showReceiptIsStampedModal.get();
    },
    getFiles() {
        let receiptId = this._id;
        return Files.find({
            itemId: receiptId
        }, {
            sort: {
                createdAt: 1
            }
        });
    },
    getLargePhotoUrl() {
        if (this.copies && this.copies['vatfree'] && this.copies['vatfree'].size > 0) {
            return this.url({store: 'vatfree'});
        } else if (this.copies && this.copies['vatfree-original'] && this.copies['vatfree-original'].size > 0) {
            return this.url({store: 'vatfree-original'});
        } else {
            return this.url({store: 'vatfree-thumbs'});
        }
    },
    getCountry() {
        return Countries.findOne({
            _id: this.profile.countryId
        });
    },
    passportExpired() {
        return moment(this.profile.passportExpirationDate).isBefore(moment());
    },
    currentTraveller() {
        return Travellers.findOne({_id: Meteor.userId()});
    },
    userIsVerified() {
        return this.private && this.private.status ? this.private.status === 'userVerified' : false;
    },
    getStatusDescription() {
        if (this.private && this.private.status) {
            let travellerType = TravellerTypes.findOne({_id: this.private.travellerTypeId}) || {};
            if (!(travellerType.isNato && this.private.status === 'waitingForDocuments')) {
                return Vatfree.translate('traveller_status_description_' + this.private.status) || "";
            }
        }
    },
    statusRejected() {
        return this.status === 'rejected' || this.status === 'notCollectable';
    },
    getReceiptRejections() {
        let selector = {};
        if (this.receiptRejectionIds && this.receiptRejectionIds.length > 0) {
            selector['_id'] = {
                $in: this.receiptRejectionIds
            };
            return ReceiptRejections.find(selector, {
                sort: {
                    name: 1
                }
            });
        }
    },
    getReceiptRejectionText() {
        const language = Vatfree.getLanguage() || 'en';
        return this.text[language] || this.text['en'];
    },
    receiptIsApproved() {
        return _.contains([
            'readyForInvoicing',
            'waitingForPayment',
            'paidByShop',
            'requestPayout',
            'paid'], this.status);
    },
    allowUploads() {
        let receipt = Template.instance().data.receipt;
        return Vatfree.allowTravellerUpload(receipt);
    },
    getTravellerType() {
        let traveller = Meteor.user();
        if (traveller && traveller.private) {
            let travellerType = TravellerTypes.findOne({
                _id: traveller.private.travellerTypeId
            }) || {};
            return travellerType;
        }
    },
    isNatoReceipt() {
        let receiptType = ReceiptTypes.findOne({_id: this.receiptTypeId});
        return receiptType ? receiptType.name === "NATO" : false;
    },
    getTravellerStatus() {
        let traveller = Meteor.user() || {};
        if (traveller.private && traveller.private.status !== 'userVerified' && _.contains([
                'userPending',
                'waitingForOriginal',
                'visualValidationPending',
                'waitingForDoubleCheck',
                'shopPending',
                'readyForInvoicing',
                'waitingForPayment',
                'userPendingForPayment'
            ], this.status)) {
            return Vatfree.translate("userPending");
        } else {
            return Vatfree.translate(this.status);
        }
    },
    allowDeleteReceipt() {
        return Vatfree.receipts.travellerAllowedDeleted(this);
    },
    isCargoReceipt() {
        return Vatfree.receipts.isCargo(this);
    },
    isReceiptStamped() {
        return this.status !== 'waitingForCustomsStamp';
    }
});

Template.receiptInfo.events({
    'click .list-group-item .dropzone .add-file'(e) {
        e.preventDefault();
        e.stopPropagation();
        $('input[name="dropzone-file"]').trigger('click');
    },
    'click .popup-upload-files .dropzone .add-file'(e) {
        e.preventDefault();
        e.stopPropagation();
        $('input[name="dropzone-file"]').trigger('click');
    },
    'change input[name="dropzone-file"]'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        FS.Utility.eachFile(e, function (file) {
            handleFileUpload.call(template, file);
        });
    },
    'dropped .dropzone'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        $('.dropzone').removeClass('visible');
        FS.Utility.eachFile(e, function (file) {
            handleFileUpload.call(template, file);
        });
    },
    'click .receipt-is-stamped'(e, template) {
        e.preventDefault();
        template.showReceiptIsStampedModal.set(true);
    },
    'click .close-submit-receipt-modal'(e, template) {
        e.preventDefault();
        template.showReceiptIsStampedModal.set();
    },
    'click .reopen-receipt'(e, template) {
        e.preventDefault();
        const receiptId = this._id;
        import swal from 'sweetalert2';
        swal({
            title: Vatfree.translate("Reopen the receipt?"),
            text: Vatfree.translate("The receipt will be re-evaluated for refund_ You will be able to upload scans for the receipt_"),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#93c01f",
            cancelButtonText: Vatfree.translate("Cancel"),
            confirmButtonText: Vatfree.translate("Yes, reopen the receipt!"),
            closeOnConfirm: true,
            reverseButtons: true
        }).then(function () {
            Meteor.call('receipt-reopen', receiptId, (err, result) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    toastr.success(Vatfree.translate("Receipt reopened"));
                }
            });
        });
    },
    'click .delete-file'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('deleted-file', 'receipts');
        let fileId = this._id;
        import swal from 'sweetalert2';
        swal({
            title: Vatfree.translate("Are you sure?"),
            text: Vatfree.translate("You will not be able to recover this file!"),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: Vatfree.translate("Cancel"),
            confirmButtonText: Vatfree.translate("Yes, delete it!"),
            closeOnConfirm: true,
            reverseButtons: true
        }).then(function () {
            Meteor.call('delete-receipt-image', fileId, (err, result) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    toastr.success(Vatfree.translate("File deleted"));
                }
            });
        });
    },
    'click .delete-receipt'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('deleted-receipt', 'receipts');
        let receiptId = this._id;
        import swal from 'sweetalert2';
        swal({
            title: Vatfree.translate("Are you sure?"),
            text: Vatfree.translate("You will not be able to recover this receipt!"),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: Vatfree.translate("Cancel"),
            confirmButtonText: Vatfree.translate("Yes, delete it!"),
            closeOnConfirm: true,
            reverseButtons: true
        }).then(function () {
            Meteor.call('traveller-delete-receipt', receiptId, (err, result) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    toastr.success(Vatfree.translate("Receipt deleted"));
                }
            });
        });
    }
});

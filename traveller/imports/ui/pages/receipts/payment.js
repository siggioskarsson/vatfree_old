let cardValidator = require('card-validator');
let Cleave = require('cleave.js');
import './payment.html';

Template.paymentMethodModal.onCreated(function() {
    this.parentTemplate = Template.instance().parentTemplate(1);

    let template = this;
    this.hideModal = () => {
        $('#modal-payment-method').modal('hide');
    };

    Tracker.afterFlush(() => {
        $('#modal-payment-method').on('hidden.bs.modal', function () {
            template.parentTemplate.setPaymentMethod.set(false);
        });
        $('#modal-payment-method').on('shown.bs.modal', function () {
        });
        $('#modal-payment-method').modal('show');
    });
});

Template.paymentMethodModal.onDestroyed(function() {
    this.hideModal();

    // sometimes the modal backdrop is not gone, because the template is destroyed before the modal has removed it
    $('.modal-backdrop').remove();
});

Template.paymentMethodModal_body.onCreated(function() {
    this.parentTemplate = Template.instance().parentTemplate(1);
    this.paymentMethodSelected = new ReactiveVar();
    this.error = new ReactiveVar();
    this.doingPayout = new ReactiveVar();

    this.creditcardType = new ReactiveVar();

    this.hideModal = () => {
        this.parentTemplate.hideModal();
    };
});

Template.paymentMethodModal_body.onRendered(function() {
    this.autorun(() => {
        let paymentMethod = this.paymentMethodSelected.get();
        // reset error when this changes
        this.error.set();
        this.creditcardType.set();
    });
});

Template.paymentMethodModal_body.helpers({
    getError() {
        return Template.instance().error.get();
    },
    doingPayout() {
        return Template.instance().doingPayout.get();
    },
    paymentMethodSelected() {
        return Template.instance().paymentMethodSelected.get();
    },
    isPaymentMethodSelected(method) {
        return Template.instance().paymentMethodSelected.get() === method;
    },
    isActiveCreditcard() {
        return Template.instance().creditcardType.get() ? 'active' : false;
    },
    isActiveCreditcardType(type) {
        return Template.instance().creditcardType.get() === type ? 'active': false;
    },
    creditcardError() {
        let type = Template.instance().creditcardType.get();
        if (type && type !== 'mastercard' && type !== 'master-card' && type !== 'visa') {
            return Vatfree.translate("Card not supported");
        }
    },
    getPayoutPerCurrency() {
        let euroCurrency = Currencies.findOne({code: "EUR"});

        let payout = {};
        Receipts.find({
            userId: Meteor.userId(),
            status: 'paidByShop',
            payoutId: {
                $exists: false
            },
            invoiceId: {
                $exists: true
            }
        }).forEach((receipt) => {
            if (receipt.euroRefund) {
                receipt.refund = receipt.euroRefund;
                receipt.currencyId = euroCurrency._id;
            }
            if (!payout[receipt.currencyId]) {
                payout[receipt.currencyId] = {
                    amount: 0,
                    currencyId: receipt.currencyId
                };
            }
            payout[receipt.currencyId].amount += receipt.refund;
        });

        return _.values(payout);
    },
    transferAmount() {
        let payout = 0;
        Receipts.find({
            userId: Meteor.userId(),
            status: 'paidByShop',
            payoutId: {
                $exists: false
            },
            invoiceId: {
                $exists: true
            }
        }).forEach((receipt) => {
            if (receipt.euroRefund) {
                receipt.refund = receipt.euroRefund;
            }
            payout += receipt.refund;
        });

        let costs = Number(Meteor.settings.public.payoutCosts[Template.instance().paymentMethodSelected.get()] || 0);
        if (costs) {
            payout -= costs;
        }

        return payout;
    },
    getReceiptNrList() {
        let payoutReceiptNrs = [];
        Receipts.find({
            userId: Meteor.userId(),
            status: 'paidByShop',
            payoutId: {
                $exists: false
            },
            invoiceId: {
                $exists: true
            }
        }).forEach((receipt) => {
            payoutReceiptNrs.push(receipt.receiptNr);
        });

        return payoutReceiptNrs.join(', ');
    }
});

Template.paymentMethodModal_body.events({
    'keyup input[name="paymentNr"]'(e, template) {
        let payoutNr = template.$('input[name="paymentNr"]').val();
        let type = template.paymentMethodSelected.get();
        if (type === 'creditcard') {
            if (payoutNr) {
                payoutNr = payoutNr.replace(/[ _]/g, '');
                let numberValidation = cardValidator.number(payoutNr);
                if (numberValidation && numberValidation.card) {
                    template.creditcardType.set(numberValidation.card.type);
                } else if (payoutNr.length > 2) {
                    template.creditcardType.set("unknown");
                }
            } else {
                template.creditcardType.set();
            }
            console.log('template.creditcardType', template.creditcardType.get());
        }
    },
    'keyup input[type="text"]'(e, template) {
        template.error.set();
    },
    'click .payment-option'(e, template) {
        e.preventDefault();
        let type = $(e.currentTarget).attr('id');
        template.paymentMethodSelected.set(type);
        Tracker.afterFlush(() => {
            if (type === 'creditcard') {
                //template.$('input[name="paymentNr"]').mask('9999 9999 9999 9999');
                var cleave = new Cleave('input[name="paymentNr"]', {
                    creditCard: true
                });
            } else if (type === 'sepa') {
                // iban mask: [a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}
                //$.mask.definitions['c'] = "[a-zA-Z0-9]";
                //template.$('input[name="paymentNr"]').mask('aa99 cccc 9999 999c cccc cccc cccc ccc');
                var cleave = new Cleave('input[name="paymentNr"]', {
                    delimiters: [' '],
                    blocks: [4, 4, 4, 4, 4, 4, 4, 3]
                });
            } else {
                //template.$('input[name="paymentNr"]').mask('a?');
                var cleave = new Cleave('input[name="paymentNr"]', {
                    email: true
                });
            }
        });
    },
    'click .close-tasks'(e, template) {
        e.preventDefault();
        template.paymentMethodSelected.set();
        template.hideModal();
    },
    'submit form[name="payment-method-form"]'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('payout-requested', 'receipts');
        template.doingPayout.set(true);
        let paymentMethod = template.paymentMethodSelected.get();
        let paymentNr = template.$('input[name="paymentNr"]').val();
        let paymentAccountName = template.$('input[name="paymentAccountName"]').val() || "";

        const triggerPayoutError = function(paymentMethod, template) {
            if (paymentMethod === 'creditcard') {
                template.error.set(Vatfree.translate('Invalid credit card number_ Please check your input and try again_'));
                Vatfree.trackEvent('payout-requested-error', 'receipts', {error: Vatfree.translate('Invalid credit card number')});
            } else if (paymentMethod === 'paypal') {
                template.error.set(Vatfree.translate('Invalid email address_ Please check your input and try again_'));
                Vatfree.trackEvent('payout-requested-error', 'receipts', {error: Vatfree.translate('Invalid email address')});
            } else if (paymentMethod === 'sepa') {
                template.error.set(Vatfree.translate('Invalid SEPA number_ Please check your input and try again_'));
                Vatfree.trackEvent('payout-requested-error', 'receipts', {error: Vatfree.translate('Invalid SEPA bank account')});
            } else if (paymentMethod === 'wechat') {
                template.error.set(Vatfree.translate('Invalid WeChat account_ Please check your input and try again_'));
                Vatfree.trackEvent('payout-requested-error', 'receipts', {error: Vatfree.translate('Invalid WeChat account')});
            } else if (paymentMethod === 'alipay') {
                template.error.set(Vatfree.translate('Invalid AliPay account_ Please check your input and try again_'));
                Vatfree.trackEvent('payout-requested-error', 'receipts', {error: Vatfree.translate('Invalid AliPay account')});
            } else {
                template.error.set(Vatfree.translate('An error occurred_ Please check your input and try again_'));
                Vatfree.trackEvent('payout-requested-error', 'receipts', {error: Vatfree.translate('error occurred')});
            }
        };

        if (Vatfree.payouts.validatePayout(paymentMethod, paymentNr)) {
            if (paymentMethod === "creditcard") {
                paymentNr = paymentNr.replace(/[_ ]/g, '');
            } else if (paymentMethod === 'paypal') {
                paymentNr = paymentNr.replace(/[ ]/g, '');
            } else if (paymentMethod === 'sepa') {
                paymentNr = paymentNr.replace(/[_ ]/g, '');
            } else if (paymentMethod === 'wechat') {
                paymentNr = paymentNr.replace(/[_ ]/g, '');
            } else if (paymentMethod === 'alipay') {
                paymentNr = paymentNr.replace(/[_ ]/g, '');
            }
        } else {
            triggerPayoutError(paymentMethod, template);
            template.doingPayout.set();
            return false;
        }

        // we need to also validate server side as some payouts need to be checked against an api
        Meteor.call('is-valid-payout', paymentMethod, paymentNr, paymentAccountName, (err, isValid) => {
            if (err || !isValid) {
                console.log(err);
                triggerPayoutError(paymentMethod, template);
                template.doingPayout.set();
                return false;
            } else {
                const payoutMethods = {
                    creditcard: "Credit Card",
                    paypal: "Paypal",
                    sepa: "SEPA bank transfer",
                    wechat: "WeChat",
                    alipay: "AliPay"
                };

                let html = Vatfree.translate('Please verify your payout before continuing') + '<br/><br/>';
                html += '<table style=\'width: 80%; margin-left: 10%;\'>';
                html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Payout method") + '</td><td style=\'text-align: right\'>' + Vatfree.translate(payoutMethods[paymentMethod]) + '</td></tr>';
                html += '<tr><td style=\'text-align: left\'>' + Vatfree.translate("Payout account") + '</td><td style=\'text-align: right\'>' + paymentNr + '</td></tr>';
                html += '</table>';

                import swal from 'sweetalert2';
                swal({
                    title: Vatfree.translate("Are you sure?"),
                    html: html,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: Vatfree.translate("Payout refunds"),
                    cancelButtonText: Vatfree.translate('Cancel'),
                    confirmButtonColor: '#93c01f',
                    reverseButtons: true
                }).then(function () {
                    Vatfree.trackEvent('payout-requested-send', 'receipts');
                    Meteor.call('request-payout', paymentMethod, paymentNr, paymentAccountName, (err, result) => {
                        if (err) {
                            alert(Vatfree.translate(err.reason || err.message));
                            Vatfree.trackEvent('payout-requested-send-error', 'receipts', {error: err.reason || err.message});
                            template.doingPayout.set();
                        } else {
                            Meteor.setTimeout(() => {
                                swal({
                                    title: Vatfree.translate("Your payout has been requested"),
                                    text: Vatfree.translate("We will proceed with paying out your refunds_ This process can take up to 15 days_ Thank you for using vatfree")
                                });
                            }, 200);
                            template.doingPayout.set();
                            template.paymentMethodSelected.set();
                            template.hideModal();
                        }
                    });
                }).catch(function() {
                    template.doingPayout.set();
                });
            }
        });
    }
});

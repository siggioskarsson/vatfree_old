import './receipts.html';

Template.receipts.onCreated(function() {
    this.activeItem = new ReactiveVar(Session.get(this.view.name + '.activeItem'));
    this.setPaymentMethod = new ReactiveVar();
});

Template.receipts.onRendered(function() {
    Vatfree.trackEvent('view', 'receipts');
    this.autorun(() => {
        let data = Template.currentData();
        let user = Meteor.user();
        this.subscribe('my-receipts', "", {createdAt: -1}, 1000, 0);
    });

    this.autorun(() => {
        if (this.subscriptionsReady()) {
            if (!this.activeItem.get()) {
                let item = Receipts.findOne({}, {
                    sort: {
                        createdAt: -1
                    }
                });
                if (item) {
                    this.activeItem.set(item._id);
                }
            }
        }
    });
});

Template.receipts.helpers({
    setPaymentMethod() {
        return Template.instance().setPaymentMethod.get();
    },
    receipts() {
        return Receipts.find({
            userId: Meteor.userId()
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getTotalRefundAmounts() {
        let euroCurrency = Currencies.findOne({code: "EUR"});

        let refund = {};
        Receipts.find({
            userId: Meteor.userId()
        }).forEach((receipt) => {
            if (receipt.currencyId && receipt.status !== 'rejected' && receipt.status !== 'notCollectable' && receipt.refund) {
                if (receipt.euroRefund) {
                    receipt.refund = receipt.euroRefund;
                    receipt.currencyId = euroCurrency._id;
                }
                if (!refund[receipt.currencyId]) refund[receipt.currencyId] = 0;
                refund[receipt.currencyId] += receipt.refund;
            }
        });

        return _.map(refund, (rr, currencyId) => {
            return {
                refund: rr,
                currencyId: currencyId
            }
        });
    },
    showSendReceipts() {
        let traveller = Meteor.user();
        if (traveller && traveller.private) {
            let travellerType = TravellerTypes.findOne({
                    _id: traveller.private.travellerTypeId
                }) || {};
            if (travellerType.isNato) {
                return false;
            }
        }

        let showSend = false;
        Receipts.find({
            userId: Meteor.userId()
        }).forEach((receipt) => {
            let sendReceipt = !receipt.originalsCheckedBy && !_.contains(['rejected', 'depreciated', 'notCollectable', 'deleted'], receipt.status);
            showSend = showSend || sendReceipt;
        });

        return showSend;
    },
    getShop() {
        return Shops.findOne({
            _id: this.shopId
        });
    },
    getActiveItem() {
        let template = Template.instance();
        let itemId = template.activeItem.get();
        if (itemId) {
            return Receipts.findOne({
                _id: itemId
            });
        }
    },
    isActiveItem() {
        let activeItem = Template.instance().activeItem;
        if (!activeItem) {
            activeItem = Template.instance().parentTemplate().activeItem;
        }
        if (activeItem) {
            return activeItem.get() === this._id ? 'active' : '';
        }
    },
    currentTraveller() {
        return Travellers.findOne({_id: Meteor.userId()});
    },
    userIsVerified() {
        return this.private && this.private.status ? this.private.status === 'userVerified' : false;
    },
    getStatusDescription() {
        if (this.private && this.private.status) {
            let travellerType = TravellerTypes.findOne({_id: this.private.travellerTypeId}) || {};
            if (!(travellerType.isNato && this.private.status === 'waitingForDocuments')) {
                return Vatfree.translate('traveller_status_description_' + this.private.status) || "";
            }
        }
    },
    receiptIsApproved() {
        let traveller = Meteor.user() || {};
        return (traveller.private && traveller.private.status === 'userVerified') && _.contains([
            'readyForInvoicing',
            'waitingForPayment',
            'paidByShop',
            'requestPayout',
            'paid'], this.status);
    },
    showRefund() {
        return !_.contains([
            'rejected',
            'notCollectable'
        ], this.status);
    },
    getReceiptsReadyForPayout() {
        return Receipts.find({
            userId: Meteor.userId(),
            status: 'paidByShop',
            payoutId: {
                $exists: false
            },
            invoiceId: {
                $exists: true
            }
        }).fetch() || false;
    },
    getPayoutPerCurrency() {
        let euroCurrency = Currencies.findOne({code: "EUR"});

        let payout = {};
        _.each(this, (receipt) => {
            if (receipt.euroRefund) {
                receipt.refund = receipt.euroRefund;
                receipt.currencyId = euroCurrency._id;
            }
            if (!payout[receipt.currencyId]) {
                payout[receipt.currencyId] = {
                    amount: 0,
                    currencyId: receipt.currencyId
                };
            }
            payout[receipt.currencyId].amount += receipt.refund;
        });

        return _.values(payout);
    },
    getReceiptNrList() {
        let payoutReceiptNrs = [];
        _.each(this, (receipt) => {
            payoutReceiptNrs.push(receipt.receiptNr);
        });

        return payoutReceiptNrs.join(', ');
    },
    getTravellerStatus() {
        let traveller = Meteor.user() || {};
        return Vatfree.receipts.getTravellerStatusForTraveller(this, traveller);
    }
});

Template.receipts.events({
    'click .add-receipt'(e) {
        e.preventDefault();
        Vatfree.trackEvent('add', 'receipts');
        let travellerType = Vatfree.travellers.getTravellerType(Meteor.user());
        if (travellerType.subType === 'cargo') {
            FlowRouter.go('/receipt/cargo');
            return;
        }

        FlowRouter.go('/receipt/add');
    },
    'click .item-row'(e, template) {
        template.activeItem.set(this._id);
        Vatfree.trackEvent('view-info', 'receipts');
        $('#wrapper').animate({
            scrollTop: -$("#page-wrapper").offset().top + $(".receipt-info").offset().top - 50
        }, 500);
    },
    'click .request-payout'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('request-payout', 'receipts');
        template.setPaymentMethod.set(true);
    }
});

import moment from 'moment-timezone';
import './popup-receipt-stamped.html';
import { getAddReceiptSuccessPopupHtml } from '../receipt-add/popups';

Template.popup_receipt_stamped.onRendered(function() {
    Tracker.afterFlush(() => {
        this.$('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: TAPi18n.__('_date_format').toLowerCase(),
            language: Vatfree.getLanguage()
        });
    });
});

Template.popup_receipt_stamped.helpers({
    getFiles() {
        let receiptId = this.receipt._id;
        return Files.find({
            itemId: receiptId
        }, {
            sort: {
                createdAt: 1
            }
        });
    },
    isUploadingFile() {
        return Template.instance().parentTemplate().isUploadingFile.get();
    },
});

Template.popup_receipt_stamped.events({
    'submit form[name="receipt-is-stamped-form"]'(e, template) {
        e.preventDefault();
        const receiptId = this.receipt._id;
        const traveller = Meteor.user();
        const travellerType = TravellerTypes.findOne({_id: traveller.private.travellerTypeId }) || {};

        let formData = Vatfree.templateHelpers.getFormData(template, $(e.currentTarget));

        let customsDate = formData.customsDate;
        customsDate = moment.tz(customsDate, TAPi18n.__('_date_format'), 'Europe/Amsterdam');
        if (!customsDate.isValid()) {
            alert(Vatfree.translate('Customs date is not a valid date'));
            return false;
        }

        if (moment(this.receipt.purchaseDate).isAfter(customsDate)) {
            alert(Vatfree.translate('Customs stamp cannot be before the purchase date'));
            Vatfree.trackEvent('submit-receipt-error', 'receipts', {error: Vatfree.translate("Customs stamp before purchase date")});
            return false;
        }

        Meteor.call('receipt-is-stamped', receiptId, customsDate.toDate(), (err, result) => {
            if (err) {
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                template.$('i.close-modal').trigger('click');
                Meteor.setTimeout(() => {
                    const receipt = Receipts.findOne({_id: receiptId});
                    let htmlText = getAddReceiptSuccessPopupHtml(receipt, travellerType, true, false);

                    import swal from 'sweetalert2';
                    swal({
                        title: Vatfree.translate("Receipt added successfully!"),
                        html: htmlText,
                        type: "success",
                        confirmButtonColor: '#93c01f'
                    });
                }, 200);
            }
        });
    },
});

export const templateHelpers = {
    loginServiceActive(service) {
        return !!Meteor.settings.public.login[service];
    },
    isLoggingIn() {
        return Template.instance().isLoggingIn.get();
    },
    isLoggingInWithSocial(social) {
        if (social) {
            return Template.instance().isLoggingInWithSocial.get() === social;
        } else {
            return !!Template.instance().isLoggingInWithSocial.get();
        }
    },
};

export const templateEvents = function(callback) {
    return {
        'click .google-login'(e, template) {
            e.preventDefault();
            template.isLoggingInWithSocial.set('google');
            Meteor.loginWithGoogle((err) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    if (dataLayer) {
                        dataLayer.push({
                            'event':'loggedin',
                            'UID': Meteor.userId(),
                            'userType':'traveller',
                            'loginService': 'google',
                        });
                    }
                }
                if (callback) callback(err);

                template.isLoggingInWithSocial.set();
            });
        },
        'click .linkedin-login'(e, template) {
            e.preventDefault();
            template.isLoggingInWithSocial.set('linkedin');
            Meteor.loginWithLinkedin((err) => {
                if (err) {
                    console.log(err);
                    toastr.error(Vatfree.translate(err.reason || err.message));
                } else {
                    if (dataLayer) {
                        dataLayer.push({
                            'event':'loggedin',
                            'UID': Meteor.userId(),
                            'userType':'traveller',
                            'loginService': 'linkedin',
                        });
                    }
                }
                if (callback) callback(err);

                template.isLoggingInWithSocial.set();
            });
        },
        'click .facebook-login'(e, template) {
            e.preventDefault();
            template.isLoggingInWithSocial.set('facebook');
            Meteor.loginWithFacebook({
                requestPermissions: ['public_profile', 'email']
            }, (err) => {
                if (err) {
                    console.log(err);
                    toastr.error(Vatfree.translate(err.reason || err.message));
                } else {
                    if (dataLayer) {
                        dataLayer.push({
                            'event':'loggedin',
                            'UID': Meteor.userId(),
                            'userType':'traveller',
                            'loginService': 'facebook',
                        });
                    }
                }
                if (callback) callback(err);

                template.isLoggingInWithSocial.set();
            });
        },
        'click .wechat-login'(e, template) {
            e.preventDefault();
            template.isLoggingInWithSocial.set('wechat');
            Meteor.loginWithWechat({}, (err) => {
                if (err) {
                    console.log(err);
                    toastr.error(Vatfree.translate(err.reason || err.message));
                } else {
                    if (dataLayer) {
                        dataLayer.push({
                            'event':'loggedin',
                            'UID': Meteor.userId(),
                            'userType':'traveller',
                            'loginService': 'wechat',
                        });
                    }
                }
                if (callback) callback(err);

                template.isLoggingInWithSocial.set();
            });
        },
        'submit form[name="register-form"]'(e, template) {
            e.preventDefault();
            template.isLoggingIn.set(true);
            let email = $('input[name="email"]').val();
            let password = $('input[name="password"]').val();
            let password2 = $('input[name="password2"]').val();

            if (!IsValidEmail(email)) {
                toastr.error(Vatfree.translate('Invalid email address_ Please check your input and try again_'));
                return false;
            }
            if (password !== password2) {
                toastr.error(Vatfree.translate('Passwords do not match'));
                return false;
            }

            Accounts.createUser({email, password}, (err, userId) => {
                if (err) {
                    toastr.error(Vatfree.translate(err.reason || err.message));
                } else {
                    alert(Vatfree.translate('An email has been sent to you_ Please click on the link in the email to finish your registration_'));
                    template.isLoggingIn.set();
                }
                if (callback) callback(err);
            });
        }
    };
};

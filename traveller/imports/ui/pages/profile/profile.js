import moment from 'moment-timezone';
import swal from "sweetalert2";
import './profile.html';

Template.profile.onCreated(function() {
    this.user = {};
    this.filesToUpload = new ReactiveArray();
    this.isUploadingFile = new ReactiveVar();
    this.countryId = new ReactiveVar();
    this.travellerTypeId = new ReactiveVar();
    this.natoTravellerTypeId = new ReactiveVar();
    this.changesMade = new ReactiveVar();
});

let initDatepicker = function () {
    Tracker.afterFlush(() => {
        this.$('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: TAPi18n.__('_date_format').toLowerCase(),
            language: Vatfree.getLanguage()
        });
    });
};

Template.profile.onRendered(function() {
    Vatfree.trackEvent('view', 'profile');
    let template = this;

    this.autorun(() => {
        this.user = Meteor.user();

        if (this.user && this.user.profile) {
            this.countryId.set(this.user.profile.countryId);
        }
        if (this.user && this.user.private) {
            let travellerType = TravellerTypes.findOne({_id: this.user.private.travellerTypeId}) || {};
            if (travellerType.isNato) {
                this.travellerTypeId.set('nato');
                this.natoTravellerTypeId.set(travellerType._id);
            } else {
                this.travellerTypeId.set(travellerType._id);
            }
        }
        initDatepicker();
    });

    this.unloadFunction = function() {
        console.log('running unload function', template.changesMade.get());
        if (template && template.changesMade && template.changesMade.get()) {
            return "You have unsaved changes. Are you sure you want to leave this site?";
        }
    };

    this.autorun(() => {
        if (this.countryId.get()) {
            initDatepicker();
        }
    });

    $(window).bind('beforeunload', template.unloadFunction);
});

Template.profile.onDestroyed(function() {
    let template = this;
    //$(window).unbind('beforeunload', template.unloadFunction);
});

const getDocumentsNeeded = function () {
    let travellerTypeId = Template.instance().travellerTypeId.get();
    if (travellerTypeId === 'nato') {
        travellerTypeId = Template.instance().natoTravellerTypeId.get();
    }
    return _.map(travellerHelpers.getIdNeeded(travellerTypeId), (doc) => { return doc.subType; });
};

var helpers = {
    isUploadingFile() {
        return Template.instance().isUploadingFile.get();
    },
    isCountrySelected() {
        let user = Template.instance().user;
        if (user && user.profile) {
            return user.profile.countryId === this._id;
        }
    },
    getCountryId() {
        return Template.instance().countryId.get();
    },
    getTravellerTypeOptions(nato) {
        let selector = {
            isNato: nato ? true : {$ne: true}
        };
        let types = TravellerTypes.find(selector, {
            sort: {
                name: nato ? 1 : -1
            }
        }).fetch();

        if (!nato) {
            // add NATO option for selection
            types.push({
                _id: "nato",
                name: Vatfree.translate("NATO / OTAN")
            });
        }

        return types;
    },
    isTravellerTypeSelected(type) {
        let travellerTypeId = Template.instance().travellerTypeId.get();
        return travellerTypeId === type;
    },
    isNatoTravellerTypeSelected(type) {
        let travellerTypeId = Template.instance().natoTravellerTypeId.get();
        return travellerTypeId === type;

    },
    getTravellerTypeId() {
        let travellerTypeId = Template.instance().travellerTypeId.get();
        if (travellerTypeId === 'nato') {
            return Template.instance().natoTravellerTypeId.get();
        } else {
            return travellerTypeId;
        }
    },
    getTemplateTravellerType() {
        let travellerTypeId = Template.instance().travellerTypeId.get();
        if (travellerTypeId === 'nato') {
            travellerTypeId = Template.instance().natoTravellerTypeId.get();
        }

        return TravellerTypes.findOne({_id: travellerTypeId});
    },
    getDocumentsNeeded() {
        return getDocumentsNeeded();
    },
    getFiles() {
        let documentsNeeded = getDocumentsNeeded();
        return Files.find({
            itemId: Meteor.userId(),
            target: 'travellers',
            $or: [
                {
                    subType: {
                        $nin: documentsNeeded
                    }
                },
                {
                    subType: {
                        $exists: false
                    }
                }
            ]
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getLargePhotoUrl() {
        if (this.copies && this.copies['vatfree'] && this.copies['vatfree'].size > 0) {
            return this.url({store: 'vatfree'});
        } else if (this.copies && this.copies['vatfree-original'] && this.copies['vatfree-original'].size > 0) {
            return this.url({store: 'vatfree-original'});
        } else {
            return this.url({store: 'vatfree-thumbs'});
        }
    },
    getStatusDescription() {
        if (this.private && this.private.status) {
            let travellerType = TravellerTypes.findOne({_id: Template.instance().travellerTypeId.get()}) || {};
            if (!(travellerType.isNato && this.private.status === 'waitingForDocuments')) {
                return Vatfree.translate('traveller_status_description_' + this.private.status) || "";
            }
        }
    },
    canUploadNewDocuments() {
        return travellerHelpers.canUploadNewDocuments.call(this);
    },
    allowDraggable() {
        let user = Meteor.user();
        return travellerHelpers.allowDraggable.call(user);
    },
    getIdFile() {
        let travellerId = Meteor.userId();
        let idFile = Files.findOne({
            itemId: travellerId,
            target: 'travellers',
            subType: this.id.subType
        });

        _.each(Template.instance().filesToUpload.array(), (file) => {
            if (file.subType === this.id.subType) {
                // uploading new id file
                idFile = file;
            }
        });

        return idFile;
    },
    getFilesToUpload(subType) {
        let files = [];
        _.each(Template.instance().filesToUpload.array(), (file) => {
            if (subType === '*' || subType === false || file.subType === subType || (!subType && !file.subType)) {
                files.push(file);
            }
        });

        return files

    },
    isVerifiedStatus() {
        return this.private && this.private.status && this.private.status === 'userVerified';
    },
    isRejectionStatus() {
        return this.private && this.private.status && _.contains(['documentationIncomplete','notEligibleForRefund'], this.private.status);
    },
    getTravellerRejections() {
        let selector = {};
        if (this.private && this.private.rejectionIds) {
            selector['_id'] = {
                $in: this.private.rejectionIds
            }
        }
        return TravellerRejections.find(selector, {
            sort: {
                name: 1
            }
        });
    },
    getRejectionText() {
        const language = Vatfree.getLanguage() || 'en';
        return this.text && this.text[language] ? this.text[language] : this.name;
    },
    getIdNeeded() {
        let travellerTypeId = Template.instance().travellerTypeId.get();
        if (travellerTypeId === "nato") {
            travellerTypeId = Template.instance().natoTravellerTypeId.get();
        }
        return travellerHelpers.getIdNeeded(travellerTypeId);
    },
    needsVisa() {
        let travellerType = TravellerTypes.findOne({
            _id: Template.instance().travellerTypeId.get()
        });
        if (travellerType) {
            let countryId = Template.instance().countryId.get();
            let country = Countries.findOne({_id: countryId}) || {};
            return country.needVisa && _.has(travellerType.documentsNeeded, 'visa');
        }
    },
    hasPassportError() {
        return travellerHelpers.hasPassportError(Meteor.userId(), Template.instance().travellerTypeId.get());
    },
    getFilesToUploadVar() {
        return Template.instance().filesToUpload;
    },
    getTravellerType() {
        let travellerTypeId = Template.instance().travellerTypeId.get();
        if (travellerTypeId === "nato") {
            travellerTypeId = Template.instance().natoTravellerTypeId.get();
        }
        return TravellerTypes.findOne({_id: travellerTypeId});
    },
    secondaryEmails() {
        let emails = [];
        _.each(this.emails, (email) => {
            if (email.address !== this.profile.email) {
                emails.push(email);
            }
        });

        return emails;
    },
    allowedToDelete() {
        return travellerHelpers.allowedToDelete.call(this);
    },
    showOtherDocumentsHeader() {
        let traveller = Meteor.user();
        if (traveller && traveller.profile && traveller.profile.extraDocumentation) {
            return true;
        } else if (helpers.getFiles.call(this).count() > 0) {
            return true;
        }

        return false;
    },
    hiddenButNotSelected() {
        let traveller = Meteor.user();
        return this.hidden && traveller.private.travellerTypeId !== this._id;
    },
    hasInputError(attribute, type) {
        let user = Meteor.user();
        let value;
        if (attribute.indexOf('.')) {
            let attributeSplit = attribute.split('.');
            value = user[attributeSplit[0]][attributeSplit[1]];
        } else {
            value = user[attribute];
        }

        let typeError = false;
        switch(type) {
            case 'string':
                typeError = typeError || !_.isString(value);
                break;
            case 'date':
                let date = moment(new Date(value));
                typeError = typeError || !date.isValid() || date.isBefore(moment());
                break;
        }

        return (!value || typeError ? 'error' : false);
    },
    getIdNameTranslated() {
        const travellerTypeId = this.user && this.user.private ? this.user.private.travellerTypeId : '';
        let travellerType = TravellerTypes.findOne({_id: travellerTypeId}) || {};
        if (travellerType.isNato) {
            return Vatfree.translate('NATO ID');
        } else {
            return Vatfree.translate('Passport/ID');
        }
    },
    allowLanguageChange() {
        return Meteor.settings.public.allowLanguageChange;
    }
};

Template.profile.helpers(helpers);
Template.profile_id.helpers(helpers);
Template.profile_id.onCreated(function() {
    this.filesToUpload = this.data.filesToUpload;
});

let handleFileUpload = function (file, template, subType, inputName, filesToUpload, callback) {
    subType = subType || null;
    inputName = inputName || 'file-upload';
    if (file.type.match(/image\/(jpg|jpeg|png|gif)/)) {
        let reader = new FileReader();
        reader.onload = function (fileLoadEvent) {
            filesToUpload.push({
                file: {
                    name: file.name,
                    type: file.type,
                    size: file.size
                },
                fileContent: reader.result,
                subType: subType
            });
            if (callback) {
                callback(null, file);
            }
            template.$('input[name="' + inputName + '"]').val('');
        };
        reader.readAsDataURL(file);
    } else {
        toastr.error('Only image files are supported (.jpeg, .jpg, .png, .gif)');
        template.$('input[name="' + inputName + '"]').val('');
    }
};
Template.profile.events({
    'dragstart .draggable': function(e) {
        (e.dataTransfer || e.originalEvent.dataTransfer).setData('targetId', this._id);
    },
    'change input, change select'(e, template) {
        template.changesMade.set(true);
    },
    'click .add-new-email'(e, template) {
        e.preventDefault();
        import swal from 'sweetalert';
        import 'swal-forms';
        swal.withForm({
            title: Vatfree.translate('Add Email address'),
            text: Vatfree.translate('Type in the new email address'),
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: Vatfree.translate('Add email!'),
            cancelButtonText: Vatfree.translate('Cancel'),
            closeOnConfirm: true,
            formFields: [
                { id: 'email', placeholder: Vatfree.translate('Email'), type: 'email' }
            ]
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('traveller-add-email', this.swalForm.email, (err ,result) => {
                    if (err) {
                        toastr.error(err.reason || err.message);
                    } else {
                        swal({
                            title: Vatfree.translate("Email address added"),
                            text: Vatfree.translate("We sent you an email to verify your address_ Please click on the link in the email message to complete adding the address_")
                        });
                    }
                });
            }
        });
    },
    'click .resend-verification-email'(e, template) {
        e.preventDefault();
        let address = this.address;
        import swal from 'sweetalert';
        swal({
            title: Vatfree.translate('Resend email verification'),
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: Vatfree.translate('Resend verification!'),
            cancelButtonText: Vatfree.translate('Cancel'),
            closeOnConfirm: true
        }, function (isConfirm) {
            if (isConfirm) {
                Meteor.call('traveller-resend-verification-email', address, (err, result) => {
                    if (err) {
                        console.error(err);
                        alert(err.reason || err.message);
                    } else {
                        swal(Vatfree.translate('Verification email re-sent'));
                    }
                });
            }
        });
    },
    'click .delete-file'(e, template) {
        let image = this;
        let traveller = Travellers.findOne({_id: (image.userId || image.itemId)}) || {};
        if (traveller.private && traveller.private.status) {
            if (_.contains(['userUnverified', 'waitingForDocuments', 'documentationExpired', 'documentationIncomplete', 'notEligibleForRefund'], traveller.private.status)) {
                import swal from 'sweetalert2';
                swal({
                    title: Vatfree.translate("Delete image?"),
                    html: Vatfree.translate("Do you want to permanently remove this image?"),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: Vatfree.translate("Delete image!"),
                    cancelButtonText: Vatfree.translate('Cancel'),
                    confirmButtonColor: '#93c01f',
                    reverseButtons: true
                }).then(function () {
                    Meteor.call('delete-traveller-file', image._id, (err ,result) => {
                        if (err) {
                            toastr.error(err.reason || err.message);
                        } else {
                            toastr.success(Vatfree.translate("File deleted"));
                        }
                    });
                });
            }
        }
    },
    'change select[name="profile.countryId"]'(e, template) {
        e.preventDefault();
        template.countryId.set($(e.currentTarget).val());
    },
    'change select[name="profile.travellerTypeId"]'(e, template) {
        e.preventDefault();
        template.travellerTypeId.set($(e.currentTarget).val());
    },
    'click .traveller-type-option'(e, template) {
        e.preventDefault();
        template.travellerTypeId.set(this._id);
    },
    'click .traveller-type-nato-option'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.natoTravellerTypeId.set(this._id);
    },
    'change input[name="file-upload"]'(e, template) {
        e.preventDefault();
        let file = e.currentTarget.files[0];
        handleFileUpload(file, template, null, null, template.filesToUpload);
    },
    'click .cancel-documentation'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('cancel-upload', 'profile');
        template.filesToUpload.clear();
    },
    'click .submit-documentation'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('submit-verification-documentation', 'profile');
        template.isUploadingFile.set(true);

        Meteor.call('submit-verification-documentation', template.filesToUpload.array(), (err, result) => {
            if (err) {
                toastr.error(err.reason)
            } else {
                template.filesToUpload.clear();
            }
            template.isUploadingFile.set();
        });
    },
    'click .cancel'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('cancel', 'profile');
        template.changesMade.set();
        FlowRouter.go('/');
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('save-user-profile', 'profile');

        let user = Meteor.user();
        let formData = Vatfree.templateHelpers.getFormData(template);

        // formData contains "" for empty elements
        if (_.isUndefined(user.profile.name)) user.profile.name = "";
        if (_.isUndefined(user.profile.passportNumber)) user.profile.passportNumber = "";
        if (_.isUndefined(user.profile.countryId)) user.profile.countryId = "";

        if (formData.profile.passportExpirationDate) {
            formData.profile.passportExpirationDate = moment.tz(formData.profile.passportExpirationDate, TAPi18n.__('_date_format'), 'Europe/Amsterdam').toDate();
        }
        if (formData.profile.visaExpirationDate) {
            formData.profile.visaExpirationDate = moment.tz(formData.profile.visaExpirationDate, TAPi18n.__('_date_format'), 'Europe/Amsterdam').toDate();
        }
        if (formData.profile.birthDate) {
            formData.profile.birthDate = moment.tz(formData.profile.birthDate, TAPi18n.__('_date_format'), 'Europe/Amsterdam').toDate();
        }

        if (user.private.status === 'userVerified') {
            let changedNCP = Vatfree.travellers.ncpChanged(user, formData.profile);
            if (changedNCP && !confirm(Vatfree.translate('ncp_changed_warning'))) {
                return false;
            }
        }

        Meteor.call('save-my-user-profile', formData.profile, Vatfree.getLanguage(), (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                template.changesMade.set();
                toastr.success(Vatfree.translate('Profile saved'));
            }
        });
    }
});

let uploadFiles = function (file, template) {
    let filesToUpload = [];
    handleFileUpload(file, template, template.data.id.subType, "id-file-" + template.data.id.subType, filesToUpload, (err, result) => {
        Meteor.call('submit-verification-documentation', filesToUpload, (err, result) => {
            if (err) {
                toastr.error(err.reason)
            } else {
                filesToUpload = [];
            }
        });
    });
};
let checkPDFandUpload = function (file, template) {
    if (file.type === "application/pdf") {
        let context = {};
        file.subType = template.data.id.subType;
        Vatfree.handlePDFUpload(file, context, function (file, context) {
            let filesToUpload = [{
                file: {
                    name: file.name,
                    type: file.type
                },
                fileContent: file.base64Url,
                subType: file.subType
            }];
            Meteor.call('submit-verification-documentation', filesToUpload, (err, result) => {
                if (err) {
                    toastr.error(err.reason)
                } else {
                    filesToUpload = [];
                }
            });
        });

        let reader = new FileReader();
        reader.onload = function (fileLoadEvent) {
            let filesToUpload = [{
                file: {
                    name: file.name,
                    type: file.type,
                    size: file.size
                },
                fileContent: reader.result,
                internal: true
            }];
            Meteor.call('submit-verification-documentation', filesToUpload, (err, result) => {
                if (err) {
                    toastr.error(err.reason)
                } else {
                    filesToUpload = [];
                }
            });
        };
        reader.readAsDataURL(file);
    } else {
        uploadFiles(file, template);
    }
};
Template.profile_id.events({
    'dragstart .draggable': function(e, template) {
        (e.dataTransfer || e.originalEvent.dataTransfer).setData('targetId', this._id);
    },
    'dropped .id-scan.file-dropzone': function (e, template) {
        e.preventDefault();
        e.stopPropagation();

        let dropId = (e.dataTransfer || e.originalEvent.dataTransfer).getData('targetId');
        if (dropId) {
            Meteor.call('set-verification-documentation-subtype', dropId, template.data.id.subType, TAPi18n.getLanguage(), (err, result) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                }
            });
        } else {
            let files = [];
            FS.Utility.eachFile(e, function (file) {
                files.push(file);
            });

            // first save the profile
            let parentTemplate = template.parentTemplate();
            let formData = Vatfree.templateHelpers.getFormData(parentTemplate);
            Meteor.call('save-my-user-profile', formData.profile, TAPi18n.getLanguage(), (err, result) => {
                if (err) {
                    toastr.error(err.reason || err.message);
                } else {
                    parentTemplate.changesMade.set();
                    _.each(files, function (file) {
                        checkPDFandUpload(file, template);
                    });
                }
            });
        }
    },
    'click .upload-instructions'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        template.$('#id-file-' + template.data.id.subType).trigger('click');
    },
    'click input[type="file"]'(e) {
        e.stopPropagation();
    },
    'change input[name^="id-file-"]'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('uploaded-file', 'profile', {type: template.data.id.subType});

        // first save the profile
        let parentTemplate = template.parentTemplate();
        let formData = Vatfree.templateHelpers.getFormData(parentTemplate);
        Meteor.call('save-my-user-profile', formData.profile, TAPi18n.getLanguage(), (err, result) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                parentTemplate.changesMade.set();
                let file = e.currentTarget.files[0];
                checkPDFandUpload(file, template);
            }
        });
    }
});

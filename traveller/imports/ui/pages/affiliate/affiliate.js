import './affiliate.html';

Accounts.onLogin(() => {
    const affiliateCode = Session.get('affiliateCode');
    if (affiliateCode) {
        Meteor.call('set-affiliate-code', affiliateCode, (err) => {
            if (err) console.error(err);
            Session.setPersistent('affiliateCode');
        });
    }
});

Template.affiliate.onCreated(function() {
    this.isLoggingIn = new ReactiveVar();
    this.isLoggingInWithSocial = new ReactiveVar();
});

Template.affiliate.onRendered(function() {
    this.autorun(() => {
        if (Meteor.user()) {
            FlowRouter.go('/');
        }

        const affiliateCode = FlowRouter.getParam('affiliateCode');
        if (affiliateCode) {
            Session.setPersistent('affiliateCode', affiliateCode);
        }
    });
});

Template.affiliate.helpers({
    loginServiceActive(service) {
        return !!Meteor.settings.public.login[service];
    },
    isLoggingIn() {
        return Template.instance().isLoggingIn.get();
    },
    isLoggingInWithSocial(social) {
        if (social) {
            return Template.instance().isLoggingInWithSocial.get() === social;
        } else {
            return !!Template.instance().isLoggingInWithSocial.get();
        }
    },
});

Template.affiliate.events({
    'click .normal-login'(e, template) {
        e.preventDefault();
        Session.setPersistent('affiliateCode');
        FlowRouter.go('/login');
    },
    'click .google-login'(e, template) {
        e.preventDefault();
        template.isLoggingInWithSocial.set('google');
        const affiliateCode = FlowRouter.getParam('affiliateCode');
        Meteor.loginWithGoogle((err) => {
            if (err) {
                toastr.error(err.reason || err.message);
            } else {
                if (dataLayer) {
                    dataLayer.push({
                        'event':'loggedin',
                        'UID': Meteor.userId(),
                        'userType':'traveller',
                        'loginService': 'google',
                    });
                }
            }
            template.isLoggingInWithSocial.set();
        });
    },
    'click .linkedin-login'(e, template) {
        e.preventDefault();
        template.isLoggingInWithSocial.set('linkedin');
        const affiliateCode = FlowRouter.getParam('affiliateCode');
        Meteor.loginWithLinkedin((err) => {
            if (err) {
                console.log(err);
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                if (dataLayer) {
                    dataLayer.push({
                        'event':'loggedin',
                        'UID': Meteor.userId(),
                        'userType':'traveller',
                        'loginService': 'linkedin',
                    });
                }
            }
            template.isLoggingInWithSocial.set();
        });
    },
    'click .facebook-login'(e, template) {
        e.preventDefault();
        template.isLoggingInWithSocial.set('facebook');
        const affiliateCode = FlowRouter.getParam('affiliateCode');
        Meteor.loginWithFacebook({
            requestPermissions: ['public_profile', 'email']
        }, (err) => {
            if (err) {
                console.log(err);
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                if (dataLayer) {
                    dataLayer.push({
                        'event':'loggedin',
                        'UID': Meteor.userId(),
                        'userType':'traveller',
                        'loginService': 'facebook',
                    });
                }
            }
            template.isLoggingInWithSocial.set();
        });
    },
    'click .wechat-login'(e, template) {
        e.preventDefault();
        template.isLoggingInWithSocial.set('wechat');
        const affiliateCode = FlowRouter.getParam('affiliateCode');
        Meteor.loginWithWechat({}, (err) => {
            if (err) {
                console.log(err);
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                if (dataLayer) {
                    dataLayer.push({
                        'event':'loggedin',
                        'UID': Meteor.userId(),
                        'userType':'traveller',
                        'loginService': 'wechat',
                    });
                }
            }
            template.isLoggingInWithSocial.set();
        });
    },
    'submit form[name="register-form"]'(e, template) {
        e.preventDefault();
        template.isLoggingIn.set(true);
        const affiliateCode = FlowRouter.getParam('affiliateCode');
        let email = $('input[name="email"]').val();
        let password = $('input[name="password"]').val();
        let password2 = $('input[name="password2"]').val();

        if (!IsValidEmail(email)) {
            toastr.error(Vatfree.translate('Invalid email address_ Please check your input and try again_'));
            return false;
        }
        if (password !== password2) {
            toastr.error(Vatfree.translate('Passwords do not match'));
            return false;
        }

        Accounts.createUser({email, password}, (err, userId) => {
            if (err) {
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                alert(Vatfree.translate('An email has been sent to you_ Please click on the link in the email to finish your registration_'));
                template.isLoggingIn.set();
            }
        });
    }
});

#!/usr/bin/env bash
rm mobile-config.js
ln -s ../app-mobile-config-prod.js mobile-config.js
rm -rf .meteor/local/cordova-build/
rm -rf ../Builds/vatfree
meteor build ../Builds/vatfree --server https://vatfree-app-prod.herokuapp.com --mobile-settings mobile-settings-prod.json

#jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 ../Builds/vatfree/android/project/build/outputs/apk/armv7/release/android-armv7-release-unsigned.apk vatfree-release --keystore ../vatfree-release.keystore
#zipalign 4 ../Builds/vatfree/android/project/build/outputs/apk/armv7/release/android-armv7-release-unsigned.apk ../Builds/vatfree/production-armv7.apk

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 ../Builds/vatfree/android/project/app/build/outputs/apk/release/app-release-unsigned.apk vatfree-release --keystore ../vatfree-release.keystore
zipalign 4 ../Builds/vatfree/android/project/app/build/outputs/apk/release/app-release-unsigned.apk ../Builds/vatfree/vatfree-production.apk

echo "Open the ios xcode project to continue with that"

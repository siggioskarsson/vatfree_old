#!/usr/bin/env bash
rm -rf .meteor/local/cordova-build/

meteor npm install --no-optional
export MONGO_URL=mongodb://localhost:6002/meteor
export ROOT_URL=http://192.168.1.98:3002
meteor run ios-device --mobile-server http://192.168.1.98:3002 --settings settings.json --port 3002

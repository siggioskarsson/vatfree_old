Meteor.methods({
    'getInitialStoreSynch': function() {
        // TODO: cache this for later usage
        // TODO: add filters
        return Shops.find({
        },{
            limit: 100
        }).fetch();
    },
    'getShopUpdates': function(lastSyncDate) {
        return Shops.find({
            $or: [
                {
                    createdAt: {
                        $gte: lastSyncDate
                    }
                },
                {
                    updatedAt: {
                        $gte: lastSyncDate
                    }
                }
            ]
        }).fetch();
    }
});

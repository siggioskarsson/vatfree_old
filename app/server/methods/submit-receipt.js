/* global Vatfree: true */

let checkMinimumAmount = function (receiptData) {
    const minimumReceiptAmount = Vatfree.receipts.checkMinimumAmount(receiptData);
    if (minimumReceiptAmount !== true) {
        throw new Meteor.Error(407, 'Receipt amount is below minimum required by country (' + Vatfree.numbers.formatCurrency(minimumReceiptAmount) + ')');
    }
};

let submitReceipt = function (userId, shopId, amount, purchaseDate, customsDate, receiptIsStamped, flightNumber, files, poprDoc, sourceIdentifier) {
    check(userId, String);
    check(shopId, String);
    check(amount, Number);
    check(purchaseDate, Date);
    check(customsDate, Date);
    check(files, Array);
    if (sourceIdentifier) {
        check(sourceIdentifier, String);
    }

    let traveller = Meteor.users.findOne({
        _id: userId
    });
    if (!traveller) {
        throw new Meteor.Error(404, 'User could not be found');
    }

    let shop = Shops.findOne({_id: shopId});
    let country = Countries.findOne({_id: shop.countryId});
    let vatRate = 21;
    if (country) {
        vatRate = Number(Vatfree.vat.getDefaultRate(country.vatRates));
    }
    let totalVat = Math.round(Vatfree.vat.calculateVat(amount * 100, vatRate));

    let status = 'userPending';
    if (!receiptIsStamped) {
        status = 'waitingForCustomsStamp';
    }

    let receiptData = {
        userId: userId,
        shopId: shopId,
        amount: Math.round(amount * 100),
        totalVat: totalVat,
        serviceFee: 0,
        refund: 0,
        vat: {},
        nonStandardVat: false,
        purchaseDate: purchaseDate,
        customsDate: customsDate,
        flightNumber: flightNumber,
        customsCountryId: Meteor.settings.defaults.countryId || null,
        currencyId: shop.currencyId || Meteor.settings.defaults.currencyId,
        status: status,
        source: 'mobile',
        sourceIdentifier: sourceIdentifier,
        channel: 'inperson'
    };
    checkMinimumAmount(receiptData);

    if (poprDoc) {
        Vatfree.receipts.processPoprReceipt(poprDoc.poprId, poprDoc.poprHash, receiptData);
    }

    // Here we check the amount again, as the amount might have been changed by POPr
    checkMinimumAmount(receiptData);

    const travellerTypeId = traveller.private.travellerTypeId;
    Vatfree.receipts.processServiceFees(travellerTypeId, receiptData, shopId);
    Vatfree.receipts.processVatLines(receiptData, country);

    let receiptId;
    try {
        receiptId = Receipts.insert(receiptData);
    } catch (e) {
        console.log(e);
        throw new Meteor.Error(500, 'Could not register receipt');
    }

    _.each(files, (file) => {
        let newFile = new FS.File();
        newFile.name(file.file.name);
        newFile.receiptId = receiptId;
        newFile.itemId = receiptId;
        newFile.target = 'receipts';
        newFile.createdAt = new Date();
        newFile.uploadedBy = userId;
        newFile.attachData(file.fileContent);
        Files.insert(newFile, function (err) {
            if (err) {
                console.log(err);
                throw new Meteor.Error(500, 'Could not process photo of receipt');
            }
        });
    });

    return Receipts.findOne({
        _id: receiptId
    });
};

Meteor.methods({
    'submit-traveller-receipt-v2'(shopId, amount, purchaseDate, customsDate, receiptIsStamped, flightNumber, files, poprDoc, sourceIdentifier) {
        if (!this.userId) {
            throw new Meteor.Error(404, 'You need to be logged in to submit receipts');
        }

        return submitReceipt(this.userId, shopId, amount, purchaseDate, customsDate, receiptIsStamped, flightNumber, files, poprDoc, sourceIdentifier);
    },
    'submit-traveller-receipt'(shopId, amount, purchaseDate, customsDate, files, poprDoc, sourceIdentifier) {
        if (!this.userId) {
            throw new Meteor.Error(404, 'You need to be logged in to submit receipts');
        }

        sourceIdentifier = "OLD MOBILE 2: " + sourceIdentifier;
        return submitReceipt(this.userId, shopId, amount, purchaseDate, customsDate, true, null, files, poprDoc, sourceIdentifier);
    },
    'submit-receipt'(shopId, amount, purchaseDate, customsDate, files, poprDoc) {
        if (!this.userId) {
            throw new Meteor.Error(404, 'You need to be logged in to submit receipts');
        }

        const sourceIdentifier = "OLD MOBILE: " + (this.connection.httpHeaders ?  this.connection.httpHeaders['user-agent'] : '');
        return submitReceipt(this.userId, shopId, amount, purchaseDate, customsDate, true, null, files, poprDoc, sourceIdentifier);
    }
});

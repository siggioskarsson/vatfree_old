Meteor.publishComposite('my-receipts', function() {
    if (!this.userId) return [];

    return {
        find: function () {
            return Receipts.find({
                userId: this.userId,
                status: {
                    $ne: 'deleted'
                }
            });
        },
        children: [
            {
                find: function (receipt) {
                    return Shops.find({
                        _id: receipt.shopId
                    });
                }
            }
        ]
    }
});

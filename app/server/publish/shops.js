import { Config } from 'meteor/cultofcoders:redis-oplog';

Config.oldPublish('shops-public', function(searchTerm, geo, limit) {
    if (!limit) {
        limit = 200;
    }
    let selector = {
        deleted: {
            $ne: true
        }
    };
    if (Meteor.settings.shopSearch && _.isObject(Meteor.settings.shopSearch.selector)) {
        selector = _.clone(Meteor.settings.shopSearch.selector);
    }

    if (searchTerm) {
        searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase();
        let searchTerms = searchTerm.split(' ');
        selector['$and'] = [];
        _.each(searchTerms, (s) => {
            selector['$and'].push({textSearch: new RegExp(s)});
        });
    } else {
        selector.partnershipStatus = {
            $in: ['partner', 'pledger', 'affiliate']
        };
    }

    if (geo && geo[0] && geo[1]) {
        selector['geo'] = {
            $near:  {
                $geometry : {
                    type : "Point",
                    coordinates : geo
                },
                $maxDistance: 20000000
            }
        };
    }

    let options = {
        limit: limit,
        disableOplog: true,
        pollingIntervalMs: 10000*60 // poll every 10 minutes
    };

    if (!selector['geo']) {
        options['sort'] = {
            name: 1
        }
    }

    console.log(searchTerm, JSON.stringify(selector), options);
    return Shops.find(selector, options);
});

Config.oldPublish('shop-search-public', function(searchTerm, geo, limit) {
    if (!limit) {
        limit = 200;
    }
    let selector = {};

    if (searchTerm) {
        searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase();
        let searchTerms = searchTerm.split(' ');
        selector['$and'] = [];
        _.each(searchTerms, (s) => {
            selector['$and'].push({t: new RegExp(s)});
        });
    } else {
        selector.p = {
            $in: ['partner', 'pledger', 'affiliate']
        };
    }

    if (geo && geo[0] && geo[1]) {
        selector['g'] = {
            $near:  {
                $geometry : {
                    type : "Point",
                    coordinates : geo
                },
                $maxDistance: searchTerm ? 20000000 : 20000
            }
        };
    }

    let options = {
        limit: limit,
        fields: {
            t: false
        },
        disableOplog: true,
        pollingIntervalMs: 10000*60 // poll every 10 minutes
    };

    if (!selector['g']) {
        options['sort'] = {
            name: 1
        }
    }

    // console.log(searchTerm, JSON.stringify(selector), options);
    return ShopSearch.find(selector, options);
});

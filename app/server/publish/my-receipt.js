Meteor.publishComposite('my-receipt', function(receiptId) {
    check(receiptId, String);
    check(this.userId, String);

    return {
        find: function () {
            return Receipts.find({
                _id: receiptId,
                userId: this.userId
            });
        },
        children: [
            {
                find: function (receipt) {
                    return Files.find({
                        itemId: receipt._id,
                        target: 'receipts'
                    });
                }
            }
        ]
    }
});

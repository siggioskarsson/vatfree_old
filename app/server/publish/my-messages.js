Meteor.publish('my-messages', function() {
    if (!this.userId) return [];

    this.unblock();
    return ActivityLogs.find({
        travellerId: this.userId,
        type: 'email',
        hideInMessageBox: {
            $ne: true
        }
    });
});

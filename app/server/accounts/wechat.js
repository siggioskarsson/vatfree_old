if (Meteor.settings.wechat) {
    ServiceConfiguration.configurations.upsert({
        service: "wechat"
    }, {
        $set: {
            appId: Meteor.settings.wechat.appId,
            secret: Meteor.settings.wechat.secret,
            mobileAppId: Meteor.settings.wechat.mobileAppId,
            mobileSecret: Meteor.settings.wechat.mobileSecret,
            mpAppId: Meteor.settings.wechat.mpAppId,
            mpSecret: Meteor.settings.wechat.mpSecret,
            loginStyle: "redirect",
            mainId: "unionId"
        }
    });
}

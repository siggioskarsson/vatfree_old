Package.describe({
    name: 'vatfree:app',
    summary: 'Vatfree app package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'vatfree:core',
        'vatfree:countries',
        'vatfree:currencies',
        'vatfree:receipts',
        'vatfree:receipt-types',
        'vatfree:shops',
        'vatfree:invoices',
        'vatfree:affiliates',
        'vatfree:companies',
        'vatfree:contacts',
        'vatfree:activity-logs',
        'vatfree:travellers',
        'vatfree:notify',
        'vatfree:email-templates',
        'vatfree:email-texts',
        'vatfree:web-texts',
        'vatfree:payouts',
        'vatfree:traveller-types',
        'vatfree:traveller-rejections'
    ], 'server');

    api.use([
        'vatfree:core'
    ], 'client');

    api.export([
        'FS',
        'Vatfree',
        'Files',
        'BillingSchema',
        'CreatedUpdatedSchema',
        'StatusDates',
        'Counters',
        'Lock',
        'Locks',
        'GoogleMaps',
        'Countries',
        'Currencies',
        'Shops',
        'ShopSearch',
        'Companies',
        'Contacts',
        'Payouts',
        'Receipts',
        'ReceiptTypes',
        'Travellers',
        'TravellerTypes',
        'TravellerRejections',
        'EmailTexts',
        'EmailTemplates',
        'ActivityLogs',
        'Affiliates'
    ]);
});

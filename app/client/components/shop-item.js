Template.shopItem.helpers({
    getShopData() {
        let data = Template.currentData();
        if (data && !data.n && data.name) {
            data.n = data.name;
            data.g = data.geo;
            data.a = (data.addressFirst || '') + '<br/>' + (data.postCode || '') + ' ' + (data.city || '');
        }

        return data;
    },
    geoDistance(fromGeo) {
        fromGeo = fromGeo || this.geo;
        let geo = Geolocation.currentLocation();
        if (geo && geo.coords && fromGeo && fromGeo.coordinates) {
            return Math.round(geoDistance(geo.coords['latitude'], geo.coords['longitude'], fromGeo.coordinates[1], fromGeo.coordinates[0])).toString() + 'km';
        }
    },
    getPartnershipStatusLogo(partnershipStatus) {
        partnershipStatus = partnershipStatus || this.partnershipStatus;
        switch (partnershipStatus) {
            case 'pledger':
            case 'affiliate':
                return 'vfcircleblue.png';
            case 'partner':
                return 'vfcircleshade.png';
            case 'new':
                return 'vfcircleunkown.png';
            default:
                return 'vfcirclenonrefund.png';
        }
    }
});

let geoDistance = function(lat1, lon1, lat2, lon2) {
    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p))/2;

    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
};

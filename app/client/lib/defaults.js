Vatfree.defaults = new ReactiveVar();
if (Meteor.isClient) {
    Meteor.startup(() => {
        Tracker.autorun(() => {
            Meteor.call('get-defaults', (err, defaults) => {
                if (defaults) {
                    Vatfree.defaults.set(defaults);
                }
            });
        });
    });
}

if (Meteor.isDevelopment) {
    Reloader.configure({
        check: false, // don't check on startup
        refresh: 'instantly' // refresh as soon as updates are available
    });
} else {
    // production configuration
    Reloader.configure({
        check: false,
        refresh: 'start'
    });
}

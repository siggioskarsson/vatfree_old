var $$ = Dom7;
f7App = null;

ActivityLogs = new Meteor.Collection('activity-logs');

Meteor.startup(() => {
    TAPi18n.subscribe('countries-i18n');
    TAPi18n.subscribe('currencies-i18n');

    Session.set('upgradeApp', false);
    Meteor.call('check-app-api-version', (err, apiVersion) => {
        if (apiVersion > Vatfree.appApiVersion) {
            Session.set('upgradeApp', true);
        }
    });

    f7App = new Framework7({
        modalTitle: 'vatfree.com'
    });

    if (Meteor.isCordova && Keyboard) {
        Keyboard.shrinkView(false);
        Keyboard.disableScrollingInShrinkView(false);
        Keyboard.hideFormAccessoryBar(false);
        Keyboard.automaticScrollToTopOnHiding = true;
    }

    Meteor.setTimeout(function () {
        if (Meteor.isCordova) {
            StatusBar.hide();
            StatusBar.show();

            if (cordova.platformId == 'android') {
                StatusBar.backgroundColorByHexString("#e6007e");
            }

            if (Meteor.settings && Meteor.settings.public && Meteor.settings.public.GoogleTagManager) {
                let tagManager = cordova.require('com.jareddickson.cordova.tag-manager.TagManager');
                let trackingId = Meteor.settings.public.GoogleTagManager;
                let intervalPeriod = 30; // seconds
                tagManager.init(null, null, trackingId, intervalPeriod);
            }
        }
    }, 2000);

    if (Meteor.isCordova && Meteor.settings && Meteor.settings.public && Meteor.settings.public.oneSignalAppId) {
        Meteor.startup(function () {
            window.plugins.OneSignal.setRequiresUserPrivacyConsent(true);

            const savePushStatus = function () {
                window.plugins.OneSignal.getPermissionSubscriptionState(function (status) {
                    const deviceId = status.subscriptionStatus.pushToken;
                    if (deviceId && Meteor.userId()) {
                        Meteor.call('savePushStatus', Meteor.userId(), deviceId, status);
                    }
                });
            };
            window.plugins.OneSignal.addPermissionObserver(function() {
                savePushStatus();
            });

            const notificationOpenedCallback = function(notificationData) {
                console.log('notificationOpenedCallback: ' + JSON.stringify(notificationData));
                if (notificationData && notificationData.notification && notificationData.notification.payload) {
                    let data = notificationData.notification.payload.additionalData;
                    if (data && data.messageId) {
                        // reroute to message
                        Session.set('open-message', data.messageId);
                    }
                }
            };

            let initialized = false;
            Tracker.autorun(() => {
                const user = Meteor.user();
                if (user && user._id) {
                    if (!initialized) {
                        window.plugins.OneSignal
                            .startInit(Meteor.settings.public.oneSignalAppId)
                            .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
                            .handleNotificationOpened(notificationOpenedCallback).endInit();

                        window.plugins.OneSignal.provideUserConsent(true);
                        window.plugins.OneSignal.sendTags({
                            user_id: user._id,
                        });
                        savePushStatus();

                        initialized = true;
                    }
                }
            });
        });
    }
});

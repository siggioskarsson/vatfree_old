import KeyCode from 'keycode-js';
import moment from 'moment';

Vatfree.templateHelpers = {};
var processFormItem = function (item, obj) {
    if (!item.name) {
        return false;
    }
    if (item.name.indexOf('.') > 0) {
        var el = item.name.split('.');
        if (el.length === 2) {
            if (!obj[el[0]]) {
                obj[el[0]] = {};
            }
            obj[el[0]][el[1]] = item.value;
        } else if (el.length === 3) {
            if (!obj[el[0]]) {
                obj[el[0]] = {};
            }
            if (!obj[el[0]][el[1]]) {
                obj[el[0]][el[1]] = {};
            }
            obj[el[0]][el[1]][el[2]] = item.value;
        }
    } else {
        obj[item.name] = item.value;
    }
};
Vatfree.templateHelpers.getFormData = function (template, formElement) {
    formElement = formElement || 'form';

    var formData = $(template.find(formElement)).serializeArray().reduce(function (obj, item) {
        if ($(template.find('select[name="' + item.name + '"]')).attr('multiple') === 'multiple') {
            // multiple select item
            if (!obj[item.name]) {
                obj[item.name] = [];
            }
            obj[item.name].push(item.value);
        } else {
            processFormItem(item, obj);
        }
        return obj;
    }, {});

    // check for unchecked checkboxes
    template.$('input:checkbox').each(function () {
        let checkbox = {
            name: this.name,
            value: this.checked ? this.value : false
        };
        if (!formData[checkbox.name]) {
            processFormItem(checkbox, formData);
        }
    });

    return formData;
};

var isScrolledIntoView = function($elem) {
    let docViewTop = $(window).scrollTop();
    let docViewBottom = docViewTop + $(window).height();

    let elemTop = $elem.offset().top;
    let elemBottom = elemTop + $elem.height();

    return (elemBottom <= docViewBottom);
};

var showMoreResults = function (template) {
    if (template.moreResults && template.moreResults.get() && template.subscriptionsReady()) {
        let $scrollElement = template.$('.infinite-scrolling');
        if ($scrollElement && $scrollElement.length > 0) {
            let target = $(".show-more");
            if (isScrolledIntoView($scrollElement)) {
                if (target.length) {
                    target.data("visible", true);
                    target[0].innerHTML = TAPi18n.__('Getting more results_') + Blaze.toHTML(Template.loading);
                }
                template.limit.set(template.limit.get() + template.itemsIncrement);
            } else {
                if (target.length && target.data("visible")) {
                    target.data("visible", false);
                }
            }
        }
    }
};

Vatfree.templateHelpers.initDatepicker = function (startDate, endDate) {
    this.$('.input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        startDate: startDate,
        endDate: endDate,
        format: TAPi18n.__('_date_format').toLowerCase(),
        language: Vatfree.getLanguage()
    });
    this.$('.input-group.date').on('change', (e) => {
        let $element = $(e.currentTarget).find('input');
        let date = ($element.val() || "").toString();
        if (date && date.length === 6 && date.indexOf('-') < 0) {
            // transform DDMMYY to official date
            let mDate = moment(date, 'DDMMYY');
            if (mDate.isValid()) {
                $element.val(mDate.format(TAPi18n.__('_date_format')));
                $element.trigger('change');
            }
        }
    });
};

Vatfree.templateHelpers.onCreated = function (collection, listUrl, detailUrl) {
    return function () {
        this.collection = collection;
        this.subscription = collection._collection.name;
        this.subscriptionItem = collection._collection.name + '_item';
        this.listUrl = listUrl;
        this.detailUrl = detailUrl;
        this.itemsIncrement = 25;

        this.searchTerm = new ReactiveVar(localStorage.getItem(this.view.name + '.searchTerm') || '');
        this.limit = new ReactiveVar(this.itemsIncrement);
        this.subLimit = 0;
        this.offset = new ReactiveVar(0);
        this.sort = new ReactiveVar(localStorage.getItem(this.view.name + '.sort') || {name: 1});
        this.activeItem = new ReactiveVar(localStorage.getItem(this.view.name + '.activeItem'));
        this.initialLoadDone = new ReactiveVar(false);
        this.moreResults = new ReactiveVar(true);

        this.statusFilter = new ReactiveArray();
        this.statusAttribute = 'status';
        _.each(localStorage.getItem(this.view.name + '.statusFilter') || [], (status) => {
            this.statusFilter.push(status);
        });

        this.addingItem = new ReactiveVar();
    }
};

Vatfree.templateHelpers.onRendered = function () {
    return function () {
        this.autorun(() => {
            this.subLimit = this.limit.get();

            let selector = {};
            if (this.statusFilter) {
                Vatfree.search.addStatusFilter.call(this, selector);
            }
            if (_.isFunction(this.selectorFunction)) {
                this.selectorFunction(selector);
            }
            this.subscribe(this.subscription, this.searchTerm.get(), selector, this.sort.get(), this.subLimit, this.offset.get());
        });

        this.autorun(() => {
            if (this.activeItem.get()) {
                this.subscribe(this.subscriptionItem, this.activeItem.get());
            }
        });

        this.autorun(() => {
            let searchTerm = this.searchTerm.get();
            // reset search stuff on changing searchTerm
            this.limit.set(this.itemsIncrement);
            this.offset.set(0);
        });

        this.autorun((comp) => {
            if (this.subscriptionsReady()) {
                this.initialLoadDone.set(true);
                comp.stop(); // stop current autorun computation
            }
        });

        this.autorun(() => {
            if (this.subscriptionsReady()) {
                let limit = this.limit.get();
                if (this.subLimit === limit) {
                    Tracker.nonreactive(() => {
                        // check whether we have more items than before
                        var numberOfItems = this.collection.find({}, {
                            reactive: false
                        }).count();
                        if (limit > numberOfItems) {
                            this.moreResults.set(false);
                        }
                    });
                }
            }
        });

        this.autorun(() => {
            Session.set(this.view.name + '.searchTerm', this.searchTerm.get());
            Session.set(this.view.name + '.activeItem', this.activeItem.get());
            Session.set(this.view.name + '.statusFilter', this.statusFilter.array());
            Session.set(this.view.name + '.sort', this.sort.get());

            localStorage.setItem(this.view.name + '.searchTerm', this.searchTerm.get());
            localStorage.setItem(this.view.name + '.activeItem', this.activeItem.get());
            localStorage.setItem(this.view.name + '.statusFilter', this.statusFilter.array());
            localStorage.setItem(this.view.name + '.sort', this.sort.get());
        });

        let infinitScrolling = Vatfree.templateHelpers.onRenderedInfiniteScrolling.call(this);
        infinitScrolling.call(this);


        this.keyHandler = (e) => {
            // general key strokes are ignored when modals are open
            let activeElement = $(document.activeElement);
            if (activeElement.length && activeElement[0].nodeName === 'INPUT') {
                return false;
            }

            if ($('.featherlight-content').length > 0) {
                if (e.keyCode === KeyCode.KEY_ESCAPE) {
                    var current = $.featherlight.current();
                    current.close();
                }
            } else if ($('.modal').length === 0) {
                let selectedLine = $('tr.item-row.active').attr('id');
                if (selectedLine) {
                    if (e.keyCode === KeyCode.KEY_UP) {
                        // up
                        e.stopPropagation();
                        let prev = $('#' + selectedLine).prev();
                        if (prev.length > 0) {
                            this.activeItem.set(prev.attr('id').replace('item-', ''));
                        }
                    } else if (e.keyCode === KeyCode.KEY_DOWN) {
                        // down
                        e.stopPropagation();
                        let next = $('#' + selectedLine).next();
                        if (next.length > 0) {
                            this.activeItem.set(next.attr('id').replace('item-', ''));
                        }
                    }
                } else if (e.keyCode === KeyCode.KEY_UP || e.keyCode === KeyCode.KEY_DOWN) {
                    // select the first element in the list
                    let rows = $('tr.item-row');
                    if (rows.length > 0) {
                        let id = rows[0].id || "";
                        console.log('set active item', id.replace('item-', ''));
                        this.activeItem.set(id.replace('item-', ''));
                    }
                }

                if (e.keyCode === KeyCode.KEY_E || e.keyCode === KeyCode.KEY_RETURN) {
                    // edit key
                    $('tr.item-row.active').find('.edit-item').trigger('click');
                } else if (e.keyCode === KeyCode.KEY_T) {
                    // task key
                    $('tr.item-row.active').find('.task-item').trigger('click');
                } else if (e.keyCode === KeyCode.KEY_ESCAPE) {
                    console.log($('.btn.cancel'));
                    $('.btn.cancel').trigger('click');
                } else {
                    console.log(e.keyCode);
                }
            }
        };
        Tracker.afterFlush(() => {
            $(document).off('keyup', null, this.keyHandler);
            $(document).on('keyup', null, this.keyHandler);
        });
    }
};

Vatfree.templateHelpers.onDestroyed = function () {
    return function () {
        $(document).off('keyup', null, this.keyHandler);

        if (this.moreResults) {
            $(window).unbind('scroll');
        }
    };
};

Vatfree.templateHelpers.onRenderedInfiniteScrolling = function () {
    return function () {
        if (this.view.isDestroyed) {
            return false;
        }
        this.autorun((comp) => {
            if (this.subscriptionsReady()) {
                Tracker.afterFlush(() => {
                    if (this.moreResults && this.moreResults.get) {
                        $(window).scroll(() => {
                            showMoreResults(this);
                        });
                    }
                    comp.stop(); // stop current autorun computation
                });
            }
        });
    }
};

Vatfree.templateHelpers.select2Search = function (searchName, fieldName, multiple = false, allowClear = false) {
    this.$('select[name="' + fieldName + '"]').select2({
        minimumInputLength: 1,
        multiple: multiple,
        allowClear: allowClear,
        placeholder: {
            id: "",
            placeholder: "Select ..."
        },
        ajax: {
            transport: function (params, success, failure) {
                let limit = 20;
                let offset = 0;
                Meteor.call(searchName, params.data.q || "", limit, offset, (err, res) => {
                    if (err) {
                        failure(err);
                    } else {
                        success({results: res});
                    }
                });
            },
            delay: 500
        }
    });
};


Vatfree.templateHelpers.helpers = {
    initialLoadDone() {
        return Template.instance().initialLoadDone.get();
    },
    getSearchTerm() {
        return Template.instance().searchTerm.get();
    },
    isActive(name) {
        let template = Template.instance();
        if (template[name] && template[name].get) {
            return Template.instance()[name].get() ? 'active' : false;
        }
    },
    isActiveStatusFilter() {
        let statusFilter = Template.instance().statusFilter.array() || [];
        return _.contains(statusFilter, this.toString()) ? 'active' : '';
    },
    getActiveItem() {
        let template = Template.instance();
        let itemId = template.activeItem.get();
        if (itemId) {
            let item = template.collection.findOne({
                _id: itemId
            });
            return item
        }
    },
    isActiveItem() {
        let activeItem = Template.instance().activeItem;
        if (!activeItem) {
            activeItem = Template.instance().parentTemplate().activeItem;
        }
        if (!activeItem) {
            activeItem = Template.instance().data.activeItem;
        }

        if (activeItem && activeItem.get) {
            return activeItem.get() === this._id ? 'active' : '';
        } else if (_.isString(activeItem)) {
            return activeItem === this._id ? 'active' : '';
        }
    },
    moreResults() {
        return Template.instance().moreResults.get();
    },
    addingItem() {
        let template = Template.instance();
        return template.addingItem ? template.addingItem.get() : false;
    },
    getSorting() {
        return Template.instance().sort.get();
    },
    isHeaderSorting(colId) {
        if (this.sorting && this.sorting[colId]) {
            return this.sorting[colId] === -1 ? 'sorting-desc' : 'sorting-asc';
        }
    }
};

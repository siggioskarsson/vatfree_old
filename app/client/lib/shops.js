// LocalShops = new Ground.Collection('local-shops');

ShopHelpers = {
    getShops() {
        let template = Template.instance();
        let searchTerm = template.searchTerm.get();
        let selector = {};

        if (searchTerm && searchTerm.length > 0) {
            searchTerm = searchTerm.replace(new RegExp(' +'), ' ').toLowerCase();
            let searchTerms = searchTerm.split(' ');
            selector['$and'] = [];
            _.each(searchTerms, (s) => {
                selector['$and'].push({
                    $or: [
                        {
                            n: new RegExp(s, "i")
                        },
                        {
                            a: new RegExp(s, "i")
                        }
                    ]
                });
            });
        }

        let geo = Geolocation.currentLocation();
        if (geo && geo.coords) {
            selector['g'] = {
                $near:  {
                    $geometry : {
                        type : "Point",
                        coordinates : [
                            geo.coords.longitude,
                            geo.coords.latitude
                        ]
                    }
                }
            };
        }

        let limit = template.limit.get();

        let options = {
            limit: limit
        };
        if (!selector['g']) {
            options['sort'] = {
                n: 1
            }
        }

        //console.log('client selector', selector, options);
        let iterator = ShopSearch.find(selector, options);

        template.searchCount.set(iterator.count());

        return iterator;
    },
    moreResults() {
        let template = Template.instance();
        return template.searchCount.get() >= template.limit.get();
    }
};

Meteor.startup(() => {
    return false;
    Tracker.autorun(() => {
        let status = Meteor.status();
        console.log(status, navigator.connection);
        if (Meteor.isCordova && navigator.connection) {
            // check for WiFi for shop sync
            if (navigator.connection.type === Connection.WIFI) {
                Tracker.nonreactive(() => {
                    syncShops();
                });
            }
        } else if (Meteor.isClient) {
            // always sync on browser connection
            Tracker.nonreactive(() => {
                syncShops();
            });
        }
    });
});

var syncShops = function() {
    let lastSyncDate = localStorage.getItem('lastSyncDate');
    console.log('synching shops', lastSyncDate);
    if (!lastSyncDate) {
        // initial synch
        Meteor.call('getInitialStoreSynch', (err, shops) => {
            if (err) {
                console.log(err);
            } else {
                console.log('got shops: ', shops.length);
                LocalShops.keep(shops);
                let maxSyncDate = 0;
                _.each(shops, (shop) => {
                    if (shop.createdAt > maxSyncDate) {
                        maxSyncDate = shop.createdAt;
                    }
                    if (shop.updatedAt && shop.updatedAt > maxSyncDate) {
                        maxSyncDate = shop.updatedAt;
                    }
                });
                localStorage.setItem('lastSyncDate', maxSyncDate);
            }
        });
    } else {
        // update synch
        Meteor.call('getShopUpdates', lastSyncDate, (err, shops) => {
            if (err) {
                console.log(err);
            } else {
                console.log('got shops: ', shops.length);
                LocalShops.keep(shops);
                let maxSyncDate = 0;
                _.each(shops, (shop) => {
                    if (shop.createdAt > maxSyncDate) {
                        maxSyncDate = shop.createdAt;
                    }
                    if (shop.updatedAt && shop.updatedAt > maxSyncDate) {
                        maxSyncDate = shop.updatedAt;
                    }
                });
                localStorage.setItem('lastSyncDate', maxSyncDate);
            }
        });
    }
};

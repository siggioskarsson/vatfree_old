Template.view_main_contact.events({
    'click .external link'(e, template) {
        e.preventDefault();
        let aHref = $(e.curentTarget);

        window.open(aHref.href(), '_system');
    }
});
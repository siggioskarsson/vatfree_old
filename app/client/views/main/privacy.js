Template.view_main_privacy.onCreated(function() {
    this.privacyPolicy = new ReactiveVar();
    this.loadPrivacyPolicy = function () {
        Meteor.call('getWebText', 'privacy_policy', Vatfree.getLanguage(), (err, privacyPolicy) => {
            if (err) {
                console.error(err);
            } else {
                this.privacyPolicy.set(privacyPolicy);
            }
        });
    };
});

Template.view_main_privacy.onRendered(function() {
    this.autorun(() => {
        this.loadPrivacyPolicy.call(this);
    });
});

Template.view_main_privacy.helpers({
    privacyPolicy() {
        return Template.instance().privacyPolicy.get();
    }
});

Template.view_main_privacy.events({
    'click .load-privacy-policy'(e, template) {
        e.preventDefault();
        template.loadPrivacyPolicy.call(template);
    }
});

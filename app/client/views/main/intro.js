import { Template } from 'meteor/templating';

Template.view_main_intro.onRendered(function() {
    let template = this;

    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;

    this.swiper = false;

    this.$$('.popup-intro').on('popup:opened', function () {
        console.log('About Popup opened')
    });
    this.$$('.popup-intro').on('popup:close', function () {
        console.log('About Popup is closing')
    });
    this.$$('.popup-intro').on('popup:open', function () {
        console.log('Services Popup is opening');
        if (!this.swiper) {
            this.swiper = template.app.swiper('.swiper-container', {
                speed: 400,
                pagination:'.swiper-pagination'
            });
        }
    });
    this.$$('.popup-intro').on('popup:closed', function () {
        console.log('Services Popup is closed');
        this.swiper.slideTo(0);
    });
});

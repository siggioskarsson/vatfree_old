import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';

Template.view_main.onCreated(function() {
    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;

    this.stack = new ReactiveVar(0);
});

Template.view_main.onRendered(function() {
    let template = this;
    Tracker.afterFlush(() => {
        this.appView = myApp.addView('.view-main', {
            domCache: true, //enable inline pages
            onSwipeBackAfterChange: function(e) {
                template.stack.set(template.stack.get() - 1);
            }
        });

        if (!Session.get('main.instruction-seen') && !localStorage.getItem('main.instruction-seen')) {
            template.$('.open-intro').trigger('click');
        }
    });
});

Template.view_main.helpers({
    stackSize() {
        return Template.instance().stack.get();
    }
});

Template.view_main.events({
    'click a.back'(e, template) {
        e.preventDefault();
        template.appView.router.back();
        template.stack.set(template.stack.get() - 1);
    },
    'click .open-intro'(e, template) {
        e.preventDefault();
        localStorage.setItem('main.instruction-seen', true);
        Session.set('main.instruction-seen', true);
        myApp.popup('.popup-intro');
    },
    'click .open-what'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('open-what', 'main');
        template.appView.router.load({pageName: 'what'});
        template.stack.set(template.stack.get() + 1);
    },
    'click .open-how'(e, template) {
        e.preventDefault();
        if (confirm("Do you want to view our howto at our website?")) {
            Vatfree.trackEvent('open-how', 'main');
            window.open('https://www.vatfree.com/redirect-app', '_system');
        }
        return false;
    },
    'click .open-faq'(e, template) {
        e.preventDefault();
        if (confirm("Do you want to view our FAQ at our website?")) {
            Vatfree.trackEvent('open-faq', 'main');
            window.open('https://www.vatfree.com/faqs', '_system');
        }
        return false;
    },
    'click .open-contact'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('open-contact', 'main');
        template.appView.router.load({pageName: 'contact'});
        template.stack.set(template.stack.get() + 1);
    },
    'click .open-terms'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('open-terms', 'main');
        template.appView.router.load({pageName: 'terms'});
        template.stack.set(template.stack.get() + 1);
    },
    'click .open-privacy'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('open-privacy', 'main');
        template.appView.router.load({pageName: 'privacy'});
        template.stack.set(template.stack.get() + 1);
    },
    'click .view-intro .cancel'(e, template) {
        e.preventDefault();
        openView('main');
        template.stack.set(0);
    },
    'click .open-vatfree-contact'(e) {
        e.preventDefault();
        Vatfree.trackEvent('open-vatfree-contact', 'main');
        window.open('https://www.vatfree.com/contact/', '_system');
    },
    'click .open-vatfree-home'(e) {
        e.preventDefault();
        Vatfree.trackEvent('open-vatfree-website', 'main');
        window.open('https://www.vatfree.com/', '_system');
    }
});

Template.toolbar.events({
    'click .tab-link'(e, template) {
        let id = $(e.currentTarget).attr('id');
        if (id === Session.get('currentView')) {
            Vatfree.trackEvent('open-view-' + id, 'tabbar');
            $('.view-' + id).trigger('resetView');
        }
        Session.set('currentView', id);
    }
});

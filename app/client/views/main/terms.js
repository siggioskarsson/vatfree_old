Template.view_main_terms.onCreated(function() {
    this.termsText = new ReactiveVar();
    this.loadTerms = function () {
        Meteor.call('getWebText', 'terms', Vatfree.getLanguage(), (err, termsText) => {
            if (err) {
                console.error(err);
            } else {
                this.termsText.set(termsText);
            }
        });
    };
});

Template.view_main_terms.onRendered(function() {
    this.autorun(() => {
        this.loadTerms.call(this);
    });
});

Template.view_main_terms.helpers({
    termsText() {
        return Template.instance().termsText.get();
    }
});

Template.view_main_terms.events({
    'click .load-terms'(e, template) {
        e.preventDefault();
        template.loadTerms.call(template);
    }
});

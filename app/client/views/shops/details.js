import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';

// TODO: shop-details page is destroyed on back action :-S

Template.view_shops_details.onCreated(function() {
    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;
});

Template.view_shops_details.onRendered(function() {
    this.autorun(() => {
        let shopId = Session.get('currentShop');
        this.subscribe('shops_item', shopId || "");
    });
});

Template.view_shops_details.helpers({
    getShop: function() {
        let shopId = Session.get('currentShop');
        return Shops.findOne({_id: shopId});
    },
    getPartnershipStatusLogo() {
        switch (this.partnershipStatus) {
            case 'pledger':
            case 'affiliate':
                return 'vfcircleblue.png';
            case 'partner':
                return 'vfcircleshade.png';
            case 'new':
                return 'vfcircleunkown.png';
            default:
                return 'vfcirclenonrefund.png';
        }
    },
    isPartnerShipStatus(partnershipStatus) {
        if (partnershipStatus === "affiliate") partnershipStatus = "pledger";
        return partnershipStatus === this.partnershipStatus;
    },
    getAffilitedWith() {
        const affiliatedWith = ReactiveMethod.call('get-shop-affiliated-with', this._id);
        if (affiliatedWith) {
            return affiliatedWith.join(', ');
        }
        return false;
    }
});

Template.view_shops_details.events({
    'click .geo'(e) {
        let geocoords = this.geo.coordinates[1] + ',' + this.geo.coordinates[0];
        if ((typeof device !== "undefined") && device.platform && device.platform === 'iOS') {
            window.open('maps://?q=' + geocoords, '_system');
        } else {
            let label = encodeURI(this.name);
            window.open('geo:0,0?q=' + geocoords + '(' + label + ')', '_system');
        }
    },
    'click .submit-receipt'(e, template) {
        e.preventDefault();
        Session.set('setShopIdForNewReceipt', this._id);
        Session.set('currentPage', 'add');
        template.$$('.view').hide(); template.$$('.view-add').show();
    }
});

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';

Template.view_shops.onCreated(function() {
    this.searchTerm = new ReactiveVar();
    this.searchCount = new ReactiveVar();
    this.limitIncrement = 20;
    this.limit = new ReactiveVar(this.limitIncrement);
    this.loading = new ReactiveVar();
    this.stopInfiniteScrolling = new ReactiveVar();
    this.geoLocation = new ReactiveVar();

    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;

    this.stack = new ReactiveVar(0);
});

Template.view_shops.onRendered(function() {
    let template = this;

    this.$$('.shop-search.infinite-scroll').on('infinite', function() {
        if (!template.stopInfiniteScrolling.get()) {
            if (!template.loading.get()) {
                template.loading.set(true);
                template.limit.set(template.limit.get() + template.limitIncrement);
            }
        }

        Meteor.setTimeout(() => {
            template.loading.set(false);
        }, 1000);
    });

    this.autorun(() => {
        // autorun if search term changes
        let searchTerm = this.searchTerm.get();
        this.limit.set(this.limitIncrement);
        this.stopInfiniteScrolling.set();
    });

    this.autorun(() => {
        let geoLocation = this.geoLocation.get();
        let newLocation = Geolocation.currentLocation();
        if (newLocation && newLocation.coords) {
            let newLatitude = Math.round(newLocation.coords.latitude * 10000) / 10000;
            let newLongitude = Math.round(newLocation.coords.longitude * 10000) / 10000;
            if (!geoLocation || !geoLocation.coords || geoLocation.coords.latitude !== newLatitude || geoLocation.coords.longitude !== newLongitude) {
                this.geoLocation.set({
                    coords: {
                        latitude: newLatitude,
                        longitude: newLongitude
                    }
                });
            }
        }
    });

    this.autorun(() => {
        let searchTerm = this.searchTerm.get();
        let limit = this.limit.get();
        let geo = this.geoLocation.get();
        this.subscribe('shop-search-public', searchTerm, (geo && geo.coords ? [ geo.coords.longitude, geo.coords.latitude ] : false), limit);
    });

    this.autorun(() => {
        if (this.subscriptionsReady()) {
            if (this.limit.get() > ShopSearch.find().count()) {
                this.stopInfiniteScrolling.set(true);
            }
        }
    });

    Tracker.afterFlush(() => {
        this.appView = myApp.addView('.view-shops', {
            domCache: true, //enable inline pages
            onSwipeBackAfterChange: function(e) {
                template.stack.set(template.stack.get() - 1);

                if (template.appView.activePage.name === 'shops') {
                    Meteor.setTimeout(() => {
                        Session.set('currentShop');
                    }, 400);
                }
                if (template.appView.activePage.name === 'shopdetails') {
                    Meteor.setTimeout(() => {
                        Session.set('shops-learn-more');
                    }, 400);
                }
            }
        });
    });
});

Template.view_shops.helpers(ShopHelpers);
Template.view_shops.helpers({
    stackSize() {
        return Template.instance().stack.get();
    },
    getCurrentShop() {
        return Session.get('currentShop');
    },
    learnMore() {
        return Session.get('shops-learn-more');
    }
});

Template.view_shops.events({
    'resetView'(e, template) {
        if (template.appView.activePage.name === 'shopdetails') {
            template.appView.router.back();
        } else {
            template.appView.router.load({pageName: 'shops', animatePages: false});
        }
        Meteor.setTimeout(() => {
            Session.set('currentShop', false);
            Session.set('shops-learn-more', false);
        }, 400);
        template.stack.set(0);
    },
    'change input[name="searchTerm"]'(e, template) {
        e.preventDefault();
        template.searchTerm.set($(e.currentTarget).val());
    },
    'keyup input[name="searchTerm"]': _.debounce((e, template) => {
        e.preventDefault();
        template.searchTerm.set($(e.currentTarget).val());
    }, 300),
    'click a.back'(e, template) {
        e.preventDefault();
        template.appView.router.back();
        template.stack.set(template.stack.get() - 1);

        if (template.appView.activePage.name === 'shops') {
            Meteor.setTimeout(() => {
                Session.set('currentShop');
            }, 400);
        }
        if (template.appView.activePage.name === 'shopdetails') {
            Meteor.setTimeout(() => {
                Session.set('shops-learn-more');
            }, 400);
        }
    },
    'click .item-link'(e, template) {
        e.preventDefault();
        Session.set('currentShop', this._id);
        Tracker.afterFlush(() => {
            Vatfree.trackEvent('open-shop-details', 'shops');
            template.appView.router.load({pageName: 'shopdetails'});
            template.stack.set(template.stack.get() + 1);
        });
    },
    'click .shops-learn-more'(e, template) {
        e.preventDefault();
        Session.set('shops-learn-more', true);
        Tracker.afterFlush(() => {
            Vatfree.trackEvent('open-shops-learn-more', 'shops');
            template.appView.router.load({pageName: 'shops-learn-more'});
            template.stack.set(template.stack.get() + 1);
        });
    }
});

Template.shops_status_learn_more.onCreated(function() {
   this.text = new ReactiveVar('');
});
Template.shops_status_learn_more.onRendered(function() {
    this.autorun(() => {
        Meteor.call('getWebText', 'traveller_shop_status_learn_more', Vatfree.getLanguage(), (err, text) => {
            if (err) {
                console.error(err);
            } else {
                this.text.set(text);
            }
        });
    });
});
Template.shops_status_learn_more.helpers({
    getText() {
        return Template.instance().text.get();
    }
});

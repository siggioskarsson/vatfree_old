import {Template} from "meteor/templating";

UI.registerHelper('userIsVerified', function() {
    if (this.private && this.private.status) {
        return this.private.status === 'userVerified';
    }
});

UI.registerHelper('getStatusDescription', function() {
    if (this.private && this.private.status) {
        let travellerTypeId = Template.instance().travellerTypeId ? Template.instance().travellerTypeId.get() : this.private.travellerTypeId;
        let travellerType = TravellerTypes.findOne({_id: travellerTypeId}) || {};
        if (!(travellerType.isNato && this.private.status === 'waitingForDocuments')) {
            return TAPi18n.__('traveller_status_description_' + this.private.status) || "";
        }
    }
});

UI.registerHelper('isNato', function() {
    let traveller = Meteor.user();
    if (traveller && traveller.private) {
        let travellerType = TravellerTypes.findOne({
            _id: traveller.private.travellerTypeId
        }) || {};
        return travellerType.isNato;
    }
});

UI.registerHelper('getTravellerTypeCertificateName', function() {
    let traveller = Meteor.user();
    if (traveller && traveller.private) {
        let travellerType = TravellerTypes.findOne({
            _id: traveller.private.travellerTypeId
        }) || {};
        return travellerType.certificateName;
    }
});

UI.registerHelper('networkProblem', () => {
    let status = Meteor.status().status;
    return status !== 'connecting' && status !== "connected";
});

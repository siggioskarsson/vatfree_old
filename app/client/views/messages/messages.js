import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';

Template.view_messages.onCreated(function() {
    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;

    this.stack = new ReactiveVar(0);
});

Template.view_messages.onRendered(function() {
    const template = this;

    this.autorun(() => {
        this.user = Meteor.user();
        if (this.user) {
            this.subscribe('my-messages');
        }
    });

    this.autorun(() => {
        if (Meteor.user() && this.subscriptionsReady()) {
            if (Session.get('open-message')) {
                Tracker.afterFlush(() => {
                    $('#messages.tab-link').trigger('click');
                    $('#' + Session.get('open-message') + '.item-link.open-message').trigger('click');
                    Session.set('open-message');
                });
            }
        }
    });

    Tracker.afterFlush(() => {
        this.appView = this.app.addView('.view-messages', {
            domCache: true, //enable inline pages
            onSwipeBackAfterChange: function(e) {
                template.stack.set(template.stack.get() - 1);
                Session.set('currentMessage', false);
            }
        });
    });
});

Template.view_messages.helpers({
    stackSize() {
        return Template.instance().stack.get();
    },
    messages() {
        return ActivityLogs.find({
            travellerId: Meteor.userId(),
            type: 'email'
        }, {
            sort: {
                createdAt: -1
            }
        });
    }
});

Template.view_messages.events({
    'resetView'(e, template) {
        let stack = template.stack.get();
        for(var i = 0; i <= stack; i++) {
            template.appView.router.back();
        }
        Session.set('currentMessage', false);
        template.stack.set(0);
    },
    'click a.back'(e, template) {
        e.preventDefault();
        template.appView.router.back();
        template.stack.set(template.stack.get() - 1);
        Session.set('currentMessage', false);
    },
    'click .item-link.open-message'(e, template) {
        e.preventDefault();
        if (this._id) {
            Vatfree.trackEvent('open-message', 'messages');
            Session.set('currentMessage', this._id);
            template.appView.router.load({pageName: 'messagedetails'});
            template.stack.set(template.stack.get() + 1);
        }
    },
});

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';

Template.view_message_details.onCreated(function() {
    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;

    this.receipt = new ReactiveVar();

    this.autorun(() => {
        let messageId = Session.get('currentMessage');
        if (messageId) {
            Meteor.call('mark-message-read', messageId, console.log);
        }
    });
});

Template.view_message_details.helpers({
    getMessage() {
        return ActivityLogs.findOne({_id: Session.get('currentMessage')});
    },
});

Template.view_message_details.events({
});

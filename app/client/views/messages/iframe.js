Template.view_message_details_iframe.helpers({
    getCleanHtml() {
        let match = this.html ? this.html.match(/<body[^>]*>([\w|\W]*)<\/body>/im) : false;
        if (match && match[1]) {
            let html = match[1];
            html = html.replace(/(<style>[\w|\W]*<\/style>)/im, '');
            return html;
        }

        return "";
    }
});

Template.view_message_details_iframe.events({
    'click .html-email a'(e, template) {
        e.preventDefault();
        e.stopPropagation();

        let href = $(e.currentTarget).attr('href');
        if (href.match(new RegExp("/login/"))) {
            // trying to login
            if (href.match(new RegExp("\\?page=profile"))) {
                $('.toolbar-inner a#account').trigger('click');
            } else if (href.match(new RegExp("\\?page=receipts"))) {
                $('.toolbar-inner a#receipts').trigger('click');
            } else if (href.match(new RegExp("\\?page=shops"))) {
                $('.toolbar-inner a#shops').trigger('click');
            } else if (href.match(new RegExp("\\?page=add"))) {
                $('.toolbar-inner a#add').trigger('click');
            } else {
                alert(Vatfree.translate('You are already logged into the app'));
            }
        } else {
            window.open(href, '_system');
        }
    }
});

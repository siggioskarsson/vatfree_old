import moment from 'moment-timezone';
import {Template} from "meteor/templating";

Template.view_account.onCreated(function() {
    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;

    this.user = {};
    this.filesToUpload = new ReactiveArray();
    this.isUploadingFile = new ReactiveVar();
    this.countryId = new ReactiveVar();
    this.travellerTypeId = new ReactiveVar();

    this.filesToUpload = new ReactiveArray();
    this.changesMade = new ReactiveVar();
    this.renderForm = new ReactiveVar(true); // helper to force the form to re-render

    this.autorun(() => {
        if (!this.renderForm.get()) {
            Meteor.setTimeout(() => {
                this.renderForm.set(true);
            }, 100);
        }
    })
});

Template.view_account.onRendered(function() {
    this.autorun(() => {
        this.user = Meteor.user();
        if (this.user) {
            this.subscribe('my-profile');
            this.subscribe('traveller-types');
        }

        if (this.user && this.user.profile) {
            this.countryId.set(this.user.profile.countryId);
        }
        if (this.user && this.user.private) {
            this.travellerTypeId.set(this.user.private.travellerTypeId);
        }
    });
});

var helpers = {
    changesMade() {
        return Template.instance().changesMade.get();
    },
    isUploadingFile() {
        return Template.instance().isUploadingFile.get();
    },
    renderForm() {
        return Template.instance().renderForm.get();
    },
    getCountries() {
        return Countries.find({}, {
            sort: {
                name: 1
            }
        });
    },
    isCountrySelected() {
        let user = Template.instance().user;
        if (user && user.profile) {
            return user.profile.countryId === this._id;
        }
    },
    getCountryId() {
        return Template.instance().countryId.get();
    },
    getTravellerTypeOptions() {
        return TravellerTypes.find({},{
            sort: {
                name: -1
            }
        });
    },
    getUploadedFiles() {
        let selector = {
            itemId: Meteor.userId(),
            target: 'travellers'
        };

        if (this.subType) {
            selector.subType = this.subType;
        } else {
            selector.subType = {
                $exists: false
            }
        }

        return Files.find(selector, {
            sort: {
                createdAt: 1
            }
        });
    },
    getFilesToUpload() {
        let files = _.filter(Template.instance().filesToUpload.array(), (file) => {
            return file.subType === this.subType;
        }) || [];

        return files;
    },
    getFiles() {
        let documentsNeeded = _.map(travellerHelpers.getIdNeeded(Template.instance().travellerTypeId.get()), (doc) => { return doc.id; });
        return Files.find({
            itemId: Meteor.userId(),
            target: 'travellers',
            $or: [
                {
                    subType: {
                        $nin: documentsNeeded
                    }
                },
                {
                    subType: {
                        $exists: false
                    }
                }
            ]
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getLargePhotoUrl() {
        if (this.copies && this.copies['vatfree'] && this.copies['vatfree'].size > 0) {
            return this.url({store: 'vatfree'});
        } else if (this.copies && this.copies['vatfree-original'] && this.copies['vatfree-original'].size > 0) {
            return this.url({store: 'vatfree-original'});
        } else {
            return this.url({store: 'vatfree-thumbs'});
        }
    },
    isVerifiedStatus() {
        return this.private && this.private.status && this.private.status === 'userVerified';
    },
    isRejectionStatus() {
        return this.private && this.private.status && _.contains(['documentationIncomplete','notEligibleForRefund'], this.private.status);
    },
    getTravellerRejections() {
        let selector = {};
        if (this.private && this.private.rejectionIds) {
            selector['_id'] = {
                $in: this.private.rejectionIds
            }
        }
        return TravellerRejections.find(selector, {
            sort: {
                name: 1
            }
        });
    },
    getRejectionText() {
        const language = Vatfree.getLanguage() || 'en';
        return this.text && this.text[language] ? this.text[language] : this.name;
    },
    getIdNeeded() {
        return travellerHelpers.getIdNeeded(Template.instance().travellerTypeId.get());
    },
    needsVisa() {
        let travellerType = TravellerTypes.findOne({
            _id: Template.instance().travellerTypeId.get()
        });
        if (travellerType) {
            let countryId = Template.instance().countryId.get();
            let country = Countries.findOne({_id: countryId}) || {};
            return country.needVisa && _.has(travellerType.documentsNeeded, 'visa');
        }
    },
    hasPassportError() {
        return travellerHelpers.hasPassportError(Meteor.userId(), Template.instance().travellerTypeId.get());
    },
    getFilesToUploadVar() {
        return Template.instance().filesToUpload;
    },
    getTravellerType() {
        return TravellerTypes.findOne({_id: Template.instance().travellerTypeId.get()});
    },
    canUploadNewDocuments() {
        return travellerHelpers.canUploadNewDocuments.call(Meteor.user());
    },
    secondaryEmails() {
        let emails = [];
        _.each(this.emails, (email) => {
            if (email.address !== this.profile.email) {
                emails.push(email);
            }
        });

        return emails;
    },
    notEnglish() {
        return Vatfree.getLanguage() !== 'en';
    },
    hasInputError(attribute, type) {
        let user = Meteor.user();
        let value;
        if (attribute.indexOf('.')) {
            let attributeSplit = attribute.split('.');
            value = user[attributeSplit[0]] ? user[attributeSplit[0]][attributeSplit[1]] : undefined;
        } else {
            value = user[attribute];
        }

        let typeError = false;
        switch(type) {
            case 'string':
                typeError = typeError || !_.isString(value);
                break;
            case 'date':
                let date = moment(new Date(value));
                typeError = typeError || !date.isValid() || date.isBefore(moment());
                break;
        }

        return (!value || typeError ? 'error' : false);
    }
};

Template.view_account.helpers(helpers);

let handleFileUpload = function (file, template, subType, inputName) {
    subType = subType || null;
    inputName = inputName || 'file-upload';
    if (file.type.match(/image\/(jpg|jpeg|png|gif)/)) {
        let reader = new FileReader();
        reader.onload = function (fileLoadEvent) {
            template.app.showPreloader(Vatfree.translate('Uploading___'));
            Vatfree.imageHelpers.resetOrientation(reader.result, (err, base64Uri) => {
                if (err) {
                    template.app.hidePreloader();
                    alert(err.reason || err.message);
                    Vatfree.trackEvent('save-file-error', 'account', {error: err.reason || err.message});
                } else {
                    let uploadFile = {
                        file: {
                            name: file.name,
                            type: file.type,
                            size: file.size
                        },
                        fileContent: base64Uri,
                        subType: subType
                    };
                    Vatfree.trackEvent('save-profile-send-file', 'account');
                    Meteor.call('submit-verification-documentation', [ uploadFile ], (err,result) => {
                        if (err) {
                            template.app.hidePreloader();
                            alert(Vatfree.translate(err.reason || err.message));
                            Vatfree.trackEvent('save-profile-send-files-error', 'account', {error: err.reason || err.message});
                        } else {
                            template.app.hidePreloader();
                        }
                    });
                }
            });
            template.$('input[name="' + inputName + '"]').val('');
        };
        reader.readAsDataURL(file);
    } else {
        toastr.error(Vatfree.translate('Only image files are supported (jpeg, jpg, png, gif)'));
        template.$('input[name="' + inputName + '"]').val('');
    }
};

Template.view_account.events({
    'change input[type="date"], change select'(e) {
        const $el = $(e.currentTarget);
        if ($el.val()) {
            $el.removeClass('error');
        } else if ($el.attr("required")) {
            $el.addClass('error');
        }
    },
    'click .logout'(e) {
        e.preventDefault();
        let userId = Meteor.userId();
        if (window.plugins && window.plugins.OneSignal) {
            window.plugins.OneSignal.getPermissionSubscriptionState(function (status) {
                if (status && status.subscriptionStatus && status.subscriptionStatus.pushToken) {
                    Meteor.call('unsetPushStatus', userId, status.subscriptionStatus.pushToken, (err, result) => {
                        if (err) {
                            console.log(err);
                        }
                        Meteor.logout();
                    });
                } else {
                    Meteor.logout();
                }
            });
        } else {
            Meteor.logout();
        }
    },
    'change input, change select'(e, template) {
        e.preventDefault();
        if (Meteor.user()) {
            template.changesMade.set(true);
        }
    },
    'change select[name="profile.countryId"]'(e, template) {
        e.preventDefault();
        template.countryId.set($(e.currentTarget).val());
    },
    'change select[name="profile.travellerTypeId"]'(e, template) {
        e.preventDefault();
        template.travellerTypeId.set($(e.currentTarget).val());
    },
    'change input[name="file-upload"]'(e, template) {
        e.preventDefault();
        let file = e.currentTarget.files[0];
        handleFileUpload(file, template);
    },
    'click .cancel-documentation'(e, template) {
        e.preventDefault();
        template.filesToUpload.clear();
    },
    'click .add-new-email'(e, template) {
        e.preventDefault();
        let newEmail = prompt(Vatfree.translate("Add a secondary email address"));
        if (newEmail) {
            Meteor.call('traveller-add-email', newEmail, (err ,result) => {
                if (err) {
                    console.error(err);
                    alert(Vatfree.translate(err.reason || err.message));
                } else {
                    alert(Vatfree.translate("We sent you an email to verify your address_ Please click on the link in the email message to complete adding the address_"));
                }
            });
        }
    },
    'click .resend-verification-email'(e, template) {
        e.preventDefault();
        if (confirm('Resend email address verification email?')) {
            Meteor.call('traveller-resend-verification-email', this.address, (err, result) => {
                if (err) {
                    console.error(err);
                    alert(Vatfree.translate(err.reason || err.message));
                } else {
                    alert(Vatfree.translate("Verification email re-sent"));
                }
            });
        }
    },
    'click .submit-documentation'(e, template) {
        e.preventDefault();
        template.isUploadingFile.set(true);

        Meteor.call('submit-verification-documentation', template.filesToUpload.array(), (err, result) => {
            if (err) {
                console.error(err);
                toastr.error(Vatfree.translate(err.reason || err.message))
            } else {
                template.filesToUpload.clear();
            }
            template.isUploadingFile.set();
        });
    },
    'click .file-thumb.allow-remove:not(.add-file-button)'(e, template) {
        e.preventDefault();
        if (confirm(Vatfree.translate('Remove file?'))) {
            template.filesToUpload.remove(this);
        }
    },
    'change input[name="fileToUpload"]': function(e, template) {
        e.preventDefault();
        let file = e.currentTarget.files[0];
        let reader = new FileReader();
        let subType = this.subType;
        reader.onload = function(fileLoadEvent) {
            template.app.showPreloader('Uploading...');
            Vatfree.imageHelpers.resetOrientation(reader.result, (err, base64Uri) => {
                let uploadFile = {
                    file: {
                        name: file.name,
                        type: file.type,
                        size: file.size
                    },
                    subType: subType,
                    fileContent: base64Uri
                };
                Vatfree.trackEvent('save-profile-send-file', 'account');
                Meteor.call('submit-verification-documentation', [ uploadFile ], (err,result) => {
                    if (err) {
                        template.app.hidePreloader();
                        alert(Vatfree.translate(err.reason || err.message));
                        Vatfree.trackEvent('save-profile-send-files-error', 'account', {error: err.reason || err.message});
                    } else {
                        template.app.hidePreloader();
                    }
                });
            });
            template.$('input[name="fileToUpload"]').val('');
        };
        reader.readAsDataURL(file);
    },
    'click .file-uploaded'(e, template) {
        let fileId = this._id;

        let selector = {
            itemId: Meteor.userId(),
            target: 'travellers'
        };
        if (this.subType) {
            selector.subType = this.subType;
        } else {
            selector.subType = {
                $exists: false
            }
        }
        openPhotoBrowser(selector, fileId, template);
    },
    'click .tab-link'(e, template) {
        if (template.changesMade.get()) {
            if (confirm(Vatfree.translate('You have unsaved changes to your profile_ Do you want to save the changes?'))) {
                template.$('form[name="item-edit-form"]').submit();
            } else {
                // reset form
                template.renderForm.set(false);
                template.changesMade.set(false)
            }
        }
    },
    'submit form[name="item-edit-form"]'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('save-profile', 'account');

        let user = Meteor.user();
        let formData = Vatfree.templateHelpers.getFormData(template);

        if (user.private.status === 'userVerified') {
            // formData contains "" for empty elements
            if (_.isUndefined(user.profile.name)) user.profile.name = "";
            if (_.isUndefined(user.profile.passportNumber)) user.profile.passportNumber = "";

            let profilePassportExpirationDate;
            if (_.isUndefined(user.profile.passportExpirationDate)) profilePassportExpirationDate = "";
            else profilePassportExpirationDate = moment.tz(user.profile.passportExpirationDate, TAPi18n.__('_date_format'), 'Europe/Amsterdam');

            if (_.isUndefined(user.profile.countryId)) user.profile.countryId = "";

            let changedNCP = Vatfree.travellers.ncpChanged(user, formData.profile);
            if (changedNCP && !confirm(Vatfree.translate('You have changed core elements of your profile_ Your profile status will be set back to "Unverified"_ Are your sure you want to continue?'))) {
                template.renderForm.set(false);
                template.changesMade.set();
                Vatfree.trackEvent('save-profile-cancel-ncp', 'account');
                return false;
            }
        }

        if (formData.profile.birthDate) {
            formData.profile.birthDate = moment.tz(formData.profile.birthDate, "YYYY-MM-DD", 'Europe/Amsterdam').toDate();
        }

        if (formData.profile.passportExpirationDate) {
            formData.profile.passportExpirationDate = moment.tz(formData.profile.passportExpirationDate, "YYYY-MM-DD", 'Europe/Amsterdam').toDate();
        }

        if (formData.profile.visaExpirationDate) {
            formData.profile.visaExpirationDate = moment.tz(formData.profile.visaExpirationDate, "YYYY-MM-DD", 'Europe/Amsterdam').toDate();
        }

        Vatfree.trackEvent('save-profile-send', 'account');
        template.app.showPreloader(Vatfree.translate('Uploading___'));
        Meteor.call('save-my-user-profile', formData.profile, TAPi18n.getLanguage(), (err, result) => {
            if (err) {
                template.app.hidePreloader();
                alert(Vatfree.translate(err.reason || err.message));
                Vatfree.trackEvent('save-profile-error', 'account', {error: err.reason || err.message});
            } else {
                alert(Vatfree.translate('Profile saved'));
                template.app.hidePreloader();
                template.changesMade.set();
            }
        });
    }
});

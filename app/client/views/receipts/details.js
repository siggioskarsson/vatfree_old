import { Template } from 'meteor/templating';
import {Session} from "meteor/session";
import {ReactiveVar} from "meteor/reactive-var";
import moment from 'moment-timezone';
import swal from './info';

Template.view_receipt_details.onCreated(function() {
    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;

    this.receipt = new ReactiveVar();
});

Template.view_receipt_details.onRendered(function() {
    this.autorun(() => {
        let receiptId = Session.get('currentReceipt');
        let user = Meteor.user();
        if (user && receiptId) {
            this.subscribe('my-receipt', receiptId);
        }
    });
});

Template.view_receipt_details.helpers({
    getReceipt() {
        return Receipts.findOne({_id: Session.get('currentReceipt')});
    },
    getShop() {
        return Shops.findOne({_id: this.shopId});
    },
    getFiles() {
        return Files.find({
            itemId: Session.get('currentReceipt'),
            target: 'receipts'
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getPartnershipStatusLogo() {
        let shop = Shops.findOne({_id: this.shopId}) || {};
        switch (shop.partnershipStatus) {
            case 'pledger':
            case 'affiliate':
                return 'vfcircleblue.png';
            case 'partner':
                return 'vfcircleshade.png';
            case 'new':
                return 'vfcircleunkown.png';
            default:
                return 'vfcirclenonrefund.png';
        }
    },
    waitingForOriginals() {
        return !this.originalsCheckedBy || !this.originalsCheckedAt;
    },
    allowUploads() {
        let receipt = Receipts.findOne({_id: Session.get('currentReceipt')});
        return Vatfree.allowTravellerUpload(receipt) && receipt.status !== 'waitingForCustomsStamp';
    },
    getTravellerStatus() {
        let traveller = Meteor.user() || {};
        return Vatfree.receipts.getTravellerStatusForTraveller(this, traveller);
    },
    allowDeleteReceipt() {
        return Vatfree.receipts.travellerAllowedDeleted(this);
    },
    isRejected() {
        return _.contains(['rejected', 'depreciated', 'notCollectable'], this.status);
    }
});

Template.view_receipt_details.events({
    'click .receipt-stamped'(e, template) {
        e.preventDefault();
        myApp.popup('.popup-customs-stamp');
    },
    'click .photo'(e, template) {
        let fileId = this._id;
        let selector = {
            itemId: Session.get('currentReceipt'),
            target: 'receipts'
        };
        openPhotoBrowser(selector, fileId, template);
    },
    'change input[name="receiptFile"]': function(e, template) {
        e.preventDefault();
        let file = e.currentTarget.files[0];
        let receiptId = this._id;

        var reader = new FileReader();
        reader.onload = function(fileLoadEvent) {
            let fileUpload = {
                name: file.name,
                type: file.type,
                size: file.size,
                fileContent: reader.result
            };
            Meteor.call('upload-receipt-image', receiptId, fileUpload, (err, result) => {
                if (err) {
                    toastr.error(Vatfree.translate(err.reason || err.message));
                } else {
                    template.$('input[name="receiptFile"]').val('');
                }
            });
        };
        reader.readAsDataURL(file);
    },
    'click .delete-receipt'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('deleted-receipt', 'receipts');
        let receiptId = this._id;
        import swal from 'sweetalert2';
        swal({
            title: Vatfree.translate("Are you sure?"),
            text: Vatfree.translate("You will not be able to recover this receipt!"),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: Vatfree.translate("Yes, delete it!"),
            closeOnConfirm: true,
            reverseButtons: true
        }).then(function () {
            Meteor.call('traveller-delete-receipt', receiptId, (err, result) => {
                if (err) {
                    toastr.error(Vatfree.translate(err.reason || err.message));
                } else {
                    toastr.success(Vatfree.translate("Receipt deleted"));
                }
            });
        });
    }
});

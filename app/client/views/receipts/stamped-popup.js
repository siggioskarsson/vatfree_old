import { Template } from "meteor/templating";
import { Session } from "meteor/session";
import moment from 'moment-timezone';

Template.popup_receipt_stamped.helpers({
    getUploadedFiles() {
        return Files.find({
            itemId: Session.get('currentReceipt'),
            target: 'receipts',
            createdAt: {
                $gt: moment().subtract(10, 'minutes').toDate()
            }
        },{
            sort: {
                createdAt: 1
            }
        });
    }
});

Template.popup_receipt_stamped.events({
    'click .submit-receipt-is-stamped'(e, template) {
        e.preventDefault();
        const receiptId = Session.get('currentReceipt');
        const receipt = Receipts.findOne({_id: receiptId});

        let customsDate = template.$('input[name="customsDate"]').val();
        customsDate = moment.tz(customsDate, 'YYYY-MM-DD', 'Europe/Amsterdam');
        if (!customsDate.isValid()) {
            alert(Vatfree.translate('Customs date is not a valid date'));
            return false;
        }

        if (moment(receipt.purchaseDate).isAfter(customsDate)) {
            alert(Vatfree.translate('Customs stamp cannot be before the purchase date'));
            Vatfree.trackEvent('submit-receipt-error', 'receipts', {error: Vatfree.translate("Customs stamp before purchase date")});
            return false;
        }

        if (!Files.find({
            itemId: Session.get('currentReceipt'),
            target: 'receipts',
            createdAt: {
                $gt: moment().subtract(10, 'minutes').toDate()
            }
        }).count()) {
            alert(Vatfree.translate('Upload a photo of the stamped receipt'));
            Vatfree.trackEvent('submit-receipt-error', 'receipts', {error: Vatfree.translate("Upload a photo of the stamped receipt")});
            return false;
        }

        Meteor.call('receipt-is-stamped', receiptId, customsDate.toDate(), (err, result) => {
            if (err) {
                toastr.error(Vatfree.translate(err.reason || err.message));
            } else {
                template.$('input[name="receiptFileInPopup"]').val('');
                $('div.close-popup').trigger('click');
                myApp.popup('.popup.popup-customs-stamp-done');
            }
        });
    },
    'change input[name="receiptFileInPopup"]': function(e, template) {
        e.preventDefault();
        let file = e.currentTarget.files[0];
        let receiptId = Session.get('currentReceipt');

        var reader = new FileReader();
        reader.onload = function(fileLoadEvent) {
            let fileUpload = {
                name: file.name,
                type: file.type,
                size: file.size,
                fileContent: reader.result
            };
            Meteor.call('upload-receipt-image', receiptId, fileUpload, (err, result) => {
                if (err) {
                    toastr.error(Vatfree.translate(err.reason || err.message));
                } else {
                    template.$('input[name="receiptFileInPopup"]').val('');
                }
            });
        };
        reader.readAsDataURL(file);
    }
});

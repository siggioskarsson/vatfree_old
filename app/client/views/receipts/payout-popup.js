import { Template } from 'meteor/templating';

Template.payout_popup.onCreated(function() {
    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;


    this.hideModal = function() {
        this.app.closeModal('.popup-payout');
    };
});

Template.payout_popup.onRendered(function() {
    this.$$('.popup-payout').on('popup:opened', function () {
        console.log('payout Popup opened')
    });
    this.$$('.popup-payout').on('popup:close', function () {
        console.log('payout Popup is closing')
    });
    this.$$('.popup-payout').on('popup:open', function () {
        console.log('payout Popup is opening');
    });
    this.$$('.popup-payout').on('popup:closed', function () {
        console.log('payout Popup is closed');
    });
});

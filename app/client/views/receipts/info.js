import moment from 'moment';
import { receiptHelpers } from './helpers';

Template.receiptInfo.onCreated(function() {
    this.app = new Framework7();
    this.$$ = Dom7;

    this.isUploadingFile = new ReactiveVar();
});
Template.receiptInfo.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        let user = Meteor.user();
        if (data && data.receipt && user) {
            this.subscribe('traveller-types');
            this.subscribe('my-profile');
            this.subscribe('receipt-rejections');
        }
    });
});

Template.receiptInfo.helpers(receiptHelpers);
Template.receiptInfo.helpers({
    isUploadingFile() {
        return Template.instance().isUploadingFile.get();
    },
    getFiles() {
        let receiptId = this._id;
        return Files.find({
            target: 'receipts',
            itemId: receiptId
        },{
            sort: {
                createdAt: 1
            }
        });
    },
    getLargePhotoUrl() {
        if (this.copies && this.copies['vatfree'] && this.copies['vatfree'].size > 0) {
            return this.url({store: 'vatfree'});
        } else if (this.copies && this.copies['vatfree-original'] && this.copies['vatfree-original'].size > 0) {
            return this.url({store: 'vatfree-original'});
        } else {
            return this.url({store: 'vatfree-thumbs'});
        }
    },
    getCountry() {
        return Countries.findOne({
            _id: this.profile.countryId
        });
    },
    passportExpired() {
        return moment(this.profile.passportExpirationDate).isBefore(moment());
    },
    currentTraveller() {
        return Travellers.findOne({_id: Meteor.userId()});
    },
    userIsVerified() {
        return this.private && this.private.status ? this.private.status === 'userVerified' : false;
    },
    getStatusDescription() {
        if (this.private && this.private.status) {
            let travellerType = TravellerTypes.findOne({_id: this.private.travellerTypeId}) || {};
            if (!(travellerType.isNato && this.private.status === 'waitingForDocuments')) {
                return TAPi18n.__('traveller_status_description_' + this.private.status) || "";
            }
        }
    },
    statusRejected() {
        return this.status === 'rejected' || this.status === 'notCollectable';
    },
    getReceiptRejections() {
        let selector = {};
        if (this.receiptRejectionIds && this.receiptRejectionIds.length > 0) {
            selector['_id'] = {
                $in: this.receiptRejectionIds
            };
            return ReceiptRejections.find(selector, {
                sort: {
                    name: 1
                }
            });
        }
    },
    getReceiptRejectionText() {
        const language = Vatfree.getLanguage() || 'en';
        return this.text[language] || this.text['en'];
    },
    receiptIsApproved() {
        return _.contains([
            'readyForInvoicing',
            'waitingForPayment',
            'paidByShop',
            'requestPayout',
            'paid'], this.status);
    },
    allowUploads() {
        let receipt = Template.instance().data.receipt;
        return Vatfree.allowTravellerUpload(receipt);
    },
    allowReopenReceipt() {
        let allowReopen = false;
        let selector = {};
        if (this.receiptRejectionIds && this.receiptRejectionIds.length > 0) {
            selector['_id'] = {
                $in: this.receiptRejectionIds
            };
            ReceiptRejections.find(selector).forEach((receiptRejection) => {
                if (receiptRejection.allowReopen) {
                    allowReopen = true;
                }
            });
        }

        return allowReopen
    },
    getTravellerType() {
        let traveller = Meteor.user();
        if (traveller && traveller.private) {
            let travellerType = TravellerTypes.findOne({
                    _id: traveller.private.travellerTypeId
                }) || {};
            return travellerType;
        }
    },
    isNatoReceipt() {
        let receiptType = ReceiptTypes.findOne({_id: this.receiptTypeId});
        return receiptType ? receiptType.name === "NATO" : false;
    },
    getTravellerStatus() {
        let traveller = Meteor.user() || {};
        return Vatfree.receipts.getTravellerStatusForTraveller(this, traveller);
    }
});

Template.receiptInfo.events({
    'click .dropzone .add-file'(e) {
        e.preventDefault();
        e.stopPropagation();
        $('input[name="dropzone-file"]').trigger('click');
    },
    'change input[name="dropzone-file"]'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        FS.Utility.eachFile(e, function (file) {
            handleFileUpload.call(template, file);
        });
    },
    'dropped .dropzone'(e, template) {
        e.preventDefault();
        e.stopPropagation();
        $('.dropzone').removeClass('visible');
        FS.Utility.eachFile(e, function (file) {
            handleFileUpload.call(template, file);
        });
    },
    'click .reopen-receipt'(e, template) {
        e.preventDefault();
        const receiptId = this._id;
        if (confirm(Vatfree.translate("Reopen receipt for submission?"))) {
            Meteor.call('receipt-reopen', receiptId, (err, result) => {
                if (err) {
                    toastr.error(Vatfree.translate(err.reason || err.message));
                } else {
                    toastr.success(Vatfree.translate("Receipt reopened"));
                }
            });
        }
    },
    'click .delete-file'(e, template) {
        e.preventDefault();
        let fileId = this._id;
        import swal from 'sweetalert2';
        swal({
            title: Vatfree.translate("Are you sure?"),
            text: Vatfree.translate("You will not be able to recover this file!"),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: Vatfree.translate("Yes, delete it!"),
            closeOnConfirm: true,
            reverseButtons: true
        }).then(function () {
            Meteor.call('delete-receipt-image', fileId, (err, result) => {
                if (err) {
                    toastr.error(Vatfree.translate(err.reason || err.message));
                }
            });
        });
    }
});

var handleFileUpload = function(file) {
    let template = this;
    let receiptId = template.data.receipt._id;
    if (file.type.match(/image\/(jpg|jpeg|png|gif)/)) {
        template.isUploadingFile.set(true);

        let reader = new FileReader();
        reader.onload = function (fileLoadEvent) {
            let fileUpload = {
                name: file.name,
                type: file.type,
                size: file.size,
                fileContent: reader.result
            };
            Meteor.call('upload-receipt-image', receiptId, fileUpload, (err, result) => {
                if (err) {
                    toastr.error(Vatfree.translate(err.reason || err.message));
                } else {
                    template.$('input[name="dropzone-file"]').val('');
                }
                template.isUploadingFile.set();
            });
        };
        reader.onerror = function(err) {
            toast.error(err.reason || err.message);
            template.isUploadingFile.set();
        };
        reader.readAsDataURL(file);

    } else {
        toastr.error(Vatfree.translate('Only image files are supported (jpeg, jpg, png, gif)'));
        template.$('input[name="dropzone-file"]').val('');
    }
};

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';

Template.view_receipts.onCreated(function() {
    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;

    this.stack = new ReactiveVar(0);

    this.popup = false;
});

Template.view_receipts.onRendered(function() {
    let template = this;

    this.autorun(() => {
        let user = Meteor.user();
        if (user) {
            this.subscribe('my-receipts');
        }
    });

    Tracker.afterFlush(() => {
        this.appView = this.app.addView('.view-receipts', {
            domCache: true, //enable inline pages
            onSwipeBackAfterChange: function(e) {
                template.stack.set(template.stack.get() - 1);
            }
        });
    });
});

let helpers = {
    stackSize() {
        return Template.instance().stack.get();
    },
    getTotalEstimatedRefunds() {
        let amount = 0;

        Receipts.find({
            userId: Meteor.userId(),
            status: {
                $nin: ['deleted', 'rejected', 'paid', 'notCollectable', 'depreciated']
            }
        }).forEach((receipt) => {
            if (receipt.euroRefund) {
                amount += Number(receipt.euroRefund || 0);
            } else {
                amount += Number(receipt.refund || 0);
            }
        });

        return amount;
    },
    getPayoutReceipts() {
        return Receipts.find({
            userId: Meteor.userId(),
            status: 'paidByShop'
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    payoutAmount() {
        let amount = 0;

        Receipts.find({
            userId: Meteor.userId(),
            status: 'paidByShop'
        }).forEach((receipt) => {
            if (receipt.euroRefund) {
                amount += Number(receipt.euroRefund || 0);
            } else {
                amount += Number(receipt.refund || 0);
            }
        });

        return amount;
    },
    getUnstampedReceipts() {
        return Receipts.find({
            userId: Meteor.userId(),
            status: 'waitingForCustomsStamp'
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getMissingOriginalReceipts() {
        return Receipts.find({
            userId: Meteor.userId(),
            status: 'waitingForOriginal'
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getPaidReceipts() {
        return Receipts.find({
            userId: Meteor.userId(),
            status: 'paid'
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getReceipts() {
        return Receipts.find({
            userId: Meteor.userId(),
            status: {
                $nin: ['deleted', 'rejected', 'waitingForOriginal', 'paidByShop', 'paid', 'waitingForCustomsStamp']
            }
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    getRejectedReceipts() {
        return Receipts.find({
            userId: Meteor.userId(),
            status: 'rejected'
        },{
            sort: {
                createdAt: -1
            }
        });
    },
    receiptsNeedSending() {
        return false;
        return Receipts.find({
            status: 'waitingForOriginal'
        }).count() > 0;
    },
    getShop() {
        return Shops.findOne({_id: this.shopId});
    },
    getPartnershipStatusLogo() {
        let shop = Shops.findOne({_id: this.shopId}) || {};
        switch (shop.partnershipStatus) {
            case 'pledger':
            case 'affiliate':
                return 'vfcircleblue.png';
            case 'partner':
                return 'vfcircleshade.png';
            case 'new':
                return 'vfcircleunkown.png';
            default:
                return 'vfcirclenonrefund.png';
        }
    },
    getTravellerStatus() {
        let traveller = Meteor.user() || {};
        return Vatfree.receipts.getTravellerStatusForTraveller(this, traveller);
    }
};
Template.view_receipts.helpers(helpers);
Template.receipt_list_item.helpers(helpers);

Template.view_receipts.events({
    'resetView'(e, template) {
        let stack = template.stack.get();
        for(var i = 0; i <= stack; i++) {
            template.appView.router.back();
        }
        Session.set('currentReceipt', false);
        template.stack.set(0);
    },
    'click a.back'(e, template) {
        e.preventDefault();
        template.appView.router.back();
        template.stack.set(template.stack.get() - 1);
    },
    'click .my-receipts-header'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('open-receipt-send-info', 'receipts');
        template.popup = template.app.popup('.receipt-send-info');
    },
    'click .close-popup'(e, template) {
        e.preventDefault();
        if (template.popup) {
            template.popup.close();
        }
    },
    'click .item-link.receipt'(e, template) {
        e.preventDefault();
        if (this._id) {
            Session.set('currentReceipt', this._id);
            Vatfree.trackEvent('open-receipt-details', 'receipts');
            template.appView.router.load({pageName: 'receiptdetails'});
            template.stack.set(template.stack.get() + 1);
        }
    },
    'click .item-link.shop'(e, template) {
        e.preventDefault();
        Session.set('currentShop', this.shopId);
        Tracker.afterFlush(() => {
            Vatfree.trackEvent('open-shop-details', 'receipts');
            template.appView.router.load({pageName: 'shopdetails'});
            template.stack.set(template.stack.get() + 1);
        });
    },
    'click .go-to-account'(e, template) {
        e.preventDefault();
        template.$$('.view').hide(); template.$$('.view-account').show();
        Session.set('currentPage', "account");
    },
    'click .shops-learn-more'(e, template) {
        e.preventDefault();
        Tracker.afterFlush(() => {
            Vatfree.trackEvent('open-shops-learn-more', 'receipts');
            template.appView.router.load({pageName: 'shops-learn-more'});
            template.stack.set(template.stack.get() + 1);
        });
    },
    'click .payout-refund'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('open-payouts', 'receipts');
        console.log(this);
        template.app.popup('.popup-payout');
    }
});

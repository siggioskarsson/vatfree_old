import moment from 'moment-timezone';
import uriParse from 'url-parse';
import {Session} from "meteor/session";
import { ReactiveVar } from "meteor/reactive-var";

Template.view_receipt_add.onCreated(function() {
    let template = this;

    this.app = new Framework7();
    this.appView = {};
    this.$$ = Dom7;
    this.stack = new ReactiveVar(0);

    this.instructionsSeen = new ReactiveVar(!!Session.get('receipt.instruction-seen') || !!localStorage.getItem('receipt.instruction-seen'));
    this.qrCodeOnReceiptAsked = new ReactiveVar();

    this.searchTerm = new ReactiveVar();
    this.searchCount = new ReactiveVar();
    this.limitIncrement = 20;
    this.limit = new ReactiveVar(this.limitIncrement);
    this.loading = new ReactiveVar();

    this.poprDoc = new ReactiveVar();
    this.shopId = new ReactiveVar();
    this.amount = new ReactiveVar();
    this.purchaseDate = new ReactiveVar(); //moment().format('YYYY-MM-DD'));
    this.customsDate = new ReactiveVar(); //moment().format('YYYY-MM-DD'));
    this.files = new ReactiveArray();
    this.stopInfiniteScrolling = new ReactiveVar();
    this.geoLocation = new ReactiveVar();
    this.addingShop = new ReactiveVar();

    this.popup = false;
    this.addedReceipt = new ReactiveVar();
    this.receiptIsStamped = new ReactiveVar();

    this.autorun(() => {
        Session.set('receipt.instruction-seen', !!this.instructionsSeen.get());
        localStorage.setItem('receipt.instruction-seen', !!this.instructionsSeen.get());
    });

    this.autorun(() => {
        let shopId = Session.get('setShopIdForNewReceipt');
        if (shopId) {
            this.shopId.set(shopId);
        }
        Session.set('setShopIdForNewReceipt');
    });

    this.autorun(() => {
        if (this.shopId.get()) {
            this.subscribe('shops_item', this.shopId.get());
        }
    });

    this.autorun(() => {
        if (Meteor.userId()) {
            // autorun if search term changes
            let searchTerm = this.searchTerm.get();
            this.limit.set(this.limitIncrement);
            this.stopInfiniteScrolling.set();
        }
    });

    this.autorun(() => {
        if (Meteor.userId()) {
            if (!this.addingShop.get()) {
                let geoLocation = this.geoLocation.get();
                let newLocation = Geolocation.currentLocation();
                if (newLocation && newLocation.coords) {
                    let newLatitude = Math.round(newLocation.coords.latitude * 100000) / 100000;
                    let newLongitude = Math.round(newLocation.coords.longitude * 100000) / 100000;
                    if (!geoLocation || !geoLocation.coords || geoLocation.coords.latitude !== newLatitude || geoLocation.coords.longitude !== newLongitude) {
                        this.geoLocation.set({
                            coords: {
                                latitude: newLatitude,
                                longitude: newLongitude
                            }
                        });
                    }
                }
            }
        }
    });

    this.autorun(() => {
        let searchTerm = this.searchTerm.get();
        let limit = this.limit.get();
        let geo = this.geoLocation.get();
        this.subscribe('shop-search-public', searchTerm, (geo && geo.coords ? [ geo.coords.longitude, geo.coords.latitude ] : false), limit);
    });

    this.autorun(() => {
        if (this.subscriptionsReady()) {
            if (this.limit.get() > ShopSearch.find().count()) {
                this.stopInfiniteScrolling.set(true);
            }
        }
    });

    this.resetReceipt = function() {
        template.poprDoc.set();
        template.shopId.set();
        template.amount.set();
        template.purchaseDate.set();
        template.customsDate.set();
        template.receiptIsStamped.set();
        template.files.clear();
        template.qrCodeOnReceiptAsked.set();
    }
});

Template.view_receipt_add.onRendered(function() {
    let template = this;

    this.$$('.search-shop-add.infinite-scroll').on('infinite', function() {
        if (!template.stopInfiniteScrolling.get()) {
            if (!template.loading.get()) {
                template.loading.set(true);
                template.limit.set(template.limit.get() + template.limitIncrement);
            }
        }

        Meteor.setTimeout(() => {
            template.loading.set(false);
        }, 1000);
    });

    Tracker.afterFlush(() => {
        this.appView = myApp.addView('.view-add', {
            domCache: true, //enable inline pages
            onSwipeBackAfterChange: function(e) {
                template.stack.set(template.stack.get() - 1);
            }
        });
    });
});

Template.view_receipt_add.helpers({
    stackSize() {
        return Template.instance().stack.get();
    },
    instructionsSeen() {
        return Template.instance().instructionsSeen.get();
    },
    qrCodeOnReceiptAsked() {
        return Template.instance().qrCodeOnReceiptAsked.get();
    },
    poprDoc() {
        return Template.instance().poprDoc.get();
    },
    hasPoprDoc() {
        return !!Template.instance().poprDoc.get();
    },
    shop() {
        let shopId = Template.instance().shopId.get();
        if (shopId) {
            return Shops.findOne({_id: shopId});
        }

        return false;
    },
    amount() {
        return Template.instance().amount.get();
    },
    purchaseDate() {
        return Template.instance().purchaseDate.get();
    },
    customsDate() {
        return Template.instance().customsDate.get();
    },
    getFiles() {
        return Template.instance().files.array();
    },
    addedReceipt() {
        return Template.instance().addedReceipt.get();
    },
    addingShop() {
        return Template.instance().addingShop.get();
    },
    isUncooperativeShop() {
        return this.partnershipStatus === "uncooperative";
    },
    allowedToAddReceipt() {
        const shopId = Template.instance().shopId.get();
        if (shopId) {
            const shop = Shops.findOne({_id: shopId}) || {};
            return shop.partnershipStatus !== "uncooperative";
        }
        return true;
    },
    selectMapLocationCallback() {
        let template = Template.instance();
        return function(place) {
            if (place && place.address_components) {
                template.$('input[name="place"]').val(JSON.stringify(place));
            }
        }
    },
    getEUCountries() {
        return Countries.find({needVisa: true}, {
            sort: {
                name: 1
            }
        });
    },
    receiptIsStamped(value) {
        return Template.instance().receiptIsStamped.get() === value;
    },
    receiptStampedChoice() {
        return !_.isUndefined(Template.instance().receiptIsStamped.get());
    }
});
Template.view_receipt_add.helpers(ShopHelpers);

let handleQrCodeScan = function (result, template) {
    console.log("We got a barcode\n" +
        "Result: " + result.text + "\n" +
        "Format: " + result.format + "\n" +
        "Cancelled: " + result.cancelled);
    let uri = uriParse(result.text, true);
    if (uri && uri.protocol === 'https:' && uri.hostname === 'app.popr.io' && uri.pathname) {
        let pathParts = uri.pathname.split('/');
        let poprId = pathParts[1];
        let poprHash = pathParts[2];
        Meteor.call('get-popr-info', poprId, poprHash, (err, poprDoc) => {
            if (err) {
                alert(Vatfree.translate(err.reason || err.message));
            } else {
                poprDoc.poprId = poprId;
                poprDoc.poprHash = poprHash;
                template.poprDoc.set(poprDoc);

                if (poprDoc.shop) {
                    template.shopId.set(poprDoc.shop._id);
                }

                template.amount.set(parseFloat(poprDoc.totalValue / 100).toFixed(2));

                let date = moment.tz(poprDoc.time, 'X', 'Europe/Amsterdam');
                template.purchaseDate.set(date.format('YYYY-MM-DD'));

                template.qrCodeOnReceiptAsked.set(true);
            }
        });
    } else {
        template.qrCodeOnReceiptAsked.set(true);
    }
};
Template.view_receipt_add.events({
    'click .receipt-stamped-yes'(e, template) {
        e.preventDefault();
        template.receiptIsStamped.set(true);
    },
    'click .receipt-stamped-no'(e, template) {
        e.preventDefault();
        template.receiptIsStamped.set(false);
    },
    'click .got-customs-stamp'(e, template) {
        e.preventDefault();
        template.instructionsSeen.set(false);
    },
    'click .add-new-shop'(e, template) {
        e.preventDefault();
        template.addingShop.set(true);
    },
    'click .cancel-add-shop'(e, template) {
        e.preventDefault();
        template.addingShop.set(false);
    },
    'click .submit-add-shop'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('add-shop', 'receipts');

        let formData = Vatfree.templateHelpers.getFormData(template, 'form[name="add-shop-form"]');

        if (!formData.geo && !formData.place) {
            toastr.error(Vatfree.translate("Please select the shop by searching for it in the input field"));
            return false;
        }

        try {
            let geo = JSON.parse(formData.geo);
            let place = JSON.parse(formData.place);
            if (place && place.address_components) {
                Meteor.call('traveller-add-shop', place, geo, (err, shopId) => {
                    if (err) {
                        toastr.error(Vatfree.translate(err.reason || err.message));
                    } else {
                        template.shopId.set(shopId);
                        template.addingShop.set(false);
                        template.appView.router.back();
                        template.stack.set(0);
                    }
                });
            } else {
                toastr.error(Vatfree.translate("Please select a shop from the pull down menu"))
            }
        } catch(e) {
            toastr.error(Vatfree.translate("Please select a shop from the pull down menu"))
        }

    },
    'click .go-to-account'(e, template) {
        e.preventDefault();
        template.$$('.view').hide();
        template.$$('.view-account').show();
        Session.set('currentPage', "account");
    },
    'click .instructions-seen'(e, template) {
        e.preventDefault();
        template.instructionsSeen.set(true);
    },
    'click a.back'(e, template) {
        e.preventDefault();
        template.appView.router.back();
        template.stack.set(template.stack.get() - 1);
    },
    'click .qr-select-yes'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('add-receipt-qr', 'receipts');

        if (Meteor.isCordova) {
            cordova.plugins.barcodeScanner.scan(
                function (result) {
                    handleQrCodeScan(result, template);
                },
                function (error) {
                    f7App.alert(Vatfree.translate("Scanning failed: __error__", {error: error}));
                    template.qrCodeOnReceiptAsked.set(true);
                },
                {
                    preferFrontCamera : false,
                    showFlipCameraButton : true,
                    showTorchButton : true,
                    torchOn: false,
                    prompt : Vatfree.translate("Place a QR-code inside the scan area"), // Android
                    resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                    formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
                    orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
                    disableAnimations : true, // iOS
                    disableSuccessBeep: false // iOS
                }
            );
        } else {
            //handleQrCodeScan({text: "https://app.popr.io/.../..."}, template);
            var qr = prompt(Vatfree.translate("QR code"));
            handleQrCodeScan({
                text: 'https://app.popr.io/' + qr,
                format: 'qr'
            }, template);
        }
    },
    'click .qr-select-no'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('add-receipt-normal', 'receipts');
        template.qrCodeOnReceiptAsked.set(true);
    },
    'click .select-shop blockquote a'(e, template) {
        e.preventDefault();
        let href = $(e.currentTarget).attr('href');
        window.open(href, '_system');
    },
    'click .select-shop a.item-link'(e, template) {
        e.preventDefault();
        if (template.poprDoc.get() && template.shopId.get()) {
            console.log('not opening shop, popr doc present');
        } else {
            console.log('opening shop');
            template.appView.router.load({pageName: 'shop-selector'});
            template.stack.set(template.stack.get() + 1);
        }
    },
    'change input[name="searchTerm"]'(e, template) {
        e.preventDefault();
        template.searchTerm.set($(e.currentTarget).val());
    },
    'keyup input[name="searchTerm"]': _.debounce((e, template) => {
        e.preventDefault();
        template.searchTerm.set($(e.currentTarget).val());
    }, 300),
    'click .item-link'(e, template) {
        e.preventDefault();
        template.shopId.set(this._id);
        template.appView.router.back();
        template.stack.set(0);
    },
    'change input[name="amount"]'(e, template) {
        e.preventDefault();
        template.amount.set(Number(template.$('input[name="amount"]').val()));
    },
    'change input[name="purchaseDate"]'(e, template) {
        e.preventDefault();
        console.log('purchaseDate changed', template.$('input[name="purchaseDate"]').val());
        template.purchaseDate.set(template.$('input[name="purchaseDate"]').val());
    },
    'change input[name="customsDate"]'(e, template) {
        e.preventDefault();
        template.customsDate.set(template.$('input[name="customsDate"]').val());
    },
    'change input[name="receiptFile"]': function(e, template) {
        e.preventDefault();
        var file = e.currentTarget.files[0];
        var reader = new FileReader();
        template.app.showPreloader(Vatfree.translate('Processing image___'));
        reader.onload = function(fileLoadEvent) {
            Vatfree.imageHelpers.resetOrientation(reader.result, (err, base64Uri) => {
                if (err) {
                    console.error(err);
                    template.app.hidePreloader();
                } else {
                    Vatfree.imageHelpers.resizeDataUriImage(base64Uri, 4032, function (err, base64Uri) {
                        if (err) {
                            console.error(err);
                        } else {
                            template.files.push({
                                file: {
                                    name: file.name,
                                    type: file.type,
                                    size: file.size
                                },
                                fileContent: base64Uri
                            });
                        }
                        template.app.hidePreloader();
                    });
                }
            });
            template.$('input[name="receiptFile"]').val('');
        };
        reader.readAsDataURL(file);
    },
    'click .file-thumb:not(.add-file-button)'(e, template) {
        e.preventDefault();
        if (confirm(Vatfree.translate('Remove file?'))) {
            template.files.remove(this);
        }
    },
    'click .close-popup'(e, template) {
        e.preventDefault();
        if (template.popup) {
            template.popup.close();
        }
    },
    'click .cancel-receipt'(e, template) {
        e.preventDefault();
        template.resetReceipt();
    },
    'click .submit-receipt'(e, template) {
        e.preventDefault();
        Vatfree.trackEvent('submit-receipt', 'receipts');

        let shopId = template.shopId.get();
        let amount = Number(template.amount.get() || template.$('input[name="amount"]').val());
        let purchaseDate = template.purchaseDate.get() || template.$('input[name="purchaseDate"]').val();
        let customsDate = template.customsDate.get() || template.$('input[name="customsDate"]').val();
        let flightNumber = template.$('input[name="flightNumber"]').val() || null;
        let files = template.files.array();
        const receiptIsStamped = template.receiptIsStamped.get();

        // fix dates
        purchaseDate = moment.tz(purchaseDate, 'YYYY-MM-DD', 'Europe/Amsterdam').toDate();
        customsDate = moment.tz(customsDate, 'YYYY-MM-DD', 'Europe/Amsterdam').toDate();

        if (!shopId || !amount || !purchaseDate || !customsDate) {
            alert(Vatfree.translate('Please fill in all the fields'));
            Vatfree.trackEvent('submit-receipt-error', 'receipts', {error: Vatfree.translate("Not all fields filled in")});
            return false;
        }
        if (receiptIsStamped && (!files || files.length <= 0)) {
            alert(Vatfree.translate('At least 1 photo of the receipt is required'));
            Vatfree.trackEvent('submit-receipt-error', 'receipts', {error: Vatfree.translate("Photo missing")});
            return false;
        }

        if (moment(purchaseDate).isAfter(customsDate)) {
            alert(Vatfree.translate('Customs stamp cannot be before the purchase date'));
            Vatfree.trackEvent('submit-receipt-error', 'receipts', {error: Vatfree.translate("Customs stamp before purchase date")});
            return false;
        }

        if (moment(purchaseDate).add(3, 'months').endOf('month').isBefore(customsDate)) {
            alert(Vatfree.translate('Customs stamp cannot be more than 3 months after purchase date'));
            Vatfree.trackEvent('submit-receipt-error', 'receipts', {error: Vatfree.translate("Customs stamp is more than 3 months after purchase date")});
            return false;
        }

        let shop = Shops.findOne({_id: shopId}) || {};
        let country = Countries.findOne({_id: shop.countryId}) || {};
        let currency = Currencies.findOne({_id: shop.currencyId}) || {};
        let currencySymbol = currency.symbol || "€";

        if (country && country.minimumReceiptAmount && country.minimumReceiptAmount > amount*100) {
            alert(Vatfree.translate("Minimum receipt amount for tax refund is __minimum_amount__ in __country__",  {minimum_amount: Vatfree.numbers.formatCurrency(country.minimumReceiptAmount, 2, currencySymbol), country: country.name}));
            Vatfree.trackEvent('submit-receipt-error', 'receipts', {error: Vatfree.translate("Minimum receipt amount for tax refund not met")});
            return false;
        }

        let sourceIdentifier = navigator.userAgent || window.ua;
        if (Meteor.isCordova && device) {
            sourceIdentifier = 'MOBILE: ' + device.platform + ' / ' + device.model + ' / ' + device.version;
        }
        Vatfree.trackEvent('submit-receipt-send', 'receipts');
        template.app.showPreloader(Vatfree.translate('Uploading___'));
        Meteor.call('submit-traveller-receipt-v2', shopId, amount, purchaseDate, customsDate, receiptIsStamped, flightNumber, files, template.poprDoc.get(), sourceIdentifier, (err, receipt) => {
            if (err) {
                alert(Vatfree.translate(err.reason || err.message));
                template.app.hidePreloader();
                Vatfree.trackEvent('submit-receipt-send-error', 'receipts', {error: err.reason || err.message});
            } else {
                template.resetReceipt();

                template.app.hidePreloader();
                template.addedReceipt.set(receipt);
                template.popup = template.app.popup('.receipt-added');

                $('.toolbar-inner #receipts.tab-link').trigger('click');
                $('.toolbar-inner #receipts.tab-link').trigger('click');
            }
        });
    }
});

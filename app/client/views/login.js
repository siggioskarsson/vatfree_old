Meteor.startup(() => {
    if (Meteor.isCordova && facebookConnectPlugin) {
        facebookConnectPlugin.activateApp(function (success) {
            console.log(success)
        }, function (error) {
            console.log(error)
        });
    }
});

Template.login.onCreated(function() {
    this.loginWithPhone = new ReactiveVar(false);
    this.loginWithEmail = new ReactiveVar(false);
    this.registering = new ReactiveVar();
    this.isLoggingIn = new ReactiveVar();
    this.isRetrievingPassword = new ReactiveVar();
    this.resetCodeSent = new ReactiveVar();
    this.phoneCodeSent = new ReactiveVar();
    this.app = new Framework7();

    this.autorun(() => {
        if (!this.isRetrievingPassword.get()) {
            this.resetCodeSent.set();
        }
    });
});

Template.login.helpers({
    loginWithPhone() {
        return Template.instance().loginWithPhone.get();
    },
    loginWithEmail() {
        return Template.instance().loginWithEmail.get();
    },
    showLoginOptions() {
        return (!Template.instance().loginWithEmail.get() && !Template.instance().loginWithPhone.get());
    },
    isRegistering() {
        return Template.instance().registering.get();
    },
    isLoggingIn() {
        return Template.instance().isLoggingIn.get();
    },
    isRetrievingPassword() {
        return Template.instance().isRetrievingPassword.get();
    },
    resetCodeSent() {
        return Template.instance().resetCodeSent.get();
    },
    phoneCodeSent() {
        return Template.instance().phoneCodeSent.get();
    },
    isActiveLogin(id) {
        let defaults = Vatfree.defaults.get() || {};
        return defaults && defaults.login ? _.has(defaults.login, id) : false;
    },
    notEnglish() {
        return Vatfree.getLanguage() !== 'en';
    }
});

const postLoginCheck = function () {
    const user = Meteor.user();
    if (user && (!user.profile || !user.profile.language)) {
        Meteor.call('setUserLanguage', Vatfree.getLanguage());
    }
};

Template.login.events({
    'change form[name="language-select-form"] select[name="language"]'(e, template) {
        e.preventDefault();
        Vatfree.setTravellerLanguage($(e.currentTarget).val());
    },
    'click .loginWithEmail'(e, template) {
        e.preventDefault();
        template.loginWithEmail.set(!template.loginWithEmail.get());
    },
    'click .loginWithPhone'(e, template) {
        e.preventDefault();
        template.loginWithPhone.set(!template.loginWithPhone.get());
    },
    'click .register-account'(e, template) {
        e.preventDefault();
        template.registering.set(true);
    },
    'click .cancel-register-account'(e, template) {
        e.preventDefault();
        template.registering.set();
    },
    'click .forgot-password'(e, template) {
        e.preventDefault();
        template.isRetrievingPassword.set(true);
    },
    'click .cancel-forgot-password'(e, template) {
        e.preventDefault();
        template.isRetrievingPassword.set();
    },
    'click .password-reset'(e, template) {
        e.preventDefault();
        let email = $('input[name="email"]').val();
        if (!email) {
            alert(Vatfree.translate("Please fill in an email address"));
        }

        Meteor.call('sendPasswordReset', email, (err, result) => {
            if (err) {
                alert(Vatfree.translate(err.reason || err.message));
            } else {
                template.resetCodeSent.set(true);
            }
        });
    },
    'click .password-reset-do'(e, template) {
        e.preventDefault();
        let email = $('input[name="email"]').val();
        let code = $('input[name="code"]').val();

        let password = $('input[name="password"]').val();
        let password2 = $('input[name="password2"]').val();

        if (!email) {
            alert(Vatfree.translate("Please fill in an email address"));
            return false;
        }
        if (!password || password !== password2) {
            alert(Vatfree.translate("Passwords do not match"));
            return false;
        }

        template.isLoggingIn.set(true);
        Meteor.call('resetPasswordFromCode', email, password, password2, code, (err, result) => {
            if (err) {
                alert(Vatfree.translate(err.reason || err.message));
                template.isLoggingIn.set();
            } else {
                Meteor.loginWithPassword(email, password, (err, result) => {
                    if (err) {
                        alert(Vatfree.translate(err.reason || err.message));
                    } else {
                        template.isRetrievingPassword.set();
                    }
                    template.isLoggingIn.set();
                });
            }
        });
    },
    'click .register'(e, template) {
        e.preventDefault();
        let email = $('input[name="email"]').val();
        let password = $('input[name="password"]').val();
        let password2 = $('input[name="password2"]').val();

        if (!email) {
            alert(Vatfree.translate("Please fill in an email address"));
        }

        if (!password || password !== password2) {
            alert(Vatfree.translate("Passwords do not match"));
        }

        template.isLoggingIn.set(true);
        Meteor.call('registerAsNewUser', email, password, password2, Vatfree.getLanguage(), (err, userId) => {
            if (err) {
                alert(Vatfree.translate(err.reason || err.message));
                template.isLoggingIn.set();
            } else {
                Meteor.loginWithPassword(email, password, (err, result) => {
                    if (err) {
                        alert(Vatfree.translate(err.reason || err.message));
                    } else {
                        template.registering.set();
                    }
                    template.isLoggingIn.set();
                });
            }
        });
    },
    'submit form[name="phone-login"]'(e, template) {
        e.preventDefault();
    },
    'click .send-phone-code'(e, template) {
        e.preventDefault();
        const phoneNumber = '' + template.$('input[name="phone"]').val();
        Meteor.call('sendOtpForLogin', phoneNumber, (err, res) => {
            if (err) {
                alert(Vatfree.translate("Could not send verification code, please check your phone number and try again"));
                return;
            }
            template.phoneCodeSent.set(true);
        });
    },
    'click .login-with-phone'(e, template) {
        e.preventDefault();
        const phone = '' + template.$('input[name="phone"]').val(),
            otp = '' + template.$('input[name="code"]').val();
        template.isLoggingIn.set(true);
        Meteor.loginWithPhone({ phone, otp }, (err, res) => {
            template.isLoggingIn.set(false);
            if (err) {
                alert(Vatfree.translate("Could not log you in, please check your verification code and try again"));
            }
        });
    },
    'click .login-with-password'(e, template) {
        e.preventDefault();
        let email = $('input[name="email"]').val();
        let password = $('input[name="password"]').val();

        if (!email || !password) {
            alert(Vatfree.translate('Please fill in your email address and password'));
            return false;
        }

        template.isLoggingIn.set(true);
        Meteor.loginWithPassword(email, password, (err, result) => {
            if (err) {
                console.log(err);
                alert(Vatfree.translate("Could not log you in, please check your email address and password and try again"));
            } else {
                $('input[name="email"]').val('');
                $('input[name="password"]').val('');
            }
            template.isLoggingIn.set();
        });
    },
    'click .login-with-facebook'(e, template) {
        e.preventDefault();
        template.app.showPreloader(Vatfree.translate('Logging in with Facebook___'));
        Meteor.loginWithFacebook({
            requestPermissions: ['email']
        }, (err) => {
            if (err) {
                console.log(JSON.stringify(err));
                alert(Vatfree.translate(err.reason || err.message));
            } else {
                // successful login!
                console.log('successful login!');
                postLoginCheck();
            }
            template.app.hidePreloader();
        });
    },
    'click .login-with-instagram'(e, template) {
        e.preventDefault();
        template.app.showPreloader(Vatfree.translate('Logging in with Instagram___'));
        Meteor.loginWithInstagram({
            requestPermissions: ['email', 'profile']
        }, (err) => {
            if (err) {
                console.log(JSON.stringify(err));
                alert(Vatfree.translate(err.reason || err.message));
            } else {
                // successful login!
                console.log('successful login!');
                postLoginCheck();
            }
            template.app.hidePreloader();
        });
    },
    'click .login-with-google'(e, template) {
        e.preventDefault();
        template.app.showPreloader(Vatfree.translate('Logging in with Google___'));
        Meteor.loginWithGoogle({
            requestPermissions: ['email', 'profile']
        }, (err, result) => {
            if (err) {
                console.log(JSON.stringify(err));
                alert(Vatfree.translate(err.reason || err.message));
            } else {
                // successful login!
                console.log('successful login!');
                postLoginCheck();
            }
            template.app.hidePreloader();
        });
    },
    'click .login-with-twitter'(e, template) {
        e.preventDefault();
        template.app.showPreloader(Vatfree.translate('Logging in with Twitter___'));
        Meteor.loginWithTwitter({
            requestPermissions: ['email', 'profile']
        }, (err) => {
            if (err) {
                console.log(JSON.stringify(err));
                alert(Vatfree.translate(err.reason || err.message));
            } else {
                // successful login!
                console.log('successful login!');
                postLoginCheck();
            }
            template.app.hidePreloader();
        });
    },
    'click .login-with-linkedin'(e, template) {
        e.preventDefault();
        Meteor.loginWithLinkedin({
            requestPermissions: ['r_basicprofile']
        }, (err) => {
            if (err) {
                console.log(JSON.stringify(err));
                alert(Vatfree.translate(err.reason || err.message));
            } else {
                // successful login!
                console.log('successful login!');
                postLoginCheck();
            }
        });
    },
    'click .login-with-wechat'(e, template) {
        e.preventDefault();
        template.app.showPreloader(Vatfree.translate('Logging in with WeChat___'));
        Meteor.loginWithWechat({}, (err) => {
            if (err) {
                console.log(JSON.stringify(err));
                alert(Vatfree.translate(err.reason || err.message));
            } else {
                // successful login!
                console.log('successful login!');
                postLoginCheck();
            }
            template.app.hidePreloader();
        });
    }
});

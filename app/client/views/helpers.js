openPhotoBrowser = function (selector, selectedFileId, template) {
    let fileIndex = 0;
    let index = 0;
    let photos = {};
    Files.find(selector, {
        sort: {
            createdAt: 1
        }
    }).forEach((photo) => {
        if (photo._id === selectedFileId) {
            fileIndex = index;
        }
        photos[photo._id] = photo.url();
        index++;
    });

    Meteor.call('getDirectUrls', _.keys(photos), (err, urlObjs) => {
        if (err) {
            console.error(err);
        } else {
            _.each(urlObjs, (urlObj) => {
                if (urlObj && urlObj.url) {
                    photos[urlObj.fileId] = urlObj.url;
                }
            });
        }
        const myPhotoBrowserDark = template.app.photoBrowser({
            photos: _.values(photos),
            theme: 'dark',
            initialSlide: fileIndex
        });
        myPhotoBrowserDark.open();
    });
};

import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';
//import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

myApp = {};
$$ = {};
Views = {};
let openView = function(viewId) {
    $$('.view').hide(); $$('.view-' + viewId).show();
};

Template.views.onRendered(function() {
    myApp = new Framework7();
    $$ = Dom7;

    Tracker.afterFlush(() => {
        Views['shops'] = myApp.addView('.view-shops', {});
        openView(Session.get('currentPage') || 'main');
    });

    this.autorun(() => {
        if (Session.get('upgradeApp')) {
            Tracker.afterFlush(() => {
                myApp.popup('.popup-upgrade');
            });
        }
    });
});

Template.views.helpers({
    upgradeApp() {
        return Session.get('upgradeApp');
    }
});

Template.views.events({
    'click .upgrade-image'(e, template) {
        e.preventDefault();
        if (Meteor.isCordova) {
            if (cordova && cordova.platformId === 'android') {
                window.open('https://play.google.com/store/apps/details?id=com.vatfreecomapp', '_system');
            } else {
                window.open('https://itunes.apple.com/us/app/vatfree-com-tax-free-shopping-tax-free-in-the-eu/id1141738494?mt=8', '_system');
            }
        }
    }
});

Template.toolbar.onCreated(function() {
    if (!Session.get('currentPage')) {
        Session.set('currentPage', 'main');
    }
});

Template.toolbar.helpers({
    activePage: function(pageId) {
        return Session.get('currentPage') === pageId ? 'active' : '';
    },
    isActivePage: function(pageId) {
        return Session.get('currentPage') === pageId;
    },
    hasUnreadMessages() {
        return ActivityLogs.find({
            travellerId: Meteor.userId(),
            type: 'email',
            readOn: {
                $exists: false
            }
        }).count() > 0;
    }
});

Template.views.events({
    'click .tab-link'(e) {
        e.preventDefault();
        let id = $(e.currentTarget).attr('id');
        let pageId = id.replace('#', '');
        Session.set('currentPage', pageId);
        openView(pageId);
    }
});

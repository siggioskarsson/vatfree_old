#!/usr/bin/env bash
rm mobile-config.js
ln -s ../app-mobile-config-test.js mobile-config.js
rm -rf .meteor/local/cordova-build/
rm -rf ../Builds/vatfree-test/android
rm -rf ../Builds/vatfree-test/ios
meteor build ../Builds/vatfree-test --server https://vatfree-app-test.herokuapp.com --mobile-settings mobile-settings-test.json

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 ../Builds/vatfree-test/android/project/build/outputs/apk/armv7/release/android-armv7-release-unsigned.apk vatfree-release --keystore ../vatfree-release.keystore
zipalign 4 ../Builds/vatfree-test/android/project/build/outputs/apk/armv7/release/android-armv7-release-unsigned.apk ../Builds/vatfree-test/vatfree-test-armv7.apk

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 ../Builds/vatfree-test/android/project/build/outputs/apk/x86/release/android-x86-release-unsigned.apk vatfree-release --keystore ../vatfree-release.keystore
zipalign 4 ../Builds/vatfree-test/android/project/build/outputs/apk/x86/release/android-x86-release-unsigned.apk ../Builds/vatfree-test/vatfree-test-x86.apk

echo "Open the ios xcode project to continue with that"
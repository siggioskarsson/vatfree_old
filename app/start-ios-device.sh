#!/usr/bin/env bash
meteor npm install --no-optional
export ROOT_URL=https://vatfree-app-test.herokuapp.com
meteor run ios-device --mobile-server https://vatfree-app-test.herokuapp.com --settings settings.json --production

#!/usr/bin/env bash
rm mobile-config.js
ln -s ../app-mobile-config-test.js mobile-config.js

meteor npm install --no-optional
export ROOT_URL=https://vatfree-app-test.herokuapp.com
meteor run android --mobile-server https://vatfree-app-test.herokuapp.com --settings settings.json

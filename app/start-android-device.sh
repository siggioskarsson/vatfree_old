#!/usr/bin/env bash
rm mobile-config.js
ln -s ../app-mobile-config-prod.js mobile-config.js
rm -rf .meteor/local/cordova-build/

meteor npm install --no-optional
export MONGO_URL=mongodb://localhost:6002/meteor
export ROOT_URL=http://192.168.1.98:3000
meteor run android-device --mobile-server http://192.168.1.98:3000 --settings settings.json --port 3000

#!/bin/sh

echo "rdr pass inet proto tcp from any to any port 80 -> 127.0.0.1 port 6011" | sudo pfctl -ef -

# list: sudo pfctl -s nat
# remove: sudo pfctl -F all -f /etc/pf.conf

// This section sets up some basic app metadata,
// the entire section is optional.
//     id: 'com.vatfreecomapp', Android
//     id: 'com.vatfree.vatfree', iOS
App.info({
    version: "2.4.2",
    id: 'com.vatfreecomapp',
    name: 'vatfree.com',
    description: 'Get VAT back from your purchases',
    author: 'vatfree.com',
    email: 'app@vatfree.com',
    website: 'http://vatfree.com'
});

App.icons({
    "app_store": "resources/icons/app_store.png", // 1024x1024
    "iphone_2x": "resources/icons/iphone_2x.png", // 120x120
    "iphone_3x": "resources/icons/iphone_3x.png", // 180x180
    "ipad": "resources/icons/ipad.png", // 76x76
    "ipad_2x": "resources/icons/ipad_2x.png", // 152x152
    "ipad_pro": "resources/icons/ipad_pro.png", // 167x167
    "ios_settings": "resources/icons/ios_settings.png", // 29x29
    "ios_settings_2x": "resources/icons/ios_settings_2x.png", // 58x58
    "ios_settings_3x": "resources/icons/ios_settings_3x.png", // 87x87
    "ios_spotlight": "resources/icons/ios_spotlight.png", // 40x40
    "ios_spotlight_2x": "resources/icons/ios_spotlight_2x.png", // 80x80
    "ios_notification": "resources/icons/ios_notification.png", // 20x20
    "ios_notification_2x": "resources/icons/ios_notification_2x.png", // 40x40
    "ios_notification_3x":"resources/icons/ios_notification_3x.png", // 60x60
    "iphone_legacy": "resources/icons/iphone_legacy.png", // 57x57
    "iphone_legacy_2x": "resources/icons/iphone_legacy_2x.png", // 114x114
    "ipad_spotlight_legacy": "resources/icons/ipad_spotlight_legacy.png", // 50x50
    "ipad_spotlight_legacy_2x": "resources/icons/ipad_spotlight_legacy_2x.png", // 100x100
    "ipad_app_legacy": "resources/icons/ipad_app_legacy.png", // 72x72
    "ipad_app_legacy_2x": "resources/icons/ipad_app_legacy_2x.png", // 144x144
    "android_mdpi": "resources/icons/android_mdpi.png", // 48x48
    "android_hdpi": "resources/icons/android_hdpi.png", // 72x72
    "android_xhdpi": "resources/icons/android_xhdpi.png", // 96x96
    "android_xxhdpi": "resources/icons/android_xxhdpi.png", // 144x144
    "android_xxxhdpi": "resources/icons/android_xxxhdpi.png", // 192x192
    "android_store": "resources/icons/android_store.png" // 512x512
});

App.launchScreens({
    "android_mdpi_portrait": "resources/splashes/android_mdpi_portrait.png", // 320x480
    "android_mdpi_landscape": "resources/splashes/android_mdpi_landscape.png", // 480x320
    "android_hdpi_portrait": "resources/splashes/android_hdpi_portrait.png", // 480x800
    "android_hdpi_landscape": "resources/splashes/android_hdpi_landscape.png", // 800x480
    "android_xhdpi_portrait": "resources/splashes/android_xhdpi_portrait.png", // 720x1280
    "android_xhdpi_landscape": "resources/splashes/android_xhdpi_landscape.png", // 1280x720
    "android_xxhdpi_portrait": "resources/splashes/android_xxhdpi_portrait.png", // 1080x1440
    "android_xxhdpi_landscape": "resources/splashes/android_xxhdpi_landscape.png", // 1440x1080
    "android_featured": "resources/splashes/android_featured.png" // 1024x500
});

App.appendToConfig(`
  <splash src="../../../resources/splashes/Default@3x~universal~anyany.png" />
`);

// Set PhoneGap/Cordova preferences
App.setPreference('Orientation', 'portrait');
//App.setPreference('StatusBarOverlaysWebView', 'true');
App.setPreference('StatusBarStyle', 'lightcontent');
App.setPreference('StatusBarBackgroundColor', '#e6007e');
App.setPreference('BackgroundColor', '#e6007e');
App.setPreference('BackupWebStorage', 'local');
App.setPreference('KeepRunning', 'false');
App.setPreference('fullscreen', 'false');

// Pass preferences for a particular PhoneGap/Cordova plugin
//App.configurePlugin('com.phonegap.plugins.facebookconnect', {
//    APP_ID: '1234567890',
//    API_KEY: 'supersecretapikey'
//});
// Add custom tags for a particular PhoneGap/Cordova plugin
// to the end of generated config.xml.

App.configurePlugin('cordova-plugin-googleplus', {
    'CLIENT_ID': '206501429660-si8j8lgtn02k2csuemummf5mbgqtmb2d.apps.googleusercontent.com',
    'REVERSED_CLIENT_ID': 'com.googleusercontent.apps.206501429660-si8j8lgtn02k2csuemummf5mbgqtmb2d'
});
App.configurePlugin('cordova-plugin-facebook4', {
    APP_ID: '416168535462541',
    APP_NAME: 'Vatfree.com',
    CLIENT_TOKEN: '3aa34b1b9912cd4f31787dac93ae6e5a'
});

// Meteor.settings.wechat.mobileAppId
App.configurePlugin('cordova-plugin-wechat', {
    WECHATAPPID: 'wx3cc8c45322a97931',
});

// Android 8
App.setPreference('android-targetSdkVersion', '26');
// Android 4.4.4
App.setPreference('android-minSdkVersion', '19');
// gradle crap
App.setPreference('ANDROID_SUPPORT_VERSION', '19+');

App.accessRule("*");
App.accessRule("blob:*");

// Universal Links is shown as an example here.
App.appendToConfig(`
    <allow-navigation href="https://www.vatfree.com/*" />
    <allow-navigation href="https://www.facebook.com/vatfree" />
    <allow-navigation href="https://www.twitter.com/vatfree" />
    <access origin="geo:*" launch-external="yes"/>
    <platform name="android">
        <config-file parent="/*" target="./res/values/strings.xml">
            <string name="fb_app_id">416168535462541</string>
            <string name="fb_app_name">vatfree.com APP</string>
        </config-file>
    </platform>
    <platform name="ios">
        <plugin name="cordova-plugin-media-capture" source="npm" spec="*">
            <variable name="CAMERA_USAGE_DESCRIPTION" value="App would like to access the camera." />
            <variable name="MICROPHONE_USAGE_DESCRIPTION" value="App would like to access the microphone." />
            <variable name="PHOTOLIBRARY_USAGE_DESCRIPTION" value="App would like to access the library." />
        </plugin>
        <config-file parent="NSPhotoLibraryUsageDescription" platform="ios" target="*-Info.plist">
            <string>This app needs access to your Photo Library to allow you to uploade photos of purchase receipts that you have saved on your device.</string>
        </config-file>
        <config-file parent="NSCameraUsageDescription" platform="ios" target="*-Info.plist">
            <string>This app uses the camera to capture and upload photos of purchase receipts you submit to Vatfree.com.</string>
        </config-file>
        <config-file parent="NSLocationWhenInUseUsageDescription" platform="ios" target="*-Info.plist">
            <string>This app uses your location to show you the most relevant results when searching for shops for a tax refund.</string>
        </config-file>
        <config-file parent="NSLocationAlwaysUsageDescription" platform="ios" target="*-Info.plist">
            <string>This app uses your location to show you the most relevant results when searching for shops for a tax refund.</string>
        </config-file>
    </platform>
`);

#!/usr/bin/env bash
rm mobile-config.js
ln -s ../mdwapp-mobile-config-test.js mobile-config.js
rm -rf .meteor/local/cordova-build/

export ROOT_URL=https://vatfree-mdw-app-test.herokuapp.com
meteor run ios-device --mobile-server=https://vatfree-mdw-app-test.herokuapp.com --settings settings.json

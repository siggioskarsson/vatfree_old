Template.shopItem.helpers({
    geoDistance() {
        let geo = Geolocation.currentLocation();
        if (geo && geo.coords) {
            return Math.round(geoDistance(geo.coords['latitude'], geo.coords['longitude'], this.geo.coordinates[0], this.geo.coordinates[1])).toString() + 'km';
        }
    },
    getPartnershipStatusLogo() {
        switch (this.partnershipStatus) {
            case 'pledger':
                return 'VFCircleShadeBlue.svg';
            case 'partner':
                return 'VFCircleShade.svg';
            default:
                return 'CircleShadeGreyNonRefund.svg';
        }
    }
});

let geoDistance = function(lat1, lon1, lat2, lon2) {
    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p))/2;

    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
};

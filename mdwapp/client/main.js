import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';
//import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

$$ = {};
Views = {};
let openView = function(viewId) {
    $$('.view').hide(); $$('.view-' + viewId).show();
};

Template.views.onRendered(function helloOnCreated() {
    $$ = Dom7;

    Tracker.afterFlush(() => {
        openView('main');
    });
});

Template.views.helpers({
    networkProblem() {
        let status = Meteor.status().status;
        return status !== 'connecting' && status !== "connected";
    },
    keyboardShown() {
        return keyboardShown.get();
    }
});

Template.views.events({
});

Template.toolbar.onCreated(function helloOnCreated() {
    if (!Session.get('currentPage')) {
        Session.set('currentPage', 'main');
    }
});

Template.toolbar.helpers({
    activePage: function(pageId) {
        return Session.get('currentPage') === pageId ? 'active' : '';
    }
});

Template.toolbar.events({
    'click .tab-link'(e) {
        e.preventDefault();
        let id = $(e.currentTarget).attr('id');
        let pageId = id.replace('#', '');
        Session.set('currentPage', pageId);
        openView(pageId);
    }
});

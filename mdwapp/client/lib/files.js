Vatfree.saveFile = function(file, callback) {
    if (_.isFunction(callback)) {
        return _saveFile(file, callback);
    } else {
        return saveFile(file);
    }
};

Vatfree.updateFile = function(fileName, fileContent, callback) {
    _removeFile(fileName, function(err, result) {
        if (err) {
            alert(err.message || err.reason);
            console.error(err);
        } else {
            getFileEntry(fileName, function(err, fileEntry) {
                writeFile(fileEntry, fileContent, function (err) {
                    console.log('writeFile(fileEntry return', fileName);
                    callback(err, fileName);
                });
            });
        }
    });
};

Vatfree.readFile = function(file, callback) {
    if (_.isFunction(callback)) {
        return _readFile(file, callback);
    } else {
        return readFile(file);
    }
};

Vatfree.removeFile = function(file, callback) {
    if (_.isFunction(callback)) {
        return _removeFile(file, callback);
    } else {
        return removeFile(file);
    }
};

let getFileEntry = function (fileName, callback) {
    if (typeof cordova === 'undefined') return;

    console.log('getFileentry', fileName, cordova.file.dataDirectory);
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
        console.log('File system open: ' + fs.name);
        // cordova.file.dataDirectory
        fs.root.getFile(fileName, {create: true, exclusive: false}, function (fileEntry) {
            callback(null, fileEntry);
        }, function (err) {
            console.log("Failed file write:", err);
            callback(err);
        });
    }, function (err) {
        console.log("Failed file write:", err);
        callback(err);
    });
};

var _saveFile = function (file, callback) {
    var fileName = 'vatfree_file_' + moment().format('x') + '_' + Random.id() + '.b64';
    getFileEntry(fileName, function(err, fileEntry) {
        writeFile(fileEntry, file.fileContent, function (err) {
            console.log('writeFile(fileEntry return', fileName);
            callback(err, fileName);
        });
    });
};
var saveFile = Meteor.wrapAsync(_saveFile);

var writeFile = function(fileEntry, fileContent, callback) {
    // Create a FileWriter object for our FileEntry (log.txt).
    fileEntry.createWriter(function (fileWriter) {
        fileWriter.onwrite = function() {
            callback(null);
        };

        fileWriter.onerror = function (e) {
            console.log("Failed file write: " + e.toString());
            callback(e);
        };

        var dataObj = new Blob([fileContent], { type: 'text/plain' });
        fileWriter.write(dataObj);
    });
};

var _readFile = function(fileName, callback) {
    getFileEntry(fileName, function(err, fileEntry) {
        if (err) {
            callback(err);
        } else {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function() {
                    callback(null, this.result);
                };
                reader.readAsText(file);
            }, function(err) {
                console.log("Failed file write: " + err.toString());
                callback(err);
            });
        }
    });
};
var readFile = Meteor.wrapAsync(_readFile);

var _removeFile = function(fileName, callback) {
    getFileEntry(fileName, function(err, fileEntry) {
        if (err) {
            callback(err);
        } else {
            fileEntry.remove(function (result) {
                callback(null, result);
            }, function(err) {
                console.log("Failed file remove: " + err.toString());
                callback(err);
            });
        }
    });
};
var removeFile = Meteor.wrapAsync(_removeFile);

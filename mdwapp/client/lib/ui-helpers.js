import moment from 'moment';

UI.registerHelper('getUserName', function(userId) {
    userId = userId || Meteor.userId();
    let user = Meteor.users.findOne({_id: userId});
    if (user && user.profile) {
        return user.profile.name || user.profile.email;
    }
});
UI.registerHelper('isSelected', function(attribute, value) {
    if (_.isArray(attribute)) {
        return _.contains(attribute, value);
    } else {
        return attribute === value;
    }
});
UI.registerHelper('isActive', function(attribute, value) {
    return attribute === value ? 'active' : '';
});
UI.registerHelper('isStatus', function(status, fieldValue) {
    let template = Template.instance();
    fieldValue = fieldValue || template.data.status;
    return fieldValue === status;
});
UI.registerHelper('toLowerCase', function(string) {
    return (string || "").toString().toLowerCase();
});
UI.registerHelper('formatDate', Vatfree.dates.formatDate);
UI.registerHelper('formatDateShort', Vatfree.dates.formatDateShort);
UI.registerHelper('formatDateTime', Vatfree.dates.formatDateTime);
UI.registerHelper('formatDateHuman', function (date) {
    if (!date) {
        date = this.toString();
    }
    return moment.duration(moment(date).diff(moment())).humanize(true);
});

UI.registerHelper('formatCurrency', function(value, decimals, currencyId) {
    currencyId = currencyId || this.currencyId;
    let currency = Currencies.findOne({_id: currencyId}) || {};
    return Vatfree.numbers.formatCurrency(value, decimals, currency.symbol);
});
UI.registerHelper('formatAmount', Vatfree.numbers.formatAmount);

UI.registerHelper('getActivityIcon', function() {
    if (!this.activity || !this.activity.target || !this.activity.target.target) return "";
    let type = this.activity.target.target;
    switch(type) {
        case 'receipts':
            return 'fa fa-file-text-o';
        case 'shops':
            return 'fa fa-cart-plus';
        case 'users':
            return 'fa fa-user';
        case 'countries':
            return 'fa fa-globe';
        case 'files':
            return 'fa fa-file-o';
    }
});

UI.registerHelper('getActivityDescription', function(hideUser) {
    var asObj = new Vatfree.ActivityStreamItem(this.activity);
    return asObj.getText(hideUser);
});

UI.registerHelper('getActivityDetails', function(hideUser) {
    var asObj = new Vatfree.ActivityStreamItem(this.activity);
    return asObj.getDetails();
});

UI.registerHelper('getStatsCount', function(id, showEmpty) {
    let stats = Stats.findOne({_id: id});
    if (stats && stats.count > 0) {
        return stats.count;
    } else if (showEmpty) {
        return 0;
    }
});

UI.registerHelper('isLanguageSelected', function(language) {
    return this.code === language;
});

UI.registerHelper('getStandardServiceFee', function() {
    return Vatfree.receipts.standardServiceFee;
});
UI.registerHelper('getMinServiceFee', function() {
    return Vatfree.receipts.standardMinFee;
});
UI.registerHelper('getMaxServiceFee', function() {
    return Vatfree.receipts.standardMaxFee;
});

UI.registerHelper('getLanguage', function() {
    return Vatfree.getLanguage();
});

UI.registerHelper('getTravellerLanguages', function() {
    return Vatfree.languages.getTravellerLanguages();
});

UI.registerHelper('getShopLanguages', function() {
    return Vatfree.languages.getShopLanguages();
});

UI.registerHelper('isDefault', function(key, value) {
    return Vatfree.defaults[key] === value;
});

UI.registerHelper('getDefault', function(key) {
    return Vatfree.defaults[key];
});

UI.registerHelper('importsEnabled', function() {
    return Meteor.settings && Meteor.settings.public && Meteor.settings.public.importsEnabled === true;
});

UI.registerHelper('getCompetitors', function() {
    return Vatfree.competitors;
});
UI.registerHelper('getCustoms', function() {
    return Vatfree.customs;
});

UI.registerHelper('fileCopiesUploaded', function(file) {
    return file && file.copies && file.copies['vatfree'] && (file.copies['vatfree-thumbs'] || !file.isImage());
});

UI.registerHelper('getCountryName', function(countryId) {
    if (!countryId) countryId = this.countryId;

    let country = Countries.findOne({_id: countryId});
    if (country) {
        return country.name;
    }

    return "";
});
UI.registerHelper('getCountryCode', function(countryId) {
    if (!countryId) countryId = this.countryId;

    let country = Countries.findOne({_id: countryId});
    if (country) {
        return country.code;
    }

    return "";
});

UI.registerHelper('getWebText', function(code) {
    let webText = WebTexts.findOne({code: code});
    if (webText && webText.text) {
        let language = TAPi18n.getLanguage() || "en";
        return webText.text[language] || "";
    } else {
        return "";
    }
});

UI.registerHelper('isInVatfreeRole', function(roles) {
    if (!_.isArray(roles)) {
        roles = roles.split(',');
    }

    return Vatfree.userIsInRole(Meteor.userId(), roles);
});

UI.registerHelper('createdByMe', function() {
    return this.createdBy === Meteor.userId();
});

UI.registerHelper('createdByVattie', function() {
    let vatties = _.map(Meteor.users.find().fetch(), (user) => { return user._id; });
    return _.contains(vatties, this.createdBy);
});

UI.registerHelper('getVatties', function() {
    return Meteor.users.find({}, {sort: {'profile.name': 1}}).fetch();
});

UI.registerHelper('getUploadQueue', function() {
    return ReceiptUploadFiles.find().count();
});

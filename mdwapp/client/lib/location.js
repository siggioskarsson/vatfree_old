Meteor.startup(() => {
    Session.set('location', {latitude: 0, longitude: 0});
    if (!(navigator && navigator.geolocation && navigator.geolocation.watchPosition)) {
        return false;
    }

    const options = {
        enableHighAccuracy: false,
        timeout: 5000,
        maximumAge: 0
    };
    let watchId;
    const startWatching = function() {
        if (!watchId) {
            watchId = navigator.geolocation.watchPosition(function(pos) {
                const crd = pos.coords;
                Session.set('location', {latitude: crd.latitude, longitude: crd.longitude});
            }, function(err) {
                console.error('location error', err);
            }, options);
        }
    };
    const stopWatching = function() {
        if (watchId) {
            navigator.geolocation.clearWatch(watchId);
            watchId = false;
        }
    };

    startWatching();
    document.addEventListener('pause', function () {
        stopWatching();
    }, false);
    document.addEventListener('resume', function () {
        startWatching();
    }, false);
});

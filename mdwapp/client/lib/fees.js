Vatfree.receipts = {};
Vatfree.receipts.standardServiceFee = 20;
Vatfree.receipts.standardMinFee = 500;
Vatfree.receipts.standardMaxFee = 15000;
Meteor.startup(() => {
    Tracker.autorun(() => {
        if (Meteor.user()) {
            Meteor.call('get-defaults', (err, defaults) => {
                if (defaults) {
                    Vatfree.receipts.standardServiceFee = defaults.standardServiceFee || 20;
                    Vatfree.receipts.standardMinFee = defaults.standardMinFee || 500;
                    Vatfree.receipts.standardMaxFee = defaults.standardMaxFee || 15000;
                }
            });
        }
    });
});

// If we need to use custom DOM library, let's save it to $$ variable:
var $$ = Dom7;
f7App = null;
keyboardShown = new ReactiveVar();

Meteor.startup(() => {
    f7App = new Framework7({
        modalTitle: 'vatfree.com',
        swipePanel: 'right'
    });

    if (Meteor.isCordova && !_.isUndefined(Keyboard)) {
        Keyboard.shrinkView(false);
        Keyboard.disableScrollingInShrinkView(false);
        Keyboard.hideFormAccessoryBar(false);
        Keyboard.automaticScrollToTopOnHiding = false;

        Keyboard.onshowing = function () {
            keyboardShown.set(true);
        };

        Keyboard.onhiding = function () {
            keyboardShown.set(false);
        };
    }

    Meteor.setTimeout(function () {
        if (Meteor.isCordova) {
            StatusBar.hide();
            StatusBar.show();

            if (cordova.platformId == 'android') {
                StatusBar.backgroundColorByHexString("#e6007e");
            }
        }
    }, 1000);

    if (Meteor.isCordova && (!!navigator.userAgent.match(/iPad/i) || !!navigator.userAgent.match(/iPhone/i) || !!navigator.userAgent.match(/iPod/i))) {
        // Hack because on first builds/runs sometimes F7 doesn't add this class itself, doesn't detect fullscreen properly
        $('html').addClass('with-statusbar-overlay')
    }

    Session.set('location', {latitude: 0, longitude: 0});
    let success = function(pos) {
        let crd = pos.coords;
        Session.set('location', {latitude: crd.latitude, longitude: crd.longitude});
    };

    let error = function(err) {
        console.log('ERROR(' + err.code + '): ' + err.message);
    };

    let options = {
        enableHighAccuracy: false,
        timeout: 5000,
        maximumAge: 0
    };
    if (navigator && navigator.geolocation && navigator.geolocation.watchPosition) {
        navigator.geolocation.watchPosition(success, error, options);
    }
});

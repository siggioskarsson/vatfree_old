import uriParse from 'url-parse';

Template.view_popr.onCreated(function() {
    this.context = new ReactiveVar();
});

Template.view_popr.onRendered(function() {
    this.autorun(() => {
        let context = this.context.get();
        if (context) {
            if (context.target === 'receipts') {
                this.subscribe('receipts_item', context.itemId);
            }
            this.subscribe('countries');
        }
    });

    this.autorun(() => {
        let user = Meteor.user() || {};
        let uploadContext = user.profile ? user.profile.qrUploadContext : false;
        this.context.set(uploadContext);
    });
});

Template.view_popr.helpers({
    currentContext() {
        return Template.instance().context.get();
    },
    getTravellerDoc() {
        return Travellers.findOne({
            _id: this.userId
        });
    },
    getReceipt() {
        let context = Template.instance().context.get();
        if (context && context.target === 'receipts') {
            return Receipts.findOne({
                _id: context.itemId
            });
        }
    },
    getShop() {
        return Shops.findOne({_id: this.shopId});
    }
});

let handleQrCodeScan = function (result, template) {
    console.log("We got a barcode\n" +
        "Result: " + result.text + "\n" +
        "Format: " + result.format + "\n" +
        "Cancelled: " + result.cancelled);
    let uri = uriParse(result.text, true);
    if (uri && uri.protocol === 'https:' && uri.hostname === 'app.popr.io' && uri.pathname) {
        let pathParts = uri.pathname.split('/');
        let poprId = pathParts[1] || "";
        let poprHash = pathParts[2] || "";
        Meteor.call('get-popr-info', poprId, poprHash, (err, poprDoc) => {
            if (err) {
                f7App.alert(err.reason);
            } else {
                let context = template.context.get();
                if (context.receiptId === "NEW") {
                    console.log("new");
                    let receiptData = {};
                    receiptData.poprId = poprId;
                    receiptData.poprSecret = poprHash;
                    receiptData.poprShopId = poprDoc.retailerId + '/' + poprDoc.shopId;
                    receiptData.purchaseDate = moment.unix(poprDoc.time).toDate();
                    receiptData.amount = Number(poprDoc.totalValue);
                    if (poprDoc.totalVAT) {
                        receiptData.totalVat = Number(poprDoc.totalVAT);
                    }

                    Meteor.call('pre-fill-receipt-add', receiptData, (err, result) => {
                        if (err) {
                            f7App.alert(err.reason || err.message);
                        }
                    });
                } else {
                    if (confirm("Claim receipt in POPr?!")) {
                        Meteor.call('claim-popr-receipt', context.receiptId, poprId, poprHash, (err, result) => {
                            if (err) {
                                f7App.alert(err.reason || err.message);
                            }
                        });
                    }
                }
            }
        });
    } else {
        f7App.alert('Unknown URL/QRCode scanned');
    }
};

Template.view_popr.events({
    'click .reset'(e, template) {
        e.preventDefault();
        template.context.set();
    },
    'click .scan-qr-code'(e, template) {
        e.preventDefault();

        if (Meteor.isCordova) {
            cordova.plugins.barcodeScanner.scan(
                function (result) {
                    console.log("We got a barcode\n" +
                        "Result: " + result.text + "\n" +
                        "Format: " + result.format + "\n" +
                        "Cancelled: " + result.cancelled);
                    let uri = uriParse(result.text, true);
                    if (uri && uri.protocol === 'vatfree:' && uri.hostname === 'image-upload' && uri.query && uri.query.context) {
                        template.context.set(JSON.parse(uri.query.context));
                    } else {
                        f7App.alert('Failed parsing QR code. Is this a valid vatfree QR code?');
                    }
                },
                function (error) {
                    f7App.alert("Scanning failed: " + error);
                },
                {
                    preferFrontCamera : false,
                    showFlipCameraButton : true,
                    showTorchButton : true,
                    torchOn: false,
                    prompt : "Place a barcode inside the scan area", // Android
                    resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                    formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
                    orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
                    disableAnimations : true, // iOS
                    disableSuccessBeep: false // iOS
                }
            );
        } else {
            template.context.set({
                itemId: 'd6csdcGkKEQzMAMQP',
                target: 'travellers'
            });
        }
    },
    'click .scan-popr-qr-code'(e, template) {
        e.preventDefault();
        if (Meteor.isCordova) {
            cordova.plugins.barcodeScanner.scan(
                function (result) {
                    handleQrCodeScan(result, template);
                },
                function (error) {
                    f7App.alert("Scanning failed: " + error);
                    template.qrCodeOnReceiptAsked.set(true);
                },
                {
                    preferFrontCamera : false,
                    showFlipCameraButton : true,
                    showTorchButton : true,
                    torchOn: false,
                    prompt : "Place a barcode inside the scan area", // Android
                    resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                    formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
                    orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
                    disableAnimations : true, // iOS
                    disableSuccessBeep: false // iOS
                }
            );
        } else {
            //handleQrCodeScan({text: "https://app.popr.io/.../..."}, template);
            var qr = prompt("QR code");
            handleQrCodeScan({
                text: 'https://app.popr.io/' + qr,
                format: 'qr'
            }, template);
        }
    }
});

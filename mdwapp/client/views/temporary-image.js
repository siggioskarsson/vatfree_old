Template.getTemporaryImage.onCreated(function() {
    this.fileContent = new ReactiveVar();
});

Template.getTemporaryImage.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        Vatfree.readFile(data.filePath, (err, fileContent) => {
            if (err) {
                alert(err.message || err.reason);
                console.error(err);
            } else {
                this.fileContent.set(fileContent);
            }
        });
    });
});

Template.getTemporaryImage.helpers({
    getFileContent() {
        return Template.instance().fileContent.get();
    }
});

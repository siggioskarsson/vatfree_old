Template.view_register_scan_receipt.onCreated(function() {
    this.files = new ReactiveArray();
});

Template.view_register_scan_receipt.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        if (data.receipts && data.receiptIndex) {
            if (_.has(data.receipts, data.receiptIndex.get())) {
                let receipt = data.receipts[data.receiptIndex.get()];
                this.files.clear();
                _.each(receipt.files, (file) => {
                    this.files.push(file);
                });
            } else {
                this.files.clear();
                if (this.$('form').length > 0) {
                    this.$('form')[0].reset();
                }
            }
        }
    });
});

Template.view_register_scan_receipt.helpers({
    receipt() {
        const receiptIndex = this.receiptIndex ? this.receiptIndex.get() : null;
        const receipts = this.receipts ? this.receipts.array() : [];
        if (_.has(receipts, receiptIndex)) {
            return receipts[receiptIndex];
        }
        // returning empty receipt
        return {
            shopId: false,
            amount: '',
            totalVat: '',
            purchaseDate: '',
            customsDate: moment().format('YYYY-MM-DD')
        }
    },
    getFiles() {
        return Template.instance().files.list();
    },
    purchaseDateFormatted() {
        if (this.purchaseDate) {
            return moment(this.purchaseDate).format('YYYY-MM-DD');
        }
    },
    customsDateFormatted() {
        if (this.customsDate) {
            return moment(this.customsDate).format('YYYY-MM-DD');
        }
    },
    addAppView() {
        this.appView = Template.instance().data.appView;
        return this;
    },
    formatAmount: Vatfree.numbers.formatAmount
});

Template.view_register_scan_receipt.events({
    'click .cancel-receipt'(e, template) {
        e.preventDefault();
        template.$('form')[0].reset();
        $('.back').trigger('click');
    },
    'click #scan-receipt li'(e) {
        let input = $(e.currentTarget).find('input');
        if (input && input.length) {
            input.focus();
        }
        let textarea = $(e.currentTarget).find('textarea');
        if (textarea && textarea.length) {
            textarea.focus();
        }
    },
    'change input[name="receiptFile"]'(e, template) {
        e.preventDefault();
        var fileInput = e.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(fileLoadEvent) {
            let file = {
                file: {
                    name: fileInput.name,
                    type: fileInput.type,
                    size: fileInput.size
                },
                fileContent: reader.result
            };

            Vatfree.saveFile(file, (err, filePath) => {
                if (!filePath) {
                    throw new Meteor.Error(500, 'Could not write file');
                }
                file.filePath = filePath;
                delete file.fileContent;

                template.files.push(file);
                template.$('input[name="receiptFile"]').val('');
            });
        };
        reader.readAsDataURL(fileInput);
    },
    'click .add-file-button'(e, template) {
        if (window.EdgeCamera) {
            e.preventDefault();
            e.stopPropagation();

            let successCallback = function(base64Data) {
                Vatfree.imageHelpers.resetOrientation(base64Data, (err, base64Uri) => {
                    Vatfree.imageHelpers.resizeDataUriImage(base64Uri, 4032, function(err, base64Uri) {
                        // this is the standard format, so we set that up first for the file write functions
                        let file = {
                            file: {
                                name: 'vatfree-scan-' + moment().format('YYYY-MM-DD_HH-mm-ss') + '.jpg',
                                type: 'image/jpeg',
                                size: Math.round((base64Uri.length) * 3 / 4)
                            },
                            fileContent: base64Uri
                        };

                        Vatfree.saveFile(file, (err, filePath) => {
                            if (!filePath) {
                                throw new Meteor.Error(500, 'Could not write file');
                            }
                            file.filePath = filePath;
                            delete file.fileContent;

                            template.files.push(file);
                        });
                    });
                });
            };
            let errorCallback = function() {
                console.log(this, arguments);
                alert('Error taking picture')
            };
            let options = {

            };
            window.EdgeCamera.takePicture(successCallback, errorCallback, options);
        }
    },
    'click .add-photo-button'(e, template) {
        if (navigator.camera) {
            e.preventDefault();
            e.stopPropagation();

            let successCallback = function(base64Data) {
                Vatfree.imageHelpers.resetOrientation(base64Data, (err, base64Uri) => {
                    Vatfree.imageHelpers.resizeDataUriImage(base64Uri, 4032, function(err, base64Uri) {
                        // this is the standard format, so we set that up first for the file write functions
                        let file = {
                            file: {
                                name: 'vatfree-photo-' + moment().format('YYYY-MM-DD_HH-mm-ss') + '.jpg',
                                type: 'image/jpeg',
                                size: Math.round((base64Uri.length)*3/4)
                            },
                            fileContent: base64Uri
                        };

                        Vatfree.saveFile(file, (err, filePath) => {
                            if (!filePath) {
                                throw new Meteor.Error(500, 'Could not write file');
                            }
                            file.filePath = filePath;
                            delete file.fileContent;

                            template.files.push(file);
                        });
                    });
                });
            };
            let errorCallback = function() {
                console.log(this, arguments);
                alert('Error taking picture')
            };
            let options = {
                quality: 90,
                destinationType: 0 // DATA_URL
            };
            navigator.camera.getPicture(successCallback, errorCallback, options);
        }
    },
    'click .file-remove'(e, template) {
        e.preventDefault();
        let self = this;
        let buttons1 = [
            {
                text: 'Delete file',
                color: 'red',
                onClick: function () {
                    if (self.filePath) {
                        Vatfree.removeFile(self.filePath, function(err, result) {
                            if (err) {
                                alert(err.message || err.reason);
                                console.error(err);
                            } else {
                                template.files.remove(self);
                            }
                        });
                    } else {
                        template.files.remove(self);
                    }
                    return true;
                }
            }
        ];
        let buttons2 = [
            {
                text: 'Cancel'
            }
        ];
        let groups = [buttons1, buttons2];
        f7App.actions(groups);
    },
    'click .file-thumb-container'(e, template) {
        e.preventDefault();
        Session.set('view_photo', this);
        template.data.appView.get().router.load({pageName: 'view_photo'});
    },
    'click .submit-receipt'(e, template) {
        let formData = f7App.formToData('#scan-receipt');
        formData.files = template.files.array();
        if (!formData.files.length) {
            f7App.alert("No receipt photos uploaded yet");
            return;
        }

        if (formData.purchaseDate) {
            formData.purchaseDate = moment(formData.purchaseDate, 'YYYY-MM-DD').toDate();
        }
        if (formData.customsDate) {
            formData.customsDate = moment(formData.customsDate, 'YYYY-MM-DD').toDate();
        }
        formData.amount = Math.round(Vatfree.numbers.unformatAmount(formData.amount) * 100);
        formData.totalVat = Math.round(Vatfree.numbers.unformatAmount(formData.totalVat) * 100);

        if (formData.nonStandardVat) {
            formData.nonStandardVat = true;
        }

        let receiptIndex = template.data.receiptIndex.get();
        if (_.has(template.data.receipts, receiptIndex)) {
            // replace receipt
            let receipts = _.clone(template.data.receipts);
            template.data.receipts.clear();
            _.each(receipts, (receipt, index) => {
                if (index === receiptIndex) {
                    template.data.receipts.push(formData);
                } else {
                    template.data.receipts.push(receipt);
                }
            });
        } else {
            template.data.receipts.push(formData);
        }
        template.data.appView.get().router.back();

        // Reset the data for this form, bit hacky but works
        template.$('form')[0].reset();
        template.$('input[name="customsDate"]').val(moment().format('YYYY-MM-DD'));

        template.files.clear();
        template.data.receiptIndex.set();
    }
});

Template.view_register_scan_receipt_shop_input.onCreated(function() {
    this.shopId = new ReactiveVar();
    this.shopName = new ReactiveVar();
    this.autocomplete = false;
});

Template.view_register_scan_receipt_shop_input.onRendered(function() {
    this.autorun(() => {
        let data = Template.currentData();
        if (data && data.shopId) {
            this.shopId.set(data.shopId);
        } else {
            this.shopId.set();
            this.shopName.set();
        }
    });

    this.autorun(() => {
        let shopId = this.shopId.get();
        this.subscribe('shops_item', shopId || "");
    });

    const template = this;
    this.autorun(() => {
        let view = this.data.appView.get();
        let shopId = this.shopId.get();
        if (view) {
            if (template.autocomplete) {
                template.autocomplete.destroy();
            }
            template.autocomplete = f7App.autocomplete({
                input: template.$('#scanReceiptShopName'),
                opener: template.$('#scanReceiptShopName'),
                view: this.data.appView.get(),
                openIn: 'page',
                preloader: true,
                valueProperty: 'text',
                textProperty: 'text',
                expandInput: true,
                backOnSelect: true,
                multiple: false,
                source: function (autocomplete, query, render) {
                    if (query.length === 0) {
                        render([]);
                        return;
                    }
                    // Show Preloader
                    autocomplete.showPreloader();
                    Meteor.call('search-shops', query || "", 20, 0, true, (err, res) => {
                        autocomplete.hidePreloader();
                        render(res);
                    });
                },
                onChange(autocomplete, value) {
                    if (value[0] && value[0].id) {
                        template.shopId.set(value[0].id);
                        template.shopName.set(value[0].text);
                        template.$('#scanReceiptShopId').val(value[0].id).trigger('change');
                    }
                }
            });
        }
    });
});

Template.view_register_scan_receipt_shop_input.helpers({
    shopId() {
        return Template.instance().shopId.get();
    },
    shopName() {
        return Template.instance().shopName.get();
    },
    getPartnershipStatusLogo() {
        let template = Template.instance();
        let shop = Shops.findOne({
            _id: template.shopId.get()
        }) || {};
        switch (shop.partnershipStatus) {
            case 'pledger':
                return 'VFCircleShadeBlue.svg';
            case 'partner':
                return 'VFCircleShade.svg';
            case 'new':
            case 'uncooperative':
                return 'CircleShadeGreyNonRefund.svg';
            default:
                return false;
        }

    }
});

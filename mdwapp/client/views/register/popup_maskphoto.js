import { fabric } from 'fabric';

Template.view_register_popup_maskphoto.onCreated(function () {
    this.fabricCanvas = false;
});

let initImage = function (index, screenWidth, imageData) {
    let i = new Image();
    i.onload = () => {
        let width = i.width;
        let height = i.height;
        let $canvas = $('#passport');
        console.log('image dimensions', index, width, height);

        $canvas.attr('width', screenWidth);
        $canvas.attr('height', Math.round(screenWidth * (height / width)));

        Tracker.afterFlush(() => {
            if (this.fabricCanvas) {
                this.fabricCanvas.clear();
                this.fabricCanvas.dispose();
                this.fabricCanvas = false;
            }

            this.fabricCanvas = new fabric.Canvas('passport');
            this.fabricCanvas.selection = true;

            fabric.Object.prototype.set({
                transparentCorners: false,
                cornerColor: 'rgba(102,153,255,0.5)',
                cornerSize: 12,
                padding: 5
            });

            this.fabricCanvas.isDrawingMode = true;
            this.fabricCanvas.freeDrawingBrush = new fabric['PencilBrush'](this.fabricCanvas);
            this.fabricCanvas.freeDrawingBrush.color = 'Black';
            this.fabricCanvas.freeDrawingBrush.width = 6 * window.devicePixelRatio;

            fabric.Image.fromURL(imageData, (img) => {
                this.fabricCanvas.insertAt(img.set({
                    left: 0,
                    top: 0,
                    angle: 0,
                    width: Number($canvas.attr('width')),
                    height: Number($canvas.attr('height')),
                    lockMovementX: true,
                    lockMovementY: true,
                    lockScaling: true,
                    LockRotation: true,
                    selectable: false,
                    isDrawingMode: true
                }).scale(1 / window.devicePixelRatio));
            });
        });
    };
    i.src = imageData;
};
Template.view_register_popup_maskphoto.onRendered(function () {
    this.autorun(() => {
        let data = Template.currentData();
        let index = data.maskPhoto.get();
        let screenWidth = $('.content-block').width();

        if (data.passportPhotos && data.passportPhotos[index] && data.passportPhotos[index].filePath) {
            Vatfree.readFile(data.passportPhotos[index].filePath, (err, fileContent) => {
                if (err) {
                    alert(err.message || err.reason);
                    console.error(err);
                } else {
                    initImage.call(this, index, screenWidth, fileContent);
                }
            });
        } else if (data.passportPhotos && data.passportPhotos[index] && data.passportPhotos[index].fileContent) {
            let imageData = data.passportPhotos[index].fileContent;
            initImage.call(this, index, screenWidth, imageData);
        }
    });
});

Template.view_register_popup_maskphoto.helpers({
});

Template.view_register_popup_maskphoto.events({
    'click .cancel'(e, template) {
        e.preventDefault();
        template.data.maskPhoto.set(false);
    },
    'click .save-popup-image'(e, template) {
        e.preventDefault();
        let dataUri = template.fabricCanvas.toDataURL({
            format: 'png'
        });

        let index = template.data.maskPhoto.get();
        if (dataUri && index !== false && template.data.passportPhotos[index]) {
            let photo = _.clone(template.data.passportPhotos[index]);
            if (photo.filePath) {
                Vatfree.updateFile(photo.filePath, dataUri, function(err, result) {
                    if (err) {
                        alert(err.message || err.reason);
                        console.error(err);
                    } else {
                        photo.updatedAt = new Date();
                        template.data.maskPhoto.set(false);
                        template.data.passportPhotos.splice(index, 1);
                        template.data.passportPhotos.splice(index, 0, photo);
                    }
                });
            } else {
                photo.fileContent = dataUri;
                template.data.passportPhotos.splice(index, 1);
                template.data.passportPhotos.splice(index, 0, photo);
                template.data.maskPhoto.set(false);
            }
        } else {
            // error ...
        }
    }
});

/**
 * Detecting vertical squash in loaded image.
 * Fixes a bug which squash image vertically while drawing into canvas for some images.
 * This is a bug in iOS6 devices. This function from https://github.com/stomita/ios-imagefile-megapixel
 *
 * http://stackoverflow.com/questions/11929099/html5-canvas-drawimage-ratio-bug-ios
 */
function detectVerticalSquash(img) {
    let iw = img.naturalWidth, ih = img.naturalHeight;
    let canvas = document.createElement('canvas');
    canvas.width = 1;
    canvas.height = ih;
    let ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);
    let data = ctx.getImageData(0, 0, 1, ih).data;
    // search image edge pixel position in case it is squashed vertically.
    let sy = 0;
    let ey = ih;
    let py = ih;
    while (py > sy) {
        let alpha = data[(py - 1) * 4 + 3];
        if (alpha === 0) {
            ey = py;
        } else {
            sy = py;
        }
        py = (ey + sy) >> 1;
    }
    let ratio = (py / ih);
    return (ratio === 0) ? 1 : ratio;
}

/**
 * A replacement for context.drawImage
 * (args are for source and destination).
 */
function drawImageIOSFix(ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) {
    let vertSquashRatio = detectVerticalSquash(img);
    // Works only if whole image is displayed:
    // ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
    // The following works correct also when only a part of the image is displayed:
    ctx.drawImage(img, sx * vertSquashRatio, sy * vertSquashRatio,
        sw * vertSquashRatio, sh * vertSquashRatio,
        dx, dy, dw, dh);
}

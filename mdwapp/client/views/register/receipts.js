Template.view_register_receipts.onRendered(function() {
    f7App.onPageAfterAnimation('receipts', (page) => {
        // if no receipts, forward directly to scan
        const receipts = this.data.receipts.array();
        if (!receipts.length && page.fromPage.name !== 'scan_receipt') {
            this.data.appView.get().router.load({pageName: 'scan_receipt'});
        }
        if (this.receiptIndex) {
            this.receiptIndex.set();
        }
    });
});

Template.view_register_receipts.helpers({
    receipts() {
        return this.receipts.list();
    },
    hasEditReceiptIndex() {
        // can be 0
        return !_.isUndefined(Template.instance().receiptIndex.get());
    },
    addReactiveVars() {
        const template = Template.instance();
        this.appView = template.data.appView;
        this.userInfo = template.data.userInfo;
        this.receipts = template.data.receipts;
        this.receiptIndex = template.receiptIndex;
        return this;
    },
    formatCurrency: Vatfree.numbers.formatCurrency,
    formatAmount: Vatfree.numbers.formatAmount
});

Template.view_register_receipts.events({
    'click .scan-receipt'(e, template) {
        template.data.receiptIndex.set();
        template.data.appView.get().router.load({pageName: 'scan_receipt'});
    },
    'click .review-input'(e, template) {
        template.data.appView.get().router.load({pageName: 'review_input'});
    },
    'click .content-receipt'(e, template) {
        e.preventDefault();
        let shopId = this.shopId;
        let index = null;
        let n = 0;
        _.each(template.data.receipts,(receipt) => {
            if (receipt.shopId === shopId) {
                index = n;
            }
            n++;
        });
        template.data.receiptIndex.set(index);
        template.data.appView.get().router.load({pageName: 'scan_receipt'});
    }
});

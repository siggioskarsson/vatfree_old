Template.view_register_check.helpers({
    userInfo() {
        return (this.userInfo ? this.userInfo.get() : {});
    },
    getFiles() {
        return Template.instance().data.passportPhotos.list();
    },
    passportExpirationDateFormatted() {
        if (this.profile && this.profile.passportExpirationDate) {
            return moment(this.profile.passportExpirationDate).format('YYYY-MM-DD');
        }
    },
    userVerified() {
        return this.private && this.private.status === 'userVerified';
    }
});

Template.view_register_check.events({
    'click #check li'(e) {
        let input = $(e.currentTarget).find('input');
        if (input && input.length) {
            input.focus();
        }
    },
    'change input[name="passportPhoto"]'(e, template) {
        e.preventDefault();
        const fileRead = e.currentTarget.files[0];
        const reader = new FileReader();
        reader.onload = function(fileLoadEvent) {
            let file = {
                file: {
                    name: file.name,
                    type: file.type,
                    size: file.size
                },
                fileContent: reader.result
            };

            Vatfree.saveFile(file, (err, filePath) => {
                if (!filePath) {
                    throw new Meteor.Error(500, 'Could not write file');
                }
                file.filePath = filePath;
                delete file.fileContent;

                template.data.passportPhotos.push(file);
                template.$('#passportUpload').val('');
                template.data.appView.get().router.load({pageName: 'passport'});
            });
        };
        reader.readAsDataURL(fileRead);
    },
    'click .add-file-button'(e, template) {
        if (window.EdgeCamera) {
            e.preventDefault();
            e.stopPropagation();

            let successCallback = function(base64Data) {
                Vatfree.imageHelpers.resetOrientation(base64Data, (err, base64Uri) => {
                    Vatfree.imageHelpers.resizeDataUriImage(base64Uri, 4032, function(err, base64Uri) {
                        let file = {
                            file: {
                                name: 'vatfree-scan-' + moment().format('YYYY-MM-DD_HH-mm-ss') + '.jpg',
                                type: 'image/jpeg',
                                size: Math.round((base64Uri.length) * 3 / 4)
                            },
                            fileContent: base64Uri
                        };

                        Vatfree.saveFile(file, (err, filePath) => {
                            if (!filePath) {
                                throw new Meteor.Error(500, 'Could not write file');
                            }
                            file.filePath = filePath;
                            delete file.fileContent;

                            template.data.passportPhotos.push(file);
                        });
                    });
                });
            };
            let errorCallback = function() {
                console.log(this, arguments);
            };
            let options = {

            };
            window.EdgeCamera.takePicture(successCallback, errorCallback, options);
        }
    },
    'click .add-photo-button'(e, template) {
        if (navigator.camera) {
            e.preventDefault();
            e.stopPropagation();

            let successCallback = function(base64Data) {
                Vatfree.imageHelpers.resetOrientation(base64Data, (err, base64Uri) => {
                    Vatfree.imageHelpers.resizeDataUriImage(base64Uri, 4032, function(err, base64Uri) {
                        let file = {
                            file: {
                                name: 'vatfree-photo-' + moment().format('YYYY-MM-DD_HH-mm-ss') + '.jpg',
                                type: 'image/jpeg',
                                size: Math.round((base64Uri.length) * 3 / 4)
                            },
                            fileContent: base64Uri
                        };

                        Vatfree.saveFile(file, (err, filePath) => {
                            if (!filePath) {
                                throw new Meteor.Error(500, 'Could not write file');
                            }
                            file.filePath = filePath;
                            delete file.fileContent;

                            template.data.passportPhotos.push(file);
                        });
                    });
                });
            };
            let errorCallback = function() {
                console.log(this, arguments);
                alert('Error taking picture')
            };
            let options = {
                quality: 90,
                destinationType: 0 // DATA_URL
            };
            navigator.camera.getPicture(successCallback, errorCallback, options);
        }
    },
    'click .scan-receipts'(e, template) {
        e.preventDefault();

        const formData = f7App.formToData('#check');
        if (formData.passportExpirationDate) {
            formData.passportExpirationDate = moment(formData.passportExpirationDate, 'YYYY-MM-DD').toDate();
        }

        let privateData = {};
        if (template.data.passportPhotos.array().length > 0) {
            privateData.status = 'userUnverified';
        }
        Meteor.call('update-traveller-profile', this._id, formData, privateData, (err, res) => {
            if (err) {
                console.log('Could not update traveller', err);
                return;
            }

            let userInfo = template.data.userInfo.get() || {};
            if (!userInfo.profile) userInfo.profile = {};
            _.each(formData, (value, key) => {
                userInfo.profile[key] = value;
            });
            template.data.userInfo.set(userInfo)
        });

        template.data.appView.get().router.load({pageName: 'receipts'});
    },
    'click .file-remove'(e, template) {
        e.preventDefault();
        e.stopPropagation();

        let self = this;
        let buttons1 = [
            {
                text: 'Delete file',
                color: 'red',
                onClick: function () {
                    const index = template.data.passportPhotos.indexOf(self);
                    if (index >= 0) {
                        template.data.passportPhotos.splice(index, 1);
                    }
                    return true;
                }
            }
        ];
        let buttons2 = [
            {
                text: 'Cancel'
            }
        ];
        let groups = [buttons1, buttons2];
        f7App.actions(groups);
    },
    'click .file-mask'(e, template) {
        e.preventDefault();
        e.stopPropagation();

        const index = template.data.passportPhotos.indexOf(this);
        if (index >= 0) {
            template.data.maskPhoto.set(index);
        }
    }
});

Template.view_register_check_country_input.onRendered(function() {
    const template = this;
    f7App.autocomplete({
        input: '#checkCountryName',
        openIn: 'dropdown',
        preloader: true,
        valueProperty: 'text',
        textProperty: 'text',
        expandInput: true,
        source: function (autocomplete, query, render) {
            if (query.length === 0) {
                render([]);
                return;
            }
            // Show Preloader
            autocomplete.showPreloader();
            Meteor.call('search-countries', query || "", 20, 0, true, (err, res) => {
                autocomplete.hidePreloader();
                render(res);
            });
        },
        onChange(autocomplete, value) {
            if (value.id) {
                template.$('#checkCountryId').val(value.id);
            }
        }
    });
});

import { Template } from 'meteor/templating';

Template.view_register.onCreated(function() {
    this.appView = new ReactiveVar();
    this.$$ = Dom7;

    this.onIndexPage = new ReactiveVar(true);
    this.userInfo = new ReactiveVar({});
    this.receipts = new ReactiveArray();
    this.receiptIndex = new ReactiveVar();
    this.passportPhotos = new ReactiveArray();
    this.maskPhoto = new ReactiveVar(false);
});

Template.view_register.onRendered(function() {
    const template = this;
    this.appView.set(f7App.addView('.view-main', {
        domCache: true
    }));

    f7App.onPageBeforeAnimation('index', (page) => {
        this.$('form').trigger('reset');
        this.userInfo.set({});
        this.receipts.clear();
        this.passportPhotos.clear();
        this.maskPhoto.set(false);
        Tracker.afterFlush(() => {
            // Placeholders don't get properly reset, stupid safari
            //https://stackoverflow.com/questions/39624902/new-input-placeholder-behavior-in-safari-10-no-longer-hides-on-change-via-java
            $('#registerEmail').focus();
        });
    });

    f7App.onPageBeforeAnimation('*', (page) => {
        this.onIndexPage.set(page.name === 'index')
    });
});

Template.view_register.helpers({
    getLocationName() {
        switch (Session.get('physical-location')) {
            case "DS":
                return "Desk Schiphol";
            case "MO":
            default:
                return "Office";
        }
    },
    onIndexPage() {
        return Template.instance().onIndexPage.get();
    },
    maskPhoto() {
        return Template.instance().maskPhoto.get() !== false;
    },
    addReactiveVars() {
        const template = Template.instance();
        this.appView = template.appView;
        this.userInfo = template.userInfo;
        this.receipts = template.receipts;
        this.receiptIndex = template.receiptIndex;
        this.passportPhotos = template.passportPhotos;
        this.maskPhoto = template.maskPhoto;
        return this;
    }
});

Template.view_register.events({
    'change input[name="amount"]'(e, template) {
        e.preventDefault();
        let amount = $(e.currentTarget).val();
        if (amount && amount > 0) {
            let totalVat = Vatfree.vat.calculateVat(amount*100, 21);
            $('input[name="totalVat"]').val(Vatfree.numbers.formatAmount(totalVat));
        }
    },
    'click .open-camera-test'(e) {
        e.preventDefault();
        EdgeCamera.takePicture(function(err, res) {
            console.log('Success', err, res)
        }, function(err) {
            console.log('Error', err);
        }, {
            quality: 90,
            allowEdit: true,
            cameraDirection: 'FRONT'
        });
    },
    'click .search-client'(e) {
        e.preventDefault();
        $('form[name="register-form"]').submit();
    },
    'submit form[name="register-form"]'(e, template) {
        e.preventDefault();

        const formData = f7App.formToData('form[name="register-form"]');
        Meteor.call('getUserInfo', formData, null, (err, res) => {
            // console.log('UserInfo found', err, res);
            if (res) {
                template.userInfo.set(res);
                template.appView.get().router.load({pageName: 'check'});
            } else {
                if (!formData.email) {
                    f7App.alert('No email provided');
                    return;
                }
                let user = {
                    profile: formData,
                    private: {}
                };
                if (confirm("Create traveller?\n" + formData.email)) {
                    Meteor.call('add-traveller', user, (err, res) => {
                        console.log('add-travvellers', err, res);
                        if (res) {
                            user._id = res;
                            user.private.status = 'userUnverified';
                            template.userInfo.set(user);
                            template.appView.get().router.load({pageName: 'check'});
                        } else {
                            f7App.alert('Could not create traveller');
                        }
                    });
                }
            }
        });
    },
    'click a.back'(e, template) {
        e.preventDefault();
        template.appView.get().router.back();
    }
});

Template.view_register_inputs.onRendered(function() {
    const template = this;
    f7App.autocomplete({
        input: '#registerEmail',
        openIn: 'dropdown',
        preloader: true,
        valueProperty: 'text',
        textProperty: 'text',
        expandInput: true,
        source: function (autocomplete, query, render) {
            if (query.length === 0) {
                render([]);
                return;
            }
            // Show Preloader
            autocomplete.showPreloader();
            Meteor.call('search-travellers', query || "", 20, 0, true, (err, res) => {
                autocomplete.hidePreloader();
                if (err) {
                    console.error(err);
                    render([]);
                } else {
                    render(res || []);
                }
            });
        },
        onChange(autocomplete, value) {
            if (value.id) {
                Meteor.call('getUserInfo', {}, value.id, (err, res) => {
                    if (res) {
                        template.data.userInfo.set(res);
                        template.data.appView.get().router.load({pageName: 'check'});
                    } else {
                        f7App.alert('Couldn\'t find traveller');
                    }
                });
            }
        }
    });
});

ReceiptUpload = new Ground.Collection('mdw-app-receipts');
ReceiptUploadFiles = new Ground.Collection('mdw-app-receipts-files');
ReceiptUploadFiles.removeFile = function(id) {
    let file = ReceiptUploadFiles.findOne({_id: id});
    if (file && file.file && file.file.filePath) {
        // remove file from file system
        Vatfree.removeFile(file.file.filePath, function(err, result) {
            if (err) {
                alert(err.message || err.reason);
                console.error(err);
            } else {
                ReceiptUploadFiles.remove({_id: id});
            }
        });
    } else {
        ReceiptUploadFiles.remove({_id: id});
    }
};

Tracker.autorun(() => {
    // check whether files are really uploaded
    let fileIds = [];
    ReceiptUploadFiles.find({
        uploaded: {
            $exists: true
        }
    }).forEach((file) => {
        fileIds.push(file.fileId);
    });
    Meteor.subscribe('uploaded-files', fileIds);

    Files.find({
        _id: {
            $in: fileIds
        },
        'copies.vatfree.createdAt': {
            $exists: true
        }
    }).forEach((file) => {
        let uploadFile = ReceiptUploadFiles.findOne({fileId: file._id});
        console.log('removing uploaded file ', uploadFile._id);
        ReceiptUploadFiles.removeFile(uploadFile._id);
    });
});

Template.view_register_review_input.onCreated(function() {
    this.estimatedRefund = new ReactiveVar(0);

    this.autorun(() => {
        if (Meteor.userId()) {
            let nextReceipt = ReceiptUpload.findOne({}, {
                sort: {
                    createdAt: 1
                }
            });
            if (nextReceipt) {
                Meteor.call('submit-receipt', nextReceipt.receipt, nextReceipt.userId, nextReceipt.archiveCode, (err, receiptId) => {
                    if (err) {
                        f7App.alert('Unable to submit receipt. ' + err.reason);
                        return;
                    } else {
                        ReceiptUploadFiles.find({
                            receiptUploadId: nextReceipt._id
                        }).forEach((fileUpload) => {
                            ReceiptUploadFiles.update({
                                _id: fileUpload._id
                            }, {
                                $set: {
                                    receiptId: receiptId,
                                    itemId: receiptId,
                                    upload: true
                                }
                            });
                        });
                        ReceiptUpload.remove({_id: nextReceipt._id});
                    }
                });
            }
        }
    });

    this.autorun(() => {
        // upload files
        let nextFile = ReceiptUploadFiles.findOne({
            upload: true,
            uploaded: {
                $exists: false
            }
        },{
            sort: {
                createdAt: 1
            }
        });
        if (nextFile) {
            // give the mobile some time to finish between file uploads
            let newFile = new FS.File();
            newFile.name(nextFile.file.file.name);
            newFile.target = nextFile.target;
            if (newFile.target.indexOf('receipts') === 0) {
                newFile.receiptId = nextFile.receiptId || nextFile.itemId;
                newFile.itemId = nextFile.itemId || nextFile.receiptId;
            } else if (newFile.target.indexOf('travellers') === 0) {
                newFile.userId = nextFile.userId || nextFile.itemId;
                newFile.itemId = nextFile.itemId || nextFile.userId;
            } else if (newFile.target.indexOf('shops') === 0) {
                newFile.shopId = nextFile.shopId || nextFile.itemId;
                newFile.itemId = nextFile.itemId || nextFile.shopId;
            }
            newFile.createdAt = new Date();
            newFile.uploadedBy = nextFile.uploadedBy;

            let fileContent = nextFile.file.fileContent;
            if (nextFile.file.filePath) {
                Vatfree.readFile(nextFile.file.filePath, function(err, fileContent) {
                    if (err) {
                        alert(err.message || err.reason);
                    } else {
                        uploadFile(newFile, fileContent, nextFile);
                    }
                });
            } else {
                uploadFile(newFile, fileContent, nextFile);
            }
        }
    });

    this.autorun(() => {
        const currentData = Template.currentData();
        const shopIds = [];
        _.each(currentData.receipts.array(), (receipt) => {
            if (receipt.shopId) {
                shopIds.push(receipt.shopId);
            }
        });

        Meteor.call('get-shop-fees', shopIds, (err, shopFees) => {
            if (err) {
                console.log('Something went wrong with fees retrieval', err);
                return;
            }

            let refund = 0;
            _.each(currentData.receipts.array(), (receipt) => {
                if (receipt.totalVat && receipt.shopId && _.has(shopFees, receipt.shopId)) {
                    const fees = shopFees[receipt.shopId];
                    refund += Math.max(0, Vatfree.vat.calculateRefund(receipt.totalVat, fees.serviceFee, fees.minFee, fees.maxFee));
                } else {
                    const fees = {
                        serviceFee: Vatfree.receipts.standardServiceFee,
                        minFee: Vatfree.receipts.standardMinFee,
                        maxFee:  Vatfree.receipts.standardMaxFee
                    };
                    refund += Math.max(0, Vatfree.vat.calculateRefund(receipt.totalVat, fees.serviceFee, fees.minFee, fees.maxFee));
                }
            });
            this.estimatedRefund.set(refund);
        });
    });
});
var uploadFile = function (newFile, fileContent, nextFile) {
    newFile.attachData(fileContent);
    Files.insert(newFile, function (err, fileObj) {
        if (err) {
            console.log(err);
            throw new Meteor.Error(500, 'Could not process photo of receipt');
        } else {
            ReceiptUploadFiles.update({
                _id: nextFile._id
            }, {
                $set: {
                    fileId: fileObj._id,
                    uploaded: new Date()
                }
            });
        }
    });
};

Template.view_register_review_input.helpers({
    userInfo() {
        return (this.userInfo ? this.userInfo.get() : {});
    },
    receipts() {
        return this.receipts.array();
    },
    passportExpirationDateFormatted() {
        if (this.profile && this.profile.passportExpirationDate) {
            return moment(this.profile.passportExpirationDate).format(TAPi18n.__('_date_format'));
        }
    },
    getTotalAmount() {
        let totalAmount = 0;
        if (this.receipts) {
            let receipts = this.receipts.array() || [];
            _.each(receipts, (receipt) => {
                if (receipt.amount) {
                    totalAmount += receipt.amount;
                }
            });
            return totalAmount;
        }
    },
    getTotalVat() {
        let totalVat = 0;
        if (this.receipts) {
            let receipts = this.receipts.array() || [];
            _.each(receipts, (receipt) => {
                if (receipt.totalVat) {
                    totalVat += receipt.totalVat;
                }
            });
        }
        return totalVat;
    },
    estimatedRefund() {
        return Template.instance().estimatedRefund.get();
    },
    formatCurrency: Vatfree.numbers.formatCurrency,
    formatAmount: Vatfree.numbers.formatAmount
});

Template.view_register_review_input.events({
    'click .file-thumb-container'(e, template) {
        e.preventDefault();
        Session.set('view_photo', this);
        template.data.appView.get().router.load({pageName: 'view_photo'});
    },
    'click .submit-receipts'(e, template) {
        let userId = this.userInfo.get()._id;
        let passportScans = this.passportPhotos.array();

        try {
            let shopIds = [];
            _.each(this.receipts.array(), (receipt) => {
                if (receipt.shopId) {
                    shopIds.push(receipt.shopId);
                }
            });
            shopIds = _.uniq(shopIds);

            if (passportScans) {
                _.each(passportScans, (file) => {
                    ReceiptUploadFiles.insert({
                        upload: true,
                        file: file,
                        target: 'travellers',
                        userId: userId,
                        itemId: userId,
                        uploadedBy: Meteor.userId(),
                        createdAt: new Date()
                    });
                });
            }

            Meteor.call('get-next-envelop-number', Session.get('physical-location'), shopIds, (err, archiveCode) => {
                if (err) {
                    f7App.alert(err.reason);
                } else {
                    _.each(this.receipts.array(), (receipt) => {
                        let files = _.clone(receipt.files);
                        delete receipt.files;
                        ReceiptUpload.insert({
                            archiveCode: archiveCode,
                            receipt: receipt,
                            userId: userId,
                            createdAt: new Date()
                        }, (err, receiptUploadId) => {
                            _.each(files, (file) => {
                                ReceiptUploadFiles.insert({
                                    upload: false,
                                    receiptUploadId: receiptUploadId,
                                    file: file,
                                    target: 'receipts',
                                    uploadedBy: Meteor.userId(),
                                    createdAt: new Date()
                                });
                            });
                        });
                    });

                    alert('Archive code: ' + archiveCode);

                    // Redirect back to
                    template.data.appView.get().router.back({
                        pageName: 'index',
                        force: true
                    });
                }
            });
        } catch(e) {
            alert(e.message);
        }
    }
});

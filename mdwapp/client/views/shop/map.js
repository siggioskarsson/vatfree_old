Template.view_shop_map.onCreated(function() {
    this.map = {};
});

Template.view_shop_map.onRendered(function() {
    Tracker.afterFlush(() => {
        this.map = window.plugins.google.maps.Map.getMap(this.$('.map_container'));
    });
});

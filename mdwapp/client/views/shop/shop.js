import { ReactiveVar } from "meteor/reactive-var";
import { Session } from "meteor/session";
import { Template } from "meteor/templating";

Template.view_shop.onCreated(function() {
    this.appView = new ReactiveVar();
    this.$$ = Dom7;

    this.shopId = new ReactiveVar();
    this.files = new ReactiveArray();
    this.stats = new ReactiveVar();
    this.nps = new ReactiveVar();

    this.materialChecked = new ReactiveVar();

    this.stack = new ReactiveVar(0);
    this.showShopMap = new ReactiveVar();
});

Template.view_shop.onRendered(function() {
    const template = this;
    this.subscribe('admins');

    this.autorun(() => {
        const shopId = this.shopId.get();
        if (shopId) {
            this.subscribe('activity-logs', '', {shopId: shopId}, {_id: 1}, 100, 0, true);
            Meteor.call('get-shop-visit-stats', shopId, (err, stats) => {
                if (err) {
                    f7App.alert(err.reason);
                } else {
                    this.stats.set(stats);
                }
            });
        }
    });

    Tracker.afterFlush(() => {
        this.appView.set(f7App.addView('.view-shop', {
            domCache: true, //enable inline pages
            onSwipeBackAfterChange: function(e) {
                template.stack.set(template.stack.get() - 1);

                if (template.appView.get().activePage.name === 'shop') {
                    Meteor.setTimeout(() => {
                        template.showShopMap.set(false);
                    }, 400);
                }
            }
        }));
    });
});

Template.view_shop.helpers({
    showShopMap() {
        return Template.instance().showShopMap.get();
    },
    shopId() {
        return Template.instance().shopId.get();
    },
    stackSize() {
        return Template.instance().stack.get();
    },
    materialChecked() {
        return Template.instance().materialChecked.get();
    },
    nps() {
        return Template.instance().nps.get();
    },
    npsBelow(number) {
        return Template.instance().nps.get() < number;
    },
    getShop() {
        return Shops.findOne({_id: Template.instance().shopId.get()});
    },
    addAppView() {
        this.appView = Template.instance().appView;
        this.shopId = Template.instance().shopId.get();
        return this;
    },
    getFiles() {
        return Template.instance().files.list();
    },
    getStats() {
        return Template.instance().stats.get();
    },
    getContactName(jobTitle) {
        const contact = _.find(Contacts.find().fetch(), (contact) => { return contact.profile.jobTitle === jobTitle;} );
        return contact ? contact.profile.name : '';
    },
    getContactEmail(jobTitle) {
        const contact = _.find(Contacts.find().fetch(), (contact) => { return contact.profile.jobTitle === jobTitle;} );
        return contact ? contact.profile.email : '';
    },
    getContactTel(jobTitle) {
        const contact = _.find(Contacts.find().fetch(), (contact) => { return contact.profile.jobTitle === jobTitle;} );
        return contact ? contact.profile.tel : '';
    },
    getActivityLogs() {
        return ActivityLogs.find({
            shopId: Template.instance().shopId.get()
        }, {
            sort: {
                createdAt: -1
            }
        });
    },
    getDataArray() {
        const data = [];
        _.each(this.data, (value, key) => {
            if (key === 'geo') return;
            data.push({
                key,
                value
            });
        });
        return data;
    },
    getDataValue(value) {
        if (_.isBoolean(value)) {
            return value ? 'Yes': 'No'
        } else if (_.isObject(value)) {
            return JSON.stringify(value);
        } else if (_.isArray(value)) {
            return value.join(', ');
        }

        return value;
    },
});

Template.view_shop.events({
    'change input[name="material"]'(e, template) {
        e.preventDefault();
        if ($(e.currentTarget).prop('checked')) {
            template.materialChecked.set(true);
        } else {
            template.materialChecked.set();
        }
    },
    'click .reset'(e, template) {
        e.preventDefault();
        template.shopId.set();
    },
    'click a.back'(e, template) {
        e.preventDefault();
        const appView = template.appView.get();

        appView.router.back();
        template.stack.set(template.stack.get() - 1);

        if (appView.activePage.name === 'shop') {
            Meteor.setTimeout(() => {
                template.showShopMap.set(false);
            }, 400);
        }
        if (appView.activePage.name === 'shopdetails') {
            Meteor.setTimeout(() => {
                Session.set('shops-learn-more');
            }, 400);
        }
    },
    'click .maps-icon'(e, template) {
        e.preventDefault();
        template.showShopMap.set(!template.showShopMap.get());
        Tracker.afterFlush(() => {
            template.appView.get().router.load({pageName: 'shop-map'});
            template.stack.set(template.stack.get() + 1);
        });
    },
    'change input[name="shopId"]'(e, template) {
        template.shopId.set($(e.currentTarget).val());
    },
    'change select[name="nps"]'(e, template) {
        template.nps.set($(e.currentTarget).val());
    },
    'change input[name="shopFile"]'(e, template) {
        e.preventDefault();
        var fileInput = e.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function(fileLoadEvent) {
            let file = {
                file: {
                    name: fileInput.name,
                    type: fileInput.type,
                    size: fileInput.size
                },
                fileContent: reader.result
            };

            Vatfree.saveFile(file, (err, filePath) => {
                if (!filePath) {
                    throw new Meteor.Error(500, 'Could not write file');
                }
                file.filePath = filePath;
                delete file.fileContent;

                template.files.push(file);
                template.$('input[name="shopFile"]').val('');
            });
        };
        reader.readAsDataURL(fileInput);
    },
    'click .add-photo-button'(e, template) {
        if (navigator.camera) {
            e.preventDefault();
            e.stopPropagation();

            let successCallback = function(base64Data) {
                Vatfree.imageHelpers.resetOrientation(base64Data, (err, base64Uri) => {
                    Vatfree.imageHelpers.resizeDataUriImage(base64Uri, 4032, function(err, base64Uri) {
                        // this is the standard format, so we set that up first for the file write functions
                        let file = {
                            file: {
                                name: 'vatfree-photo-' + moment().format('YYYY-MM-DD_HH-mm-ss') + '.jpg',
                                type: 'image/jpeg',
                                size: Math.round((base64Uri.length)*3/4)
                            },
                            fileContent: base64Uri
                        };

                        Vatfree.saveFile(file, (err, filePath) => {
                            if (!filePath) {
                                throw new Meteor.Error(500, 'Could not write file');
                            }
                            file.filePath = filePath;
                            delete file.fileContent;

                            template.files.push(file);
                        });
                    });
                });
            };
            let errorCallback = function() {
                console.log(this, arguments);
                alert('Error taking picture')
            };
            let options = {
                quality: 90,
                destinationType: 0 // DATA_URL
            };
            navigator.camera.getPicture(successCallback, errorCallback, options);
        }
    },
    'click .register-visit'(e, template) {
        e.preventDefault();

        const shopVisitData = {
            shopId: template.shopId.get(),
            notes: template.$('textarea[name="notes"]').val(),
            data: {
                material: !!template.$('input[name="material"]').prop('checked'),
                'material-amount': Number(template.$('select[name="material-amount"]').val() || 0),
                training: !!template.$('input[name="training"]').prop('checked'),
                sticker: !!template.$('input[name="sticker"]').prop('checked'),
                triangle: !!template.$('input[name="triangle"]').prop('checked'),
                geo: Session.get('location')
            }
        };

        const nps = template.$('input[name="nps"]').prop('checked');
        if (nps) {
            shopVisitData.data.nps = nps;
        }
        const npsReason = template.$('input[name="npsReason"]').prop('checked');
        if (npsReason) {
            shopVisitData.data.npsReason = npsReason;
        }

        const ambassadorName = template.$('input[name="ambassadorName"]').val();
        const ambassadorEmail = template.$('input[name="ambassadorEmail"]').val();
        const ambassadorTel = template.$('input[name="ambassadorTel"]').val();
        const storeManagerName = template.$('input[name="storeManagerName"]').val();
        const storeManagerEmail = template.$('input[name="storeManagerEmail"]').val();
        const storeManagerTel = template.$('input[name="storeManagerTel"]').val();
        const storeManagerName2 = template.$('input[name="storeManagerName2"]').val();
        const storeManagerEmail2 = template.$('input[name="storeManagerEmail2"]').val();
        const storeManagerTel2 = template.$('input[name="storeManagerTel2"]').val();
        const affiliatedWith = template.$('select[name="affiliatedWith"]').val();
        const status = template.$('select[name="status"]').val();
        const shopData = {
            ambassadorName,
            ambassadorEmail,
            ambassadorTel,
            storeManagerName,
            storeManagerEmail,
            storeManagerTel,
            storeManagerName2,
            storeManagerEmail2,
            storeManagerTel2,
            affiliatedWith,
            status
        };

        Meteor.call('add-shop-visit', shopVisitData, shopData, (err, activityId) => {
            if (err) {
                alert(err.reason);
            } else {
                // upload files
                // TODO
                _.each(template.files.list(), (file) => {
                    ReceiptUploadFiles.insert({
                        upload: true,
                        file: file,
                        target: 'shops',
                        itemId: shopVisitData.shopId,
                        uploadedBy: Meteor.userId(),
                        createdAt: new Date()
                    });
                });

                template.shopId.set();
                template.materialChecked.set();
                template.$('#scanReceiptShopId').val('').trigger('change');
                template.$('#scanReceiptShopName').html('');
            }
        });
    },
    'click .add-shop-activity'(e, template) {
        e.preventDefault();
        const shop = Shops.findOne({_id: this.toString() });
        Session.set('activityObj', {
            name: shop.name || shop.email,
            shopId: shop._id
        });
        f7App.popup('.popup-activity');
    }
});

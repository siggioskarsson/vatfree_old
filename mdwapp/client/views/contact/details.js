Template.view_contact_details.onCreated(function() {
    this.nrTravellers = new ReactiveVar();
    this.avgSpend = new ReactiveVar();
});

Template.view_contact_details.onRendered(function() {
    this.subscribe('countries');
    this.subscribe('currencies');

    this.autorun(() => {
        const contactInfo = this.data.contactInfo.get();

        if (contactInfo && contactInfo.contact) {
            this.nrTravellers.set(contactInfo.contact.nrTravellers || 0);
            this.avgSpend.set(contactInfo.contact.avgSpend || 0);
        }
    });
});

Template.view_contact_details.helpers({
    isDisabled() {
        return !Vatfree.userIsInRole(Meteor.userId(), 'contacts-update');
    },
    contactInfo() {
        return (this.contactInfo ? this.contactInfo.get() : {});
    },
    userVerified() {
        return this.private && this.private.status === 'userVerified';
    },
    nrTravellers() {
        return Template.instance().nrTravellers.get();
    },
    avgSpend() {
        return Template.instance().avgSpend.get();
    },
    getExpectedEarnings() {
        const nrTravellers = Number(Template.instance().nrTravellers.get());
        const avgSpend = Number(Template.instance().avgSpend.get());

        const expectedTurnover = nrTravellers * avgSpend * 100;
        const expectedVat = expectedTurnover / 1.21;
        const expectedFees = expectedVat * 0.3;
        return {
            expectedTurnover: expectedTurnover,
            expectedVat: expectedVat,
            expectedFees: expectedFees,
            expectedEarnings: expectedFees * 0.1
        };
    }
});

Template.view_contact_details.events({
    'change input[name="contact.nrTravellers"]'(e, template) {
        e.preventDefault();
        template.nrTravellers.set($(e.currentTarget).val());
    },
    'change input[name="contact.avgSpend"]'(e, template) {
        e.preventDefault();
        template.avgSpend.set($(e.currentTarget).val());
    },
    'click #check li'(e) {
        let input = $(e.currentTarget).find('input');
        if (input && input.length) {
            input.focus();
        }
    },
    'click .add-contact-activity'(e, template) {
        e.preventDefault();
        Session.set('activityObj', {
            name: template.$('input[name="name"]').val() || template.$('input[name="email"]').val(),
            contactId: this._id
        });
        f7App.popup('.popup-activity');
    },
    'click .save-contact'(e, template) {
        e.preventDefault();
        template.$('form[name="contact"]').submit();
    },
    'submit form[name="contact"]'(e, template) {
        e.preventDefault();
        const formData = f7App.formToData('form[name="contact"]');
        const currentContactData = template.data.contactInfo.get();

        const contactData = {
            $set: formData
        };

        Meteor.call('contacts-update', currentContactData._id, contactData, function(err, result) {
            if (err) {
                f7App.alert(err.reason || err.message);
            } else {
                f7App.alert('Contact saved!')
            }
        });
    }
});

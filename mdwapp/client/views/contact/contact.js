import { Template } from 'meteor/templating';

Template.view_contact.onCreated(function() {
    this.appView = new ReactiveVar();
    this.$$ = Dom7;

    this.onIndexPage = new ReactiveVar(true);
    this.contactInfo = new ReactiveVar({});
});

Template.view_contact.onRendered(function() {
    this.appView.set(f7App.addView('.view-contact', {
        domCache: true
    }));

    f7App.onPageBeforeAnimation('index', (page) => {
        this.$('form').trigger('reset');
        this.contactInfo.set({});
        Tracker.afterFlush(() => {
            // Placeholders don't get properly reset, stupid safari
            //https://stackoverflow.com/questions/39624902/new-input-placeholder-behavior-in-safari-10-no-longer-hides-on-change-via-java
            $('#registerEmail').focus();
        });
    });

    f7App.onPageBeforeAnimation('*', (page) => {
        this.onIndexPage.set(page.name === 'index')
    });
});

Template.view_contact.helpers({
    onIndexPage() {
        return Template.instance().onIndexPage.get();
    },
    addReactiveVars() {
        const template = Template.instance();
        this.appView = template.appView;
        this.contactInfo = template.contactInfo;

        return this;
    }
});

Template.view_contact.events({
    'click .search-client'(e) {
        e.preventDefault();
        $('form[name="register-contact-form"]').submit();
    },
    'submit form[name="register-contact-form"]'(e, template) {
        e.preventDefault();

        const formData = f7App.formToData('form[name="register-contact-form"]');
        Meteor.call('getContactInfo', formData, null, (err, res) => {
            // console.log('UserInfo found', err, res);
            if (res) {
                template.contactInfo.set(res);
                template.appView.get().router.load({pageName: 'contact_details'});
            } else {
                if (!formData.email) {
                    f7App.alert('No email provided');
                    return;
                }
                if (confirm("Create contact?\n" + formData.email)) {
                    Meteor.call('contacts-insert', formData, (err, contactId) => {
                        if (err) {
                            f7App.alert(err.reason || err.message);
                        } else {
                            Meteor.call('getContactInfo', {}, contactId, (err, contact) => {
                                if (err) {
                                    f7App.alert(err.reason || err.message);
                                } else {
                                    template.contactInfo.set(contact);
                                    template.appView.get().router.load({pageName: 'contact_details'});
                                }
                            });
                        }
                    });
                }
            }
        });
    }
});


Template.view_register_contact_inputs.onRendered(function() {
    const template = this;
    f7App.autocomplete({
        input: '#contactEmail',
        openIn: 'dropdown',
        preloader: true,
        valueProperty: 'text',
        textProperty: 'text',
        expandInput: true,
        source: function (autocomplete, query, render) {
            if (query.length === 0) {
                render([]);
                return;
            }
            // Show Preloader
            autocomplete.showPreloader();
            Meteor.call('search-contacts', query || "", 20, 0, true, (err, res) => {
                autocomplete.hidePreloader();
                if (err) {
                    console.error(err);
                    render([]);
                } else {
                    render(res || []);
                }
            });
        },
        onChange(autocomplete, value) {
            if (value.id) {
                Meteor.call('getContactInfo', {}, value.id, (err, res) => {
                    if (res) {
                        template.data.contactInfo.set(res);
                        template.data.appView.get().router.load({pageName: 'contact_details'});
                    } else {
                        f7App.alert('Couldn\'t find contact');
                    }
                });
            }
        }
    });
});

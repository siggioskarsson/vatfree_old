import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';

Template.view_refund.onCreated(function() {
    this.appView = {};
    this.$$ = Dom7;
});

Template.view_refund.onRendered(function() {
    let template = this;

    this.appView = f7App.addView('.view-refund', {
        domCache: true //enable inline pages
    });
});

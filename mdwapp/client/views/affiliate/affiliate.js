import { Template } from 'meteor/templating';

Template.view_affiliate.onCreated(function() {
    this.appView = new ReactiveVar();
    this.$$ = Dom7;

    this.onIndexPage = new ReactiveVar(true);
    this.affiliateInfo = new ReactiveVar({});
});

Template.view_affiliate.onRendered(function() {
    this.appView.set(f7App.addView('.view-affiliate', {
        domCache: true
    }));

    f7App.onPageBeforeAnimation('index', (page) => {
        this.$('form').trigger('reset');
        this.affiliateInfo.set({});
        Tracker.afterFlush(() => {
            // Placeholders don't get properly reset, stupid safari
            //https://stackoverflow.com/questions/39624902/new-input-placeholder-behavior-in-safari-10-no-longer-hides-on-change-via-java
            $('#registerEmail').focus();
        });
    });

    f7App.onPageBeforeAnimation('*', (page) => {
        this.onIndexPage.set(page.name === 'index')
    });
});

Template.view_affiliate.helpers({
    onIndexPage() {
        return Template.instance().onIndexPage.get();
    },
    addReactiveVars() {
        const template = Template.instance();
        this.appView = template.appView;
        this.affiliateInfo = template.affiliateInfo;

        return this;
    }
});

Template.view_affiliate.events({
    'click .search-client'(e) {
        e.preventDefault();
        $('form[name="register-affiliate-form"]').submit();
    },
    'submit form[name="register-affiliate-form"]'(e, template) {
        e.preventDefault();

        const formData = f7App.formToData('form[name="register-affiliate-form"]');
        Meteor.call('getAffiliateInfo', formData, null, (err, res) => {
            // console.log('UserInfo found', err, res);
            if (res) {
                template.affiliateInfo.set(res);
                template.appView.get().router.load({pageName: 'affiliate_details'});
            } else {
                if (!formData.email) {
                    f7App.alert('No email provided');
                    return;
                }
                if (confirm("Create affiliate?\n" + formData.email)) {
                    Meteor.call('affiliates-insert', formData, (err, affiliateId) => {
                        if (err) {
                            f7App.alert(err.reason || err.message);
                        } else {
                            Meteor.call('getAffiliateInfo', {}, affiliateId, (err, affiliate) => {
                                if (err) {
                                    f7App.alert(err.reason || err.message);
                                } else {
                                    template.affiliateInfo.set(affiliate);
                                    template.appView.get().router.load({pageName: 'affiliate_details'});
                                }
                            });
                        }
                    });
                }
            }
        });
    }
});


Template.view_register_affiliate_inputs.onRendered(function() {
    const template = this;
    f7App.autocomplete({
        input: '#affiliateEmail',
        openIn: 'dropdown',
        preloader: true,
        valueProperty: 'text',
        textProperty: 'text',
        expandInput: true,
        source: function (autocomplete, query, render) {
            if (query.length === 0) {
                render([]);
                return;
            }
            // Show Preloader
            autocomplete.showPreloader();
            Meteor.call('search-affiliates', query || "", 20, 0, true, (err, res) => {
                autocomplete.hidePreloader();
                if (err) {
                    console.error(err);
                    render([]);
                } else {
                    render(res || []);
                }
            });
        },
        onChange(autocomplete, value) {
            if (value.id) {
                Meteor.call('getAffiliateInfo', {}, value.id, (err, res) => {
                    if (res) {
                        template.data.affiliateInfo.set(res);
                        template.data.appView.get().router.load({pageName: 'affiliate_details'});
                    } else {
                        f7App.alert('Couldn\'t find affiliate');
                    }
                });
            }
        }
    });
});

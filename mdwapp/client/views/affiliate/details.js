Template.view_affiliate_details.onCreated(function() {
    this.nrTravellers = new ReactiveVar();
    this.avgSpend = new ReactiveVar();
    this.showFees = new ReactiveVar();
});

Template.view_affiliate_details.onRendered(function() {
    this.subscribe('countries');
    this.subscribe('currencies');

    this.autorun(() => {
        const affiliateInfo = this.data.affiliateInfo.get();

        if (affiliateInfo && affiliateInfo.affiliate) {
            this.nrTravellers.set(affiliateInfo.affiliate.nrTravellers || 0);
            this.avgSpend.set(affiliateInfo.affiliate.avgSpend || 0);
        }
    });
});

Template.view_affiliate_details.helpers({
    isDisabled() {
        return !Vatfree.userIsInRole(Meteor.userId(), 'affiliates-update');
    },
    affiliateInfo() {
        return (this.affiliateInfo ? this.affiliateInfo.get() : {});
    },
    userVerified() {
        return this.private && this.private.status === 'userVerified';
    },
    nrTravellers() {
        return Template.instance().nrTravellers.get();
    },
    avgSpend() {
        return Template.instance().avgSpend.get();
    },
    showFees() {
        return Template.instance().showFees.get();
    },
    getExpectedEarnings() {
        const nrTravellers = Number(Template.instance().nrTravellers.get());
        const avgSpend = Number(Template.instance().avgSpend.get());

        const expectedTurnover = nrTravellers * avgSpend * 100;
        const expectedVat = expectedTurnover - (expectedTurnover / 1.21);
        const expectedFees = expectedVat * 0.3;
        return {
            expectedTurnover: expectedTurnover,
            expectedVat: expectedVat,
            expectedFees: expectedFees,
            expectedEarnings: expectedFees * 0.1
        };
    }
});

let expectedVatClicked = false;
Template.view_affiliate_details.events({
    'click .expected-vat'(e, template) {
        e.preventDefault();
        if (expectedVatClicked) {
            template.showFees.set(!template.showFees.get());
            expectedVatClicked = false;
        } else {
            expectedVatClicked = true;
        }

        Meteor.setTimeout(() => {
            expectedVatClicked = false;
        }, 500);
    },
    'change input[name="affiliate.nrTravellers"]'(e, template) {
        e.preventDefault();
        template.nrTravellers.set($(e.currentTarget).val());
    },
    'change input[name="affiliate.avgSpend"]'(e, template) {
        e.preventDefault();
        template.avgSpend.set($(e.currentTarget).val());
    },
    'click #check li'(e) {
        let input = $(e.currentTarget).find('input');
        if (input && input.length) {
            input.focus();
        }
    },
    'click .add-affiliate-activity'(e, template) {
        e.preventDefault();
        Session.set('activityObj', {
            name: template.$('input[name="profile.name"]').val() || template.$('input[name="profile.email"]').val(),
            affiliateId: this._id
        });
        f7App.popup('.popup-activity');
    },
    'click .save-affiliate'(e, template) {
        e.preventDefault();
        template.$('form[name="affiliate"]').submit();
    },
    'click .add-photo-button'(e, template) {
        e.preventDefault();
        e.stopPropagation();

        if (navigator.camera) {
            const currentAffiliateData = template.data.affiliateInfo.get();

            let successCallback = function(base64Data) {
                Vatfree.imageHelpers.resetOrientation(base64Data, (err, base64Uri) => {
                    Vatfree.imageHelpers.resizeDataUriImage(base64Uri, 4032, function(err, base64Uri) {
                        // this is the standard format, so we set that up first for the file write functions
                        let file = {
                            name: 'vatfree-photo-' + moment().format('YYYY-MM-DD_HH-mm-ss') + '.jpg',
                            type: 'image/jpeg',
                            size: Math.round((base64Uri.length)*3/4)
                        };

                        const newFile = new FS.File();
                        newFile.name('vatfree-photo-' + moment().format('YYYY-MM-DD_HH-mm-ss') + '.jpg');
                        newFile.target = 'affiliates';
                        newFile.itemId = currentAffiliateData._id;

                        newFile.createdAt = new Date();
                        newFile.uploadedBy = Meteor.userId();

                        newFile.attachData(base64Uri);
                        Files.insert(newFile, function (err, fileObj) {
                            if (err) {
                                console.log(err);
                                alert('Could not process photo');
                            }
                        });
                    });
                });
            };
            let errorCallback = function() {
                console.log(this, arguments);
                alert('Error taking picture')
            };
            let options = {
                quality: 90,
                destinationType: 0 // DATA_URL
            };
            navigator.camera.getPicture(successCallback, errorCallback, options);
        } else {
            alert('Please use the mobile app');
        }
    },
    'submit form[name="affiliate"]'(e, template) {
        e.preventDefault();
        const formData = f7App.formToData('form[name="affiliate"]');
        const currentAffiliateData = template.data.affiliateInfo.get();

        const affiliateData = {
            $set: {}
        };
        _.each(formData, (value, key) => {
            const k = key.split('.');
            if (!affiliateData.$set[k[0]]) affiliateData.$set[k[0]] = {};
            affiliateData.$set[k[0]][k[1]] = value;
        });
        Meteor.call('affiliates-update', currentAffiliateData._id, affiliateData, function(err, result) {
            if (err) {
                f7App.alert(err.reason || err.message);
            } else {
                f7App.alert('Affiliate saved!')
            }
        });
    }
});

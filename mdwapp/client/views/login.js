Template.login.events({
    'click .login-with-google'(e) {
        e.preventDefault();
        Meteor.loginWithGoogle({
            requestPermissions: ['email', 'profile']
        }, (err) => {
            if (err) {
                console.log(err);
                alert(err.reason || err.message);
            } else {
                // successful login!
                console.log('successful login!');
            }
        });
    }
});

Template.activity_popup.helpers({
    getActivityObj() {
        return Session.get('activityObj') || {};
    }
});

Template.activity_popup.events({
    'click .save-activity'(e, template) {
        e.preventDefault();
        template.$('form[name="add-activity-log"]').submit();
    },
    'submit form[name="add-activity-log"]'(e, template) {
        e.preventDefault();
        const formData = f7App.formToData('form[name="add-activity-log"]');
        if (formData.due) formData.due = moment(formData.due, 'YYYY-MM-DD').toDate();

        Meteor.call('activity-log-insert', formData, function(err, result) {
            if (err) {
                f7App.alert(err.reason || err.message);
            } else {
                f7App.closeModal();
           }
        });
    }
});

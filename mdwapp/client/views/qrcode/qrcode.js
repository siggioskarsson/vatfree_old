import uriParse from 'url-parse';

Template.view_qrcode.onCreated(function() {
    this.context = new ReactiveVar();
});

Template.view_qrcode.onRendered(function() {
    this.autorun(() => {
        let context = this.context.get();
        if (context) {
            if (context.target === 'travellers') {
                this.subscribe('travellers_item', context.itemId);
            }
            if (context.target === 'receipts') {
                this.subscribe('receipts_item', context.itemId);
            }
            this.subscribe('countries');
        }
    });

    this.autorun(() => {
        let user = Meteor.user() || {};
        let uploadContext = user.profile ? user.profile.qrUploadContext : false;
        this.context.set(uploadContext);
    });
});

Template.view_qrcode.helpers({
    currentContext() {
        return Template.instance().context.get();
    },
    getTraveller() {
        let context = Template.instance().context.get();
        if (context && context.target === 'travellers') {
            return Travellers.findOne({
                _id: context.itemId
            });
        }
    },
    getTravellerDoc() {
        return Travellers.findOne({
            _id: this.userId
        });
    },
    getReceipt() {
        let context = Template.instance().context.get();
        if (context && context.target === 'receipts') {
            return Receipts.findOne({
                _id: context.itemId
            });
        }
    },
    getShop() {
        return Shops.findOne({_id: this.shopId});
    },
    getId() {
        let id = '';
        _.each(this, (value, key) => {
            if (key !== 'target') {
                id = value;
            }
        });

        return id;
    }
});

Template.view_qrcode.events({
    'click .reset'(e, template) {
        e.preventDefault();
        template.context.set();
    },
    'click .scan-qr-code'(e, template) {
        e.preventDefault();

        if (Meteor.isCordova) {
            cordova.plugins.barcodeScanner.scan(
                function (result) {
                    console.log("We got a barcode\n" +
                        "Result: " + result.text + "\n" +
                        "Format: " + result.format + "\n" +
                        "Cancelled: " + result.cancelled);
                    let uri = uriParse(result.text, true);
                    if (uri && uri.protocol === 'vatfree:' && uri.hostname === 'image-upload' && uri.query && uri.query.context) {
                        template.context.set(JSON.parse(uri.query.context));
                    } else {
                        f7App.alert('Failed parsing QR code. Is this a valid vatfree QR code?');
                    }
                },
                function (error) {
                    f7App.alert("Scanning failed: " + error);
                },
                {
                    preferFrontCamera : false,
                    showFlipCameraButton : true,
                    showTorchButton : true,
                    torchOn: false,
                    prompt : "Place a barcode inside the scan area", // Android
                    resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                    formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
                    orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
                    disableAnimations : true, // iOS
                    disableSuccessBeep: false // iOS
                }
            );
        } else {
            template.context.set({
                itemId: 'd6csdcGkKEQzMAMQP',
                target: 'travellers'
            });
        }
    },
    'change input[name="photoFile"]'(e, template) {
        e.preventDefault();
        let file = e.currentTarget.files[0];
        let reader = new FileReader();
        reader.onload = function(fileLoadEvent) {
            let base64 = reader.result.substr(reader.result.indexOf(',') + 1);
            Vatfree.imageHelpers.resetOrientation(base64, (err, base64Uri) => {
                Vatfree.imageHelpers.resizeDataUriImage(base64Uri, 4032, function(err, base64Uri) {
                    if (template.context.get()) {
                        let newFile = {
                            upload: true,
                            file: {
                                file: {
                                    name: 'vatfree-photo-' + moment().format('YYYY-MM-DD_HH-mm-ss') + '.jpg',
                                    type: file.type,
                                    size: file.size
                                },
                                fileContent: base64Uri
                            },
                            uploadedBy: Meteor.userId(),
                            createdAt: new Date(),
                        };

                        _.each(template.context.get(), (contextValue, contextKey) => {
                            newFile[contextKey] = contextValue;
                        });

                        ReceiptUploadFiles.insert(newFile);
                    }
                });
            });
        };
        reader.readAsDataURL(file);
    },
    'click .add-photo-button'(e, template) {
        e.preventDefault();
        if (window.EdgeCamera) {
            e.preventDefault();
            e.stopPropagation();

            let successCallback = function(base64Data) {
                Vatfree.imageHelpers.resetOrientation(base64Data, (err, base64Uri) => {
                    Vatfree.imageHelpers.resizeDataUriImage(base64Uri, 4032, function(err, base64Uri) {
                        let newFile = {
                            upload: true,
                            file: {
                                file: {
                                    name: 'vatfree-scan-' + moment().format('YYYY-MM-DD_HH-mm-ss') + '.jpg',
                                    type: 'image/jpeg',
                                    size: Math.round((base64Uri.length) * 3 / 4)
                                },
                                fileContent: base64Uri
                            },
                            uploadedBy: Meteor.userId(),
                            createdAt: new Date(),
                        };

                        _.each(template.context.get(), (contextValue, contextKey) => {
                            newFile[contextKey] = contextValue;
                        });

                        ReceiptUploadFiles.insert(newFile);
                    });
                });
            };
            let errorCallback = function() {
                console.log(this, arguments);
                alert('Error taking picture')
            };
            let options = {};
            window.EdgeCamera.takePicture(successCallback, errorCallback, options);
        }
    }
});

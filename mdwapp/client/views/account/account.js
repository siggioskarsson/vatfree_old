Template.view_account_content.onCreated(function() {
    this.physicalLocation = new ReactiveVar(localStorage.getItem('physical-location'));

    this.autorun(() => {
        let physicalLocation = this.physicalLocation.get();
        Session.set('physical-location', physicalLocation);
        localStorage.setItem('physical-location', physicalLocation);
    });
});

Template.view_account_content.helpers({
    isSelected(a, b) {
        return a === b ? "selected" : false;
    },
    physicalLocation() {
        return Template.instance().physicalLocation.get();
    },
    filesToUpload() {
        return ReceiptUploadFiles.find({}, {
            sort: {
                createdAt: 1
            }
        });
    }
});

Template.view_account_content.events({
    'change select[name="physical-location"]'(e, template) {
        template.physicalLocation.set($(e.currentTarget).val());
    },
    'click .logout'(e) {
        e.preventDefault();
        Meteor.logout();
    },
    'click .item-link.file-upload'(e, template) {
        e.preventDefault();
        if (this.receiptId || this.itemId) {
            ReceiptUploadFiles.update({
                _id: this._id
            },{
                $set: {
                    upload: true
                },
                $unset: {
                    uploaded: true
                }
            });
        } else {
            f7App.alert('Cannot upload file. Receipt has not been created.');
            // reset date to re-trigger autorun for receipts
            ReceiptUpload.update({
                _id: this.receiptUploadId
            }, {
                $set: {
                    createdAt: new Date()
                }
            });
        }
    },
    'click .item-link.file-upload .delete-file'(e, template) {
        e.preventDefault();
        ReceiptUploadFiles.removeFile(this._id);
    }
});

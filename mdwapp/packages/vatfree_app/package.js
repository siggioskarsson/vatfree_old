Package.describe({
    name: 'vatfree:app',
    summary: 'Vatfree app package',
    version: '0.0.1',
    git: ''
});

Package.onUse(function (api) {
    api.use([
        'vatfree:core',
        'vatfree:countries',
        'vatfree:currencies',
        'vatfree:receipts',
        'vatfree:shops',
        'vatfree:invoices',
        'vatfree:companies',
        'vatfree:affiliates',
        'vatfree:contacts',
        'vatfree:activity-logs',
        'vatfree:travellers',
        'vatfree:notify',
        'vatfree:email-templates',
        'vatfree:email-texts',
        'vatfree:payouts',
        'vatfree:traveller-types',
        'vatfree:traveller-rejections'
    ], 'server');

    api.use([
        'vatfree:core'
    ], 'client');

    api.export([
        'FS',
        'Vatfree',
        'Files',
        'BillingSchema',
        'CreatedUpdatedSchema',
        'StatusDates',
        'Counters',
        'Lock',
        'Locks',
        'GoogleMaps',
        'Countries',
        'Currencies',
        'Shops',
        'Invoices',
        'Companies',
        'Contacts',
        'Payouts',
        'Receipts',
        'Travellers',
        'TravellerTypes',
        'TravellerRejections',
        'EmailTexts',
        'EmailTemplates',
        'ActivityLogs',
        'Affiliates'
    ]);
});

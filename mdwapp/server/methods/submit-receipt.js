import { Meteor } from "meteor/meteor";

Meteor.methods({
    'submit-passport'(userId, passportScans) {
        this.unblock();
        if (!Vatfree.userIsInRole(this.userId, 'travellers-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        check(userId, String);
        check(passportScans, Array);

        if (passportScans) {
            _.each(passportScans, (file) => {
                let newFile = new FS.File();
                newFile.name(file.file.name);
                newFile.userId = userId;
                newFile.itemId = userId;
                newFile.target = 'travellers';
                newFile.createdAt = new Date();
                newFile.uploadedBy = this.userId;
                newFile.attachData(file.fileContent);
                Files.insert(newFile, function (err) {
                    if (err) {
                        console.log(err);
                        throw new Meteor.Error(500, 'Could not process photo of receipt');
                    }
                });
            });
        }

        return true;
    },
    'submit-receipt'(receipt, userId, archiveCode) {
        this.unblock();
        if (!Vatfree.userIsInRole(this.userId, 'receipts-create')) {
            throw new Meteor.Error(404, 'access denied');
        }

        check(receipt, Object);
        check(userId, String);

        let receiptData = {
            userId: userId,
            shopId: receipt.shopId,
            amount: receipt.amount,
            totalVat: receipt.totalVat,
            serviceFee: 0,
            refund: 0,
            nonStandardVat: !!receipt.nonStandardVat,
            purchaseDate: receipt.purchaseDate,
            customsDate: receipt.customsDate,
            customsCountryId: Meteor.settings.defaults.countryId || null,
            currencyId: Meteor.settings.defaults.currencyId,
            status: 'userPending',
            source: 'desk',
            channel: 'inperson',
            archiveCode: archiveCode,
            archiveBox: 'deskClient',
            originalsCheckedBy: this.userId,
            originalsCheckedAt: new Date()
        };

        let traveller = Meteor.users.findOne({
            _id: userId
        });
        if (!traveller) {
            throw new Meteor.Error(404, 'User could not be found');
        }

        let travellerType = TravellerTypes.findOne({_id: traveller.private.travellerTypeId});
        if (travellerType && travellerType.forceReceiptTypeId) {
            receiptData.receiptTypeId = travellerType.forceReceiptTypeId;
        }

        if (travellerType && travellerType.isNato) {
            receiptData.serviceFee = 0;
            receiptData.refund = receiptData.totalVat;
            receiptData.nonStandardVat = true;
        } else {
            let fees = Vatfree.receipts.getFees(receipt.shopId);
            if (fees.deferredServiceFee) {
                receiptData.serviceFee = 0;
                receiptData.deferredServiceFee = Vatfree.vat.calculateFee(receiptData.totalVat, fees.deferredServiceFee, fees.deferredMinFee, fees.deferredMaxFee);
                receiptData.refund = receiptData.totalVat;
            } else {
                receiptData.serviceFee = Vatfree.vat.calculateFee(receiptData.totalVat, fees.serviceFee, fees.minFee, fees.maxFee);
                receiptData.refund = Vatfree.vat.calculateRefund(receiptData.totalVat, fees.serviceFee, fees.minFee, fees.maxFee);
            }
        }

        console.log(JSON.stringify(receiptData));
        return Receipts.insert(receiptData, console.log);
    }
});

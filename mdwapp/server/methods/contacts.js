import { Meteor } from "meteor/meteor";

Meteor.methods({
    'getContactInfo'(searchFields, contactId) {
        if (!Vatfree.userIsInRole(this.userId, 'contacts-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {
            'roles.__global_roles__' : 'contact'
        };
        if (contactId) {
            selector._id = contactId;
        } else {
            selector['$or'] = [];
            _.each(searchFields, (value, key) => {
                selector['$or'].push({
                    ['profile.' + key]: value
                });
            });
        }

        let contact = Meteor.users.findOne(selector, {
            fields: {
                _id: 1,
                profile: 1,
                private: 1,
                contact: 1
            }
        });
        if (contact && contact.profile) {
            const country = Countries.findOne({
                _id: contact.profile.countryId
            });
            if (country) {
                contact.profile.country = country.name;
                contact.profile.countryCode = country.code;
            }
        }

        return contact;
    }
});

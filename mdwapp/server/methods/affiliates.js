import { Meteor } from "meteor/meteor";

Meteor.methods({
    'getAffiliateInfo'(searchFields, affiliateId) {
        if (!Vatfree.userIsInRole(this.userId, 'affiliates-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {
            'roles.__global_roles__' : 'affiliate'
        };
        if (affiliateId) {
            selector._id = affiliateId;
        } else {
            selector['$or'] = [];
            _.each(searchFields, (value, key) => {
                selector['$or'].push({
                    ['profile.' + key]: value
                });
            });
        }

        let affiliate = Meteor.users.findOne(selector, {
            fields: {
                _id: 1,
                profile: 1,
                private: 1,
                affiliate: 1
            }
        });
        if (affiliate && affiliate.profile) {
            const country = Countries.findOne({
                _id: affiliate.profile.countryId
            });
            if (country) {
                affiliate.profile.country = country.name;
                affiliate.profile.countryCode = country.code;
            }
        }

        return affiliate;
    }
});

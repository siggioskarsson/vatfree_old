import { Meteor } from "meteor/meteor";

Meteor.methods({
    'search-countries'(searchTerm, limit, offset) {
        this.unblock();
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        let selector = {};
        if (searchTerm) {
            check(searchTerm, String);
            Vatfree.search.addSearchTermSelector(searchTerm, selector);
        }

        const countries = [];
        Countries.find(selector, {
            sort: {
                code: 1,
                name: 1
            },
            limit: limit,
            offset: offset
        }).forEach((country) => {
            countries.push({
                text: country.code + ' ' + country.name,
                id: country._id
            });
        });

        return countries;
    }
});

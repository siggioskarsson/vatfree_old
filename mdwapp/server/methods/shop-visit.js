import { Meteor } from "meteor/meteor";

Meteor.methods({
    'add-shop-visit'(data, shopData) {
        if (!Vatfree.userIsInRole(this.userId, 'shops-update')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const activityData = {
            type: 'shop-visit',
            subject: 'Visited shop',
            status: 'done',
            userId: this.userId
        };
        _.extend(activityData, data);

        Shops.update({
            _id: data.shopId
        }, {
            $set: {
                affiliatedWith: shopData.affiliatedWith,
                status: shopData.status,
                nps: activityData.data.nps,
                npsReason: activityData.data.npsReason
            }
        });

        const processContact = function(shopId, jobTitle, contactName, contactEmail, contactTel) {
            if (contactName || contactEmail) {
                const existingContact = Meteor.users.findOne({
                    'profile.shopId': shopId,
                    'profile.jobTitle': jobTitle
                });
                if (existingContact) {
                    Meteor.users.update({
                        _id: existingContact._id
                    }, {
                        $set: {
                            'profile.name': contactName,
                            'profile.email': contactEmail,
                            'profile.tel': contactTel
                        }
                    });
                } else {
                    Meteor.users.insert({
                        profile: {
                            'shopId': shopId,
                            'jobTitle': jobTitle,
                            'name': contactName,
                            'email': contactEmail,
                            'tel': contactTel,
                        },
                        private: {},
                        roles: {
                            __global_roles__: ['contact']
                        }
                    });
                }
            } else {
                Meteor.users.remove({
                    'profile.shopId': shopId,
                    'profile.jobTitle': jobTitle
                });
            }
        };
        processContact(data.shopId, "AMBASSADOR", shopData.ambassadorName, shopData.ambassadorEmail, shopData.ambassadorTel);
        processContact(data.shopId, "STOREMANAGER", shopData.storeManagerName, shopData.storeManagerEmail, shopData.storeManagerTel);
        processContact(data.shopId, "STOREMANAGER2", shopData.storeManagerName2, shopData.storeManagerEmail2, shopData.storeManagerTel2);

        return ActivityLogs.insert(activityData);
    },
    'get-shop-visit-stats'(shopId) {
        if (!Vatfree.userIsInRole(this.userId, 'shops-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        const mYear = moment().subtract(1, 'year');
        const mQuarter = moment().subtract(1, 'quarter');
        const mMonth = moment().subtract(1, 'month');
        const stats = {
            year: 0,
            quarter: 0,
            month: 0
        };
        Receipts.find({
            shopId: shopId,
            createdAt: {
                $gte: mYear.toDate()
            }
        }, {
            fields: {
                createdAt: 1
            }
        }).forEach((r) => {
            let mCreatedAt = moment(r.createdAt);
            if (mCreatedAt.isAfter(mYear)) {
                stats.year++;
            }
            if (mCreatedAt.isAfter(mQuarter)) {
                stats.quarter++;
            }
            if (mCreatedAt.isAfter(mMonth)) {
                stats.month++;
            }
        });
        console.log(stats);

        return stats
    }
});

import { Meteor } from "meteor/meteor";

Meteor.methods({
    'getUserInfo'(searchFields, userId) {
        if (!Vatfree.userIsInRole(this.userId, 'travellers-read')) {
            throw new Meteor.Error(404, 'access denied');
        }

        let selector = {
            'roles.__global_roles__' : 'traveller'
        };
        if (userId) {
            selector._id = userId;
        } else {
            selector['$or'] = [];
            _.each(searchFields, (value, key) => {
                selector['$or'].push({
                    ['profile.' + key]: value
                });
            });
        }

        let user = Meteor.users.findOne(selector, {
            fields: {
                profile: 1,
                private: 1
            }
        });
        if (user && user.profile) {
            const country = Countries.findOne({
                _id: user.profile.countryId
            });
            if (country) {
                user.profile.country = country.name;
                user.profile.countryCode = country.code;
            }
        }

        return user;
    }
});

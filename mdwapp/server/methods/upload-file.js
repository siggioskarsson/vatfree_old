import { Meteor } from "meteor/meteor";

Meteor.methods({
    'upload-file'(file, context) {
        if (!Roles.userIsInRole(this.userId, 'admin', Roles.GLOBAL_GROUP)) {
            if (!Roles.userIsInRole(this.userId, 'vatfree', Roles.GLOBAL_GROUP)) {
                throw new Meteor.Error(404, 'access denied');
            }
        }

        check(file, Object);
        check(context, Object);

        let newFile = new FS.File();
        newFile.name(file.file.name);
        _.each(context, (contextValue, contextKey) => {
            check(contextKey, String);
            check(contextValue, String);
            newFile[contextKey] = contextValue;
        });
        newFile.createdAt = new Date();
        newFile.uploadedBy = this.userId;
        newFile.attachData(file.fileContent);

        Files.insert(newFile, function (err) {
            if (err) {
                console.log(err);
                throw new Meteor.Error(500, 'Could not process file');
            }
        });

        return true;
    }
});

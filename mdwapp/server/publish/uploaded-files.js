Meteor.publish('uploaded-files', function(fileIds) {
    check(fileIds, Array);
    if (!Roles.userIsInRole(this.userId, ['admin', 'vatfree'], Roles.GLOBAL_GROUP)) {
        throw new Meteor.Error(404, 'access denied');
    }

    return Files.find({
        _id: {
            $in: fileIds
        }
    });
});

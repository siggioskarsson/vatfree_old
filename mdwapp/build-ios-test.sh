#!/usr/bin/env bash
rm mobile-config.js
ln -s ../mdwapp-mobile-config-test.js mobile-config.js

meteor build build --server=https://vatfree-mdw-app-test.herokuapp.com
